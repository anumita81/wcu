<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('signup', '\App\Http\Controllers\Api\UsersController@saveapiuser');
Route::post('activateuser', '\App\Http\Controllers\Api\UsersController@activateuser');
Route::post('signin', '\App\Http\Controllers\Api\UsersController@userlogin');
Route::post('userloginforce', '\App\Http\Controllers\Api\UsersController@userloginforce');
Route::post('resendactivationmail', '\App\Http\Controllers\Api\UsersController@resendactivationmail');

Route::post('signout', '\App\Http\Controllers\Api\UsersController@userlogout');
Route::post('updateuser/{id?}', '\App\Http\Controllers\Api\UsersController@updateuser');
Route::post('usershippingaddress/{type?}', '\App\Http\Controllers\Api\UsersController@usershippingaddress');
Route::post('usergroupshippingaddress/{type?}', '\App\Http\Controllers\Api\UsersController@usergroupshippingaddress');
Route::post('usershipmentsetting', '\App\Http\Controllers\Api\UsersController@usershipmentsetting');
Route::post('usertermssetting', '\App\Http\Controllers\Api\UsersController@usertermssetting');
Route::post('saveshippingaddress/{userId}/{id?}', '\App\Http\Controllers\Api\UsersController@saveshippingaddress');
Route::get('setdefaultshipping/{userId}/{id?}/{type?}', '\App\Http\Controllers\Api\UsersController@setdefaultshipping');
Route::post('updateprofileimage/{userid}', '\App\Http\Controllers\Api\UsersController@updateprofileimage');
Route::post('saveshipmentsettings/{userid}', '\App\Http\Controllers\Api\UsersController@saveshipmentsettings');
Route::get('getshipmentsettings/{userId}', '\App\Http\Controllers\Api\UsersController@getshipmentsettings');
Route::post('sendreferralinviation/{userId}', '\App\Http\Controllers\Api\UsersController@sendreferralinviation');
Route::post('virtualtourfinished', '\App\Http\Controllers\Api\UsersController@virtualtourfinished');
Route::post('updatevirtualtourlog', '\App\Http\Controllers\Api\UsersController@updatevirtualtourlog');
Route::post('checkvirtualtourfinished', '\App\Http\Controllers\Api\UsersController@checkvirtualtourfinished');
Route::post('saveSignature/{userId}', '\App\Http\Controllers\Api\UsersController@saveSignature');
Route::post('getaccountmanager', '\App\Http\Controllers\Api\UsersController@getaccountmanager');
Route::post('getgroupmanager', '\App\Http\Controllers\Api\UsersController@getgroupmanager');

Route::post('savecontactus', '\App\Http\Controllers\Api\ContentController@savecontactusrequest');
Route::post('savesendmessage', '\App\Http\Controllers\Api\ContentController@savesendmessage');
Route::post('saveContact', '\App\Http\Controllers\Api\ContentController@saveContact');

Route::get('banner/{type?}', '\App\Http\Controllers\Api\ContentController@getbannerimage');
Route::get('homepageblock', '\App\Http\Controllers\Api\ContentController@gethomepageblockcontents');
Route::get('storedata', '\App\Http\Controllers\Api\ContentController@getstoredata');
Route::get('registrationbanner', '\App\Http\Controllers\Api\ContentController@getregistrationbanner');
Route::get('registrationbannerById/{id?}', '\App\Http\Controllers\Api\ContentController@registrationbannerById');
Route::get('getcontactinfo', '\App\Http\Controllers\Api\ContentController@getcontactinfo');
Route::post('getcontactinfo', '\App\Http\Controllers\Api\ContentController@getcontactinfo');
Route::get('gethomepageadbanner', '\App\Http\Controllers\Api\ContentController@gethomepageadbanner');

Route::get('getallwarehouse', '\App\Http\Controllers\Api\DataController@getallwarehouse');
Route::get('getcountry/{fromPage?}/{type?}', '\App\Http\Controllers\Api\DataController@getcountrylist');
Route::get('getstatecity/{id?}', '\App\Http\Controllers\Api\DataController@getstatecitylist');
Route::get('getcity/{id?}', '\App\Http\Controllers\Api\DataController@getcitylist');
Route::get('getisdcode', '\App\Http\Controllers\Api\DataController@getisdcodelist');
Route::post('getlocationdetails', '\App\Http\Controllers\Api\DataController@getlocationdetails');
Route::get('getlocationcountry', '\App\Http\Controllers\Api\DataController@getlocationcountrylist');
Route::get('getlocationstate/{id?}', '\App\Http\Controllers\Api\DataController@getlocationstatelist');
Route::get('getlocationcity/{id?}', '\App\Http\Controllers\Api\DataController@getlocationcitylist');

Route::post('content', '\App\Http\Controllers\Api\ContentController@getpagecontent');
Route::post('getpagetype', '\App\Http\Controllers\Api\ContentController@getpagetype');
Route::get('getpagetype', '\App\Http\Controllers\Api\ContentController@getpagetype');
Route::post('getquote/fetchquote', '\App\Http\Controllers\Api\GetquoteController@fetchquote');
Route::post('getquote/fetchitemquote', '\App\Http\Controllers\Api\GetquoteController@fetchitemquote');
Route::post('getquote/fetchautoquote', '\App\Http\Controllers\Api\GetquoteController@fetchautoquote');
Route::get('getquote/citylist/{id?}', '\App\Http\Controllers\Api\DataController@getcitylist');
Route::post('getstore', '\App\Http\Controllers\Api\DataController@getstorelist');
Route::post('getcategory', '\App\Http\Controllers\Api\DataController@getcategorylist');
Route::get('getsubcategory/{id?}', '\App\Http\Controllers\Api\DataController@getsubcategorylist');
Route::get('getproduct/{id?}', '\App\Http\Controllers\Api\DataController@getproductlist');
Route::get('getmakelist', '\App\Http\Controllers\Api\DataController@getmakelist');
Route::get('getmodellist/{id?}', '\App\Http\Controllers\Api\DataController@getmodellist');
Route::post('getcurrency', '\App\Http\Controllers\Api\DataController@getcurrencylist');
Route::get('getcurrency', '\App\Http\Controllers\Api\DataController@getcurrencylist');
Route::get('getwarehouselist', '\App\Http\Controllers\Api\DataController@getwarehouselist');
Route::get('getprocessingfee/{itemprice}/{warehouseid}', '\App\Http\Controllers\Api\DataController@getprocessingfee');
Route::get('getuserwarehouselist/{userId?}', '\App\Http\Controllers\Api\DataController@getuserwarehouselist');
Route::post('getautoallcharges/{userId}/{warehouseid}', '\App\Http\Controllers\Api\DataController@getautoallcharges');
//Route::get('getwarehouselist/{userId}', '\App\Http\Controllers\Api\DataController@getwarehouselist');
Route::post('getshipmycarallcharges', '\App\Http\Controllers\Api\DataController@getshipmycarallcharges');
Route::get('getcurrencylist', '\App\Http\Controllers\Api\DataController@getcurrencylist');
Route::post('getcurrencylist', '\App\Http\Controllers\Api\DataController@getcurrencylist');
Route::post('getpaymentmethodlist', '\App\Http\Controllers\Api\DataController@getpaymentmethodlist');
Route::get('getdefaultcurrency', '\App\Http\Controllers\Api\DataController@getdefaultcurrency');
Route::get('getdefaultcurrencycode', '\App\Http\Controllers\Api\DataController@getdefaultcurrencycode');
Route::post('getamountforpaystack', '\App\Http\Controllers\Api\DataController@getamountforpaystack');
Route::post('getamountforpayeezy', '\App\Http\Controllers\Api\DataController@getamountforpayeezy');
Route::post('savepaystackreference', '\App\Http\Controllers\Api\DataController@savepaystackreference');
Route::post('getamountforpaypalstandard', '\App\Http\Controllers\Api\DataController@getamountforpaypalstandard');

Route::post('getstoreByCatergory', '\App\Http\Controllers\Api\DataController@getstoreByCatergory');

Route::get('getshopfromwarehouselist/{userId}', '\App\Http\Controllers\Api\DataController@getshopfromwarehouselist');

Route::get('getallwarehouseshopfrom', '\App\Http\Controllers\Api\DataController@getallwarehouseshopfrom');


Route::post('submitContact', '\App\Http\Controllers\Api\GetquoteController@submitContact');
Route::post('getquote/saveitemquotecontact', '\App\Http\Controllers\Api\GetquoteController@saveitemquotecontact');



Route::get('getsettings/{contact_number}', '\App\Http\Controllers\Api\HeaderController@getsettings');
Route::get('getemail/{contact_email}', '\App\Http\Controllers\Api\HeaderController@getemail');

/* Save data into temp cart table */
Route::post('savetempcartdata','\App\Http\Controllers\Api\UsersController@savetempcartdata');

/* Auto */
Route::get('getwebsiteist', '\App\Http\Controllers\Api\AutoController@getwebsiteist');
Route::get('getwebsitemake/{websiteId?}', '\App\Http\Controllers\Api\AutoController@getwebsitemake');
Route::get('getwebsitemodel/{websiteId}/{makeId}', '\App\Http\Controllers\Api\AutoController@getwebsitemodel');
Route::post('saveothervehicledata', '\App\Http\Controllers\Api\AutoController@saveothervehicledata');
Route::post('savebuycardata/{userId}/{warehouseId}', '\App\Http\Controllers\Api\AutoController@savebuycardata');
Route::post('saveshipmycar', '\App\Http\Controllers\Api\AutoController@saveshipmycar');
Route::post('getuserautocart', '\App\Http\Controllers\Api\AutoController@getuserautocart');
Route::post('updatecartcurrency', '\App\Http\Controllers\Api\UsersController@updatecartcurrency');
Route::post('updatecartcontent', '\App\Http\Controllers\Api\UsersController@updatecartcontent');
Route::post('checkautocartdata', '\App\Http\Controllers\Api\UsersController@checkautocartdata');
Route::post('deleteautocartdata', '\App\Http\Controllers\Api\UsersController@deleteautocartdata');
Route::post('autovalidatecoupon', '\App\Http\Controllers\Api\AutoController@validatecouponcode');
Route::post('autoinvoice', '\App\Http\Controllers\Api\AutoController@printinvoice');
Route::get('getlocationtype', '\App\Http\Controllers\Api\AutoController@getlocationtype');


/* Shop For Me */
Route::post('updateusercart/{userId}/{type}', '\App\Http\Controllers\Api\UsersController@updateusercart');
Route::post('getusercartlist', '\App\Http\Controllers\Api\UsersController@getusercartlist');
Route::post('savecartlist/{userId}', '\App\Http\Controllers\Api\UsersController@savecartlist');
Route::post('submitprocurementdata/{userId}', '\App\Http\Controllers\Api\ProcurementController@submitprocurementdata');
Route::post('recalculateshipmentcost', '\App\Http\Controllers\Api\ProcurementController@recalculateshipmentcost');
Route::post('calculateshippingcost', '\App\Http\Controllers\Api\ProcurementController@calculateshippingcost');
Route::post('removecartitem', '\App\Http\Controllers\Api\UsersController@removecartitem');
Route::post('getsaveforlaterlist', '\App\Http\Controllers\Api\UsersController@getsaveforlaterlist');
Route::post('movetousercart', '\App\Http\Controllers\Api\UsersController@movetousercart');
Route::post('submitautopartsdata/{userId}', '\App\Http\Controllers\Api\ProcurementController@submitautopartsdata');
Route::post('getautopartslist', '\App\Http\Controllers\Api\UsersController@getautopartslist');
Route::post('getusercartsessionlist', '\App\Http\Controllers\Api\UsersController@getusercartsessionlist');
Route::post('clearusercart', '\App\Http\Controllers\Api\UsersController@clearusercart');
Route::post('submitorder/{userId}', '\App\Http\Controllers\Api\ProcurementController@submitorder');
Route::post('validateewallet', '\App\Http\Controllers\Api\UsersController@validateewallet');
Route::post('getprocurementshippingcost', '\App\Http\Controllers\Api\ProcurementController@getprocurementshippingcost');
Route::post('submitrequestforcostdata/{userId}', '\App\Http\Controllers\Api\ProcurementController@submitrequestforcostdata');
Route::post('getinvoice', '\App\Http\Controllers\Api\ProcurementController@getinvoice');
Route::post('getprocurementItems', '\App\Http\Controllers\Api\ProcurementController@getprocurementItems');

/* Auto Parts */

Route::post('placeorder/{userId}', '\App\Http\Controllers\Api\ProcurementController@placeorder');
Route::post('getautopartsinvoice', '\App\Http\Controllers\Api\ProcurementController@getautopartsinvoice');
Route::post('updateusercartautopartsdata', '\App\Http\Controllers\Api\ProcurementController@updateusercartautoparts');
Route::post('submitrequestforcostautopartsdata/{userId}', '\App\Http\Controllers\Api\ProcurementController@submitrequestforcostautopartsdata');
Route::post('getautopartsItems', '\App\Http\Controllers\Api\ProcurementController@getautopartsItems');

/* Fill and Ship */
Route::post('findmybox/{userId}', '\App\Http\Controllers\Api\FillshipController@findmybox');
Route::post('fillshipselectbox/{userId}', '\App\Http\Controllers\Api\FillshipController@fillshipselectbox');
Route::post('submitfillshipdata/{userId}', '\App\Http\Controllers\Api\FillshipController@submitfillshipdata');
Route::post('updatefillshipdata/{userId}', '\App\Http\Controllers\Api\FillshipController@updatefillshipdata');
Route::post('submitfillshiporder/{userId}', '\App\Http\Controllers\Api\FillshipController@submitorder');
Route::post('validatefillshipcoupon', '\App\Http\Controllers\Api\FillshipController@validatecouponcode');
Route::post('getuserfillship', '\App\Http\Controllers\Api\FillshipController@getuserfillship');
Route::post('fillshipshipout', '\App\Http\Controllers\Api\FillshipController@fillshipshipout');
Route::post('payfillshipextracharge', '\App\Http\Controllers\Api\FillshipController@payfillshipextracharge');
Route::post('fillshipdeliverydetails', '\App\Http\Controllers\Api\FillshipController@fillshipdeliverydetails');
Route::get('fillshipalladdedboxes', '\App\Http\Controllers\Api\FillshipController@alladdedboxes');



/* FAQs */
Route::get('faq/', '\App\Http\Controllers\Api\ContentController@getFaq');

Route::post('payauthorizedotnet/', '\App\Http\Controllers\Api\PayController@payauthorizedotnet');
Route::post('paypaypalpro/', '\App\Http\Controllers\Api\PayController@paypaypalpro');
Route::post('getPayMethod/', '\App\Http\Controllers\Api\PayController@getpaymethod');

Route::post('processProcurementPaid/{id}', '\App\Http\Controllers\Api\ProcurementController@processprocurementpaid');
Route::post('processProcurementDetails/{id}', '\App\Http\Controllers\Api\ProcurementController@processprocurementdetails');
Route::post('getProcurementShipmentDetails', '\App\Http\Controllers\Api\ProcurementController@getprocurementshipmentdetails');
Route::post('getProcurementShipmentForAutoPartsDetails', '\App\Http\Controllers\Api\ProcurementController@getprocurementshipmentforautopartsdetails');

Route::post('updateusercartdata', '\App\Http\Controllers\Api\ProcurementController@updateusercartdata');

Route::post('validateCouponCode', '\App\Http\Controllers\Api\ProcurementController@validatecouponcode');


/* Car I have bought */
Route::post('getcarihaveboughtdetails', '\App\Http\Controllers\Api\AutoController@getcarihaveboughtdetails');

/* My Warehouse under Auto */
Route::post('getcarwarehousedetails', '\App\Http\Controllers\Api\AutoController@getcarwarehousedetails');
Route::get('getautowarehouseimage/{imageId}', '\App\Http\Controllers\Api\AutoController@getautowarehouseimage');


/* Payment */
Route::post('getpaymenthistory', '\App\Http\Controllers\Api\UsersController@getpaymenthistory');
Route::post('getinvoicelist', '\App\Http\Controllers\Api\UsersController@getinvoicelist');
Route::post('getpaymentmethodbyuser', '\App\Http\Controllers\Api\UsersController@getpaymentmethodlist');
Route::post('getinvoicedetail', '\App\Http\Controllers\Api\UsersController@getinvoice');
Route::post('payinvoice', '\App\Http\Controllers\Api\UsersController@payinvoice');
Route::post('getpaymentinvoice', '\App\Http\Controllers\Api\UsersController@getpaymentinvoice');

/* My Warehouse */
Route::post('getusershipmentlist', '\App\Http\Controllers\Api\ShipmentController@getusershipmentlist');
Route::post('allshippingmethodlist', '\App\Http\Controllers\Api\ShipmentController@allshippingmethodlist');
Route::post('calculatedeliveryshipping', '\App\Http\Controllers\Api\ShipmentController@calculatedeliveryshipping');
Route::post('adddeliveryothercharges', '\App\Http\Controllers\Api\ShipmentController@adddeliveryothercharges');
Route::post('addpackagesnapshot', '\App\Http\Controllers\Api\ShipmentController@addpackagesnapshot');
Route::post('sendwronginventorynotification', '\App\Http\Controllers\Api\ShipmentController@sendwronginventorynotification');
Route::post('updateinventory', '\App\Http\Controllers\Api\ShipmentController@updateinventory');
Route::post('getshippimgmethod', '\App\Http\Controllers\Api\ShipmentController@getshippimgmethod');
Route::post('warehousecheckout', '\App\Http\Controllers\Api\ShipmentController@warehousecheckout');
Route::post('updateuserwarehouse/{userId}', '\App\Http\Controllers\Api\ShipmentController@updateuserwarehouse');
Route::post('validateshipmentcoupon', '\App\Http\Controllers\Api\ShipmentController@validatecouponcode');
Route::post('submitshipmentorder/{userId}', '\App\Http\Controllers\Api\ShipmentController@submitorder');
Route::post('uploadshipmentinvoice', '\App\Http\Controllers\Api\ShipmentController@uploadshipmentinvoice');
Route::post('getgalleryImages', '\App\Http\Controllers\Api\ShipmentController@getgalleryImages');
Route::post('updateitemprice', '\App\Http\Controllers\Api\ShipmentController@updateitemprice');
Route::post('uploaddicountedinvoice', '\App\Http\Controllers\Api\ShipmentController@uploaddicountedinvoice');
Route::post('getdiscounteduploadedinvoice', '\App\Http\Controllers\Api\ShipmentController@getdiscounteduploadedinvoice');
Route::get('downloadinvoice/{packageId}', '\App\Http\Controllers\Api\ShipmentController@downloadinvoice');
Route::post('getDelieveryItems', '\App\Http\Controllers\Api\ShipmentController@getDelieveryItems');
Route::post('updateReturnData', '\App\Http\Controllers\Api\ShipmentController@updateReturnData');
Route::post('payreturn', '\App\Http\Controllers\Api\ShipmentController@payreturn');
Route::post('getdeliverydetails', '\App\Http\Controllers\Api\ShipmentController@getDeliveryDetails');

/* Group Shipment */
Route::post('getusergroupshipmentlist', '\App\Http\Controllers\Api\ShipmentController@getusergroupshipmentlist');
Route::post('getgroupshipmentdeliverydetails', '\App\Http\Controllers\Api\ShipmentController@getgroupshipmentdeliverydetails');
Route::post('allshippingmethodlistforgroup', '\App\Http\Controllers\Api\ShipmentController@allshippingmethodlistforgroup');
Route::post('getgroupshippingmethod', '\App\Http\Controllers\Api\ShipmentController@getgroupshippimgmethod');

/* Shipment order history */
Route::post('shipmentorderhistory', '\App\Http\Controllers\Api\ShipmentController@shipmentorderhistory');
Route::get('getallshipmentstatus', '\App\Http\Controllers\Api\ShipmentController@getallshipmentstatus');
Route::post('getshipmentdetails', '\App\Http\Controllers\Api\ShipmentController@getshipmentdetails');
Route::post('printshipmentinvoice', '\App\Http\Controllers\Api\ShipmentController@printshipmentinvoice');
Route::post('getshipmentsnapshots', '\App\Http\Controllers\Api\ShipmentController@getshipmentsnapshots');


/* get Partners data in home page */
Route::get('getpartnercontent', '\App\Http\Controllers\Api\ContentController@getpartnercontent');
Route::get('getpartnerbanking', '\App\Http\Controllers\Api\ContentController@getpartnerbanking');
Route::get('getpartnerlogistic', '\App\Http\Controllers\Api\ContentController@getpartnerlogistic');

/* get Reward details */
Route::post('getrewarddetails', '\App\Http\Controllers\Api\ContentController@getrewarddetails');
Route::post('getpointtransction', '\App\Http\Controllers\Api\ContentController@getpointtransction');
Route::post('getwalletcode', '\App\Http\Controllers\Api\ContentController@getwalletcode');


/* Track Shipment - has item arrived */
Route::post('itemarriveddetails', '\App\Http\Controllers\Api\DataController@itemarriveddetails');
Route::post('verifyitemarrivedtime', '\App\Http\Controllers\Api\DataController@verifyitemarrivedtime');
Route::post('verifytrackingdetails', '\App\Http\Controllers\Api\DataController@verifytrackingdetails');

/* Track Shipment - track with order number */
Route::post('shipmenttrackingdetails', '\App\Http\Controllers\Api\DataController@shipmenttrackingdetails');


Route::post('getpaystacksettings', '\App\Http\Controllers\Api\ContentController@getpaystacksettings');
Route::post('getpaypalipnsettings', '\App\Http\Controllers\Api\ContentController@getpaypalipnsettings');

/* my documents details */
Route::post('getdocuments', '\App\Http\Controllers\Api\ContentController@getdocuments');
Route::post('deletedocument', '\App\Http\Controllers\Api\ContentController@deletedocument');
Route::post('uploaddocument/{userId}', '\App\Http\Controllers\Api\ContentController@uploaddocument');

/* Claim File */
Route::post('postclaim/{userId}', '\App\Http\Controllers\Api\ContentController@postclaim');
Route::post('getclaims', '\App\Http\Controllers\Api\ContentController@getclaims');

/* Messages */
Route::post('getmessages', '\App\Http\Controllers\Api\ContentController@getmessages');
Route::post('getmessagecontent', '\App\Http\Controllers\Api\ContentController@getmessagecontent');

Route::post('subscriptionpayment', '\App\Http\Controllers\Api\UsersController@subscriptionpayment');


/* User Forgot Password */
Route::post('userforgotPassword', '\App\Http\Controllers\Api\ContentController@userforgotpassword');
Route::post('userforgotPasswordToken', '\App\Http\Controllers\Api\ContentController@userforgotpasswordtoken');

/* Get Menus */
Route::get('getheadernavigation', '\App\Http\Controllers\Api\ContentController@getheadernavigation');
Route::get('getfooternavigation', '\App\Http\Controllers\Api\ContentController@getfooternavigation');
Route::get('getsociallinks', '\App\Http\Controllers\Api\ContentController@getsociallinks');

/* Help */
Route::get('getallcustomerhelpcategory', '\App\Http\Controllers\Api\ContentController@getallcustomerhelpcategory');
Route::post('firsthelpsection', '\App\Http\Controllers\Api\ContentController@firsthelpsection');

/* Contact Us Reasons */
Route::post('getcontactusreasons', '\App\Http\Controllers\Api\ContentController@getcontactusreasons');

/* Send OTP for modify default shipping address */
Route::post('sendotpforshippingaddress', '\App\Http\Controllers\Api\UsersController@sendotpforshippingaddress');
Route::post('checkotpforshippingaddress', '\App\Http\Controllers\Api\UsersController@checkotpforshippingaddress');
Route::post('resendotpforshippingaddress', '\App\Http\Controllers\Api\UsersController@resendotpforshippingaddress');

Route::get('processpayeezy', '\App\Http\Controllers\Api\DataController@processpayeezy');

Route::get('getsubscriptiondetails', '\App\Http\Controllers\Api\DataController@getsubscriptiondetails');
Route::post('paymentforsubscription', '\App\Http\Controllers\Api\UsersController@paymentforsubscription');
Route::post('getusersubscriptiondetail', '\App\Http\Controllers\Api\UsersController@getusersubscriptiondetail');

/* Group Request */
Route::post('savegrouprequest/{userId?}','\App\Http\Controllers\Api\UsersController@savegrouprequest');
Route::get('getgroupinfo/{userId?}','\App\Http\Controllers\Api\UsersController@getgroupinfo');
Route::post('getalluseremail/{userId?}/{string?}','\App\Http\Controllers\Api\UsersController@getalluseremail');
Route::post('sendinvitation','\App\Http\Controllers\Api\UsersController@sendinvitation');
Route::post('updategroupinvitationstatus','\App\Http\Controllers\Api\UsersController@updategroupinvitationstatus');
Route::post('getcoordinatorcommissioninfo','\App\Http\Controllers\Api\UsersController@getcoordinatorcommissioninfo');
Route::post('savecoordinatorcccountinfo','\App\Http\Controllers\Api\UsersController@savecoordinatorcccountinfo');
Route::post('requestforcommission','\App\Http\Controllers\Api\UsersController@requestforcommission');

/* Mobile API */

/* user signup */
Route::post('mobile/signup', '\App\Http\Controllers\Api\mobile\UsersController@saveapiuser_mobile');
Route::post('mobile/signin', '\App\Http\Controllers\Api\mobile\UsersController@userlogin_mobile');

/* Verified OTP */
Route::post('mobile/getotp', '\App\Http\Controllers\Api\mobile\UsersController@getotp_mobile');

/* Resend OTP */
Route::post('mobile/resendotp', '\App\Http\Controllers\Api\mobile\UsersController@resendotp_mobile');

/* Update user/customer profile */
Route::post('mobile/updateuser', '\App\Http\Controllers\Api\mobile\UsersController@updateuser_mobile');

/* Update shipping and billing address of customers */
Route::post('mobile/saveshippingaddress', '\App\Http\Controllers\Api\mobile\UsersController@saveshippingaddress_mobile');

/* List all addressbook records of a customer */
Route::post('mobile/getshippingaddress', '\App\Http\Controllers\Api\mobile\UsersController@getshippingaddress_mobile');

/* Set a billing or shipping address as default for a customer */
Route::post('mobile/setdefaultshipping', '\App\Http\Controllers\Api\mobile\UsersController@setdefaultshipping_mobile');

/* Get Master Data */
Route::post('mobile/getmasterdata', '\App\Http\Controllers\Api\mobile\UsersController@getmasterdata_mobile');

/* Show user details */
Route::post('mobile/getuserdetails', '\App\Http\Controllers\Api\mobile\UsersController@getuserdetails_mobile');
Route::post('mobile/getshipmentcounts', '\App\Http\Controllers\Api\mobile\UsersController@getshipmentcounts_mobile');

/* Change user password */
Route::post('mobile/changepassword', '\App\Http\Controllers\Api\mobile\UsersController@changepassword_mobile');
Route::post('mobile/userforgotPassword', '\App\Http\Controllers\Api\mobile\ContentController@userforgotpassword_mobile');
Route::post('mobile/resetPassword', '\App\Http\Controllers\Api\mobile\ContentController@resetPassword_mobile');

/* Get A Quote */
Route::post('mobile/getquote/fetchquote', '\App\Http\Controllers\Api\mobile\GetquoteController@fetchquote_mobile');

/* Get Item Quote */
Route::post('mobile/getquote/fetchitemquote', '\App\Http\Controllers\Api\mobile\GetquoteController@fetchitemquote_mobile');

/* Get Auto Quote */
Route::post('mobile/getquote/fetchautoquote', '\App\Http\Controllers\Api\mobile\GetquoteController@fetchautoquote_mobile');

/* Shop for me */
Route::post('mobile/getusercartlist', '\App\Http\Controllers\Api\mobile\UsersController@getusercartlist_mobile');

Route::post('mobile/savecartlist', '\App\Http\Controllers\Api\mobile\UsersController@savecartlist_mobile');

Route::post('mobile/getsaveforlaterlist', '\App\Http\Controllers\Api\mobile\UsersController@getsaveforlaterlist_mobile');

Route::post('mobile/updateusercart', '\App\Http\Controllers\Api\mobile\UsersController@updateusercart_mobile');

Route::post('mobile/movetousercart', '\App\Http\Controllers\Api\mobile\UsersController@movetousercart_mobile');

Route::post('mobile/removecartitem', '\App\Http\Controllers\Api\mobile\UsersController@removecartitem_mobile');

Route::post('mobile/calculateshippingcost', '\App\Http\Controllers\Api\mobile\ProcurementController@calculateshippingcost_mobile');

Route::post('mobile/submitprocurementdata', '\App\Http\Controllers\Api\mobile\ProcurementController@submitprocurementdata_mobile');

Route::post('mobile/updateusercartdata', '\App\Http\Controllers\Api\mobile\ProcurementController@updateusercartdata_mobile');

Route::post('mobile/clearusercart', '\App\Http\Controllers\Api\mobile\UsersController@clearusercart_mobile');

Route::post('mobile/getpaymentmethodlist', '\App\Http\Controllers\Api\mobile\DataController@getpaymentmethodlist_mobile');

Route::post('mobile/validateewallet', '\App\Http\Controllers\Api\mobile\UsersController@validateewallet_mobile');

Route::post('mobile/paymentgetway', '\App\Http\Controllers\Api\mobile\UsersController@paymentgetway_mobile');

Route::post('mobile/validateCouponCode', '\App\Http\Controllers\Api\mobile\ProcurementController@validatecouponcode_mobile');

Route::post('mobile/submitorder', '\App\Http\Controllers\Api\mobile\ProcurementController@submitorder_mobile');

Route::get('mobile/getlocationcountry', '\App\Http\Controllers\Api\mobile\DataController@getlocationcountrylist_mobile');

Route::post('mobile/getlocationdetails', '\App\Http\Controllers\Api\mobile\DataController@getlocationdetails_mobile');

Route::post('mobile/getProcurementShipmentDetails', '\App\Http\Controllers\Api\mobile\ProcurementController@getprocurementshipmentdetails_mobile');

/* auto parts */
Route::post('mobile/getautopartslist', '\App\Http\Controllers\Api\mobile\UsersController@getautopartslist_mobile');

Route::post('mobile/submitautopartsdata', '\App\Http\Controllers\Api\mobile\ProcurementController@submitautopartsdata_mobile');

Route::post('mobile/updateusercartautopartsdata', '\App\Http\Controllers\Api\mobile\ProcurementController@updateusercartautoparts_mobile');

Route::post('mobile/getProcurementShipmentForAutoPartsDetails', '\App\Http\Controllers\Api\mobile\ProcurementController@getprocurementshipmentforautopartsdetails_mobile');

Route::post('mobile/placeorder', '\App\Http\Controllers\Api\mobile\ProcurementController@placeorder_mobile');

/* Auto */
Route::get('mobile/getwebsiteist', '\App\Http\Controllers\Api\mobile\AutoController@getwebsiteist_mobile');

Route::post('mobile/getwebsitemake', '\App\Http\Controllers\Api\mobile\AutoController@getwebsitemake_mobile');

Route::post('mobile/getwebsitemodel', '\App\Http\Controllers\Api\mobile\AutoController@getwebsitemodel_mobile');

Route::post('mobile/getautoallcharges', '\App\Http\Controllers\Api\mobile\DataController@getautoallcharges_mobile');

Route::post('mobile/checkautocartdata', '\App\Http\Controllers\Api\mobile\UsersController@checkautocartdata_mobile');

Route::post('mobile/deleteautocartdata', '\App\Http\Controllers\Api\mobile\UsersController@deleteautocartdata_mobile');

Route::get('mobile/getlocationtype', '\App\Http\Controllers\Api\mobile\AutoController@getlocationtype_mobile');

Route::post('mobile/getshipmycarallcharges', '\App\Http\Controllers\Api\mobile\DataController@getshipmycarallcharges_mobile');

Route::post('mobile/getmakelist', '\App\Http\Controllers\Api\mobile\DataController@getmakelist_mobile');

Route::post('mobile/getmodellist', '\App\Http\Controllers\Api\mobile\DataController@getmodellist_mobile');

Route::post('mobile/updatecartcurrency', '\App\Http\Controllers\Api\mobile\UsersController@updatecartcurrency_mobile');

Route::post('mobile/updatecartcontent', '\App\Http\Controllers\Api\mobile\UsersController@updatecartcontent_mobile');

Route::post('mobile/autovalidatecoupon', '\App\Http\Controllers\Api\mobile\AutoController@validatecouponcode_mobile');

Route::post('mobile/autoinvoice', '\App\Http\Controllers\Api\mobile\AutoController@printinvoice_mobile');

Route::post('mobile/tempfileupload', '\App\Http\Controllers\Api\mobile\UsersController@uploadfiletem_mobile');

Route::post('mobile/removetempuploaddata', '\App\Http\Controllers\Api\mobile\UsersController@deletetempuoloaddata_mobile');

Route::post('mobile/savebuycardata', '\App\Http\Controllers\Api\mobile\AutoController@savebuycardata_mobile');

Route::post('mobile/saveshipmycar', '\App\Http\Controllers\Api\mobile\AutoController@saveshipmycar_mobile');


Route::post('mobile/forcelogin', '\App\Http\Controllers\Api\mobile\UsersController@forcelogin_mobile');

Route::get('mobile/getwarehouselist', '\App\Http\Controllers\Api\mobile\DataController@getwarehouselist_mobile');

Route::post('mobile/getstore', '\App\Http\Controllers\Api\mobile\DataController@getstorelist_mobile');

Route::post('mobile/getcategory', '\App\Http\Controllers\Api\mobile\DataController@getcategorylist_mobile');

Route::post('mobile/getsubcategory', '\App\Http\Controllers\Api\mobile\DataController@getsubcategorylist_mobile');

Route::post('mobile/getproduct', '\App\Http\Controllers\Api\mobile\DataController@getproductlist_mobile');

/* Car I have bought */
Route::post('mobile/getcarihaveboughtdetails', '\App\Http\Controllers\Api\mobile\AutoController@getcarihaveboughtdetails_mobile');

/* My Warehouse under Auto */
Route::post('mobile/getcarwarehousedetails', '\App\Http\Controllers\Api\mobile\AutoController@getcarwarehousedetails_mobile');
Route::get('mobile/getautowarehouseimage/{imageId}', '\App\Http\Controllers\Api\mobile\AutoController@getautowarehouseimage_mobile');

Route::post('mobile/usershippingaddress', '\App\Http\Controllers\Api\mobile\UsersController@usershippingaddress_mobile');

Route::get('mobile/getcontactinfo', '\App\Http\Controllers\Api\mobile\ContentController@getcontactinfo_mobile');

Route::get('mobile/gethomepageadbanner', '\App\Http\Controllers\Api\mobile\ContentController@gethomepageadbanner_mobile');

/* Get Menus */
Route::get('mobile/getheadernavigation', '\App\Http\Controllers\Api\mobile\ContentController@getheadernavigation_mobile');

Route::get('mobile/getfooternavigation', '\App\Http\Controllers\Api\mobile\ContentController@getfooternavigation_mobile');

Route::get('mobile/getsociallinks', '\App\Http\Controllers\Api\mobile\ContentController@getsociallinks_mobile');

/* get Reward details */
Route::post('mobile/getrewarddetails', '\App\Http\Controllers\Api\mobile\ContentController@getrewarddetails_mobile');

Route::get('mobile/getallwarehouse', '\App\Http\Controllers\Api\mobile\DataController@getallwarehouse_mobile');

/* My Warehouse */
Route::post('mobile/getusershipmentlist', '\App\Http\Controllers\Api\mobile\ShipmentController@getusershipmentlist_mobile');

Route::post('mobile/uploadshipmentinvoice', '\App\Http\Controllers\Api\mobile\ShipmentController@uploadshipmentinvoice_mobile');

Route::post('mobile/allshippingmethodlist', '\App\Http\Controllers\Api\mobile\ShipmentController@allshippingmethodlist_mobile');

Route::post('mobile/calculatedeliveryshipping', '\App\Http\Controllers\Api\mobile\ShipmentController@calculatedeliveryshipping_mobile');

Route::post('mobile/adddeliveryothercharges', '\App\Http\Controllers\Api\mobile\ShipmentController@adddeliveryothercharges_mobile');

Route::post('mobile/getshippimgmethod', '\App\Http\Controllers\Api\mobile\ShipmentController@getshippimgmethod_mobile');

Route::post('mobile/getpointtransction', '\App\Http\Controllers\Api\mobile\ContentController@getpointtransction_mobile');

Route::post('mobile/updateuserwarehouse', '\App\Http\Controllers\Api\mobile\ShipmentController@updateuserwarehouse_mobile');

Route::post('mobile/validateshipmentcoupon', '\App\Http\Controllers\Api\mobile\ShipmentController@validatecouponcode_mobile');

/* Shipment order history */
Route::post('mobile/shipmentorderhistory', '\App\Http\Controllers\Api\mobile\ShipmentController@shipmentorderhistory_mobile');

Route::get('mobile/getallshipmentstatus', '\App\Http\Controllers\Api\mobile\ShipmentController@getallshipmentstatus_mobile');

Route::post('mobile/getshipmentdetails', '\App\Http\Controllers\Api\mobile\ShipmentController@getshipmentdetails_mobile');

Route::post('mobile/printshipmentinvoice', '\App\Http\Controllers\Api\mobile\ShipmentController@printshipmentinvoice_mobile');

Route::post('mobile/getshipmentsnapshots', '\App\Http\Controllers\Api\mobile\ShipmentController@getshipmentsnapshots_mobile');

/* Check user login status (for angular front end) */
Route::post('checkuserloginstatus', '\App\Http\Controllers\Api\UsersController@checkuserloginstatus');

Route::post('mobile/checkuserloginstatus', '\App\Http\Controllers\Api\mobile\UsersController@checkuserloginstatus_mobile');

Route::post('mobile/warehousecheckout', '\App\Http\Controllers\Api\mobile\ShipmentController@warehousecheckout_mobile');

Route::post('mobile/submitshipmentorder', '\App\Http\Controllers\Api\mobile\ShipmentController@submitorder_mobile');

Route::post('mobile/getamountforpaystack', '\App\Http\Controllers\Api\mobile\DataController@getamountforpaystack_mobile');

Route::post('mobile/getpaystacksettings', '\App\Http\Controllers\Api\mobile\ContentController@getpaystacksettings_mobile');

/* Track Shipment - track with order number */
Route::post('mobile/shipmenttrackingdetails', '\App\Http\Controllers\Api\mobile\DataController@shipmenttrackingdetails_mobile');

Route::post('mobile/submitContact', '\App\Http\Controllers\Api\mobile\GetquoteController@submitContact_mobile');

/* Fill and Ship for mobile */
Route::post('mobile/findmybox', '\App\Http\Controllers\Api\mobile\FillshipController@findmybox_mobile');

Route::post('mobile/submitfillshipdata', '\App\Http\Controllers\Api\mobile\FillshipController@submitfillshipdata_mobile');

Route::post('mobile/updatefillshipdata', '\App\Http\Controllers\Api\mobile\FillshipController@updatefillshipdata_mobile');

Route::post('mobile/submitfillshiporder', '\App\Http\Controllers\Api\mobile\FillshipController@submitorder_mobile');

Route::post('mobile/validatefillshipcoupon', '\App\Http\Controllers\Api\mobile\FillshipController@validatecouponcode_mobile');

Route::post('mobile/getuserfillship', '\App\Http\Controllers\Api\mobile\FillshipController@getuserfillship_mobile');

Route::post('mobile/fillshipshipout', '\App\Http\Controllers\Api\mobile\FillshipController@fillshipshipout_mobile');

Route::post('mobile/payfillshipextracharge', '\App\Http\Controllers\Api\mobile\FillshipController@payfillshipextracharge_mobile');

Route::post('mobile/fillshipdeliverydetails', '\App\Http\Controllers\Api\mobile\FillshipController@fillshipdeliverydetails_mobile');

Route::post('mobile/getpaypalipnsettings', '\App\Http\Controllers\Api\mobile\ContentController@getpaypalipnsettings_mobile');

/* Send OTP for modify default shipping address */
Route::post('mobile/sendotpforshippingaddress', '\App\Http\Controllers\Api\mobile\UsersController@sendotpforshippingaddress_mobile');
Route::post('mobile/resendotpforshippingaddress', '\App\Http\Controllers\Api\mobile\UsersController@resendotpforshippingaddress_mobile');
Route::post('mobile/checkotpforshippingaddress', '\App\Http\Controllers\Api\mobile\UsersController@checkotpforshippingaddress_mobile');

/* payment */
Route::post('mobile/getpaymentmethodbyuser', '\App\Http\Controllers\Api\mobile\UsersController@getpaymentmethodlist_mobile');
Route::post('mobile/getpaymenthistory', '\App\Http\Controllers\Api\mobile\UsersController@getpaymenthistory_mobile');
Route::post('mobile/getinvoicelist', '\App\Http\Controllers\Api\mobile\UsersController@getinvoicelist_mobile');
Route::post('mobile/getpaymentinvoice', '\App\Http\Controllers\Api\mobile\UsersController@getpaymentinvoice_mobile');
Route::post('mobile/payinvoice', '\App\Http\Controllers\Api\mobile\UsersController@payinvoice_mobile');
Route::post('mobile/getinvoicedetail', '\App\Http\Controllers\Api\mobile\UsersController@getinvoice_mobile');

/* my documents details */
Route::post('mobile/getdocuments', '\App\Http\Controllers\Api\mobile\ContentController@getdocuments_mobile');
Route::post('mobile/deletedocument', '\App\Http\Controllers\Api\mobile\ContentController@deletedocument_mobile');
Route::post('mobile/uploaddocument', '\App\Http\Controllers\Api\mobile\ContentController@uploaddocument_mobile');

Route::post('mobile/sendwronginventorynotification', '\App\Http\Controllers\Api\mobile\ShipmentController@sendwronginventorynotification_mobile');
Route::post('mobile/updateinventory', '\App\Http\Controllers\Api\mobile\ShipmentController@updateinventory_mobile');

/* Claim File */
Route::post('mobile/postclaim', '\App\Http\Controllers\Api\mobile\ContentController@postclaim_mobile');
Route::post('mobile/postclaimimage', '\App\Http\Controllers\Api\mobile\ContentController@postclaimimage_mobile');
Route::post('mobile/postclaimsendmail', '\App\Http\Controllers\Api\mobile\ContentController@postclaimsendmail_mobile');
Route::post('mobile/getclaims', '\App\Http\Controllers\Api\mobile\ContentController@getclaims_mobile');

/* Messages */
Route::post('mobile/getmessages', '\App\Http\Controllers\Api\mobile\ContentController@getmessages_mobile');
Route::post('mobile/getmessagecontent', '\App\Http\Controllers\Api\mobile\ContentController@getmessagecontent_mobile');

Route::post('mobile/getinvoice', '\App\Http\Controllers\Api\mobile\ProcurementController@getinvoice_mobile');

Route::post('mobile/getprocurementItems', '\App\Http\Controllers\Api\mobile\ProcurementController@getprocurementItems_mobile');

Route::post('mobile/getautopartsinvoice', '\App\Http\Controllers\Api\mobile\ProcurementController@getautopartsinvoice_mobile');
Route::post('mobile/getautopartsItems', '\App\Http\Controllers\Api\mobile\ProcurementController@getautopartsItems_mobile');

Route::post('mobile/updateitemprice', '\App\Http\Controllers\Api\mobile\ShipmentController@updateitemprice_mobile');
Route::post('mobile/uploaddicountedinvoice', '\App\Http\Controllers\Api\mobile\ShipmentController@uploaddicountedinvoice_mobile');

Route::post('mobile/addpackagesnapshot', '\App\Http\Controllers\Api\mobile\ShipmentController@addpackagesnapshot_mobile');

Route::post('mobile/getquote/saveitemquotecontact', '\App\Http\Controllers\Api\mobile\GetquoteController@saveitemquotecontact_mobile');

Route::post('mobile/updateReturnData', '\App\Http\Controllers\Api\mobile\ShipmentController@updateReturnData_mobile');

Route::post('mobile/payreturn', '\App\Http\Controllers\Api\mobile\ShipmentController@payreturn_mobile');

Route::post('mobile/getDelieveryItems', '\App\Http\Controllers\Api\mobile\ShipmentController@getDelieveryItems_mobile');

Route::post('mobile/fillshipselectbox', '\App\Http\Controllers\Api\mobile\FillshipController@fillshipselectbox_mobile');

Route::get('mobile/fillshipalladdedboxes', '\App\Http\Controllers\Api\mobile\FillshipController@alladdedboxes_mobile');

/* Track Shipment - has item arrived */
Route::post('mobile/itemarriveddetails', '\App\Http\Controllers\Api\mobile\DataController@itemarriveddetails_mobile');
Route::post('mobile/verifyitemarrivedtime', '\App\Http\Controllers\Api\mobile\DataController@verifyitemarrivedtime_mobile');
Route::post('mobile/verifytrackingdetails', '\App\Http\Controllers\Api\mobile\DataController@verifytrackingdetails_mobile');
Route::post('mobile/verifytrackingdetailsfile', '\App\Http\Controllers\Api\mobile\DataController@verifytrackingdetailsfile_mobile');

Route::post('mobile/getamountforpayeezy', '\App\Http\Controllers\Api\mobile\DataController@getamountforpayeezy_mobile');
Route::post('mobile/registerDevice', '\App\Http\Controllers\Api\mobile\UsersController@registerDevice_mobile');
Route::post('mobile/getNotificationList', '\App\Http\Controllers\Api\mobile\UsersController@getNotificationList_mobile');
Route::post('mobile/markasread', '\App\Http\Controllers\Api\mobile\UsersController@markasread_mobile');

Route::post('mobile/virtualtourfinished', '\App\Http\Controllers\Api\mobile\UsersController@virtualtourfinished_mobile');
Route::post('mobile/saveshipmentsettings', '\App\Http\Controllers\Api\mobile\UsersController@saveshipmentsettings_mobile');
Route::post('mobile/saveSignature', '\App\Http\Controllers\Api\mobile\UsersController@saveSignature_mobile');

/*Subscription API */

Route::get('mobile/getsubscriptiondetails', '\App\Http\Controllers\Api\mobile\UsersController@getsubscriptiondetails_mobile');
Route::get('mobile/subscriptionpayment', '\App\Http\Controllers\Api\mobile\UsersController@subscriptionpayment_mobile');
Route::get('mobile/getusersubscriptiondetail', '\App\Http\Controllers\Api\mobile\UsersController@getusersubscriptiondetail_mobile');
