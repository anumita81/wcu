<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
/* Create Image Url */
Route::get('/profileimage/{filename}', function ($filename) {
    $path = public_path() . '/uploads/profile/thumbnail/' . $filename;
    if (!File::exists($path))
        abort(404);
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
})->name('profileimage');
Route::get('/blogimages/{filename}', function ($filename) {
    $path = public_path() . '/uploads/blog/thumbnail/' . $filename;
    if (!File::exists($path))
        abort(404);
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
})->name('blogimages');

Route::get('testemail', 'Mailcontroller@testemail');

//, 'middleware' => ['validuser']
Route::get('/', 'HomeController@index');
Route::get('paystackcb', 'PaystackcallbackController@paystackcallback');
Route::post('paystackcb', 'PaystackcallbackController@paystackcallback');

Route::get('payeezytrans', 'PaystackcallbackController@payeezytrans');
Route::post('payeezytrans', 'PaystackcallbackController@payeezytrans');

Route::get('admin', array('before' => 'auth', 'uses' => 'Administrator\IndexController@index'));
Route::group(['prefix' => 'administrator'], function() {

    /* Authentication */
    Route::get('login', 'Administrator\IndexController@index')->name('login');
    Route::post('login', 'Auth\LoginController@doLogin');
    Route::get('logout', 'Auth\LoginController@logout');


    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.request');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.reset');

    /* DASHBOARD */
    Route::get('dashboard', 'Administrator\DashboardController@getIndex');
    Route::get('dashboard/index', 'Administrator\DashboardController@getIndex');
    Route::post('dashboard/searchbyselection/{searchKey?}/{searchType?}', 'Administrator\DashboardController@searchbyselection');
    

    /* Content */

    /* My Profile */
    Route::resource('myProfile', 'Administrator\MyProfileController');

    /* Admin user Management */
    Route::get('adminuser', 'Administrator\AdminuserController@index');
    Route::get('adminuser/index', 'Administrator\AdminuserController@index');
    Route::post('adminuser/index', 'Administrator\AdminuserController@index');
    Route::get('adminuser/changestatus/{id?}/{page?}/{status?}', 'Administrator\AdminuserController@changestatus');
    Route::get('adminuser/addedit/{id?}/{page?}', 'Administrator\AdminuserController@addedit');
    Route::post('adminuser/save/{id?}/{page?}', 'Administrator\AdminuserController@savedata');
    Route::post('adminuser/add/{page?}', 'Administrator\AdminuserController@adddata');
    Route::get('adminuser/changepassword/{id?}/{page?}', 'Administrator\AdminuserController@changepassword');
    Route::post('adminuser/changepassword/{id?}/{page?}', 'Administrator\AdminuserController@changepassword');
    Route::get('adminuser/notify/{id?}/{page?}', 'Administrator\AdminuserController@notify');
    Route::post('adminuser/notify/{id?}/{page?}', 'Administrator\AdminuserController@notify');
    Route::post('adminuser/exportall/{page?}', 'Administrator\AdminuserController@exportall');
    Route::post('adminuser/exportselected/{page?}', 'Administrator\AdminuserController@exportSelected');
    Route::get('adminuser/editrole/{id?}', 'Administrator\AdminuserController@editRole');
    Route::post('adminuser/editrole/{id?}', 'Administrator\AdminuserController@saveRole');
    Route::get('adminuser/showall', 'Administrator\AdminuserController@showall');
    Route::post('adminuser/inheritperm/{page?}', 'Administrator\AdminuserController@inheritPermission');


    /* Notification History */
    Route::get('notificationhistory/{id?}', 'Administrator\NotificationhistoryController@index');
    Route::get('notificationhistory/index/{id?}', 'Administrator\NotificationhistoryController@index');
    Route::post('notificationhistory/index/{id?}', 'Administrator\NotificationhistoryController@index');

    

    /* admin User Roles */
    Route::get('adminrole', 'Administrator\AdminroleController@index');
    Route::get('adminrole/index', 'Administrator\AdminroleController@index');
    Route::post('adminrole/index', 'Administrator\AdminroleController@index');
    Route::get('adminrole/changestatus/{id?}/{status?}', 'Administrator\AdminroleController@changestatus');

    /* Permissions for admin user (roles) */
    Route::resource('permission', 'Administrator\PermissionController');

    /* Authentication required */
    Route::resource('authentication', 'Administrator\AuthenticationrequiredController');

    /* Test Controller */
    Route::resource('test', 'Administrator\TestController');

    

    /* Block Content */
    Route::get('blockcontent', 'Administrator\BlockcontentController@index');
    Route::post('blockcontent', 'Administrator\BlockcontentController@index');
    Route::get('blockcontentedit/{id?}/{slug?}', 'Administrator\BlockcontentController@geteditcontent');
    Route::post('blockcontentupdate/{id?}', 'Administrator\BlockcontentController@updtecontent');
    Route::get('editblockcontentstatus/{id?}/{status?}', 'Administrator\BlockcontentController@editstatus');
    Route::get('blockcontent/add', 'Administrator\BlockcontentController@showaddform')->name('addblockcontent');
    Route::post('blockcontent/save', 'Administrator\BlockcontentController@addData')->name('saveblockcontent');
    Route::get('blockcontent/delete/{id?}', 'Administrator\BlockcontentController@deleteData');

    
    /* Site static & landing pages */
    Route::get('pagelist/{pagetype?}', 'Administrator\SitepageController@staticpage')->name('pagelist');
    Route::post('pagelist/{pagetype?}', 'Administrator\SitepageController@staticpage')->name('pagelist');
    Route::get('addeditpage/{id?}/{pagetype?}/{page?}', 'Administrator\SitepageController@addeditpage')->name('addeditpage');
    Route::post('addeditpagerecord/{id?}/{pagetype?}/{page?}', 'Administrator\SitepageController@addeditpagerecord');
    Route::get('editpagestatus/{id?}/{status?}', 'Administrator\SitepageController@editstatus');
    Route::get('deletepage/{id?}/{pagetype?}/{page?}', 'Administrator\SitepageController@deleterecord')->name('deletepage');

    


    /* Country */
    Route::get('country', 'Administrator\CountryController@index');
    Route::get('country/index', 'Administrator\CountryController@index');
    Route::post('country/index', 'Administrator\CountryController@index');
    Route::get('country/changestatus/{id?}/{page?}/{status?}', 'Administrator\CountryController@changestatus');
    Route::get('country/addedit/{id?}/{page?}', 'Administrator\CountryController@addedit');
    Route::post('country/save/{id?}/{page?}', 'Administrator\CountryController@savedata');
    Route::get('country/getcountryisocode/{countryid?}', 'Administrator\CountryController@getcountryisocode');


    /* State */
    Route::get('state', 'Administrator\StateController@index');
    Route::get('state/index', 'Administrator\StateController@index');
    Route::post('state/index', 'Administrator\StateController@index');
    Route::get('state/changestatus/{id?}/{page?}/{status?}', 'Administrator\StateController@changestatus');
    Route::get('state/addedit/{id?}/{page?}', 'Administrator\StateController@addedit');
    Route::post('state/save/{id?}/{page?}', 'Administrator\StateController@savedata');
    Route::get('state/delete/{id?}/{page?}', 'Administrator\StateController@delete');

    /* City */
    Route::get('city', 'Administrator\CityController@index');
    Route::get('city/index', 'Administrator\CityController@index');
    Route::post('city/index', 'Administrator\CityController@index');
    Route::get('city/changestatus/{id?}/{page?}/{status?}', 'Administrator\CityController@changestatus');
    Route::get('city/addedit/{id?}/{page?}', 'Administrator\CityController@addedit');
    Route::post('city/save/{id?}/{page?}', 'Administrator\CityController@savedata');
    Route::get('city/delete/{id?}/{page?}', 'Administrator\CityController@delete');
    Route::get('city/getstatelist/{countrycode?}', 'Administrator\CityController@getstatelist');
    Route::get('city/importdata', 'Administrator\CityController@importdata');
    Route::post('city/saveimport', 'Administrator\CityController@saveimport');

    

   

    /* Location */
    Route::get('location', 'Administrator\LocationController@index');
    Route::get('location/index', 'Administrator\LocationController@index');
    Route::post('location/index', 'Administrator\LocationController@index');
    Route::get('location/changestatus/{id?}/{page?}/{status?}', 'Administrator\LocationController@changestatus');
    Route::get('location/addedit/{id?}/{page?}', 'Administrator\LocationController@addedit');
    Route::post('location/save/{id?}/{page?}', 'Administrator\LocationController@savedata');
    Route::get('location/delete/{id?}/{page?}', 'Administrator\LocationController@delete');
    Route::get('location/getstatelist/{countryid?}', 'Administrator\LocationController@getstatelist');
    Route::get('location/getcitylist/{stateid?}', 'Administrator\LocationController@getcitylist');
    Route::get('location/getcitylistbycountry/{countryid?}', 'Administrator\LocationController@getcitylistbycountry');

    


    /* Email Template */
    Route::get('emailtemplate', 'Administrator\EmailtemplateController@index');
    Route::get('emailtemplate/index', 'Administrator\EmailtemplateController@index');
    Route::post('emailtemplate/index', 'Administrator\EmailtemplateController@index');
    Route::get('emailtemplate/addedit/{id?}/{page?}', 'Administrator\EmailtemplateController@addedit');
    Route::post('emailtemplate/save/{id?}/{page?}', 'Administrator\EmailtemplateController@savedata');
    Route::get('emailtemplate/clear', 'Administrator\EmailtemplateController@cleardata');
    Route::post('emailtemplate/generatekeyoptions', 'Administrator\EmailtemplateController@generateKeyOptions');

    /* Center */
    Route::get('center', 'Administrator\CenterController@index');
    Route::get('center/index', 'Administrator\CenterController@index');
    Route::post('center/index', 'Administrator\CenterController@index');
    Route::get('center/addedit/{id?}/{page?}', 'Administrator\CenterController@addedit');
    Route::post('center/save/{id?}/{page?}', 'Administrator\CenterController@savedata');
    Route::get('center/delete/{id?}/{page?}', 'Administrator\CenterController@delete');
    Route::get('center/showall', 'Administrator\CenterController@showall');
    Route::get('center/getstatelist/{countryid?}', 'Administrator\CenterController@getstatelist');
    Route::get('center/getcitylist/{stateid?}', 'Administrator\CenterController@getcitylist');
    Route::get('center/getcitylistbycountry/{countryid?}', 'Administrator\CenterController@getcitylistbycountry');
    Route::get('center/addressdelete/{userid?}/{id?}/{page?}', 'Administrator\CenterController@deleteaddressbook');
    Route::post('center/saveaddressbook/{id?}/{page?}', 'Administrator\CenterController@saveaddressbook');
    Route::get('center/addressaddedit/{userid?}/{id?}/{page?}', 'Administrator\CenterController@addressaddedit');
    Route::post('center/saveaddress/{userid?}/{id?}/{page?}', 'Administrator\CenterController@saveaddressdata');
    Route::post('center/exportall/{page?}', 'Administrator\CenterController@exportall');
    Route::post('center/exportselected/{page?}', 'Administrator\CenterController@exportselected');
    Route::get('center/changepassword/{id?}/{page?}', 'Administrator\CenterController@changepassword');
    Route::post('center/changepassword/{id?}/{page?}', 'Administrator\CenterController@changepassword');

    /* Admin profile */
    Route::get('/profile', 'Administrator\ProfileController@getProfileView');

    /* General Settings */

    Route::get('/generalsettings', 'Administrator\SettingsController@index');
    Route::get('generalsettings/index', 'Administrator\SettingsController@index');
    Route::post('generalsettings/save', 'Administrator\SettingsController@savedata');

    /* Social Media Settings */

    Route::get('/socialmedia', 'Administrator\SocialMediaController@index');
    Route::get('socialmedia/index', 'Administrator\SocialMediaController@index');
    Route::post('socialmedia/save', 'Administrator\SocialMediaController@savedata');


    /* SMS Template */
    Route::get('smstemplate', 'Administrator\SmstemplateController@index');
    Route::get('smstemplate/index', 'Administrator\SmstemplateController@index');
    Route::post('smstemplate/index', 'Administrator\SmstemplateController@index');
    Route::get('smstemplate/addedit/{id?}/{page?}', 'Administrator\SmstemplateController@addedit');
    Route::post('smstemplate/save/{id?}/{page?}', 'Administrator\SmstemplateController@savedata');
   

    /* Menu */
    Route::get('menu', 'Administrator\MenuController@index');
    Route::get('menu/index', 'Administrator\MenuController@index');
    Route::post('menu/index', 'Administrator\MenuController@index');
    Route::get('menu/changestatus/{id?}/{page?}/{status?}', 'Administrator\MenuController@changestatus');
    Route::get('menu/addedit/{id?}/{page?}', 'Administrator\MenuController@addedit');
    Route::post('menu/save/{id?}/{page?}', 'Administrator\MenuController@savedata');
    Route::get('menu/delete/{id?}/{page?}', 'Administrator\MenuController@delete');

    

    /* Cron */
    Route::get('cron/sendnotification', 'Administrator\Cron@sendnotification');

   
    /* Get Country Phone List */
    Route::post('generalsettings/getphone', 'Administrator\SettingsController@getphonedata');

    /* Remove Contact from General Settings */
    Route::get('generalsettings/deletecontact/{val?}', 'Administrator\SettingsController@deletecontact');


    /* Newsletter Management */
    Route::get('newsletter', 'Administrator\NewsletterController@index');
    Route::get('newsletter/index', 'Administrator\NewsletterController@index');
    Route::post('newsletter/index', 'Administrator\NewsletterController@index');
    Route::get('newsletter/addedit/{id?}/{page?}', 'Administrator\NewsletterController@addedit');
    Route::post('newsletter/save/{id?}/{page?}', 'Administrator\NewsletterController@savedata');
    Route::get('newsletter/sendmessage/{id?}/{page?}', 'Administrator\NewsletterController@sendmessage');
    Route::post('newsletter/savesubscriber/{id?}/{page?}', 'Administrator\NewsletterController@savesubscriber');


    /* FAQ */
    Route::get('faq', 'Administrator\FaqController@index');
    Route::get('faq/index', 'Administrator\FaqController@index');
    Route::post('faq/index', 'Administrator\FaqController@index');
    Route::get('faq/addedit/{id?}/{page?}', 'Administrator\FaqController@addedit');
    Route::post('faq/savedata/{id?}/{page?}', 'Administrator\FaqController@savedata');
    Route::get('faq/changestatus/{id?}/{page?}/{status?}', 'Administrator\FaqController@changestatus');
    Route::get('faq/delete/{id?}/{page?}', 'Administrator\FaqController@delete');

    /* SUBJECT */
    Route::get('subject', 'Administrator\SubjectController@index');
    Route::get('subject/index', 'Administrator\SubjectController@index');
    Route::post('subject/index', 'Administrator\SubjectController@index');
    Route::get('subject/addedit/{id?}/{page?}', 'Administrator\SubjectController@addedit');
    Route::post('subject/save/{id?}/{page?}', 'Administrator\SubjectController@savedata');
    Route::get('subject/changestatus/{id?}/{page?}/{status?}', 'Administrator\SubjectController@changestatus');
    Route::get('subject/delete/{id?}/{page?}', 'Administrator\SubjectController@delete');

    /*Student*/
    Route::get('student', 'Administrator\StudentController@index');
    Route::get('student/index', 'Administrator\StudentController@index');
    Route::post('student/index', 'Administrator\StudentController@index');
    Route::get('student/addedit/{id?}/{page?}', 'Administrator\StudentController@addedit');
    Route::post('student/save/{id?}/{page?}', 'Administrator\StudentController@savedata');
    Route::post('student/changestatus/{id?}/{page?}/{status?}', 'Administrator\StudentController@changestatus');
    Route::get('student/delete/{id?}/{page?}', 'Administrator\StudentController@delete');
    Route::get('student/showall', 'Administrator\StudentController@showall');
    Route::get('student/getstatelist/{countryid?}', 'Administrator\StudentController@getstatelist');
    Route::get('student/getcitylist/{stateid?}', 'Administrator\StudentController@getcitylist');
    Route::post('student/exportall/{page?}', 'Administrator\StudentController@exportall');
    Route::post('student/exportselected/{page?}', 'Administrator\StudentController@exportselected');
    Route::get('student/changepassword/{id?}/{page?}', 'Administrator\StudentController@changepassword');
    Route::post('student/changepassword/{id?}/{page?}', 'Administrator\StudentController@changepassword');

       /* Class */
    Route::get('studentclass', 'Administrator\ClassController@index');
    Route::get('studentclass/index', 'Administrator\ClassController@index');
    Route::post('studentclass/index', 'Administrator\ClassController@index');
    Route::get('studentclass/addedit/{id?}/{page?}', 'Administrator\ClassController@addedit');
    Route::post('studentclass/save/{id?}/{page?}', 'Administrator\ClassController@savedata');
    Route::get('studentclass/changestatus/{id?}/{page?}/{status?}', 'Administrator\ClassController@changestatus');
    Route::get('studentclass/delete/{id?}/{page?}', 'Administrator\ClassController@delete');


});



//Auth::routes();post

//Route::get('/home', 'HomeController@index')->name('home');
