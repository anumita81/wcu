<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Warehouselocation;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class WarehouselocationController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function rows(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.WarehouselocationRows'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('SHIPMENTROWDATA');
            \Session::push('SHIPMENTROWDATA.searchDisplay', $searchDisplay);
            \Session::push('SHIPMENTROWDATA.field', $field);
            \Session::push('SHIPMENTROWDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('SHIPMENTROWDATA.field');
            $sortType = \Session::get('SHIPMENTROWDATA.type');
            $searchDisplay = \Session::get('SHIPMENTROWDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH ROW LIST  */
        $shipmentLocationData = Warehouselocation::getRowList($param);
        $inUseLocations = Warehouselocation::getInUseDataRow();

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Shipment Locations - Rows";
        $data['contentTop'] = array('breadcrumbText' => 'Shipment Locations - Rows', 'contentTitle' => 'Shipment Locations - Rows', 'pageInfo' => 'This section allows you to manage warehouse location rows');
        $data['pageTitle'] = "Shipment Locations - Rows";
        $data['page'] = $shipmentLocationData->currentPage();
        $data['shipmentLocationData'] = $shipmentLocationData;
        $data['inUseLocations'] = $inUseLocations;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.warehouselocation.rows', $data);
    }

    public function zones(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.WarehouselocationZones'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchByRow = \Input::get('searchByRow', $this->_perPage);
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('SHIPMENTZONEDATA');
            \Session::push('SHIPMENTZONEDATA.searchByRow', $searchByRow);
            \Session::push('SHIPMENTZONEDATA.searchDisplay', $searchDisplay);
            \Session::push('SHIPMENTZONEDATA.field', $field);
            \Session::push('SHIPMENTZONEDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByRow'] = $searchByRow;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('SHIPMENTZONEDATA.field');
            $sortType = \Session::get('SHIPMENTZONEDATA.type');
            $searchByRow = \Session::get('SHIPMENTZONEDATA.searchByRow');
            $searchDisplay = \Session::get('SHIPMENTZONEDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByRow'] = !empty($searchByRow) ? $searchByRow[0] : '';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'rowName' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH ZONE LIST  */
        $shipmentLocationData = Warehouselocation::getZoneList($param);
        $inUseLocations = Warehouselocation::getInUseDataZone();
        $data['rowList'] = Warehouselocation::where('status', '1')->where('type', 'R')->orderby('name', 'asc')->get();

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Shipment Locations - Zones";
        $data['contentTop'] = array('breadcrumbText' => 'Shipment Locations - Zones', 'contentTitle' => 'Shipment Locations - Zones', 'pageInfo' => 'This section allows you to manage warehouse location zones');
        $data['pageTitle'] = "Shipment Locations - Zones";
        $data['page'] = $shipmentLocationData->currentPage();
        $data['shipmentLocationData'] = $shipmentLocationData;
        $data['inUseLocations'] = $inUseLocations;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.warehouselocation.zones', $data);
    }

    /**
     * Method for add edit rows
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function rowsaddedit($id = '0', $page = '') {
        $data = array();
        $data['page'] = !empty($page) ? $page : '1';
        if (!empty($id)) {
            $data['id'] = $id;
            $data['pageTitle'] = 'Edit Row';
            $warehouselocation = Warehouselocation::find($id);
            $data['warehouselocation'] = $warehouselocation;
        } else {
            $data['id'] = 0;
            $data['pageTitle'] = 'Add New Row';
        }
        return view('Administrator.warehouselocation.rowsaddedit', $data);
    }

    /**
     * Method for add edit rows
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function zonesaddedit($id = '0', $page = '') {
        $data = array();
        $data['rowList'] = Warehouselocation::where('status', '1')->where('type', 'R')->orderby('name', 'asc')->get();
        $data['page'] = !empty($page) ? $page : '1';
        if (!empty($id)) {
            $data['id'] = $id;
            $data['pageTitle'] = 'Edit Row';
            $warehouselocation = Warehouselocation::find($id);
            $data['warehouselocation'] = $warehouselocation;
        } else {
            $data['id'] = 0;
            $data['pageTitle'] = 'Add New Row';
        }
        return view('Administrator.warehouselocation.zonesaddedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $id
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $warehouselocation = new Warehouselocation;

        if ($request->type == 'Z') {
            $validator = Validator::make($request->all(), [
                        'name' => 'required|unique:' . $warehouselocation->table . ',name,NULL,parentId,id,' . $id,
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                        'name' => 'required|unique:' . $warehouselocation->table . ',name,' . $id,
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $warehouselocation = Warehouselocation::find($id);
                $warehouselocation->modifiedBy = Auth::user()->id;
                $warehouselocation->modifiedOn = Config::get('constants.CURRENTDATE');
            } else {
                $warehouselocation->type = $request->type;
                $warehouselocation->createdBy = Auth::user()->id;
                $warehouselocation->createdOn = Config::get('constants.CURRENTDATE');
            }
            $warehouselocation->name = $request->name;

            if ($warehouselocation->type == 'Z')
                $warehouselocation->parentId = $request->parentId;

            $warehouselocation->save();
            $warehouselocationId = $warehouselocation->id;

            if ($request->type == 'R')
                return redirect('/administrator/warehouselocation/rows?page=' . $page)->with('successMessage', 'Shipment row location information saved successfuly.');
            else
                return redirect('/administrator/warehouselocation/zones?page=' . $page)->with('successMessage', 'Shipment zone location information saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Warehouselocation::changeStatus($id, $createrModifierId, $status)) {
                $warehouselocation = Warehouselocation::find($id);
                if ($warehouselocation->type == 'R')
                    return \Redirect::to('administrator/warehouselocation/rows?page=' . $page)->with('successMessage', 'Shipment row location status changed successfully.');
                else
                    return redirect('/administrator/warehouselocation/zones?page=' . $page)->with('successMessage', 'Shipment zone location status changed successfuly.');
            } else {
                return redirect()->back()->with('errorMessage', 'Error in operation!');
            }
        } else {
            return redirect()->back()->with('errorMessage', 'Error in operation!');
        }
    }

    public function deletelocation($id, $pageSource, $page) {
        if(!empty($id)) {
            $warehouselocation = Warehouselocation::find($id);
            $warehouselocation->delete();
            if ($pageSource == 'row')
                return \Redirect::to('administrator/warehouselocation/rows?page=' . $page)->with('successMessage', 'Shipment row deleted successfully.');
            else
                return \Redirect::to('/administrator/warehouselocation/zones?page=' . $page)->with('successMessage', 'Shipment zone deleted successfuly.');
        }
    }

    /**
     * Method used to show import warehouse location rows modal
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function importdata() {
        $data = array();
        $data['title'] = "Administrative Panel :: Shipment Locations - Rows";
        $data['contentTop'] = array('breadcrumbText' => 'Shipment Locations - Rows', 'contentTitle' => 'Shipment Locations - Rows', 'pageInfo' => 'This section allows you to manage warehouse location rows');

        return view('Administrator.warehouselocation.import', $data);
    }


    /**
     * Method used to show import warehouse location zones modal
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function importzonedata() {
        $data = array();
        $data['title'] = "Administrative Panel :: Shipment Locations - Zones";
        $data['contentTop'] = array('breadcrumbText' => 'Shipment Locations - Zones', 'contentTitle' => 'Shipment Locations - Zones', 'pageInfo' => 'This section allows you to manage warehouse location zones');
        return view('Administrator.warehouselocation.importzone', $data);
    }

    /**
     * Method used to import warehouse location rows functionality
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function saveimport(Request $request) {
        $data = $insert = array();
        

        $validator = Validator::make($request->all(), [
                    'uploadFile' => 'required|mimes:csv,xlsx,xls,ods',
        ]);

        if ($request->hasFile('uploadFile')) {

            $file = $request->file('uploadFile');

            //$path = $request->file->getRealPath();
            $path = $file->getRealPath();


            \Excel::load($path, function($reader) {
                foreach ($reader->toArray() as $rows) {
                //print_r($rows);  die; Array ( [countryCode] => YY [codeA3] => AND [codeN3] => 356 [region] => AS [countryName] => India [stateCode] => WB [stateName] => WestBengal [cityName] => Kolkata )                
                    //foreach ($rows as $row) {
                       
                        $this->warelocationrows[] = ['name' => $rows['Name']];
                }
            });
           #print_r($this->warelocationrows); die;
            if (count($this->warelocationrows) > 0) {

                foreach($this->warelocationrows as $val) {
                    //dd($val['name']);
                    #if (!empty($this->warelocationrows[0])) {
                        $Warehouselocation = new Warehouselocation;
                        $Warehouselocation->name = $val['name'];
                        $Warehouselocation->type = 'R';
                        $Warehouselocation->parentId = 0;
                        $Warehouselocation->status = 1;
                        $Warehouselocation->createdOn = Config::get('constants.CURRENTDATE');
                        $Warehouselocation->createdBy = Auth::user()->id;
                        $Warehouselocation->save();
                    #}
                } 
            } else {
                return redirect('/administrator/warehouselocation/rows')->with('errorMessage', 'Error while inserting the data');
            }

        } else {
            return redirect('/administrator/warehouselocation/rows')->with('errorMessage', 'Please upload a valid xls/csv file..!!');
        }
        return redirect('/administrator/warehouselocation/rows')->with('successMessage', 'Data imported successfully');
    }



    /**
     * Method used to import warehouse location zones functionality
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function saveimportzone(Request $request) {
        $data = $insert = array();
        

        $validator = Validator::make($request->all(), [
                    'uploadFile' => 'required|mimes:csv,xlsx,xls,ods',
        ]);

        if ($request->hasFile('uploadFile')) {

            $file = $request->file('uploadFile');

            //$path = $request->file->getRealPath();
            $path = $file->getRealPath();
            $stringFailure = '<ul class="remainList">';

            \Excel::load($path, function($reader) {
                foreach ($reader->toArray() as $rows) {
                    $this->warelocationzones[] = ['name' => $rows['Zones'], 'rows' => $rows['Rows']];
                }
            });
           #print_r($this->warelocationrows); die;
            if (count($this->warelocationzones) > 0) {

                foreach($this->warelocationzones as $val) {
                    //dd($val['name']);
                    #if (!empty($this->warelocationrows[0])) {
                        $Warehouselocation = new Warehouselocation;
                        $SelRowParentId = Warehouselocation::select('id', 'name')->where('name', $val['rows'])->first();
                        if(count($SelRowParentId) > 0) {
                        $Warehouselocation->name = $val['name'];
                        $Warehouselocation->type = 'Z';
                        $Warehouselocation->parentId = $SelRowParentId['id'];
                        $Warehouselocation->status = 1;
                        $Warehouselocation->createdOn = Config::get('constants.CURRENTDATE');
                        $Warehouselocation->createdBy = Auth::user()->id;
                        $Warehouselocation->save();
                        } else {
                            $stringFailure .= "<li>" . $val['name'] ." with Rows ". $val['rows'] . "</li>";
                        }
                    #}
                        
                } $stringFailure .= '</ul>'; 
            } else {
                return redirect('/administrator/warehouselocation/zones')->with('errorMessage', 'Error while inserting the data');
            }

        } else {
            return redirect('/administrator/warehouselocation/zones')->with('errorMessage', 'Please upload a valid xls/csv file..!!');
        }
        if($stringFailure == '<ul class="remainList"></ul>'){
            $messageData = 'Data imported successfully';
        } else {
            $messageData = "Some data has been imported however, the following data has not been imported as no corresponding Rows found!" . rtrim($stringFailure);
        }
        return redirect('/administrator/warehouselocation/zones')->with('successMessage', $messageData);
    }



}
