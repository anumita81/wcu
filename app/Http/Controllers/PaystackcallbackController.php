<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Illuminate\Routing\Router;

class PaystackcallbackController extends Controller {
    
    public function paystackcallback(Request $request) {

        $apiBaseUrl = "http://stmd.indusnet.cloud/shoptomydoordev/public/api/";
        // Retrieve the request's body
        $body = @file_get_contents("php://input");
        //echo 'test';echo $body;exit;
        //$body = $request->all();
        $postData = new \App\Model\Usercart;
        $postData->userId = "0000";
        $postData->warehouseId = "0000";
        $postData->cartContent = $body;
        $postData->createdOn = date('Y-m-d H:i:s');
        $postData->save();
        //print_r($_SERVER);
        
//        $signature = (isset($_SERVER['HTTP_X_PAYSTACK_SIGNATURE']) ? $_SERVER['HTTP_X_PAYSTACK_SIGNATURE'] : '');
//
//        /* It is a good idea to log all events received. Add code *
//         * here to log the signature and body to db or file       */
//
//        if (!$signature) {
//            // only a post with paystack signature header gets our attention
//            exit();
//        }
//
//        define('PAYSTACK_SECRET_KEY', 'sk_test_9d2b7788b8810d03d8c7a7315b7107f609c4ab40');
//// confirm the event's signature
//        if ($signature !== hash_hmac('sha512', $body, PAYSTACK_SECRET_KEY)) {
//            // silently forget this ever happened
//            exit();
//        }

        http_response_code(200);
// parse event (which is json string) as object
// Give value to your customer but don't give any output
// Remember that this is a call from Paystack's servers and 
// Your customer is not seeing the response here at all
        $event = json_decode($body,TRUE);
        
        if($event['event'] == 'charge.success')
        {
            if($event['data']['status'] == 'success')
            {
                $findRecord = \App\Model\Paystackreferencehistory::where("referenceNo",$event['data']['reference'])->where("processed","0")->orderBy("id","desc")->first();
                if(!empty($findRecord))
                {
                    $paymentTransacionData = \App\Model\Paymenttransaction::where("transactionData","like",'%"reference":"'.$event['data']['reference'].'"%')->first();
                    if(empty($paymentTransacionData) || (!empty($paymentTransacionData) && $paymentTransacionData->status=='unpaid'))
                    {
                        \App\Model\Paystackreferencehistory::where("id",$findRecord->id)->update(["processed"=>"1"]);
                        $apiCallData = json_decode($findRecord->postData,TRUE);
                        $apiCallData['paystackData'] = $event['data'];
                        $apiCallData['paystackCreatedReference'] = $event['data']['reference'];
                        
                        if($findRecord->paidType == 'fill-and-ship')
                        {
                            $apicallbackUrl = $apiBaseUrl.'submitfillshiporder/'.$findRecord->userId;
                        }
                        else if($findRecord->paidType == 'my-warehouse')
                        {
                            $apicallbackUrl = $apiBaseUrl."submitshipmentorder/".$findRecord->userId;
                        }
                        else if($findRecord->paidType == 'my-warehouse-payment')
                        {
                            $apicallbackUrl = $apiBaseUrl."payreturn";
                        }
                        else if($findRecord->paidType == 'shop-for-me') 
                        {
                            $apicallbackUrl = $apiBaseUrl."submitorder/".$findRecord->userId;
                        }
                        else if($findRecord->paidType == 'auto-parts')
                        {
                            $apicallbackUrl = $apiBaseUrl."placeorder/".$findRecord->userId;
                        }
                        else if($findRecord->paidType == 'buy_a_car')
                        {
                            $apicallbackUrl = $apiBaseUrl.'savebuycardata/'.$findRecord->userId.'/'.$apiCallData['warehouseId'];
                        }
                        else if($findRecord->paidType == 'ship_my_car')
                        {
                            $apiCallData['userId'] = $findRecord->userId;
                            $apicallbackUrl = $apiBaseUrl.'saveshipmycar';
                        }
                        else if($findRecord->paidType == 'payment')
                        {
                            $apicallbackUrl = $apiBaseUrl.'payinvoice';
                        }
                        
                        //if($findRecord->paidFor == 5099)
                        if($findRecord->paidType == 'my-warehouse' && empty($apiCallData['personaldetails']['shippingCountryId']))
                        {
                            $shippingInfo = \App\Model\Shipment::find($findRecord->paidFor);
                            $billingInfo = \App\Model\Addressbook::where("userId",$shippingInfo->userId)->where("isDefaultBilling","1")->first();
                            $apiCallData['personaldetails']['shippingCountryId'] = $shippingInfo->toCountry;
                            $apiCallData['personaldetails']['shippingStateId'] = $shippingInfo->toState;
                            $apiCallData['personaldetails']['shippingCityId'] = $shippingInfo->toCity;
                            $apiCallData['personaldetails']['shippingAddress'] = $shippingInfo->toAddress;
                            $apiCallData['personaldetails']['shippingFirstName'] = $apiCallData['personaldetails']['firstName'];
                            $apiCallData['personaldetails']['shippingLastName'] = $apiCallData['personaldetails']['lastName'];
                            $apiCallData['personaldetails']['shippingEmail'] = $shippingInfo->toEmail;
                            $apiCallData['personaldetails']['shippingPhone'] = $shippingInfo->toPhone;
                            $apiCallData['personaldetails']['billingFirstName'] = $billingInfo->firstName;
                            $apiCallData['personaldetails']['billingLastName'] = $billingInfo->lastName;
                            $apiCallData['personaldetails']['billingEmail'] = $billingInfo->email;
                            $apiCallData['personaldetails']['billingAddress'] = $billingInfo->address;
                            $apiCallData['personaldetails']['billingCityId'] = $billingInfo->cityId;
                            $apiCallData['personaldetails']['billingStateId'] = $billingInfo->stateId;
                            $apiCallData['personaldetails']['billingCountryId'] = $billingInfo->countryId;
                            $apiCallData['personaldetails']['billingPhone'] = $billingInfo->phone;
                            //print_r($apiCallData);exit;
                        }
                        
                        
                        $ch = curl_init();

                        curl_setopt($ch, CURLOPT_URL,$apicallbackUrl);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($apiCallData));
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        $server_output = curl_exec($ch);

                        curl_close ($ch);
                        echo $server_output;
                        
                        
                    }
                    else
                    {
                        $postData = new \App\Model\Usercart;
                        $postData->userId = "0000";
                        $postData->warehouseId = "0000";
                        $postData->cartContent = $paymentTransacionData;
                        $postData->createdOn = date('Y-m-d H:i:s');
                        $postData->save();
                    }
                }
                else
                {
                    $postData = new \App\Model\Usercart;
                    $postData->userId = "0000";
                    $postData->warehouseId = "0000";
                    $postData->cartContent = 'paystack ref error';
                    $postData->createdOn = date('Y-m-d H:i:s');
                    $postData->save();
                }
                
            }
            else
            {
                $postData = new \App\Model\Usercart;
                $postData->userId = "0000";
                $postData->warehouseId = "0000";
                $postData->cartContent = 'Success error';
                $postData->createdOn = date('Y-m-d H:i:s');
                $postData->save();
            }
        }
        else
        {
            $postData = new \App\Model\Usercart;
            $postData->userId = "0000";
            $postData->warehouseId = "0000";
            $postData->cartContent = $body;
            $postData->createdOn = date('Y-m-d H:i:s');
            $postData->save();
        }
        
        
        
        exit();
    }
    
}
