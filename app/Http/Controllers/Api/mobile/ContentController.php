<?php

namespace App\Http\Controllers\Api\mobile;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Banner;
use App\Model\Blockcontent;
use App\Model\Stores;
use App\Model\Sitepage;
use App\Model\Contactus;
use App\Model\Emailtemplate;
use customhelper;
use Illuminate\Support\Facades\Hash;
use DB;
use Auth;
use Config;

class ContentController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->_perPage = 10;
    }

    public function getcontactinfo_mobile() {
        
        $result = array();
        $contactEmail = \App\Model\Generalsettings::where('settingsKey', 'contact_email')->first();
        $result['email'] = $contactEmail->settingsValue;
        $contactNumber = \App\Model\Generalsettings::where('settingsKey', 'contact_number')->first();
        if (!empty($contactNumber)) {

            $numberData = json_decode($contactNumber->settingsValue);
            if (!empty($numberData)) {
                $numCount = 0;
                foreach ($numberData as $index => $eachNumber) {
                    if ($numCount >= 2)
                        break;
                    $numParts = explode('^', $eachNumber);
                    $result['number'][$index]['img'] = "https://www.countryflags.io/" . $numParts[0] . "/flat/64.png";
                    $result['number'][$index]['title'] = $numParts[1];
                    $numCount++;
                }
            }
            if(!empty($result)){

                return response()->json([
                'status' => '200',
                'data' => $result,
                'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);

            }

        } else {

            return response()->json([
            'status' => '200',
            'data' => (object)[],
            'message' => 'success'
            ], 200);
        }
    }

    /**
     * Method for get reward details
     * @return jSON
     */
    public function getrewarddetails_mobile(Request $request) {

        if(empty($request->userId)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        } else {
            $userId = $request->userId;
            $data = array();

            /* GET USER NAME */
            $getQueryUserName = \App\Model\User::select('id', 'firstName', 'lastName', 'email', 'unit')->where('id', $userId)->first();
            if(!empty($getQueryUserName)){
                $data['userName'] = $getQueryUserName->firstName . " " . $getQueryUserName->lastName;
            }
            /* GET WALLET FUND */
            $walletQuery = \App\Model\Ewallet::where('recipientEmail', $getQueryUserName['email'])->first();
            if (!empty($walletQuery)) {
                $data['walletBalance'] = customhelper::getCurrencySymbolFormat($walletQuery->amount);
            } else {
                $data['walletBalance'] = customhelper::getCurrencySymbolFormat(0.00);
            }

            /* GET TOTAL ORDERS WITH ORDER AMOUNT */
            $getQueryOrders = \App\Model\Invoice::select(DB::raw("SUM(totalBillingAmount) as totalCost"), DB::raw("COUNT(id) as totalOrders"))->where('createdOn', 'LIKE', date('Y') . '-%')
                            ->where('userUnit', $getQueryUserName['unit'])->where("extracostCharged", 'N')->where('invoiceType', 'receipt')->get();
            if (!empty($getQueryOrders)) {
                $data['totalOrders'] = $getQueryOrders[0]->totalOrders;
                $data['totalCost'] = customhelper::getCurrencySymbolFormat($getQueryOrders[0]->totalCost);
            } else {
                $data['totalOrders'] = 0;
                $data['totalCost'] = customhelper::getCurrencySymbolFormat(0.00);
            }

            /* GET BALANCE POINTS */
            $getQueryBalancePoints = \App\Model\Fundpoint::where('userId', $userId)->first();
            if (!empty($getQueryBalancePoints)) {

                /* COUNT TOTAL POINT EARN */
                $getQueryEarnedPoints = \App\Model\Fundpointtransaction::where('userId', $userId)->where('type', 'A')->sum('point');
                $data['earnedPoints'] = $getQueryEarnedPoints;


                $data['balancePoint'] = $getQueryBalancePoints->point;

                /* GET TOTAL EARNED POINTS TRANSACTIONS */


                /* GET DATA FROM REWARD SETTINGS */
                $gainedPoint = ($getQueryBalancePoints->point == 0 ? 1 : $getQueryBalancePoints->point);

                $getQueryReward = \App\Model\Rewardsetting::where('deleted', '0')->whereRaw('points <= ' . $gainedPoint)->orderby('id', 'desc')->first();
                if (!empty($getQueryReward)) {
                    $data['rewardName'] = $getQueryReward->name;
                    $data['rewardImage'] = (file_exists(public_path() . '/uploads/rewardpoints/' . $getQueryReward->image) ? url('uploads/rewardpoints/' . $getQueryReward->image) : url('administrator/img/default-no-img.jpg') );

                    /* POINTS REMAINING TO ACHIEVE NEXT LEVEL */
                    $reminingPointsQuery = \App\Model\Rewardsetting::where('deleted', '0')->where("id", ">", $getQueryReward->id)->orderby('id', 'asc')->first();
                    $data['remaining'] = ($gainedPoint == 1 ? $reminingPointsQuery->points : $reminingPointsQuery->points - $gainedPoint);
                } else {
                    $data['rewardName'] = "No Reward";
                    $data['rewardImage'] = '';
                    $data['remaining'] = 'N/A';
                }



                /* POINTS NEAR TO EXPIRE */
                $splitExpDate = '';

                $nearToExpireQuery = \App\Model\Fundpointtransaction::where('expiredDate', ">", date('Y-m-d'))->orderby("expiredDate", 'asc')->first();
                if (!empty($nearToExpireQuery)) {
                    $splitExpDate = explode(" ", $nearToExpireQuery->expiredDate);
                    $totalPointsExpired = \App\Model\Fundpointtransaction::select(DB::raw("SUM(point) as point"))->where('expiredDate', "LIKE", trim($splitExpDate[0]) . "%")->get();

                    $dateExp = date_create(trim($splitExpDate[0]));
                    $data['expString'] = $totalPointsExpired[0]->point . " points will expires on " . date_format($dateExp, "M dS, Y");
                    $data['expStringShort'] = date_format($dateExp, "M dS, Y");
                } else {
                    $data['expString'] = '';
                    $data['expStringShort'] = '';
                }
            } else {

                /* NO POINT IN MAIN POINT TABLE */
                $data['balancePoint'] = 0;
                $data['earnedPoints'] = 0;

                $gainedPoint = 1;

                /* GET DATA FROM REWARD SETTINGS */
                $getQueryReward = \App\Model\Rewardsetting::where('deleted', '0')->orderby('points', 'asc')->first();
                if (!empty($getQueryReward)) {
                    $data['rewardName'] = $getQueryReward->name;
                    $data['rewardImage'] = url('uploads/rewardpoints/' . $getQueryReward->image);

                    /* POINTS REMAINING TO ACHIEVE NEXT LEVEL */
                    $reminingPointsQuery = \App\Model\Rewardsetting::where('deleted', '0')->where("id", ">", $getQueryReward->id)->orderby('id', 'asc')->first();
                    if (!empty($reminingPointsQuery)) {
                        $data['remaining'] = $reminingPointsQuery->points - $gainedPoint + 1;
                    } else {
                        $data['remaining'] = 0;
                    }
                } else {
                    $data['rewardName'] = "No Reward";
                    $data['rewardImage'] = '';
                    $data['remaining'] = 0;
                }





                $data['expString'] = '';
                $data['expStringShort'] = 'N/A';
            }

            if(!empty($data)){

                return response()->json([
                    'status' => '200',
                    'data' => $data,
                    'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success'
                ], 200);

            }
           
        }
    }

    /**
     * Method for get fund point transaction
     * @return jSON
     */
    public function getpointtransction_mobile(Request $request) {

        if(empty($request->activity) || empty($request->currentPage) || empty($request->perPage) || empty($request->userId)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        } else {

            $where = " 1 ";
            if ($request->activity == 'month')
                $where .= "  AND MONTH(date) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($request->activity == 'week')
                $where .= "  AND date BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($request->activity == 'year')
                $where .= "  AND date LIKE '" . date('Y') . '-%' . "'";
            else
                $where .= "";

            $page = $request->currentPage;
            $offset = ($page - 1) * $this->_perPage;

            $perPage = ($request->perPage != null ? $request->perPage : $this->_perPage);
            $offset1 = ($page - 1) * $perPage;


            $totalItems = \App\Model\Fundpointtransaction::where('userId', $request->userId)->whereRaw($where)->count();
            $queryTransctions = \App\Model\Fundpointtransaction::selectRaw("point, userId, id, senderId, reason, type, DATE_FORMAT(date,'%m/%d/%Y') as date")
                            ->where('userId', $request->userId)->whereRaw($where)
                            ->orderby('id', 'desc')->take($perPage)->skip($offset1)->get();

            if (count($queryTransctions)) {

                $data['transctions'] = $queryTransctions;
                $data['totalItems']  = $totalItems;
                $data['itemsPerPage']  = "$perPage";
                $data['countData']  = 1;

                return response()->json([
                    'status' => '200',
                    'data' => (object)$data,
                    'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success'
                ], 200);

            }
        }
    }

    /**
     * Method for get paystack settings
     * @return jSON
     */
    public function getpaystacksettings_mobile(Request $request) {

        $getSettings = \App\Model\Paymentgatewaysettings::where("paymentGatewayId", 9)->get();

        if(count($getSettings)){ 

            $data['key'] = $getSettings[2]->configurationValues;
            return response()->json([
            'status' => '200',
            'data' => (object)$data,
            'message' => 'success',
            ], 200);

        } else {

            return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success',
            ], 200);

        }

    }

    /**
     * Method for valid password checking
     * @param string $password
     * @return boolean
     */
    public function is_valid_password($password) {
        return preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$_!%*?&-]{8,20}$/', $password) ? true : false;
    }

    public function userforgotpassword_mobile(Request $request)
    {
        $inputEmail = $request->inputEmail;

        $data = array();

        /* MATCH THE PASSWORD */
        $queryTransctions = \App\Model\User::where('email', $inputEmail)->first();



        if (empty($queryTransctions)) {
            return response()->json([
                        'message' => 'Email does not match. Please correct.',
                        'status' => '400',
                        'data' => (object)[],
                            ], 200);
        }

        
        $otp = customhelper::generateOTP();
        $data['userId'] = $queryTransctions->id;
        $userObj = \App\Model\User::find($queryTransctions->id);
        $userObj->verifiedCode = $otp;
        $userObj->otpSentDate = \Carbon\Carbon::now();
        $userObj->save();

        /* ++++++++++ email functionality ++++++++ */
        $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'user_forgot_password_for_Mobile')->first();
        $replace['[NAME]'] = $queryTransctions->firstName . " " . $queryTransctions->lastName;
        $replace['[OTP]'] = $otp;
        $to = $queryTransctions->email;
        $mail = customhelper::SendMail($emailTemplate, $replace, $to);
        /* ++++++++++ end of email functionality ++++ */



        if($mail){ 
            return response()->json([
            'status' => '200',
            'data' => (object)$data,
            'message' => 'success',
            ], 200);

        } else {

            return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success',
            ], 200);

        }

    }

    public function resetPassword_mobile(Request $request) {
        $data = array();
        $userId = $request->userId;

        if(!$userId){
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'Please provide a valid user Id'
            ], 400);
        }

        /* GET USER DETAILS */

        $queryUserDetails = \App\Model\User::select("email", "id")->where('id', $userId)->where('deleted', '0')->first();

        if (!empty($queryUserDetails)) {
            $data['userEmail'] = $queryUserDetails;
        }

        $userObj = \App\Model\User::find($userId);

            if (!empty($request->newPassword) && !empty($request->newRepeatPassword)) {
                if ($request->newPassword == $request->newRepeatPassword) {
                    if($this->is_valid_password($request->newPassword) == false){
                        return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => 'Password must contains minimum 8 characters, atleast one digit, one uppercase letter'
                        ], 400);
                    }

                    $userObj->password = Hash::make($request->newPassword);
                    $userObj->save();

                    /* ++++++++++ email functionality ++++++++ */
                        $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'successful_reset_password')->first();
                        $replace['[NAME]'] = $queryUserDetails->firstName . " " . $queryUserDetails->lastName;
                        $replace['[PASSWORD]'] = $request->newPassword;
                        $to = $queryUserDetails->email;
                        $mail = customhelper::SendMail($emailTemplate, $replace, $to);
                    /* ++++++++++ end of email functionality ++++ */

                    return response()->json([
                    'status' => '200',
                    'data' => null,
                    'message' => 'Password has been changed successfully'
                    ], 200);


                } else {
                    return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'New password and confirm password did not matched'
                    ], 400);
                }
            } else {
                return response()->json([
                 'status' => '200',
                    'data' => null,
                    'message' => 'Old password not matched'
                ], 200);
            }
       

    }


    /**
     * Method for valid password checking
     * @param integer $length
     * @return string
     */
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Method for get the header menus
     * @return array
     */
    public function getheadernavigation_mobile() {

        $data = array();
        $data['baseurl'] = str_replace('/public', '', url('web/#/'));
        $nav = \App\Model\Menu::where('deleted', '0')->where('position', '0')->where('status', '1')->orderby('sort', 'asc')->get();
        if (!empty($nav)) {
            $data['headermNav'] = $nav;
        }
        
        if(!empty($data)){

            return response()->json([
                'status' => '200',
                'data' => $data,
                'message' => 'success'
            ], 200);

        } else {

            return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
            ], 200);

        }
    }

    /**
     * Method for get the footer menus
     * @return array
     */
    public function getfooternavigation_mobile() {
        
        $data = array();
        $nav = \App\Model\Menu::where('deleted', '0')->where('position', '1')->where('status', '1')->orderby('sort', 'asc')->get();
        if (!empty($nav)) {
            $data['footermNav'] = $nav;
        }

        if(!empty($data)){

            return response()->json([
                'status' => '200',
                'data' => $data,
                'message' => 'success'
            ], 200);

        } else {

            return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
            ], 200);

        }

    }

    public function getsociallinks_mobile() {

        $footerLinks = \App\Model\Socialmedia::where('display', '1')->get();

        if(count($footerLinks)){

            return response()->json([
                'status' => '200',
                'data' => $footerLinks,
                'message' => 'success'
            ], 200);

        } else {

            return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
            ], 200);

        }

    }

    public function gethomepageadbanner_mobile() {
        
        $results = array();
        $data = Banner::where('type', '7')->where('isDefault', '1')->first();
        if (!empty($data)) {
            $results['bannerImage'] = url('uploads/banner/' . $data->imagePath);
            $results['pageLink'] = $data->pageLink;
        }

        if(!empty($results)){

            return response()->json([
                'status' => '200',
                'data' => $results,
                'message' => 'success'
            ], 200);

        } else {

            return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
            ], 200);

        }
        
    }

    /**
     * Method for get paypal ipn settings
     * @return jSON
     */
    public function getpaypalipnsettings_mobile(Request $request) {
        
        if(empty($request->currencyCode) || empty($request->total)){
           
           return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $data = array();
            $getSettings = \App\Model\Paymentgatewaysettings::where("paymentGatewayId", 10)->where('configurationKeys', 'clientID')->first()->toArray();
            $getMode = \App\Model\Paymentgatewaysettings::where("paymentGatewayId", 10)->where('configurationKeys', 'Mode')->first()->toArray();
            $data['key'] = $getSettings['configurationValues'];
            //mode
            $mode = ($getMode['configurationValues']=='Live'?'production':'sandbox');

            /* CURRENCY EXCHANGE IF CURRENCY NOT IN USD */
            if (!empty($request->currencyCode)) {
                $exchangeRate = \App\Model\Currency::currencyExchangeRate($request->currencyCode, 'USD');
                $data['total'] = \App\Model\Currency::currencyConverter($request->total, $exchangeRate);
                $data['currencyCode'] = 'USD';
            } else {
                $data['total'] = $request->total;
                $data['currencyCode'] = 'USD';
            }
            if(!empty($data)){

                $data['config'] = array(
                    //'ENV'=> 'sandbox',
                    //'ENV'=> 'sandbox',
                    'ENV'=> $mode,
                    'SANDBOX_CLIENT_ID'=> 'AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R',
                    'PRODUCTION_CLIENT_ID'=> $data['key']
                ); 
                
                return response()->json([
                'status' => '200',
                'data' => $data,
                'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);
            }
        }

    }

    /**
     * Method for get documents for customers
     * 
     */
    public function getdocuments_mobile(Request $request) {
        
        if(empty($request->userId)){
           
           return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $data = array();
            $fileExt = '';
            $base64Img = '';
            $encodedImg = '';
            $imageSmall = '';
            $userId = $request->userId;
            $documentId = '';

            $getDoc = \App\Model\Document::where("userId", $userId)->where('deleted', '0')->orderby('id', 'desc')->get();
            if (!empty($getDoc)) {

                foreach ($getDoc as $key => $val) {

                    if (file_exists(public_path() . "/uploads/documents/" . $userId . "/" . $val->filename)) {

                        //$encodedImg = base64_encode(file_get_contents(public_path() . "/uploads/documents/" . $userId . "/" . $val->filename));
                        $fileExt = pathinfo(public_path() . "/uploads/documents/" . $userId . "/" . $val->filename, PATHINFO_EXTENSION);

                        if ($fileExt == 'png' || $fileExt == 'jpeg' || $fileExt == 'jpg' || $fileExt == 'svg' || $fileExt == 'bmp' || $fileExt == 'gif') {
                            $imageSmall = 'images.png';
                            //$base64Img = 'data:image/' . $fileExt . ';base64,' . $encodedImg;
                            $base64Img = url('/') . "/uploads/documents/" . $userId . "/" . $val->filename;
                        } elseif ($fileExt == 'pdf' || $fileExt == 'PDF') {
                            $imageSmall = 'pdf.png';
                            $base64Img = url('/') . "/uploads/documents/" . $userId . "/" . $val->filename;
                        } else {
                            $imageSmall = 'doc.png';
                            $base64Img = url('/') . "/uploads/documents/" . $userId . "/" . $val->filename;
                        }

                        $data[] = array(
                            "des" => $val->descr,
                            "document" => $val->filename,
                            "base64" => $base64Img,
                            "fileExt" => $imageSmall,
                            "documentId" => $val->id,
                            "createdOn" => date("m/d/Y", strtotime($val->createdOn))
                        );

                    } else {

                        $data[] = array(
                            "des" => $val->descr,
                            "document" => $val->filename,
                            "base64" => '',
                            "fileExt" => 'image-not-available.jpg',
                            "documentId" => $val->id,
                            "createdOn" => $val->createdOn
                        );
                    }
                }
            }

            if(!empty($data)){

                return response()->json([
                'status' => '200',
                'data' => $data,
                'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);
            }
        }
    }

    /**
     * Method for delete document for customers
     *
     */
    public function deletedocument_mobile(Request $request) {
        
        if(empty($request->userId) || empty($request->docId)){
           
           return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $userId = $request->userId;
            $docId = $request->docId;
            $imgPath = '';
            $data = array();
            /* GET FILE NAME */
            $getRecord = \App\Model\Document::where("userId", $userId)->where('id', $docId)->first();

            if (!empty($getRecord)) {
                $imgPath = public_path() . "/uploads/documents/" . $userId . "/" . $getRecord->filename;

                #chmod($imgPath, 0777);

                /* DELETE ACTUAL FILE */
                if (file_exists($imgPath)) {
                    //unlink($imgPath);
                }

                /* DELETE RECORD */

                $doc = \App\Model\Document::find($docId);
                $doc->deletedBy = $userId;
                $doc->deletedOn = Config::get('constants.CURRENTDATE');
                $doc->deleted = '1';
                $doc->save();

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                'status' => '400',
                'data' => (object)[],
                'message' => 'No record found!'
                ], 200);
            }
        }
    }

    /**
     * Method for upload document for customers
     *
     */
    public function uploaddocument_mobile(Request $request) {
        
        if(empty($request->userId)){
           
           return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $data = array();
            $userId = $request->userId;
            if ($request->hasFile('itemImage')) {

                $validator = Validator::make($request->all(), [
                            'itemImage' => 'mimes:jpeg,jpg,gif,png,bmp,svg,doc,docx,odt,xls,xlsx,pdf,txt|max:4096'
                ]);

                if ($validator->fails()) {

                    $err = json_decode($validator->messages());
                    return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => $err->itemImage[0]
                    ], 200);

                }


                $image = $request->file('itemImage');
                $name = time() . '_' . $image->getClientOriginalName();

                $destinationPath = public_path('/uploads/documents/' . $userId);
                if (!file_exists($destinationPath)) {
                    mkdir($destinationPath);
                    chmod($destinationPath, 0777);
                }

                $image->move($destinationPath, $name);

                /* INSERT QUERY */
                $doc = new \App\Model\Document;
                $values = array('userId' => $userId, 'filename' => $name, 'descr' => $request->descr, 'createdOn' => Config::get('constants.CURRENTDATE'), 'createdBy' => $userId);
                $doc->insert($values);

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                'status' => '400',
                'data' => (object)[],
                'message' => 'Error in operation'
                ], 200);

            }
        }
    }

    /**
     * Method post claim for customers
     */
    public function postclaim_mobile(Request $request) {
        
        if(empty($request->userId) || empty($request->orderNumber)){
           
           return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $userId = $request->userId;
            $data = array();
            $message = '';
            $user = \App\Model\User::find($userId);
            $adminuser = \App\Model\UserAdmin::find(1);

            /* VALIDATE ORDER ID */
            $validateOrderNumber = \App\Model\Order::where("userId", $userId)->where("orderNumber", filter_var($request->orderNumber, FILTER_SANITIZE_STRING))->first();
            if (empty($validateOrderNumber)) {

                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'Order number not matched'
                ], 200);

            }

            /* GET THE FORM VALUES */
            $itemDescription = filter_var($request->itemDescription, FILTER_SANITIZE_STRING);
            $orderNumber = filter_var($request->orderNumber, FILTER_SANITIZE_STRING);
            $itemNumber = filter_var($request->itemNumber, FILTER_SANITIZE_STRING);
            $dateOfPurchase = filter_var($request->dateOfPurchase, FILTER_SANITIZE_STRING);
            $purchaseDateOfItem = filter_var($request->purchaseDateOfItem, FILTER_SANITIZE_STRING);
            $purchaseDateOfItem = substr($purchaseDateOfItem,0,(strpos($purchaseDateOfItem,'(')-1));
            $purchaseDateOfItem = \Carbon\Carbon::parse($purchaseDateOfItem)->format('Y-m-d H:i:s');
            $placeOfPurchase = filter_var($request->placeOfPurchase, FILTER_SANITIZE_STRING);
            $totalAmountClaimed = filter_var($request->totalAmountClaimed, FILTER_SANITIZE_STRING);
            $natureOfDamage = filter_var($request->natureOfDamage, FILTER_SANITIZE_STRING);
            $dateItemsReceived = filter_var($request->dateItemsReceived, FILTER_SANITIZE_STRING);
            $shipmentVia = filter_var($request->shipmentVia, FILTER_SANITIZE_STRING);

            $cliamentName = filter_var($request->cliamentName, FILTER_SANITIZE_STRING);
            $cliamentPhone = filter_var($request->cliamentPhone, FILTER_SANITIZE_STRING);
            $cliamentAddress = filter_var($request->cliamentAddress, FILTER_SANITIZE_STRING);

            $request->claimDate = substr($request->claimDate,0,(strpos($request->claimDate,'(')-1));
            $claimDate = \Carbon\Carbon::parse($request->claimDate)->format('Y-m-d H:i:s');

            $loss = $request->loss;
            $damage = $request->damage;
            $incomplete = $request->incomplete;

            if ($loss)
                $loss = "Loss";
            if ($damage)
                $damage = "Damage";
            if ($incomplete)
                $incomplete = "Incomplete";

            $makeArray = array(
                "Descriptionofitem" => $itemDescription,
                "OrderNumber" => $orderNumber,
                "ItemNumber" => $itemNumber,
                "Valueifitematdateofpurchase" => $dateOfPurchase,
                "PurchaseDateforItem" => $purchaseDateOfItem,
                "PlaceOfPurchase" => $placeOfPurchase,
                "Totalamountbeenclaimed" => $totalAmountClaimed,
                "Explainnatureofdamage" => $natureOfDamage,
                "Dateitemswerereceived" => $dateItemsReceived,
                "shipmentVia" => $shipmentVia,
                "claimDate" => $claimDate,
                "cliamentName" => $cliamentName,
                "cliamentPhone" => $cliamentPhone,
                "cliamentAddress" => $cliamentAddress,
                "loss" => $loss,
                "damage" => $damage,
                "incomplete" => $incomplete
            );

            $res = $this->postcontact($user, $makeArray);
            
            if($res){

                return response()->json([
                    'status' => '200',
                    'data' => (object)['message_id'=>$res],
                    'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                    'status' => '400',
                    'data' => null,
                    'message' => 'An error occured while adding information!'
                ], 200);
            }
        }
    }
    public function postcontact($user, $makeArray) {

        /* SAVE THIS RECORD INTO CONTACT US TABLE */

        $contact = new Contactus;
        $contact->name = $user->firstName . " " . $user->lastName;
        $contact->address = "";
        $contact->alternateAddress = "";
        $contact->country = $user->countryId;
        $contact->state = $user->stateId;
        $contact->city = $user->cityId;
        $contact->company = $user->company;
        $contact->phone = $user->contactNumber;
        $contact->email = $user->email;
        $contact->subject = "File a Claim";
        $contact->email = $user->email;
        $contact->message = json_encode($makeArray);
        $contact->status = '1';
        $contact->reason_id = '11';
        $contact->postedOn = Config::get('constants.CURRENTDATE');
        if($contact->save()){
            return $contact->id;
        } else {
            return false;
        }
    }

    /*
    * upload post claim image
    */
    public function postclaimimage_mobile(Request $request){
        
        if(empty($request->userId) || empty($request->message_id)){
           
           return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $userId = $request->userId;
            $messageId = $request->message_id;

            $userMessage = \App\Model\Contactus::find($messageId);

            if($userMessage){

                $requestMessage = json_decode($userMessage->message);
                $preDamagedItemImagePath = !empty($requestMessage->image2)?$requestMessage->image2:'';
                $preItemImagePath = !empty($requestMessage->image)?$requestMessage->image:'';
                $preItemImage = '';
                $preDamagedItemImage = '';

                if($preDamagedItemImagePath){
                    $arr = explode('/', $preDamagedItemImagePath);
                    $preDamagedItemImage = end($arr);
                }

                if($preItemImagePath){
                    $arrI = explode('/', $preItemImagePath);
                    $preItemImage = end($arrI);
                }

                /* IF SECOND FILE [Upload Photo of Damaged Item(If Applicable)] EXISTS */
                if ($request->hasFile('damagedItemImage')) {

                    $validator = Validator::make($request->all(), [
                                'damagedItemImage' => 'mimes:jpeg,jpg,gif,png,bmp,svg,doc,docx,odt,xls,xlsx,pdf,txt|max:4096'
                    ]);

                    if ($validator->fails()) {

                        $err = json_decode($validator->messages());
                        return response()->json([
                            'status' => '400',
                            'data' => null,
                            'message' => $err->damagedItemImage[0]
                        ], 200);
                    }

                    $image2 = $request->file('damagedItemImage');
                    $name2 = time() . '_' . $image2->getClientOriginalName();

                    $attachmentpath2 = public_path('/uploads/fileaclaim/' . $userId) . "/" . $name2;
                    $actualpath2 = url('/uploads/fileaclaim/' . $userId) . "/" . $name2;

                    $destinationPath2 = public_path('/uploads/fileaclaim/' . $userId);
                    if (!file_exists($destinationPath2)) {
                        mkdir($destinationPath2);
                        chmod($destinationPath2, 0777);
                    }

                    $image2->move($destinationPath2, $name2);

                    $requestMessage->image2 = $actualpath2;
                }

                /* IF FILE EXISTS */
                if ($request->hasFile('itemImage')) {
                    $validator = Validator::make($request->all(), [
                                'itemImage' => 'mimes:jpeg,jpg,gif,png,bmp,svg,doc,docx,odt,xls,xlsx,pdf,txt|max:4096'
                    ]);

                    if ($validator->fails()) {

                        $err = json_decode($validator->messages());
                        return response()->json([
                            'status' => '400',
                            'data' => null,
                            'message' => $err->itemImage[0]
                        ], 200);

                    }


                    $image = $request->file('itemImage');
                    $name = time() . '_' . $image->getClientOriginalName();

                    $attachmentpath = public_path('/uploads/fileaclaim/' . $userId) . "/" . $name;
                    $actualpath = url('/uploads/fileaclaim/' . $userId) . "/" . $name;

                    $destinationPath = public_path('/uploads/fileaclaim/' . $userId);
                    if (!file_exists($destinationPath)) {
                        mkdir($destinationPath);
                        chmod($destinationPath, 0777);
                    }

                    $image->move($destinationPath, $name);

                    

                    $requestMessage->image = $actualpath;
                }

                
                $userMessage->message = json_encode($requestMessage);

                if($userMessage->save()){

                    $preItemImage = '';
                    $preDamagedItemImage = '';
                    
                    $destinationDelPath = public_path('/uploads/fileaclaim/' . $userId.'/'.$preItemImage);
                    if($preItemImage && file_exists($destinationDelPath)){
                        @unlink($destinationDelPath);
                    }
                    $destinationDelPathI = public_path('/uploads/fileaclaim/' . $userId.'/'.$preDamagedItemImage);
                    if($preDamagedItemImage && file_exists($destinationDelPathI)){
                        @unlink($destinationDelPathI);
                    }

                    return response()->json([
                        'status' => '200',
                        'data' => (object)['message_id'=>$messageId,'image'=>!empty($actualpath)?$actualpath:'','attachment_image'=>!empty($attachmentpath)?$attachmentpath:'','image2'=>!empty($actualpath2)?$actualpath2:'','attachment_damage_image'=>!empty($attachmentpath2)?$attachmentpath2:''],
                        'message' => 'success'
                    ], 200);

                } else {

                   return response()->json([
                        'status' => '400',
                        'data' => null,
                        'message' => 'An error occured while adding information!'
                    ], 200); 
                }


            } else {

                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured'
                ], 200);
            }

        }
    }

    /*
    * send mail after post claim
    */
    public function postclaimsendmail_mobile(Request $request){

        if(empty($request->userId) || empty($request->message_id) || empty($request->has_attachment)){
           
           return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $userId = $request->userId;
            $messageId = $request->message_id;
            $message = '';
            $user = \App\Model\User::find($userId);
            $userMessage = \App\Model\Contactus::find($messageId);
            $adminuser = \App\Model\UserAdmin::find(1);
            
            if($userMessage){

                $requestMessage = json_decode($userMessage->message);
                $itemDescription = filter_var($requestMessage->Descriptionofitem, FILTER_SANITIZE_STRING);
                $orderNumber = filter_var($requestMessage->OrderNumber, FILTER_SANITIZE_STRING);
                $itemNumber = filter_var($requestMessage->ItemNumber, FILTER_SANITIZE_STRING);
                $dateOfPurchase = filter_var($requestMessage->Valueifitematdateofpurchase, FILTER_SANITIZE_STRING);
                $purchaseDateOfItem = filter_var($requestMessage->PurchaseDateforItem, FILTER_SANITIZE_STRING);
                
                $purchaseDateOfItem = substr($purchaseDateOfItem,0,(strpos($purchaseDateOfItem,'(')-1));
                $purchaseDateOfItem = \Carbon\Carbon::parse($purchaseDateOfItem)->format('Y-m-d H:i:s');

                $placeOfPurchase = filter_var($requestMessage->PlaceOfPurchase, FILTER_SANITIZE_STRING);
                $totalAmountClaimed = filter_var($requestMessage->Totalamountbeenclaimed, FILTER_SANITIZE_STRING);
                $natureOfDamage = filter_var($requestMessage->Explainnatureofdamage, FILTER_SANITIZE_STRING);
                $dateItemsReceived = filter_var($requestMessage->Dateitemswerereceived, FILTER_SANITIZE_STRING);
                $shipmentVia = filter_var($requestMessage->shipmentVia, FILTER_SANITIZE_STRING);

                $cliamentName = filter_var($requestMessage->cliamentName, FILTER_SANITIZE_STRING);
                $cliamentPhone = filter_var($requestMessage->cliamentPhone, FILTER_SANITIZE_STRING);
                $cliamentAddress = filter_var($requestMessage->cliamentAddress, FILTER_SANITIZE_STRING);
                
                //$claimDate = $requestMessage->claimDate;
                $requestMessage->claimDate = substr($requestMessage->claimDate,0,(strpos($requestMessage->claimDate,'(')-1));
                $claimDate = \Carbon\Carbon::parse($requestMessage->claimDate)->format('Y-m-d H:i:s');

                $loss = $requestMessage->loss;
                $damage = $requestMessage->damage;
                $incomplete = $requestMessage->incomplete;

                $message .= '
                <p><b>Description of item:</b> ' . $itemDescription . '</p>
                <p><b>Order Number:</b> ' . $orderNumber . '</p>
                <p><b>Item number:</b> ' . $itemNumber . '</p>
                <p><b>Value if item at date of purchase:</b> ' . $dateOfPurchase . '</p>
                <p><b>Purchase date for item:</b> ' . $purchaseDateOfItem . '</p>
                <p><b>Place of purchase:</b> ' . $placeOfPurchase . '</p>
                <p><b>Total amount been claimed:</b> ' . $totalAmountClaimed . '</p>
                <p><b>Explain nature of damage:</b> ' . $natureOfDamage . '</p>
                <p><b>Shipment via:</b>    ' . $shipmentVia . '</p>
                <p><b>Date items were received:</b>    ' . $dateItemsReceived . '</p>
                <p><b>The Claim is made against the carrier for:<b> ' . $loss . '&nbsp;' . $damage . '&nbsp;' . $incomplete . '</p>
                <p><b>Claim Date:</b> ' . $claimDate . '</p>
                <p><b>Name of Claimant:</b> ' . $cliamentName . '</p>
                <p><b>Address:</b> ' . $cliamentAddress . '</p>
                <p><b>Phone:</b> ' . $cliamentPhone . '</p>
                ';

                if($request->has_attachment=='yes'){

                    $attachmentpath = $request->attachment_image;
                    $attachmentpath2 = $request->attachment_damage_image;
                    $mailTemplatevars = Config::get('constants.emailTemplateVariables');
                    $emailTemplate = Emailtemplate::where('templateKey', 'File_a_claim')->first();
                    $arrayvalues = [];
                    $replace['[NAME]'] = $user->firstName . " " . $user->lastName;
                    $replace['[EMAIL]'] = $user->email;
                    $replace['[NOTIFICATION]'] = $message;
                    $to = $adminuser->email;

                    foreach ($mailTemplatevars as $key => $val) {
                        if (isset($replace[$val]) == $val) {
                            $arrayvalues[] = $replace[$val];
                        } else {
                            $arrayvalues[] = '';
                        }
                    }

                    $content = str_replace($mailTemplatevars, $arrayvalues, stripslashes($emailTemplate->templateBody));

                    /* ++++++++++ email functionality ++++++++ */
                    \Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($to, $attachmentpath, $emailTemplate, $user, $request, $attachmentpath2) {
                        $message->from($user->email, $user->firstName . " " . $user->lastName);
                        $message->subject($emailTemplate->templateSubject);
                        $message->to($to);
                        if($attachmentpath){
                            $message->attach($attachmentpath);
                        }
                        if ($attachmentpath2) {
                            $message->attach($attachmentpath2);
                        }
                    });
                    /* ++++++++++ end of email functionality ++++ */

                } else {

                    /* ++++++++++ email functionality ++++++++ */
                    $emailTemplate = Emailtemplate::where('templateKey', 'File_a_claim')->first();
                    $replace['[NAME]'] = $user->firstName . " " . $user->lastName;
                    $replace['[EMAIL]'] = $user->email;
                    $replace['[NOTIFICATION]'] = $message;
                    $to = $adminuser->email;
                    customhelper::SendMail($emailTemplate, $replace, $to);
                    /* ++++++++++ end of email functionality ++++ */
                }

                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'Thank you. We will get back to you within 2 business days.'
                ], 200);


            } else {

                return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured'
                ], 200);
            }

        }
    }

    /**
     * Method for get claims list
     */
    public function getclaims_mobile(Request $request) {
        
        if(empty($request->useEmail) || empty($request->perPage) || empty($request->currentPage)){
           
           return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $data = array();
            $reply = new \App\Model\Contactreply;
            $replyTable = $reply->table;
            $contact = new \App\Model\Contactus;
            $contactTable = $contact->table;

            $page = $request->currentPage;
            $offset = ($page - 1) * $this->_perPage;

            $perPage = ($request->perPage != null ? $request->perPage : $this->_perPage);
            $offset1 = ($page - 1) * $perPage;


            $useEmail = $request->useEmail;

            $totalItems = \App\Model\Contactus::where('email', $useEmail)->where($contact->table . '.status', '3')->where($contact->table . '.reason_id', 11)->join($replyTable, "$replyTable.contactusId", "=", "$contactTable.id")->count();

            $resultSet = \App\Model\Contactus::where('email', $useEmail)->where($contact->table . '.status', '3')->where($contact->table . '.reason_id', 11)->join($replyTable, $replyTable . ".contactusId", "=", $contactTable . ".id")->orderby($contact->table . '.id', 'desc')->take($perPage)->skip($offset1)->get();
            ;

            if (!empty($resultSet)) {

                $data['resultSet'] = $resultSet;
                $data['totalItems'] = $totalItems;
                $data['itemsPerPage'] = $perPage;
                $data['countData'] = 1;

                return response()->json([
                    'status' => '200',
                    'data' => (object)$data,
                    'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success'
                ], 200);

            }
        }
    }


    /**
     * Method for get user messages
     *
     */
    public function getmessages_mobile(Request $request) {

        if(empty($request->userId) || empty($request->activity) || empty($request->perPage) || empty($request->currentPage)){
           
           return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $where = " 1 ";
            if ($request->activity == 'month')
                $where .= "  AND MONTH(sentOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($request->activity == 'month2')
                $where .= "  AND sentOn BETWEEN '" . \Carbon\Carbon::now()->startOfMonth()->subMonth()->toDateString() . "' AND '" . \Carbon\Carbon::now()->endOfMonth()->toDateString() . "'";
            else if ($request->activity == 'week')
                $where .= "  AND sentOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($request->activity == 'week2')
                $where .= "  AND sentOn BETWEEN '" . \Carbon\Carbon::today()->subWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($request->activity == 'year')
                $where .= "  AND sentOn LIKE '" . date('Y') . '-%' . "'";
            else if ($request->activity == 'day')
                $where .= "  AND sentOn BETWEEN '" . \Carbon\Carbon::now()->subDay()->toDateTimeString() . "' AND '" . \Carbon\Carbon::now() . "'";
            else
                $where .= "";

            $unot = new \App\Model\Usernotification();
            $unotTable = $unot->prefix . $unot->table;

            if ($request->msgtype != '') {
                $where .= "  AND " . $unotTable . ".status = '" . $request->msgtype . "'";
            }

            $page = $request->currentPage;
            $offset = ($page - 1) * $this->_perPage;

            $perPage = ($request->perPage != null ? $request->perPage : $this->_perPage);
            $offset1 = ($page - 1) * $perPage;


            $totalItems = \App\Model\Usernotification::where('userId', $request->userId)->whereRaw($where)->count();
            $queryTransctions = \App\Model\User::getmessages($request->userId, $where, $perPage, $offset1);

            if (!empty($queryTransctions)) {

                $data['transctions'] = $queryTransctions;
                $data['totalItems'] = $totalItems;
                $data['itemsPerPage'] = "$perPage";
                $data['countData'] = 1;

                return response()->json([
                    'status' => '200',
                    'data' => $data,
                    'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success'
                ], 200);

            }
        }
    }

    /**
     * Method for get message (notification) content
     *
     */
    public function getmessagecontent_mobile(Request $request) {
        
        if(empty($request->userId) || empty($request->msgId)){
           
           return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $data = array();
            
            $content = \App\Model\Usernotification::where('userId', $request->userId)->where("id", $request->msgId)->first();

            $data['subject'] = $content->subject;
            $data['message'] = html_entity_decode($content->message);
            $data['countData'] = 1;

            $notifObj = \App\Model\Usernotification::find($request->msgId);
            $notifObj->status = '1';
            $notifObj->save();

            return response()->json([
                'status' => '200',
                'data' => $data,
                'message' => 'success'
            ], 200);

        }
    }

}