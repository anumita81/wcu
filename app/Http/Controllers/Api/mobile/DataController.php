<?php

namespace App\Http\Controllers\Api\mobile;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Warehouse;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use DB;
use Mail;

class DataController extends Controller {

    public function getallwarehouse_mobile() {
        
        $results = array();
        $allWarehouse = Warehouse::where('status', '1')->where('deleted', '0')->with('country', 'state', 'city')->get();
        $warehouseBanner = \App\Model\Banner::select('imagePath')->where('type', '3')->where('bannerType', 'image')->where('status','1')->get()->toArray();
        if (!empty($allWarehouse)) {
            $results = $allWarehouse;

            for($i=0; $i<count($results);$i++)
            {
               $results[$i]['imagepath'] = url('uploads/warehouse/' .$results[$i]['image']); 
               $results[$i]['name'] = $results[$i]['country']['name'];
               $results[$i]['code'] = $results[$i]['country']['code'];  
            }
        }
    
        if(!empty($warehouseBanner))
        {
            $banner = url('uploads/banner/'.$warehouseBanner[0]['imagePath']);
        }else{
            $banner = '';
        }
    
        if(!empty($results)){

            return response()->json([
            'status' => '200',
            'data' => (object)['results'=>$results,'banner'=>$banner],
            'message' => 'success'
            ], 200);

        } else {

            return response()->json([
            'status' => '200',
            'data' => (object)[],
            'message' => 'success'
            ], 200);

        }
        
    }

    public function getstorelist_mobile(Request $request) {
        
        if(empty($request->type)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else if(!empty($request->type) && !in_array($request->type, ['shopforme','autopart'])){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured, invalid type'
            ], 200);

        }else {

            $data = array();
            $type = $request->type;
            $warehouseId = $request->warehouseId;

            if (!empty($warehouseId)) {
                $data = \App\Model\Stores::getAllStores(array('type' => $type, 'warehouseId' => $warehouseId));
            } else {
                $data = \App\Model\Stores::select('id', 'storeName')
                        ->where('deleted', '0')
                        ->where('status', '1')
                        ->where('storeType', $type)
                        ->get();
            }
            if(count($data)){

                return response()->json([
                'status' => '200',
                'data' => $data,
                'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);

            }   
        }
        
    }

    public function getcategorylist_mobile(Request $request) {
        
        $data = array();

        $type = $request->type;
        $storeId = $request->storeId;

        if (!empty($storeId)) {
            $data = \App\Model\Stores::getAllCategoriesByStore(array('type' => $type,  'storeId' => $storeId));
        } else {
            if (!empty($type)) {
                $data = \App\Model\Sitecategory::select('id', 'categoryName')
                        ->where('parentCategoryId', '-1')
                        ->where('status', '1')
                        ->where('type', $type)
                        ->get();
            } else {
                $data = \App\Model\Sitecategory::select('id', 'categoryName')
                        ->where('parentCategoryId', '-1')
                        ->where('status', '1')
                        ->get();
            }
        }

        if(count($data)){

            return response()->json([
            'status' => '200',
            'data' => $data,
            'message' => 'success'
            ], 200);

        } else {

            return response()->json([
            'status' => '200',
            'data' => (object)[],
            'message' => 'success'
            ], 200);

        }

    }

    public function getsubcategorylist_mobile(Request $request) {
        
        if(empty($request->categoryId)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $id = $request->categoryId;
            $data = array();

            $data = \App\Model\Sitecategory::select('id', 'categoryName')
                    ->where('parentCategoryId', $id)
                    ->where('status', '1')
                    ->get();

            if(count($data)){

                return response()->json([
                'status' => '200',
                'data' => $data,
                'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);

            }        
        }

    }

    public function getproductlist_mobile(Request $request) {
        
        if(empty($request->categoryId)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $id = $request->categoryId;
            $results = array();
            $data = \App\Model\Siteproduct::select('id', 'productName', 'image')
                    ->where('subCategoryId', $id)
                    ->where('status', '1')
                    ->get();


            if (!empty($data)) {
                foreach ($data as $key => $eachdata) {
                    $results[$key]['id'] = $eachdata->id;
                    $results[$key]['productName'] = $eachdata->productName;
                    if ($eachdata->image != '')
                        $results[$key]['image'] = url('uploads/site_products/' . $eachdata->image);
                    else
                        $results[$key]['image'] = "";
                }

                return response()->json([
                'status' => '200',
                'data' => $results,
                'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success'
                ], 200);

            }
        }

    }

    public function getmakelist_mobile(Request $request) {
        $type = '0';
        if(!empty($request->typeId)){
            $type = $request->typeId;
        }

        $data = \App\Model\Automake::where('deleted', '0')->where('type', $type)->where('status', '0')->orderBy('name', 'ASC')->get();

        if(count($data)){ 
            return response()->json([
            'status' => '200',
            'data' => $data,
            'message' => 'success',
            ], 200);
        } else {
            return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success',
            ], 200);
        }
    }

    public function getmodellist_mobile(Request $request) {
        
        if(empty($request->makeId)){

             return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $makeId = $request->makeId;
            $data = \App\Model\Automodel::where('makeId', $makeId)->where('makeType', '0')->where('deleted', '0')->orderBy('name', 'ASC')->get();

            if(count($data)){ 
                return response()->json([
                'status' => '200',
                'data' => $data,
                'message' => 'success',
                ], 200);
            } else {
                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);
            }
        }
    }

    public function getwarehouselist_mobile() {

        $data = \App\Model\Warehouse::warehouseCountryMap();
        if(count($data)){ 
            return response()->json([
            'status' => '200',
            'data' => $data,
            'message' => 'success',
            ], 200);
        } else {
            return response()->json([
                'status' => '200',
                'data' => (object)[],
                'message' => 'success',
            ], 200);
        }
    }

    public function getautoallcharges_mobile(Request $request) {
        
        if(empty($request->userId) || empty($request->warehouseId)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $userId = $request->userId;
            $warehouseId = $request->warehouseId;
            $results = array();
            $itemPrice = "0.00";
            $insuranceCost = "0.00";
            $fromCity = "";
            if ($request->price != '')
                $itemPrice = $request->price;
            if ($request->fromCity != '')
                $fromCity = $request->fromCity;

            $userShippingAddress = \App\Model\Addressbook::where('userId', $userId)->where('isDefaultShipping', '1')->first();

            $param = array(
                'fromCountry' => $request->fromCountry,
                'fromState' => $request->fromState,
                'fromCity' => $fromCity,
                'toCountry' => $userShippingAddress->countryId,
                'toState' => $userShippingAddress->stateId,
                'toCity' => $userShippingAddress->cityId
            );
            $currencyCode = \App\Helpers\customhelper::getCurrencySymbolCode('', true);
            $currencySymbol = \App\Helpers\customhelper::getCurrencySymbolCode($currencyCode);
            $exchangeRate = 1;
            

            $autoData = array('make' => $request->make, 'model' => $request->model);
            $shippingCostsArr = \App\Model\Shippingcost::getShippingCost($itemPrice, $param, $autoData);
            if (!empty($shippingCostsArr)) {
                $shippingCost = $shippingCostsArr['totalShippingCost'];
                $insuranceCost = $shippingCostsArr['insurance'];
            }
            $processingFee = \App\Model\Procurementfees::getProcessingFee($warehouseId, $itemPrice);
            $pickupCost = \App\Model\Autopickupcost::getPickupCost($warehouseId, $fromCity);

            $totalCost = round(($processingFee + $pickupCost + $shippingCost + $itemPrice+$insuranceCost), 2);
            $results['processingFee'] = $processingFee;
            $results['pickupCost'] = $pickupCost;
            $results['shippingCost'] = $shippingCost;
            $results['totalCost'] = $totalCost;
            $results['currencyCode'] = $currencyCode;
            $results['defaultCurrency'] = $currencyCode;
            $results['currencySymbol'] = $currencySymbol;
            $results['exchangeRate'] = $exchangeRate;
            $results['insuranceCost'] = $insuranceCost;
            $results['isInsuranceCharged'] = 'Y';

            if(!empty($results)){
                return response()->json([
                'status' => '200',
                'data' => $results,
                'message' => 'success',
                ], 200);
            } else {
                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);
            }
            
        }
    }

    public function getshipmycarallcharges_mobile(Request $request) {

        if(empty($request->formData) || empty($request->userId) || empty($request->warehouseId)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $results = array();
            $formData = $request->formData;
            $userId = $request->userId;
            $warehouseId = $request->warehouseId;
            $itemPrice = "";
            $fromCity = "";
            $insuranceCost = "0.00";

            if ($formData['itemPrice'] != '')
                $itemPrice = $formData['itemPrice'];
            if ($formData['pickupCity'] != '')
                $fromCity = $formData['pickupCity'];

            $param = array(
                'fromCountry' => $formData['pickupCountry'],
                'fromState' => $formData['pickupState'],
                'fromCity' => $fromCity,
                'toCountry' => $formData['destinationCountry'],
                'toState' => $formData['destinationState'],
                'toCity' => $formData['destinationCity']
            );

            $autoData = array('make' => $formData['makeId'], 'model' => $formData['modelId']);
            $shippingCostsArr = \App\Model\Shippingcost::getShippingCost($itemPrice, $param, $autoData);
            if (!empty($shippingCostsArr)) {
                $shippingCost = $shippingCostsArr['totalShippingCost'];
                $insuranceCost = $shippingCostsArr['insurance'];
            }
            $pickupCost = \App\Model\Autopickupcost::getPickupCost($warehouseId, $fromCity);
            $totalCost = round(($pickupCost + $shippingCost + $insuranceCost), 2);

            $currencyCode = \App\Helpers\customhelper::getCurrencySymbolCode('', true);
            $currencySymbol = \App\Helpers\customhelper::getCurrencySymbolCode($currencyCode);
            $exchangeRate = 1;
            $results['currencyCode'] = $currencyCode;
            $results['defaultCurrency'] = $currencyCode;
            $results['currencySymbol'] = $currencySymbol;
            $results['exchangeRate'] = $exchangeRate;
            $results['pickupCost'] = $pickupCost;
            $results['shippingCost'] = $shippingCost;
            $results['totalCost'] = $totalCost;
            $results['insuranceCost'] = $insuranceCost;
            $results['isInsuranceCharged'] = 'Y';

            if(!empty($results)){ 
                return response()->json([
                'status' => '200',
                'data' => $results,
                'message' => 'success',
                ], 200);
            } else {
                return response()->json([
                    'status' => '200',
                    'data' => (object)[],
                    'message' => 'success',
                ], 200);
            }

        }
    }

    /*payment method list*/
    public function getpaymentmethodlist_mobile(Request $request) {
        
        if (empty($request->billingCountryId)) {
            
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else { 

            $paymentMethodList = \App\Model\Paymentmethod::getallpaymentmethods($request->billingCountryId);
            
            if(!empty($paymentMethodList)){

                return response()->json([
                'status' => '200',
                'data' =>$paymentMethodList,
                'message' => 'success'
                ], 200);

            } else {

                return response()->json([
                'status' => '200',
                'data' =>(object)[],
                'message' => 'success'
                ], 200);

            }
        }
    }

    public function getamountforpaystack_mobile(Request $request) {

        if(empty($request->payAmount) || empty($request->defaultCurrency)){

            return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'An error occured',
            ], 200);

        } else {

            $payAmount = round($request->payAmount, 2);
            $defaultCurrency = $request->defaultCurrency;
            if($defaultCurrency != 'NGN')
            {
              $exchangeRateToNaira = \App\Model\Currency::currencyExchangeRate($defaultCurrency, 'NGN');
              $convertedAmount = round(($payAmount * $exchangeRateToNaira), 2);
            }else{
              $convertedAmount = $payAmount;
            }
            $amountForPaystack = round($convertedAmount * 100, 2);
            
            return response()->json([
                    'status' => '200',
                    'data' => (object)['amountForPaystack' => $amountForPaystack],
                    'message' => 'success',
                ], 200);

        }

    }

    /*get location country list*/
    public function getlocationcountrylist_mobile() {
        
        $data = array();
        $location = new \App\Model\Location;
        $country = new Country;
        $locationTable = $location->table;
        $countryTable = $country->table;
        $data = DB::table($countryTable)->select(DB::raw("DISTINCT ".$country->prefix."$countryTable.id"),"$countryTable.*")->join("$locationTable","$countryTable.id","=","$locationTable.countryId")->where("$locationTable.status", "1")->where("$locationTable.deleted", "0")->get();
        if(!empty($data)){
            return response()->json([
            'status' => '200',
            'data' => $data,
            'message' => 'success'
                ], 200);

        } else {
            return response()->json([
            'status' => '200',
            'data' => (object)[],
            'message' => 'success'
                ], 200);
        }
    }

    /*get location details*/
    public function getlocationdetails_mobile(Request $request) {

        if(empty($request->countryId) || empty($request->cityId)){
            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);

        } else {

            $countryId = $request->countryId;
            $stateId = $request->stateId;
            $cityId = $request->cityId;
            $locationDetails = array();

            $where = "countryId=".$countryId." AND cityId=".$cityId;
            if($stateId!="")
                $where .= " AND stateId=".$stateId;
            $locationDetails = \App\Model\Location::whereRaw($where)
                                ->where('status','1')->where('deleted','0')->get();

            
            if($locationDetails->count()>0)
            {
                return response()->json([
                'status' => '200',
                'data' => $locationDetails,
                'message' => 'success'
                    ], 200);
            }
            else
            {
                return response()->json([
                'status' => '400',
                'data' => (object)[],
                'message' => 'No location found for selected country/state/city'
                    ], 200);
            }
        }

    }

    public function shipmenttrackingdetails_mobile(Request $request) {

        if(empty($request->orderNumber)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'Please provide order number!'
            ], 200);

        } else {

            $orderNumber = $request->orderNumber;
            $userId = $request->userId;
            $result = array();
            $orderData = \App\Model\Order::where('orderNumber', $orderNumber)->where('userId',$userId)->first();
            if (!empty($orderData)) {

                if ($orderData->type == 'shipment') {
                    $shipmentDeliveryStatus = \App\Model\Shipmentdelivery::deliveryStatus();
                    $shipemntDeliveryKeys = array_keys($shipmentDeliveryStatus);
                    $deliveryData = \App\Model\Shipmentdelivery::where('shipmentId', $orderData->shipmentId)->get();
                    $deliveryNum = 1;
                    if ($deliveryData->count() > 0) {
                        foreach ($deliveryData as $eachDeliveryData) {
                            $shippingMethodDetails = \App\Model\Shippingmethods::where('shippingid',$eachDeliveryData->shippingMethodId)->first();
                            if(!empty($shippingMethodDetails))
                                $itemArr = array('deliveryId' => $deliveryNum,'shippingMethod' => $shippingMethodDetails->shipping,'shippingMethodLogo'=>url('uploads/shipping/'.$shippingMethodDetails->companyLogo));
                            else
                                $itemArr = array('deliveryId' => $deliveryNum,'shippingMethod' => '','shippingMethodLogo'=>'');
                            $statusdata = array();
                            $shipmentStatusLog = \App\Model\Shipmentstatuslog::where('shipmentId', $orderData->shipmentId)->where('deliveryId', $eachDeliveryData->id)->get();
                            $statusCompletedIndex = 0;
                            if ($shipmentStatusLog->count() > 0) {
                                foreach ($shipmentStatusLog as $eachStatus) {
                                    $statusdata[] = array(
                                        'status' => $shipmentDeliveryStatus[$eachStatus->status],
                                        'date' => $eachStatus->updatedOn,
                                        'completed' => true,
                                        'visited' => true,
                                        'comment' => $eachStatus->comments,
                                    );
                                    $statusCompletedIndex++;
                                }
                            }
                            for($index = $statusCompletedIndex; $index < count($shipemntDeliveryKeys); $index++) {
                                $statusdata[] = array(
                                    'status' => $shipmentDeliveryStatus[$shipemntDeliveryKeys[$index]],
                                    'date' => '',
                                    'completed' => FALSE,
                                    'visited' => FALSE,
                                );
                            }
                            $itemArr['statusDetails'] = $statusdata;
                            array_push($result, $itemArr);
                            $deliveryNum++;
                        }
                        
                        
                    }


                } else if ($orderData->type == 'autoshipment') {
                
                    $shipmentDeliveryStatus = \App\Model\Autoshipment::allStatus();
                    $shipemntDeliveryKeys = array_keys($shipmentDeliveryStatus);
                    $itemArr = array('deliveryId' => '','shippingMethod'=>'','shippingMethodLogo'=>'');
                    $statusdata = array();
                    $shipmentStatusLog = \App\Model\Autoshipmentstatuslog::where('autoshipmentId', $orderData->shipmentId)->get();
                    $statusCompletedIndex = 0;
                    if ($shipmentStatusLog->count() > 0) {
                        foreach ($shipmentStatusLog as $eachStatus) {
                            $statusdata[] = array(
                                'status' => $shipmentDeliveryStatus[$eachStatus->status],
                                'date' => $eachStatus->updatedOn,
                                'completed' => true,
                                'visited' => true,
                                
                            );
                            $statusCompletedIndex++;
                        }
                    }
                    for ($index = $statusCompletedIndex; $index < count($shipemntDeliveryKeys); $index++) {
                        $statusdata[] = array(
                            'status' => $shipmentDeliveryStatus[$shipemntDeliveryKeys[$index]],
                            'date' => '',
                            'completed' => FALSE,
                            'visited' => FALSE,
                        );
                    }
                    $itemArr['statusDetails'] = $statusdata;
                    array_push($result, $itemArr);
                }
            
                if ($orderData->type == 'fillship') {
                    $itemArr['statusDetails'] = \App\Model\Fillshipshipmentstatuslog::getStatusLog($orderData->shipmentId);
                    array_push($result, $itemArr);
                }
                if (!empty($result)) {

                    return response()->json([
                        'status' => '200',
                        'data' => $result,
                        'message' => 'success'
                            ], 200);

                } else {

                    return response()->json([
                    'status' => '400',
                    'data' => (object)[],
                    'message' => 'No shipment details found!'
                        ], 200);
                }

            } else {

               return response()->json([
                'status' => '400',
                'data' => null,
                'message' => 'Invalid order number!'
                ], 200);
             
            }

        }
    }

    public function itemarriveddetails_mobile(Request $request) {

        if(empty($request->trackingNumber)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured'
            ], 200);


        } else {

            $trackingNumber = $request->trackingNumber;
            $itemarrivalDate = $request->itemarrivalDate;
            if(!empty($request->rangeDate))
            $rangeDate = $request->rangeDate;
            $where = "1";
            $result = array();
            $shipmenttracking = new \App\Model\Shipmenttrackingnumber;
            $shipmenttrackingTable = $shipmenttracking->prefix . $shipmenttracking->table;
            if ($itemarrivalDate == 'TW'){                    
                $where .= "  AND $shipmenttrackingTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            } else if ($itemarrivalDate == 'TO'){                   
                $where .= "  AND date($shipmenttrackingTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            } else if (isset($rangeDate)) {
                //$searchDate = explode('-',$rangeDate);
                $searchDate = $rangeDate;
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($shipmenttrackingTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            } else { 
                // $where .= "  AND MONTH($shipmenttrackingTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";

                $where .= "  AND date($shipmenttrackingTable.createdOn) BETWEEN DATE_SUB('".\Carbon\Carbon::now()."' , INTERVAL 30 DAY) AND '".\Carbon\Carbon::now()."'";
            }
            $shipmentArrivedDetails = \App\Model\Shipmenttrackingnumber::where('trackingNumber','LIKE',"%$trackingNumber%")->whereRaw($where)->first();

            if (!empty($shipmentArrivedDetails)) {

                $trackingNumberQ = '"'.$trackingNumber.'"';

                //Search for the item in quick ship out which are in shipment tracking also

                $where = "1";
                $quicktracking = new \App\Model\Commercialinvoice;
                $quicktrackingTable = $quicktracking->prefix . $quicktracking->table;
                if ($itemarrivalDate == 'TW'){
                   $where .= "  AND $quicktrackingTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'"; 
                } else if ($itemarrivalDate == 'TO'){                    
                    $where .= "  AND date($quicktrackingTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                } else if (isset($rangeDate)) {
                    //$searchDate = explode('-',$rangeDate);
                    $searchDate = $rangeDate;
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($quicktrackingTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }  else {
                    
                   // $where .= "  AND MONTH($quicktrackingTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                    $where .= "  AND date($quicktrackingTable.createdOn) BETWEEN DATE_SUB('".\Carbon\Carbon::now()."' , INTERVAL 30 DAY) AND '".\Carbon\Carbon::now()."'";
                    
                }
                $itemInQuickshipout = \App\Model\Commercialinvoice::where('invoiceDetails','LIKE',"%$trackingNumberQ%")->whereRaw($where)->first();

                if(!empty($itemInQuickshipout))
                {
                    //Find Quick shipout and get details of log

                    $quickShipDetails[0]['quickshipoutId'] = $itemInQuickshipout->id;

                    $details = \App\Model\Quickshipoutstatuslog::where('commercialInvoiceId', $itemInQuickshipout->id)->groupBy('status')->orderBy('id')->get();

                    //$invoicedetails = $itemInQuickshipout->invoiceDetails;

                    $invoicedetails = json_decode($itemInQuickshipout->invoiceDetails);

                    //$quickShipDetails[0]['packages'] = $invoicedetails[0]->packages;

                    $invoice = $invoicedetails[0]->packages;


                    $quickShipDetails[0]['packages'] =array();

                   for($i=0; $i<count($invoice); $i++)
                    { 
                        if(strtolower($invoice[$i]->tracking) == strtolower($trackingNumber))
                         {
                            $quickShipDetails[0]['packages'][0] = $invoice[$i];
                         }
                         if(strtolower($invoice[$i]->unit) == strtolower($trackingNumber))
                         {
                            $quickShipDetails[0]['packages'][] = $invoice[$i];
                         }
                    }

                    $quickShipDetails[0]['details'] = $details;
                    $shipmentDeliveryStatus = \App\Model\Shipmentdelivery::deliveryStatusQuickShipout();
                    $quickShipDetails[0]['shipemntDeliveryKeys'] = array_keys($shipmentDeliveryStatus);

                    $statusCompletedIndex = 0;
                    if ($details->count() > 0) {
                        foreach ($details as $eachStatus) {
                            $quickShipDetails[0]['statusdata'][] = array(
                                'status' => $shipmentDeliveryStatus[$eachStatus->status],
                                'date' => $eachStatus->updatedOn,
                                'completed' => true,
                                'visited' => true,
                                'comment' => $eachStatus->comments,
                            );
                            $statusCompletedIndex++;
                        }
                    }
                    for ($index = $statusCompletedIndex; $index < count($quickShipDetails[0]['shipemntDeliveryKeys']); $index++) {
                        $quickShipDetails[0]['statusdata'][] = array(
                            'status' => $shipmentDeliveryStatus[$quickShipDetails[0]['shipemntDeliveryKeys'][$index]],
                            'date' => '',
                            'completed' => FALSE,
                            'visited' => FALSE,
                        );
                    }

                    //$itemArr['statusDetails'] = $statusdata;
                    array_push($result, $quickShipDetails);

                    return response()->json([
                        'status' => '200',
                        'data' => $result,
                        'message' => 'success'
                    ], 200);


                }   else{

                    return response()->json([
                        'status' => '200',
                        'data' => \Carbon\Carbon::parse($shipmentArrivedDetails->createdOn)->format('m-d-Y h:i a'),
                        'message' => 'success'
                    ], 200);

                }
                
            } else {

              
                $where = "1";
                //Search for the item in quick ship out which are not in shipment tracking
                $quicktracking = new \App\Model\Commercialinvoice;
                $quicktrackingTable = $shipmenttracking->prefix . $quicktracking->table;
                if ($itemarrivalDate == 'TW'){
                  $where .= "  AND $quicktrackingTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";   
                } else if ($itemarrivalDate == 'TO'){
                    $where .= "  AND date($quicktrackingTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";              
                } else if (isset($rangeDate)) {
                   // $searchDate = explode('-',$rangeDate);
                    $searchDate = $rangeDate;
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($quicktrackingTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                } else {
                    //$where .= "  AND MONTH($quicktrackingTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                    $where .= "  AND date($quicktrackingTable.createdOn) BETWEEN DATE_SUB('".\Carbon\Carbon::now()."' , INTERVAL 30 DAY) AND '".\Carbon\Carbon::now()."'";
                
                }
                //DB::enableQueryLog();
                $itemInQuickshipout = \App\Model\Commercialinvoice::where('invoiceDetails','LIKE','%\"'.$trackingNumber.'\"%')->whereRaw($where)->orderBy('id', 'desc')->get();
                //dd(DB::getQueryLog());
                //print_r($itemInQuickshipout); die;

                if(!empty($itemInQuickshipout) && count($itemInQuickshipout)>0)
                {
                    //Find Quick shipout and get details of log

                    foreach($itemInQuickshipout as $key => $quickship)
                    {

                        $quickShipDetails[$key]['quickshipoutId'] = $quickship->id;
                        $quickShipDetails[$key]['packages'] = array();
                        $invoicedetails = json_decode($quickship->invoiceDetails);
                        $invoice = $invoicedetails[0]->packages;
                        for($i=0; $i<count($invoice); $i++)
                        {
                           if(strtolower($invoice[$i]->unit) == strtolower($trackingNumber))
                           {
                            $quickShipDetails[$key]['packages'][] = $invoice[$i];

                            }
                            if(strtolower($invoice[$i]->tracking) == strtolower($trackingNumber))
                             {

                                $quickShipDetails[$key]['packages'][] = $invoice[$i];
                             }
                        }
                        $details = \App\Model\Quickshipoutstatuslog::where('commercialInvoiceId', $quickship->id)->groupBy('status')->orderBy('id')->get();
                        $quickShipDetails[$key]['details'] = $details;
                        $shipmentDeliveryStatus = \App\Model\Shipmentdelivery::deliveryStatusQuickShipout();
                    
                        $quickShipDetails[$key]['shipemntDeliveryKeys'] = array_keys($shipmentDeliveryStatus);
                        $statusCompletedIndex = 0;

                        if ($details->count() > 0) {
                            foreach ($details as $eachStatus) {
                                $quickShipDetails[$key]['statusdata'][] = array(
                                    'status' => $shipmentDeliveryStatus[$eachStatus->status],
                                    'date' => $eachStatus->updatedOn,
                                    'completed' => true,
                                    'visited' => true,
                                    'comment' => $eachStatus->comments,
                                );
                                $statusCompletedIndex++;
                            }
                        }

                        for ($index = $statusCompletedIndex; $index < count($quickShipDetails[$key]['shipemntDeliveryKeys']); $index++) {
                            $quickShipDetails[$key]['statusdata'][] = array(
                                'status' => $shipmentDeliveryStatus[$quickShipDetails[$key]['shipemntDeliveryKeys'][$index]],
                                'date' => '',
                                'completed' => FALSE,
                                'visited' => FALSE,
                            );
                        }

                   
                    }
                    array_push($result, $quickShipDetails);

                    return response()->json([
                        'status' => '200',
                        'data' => $result,
                        'message' => 'success'
                    ], 200);

                } else {

                    return response()->json([
                        'status' => '200',
                        'data' => (object)[],
                        'message' => 'success'
                    ], 200);

                }
                
            }
        }
    }

    public function getamountforpayeezy_mobile(Request $request) {
        
        if(empty($request->payAmount) || empty($request->defaultCurrency)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        } else {

            $payAmount = round($request->payAmount, 2);
            $defaultCurrency = $request->defaultCurrency;
            
            if($defaultCurrency != 'USD')
            {
               $exchangeRateToNaira = \App\Model\Currency::currencyExchangeRate($defaultCurrency, 'USD');
               $convertedAmount = ($payAmount * $exchangeRateToNaira); 
            }else{
                $convertedAmount = $payAmount;
            }
            
            $amountDisplay = round($convertedAmount,2);
            $amountForPayeezy = round($amountDisplay*100);
            //$amountForPayeezy = $amountDisplay*100;
            return response()->json([
                'status' => '200',
                'data' => array('amountForPayeezy' => $amountForPayeezy, 'amountDisplay' => $amountDisplay),
                'message' => 'success'
            ], 200);

        }
    }

    public function verifyitemarrivedtime_mobile(Request $request) {
        
        if(empty($request->deliveryDate)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        } else {

            $deliveryDate = date('Y-m-d',strtotime($request->deliveryDate));

            $fullDate = strtotime($deliveryDate);
            $difference = floor((time()-$fullDate)/3600);
            if($difference>24)
            {

                return response()->json([
                    'status' => '200',
                    'data' => (object)array('action'=>'showform'),
                    'message' => 'success'
                ], 200);
            }
            else
            {
                return response()->json([
                    'status' => '200',
                    'data' => (object)array('action'=>'wait'),
                    'message' => 'success'
                ], 200);
            }
        }
    }

    public function verifytrackingdetails_mobile(Request $request) {
        
        if(empty($request->customerName) || empty($request->customerEmail) || empty($request->customerUnit) || empty($request->customerPurchaseStore) || empty($request->customerTracking) || empty($request->invoiceFile) || empty($request->itemPhotoFile)){

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        } else {

            $name = $request->customerName;
            $email = $request->customerEmail;
            $unit = $request->customerUnit;
            $store = $request->customerPurchaseStore;
            $tracking = $request->customerTracking;
            $photoFilePath = $request->itemPhotoFile;
            $invFilePath = $request->invoiceFile;
            
            $content = "<p>Dear Admin,</p>";
            $content.= "<p>The following customers items have not been updated since 48 hours. Please update immediately.</p>";
            $content.= "<p>Name – $name</p>";
            $content.= "<p>Email – $email</p>";
            $content.= "<p>Unit Number – $unit</p>";
            $content.= "<p>Store of Purchase – $store</p>";
            $content.= "<p>Tracking Number – $tracking</p>";
            $from = $email;
            $to = "contact@shoptomydoor.com";
            $subject = "Update Delay From Customer";
            $mailSent = Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($subject,$from, $to,$photoFilePath,$invFilePath) {
                $message->from($from, $from);
                $message->subject($subject);
                $message->to($to);
                if($photoFilePath!="")
                    $message->attach($photoFilePath);
                if($invFilePath != "")
                    $message->attach($invFilePath);
            });

            return response()->json([
                'status' => '200',
                'data' => null,
                'message' => 'success'
            ], 200);
            
        }

    }

    public function verifytrackingdetailsfile_mobile(Request $request) {
        
        if ($request->hasFile('invoiceFile') || $request->hasFile('itemPhotoFile')) {

            $photoFilePath = "";
            $invFilePath = "";
            
            if ($request->hasFile('invoiceFile')) {
                
                $invFile = $request->file('invoiceFile');
                $invFileName = time() . '_' . str_replace(" ", "_", $invFile->getClientOriginalName());
                $invDestinationPath = public_path('/uploads/verifytracking');
                $invFilePath = $invDestinationPath."/".$invFileName;
                $invFile->move($invDestinationPath, $invFileName);
            }
            
            if ($request->hasFile('itemPhotoFile')) {
                
                $photoFile = $request->file('itemPhotoFile');
                $photoFileName = time() . '_' . str_replace(" ", "_", $photoFile->getClientOriginalName());
                $photoDestinationPath = public_path('/uploads/verifytracking');
                $photoFilePath = $photoDestinationPath."/".$photoFileName;
                $photoFile->move($photoDestinationPath, $photoFileName);
            } 

            return response()->json([
                'status' => '200',
                'data' => (object)array('invoiceFile'=>$invFilePath,'itemPhotoFile'=>$photoFilePath),
                'message' => 'success'
            ], 200);
            
        } else {

            return response()->json([
            'status' => '400',
            'data' => null,
            'message' => 'An error occured!'
            ], 200);

        }

    }

}
