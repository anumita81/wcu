<?php

namespace App\Http\Controllers\Api;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Usercart;
use App\Model\Fillship;
use App\Model\Fillshipbox;
use App\Model\Fillshipitem;
use App\Model\Fillshipothercharges;
use App\Model\Fillshipitemstatus;
use App\Model\Fillshipshipmentstatuslog;
use App\Model\Shippingcharges;
use App\Model\Paymenttransaction;
use Config;
use Auth;
use customhelper;
use DB;
use PDF;
use Mail;
use Carbon\Carbon;

class FillshipController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->_perPage = 10;
    }

    /**
     * Method used to find my box
     * @param integer $userId
     * @param object $request
     * @return boolean
     */
    public function findmybox($userId, Request $request) {

        $totalChargeableWeight = $totalQuantity = 0;
        $param = $shippingMethodData = array();

        /* Fetch User Addressbook Details */
        $addressBookData = User::find($userId)->addressbook()->where('isDefaultShipping', '1')->first();

        /* Fetch Warehouse Details */
        $warehouseData = \App\Model\Warehouse::find($request->warehouseId);

        /* Fetch User Details */
        $userData = User::find($userId);

        $totalItemCost = customhelper::getExtractCurrency($request->paymentDetails['totalItemCost']);

        if (!empty($request->items)) {
            foreach ($request->items as $item) {
                if (!empty($item['siteProductId'])) {
                    $siteProduct = \App\Model\Siteproduct::find($item['siteProductId']);
                    $totalChargeableWeight += ($siteProduct->weight * $item['itemQuantity']);
                }

                $totalQuantity += $item['itemQuantity'];
            }
        }

        /*  FETCH RECOMMENDED BOX DETAILS */
        $boxDetails = Fillshipbox::where('chargeableWeight', '>=', $totalChargeableWeight)
                ->where('deleted', '0')
                ->orderby('chargeableWeight', 'ASC')
                ->first();

        if (!empty($boxDetails)) {
            $boxDetails = $boxDetails->toArray();
            $boxDetails['imagePath'] = asset('uploads/fillship');
            $boxChargeableWeight = $boxDetails['chargeableWeight'];

            /*  FETCH  SHIPPING METHOD CHARGES DETAILS */
            $param = array(
                'userId' => $userId,
                'fromCountry' => $warehouseData->countryId,
                'fromState' => $warehouseData->stateId,
                'fromCity' => $warehouseData->cityId,
                'toCountry' => $addressBookData->countryId,
                'toState' => $addressBookData->stateId,
                'toCity' => $addressBookData->cityId,
                'totalWeight' => $boxChargeableWeight,
                'totalProcurementCost' => $totalItemCost,
            );
            $shippingMethodCharges = \App\Model\Shippingmethods::calculateShippingMethodCharges($param);

            if (!empty($shippingMethodCharges)) {
                foreach ($shippingMethodCharges as $key => $row) {
                    foreach ($row as $field => $value) {
                        if ($field == 'companyLogo') {
                            $shippingMethodData[$key][$field] = asset('uploads/shipping/' . $value);
                        } else if ($field == 'shippingCost' || $field == 'duty' || $field == 'clearing' || $field == 'clearingDuty' || $field == 'totalShippingCost') {
                            $shippingMethodData[$key][$field] = customhelper::getCurrencySymbolFormat($value);
                        } else if ($field == 'days') {
                            $shippingMethodData[$key][$field] = $value;
                            $shippingMethodData[$key]['estimatedDeliveryDate'] = \Carbon\Carbon::now()->addWeekDays($value)->format('D, M dS Y');
                        } else {
                            $shippingMethodData[$key][$field] = $value;
                        }
                    }
                }
            }

            $boxDetails['boxFillPercentage'] = round(($totalChargeableWeight / $boxChargeableWeight) * 100, 2);



            return response()->json([
                        'status' => '1',
                        'results' => 'success',
                        'boxDetails' => $boxDetails,
                        'shippingCharges' => $shippingMethodData
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => 'error',
            ]);
        }
    }

    public function fillshipselectbox($userId, Request $request) {

        $totalChargeableWeight = $totalQuantity = 0;
        $param = $shippingMethodData = array();

        /* Fetch User Addressbook Details */
        $addressBookData = User::find($userId)->addressbook()->where('isDefaultShipping', '1')->first();

        /* Fetch Warehouse Details */
        $warehouseData = \App\Model\Warehouse::find($request->warehouseId);

        /* Fetch User Details */
        $userData = User::find($userId);

        $boxDetails = Fillshipbox::where('deleted', '0')->where('id', $request->boxId)->first();
        if (!empty($boxDetails)) {
            $boxDetails = $boxDetails->toArray();
            $boxDetails['imagePath'] = asset('uploads/fillship');
            $boxChargeableWeight = $boxDetails['chargeableWeight'];
            $param = array(
                'userId' => $userId,
                'fromCountry' => $warehouseData->countryId,
                'fromState' => $warehouseData->stateId,
                'fromCity' => $warehouseData->cityId,
                'toCountry' => $addressBookData->countryId,
                'toState' => $addressBookData->stateId,
                'toCity' => $addressBookData->cityId,
                'totalWeight' => $boxChargeableWeight,
                'totalProcurementCost' => '0',
            );
            $shippingMethodCharges = \App\Model\Shippingmethods::calculateShippingMethodCharges($param);

            if (!empty($shippingMethodCharges)) {
                foreach ($shippingMethodCharges as $key => $row) {
                    foreach ($row as $field => $value) {
                        if ($field == 'companyLogo') {
                            $shippingMethodData[$key][$field] = asset('uploads/shipping/' . $value);
                        } else if ($field == 'shippingCost' || $field == 'duty' || $field == 'clearing' || $field == 'clearingDuty' || $field == 'totalShippingCost') {
                            $shippingMethodData[$key][$field] = customhelper::getCurrencySymbolFormat($value);
                        } else if ($field == 'days') {
                            $shippingMethodData[$key][$field] = $value;
                            $shippingMethodData[$key]['estimatedDeliveryDate'] = \Carbon\Carbon::now()->addWeekDays($value)->format('D, M dS Y');
                        } else {
                            $shippingMethodData[$key][$field] = $value;
                        }
                    }
                }
            }

            return response()->json([
                        'status' => '1',
                        'results' => 'success',
                        'boxDetails' => $boxDetails,
                        'shippingCharges' => $shippingMethodData
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => 'error',
            ]);
        }
    }

    /**
     * Method used to submit fill and ship data
     * @param integer $userId
     * @param object $request
     * @return boolean
     */
    public function submitfillshipdata($userId, Request $request) {

        $totalWeight = 0;
        $shippingId = (int) $request->shippingId;
        $param = array();

        /* Fetch User Addressbook Details */
        $addressBookData = User::find($userId)->addressbook()->where('isDefaultShipping', '1')->first();

        /* Fetch Warehouse Details */
        $warehouseData = \App\Model\Warehouse::find($request->warehouseId);

        /* Fetch User Details */
        $userData = User::find($userId);
        $totalCost = $totalItemCost = "0.00";
        if (!empty($request->paymentDetails['totalCost']))
            $totalCost = customhelper::getExtractCurrency($request->paymentDetails['totalCost']);
        if (!empty($request->paymentDetails['totalItemCost']))
            $totalItemCost = customhelper::getExtractCurrency($request->paymentDetails['totalItemCost']);
        $fillship = $fillship['items'] = array();
        $totalQuantity = 0;

        $fillship['isCurrencyChanged'] = 'N';
        $fillship['exchangeRate'] = 1;
        $fillship['defaultCurrencySymbol'] = $fillship['currencySymbol'] = customhelper::getCurrencySymbolCode();
        $fillship['defaultCurrencyCode'] = $fillship['currencyCode'] = customhelper::getCurrencySymbolCode('', true);
        if (!empty($request->items)) {
            foreach ($request->items as $key => $item) {
                if (!empty($item['siteProductId'])) {
                    $siteProduct = \App\Model\Siteproduct::find($item['siteProductId']);
                    $totalWeight += ($siteProduct->weight * $item['itemQuantity']);
                }

                $totalQuantity += $item['itemQuantity'];

                /* SET DATA INTO SESSION */
                $fillship['items'][$key]['siteProductName'] = $item['siteProductName'];
                $fillship['items'][$key]['siteCategoryId'] = $item['siteCategoryId'];
                $fillship['items'][$key]['siteSubCategoryId'] = $item['siteSubCategoryId'];
                $fillship['items'][$key]['siteProductId'] = $item['siteProductId'];
                $fillship['items'][$key]['itemPrice'] = customhelper::getExtractCurrency($item['itemPrice']);
                $fillship['items'][$key]['itemQuantity'] = $item['itemQuantity'];
                $fillship['items'][$key]['itemWeight'] = $siteProduct->weight;
                $fillship['items'][$key]['itemTotalCost'] = customhelper::getExtractCurrency($item['itemTotalCost']);
                $fillship['items'][$key]['itemImage'] = isset($item['itemImage']) ? $item['itemImage'] : '';
            }
        }

        $fillship['data']['warehouseId'] = $request->warehouseId;
        $fillship['data']['totalItemCost'] = $totalItemCost;
        $fillship['data']['totalQuantity'] = $totalQuantity;
        $fillship['data']['totalWeight'] = $totalWeight;
        $fillship['data']['isInsuranceCharged'] = 'N';
        $fillship['data']['totalTax'] = 0;
        $fillship['data']['totalDiscount'] = 0;
        $fillship['data']['totalBDiscountCost'] = 0;

        if ($shippingId > 0) {
            $param['userId'] = $userId;
            $param['totalWeight'] = $request->boxDetails['chargeableWeight'];
            $param['totalProcurementCost'] = $totalItemCost;
            $param['toCountry'] = $addressBookData->countryId;
            $param['toState'] = $addressBookData->stateId;
            $param['toCity'] = $addressBookData->cityId;
            $param['fromCountry'] = $warehouseData->countryId;
            $param['fromState'] = $warehouseData->stateId;
            $param['fromCity'] = $warehouseData->cityId;
            $param['shippingId'] = $shippingId;
            $fillship['data']['shipmentMethodId'] = $shippingId;

            $shippingChargeData = Shippingcharges::getShippingChargeData($param);

            $shippingMethod = \App\Model\Shippingmethods::where('shippingid', $shippingId)->first();
            if (!empty($shippingChargeData)) {
                $shippingChargeDetails = Shippingcharges::calculateShippingCharge($param, $shippingChargeData);

                $fillship['data']['shippingCost'] = $shippingChargeDetails['shippingCost'];
                $fillship['data']['totalShippingCost'] = $shippingChargeDetails['shippingCost'];
                $fillship['data']['totalInsurance'] = $shippingChargeDetails['insurance'];
                $fillship['data']['isInsuranceCharged'] = 'N';
                $fillship['data']['shippingMethodName'] = $shippingMethod->shipping;
                $fillship['data']['estimateDeliveryDate'] = \Carbon\Carbon::now()->addWeekDays($shippingMethod->days)->format('Y-m-d');

                $totalCost = $shippingChargeDetails['shippingCost'];
            }
        }

        $fillship['data']['totalClearingDuty'] = $request->boxDetails['clearingAndDuty'];
        $fillship['data']['otherCharge'] = $request->boxDetails['warehousing'];
        $fillship['data']['boxChargeableWeight'] = $request->boxDetails['chargeableWeight'];
        $fillship['data']['boxId'] = $request->boxDetails['id'];
        $fillship['data']['boxName'] = $request->boxDetails['name'];
        $fillship['data']['boxLength'] = $request->boxDetails['length'];
        $fillship['data']['boxHeight'] = $request->boxDetails['height'];
        $fillship['data']['boxWidth'] = $request->boxDetails['width'];
        $totalCost = $totalCost + $request->boxDetails['clearingAndDuty'] + $request->boxDetails['warehousing'];
        $fillship['data']['totalCost'] = round($totalCost, 2);

        return response()->json([
                    'status' => '1',
                    'results' => 'success',
                    'fillship' => $fillship,
        ]);
    }

    public function updatefillshipdata($userId, Request $request) {
        $fillship = array();
        $fromCurrency = $request->fromCurrency;
        $toCurrency = $request->toCurrency;

        $deafultCurrency = customhelper::getCurrencySymbolCode('', true);

        $fillship['defaultCurrencySymbol'] = customhelper::getCurrencySymbolCode();
        $fillship['defaultCurrencyCode'] = $deafultCurrency;
        $fillship['isCurrencyChanged'] = ($request->toCurrency != $deafultCurrency) ? 'Y' : 'N';
        $fillship['currencySymbol'] = customhelper::getCurrencySymbolCode($request->toCurrency);
        $fillship['currencyCode'] = $request->toCurrency;
        $fillship['exchangeRate'] = $exchangeRate = \App\Model\Currency::currencyExchangeRate($deafultCurrency, $request->toCurrency);

        return response()->json([
                    'status' => '1',
                    'fillship' => $fillship,
        ]);
    }

    /**
     * Method used to get the current week
     * @return string
     */
    public static function getCurrentWeek() {
        $monday = strtotime("last monday");
        $monday = date('w', $monday) == date('w') ? $monday + 7 * 86400 : $monday;

        $sunday = strtotime(date("Y-m-d", $monday) . " +6 days");

        $this_week_sd = date("Y-m-d", $monday);
        $this_week_ed = date("Y-m-d", $sunday);
        return "'" . $this_week_sd . "%' And '" . $this_week_ed . "%'";
    }

    /**
     * Method used to validate coupon code
     * @param request $request
     * @return string
     */
    public static function validatecouponcode(Request $request) {

        /* INITALIZE THE VARIABLE AS FALSE */
        $match = false;

        /* GET THE COUPON CODE */
        $code = $request->coupon;

        /* FETCH USER DETAILS */
        $userId = $request->userId;
        $userDetails = \App\Model\User::where("id", $userId)->first();

        /* VALIDATE THE COUPON CODE */
        $currDate = date('m/d/Y');

        /* GET THE LOG */
        $couponLog = \App\Model\Couponlog::where('userId', $userId)->where('code', $code)->count();
        if ($couponLog == 1) {
            return response()->json([
                        'status' => '0',
                        'results' => 'invalid',
                        'message' => 'Coupon already used'
            ]);
        }

        /* GET THE OFFER ID */
        $offerIdQuery = \App\Model\Offer::where('status', '1')->where('deleted', '0')->where('couponCode', $code)->get();

        if (count($offerIdQuery) == 0) {
            return response()->json([
                        'status' => '0',
                        'results' => 'invalid',
                        'message' => 'Invalid coupon'
            ]);
        }

        $offerCondQuery = \App\Model\Offercondition::where('offerId', $offerIdQuery[0]->id)->whereIn('keyword', array('new_customer_reg_from_app', 'user_subscription'))->count();

        if ($offerCondQuery == 0) {
            /* QUERY FOR NOT REGISTERED BY APP */
            $where = "'$currDate' between offerRangeDateFrom and offerRangeDateTo";
            $ifCodeExist = \App\Model\Offer::where('status', '1')->where('deleted', '0')->where('couponCode', $code)->whereRaw($where)->get();
        } else {
            /* QUERY FOR REGISTERED BY APP */
            $ifCodeExist = \App\Model\Offer::where('status', '1')->where('deleted', '0')->where('couponCode', $code)->get();
        }

        if (count($ifCodeExist) == 0) {
            return response()->json([
                        'status' => '0',
                        'results' => 'invalid',
                        'message' => 'Invalid coupon'
            ]);
        } else {
            /* COUPON CODE IS VALID, DO SEARCH TO CHECK CONDITIONS */
            $offerId = $ifCodeExist[0]->id;

            /* IF REGISTERED BY APP */
            if ($offerCondQuery > 0) {
                if ($ifCodeExist[0]->keyword == 'new_customer_reg_from_app') {
                    $createdOn = explode(" ", $userDetails->createdOn);
                    $date1 = date_create($createdOn[0]);
                    $date2 = date_create(date('Y-m-d'));
                    $diff = date_diff($date1, $date2);
                    $exactDay = $diff->format("%d");

                    $getOffer = \App\Model\Offer::where('id', $offerIdQuery[0]->id)->first();
                    if ($exactDay > $getOffer->offerRangeDateFrom) {
                        return response()->json([
                                    'status' => '0',
                                    'results' => 'invalid',
                                    'message' => 'Invalid coupon'
                        ]);
                        exit;
                    } else {
                        //Nothing
                    }
                }
            }

            /* GET THE CONDITIONS */
            $offerConditions = \App\Model\Offercondition::where('offerId', $offerId)->orderby('id')->get();

            /* GET THE CUSTOMER ADDRESS DETAILS */
            $userAddress = \App\Model\Addressbook::where("userId", $userId)->where('deleted', '0')->get();

            /* GET THE CART ITEMS ARRAY */
            foreach ($offerConditions as $keyO => $valO) {

                /* CONDITION 3 */
                if ($valO->keyword == "discount_on_total_amount_of_shipping") {

                    $offerConditionByKeyword = \App\Model\Offercondition::where('offerId', $offerId)
                            ->where('keyword', 'discount_on_total_amount_of_shipping')
                            ->get();
                    $OfferCond = $offerConditionByKeyword[0]->jsonValues;
                    $decodedValues = json_decode($OfferCond);

                    /* GET THE SHIPPING AMOUNT */
                    $shippingAmount = $totalWeight = $request->usercart['data']['totalShippingCost'];

                    if ($shippingAmount == null) {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    }


                    if ($shippingAmount >= $decodedValues->shipping_cost->from && $shippingAmount <= $decodedValues->shipping_cost->to) {
                        /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                        $match = true;
                    } else {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    }
                }

                /* CONDITION 4 */
                if ($valO->keyword == "customer_has_not_purchased_any_goods_for_a_certain_timeframe") {

                    $offerConditionByKeyword = \App\Model\Offercondition::where('offerId', $offerId)
                            ->where('keyword', 'customer_has_not_purchased_any_goods_for_a_certain_timeframe')
                            ->get();
                    $OfferCond = $offerConditionByKeyword[0]->jsonValues;
                    $decodedValues = json_decode($OfferCond);

                    /* GET THE DATES FROM JSON DECODE */
                    $from = date_format(date_create($decodedValues->user_not_purchased->from), "Y-m-d");
                    $to = date_format(date_create($decodedValues->user_not_purchased->to), "Y-m-d");

                    /* GET THE LAST DATE OF THE PURCHASE */
                    $lastOrder = \App\Model\Order::where('userId', $userId)->where('status', '5')->orderby('id', 'desc')->first();
                    if (!empty($lastOrder)) {
                        $notPurchasedFrom = data('Y-m-d', strtotime($lastOrder->createdDate));

                        /* CALCULATE PART */
                        if (( $notPurchasedFrom >= $from ) && ( $notPurchasedFrom <= $to )) {
                            /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                            $match = false;
                            break;
                        } else {
                            /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                            $match = true;
                        }
                    }
                }

                /* CONDITION 5 */
                if ($valO->keyword == "customer_purchased_an_item_from_particular_store") {

                    /* GET THE STORE IDS FROM CART */
                    foreach ($request->usercart['items'] as $keycart => $valCart) {
                        $storeId[] = "'[" . '"' . $valCart['storeId'] . '"' . "]'";
                    }

                    $query = \App\Model\Offercondition::where('offerId', $offerId)->where('keyword', 'customer_purchased_an_item_from_particular_store');

                    $query->where(function($q) use ($storeId) {
                        foreach ($storeId as $keyQ => $condition) {
                            if ($keyQ == 0) {
                                $q->whereRaw('json_contains(jsonValues, ' . $condition . ', \'$.store_id\')');
                            } else {
                                $q->orWhereRaw('json_contains(jsonValues, ' . $condition . ', \'$.store_id\')');
                            }
                        }
                    });

                    if (count($query->get()) > 0) {
                        /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                        $match = true;
                    } else {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    }
                }


                /* CONDITION 6 */
                if ($valO->keyword == "discount_on_total_weight_of_shipping") {

                    /* MATCH THE WEIGHT WHICH MAY FALL UNDER THE RANGE */
                    $totalWeight = $request->usercart['data']['totalWeight'];

                    $offerConditionByKeyword = \App\Model\Offercondition::where('offerId', $offerId)
                            ->where('keyword', 'discount_on_total_weight_of_shipping')
                            ->get();
                    $OfferCond = $offerConditionByKeyword[0]->jsonValues;
                    $decodedValues = json_decode($OfferCond);


                    if ($totalWeight >= $decodedValues->shipping_weight->from && $totalWeight <= $decodedValues->shipping_weight->to) {
                        /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                        $match = true;
                    } else {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    }
                }

                /* CONDITION 7 */
                if ($valO->keyword == "discount_on_total_weight_of_shipping_for_any_specific_customer") {
                    $createdOn = date('Y-m-d', strtotime($userDetails->createdOn));

                    $date1 = date_create($createdOn[0]);
                    $date2 = date_create(date('Y-m-d'));
                    $diff = date_diff($date1, $date2);

                    /* GET THE ACTUAL MONTH */
                    $exactMonth = $diff->format("%m");

                    /* GET THE MONTH DEFINED IN THE CONDITION */
                    $offerConditionByKeyword = \App\Model\Offercondition::where('offerId', $offerId)
                            ->where('keyword', 'discount_on_total_weight_of_shipping_for_any_specific_customer')
                            ->get();
                    $monthOffer = $offerConditionByKeyword[0]->jsonValues;
                    $decodedValues = json_decode($monthOffer);
                    $duration = $decodedValues->shipping_weight_for_specific_user->duration;

                    if ($exactMonth >= $duration) {
                        /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                        $match = true;

                        /* NOW MATCH THE WEIGHT WHICH MAY FALL UNDER THE RANGE */
                        $totalWeight = $request->usercart['data']['totalWeight'];
                        if ($totalWeight >= $decodedValues->shipping_weight_for_specific_user->from && $totalWeight <= $decodedValues->shipping_weight_for_specific_user->to) {
                            /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                            $match = true;
                        } else {
                            /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                            $match = false;
                            break;
                        }
                    } else {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    }
                }

                /* CONDITION 8 */
                if ($valO->keyword == "customer_is_using_a_particular_shipping_method") {

                    /* GET THE SHIPPING METHOD ID */
                    $id = $request->usercart['data']['shipmentMethodId'];

                    /* IF NO METHOD FOUND, EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                    if ($id == null) {
                        $match = false;
                        break;
                    } else {
                        $shippingMethidId = '["' . $id . '"]';
                    }

                    $query = \App\Model\Offercondition::where('offerId', $offerId)->where('keyword', 'customer_is_using_a_particular_shipping_method')
                            ->whereRaw('json_contains(jsonValues, ' . "'" . $shippingMethidId . "'" . ', \'$.shipping_method\')');

                    if ($query->count() == 0) {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    } else {
                        /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                        $match = true;
                    }
                }

                /* CONDITION 9 */
                if ($valO->keyword == "new_customer_reg_from_app") {
                    if ($userDetails->registeredBy == 'app')
                        $appReg = '["Y"]';
                    else
                        $appReg = '["N"]';

                    $query = \App\Model\Offercondition::where('offerId', $offerId)->where('keyword', 'new_customer_reg_from_app')
                            ->whereRaw('json_contains(jsonValues, ' . "'" . $appReg . "'" . ', \'$.app_user_registration\')');
                    if ($query->count() == 0) {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    } else {
                        /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                        $match = true;
                    }
                }

                /* CONDITION 1 */
                if ($valO->keyword == "customer_get_discounts_and_special_offers_on_certain_product_or_categories") {
                    foreach ($request->usercart['items'] as $keycart => $valCart) {
                        if ($valCart['siteProductId'] != null) {
                            $pId[] = array('{"values":["' . $valCart['siteProductId'] . '"]}');
                        }
                        if ($valCart['siteCategoryId'] != null) {
                            $pId[] = array('{"values":"' . $valCart['siteCategoryId'] . '"}');
                        }
                    }

                    /* IF ARRAY IS BLANK THEN CONDITION IS NOT SATISFIED */
                    if (count($pId) == 0) {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    }

                    $query = \App\Model\Offercondition::where('offerId', $offerId)->where('keyword', 'customer_get_discounts_and_special_offers_on_certain_product_or_categories');
                    $query->where(function($q) use ($pId) {
                        foreach ($pId as $keyQ => $condition) {
                            if ($keyQ == 0) {
                                $q->whereRaw('json_contains(jsonValues, ' . "'" . $condition[0] . "'" . ')');
                            } else {
                                $q->orWhereRaw('json_contains(jsonValues, ' . "'" . $condition[0] . "'" . ')');
                            }
                        }
                    });

                    if (count($query->get()) > 0) {
                        $match = true;
                    } else {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    }
                }

                if ($valO->keyword == "user_subscription") {
                    if ($userDetails->isSubscribed == 'Y')
                        $appReg = '["Y"]';
                    else
                        $appReg = '["N"]';

                    $query = \App\Model\Offercondition::where('offerId', $offerId)->where('keyword', 'user_subscription')
                            ->whereRaw('json_contains(jsonValues, ' . "'" . $appReg . "'" . ', \'$.user_subscription\')');
                    if ($query->count() == 0) {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    } else {
                        /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                        $match = true;
                    }
                }

                /* CONDITION 2 */
                if ($valO->keyword == "customer_of_a_specific_shipping_location") {

                    /* GET LOCATION DETAILS */
                    if (isset($request->usercart['personaldetails'])) {
                        $countryId = $request->usercart['personaldetails']['shippingCountryId'];
                        $stateId = $request->usercart['personaldetails']['shippingStateId'];
                        $cityId = $request->usercart['personaldetails']['shippingCityId'];
                    } else {
                        $countryId = $request->usercart['personalDetails']['shippingCountry'];
                        $stateId = $request->usercart['personalDetails']['shippingState'];
                        $cityId = $request->usercart['personalDetails']['shippingCity'];
                    }


                    if ($stateId == null) {
                        $stringState = "N/A";
                    } else {
                        $stringState = $stateId;
                    }

                    if ($cityId == null) {
                        $stringCity = "N/A";
                    } else {
                        $stringCity = $cityId;
                    }

                    $makeArray = '"countrySelected":"' . $countryId . '","stateSelected":"' . $stringState . '","citySelected":"' . $stringCity . '"';
                    $makeArray2 = '"countrySelected":"' . $countryId . '","stateSelected":"' . $stringState . '","citySelected":"N/A"';
                    $makeArray3 = '"countrySelected":"' . $countryId . '","stateSelected":"N/A","citySelected":"N/A"';

                    #DB::enableQueryLog(); 


                    $query = $condition2 = \App\Model\Offercondition::where('offerId', $offerId)->where('keyword', 'customer_of_a_specific_shipping_location');

                    $query->where(function($q) use ($makeArray, $makeArray2, $makeArray3) {

                        $q->whereRaw('json_contains(jsonValues, \'{' . $makeArray . '}\')');
                        $q->orWhereRaw('json_contains(jsonValues, \'{' . $makeArray2 . '}\')');
                        $q->orWhereRaw('json_contains(jsonValues, \'{' . $makeArray3 . '}\')');
                    });
                    //->get(); 
                    #dd(DB::getQueryLog());

                    if (count($query->get()) > 0) {
                        $match = true;
                    } else {
                        $match = false;
                        break;
                    }
                }
            }
            if ($match == false) {
                return response()->json([
                            'status' => '0',
                            'results' => 'invalid',
                            'message' => 'Invalid coupon'
                ]);
            } else {

                /* CALCULATE LOGIC */
                $bonus = unserialize($offerIdQuery[0]->bonusValuesSerialized);
                $countBonusArray = count($bonus);

                /* GET THE TOTAL SHIPPING DISCOUNT FROM LOCAL STORAGE */
                if ($request->usercart['isCurrencyChanged'] == 'Y')
                    $totalShippingCost = $request->usercart['data']["defaultTotalShippingCost"];
                else
                    $totalShippingCost = $request->usercart['data']["totalShippingCost"];


                /* GET THE AMOUNT */
                if ($countBonusArray == 4) {
                    $AmountOrPoint = "Amount";
                    $pointToBeDiscounted = 0.00;
                    $type = $bonus["type"];
                    if ($type == 'Absolute') {
                        if ($bonus["discount"] > $totalShippingCost) {
                            $amountToBeDiscounted = $totalShippingCost;
                        } else {
                            $amountToBeDiscounted = $bonus["discount"];
                        }
                    } else {
                        $discountNotExceed = $bonus["discountNotExceed"];
                        $getAmount = (($totalShippingCost * $bonus["discount"]) / 100);
                        if ($getAmount > $discountNotExceed) {
                            $amountToBeDiscounted = $discountNotExceed;
                        } else {
                            $amountToBeDiscounted = number_format($getAmount, 2);
                        }
                    }
                } else {
                    /* GET THE POINTS */
                    $AmountOrPoint = "Point";
                    $amountToBeDiscounted = 0.00;
                    if ($bonus['bonusPoint'] == 1) {
                        $pointToBeDiscounted = round($bonus["fixedAmount"]);
                    } else {
                        /* HANDLE DIVISION BY ZERO */
                        if ($bonus["bonusPer"] > 0) {
                            $getPer = ($totalShippingCost / $bonus["bonusPer"]);
                            $pointToBeDiscounted = round($getPer * $bonus["amountPer"]);
                        } else {
                            $pointToBeDiscounted = 0;
                        }
                    }
                }

                return response()->json([
                            'status' => '0',
                            'results' => 'valid',
                            'message' => 'Coupon applied',
                            'amount_or_point' => $AmountOrPoint,
                            'amount_to_be_discounted' => $amountToBeDiscounted,
                            'point_to_be_discounted' => $pointToBeDiscounted
                ]);
            }
        }
    }

    /**
     * Method used to submit order details
     * 
     * @return string
     */
    public function submitorder($userId, Request $request) {
        //print_r($request->all());
        $paymentStatus = 'unpaid';
        //$transactionId = $couponCode = $discountAmount = $discountPoint = $discountType = '';
        $transactionId = $couponCode = $discountPoint = $discountType = '';
        $discountAmount = 0;
        $transactionData = $transactionErrorMsg = array();
        $couponData = array();
        $paymentMode = 'offline';
        $paymentErrorMessage = '';

        /* Fetch User Details */
        $userData = User::find($userId);
        $totalInsurance = 0;
        $totalQuantity = 1;

        $warehouseId = $request->data['warehouseId'];

        /* Fetch Warehouse Details */
        $warehouseData = \App\Model\Warehouse::find($warehouseId);

        /* Fetch User Details */
        $userDetails = \App\Model\User::where("id", $userId)->first();

        $totalItemCost = $request->data['totalItemCost'];
        $isInsuranceCharged = $request->data['isInsuranceCharged'];
        $totalTax = isset($request->data['totalTax']) ? $request->data['totalTax'] : 0;
        $shippingCost = $request->data['shippingCost'];
        $totalClearingDuty = $request->data['totalClearingDuty'];
        $totalInsurance = $request->data['totalInsurance'];
        $totalOtherCharges = $request->data['otherCharge'];
        $totalCost = $request->data['totalCost'] + $totalTax;

        $couponCode = '';
        if (isset($request->coupondetails)) {
            $totalBDiscountCost = $request->data['totalBDiscountCost'];
            $couponCode = $request->coupondetails['couponCode'];
            $discountAmount = $request->coupondetails['discountAmount'];
            $discountPoint = $request->coupondetails['discountPoint'];
            $discountType = $request->coupondetails['discountType'];
            $offerData = \App\Model\Offer::select('id')->where('couponCode', $couponCode)->first();
            $couponId = $offerData['id'];
        }

        /*  SET DATA FOR OFFLINE GATEWAYS */
        if ($request->paymentMethod['paymentMethodKey'] == 'bank_online_transfer' || $request->paymentMethod['paymentMethodKey'] == 'bank_pay_at_bank' || $request->paymentMethod['paymentMethodKey'] == 'bank_account_pay' || $request->paymentMethod['paymentMethodKey'] == 'check_cash_payment')
            $paymentStatus = 'paid';

        /*  SET DATA FOR WIRE TRANSFER PAYMENT */
        if ($request->paymentMethod['paymentMethodKey'] == 'wire_transfer') {
            $paymentStatus = 'paid';

            $trasactionData = json_encode(array(
                'poNumber' => $request->paymentdetails['poNumber'],
                'companyName' => $request->paymentdetails['companyName'],
                'buyerName' => $request->paymentdetails['buyerName'],
                'position' => $request->paymentdetails['position'],
                    )
            );
        }

        /*  PROCESS EWALLET PAYMENT */
        if ($request->paymentMethod['paymentMethodKey'] == 'ewallet') {
            $eawalletid = (int) $request->paymentdetails['id'];
            $userEwallet = \App\Model\Ewallet::find($eawalletid)->decrement('amount', $totalCost);
            $ewalletTransaction = new \App\Model\Ewallettransaction;
            $ewalletTransaction->userId = $userId;
            $ewalletTransaction->ewalletId = $eawalletid;
            $ewalletTransaction->amount = $totalCost;
            $ewalletTransaction->transactionType = 'debit';
            $ewalletTransaction->transactionOn = Config::get('constants.CURRENTDATE');

            if ($ewalletTransaction->save())
                $paymentStatus = 'paid';
            else
                $paymentStatus = 'failed';

            $paymentMode = 'online';

            $transactionData = json_encode(array(
                'ewalletId' => $request->paymentdetails['ewalletId'],
                    )
            );
        }
        /* PROCESS PAYSTACK DATA */
        if ($request->paymentMethod['paymentMethodKey'] == 'paystack_checkout') {

            if (!empty($request->paystackData['trans']))
                $findRecord = \App\Model\Paystackreferencehistory::whereRaw("referenceNo = $request->paystackCreatedReference and processed='0'")->first();
            else
                $findRecord = \App\Model\Paystackreferencehistory::whereRaw("referenceNo = $request->paystackCreatedReference")->first();

            if (!empty($findRecord)) {
                $paymentTransacionData = \App\Model\Paymenttransaction::where("transactionData", "like", '%"reference":"' . $request->paystackCreatedReference . '"%')->first();
                if (empty($paymentTransacionData) || (!empty($paymentTransacionData) && $paymentTransacionData->status == 'unpaid')) {
                    if (!empty($request->paystackData)) {
                        $checkoutReturn = Paymenttransaction::paystack($request->paystackCreatedReference);

                        if ($checkoutReturn) {
                            $paymentMode = 'online';
                            $paymentStatus = 'paid';
                            if (!empty($request->paystackData['trans']))
                                $trasactionId = $request->paystackData['trans'];
                            else if (!empty($request->paystackData['id']))
                                $transactionId = $request->paystackData['id'];
                            else
                                $transactionId = '123456789';
                            $transactionData = json_encode($request->paystackData);
                        } else {
                            $paymentStatus = 'failed';
                        }
                    }
                } else {
                    $paymentStatus = 'pastackProcessed';
                }
            }
            
            if ($paymentStatus == 'pastackProcessed') {
                return response()->json([
                            'status' => '1',
                            'results' => 'success',
                ]);
            }
        }

        /*  PROCESS CREDIT CARD PAYMENT */
        if ($request->paymentMethod['paymentMethodKey'] == 'credit_debit_card') {
            $paymentMethod = \App\Model\Paymentmethod::where('paymentMethodKey', $request->paymentMethod['paymentMethodKey'])->first();
            if ($paymentMethod->paymentGatewayId == 1) {
                $checkoutData = array();
                $checkoutData['cardNumber'] = $request->paymentdetails['cardNumber'];
                $checkoutData['expMonth'] = $request->paymentdetails['expiryMonth'];
                $checkoutData['expYear'] = $request->paymentdetails['expiryYear'];
                $checkoutData['cardCode'] = $request->paymentdetails['cvvCode'];
                $checkoutData['customerFirstName'] = $userData->firstName;
                $checkoutData['customerLastName'] = $userData->lastName;
                $checkoutData['customerAddress'] = $request->personaldetails['billingAddress'];
                $checkoutData['customerCity'] = isset($request->personaldetails['billingCityName']) ? $request->personaldetails['billingCityName'] : "";
                $checkoutData['customerState'] = isset($request->personaldetails['billingStateName']) ? $request->personaldetails['billingStateName'] : "";
                $checkoutData['customerCountry'] = $request->personaldetails['billingCountryName'];
                $checkoutData['customerZip'] = $request->personaldetails['billingZipcode'];
                $checkoutData['amount'] = round($totalCost, 2);
                $checkoutData['defaultCurrency'] = $request->defaultCurrencyCode;

                $checkoutReturn = Paymenttransaction::paypaypalpro($checkoutData);

                if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'successWithWarning')) {
                    $paymentStatus = 'paid';
                    $paymentMode = 'online';
                    $transactionId = $checkoutReturn['TRANSACTIONID'];
                    $transactionData = json_encode($checkoutReturn);
                } else if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Failure')) {
                    $paymentStatus = 'failed';
                    $transactionErrorMsg = json_encode($checkoutReturn);
                    $paymentErrorMessage = $checkoutReturn["L_LONGMESSAGE0"];
                } else if ($checkoutReturn == 'error') {
                    $paymentStatus = 'failed';
                }
            } else {
                $checkoutData['cardNumber'] = $request->paymentdetails['cardNumber'];
                $checkoutData['cardNumber'] = $request->paymentdetails['cardNumber'];
                $checkoutData['expMonth'] = $request->paymentdetails['expiryMonth'];
                $checkoutData['expYear'] = $request->paymentdetails['expiryYear'];
                $checkoutData['cardCode'] = $request->paymentdetails['cvvCode'];
                $checkoutData['customerFirstName'] = $request->personaldetails['billingFirstName'];
                $checkoutData['customerLastName'] = $request->personaldetails['billingLastName'];
                $checkoutData['customerAddress'] = $request->personaldetails['billingAddress'];
                $checkoutData['customerCity'] = isset($request->personaldetails['billingCityName']) ? $request->personaldetails['billingCityName'] : "";
                $checkoutData['customerState'] = isset($request->personaldetails['billingStateName']) ? $request->personaldetails['billingStateName'] : "";
                $checkoutData['customerCountry'] = $request->personaldetails['billingCountryName'];
                $checkoutData['customerZip'] = !empty($request->personaldetails['billingZipcode']) ? $request->personaldetails['billingZipcode'] : "";
                $checkoutData['customerShippingAddress'] = $request->personaldetails['shippingAddress'];
                $checkoutData['customerShippingCity'] = isset($request->personaldetails['shippingCityName']) ? $request->personaldetails['shippingCityName'] : "";
                $checkoutData['customerShippingState'] = isset($request->personaldetails['shippingStateName']) ? $request->personaldetails['shippingStateName'] : "";
                $checkoutData['customerShippingCountry'] = $request->personaldetails['shippingCountryName'];
                $checkoutData['customerShippingZip'] = $request->personaldetails['shippingZipcode'];
                $checkoutData['amount'] = round($totalCost, 2);
                $checkoutData['defaultCurrency'] = $request->defaultCurrencyCode;
                $checkoutData['shippingFirstName'] = $request->personaldetails['shippingFirstName'];
                $checkoutData['shippingLastName'] = $request->personaldetails['shippingFirstName'];

                $checkoutReturn = Paymenttransaction::payauthorizedotnet($checkoutData);
                if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Ok') {
                    $paymentStatus = 'paid';
                    $paymentMode = 'online';
                    $transactionId = $checkoutReturn['transactionResponse']['transId'];
                    $transactionData = json_encode($checkoutReturn['transactionResponse']);
                } else if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Error') {
                    $paymentStatus = 'failed';
                    if (!empty($checkoutReturn['transactionResponse'])) {
                        $transactionErrorMsg = json_encode($checkoutReturn['transactionResponse']);
                        if (isset($checkoutReturn['transactionResponse']['errors']['error'][0]))
                            $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['transactionResponse']['errors']['error'][0]['errorText'];
                        else
                            $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['transactionResponse']['errors']['error']['errorText'];
                    }else {
                        if (!empty($checkoutReturn['messages']['message'])) {
                            $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['messages']['message']['text'];
                        }
                    }
                }
            }
        }

        if ($request->paymentMethod['paymentMethodKey'] == 'paypalstandard') {
            if (isset($request->paypalData)) {
                $paymentStatus = 'paid';
                $paymentMode = 'online';
                $transactionId = $request->paypalData['orderID'];
                $transactionData = json_encode($request->paypalData);
            } else {
                $paymentStatus = 'error';
            }
        }

        if ($request->paymentMethod['paymentMethodKey'] == 'payeezy') {
            if (!empty($request->payeezyData)) {
                $checkoutData['amount'] = $request->payeezyData["paidAmount"];
                $checkoutData['method'] = $request->payeezyData["paymentType"];
                $checkoutData['currency_code'] = "USD";
                $checkoutData['type'] = $request->payeezyData["cardType"];
                $checkoutData['cardholder_name'] = $request->payeezyData["cardHolderName"];
                $checkoutData['card_number'] = $request->payeezyData['ccardNumber'];
                $checkoutData['exp_date'] = $request->payeezyData['expiryMonth'] . substr($request->payeezyData['expiryYear'], 2);
                $checkoutData['cvv'] = $request->payeezyData['cvvCode'];
                $checkoutData['userUnit'] = $userDetails->unit;
                $checkoutReturn = Paymenttransaction::processpayeezy($checkoutData);

                if (is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'success' && !empty($checkoutReturn['transaction_status']) && $checkoutReturn['transaction_status'] == 'approved') {
                    $paymentStatus = 'paid';
                    $paymentMode = 'online';
                    $transactionId = $checkoutReturn['transaction_id'];
                    $transactionData = json_encode($checkoutReturn);
                } else if (is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'failed') {
                    $paymentStatus = 'failed';
                    $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['Error']['messages'][0]['description'];
                } else {
                    $paymentStatus = 'failed';
                    $paymentErrorMessage = 'Payment failed. Something Went Wrong ';
                }
            } else {
                $paymentStatus = 'error';
            }
        }

            /* Insert Data into Main Fill&Ship Table */
            $fillship = new Fillship;
            $fillship->userId = $userId;
            $fillship->warehouseId = $request->data['warehouseId'];
            $fillship->boxId = $request->data['boxId'];
            $fillship->boxLength = $request->data['boxLength'];
            $fillship->boxHeight = $request->data['boxHeight'];
            $fillship->boxWidth = $request->data['boxWidth'];
            $fillship->boxChargeableWeight = $request->data['boxChargeableWeight'];
            $fillship->fromCountry = $warehouseData->countryId;
            $fillship->fromState = $warehouseData->stateId;
            $fillship->fromCity = $warehouseData->cityId;
            $fillship->fromAddress = $warehouseData->address;
            $fillship->fromZipCode = $warehouseData->zipcode;
            //$fillship->fromPhone = $request->personaldetails['contactNumber'];
            if (isset($request->personaldetails['phcode']) && !empty($request->personaldetails['phcode'])) {
                $fillship->fromPhone = $request->personaldetails['phcode'] . $request->personaldetails['contactNumber'];
            } else {
                $fillship->fromPhone = $request->personaldetails['contactNumber'];
            }
            $fillship->fromName = $request->personaldetails['userTitle'] . " " . $request->personaldetails['firstName'] . " " . $request->personaldetails['lastName'];
            $fillship->fromEmail = $request->personaldetails['userEmail'];
            $fillship->toCountry = $request->personaldetails['shippingCountryId'];
            $fillship->toState = $request->personaldetails['shippingStateId'];
            $fillship->toCity = $request->personaldetails['shippingCityId'];
            $fillship->toAddress = $request->personaldetails['shippingAddress'];
            if (!empty($request->personaldetails['shippingZipcode']))
                $fillship->toZipCode = $request->personaldetails['shippingZipcode'];
            if (isset($request->personaldetails['shippingIsdcode']) && !empty($request->personaldetails['shippingIsdcode'])) {
                $fillship->toPhone = $request->personaldetails['shippingIsdcode'] . $request->personaldetails['shippingPhone'];
            } else {
                $fillship->toPhone = $request->personaldetails['shippingPhone'];
            }
            // $fillship->toPhone = $request->personaldetails['shippingPhone'];
            $fillship->toName = $request->personaldetails['shippingFirstName'] . " " . $request->personaldetails['shippingLastName'];
            $fillship->toEmail = $request->personaldetails['shippingEmail'];
            $fillship->totalItemCost = $totalItemCost;
            $fillship->totalShippingCost = $shippingCost;
            $fillship->totalClearingDuty = $totalClearingDuty;
            $fillship->totalInsurance = ($isInsuranceCharged == 'Y') ? $totalInsurance : 0;
            $fillship->totalTax = $totalTax;
            $fillship->totalOtherCharges = $totalOtherCharges;
            $fillship->totalCost = $totalCost;
            $fillship->shippingMethod = 'Y';
            $fillship->shippingMethodId = $request->data['shipmentMethodId'];
            $fillship->status = ($paymentMode == 'offline') ? 'submitted' : 'processed';
            $fillship->paymentMethodId = $request->paymentMethod['paymentMethodId'];
            $fillship->paymentStatus = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
            $fillship->paymentReceivedOn = Config::get('constants.CURRENTDATE');
            $fillship->isCurrencyChanged = $request->isCurrencyChanged;
            $fillship->defaultCurrencyCode = $request->defaultCurrencyCode;
            $fillship->paidCurrencyCode = ($request->isCurrencyChanged == 'Y') ? $request->currencyCode : $request->defaultCurrencyCode;
            $fillship->exchangeRate = ($request->isCurrencyChanged == 'Y') ? $request->exchangeRate : 1;
            $fillship->createdBy = $userId;
            $fillship->createdByType = 'user';
            $fillship->createdOn = Config::get('constants.CURRENTDATE');
            $fillship->orderDate = Config::get('constants.CURRENTDATE');

            if (!empty($couponCode)) {
                if ($discountType == 'amount')
                    $fillship->totalDiscount = $discountAmount;
                //$fillship->totalDiscount = $totalDiscount;
                $fillship->couponcodeApplied = $couponCode;
            }

            if ($isInsuranceCharged == 'Y') {
                $fillship->totalInsurance = $request->data['totalInsurance'];
            }

            $fillship->save();
            $fillshipId = $fillship->id;

            /* Update Fillship Other Charges */
            $fillshipOtherCharge = new Fillshipothercharges;
            $fillshipOtherCharge->fillshipId = $fillshipId;
            $fillshipOtherCharge->otherChargeName = 'Warehousing';
            $fillshipOtherCharge->otherChargeAmount = $totalOtherCharges;
            $fillshipOtherCharge->createdBy = $userDetails->email;
            $fillshipOtherCharge->createdOn = Config::get('constants.CURRENTDATE');
            $fillshipOtherCharge->save();
            
            if(!empty($fillshipId)) {
                if(isset($request->tempDataId) && !empty($request->tempDataId)) {
                
                    $tempCartData = \App\Model\Temporarycartdata::find($request->tempDataId);
                    $tempCartData->isDataSaved = 'Y';
                    $tempCartData->dataSavedId = $fillshipId;
                    $tempCartData->save();
                }
            }

            /* Create order on successful payment */
            /* $orderObj = new \App\Model\Order;
              $orderObj->shipmentId = $fillship->id;
              $orderObj->userId = $userId;
              $orderObj->totalCost = $totalCost;
              $orderObj->status = '2';
              $orderObj->type = 'fillship';
              $orderObj->createdBy = $userId;
              $orderObj->save();
              $orderNumber = \App\Model\Order::generateOrderNumber($orderObj->id, $orderObj->userId);
              $orderObj->orderNumber = $orderNumber;
              $orderObj->save();

             */

            /*  PREPARE DATA FOR INVOICE PARTICULARS */
            

            /*  COUPON DATA */
            if (isset($request->coupondetails) && !empty($couponCode)) {
                /*  INSERT DATA INTO COUPON TABLE */
                $data = array(
                    'couponId' => $couponId,
                    'code' => $couponCode,
                    'userId' => $userId,
                    'totalAmount' => $totalBDiscountCost,
                    //'discountAmount' => $totalDiscount,
                    'discountAmount' => $discountAmount,
                    'discountPoint' => $discountPoint,
                );
                $couponLog = \App\Model\Couponlog::insertLog($data);

                /*  UPDATE DATA IF COUPON POINTS */
                if ($discountType == 'points') {
                    $fundpoint = \App\Model\Fundpoint::where('userId', $userId)->increment('point', $discountPoint);

                    $fundpointTransaction = new \App\Model\Fundpointtransaction;
                    $fundpointTransaction->userId = $userId;
                    $fundpointTransaction->point = $discountPoint;
                    $fundpointTransaction->date = Config::get('constants.CURRENTDATE');
                    $fundpointTransaction->save();
                }
                /*  UPDATE DATA IF COUPON POINTS */
            }


            /*  EMAIL INVOICE */
            if (!empty($invoiceId)) {
                $data['invoice'] = \App\Model\Invoice::find($invoiceId);
                //$data['orderNumber'] = $orderNumber;
                $fileName = "Receipt_" . $invoiceUniqueId . ".pdf";
                PDF::loadView('Administrator.fillnship.invoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                $to = $request->personaldetails['userEmail'];

                $content = " Invoice for Shipment # " . $fillshipId;
                $replace['[NAME]'] = $userData->firstName . " " . $userData->lastName;
                $replace['[SHIPMENTID]'] = $fillshipId;
                $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'fill_ship_invoice')->first();
                customhelper::SendMailWithAttachment($emailTemplate, $replace, $to, public_path('exports/invoice/' . $fileName));
            }

            /* DELETE USER CART ITEMS */
            $userCart = Usercart::where('userId', $userId)->where('type', 'fillship')->delete();
        
        if ($paymentStatus == 'paid') {
            
            $invoiceData = array(
                'shipment' => array(
                    'totalItemCost' => $totalItemCost,
                    'totalQuantity' => $totalQuantity,
                    'totalTax' => $totalTax,
                    'isInsuranceCharged' => $isInsuranceCharged,
                    'totalInsurance' => $totalInsurance,
                    'totalWeight' => $fillship->totalChargeableWeight,
                    'otherChargeCost' => $totalOtherCharges,
                    'discount' => $request->discount,
                    'shippingCost' => $shippingCost,
                    'clearingDutyCost' => $totalClearingDuty,
                    'totalCost' => $totalCost,
                    'shippingMethodName' => $request->data['shippingMethodName'],
                    'boxName' => $request->data['boxName'],
                    'boxWidth' => $request->data['boxWidth'],
                    'boxHeight' => $request->data['boxHeight'],
                    'boxLength' => $request->data['boxLength'],
                    'boxChargeableWeight' => $fillship->boxChargeableWeight,
                ),
                'warehouse' => array(
                    'fromAddress' => $warehouseData->address,
                    'fromZipCode' => $warehouseData->zipcode,
                    'fromCountry' => $warehouseData->countryId,
                    'fromState' => $warehouseData->stateId,
                    'fromCity' => $warehouseData->cityId,
                ),
                'shippingaddress' => array(
                    'toCountry' => $request->personaldetails['shippingCountryId'],
                    'toState' => $request->personaldetails['shippingStateId'],
                    'toCity' => $request->personaldetails['shippingCityId'],
                    'toAddress' => $request->personaldetails['shippingAddress'],
                    'toAlternateAddress' => '',
                    'toZipCode' => '',
                    'toName' => $request->personaldetails['shippingFirstName'] . " " . $request->personaldetails['shippingLastName'],
                    'toEmail' => $request->personaldetails['shippingEmail'],
                    'toPhone' => $request->personaldetails['shippingPhone'],
                ),
                'items' => array(),
                'payment' => array(
                    'paymentMethodId' => $request->paymentMethod['paymentMethodId'],
                    'paymentMethodName' => $request->paymentMethod['paymentMethodName'],
                ),
            );

            if (isset($request->coupondetails) && !empty($couponCode)) {
                $invoiceData['shipment']['couponCode'] = $couponCode;
                $invoiceData['shipment']['discountAmount'] = $discountAmount;
                $invoiceData['shipment']['discountPoint'] = $discountPoint;
                $invoiceData['shipment']['discountType'] = $discountType;
            }

            if ($request->paymentMethod['paymentMethodKey'] == 'wire_transfer') {
                $invoiceData['payment']['poNumber'] = $request->paymentdetails['poNumber'];
                $invoiceData['payment']['companyName'] = $request->paymentdetails['companyName'];
                $invoiceData['payment']['buyerName'] = $request->paymentdetails['buyerName'];
                $invoiceData['payment']['position'] = $request->paymentdetails['position'];
            }
            if ($request->paymentMethod['paymentMethodKey'] == 'ewallet') {
                $invoiceData['payment']['ewalletId'] = $request->paymentdetails['ewalletId'];
            }

            if (!empty($request->data['shipmentMethodId'])) {
                $invoiceData['shippingcharges']['shippingMethod'] = 'Y';
                $invoiceData['shippingcharges']['shippingCost'] = $shippingCost;
                $invoiceData['shippingcharges']['totalClearingDuty'] = $totalClearingDuty;
                $invoiceData['shippingcharges']['totalOtherCharges'] = $totalOtherCharges;
            }

            $invoiceUniqueId = 'REC' . $userData->unit . '-' . $fillshipId . '-' . date('Ymd');
            /*  PREPARE DATA FOR INVOICE PARTICULARS */

            /*  INSERT DATA INTO INVOICE TABLE */
            $invoice = new \App\Model\Invoice;
            $invoice->invoiceUniqueId = $invoiceUniqueId;
            $invoice->shipmentId = $fillshipId;
            $invoice->invoiceType = ($paymentMode == 'offline') ? 'invoice' : 'receipt';
            $invoice->type = 'fillship';
            $invoice->userUnit = $userData->unit;
            $invoice->userFullName = $request->personaldetails['userTitle'] . " " . $request->personaldetails['firstName'] . " " . $request->personaldetails['lastName'];
            $invoice->userEmail = $request->personaldetails['userEmail'];
            $invoice->userContactNumber = $request->personaldetails['contactNumber'];
            $invoice->billingName = $request->personaldetails['billingFirstName'] . ' ' . $request->personaldetails['billingLastName'];
            $invoice->billingEmail = $request->personaldetails['billingEmail'];
            $invoice->billingAddress = $request->personaldetails['billingAddress'];
            if (!empty($request->personaldetails['billingAlternateAddress']))
                $invoice->billingAlternateAddress = $request->personaldetails['billingAlternateAddress'];
            $invoice->billingCity = $request->personaldetails['billingCityId'];
            $invoice->billingState = $request->personaldetails['billingStateId'];
            $invoice->billingCountry = $request->personaldetails['billingCountryId'];
            if (!empty($request->personaldetails['billingZipcode']))
                $invoice->billingZipcode = $request->personaldetails['billingZipcode'];
            $invoice->billingPhone = $request->personaldetails['billingPhone'];
            $invoice->totalBillingAmount = $totalCost;
            $invoice->paymentMethodId = $request->paymentMethod['paymentMethodId'];
            $invoice->paymentStatus = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
            $invoice->invoiceParticulars = json_encode($invoiceData);
            $invoice->createdOn = Config::get('constants.CURRENTDATE');
            $invoice->save();
            $invoiceId = $invoice->id;
            
            /*  INSERT DATA INTO PAYMENT TABLE */
            $paidfor = 'fillship';
            $paymentTransaction = new Paymenttransaction;
            $paymentTransaction->userId = $userId;
            $paymentTransaction->paymentMethodId = $request->paymentMethod['paymentMethodId'];
            $paymentTransaction->paidFor = $paidfor;
            $paymentTransaction->paidForId = $fillshipId;
            $paymentTransaction->invoiceId = $invoice->id;
            $paymentTransaction->amountPaid = $totalCost;
            $paymentTransaction->status = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;

            if (!empty($transactionData))
                $paymentTransaction->transactionData = $transactionData;
            if (!empty($transactionId))
                $paymentTransaction->transactionId = $transactionId;
            $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
            $paymentTransaction->save();
            /*  INSERT DATA INTO PAYMENT TABLE */
            
        } else if ($paymentStatus == 'failed') {
            /*  INSERT DATA INTO PAYMENT TABLE */
            $paidfor = 'fillship';

            $paymentTransaction = new Paymenttransaction;
            $paymentTransaction->userId = $userId;
            $paymentTransaction->paymentMethodId = $request->paymentMethod['paymentMethodId'];
            $paymentTransaction->paidFor = $paidfor;
            $paymentTransaction->amountPaid = $totalCost;
            $paymentTransaction->status = $paymentStatus;
            if (!empty($transactionErrorMsg))
                $paymentTransaction->errorMsg = $transactionErrorMsg;
            $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
            $paymentTransaction->save();
            /*  INSERT DATA INTO PAYMENT TABLE */

        }
        
        if ($paymentStatus == 'paid') {
            return response()->json([
                        'status' => '1',
                        'results' => 'success',
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => 'payment_failed',
                        'msg' => !empty($paymentErrorMessage) ? $paymentErrorMessage.'. Please contact site admin.'  : 'There seems to be some issue with payment. Please contact site admin.'
            ]);
        }
    }

    public function getuserfillship(Request $request) {

        $userId = $request->userId;
        $warehouse = $request->warehouse;
        $userFillshipData = array();
        $dateAfterTwoWeek = date('Y-m-d', strtotime('+2 weeks'));
        $settings = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();
        if ($request->dataDuration == 'active')
            $fillShipShipmentData = Fillship::where('userId', $userId)->where('warehouseId', $warehouse['id'])->where('deleted', '0')->where('paymentStatus', 'paid')->whereRaw("(deliveredOn is null OR deliveredOn<='" . $dateAfterTwoWeek . "')")->with('filshipboxinfo')->orderBy('id', 'desc')->get()->toArray();
        else
            $fillShipShipmentData = Fillship::where('userId', $userId)->where('warehouseId', $warehouse['id'])->where('deleted', '0')->where('paymentStatus', 'paid')->whereRaw("deliveredOn >'" . $dateAfterTwoWeek . "'")->with('filshipboxinfo')->orderBy('id', 'desc')->get()->toArray();
        foreach ($fillShipShipmentData as $index => $eachShipmentData) {
            $userFillshipData['shipments'][$index] = $eachShipmentData;

            $packages = $this->getuserfillshipitems($eachShipmentData['id']);
            $shipmentReceivedStatus = true;
            $totalWeight = 0;
            $totalVolumeWeight = 0;
            if (!empty($packages)) {
                foreach ($packages as $package) {
                    $totalWeight += $package['weight'];
                    $totalVolumeWeight += (($package['length'] * $package['width'] * $package['height']) / $settings->settingsValue);
                    if ($package['status'] == 'submitted')
                        $shipmentReceivedStatus = false;
                }
            }

            $userFillshipData['shipments'][$index]['totalWeight'] = $totalWeight;
            $userFillshipData['shipments'][$index]['totalVolumeWeight'] = round($totalVolumeWeight, 2);
            $userFillshipData['shipments'][$index]['shipmentReceivedStatus'] = $shipmentReceivedStatus;
            $userFillshipData['shipments'][$index]['actualFillPercentage'] = Fillship::calculateBoxFill($eachShipmentData);
            $userFillshipData['shipments'][$index]['currencySymbol'] = customhelper::getCurrencySymbolCode($eachShipmentData['defaultCurrencyCode']);
            $userFillshipData['shipments'][$index]['packages'] = $packages;
            $userFillshipData['shipments'][$index]['stages'] = \App\Model\Fillshipshipmentstatuslog::getStatusLog($eachShipmentData['id']);
            if ($eachShipmentData['shippOutOpt'] == '0') {
                $userFillshipData['shipments'][$index]['shipout_extracharge'] = $this->getshipoutoptionsandextracharges($eachShipmentData['id']);
            }
        }

        echo json_encode($userFillshipData);
        exit;
    }

    public function getuserfillshipitems($fillshipShipmentId) {

        $fillshipItems = array();
        $items = Fillshipitem::getItems($fillshipShipmentId);
        if ($items->count() > 0) {
            $fillshipItems = $items->toArray();
        }

        return $fillshipItems;
    }

    public function getshipoutoptionsandextracharges($fillshipShipmentId) {

        $returnData = array();
        $storageCharge = $ewalletTransfer = $extraCost = '0.00';
        $type = 'shipout';
        $message = '';
        /* Fill & ship settings values */
        $excessVolumeHandlingCharge = \App\Model\Generalsettings::where('settingsKey', 'excess_volume_handling_charge')->first()->settingsValue;
        $excessVolumeHandlingChargePercent = \App\Model\Generalsettings::where('settingsKey', 'excess_volume_charge_prc')->first()->settingsValue;
        $ewalletTransferDiscountPercent = \App\Model\Generalsettings::where('settingsKey', 'ewallet_transfer_discount_prc')->first()->settingsValue;
        $boxFullThresholdPercent = \App\Model\Generalsettings::where('settingsKey', 'box_full_threshold_prc')->first()->settingsValue;
        $boxOverflowThresholdPercent = \App\Model\Generalsettings::where('settingsKey', 'box_overflow_threshold_prc')->first()->settingsValue;
        $storageDays = \App\Model\Generalsettings::where('settingsKey', 'fill_and_ship_storage_days')->first()->settingsValue;
        $storageUnitCharge = \App\Model\Generalsettings::where('settingsKey', 'unit_weight_charge')->first()->settingsValue;

        /* Fill & ship shipment details */
        $fillshipShipment = Fillship::find($fillshipShipmentId);
        $actualBoxFill = Fillship::calculateBoxFill($fillshipShipment);
        $totalBoxCost = $fillshipShipment->totalCost;
        $maxStorageDate = $fillshipShipment->maxStorageDate;
        // $perDayStorageCharge = $fillshipShipment->storageCharge;

        /* Storage Calculattion */
        $maxStorageDate = Carbon::parse($maxStorageDate);
        $now = Carbon::now();
        $diff = $maxStorageDate->diffInDays($now, FALSE);

        if ($diff > 0) {
            $perDayStorageCharge = (float) ($fillshipShipment->totalChargeableWeight * $storageUnitCharge);

            $storageCharge = round(($perDayStorageCharge * $diff), 2);
            $type = 'extracharge';
            $message = "<p>Your box has crossed maximum free storage period. You have a storage charge of " . customhelper::getCurrencySymbolFormat($storageCharge) . " that will have to be paid prior to ship out.</p>";
            $extraCost = $storageCharge;
//            if($actualBoxFill>(100+$boxOverflowThresholdPercent))
//            {
//                $excessBoxFill = $actualBoxFill-100;
//                $extraCharge = round(($excessVolumeHandlingCharge+($totalBoxCost*($excessVolumeHandlingChargePercent/100))),2);
//                $returnData['extraCost'] = $storageCharge+$extraCharge;
//                $returnData['message'] = '<p>Your box is more than 100% full. You also have a storage charge of '.customhelper::getCurrencySymbolFormat($storageCharge).'. Your total extra charge of '.customhelper::getExtractCurrency(($storageCharge+$extraCharge)).' that will have to be paid prior to ship out.</p>';
//            }
        }

        if ($actualBoxFill < $boxFullThresholdPercent) {
            $remainingBoxfill = 100 - $actualBoxFill;
            $balanceAmount = round(($totalBoxCost * ($remainingBoxfill / 100)), 2);
            $ewalletCreadit = round(($balanceAmount * ((100 - $ewalletTransferDiscountPercent) / 100)), 2);
            $ewalletTransfer = $ewalletCreadit;
            $message .= '<p>Since your box is not full, ' . customhelper::getCurrencySymbolFormat($ewalletCreadit) . ' will be credited to your e-wallet account. You can use this for future shipping.</p>';
        } else if ($actualBoxFill > $boxFullThresholdPercent && $actualBoxFill < 100) {
            $message .= '<p>Thank for using "Fill and Ship"</p>';
        } else if ($actualBoxFill > 100 && $actualBoxFill < (100 + $boxOverflowThresholdPercent)) {
            $excessBoxFill = $actualBoxFill - 100;
            $excessAmount = round(($totalBoxCost * ($excessBoxFill / 100)), 2);
            $message .= '<p>Your box is more than 100% full. You have an extra charge of ' . customhelper::getCurrencySymbolFormat($excessAmount) . ' . However this will be waved. Thanks for been a valued customer.</p>';
        } else if ($actualBoxFill > (100 + $boxOverflowThresholdPercent)) {
            $excessBoxFill = $actualBoxFill - 100;
            //$extraCharge = round(($excessVolumeHandlingCharge + ($totalBoxCost * ($excessVolumeHandlingChargePercent / 100))), 2);
            $extraCharge = round(($excessVolumeHandlingCharge + ($totalBoxCost * (($excessVolumeHandlingChargePercent + $excessBoxFill) / 100))), 2);
            $type = 'extracharge';
            $extraCost += $extraCharge;
            $message .= '<p>Your box is more than 100% full. You have an extra charge of ' . customhelper::getCurrencySymbolFormat($extraCharge) . ' that will have to be paid prior to ship out.</p>';
        }

        $returnData['type'] = $type;
        $returnData['ewalletTransfer'] = $ewalletTransfer;
        $returnData['extraCost'] = round($extraCost, 2);
        $returnData['message'] = $message;
        $returnData['storageCharge'] = $storageCharge;
        if ($type == 'shipout') {
            $returnData['message'].= '<p>If you select "Continue", your box will be on its way to you. Track it by using your order number as the tracking number. We appreciate your business.</p>';
        } else {
            $returnData['message'].= '<p>Please proceed with the payment, once payment is complete your box will be on its way to you. Track it by using your order number as the tracking number. We appreciate your business.</p>';
        }


        return $returnData;
    }

    public function fillshipshipout(Request $request) {

        $fillshipShipmentId = $request->fillshipShipmentId;
        $ewalletTransferAmount = $request->ewalletTransferAmount;
        $userId = $request->userId;

        $shipout = Fillship::proceedShipout($fillshipShipmentId, $userId, $ewalletTransferAmount);

        if ($shipout) {
            return response()->json([
                        'status' => '1',
                        'results' => 'success',
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => 'error',
            ]);
        }
    }

    public function payfillshipextracharge(Request $request) {
//        print_r($request->all());exit;
        if (!empty($request->data)) {
            $fillshipShipmentId = $request->fillshipShipmentId;
            $ewalletTransferAmount = (isset($request->ewalletTransferAmount) && $request->ewalletTransferAmount != "") ? $request->ewalletTransferAmount : '0.00';
            $paidAmount = $request->data['paidAmount'];
            $paymentErrorMessage = '';
            $paymentStatus = 'unpaid';
            $paymentMode = 'offline';

            /*  SET DATA FOR OFFLINE GATEWAYS */

            if ($request->data['paymentMethodKey'] == 'bank_online_transfer' || $request->data['paymentMethodKey'] == 'bank_pay_at_bank' || $request->data['paymentMethodKey'] == 'bank_account_pay' || $request->data['paymentMethodKey'] == 'check_cash_payment')
                $paymentStatus = 'paid';

            $defaultCurrencyCode = \App\Helpers\customhelper::getCurrencySymbolCode('', true);

            $userId = $request->userId;

            $userData = User::find($userId);
            $invoice = \App\Model\Invoice::where('shipmentId', $fillshipShipmentId)->where('invoiceType', 'receipt')->where('type', 'fillship')->where('deleted', '0')->where('extraCostCharged', 'N')->first();
            $newInvoice = $invoice->replicate();
            $invoiceParticulars = json_decode($invoice->invoiceParticulars, true);
            $shippingAddress = (array) $invoiceParticulars['shippingaddress'];

            if ($request->data['paymentMethodKey'] == 'wire_transfer') {
                $paymentStatus = 'paid';
                $trasactionData = json_encode(array(
                    'poNumber' => $request->data['poNumber'],
                    'companyName' => $request->data['companyName'],
                    'buyerName' => $request->data['buyerName'],
                    'position' => $request->data['position'],
                        )
                );
            } else if ($request->data['paymentMethodKey'] == 'ewallet') { /*  PROCESS EWALLET PAYMENT */
                $paymentMode = 'online';
                $eawalletid = (int) $request->data['id'];
                $userEwallet = \App\Model\Ewallet::find($eawalletid)->decrement('amount', $totalCost);

                $ewalletTransaction = new \App\Model\Ewallettransaction;
                $ewalletTransaction->userId = $userId;
                $ewalletTransaction->ewalletId = $eawalletid;
                $ewalletTransaction->amount = $paidAmount;
                $ewalletTransaction->transactionType = 'debit';
                $ewalletTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                if ($ewalletTransaction->save())
                    $paymentStatus = 'paid';
                else
                    $paymentStatus = 'failed';


                $transactionData = json_encode(array(
                    'ewalletId' => $request->data['ewalletId'],
                        )
                );
            } else if ($request->data['paymentMethodKey'] == 'paystack_checkout') {/* PROCESS PAYSTACK DATA */
                /* Old code */
//                if (!empty($request->paystackData)) {
//                    $checkoutReturn = \App\Model\Paymenttransaction::paystack($request->paystackCreatedReference);
//                    if ($checkoutReturn) {
//                        $paymentStatus = 'paid';
//                        $trasactionId = $request->paystackData['trans'];
//                        $transactionData = json_encode($request->paystackData);
//                    }
//                } 
                $paymentMode = 'online';
                if (!empty($request->paystackData)) {
                    $checkoutReturn = \App\Model\Paymenttransaction::paystack($request->paystackCreatedReference);
                    if ($checkoutReturn) {
                        $paymentStatus = 'paid';
                        $transactionId = $request->paystackData['trans'];
                        $transactionData = json_encode($request->paystackData);
                    } else {
                        $paymentStatus = 'failed';
                        $transactionErrorMsg = json_encode($request->paystackData);
                        $paymentErrorMessage = "Payment Failed";
                    }
                }
            } elseif ($request->data['paymentMethodKey'] == 'credit_debit_card') { /*  PROCESS CREDIT CARD PAYMENT */
                $paymentMode = 'online';
                $paymentMethod = \App\Model\Paymentmethod::where('paymentMethodKey', $request->data['paymentMethodKey'])->first();
                if ($paymentMethod->paymentGatewayId == 1) {
                    $checkoutData = array();
                    $checkoutData['cardNumber'] = $request->data['ccardNumber'];
                    $checkoutData['expMonth'] = $request->data['expiryMonth'];
                    $checkoutData['expYear'] = $request->data['expiryYear'];
                    $checkoutData['cardCode'] = $request->data['cvvCode'];
                    $checkoutData['customerFirstName'] = $invoice->billingName;
                    $checkoutData['customerLastName'] = '';
                    $checkoutData['customerAddress'] = $invoice->billingAddress;
                    $checkoutData['customerCity'] = $invoice->billingCity;
                    $checkoutData['customerState'] = $invoice->billingState;
                    $checkoutData['customerCountry'] = $invoice->billingCountry;
                    $checkoutData['customerZip'] = $invoice->billingZipcode;
                    $checkoutData['amount'] = $paidAmount;
                    $checkoutData['defaultCurrency'] = $defaultCurrencyCode;

                    /* Old code */
//                    $checkoutReturn = \App\Model\Paymenttransaction::paypaypalpro($checkoutData);
//                    if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'successWithWarning')) {
//                        $paymentStatus = 'paid';
//                        $transactionId = $checkoutReturn['TRANSACTIONID'];
//                        $transactionData = json_encode($checkoutReturn);
//                    }

                    $checkoutReturn = Paymenttransaction::paypaypalpro($checkoutData);

                    if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'successWithWarning')) {
                        $paymentStatus = 'paid';
                        $transactionId = $checkoutReturn['TRANSACTIONID'];
                        $transactionData = json_encode($checkoutReturn);
                    } else if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Failure')) {
                        $paymentStatus = 'failed';
                        $transactionErrorMsg = json_encode($checkoutReturn);
                        $paymentErrorMessage = $checkoutReturn["L_LONGMESSAGE0"];
                    } else if ($checkoutReturn == 'error') {
                        $paymentStatus = 'failed';
                        $paymentErrorMessage = "Payment Error";
                    }
                } else {
                    $checkoutData['cardNumber'] = $request->data['ccardNumber'];
                    $checkoutData['expMonth'] = $request->data['expiryMonth'];
                    $checkoutData['expYear'] = $request->data['expiryYear'];
                    $checkoutData['cardCode'] = $request->data['cvvCode'];
                    $checkoutData['customerFirstName'] = $invoice->billingName;
                    $checkoutData['customerLastName'] = "";
                    $checkoutData['customerAddress'] = $invoice->billingAddress;
                    $checkoutData['customerCity'] = $invoice->billingCity;
                    $checkoutData['customerState'] = $invoice->billingState;
                    $checkoutData['customerCountry'] = $request->personaldetails['billingCountryName'];
                    $checkoutData['customerZip'] = !empty($invoice->billingZipcode) ? $invoice->billingZipcode : "";
                    $checkoutData['customerShippingAddress'] = $shippingAddress['toAddress'];
                    $checkoutData['customerShippingCity'] = $shippingAddress['toCity'];
                    $checkoutData['customerShippingState'] = $shippingAddress['toState'];
                    $checkoutData['customerShippingCountry'] = $shippingAddress['toCountry'];
                    $checkoutData['customerShippingZip'] = $shippingAddress['toZipCode'];
                    $checkoutData['amount'] = $paidAmount;
                    $checkoutData['defaultCurrency'] = $defaultCurrencyCode;
                    $checkoutData['shippingFirstName'] = $shippingAddress['toName'];
                    $checkoutData['shippingLastName'] = "";

                    /*
                      $checkoutReturn = \App\Model\Paymenttransaction::payauthorizedotnet($checkoutData);

                      if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Ok') {
                      $paymentStatus = 'paid';
                      $transactionId = $checkoutReturn['transactionResponse']['transId'];
                      $transactionData = json_encode($checkoutReturn['transactionResponse']);
                      } else {
                      $transactionData = json_encode($checkoutReturn['transactionResponse']);
                      } */

                    $checkoutReturn = Paymenttransaction::payauthorizedotnet($checkoutData);

                    if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Ok') {
                        $paymentStatus = 'paid';
                        $transactionId = $checkoutReturn['transactionResponse']['transId'];
                        $transactionData = json_encode($checkoutReturn['transactionResponse']);
                    } else if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Error') {
                        $paymentStatus = 'failed';
                        $transactionErrorMsg = json_encode($checkoutReturn['transactionResponse']);
                        if (isset($checkoutReturn['transactionResponse']['errors']['error'][0]))
                            $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['transactionResponse']['errors']['error'][0]['errorText'];
                        else
                            $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['transactionResponse']['errors']['error']['errorText'];
                    }
                }
            } else if ($request->data['paymentMethodKey'] == 'paypalstandard') {
                $paymentMode = 'online';
                if (!empty($request->paypalTransaction)) {
                    $paymentStatus = 'paid';
                    $transactionId = $request->paypalTransaction['orderID'];
                    $transactionData = json_encode($request->paypalTransaction);
                }
            }

            if ($request->data['paymentMethodKey'] == 'payeezy') {
                if (!empty($request->payeezyData)) {
                    $checkoutData['amount'] = $request->payeezyData["paidAmount"];
                    $checkoutData['method'] = $request->payeezyData["paymentType"];
                    $checkoutData['currency_code'] = "USD";
                    $checkoutData['type'] = $request->payeezyData["cardType"];
                    $checkoutData['cardholder_name'] = $request->payeezyData["cardHolderName"];
                    $checkoutData['card_number'] = $request->payeezyData['ccardNumber'];
                    $checkoutData['exp_date'] = $request->payeezyData['expiryMonth'] . substr($request->payeezyData['expiryYear'], 2);
                    $checkoutData['cvv'] = $request->payeezyData['cvvCode'];
                    $checkoutData['userUnit'] = (!empty($userData->unit) ? $userData->unit : "");
                    $checkoutReturn = Paymenttransaction::processpayeezy($checkoutData);

                    if (is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'success' && !empty($checkoutReturn['transaction_status']) && $checkoutReturn['transaction_status'] == 'approved') {
                        $paymentStatus = 'paid';
                        $paymentMode = 'online';
                        $transactionId = $checkoutReturn['transaction_id'];
                        $transactionData = json_encode($checkoutReturn);
                    } else if (is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'failed') {
                        $paymentStatus = 'failed';
                        $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['Error']['messages'][0]['description'];
                    } else {
                        $paymentStatus = 'failed';
                        $paymentErrorMessage = 'Payment failed. Something Went Wrong ';
                    }
                } else {
                    $paymentStatus = 'error';
                }
            }
            $paymentMethodId = "";
            if (empty($request->data['paymentMethodId'])) {
                $paymentMethodDetails = \App\Model\Paymentmethod::where('paymentMethodKey', $request->data['paymentMethodKey'])->first();
                if (!empty($paymentMethodDetails)) {
                    $paymentMethodId = $paymentMethodDetails->id;
                }
            } else
                $paymentMethodId = $request->data['paymentMethodId'];

            $invoiceParticulars['payment'] = array(
                'paymentMethodId' => $paymentMethodId,
                'paymentMethodName' => $request->data['paymentMethodName'],
            );
            $invoiceParticulars['shipment']['totalTax'] = $request->data['taxAmount'];
            $invoiceParticulars['shipment']['totalCost'] = $request->data['paidAmount'];
            if ($paymentStatus == 'paid')
                $newInvoice->invoiceType = 'receipt';
            $invoiceUniqueId = 'REC' . $userData->unit . '-' . $fillshipShipmentId . '-' . date('Ymd');
            $newInvoice->invoiceUniqueId = $invoiceUniqueId;
            $newInvoice->createdOn = Config::get('constants.CURRENTDATE');
            $newInvoice->extraCostCharged = 'Y';
            //$newInvoice->extraCostAmount = $request->data['paidAmount'];
            $newInvoice->extraCostAmount = round(($request->data['paidAmount'] - $request->data['taxAmount']), 2);
            $newInvoice->invoiceParticulars = json_encode($invoiceParticulars);
            $newInvoice->paymentMethodId = $paymentMethodId;
            $newInvoice->paymentStatus = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
            ;
            $newInvoice->totalBillingAmount = $request->data['paidAmount'];


            if ($paymentStatus == 'paid') {
                if ($newInvoice->save()) {

                    $paymentTransaction = new \App\Model\Paymenttransaction;
                    $paymentTransaction->userId = $userId;
                    $paymentTransaction->paymentMethodId = $paymentMethodId;
                    $paymentTransaction->paidFor = $newInvoice->type;
                    $paymentTransaction->paidForId = $newInvoice->shipmentId;
                    $paymentTransaction->invoiceId = $newInvoice->id;
                    $paymentTransaction->amountPaid = $request->data['paidAmount'];
                    $paymentTransaction->status = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
                    ;
                    if (!empty($transactionData))
                        $paymentTransaction->transactionData = $transactionData;
                    if (!empty($transactionId))
                        $paymentTransaction->transactionId = $transactionId;
                    $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                    $paymentTransaction->save();

                    $invoiceUniqueId = $newInvoice->invoiceUniqueId;
                    $fileName = "Receipt_" . $invoiceUniqueId . ".pdf";

                    $data['invoice'] = $newInvoice;
                    $data['pageTitle'] = "Print Invoice";

                    PDF::loadView('Administrator.fillnship.extrainvoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');

                    $to = $newInvoice->billingEmail;
                    Mail::send(['html' => 'mail'], ['content' => strtoupper($newInvoice->invoiceType) . ' for #' . $invoiceUniqueId], function ($message) use($invoiceUniqueId, $to, $fileName) {
                        $message->from('contact@shoptomydoor.com', 'Shoptomydoor');
                        $message->subject("$invoiceUniqueId - Invoice");
                        $message->to($to);
                        $message->attach(public_path('exports/invoice/' . $fileName));
                    });
                    Fillship::proceedShipout($fillshipShipmentId, $userId, $ewalletTransferAmount);
                    return response()->json([
                                'status' => '1',
                                'results' => 'success',
                    ]);
                }
            } else {

                $paymentTransaction = new \App\Model\Paymenttransaction;
                $paymentTransaction->userId = $userId;
                $paymentTransaction->paymentMethodId = $paymentMethodId;
                $paymentTransaction->paidFor = $invoice->type;
                $paymentTransaction->amountPaid = $request->data['paidAmount'];
                $paymentTransaction->status = $paymentStatus;
                if (!empty($transactionErrorMsg))
                    $paymentTransaction->errorMsg = $transactionErrorMsg;
                $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                $paymentTransaction->save();

                return response()->json([
                            'status' => '-1',
                            'results' => $paymentErrorMessage,
                ]);
            }
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => 'error',
            ]);
        }
    }

    public function fillshipdeliverydetails(Request $request) {
        $fillshipItemId = $request->fillshipItemId;
        $deliveryCompanyInfo = new \App\Model\Deliverycompany;
        $fillshipItem = new Fillshipitem;
        $itemDetails = Fillshipitem::select(array("$fillshipItem->table.status", 'deliveryCompanyId', 'name', 'receivedDate', 'trackingNumber', 'tracking2', 'deliveryNotes'))
                        ->leftJoin($deliveryCompanyInfo->table, "$deliveryCompanyInfo->table.id", "=", "deliveryCompanyId")
                        ->where("$fillshipItem->table.id", $fillshipItemId)->first();
        if (!empty($itemDetails) && $itemDetails->status == 'received') {
            return response()->json([
                        'status' => '1',
                        'results' => $itemDetails,
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => array(),
            ]);
        }
    }

    public function alladdedboxes() {

        $allData = Fillshipbox::where("deleted", "0")->orderBy("orderby", "asc")->get();
        if (!empty($allData)) {
            foreach ($allData as $eachDataIndex => $eachData) {
                $allData[$eachDataIndex]->uploadedImage = url('uploads/fillship/' . $eachData->uploadedImage);
            }

            return response()->json([
                        'status' => '1',
                        'results' => $allData,
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => array(),
            ]);
        }
    }

}
