<?php
namespace App\Http\Controllers\Api;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Generalsettings;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class HeaderController extends Controller {

    public function getsettings($settingsKey = '') {      

        DB::enableQueryLog();  

        $data = Generalsettings::select(array('settingsValue', 'settingsTitle'))->where('groupName', 'siteSettings')->where('subGroupName', 'General Parameters')->where('settingsKey', $settingsKey)->get();

        //dd(DB::getQueryLog());



        return json_encode(array('results' => $data));
    }
  
      public function getemail($settingsKey = '') {      

        DB::enableQueryLog();  

        $data = Generalsettings::select(array('settingsValue', 'settingsTitle'))->where('groupName', 'siteSettings')->where('subGroupName', 'General Parameters')->where('settingsKey', $settingsKey)->get();

        //dd(DB::getQueryLog());



        return json_encode(array('results' => $data));
    }
}
