<?php

namespace App\Http\Controllers\Api;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Usercart;
use App\Model\Procurement;
use App\Model\Procurementitem;
use App\Model\Procurementitemstatus;
use App\Model\Paymenttransaction;
use App\Model\Shipment;
use App\Model\Shippingcharges;
use Config;
use Auth;
use customhelper;
use DB;
use PDF;
use Mail;

class ProcurementController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->_perPage = 10;
    }

    /**
     * Method used to submit procurement data
     * @param integer $userId
     * @param object $request
     * @return boolean
     */
    public function submitprocurementdata($userId, Request $request) {

        $totalWeight = $totalQuantity = 0;
        $shippingId = (int) $request->shippingId;
        $param = array();

        /* Fetch User Addressbook Details */
        $addressBookData = User::find($userId)->addressbook()->where('isDefaultShipping', '1')->first();

        /* Fetch Warehouse Details */
        $warehouseData = \App\Model\Warehouse::find($request->warehouseId);

        /* Fetch User Details */
        $userData = User::find($userId);

        $totalProcurementCost = customhelper::getExtractCurrency($request->paymentDetails['totalCost']);

        if ($shippingId == -1) {
            /* Insert Data into Main Procurement Table */
            $procurement = new Procurement;
            $procurement->userId = $userId;
            $procurement->warehouseId = $request->warehouseId;
            $procurement->fromCountry = $warehouseData->countryId;
            $procurement->fromState = $warehouseData->stateId;
            $procurement->fromCity = $warehouseData->cityId;
            $procurement->fromAddress = $warehouseData->address;
            $procurement->fromZipCode = $warehouseData->zipcode;
            $procurement->fromPhone = $userData->contactNumber;
            $procurement->fromName = $userData->firstName . " " . $userData->lastName;
            $procurement->fromEmail = $userData->email;
            $procurement->toCountry = $addressBookData->countryId;
            $procurement->toState = $addressBookData->stateId;
            $procurement->toCity = $addressBookData->cityId;
            $procurement->toAddress = $addressBookData->address;
            $procurement->toZipCode = $addressBookData->zipcode;
            $procurement->toPhone = $addressBookData->phone;
            $procurement->toName = $addressBookData->firstName . " " . $addressBookData->lastName;
            $procurement->toEmail = $addressBookData->email;
            $procurement->procurementType = 'shopforme';
            $procurement->urgent = $request->urgent;
            $procurement->totalItemCost = customhelper::getExtractCurrency($request->paymentDetails['totalItemCost']);
            $procurement->totalProcessingFee = customhelper::getExtractCurrency($request->paymentDetails['totalProcessingFee']);
            $procurement->urgentPurchaseCost = customhelper::getExtractCurrency($request->paymentDetails['totalUrgentCost']);
            $procurement->totalProcurementCost = $totalProcurementCost;
            $procurement->status = 'requestforcost';
            $procurement->paymentStatus = 'unpaid';
            $procurement->paymentReceivedOn = Config::get('constants.CURRENTDATE');
            $procurement->createdBy = $userId;
            $procurement->createdByType = 'user';
            $procurement->createdOn = Config::get('constants.CURRENTDATE');
            $procurement->orderDate = Config::get('constants.CURRENTDATE');
            $procurement->save();
            $procurementId = $procurement->id;
            /* Insert Data into Main Procurement Table */

            if (!empty($request->items)) {
                foreach ($request->items as $item) {
                    if (!empty($item['siteProductId'])) {
                        $siteProduct = \App\Model\Siteproduct::find($item['siteProductId']);
                        $totalWeight += ($siteProduct->weight * $item['itemQuantity']);
                    }

                    $totalQuantity += $item['itemQuantity'];

                    /* INSERT DATA INTO PROCUREMENT ITEM TABLE */
                    $pocurementitem = new Procurementitem;
                    $pocurementitem->procurementId = $procurementId;
                    $pocurementitem->itemName = $item['itemName'];
                    $pocurementitem->websiteUrl = $item['websiteUrl'];
                    $pocurementitem->storeId = (!empty($item['storeId']) ? $item['storeId'] : 0);
                    $pocurementitem->siteCategoryId = $item['siteCategoryId'];
                    $pocurementitem->siteSubCategoryId = $item['siteSubCategoryId'];
                    $pocurementitem->siteProductId = (!empty($item['siteProductId']) ? $item['siteProductId'] : 0);
                    $pocurementitem->options = $item['options'];
                    $pocurementitem->itemPrice = customhelper::getExtractCurrency($item['itemPrice']);
                    $pocurementitem->itemQuantity = $item['itemQuantity'];
                    $pocurementitem->itemImage = isset($item['itemImage']) ? $item['itemImage'] : '';
                    $pocurementitem->itemShippingCost = customhelper::getExtractCurrency($item['itemShippingCost']);
                    $pocurementitem->itemTotalCost = customhelper::getExtractCurrency($item['itemTotalCost']);
                    $pocurementitem->save();

                    $procurementItemId = $pocurementitem->id;

                    /* INSERT DATA INTO STATUS LOG TABLE */
                    $procurementitemstatus = new Procurementitemstatus;
                    $procurementitemstatus->procurementId = $procurementId;
                    $procurementitemstatus->procurementItemId = $procurementItemId;
                    $procurementitemstatus->oldStatus = '';
                    $procurementitemstatus->status = 'submitted';
                    $procurementitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
                    $procurementitemstatus->save();

                    /* DELETE USER CART ITEMS */
                    $userCart = Usercart::find($item['id']);
                    $userCart->delete();
                }
            }

            /* Fetch Shipping Charge Details */

            $procurement = Procurement::find($procurementId);
            $procurement->totalWeight = $totalWeight;
            $procurement->totalQuantity = $totalQuantity;
            $procurement->shippingMethod = 'N';
            $procurement->totalCost = $totalProcurementCost;
            $procurement->procurementLocked = 'N';
            $procurement->save();

            return response()->json([
                        'status' => '1',
                        'results' => 'success',
                        'procurementId' => $procurementId,
                        'procurement' => '',
            ]);
        } else {
            $procurement = $procurement['items'] = array();
            $totalQuantity = 0;

            $procurement['isCurrencyChanged'] = 'N';
            $procurement['defaultCurrencySymbol'] = $procurement['currencySymbol'] = customhelper::getCurrencySymbolCode();
            $procurement['defaultCurrencyCode'] = $procurement['currencyCode'] = customhelper::getCurrencySymbolCode('', true);
            if (!empty($request->items)) {
                foreach ($request->items as $key => $item) {
                    if (!empty($item['siteProductId'])) {
                        $siteProduct = \App\Model\Siteproduct::find($item['siteProductId']);
                        $totalWeight += ($siteProduct->weight * $item['itemQuantity']);
                    }

                    $totalQuantity += $item['itemQuantity'];

                    /* SET DATA INTO SESSION */
                    $procurement['items'][$key]['itemName'] = $item['itemName'];
                    $procurement['items'][$key]['websiteUrl'] = $item['websiteUrl'];
                    $procurement['items'][$key]['storeId'] = (!empty($item['storeId']) ? $item['storeId'] : 0);
                    $procurement['items'][$key]['siteCategoryId'] = $item['siteCategoryId'];
                    $procurement['items'][$key]['siteSubCategoryId'] = $item['siteSubCategoryId'];
                    $procurement['items'][$key]['siteProductId'] = (!empty($item['siteProductId']) ? $item['siteProductId'] : 0);
                    $procurement['items'][$key]['options'] = $item['options'];
                    $procurement['items'][$key]['itemPrice'] = customhelper::getExtractCurrency($item['itemPrice']);
                    $procurement['items'][$key]['itemQuantity'] = $item['itemQuantity'];
                    $procurement['items'][$key]['itemShippingCost'] = customhelper::getExtractCurrency($item['itemShippingCost']);
                    $procurement['items'][$key]['itemTotalCost'] = customhelper::getExtractCurrency($item['itemTotalCost']);
                    $procurement['items'][$key]['itemImage'] = isset($item['itemImage']) ? $item['itemImage'] : '';
                }
            }

            $procurement['data']['warehouseId'] = $request->warehouseId;
            $procurement['data']['urgent'] = $request->urgent;
            $procurement['data']['totalItemCost'] = customhelper::getExtractCurrency($request->paymentDetails['totalItemCost']);
            $procurement['data']['totalProcessingFee'] = customhelper::getExtractCurrency($request->paymentDetails['totalProcessingFee']);
            $procurement['data']['urgentPurchaseCost'] = customhelper::getExtractCurrency($request->paymentDetails['totalUrgentCost']);
            $procurement['data']['totalQuantity'] = $totalQuantity;
            $procurement['data']['totalWeight'] = $totalWeight;
            $procurement['data']['totalProcurementCost'] = $totalProcurementCost;
            $procurement['data']['isInsuranceCharged'] = 'N';
            $procurement['data']['totalTax'] = 0;
            $procurement['data']['totalDiscount'] = 0;
            $procurement['data']['totalBDiscountCost'] = 0;

            if ($shippingId > 0) {
                $param['userId'] = $userId;
                $param['totalWeight'] = $totalWeight;
                $param['totalProcurementCost'] = $procurement['data']['totalItemCost'];
                $param['toCountry'] = $addressBookData->countryId;
                $param['toState'] = $addressBookData->stateId;
                $param['toCity'] = $addressBookData->cityId;
                $param['fromCountry'] = $warehouseData->countryId;
                $param['fromState'] = $warehouseData->stateId;
                $param['fromCity'] = $warehouseData->cityId;
                $param['shippingId'] = $shippingId;

                $procurement['data']['shipmentMethodId'] = $shippingId;
                $shippingChargeData = Shippingcharges::getShippingChargeData($param);
                $shippingMethod = \App\Model\Shippingmethods::where('shippingid', $shippingId)->first();
                if (!empty($shippingChargeData)) {
                    $shippingChargeDetails = Shippingcharges::calculateShippingCharge($param, $shippingChargeData);

                    $procurement['data']['shippingCost'] = $shippingChargeDetails['shippingCost'];
                    if (!empty($shippingChargeDetails['isDutyCharged'])) {
                        $totalCost = trim($totalProcurementCost) + $shippingChargeDetails['shippingCost'] + $shippingChargeDetails['clearing'] + $shippingChargeDetails['duty'] + $shippingChargeDetails['insurance'];
                        $procurement['data']['totalClearingDuty'] = $shippingChargeDetails['clearing'] + $shippingChargeDetails['duty'];
                        $procurement['data']['totalShippingCost'] = $shippingChargeDetails['clearing'] + $shippingChargeDetails['duty'] + $shippingChargeDetails['shippingCost'];
                    } else {
                        $totalCost = trim($totalProcurementCost) + $shippingChargeDetails['shippingCost'] + $shippingChargeDetails['clearing'] + $shippingChargeDetails['insurance'];
                        $procurement['data']['totalClearingDuty'] = $shippingChargeDetails['clearing'];
                        $procurement['data']['totalShippingCost'] = $shippingChargeDetails['clearing'] + $shippingChargeDetails['shippingCost'];
                    }
                    $procurement['data']['totalInsurance'] = $shippingChargeDetails['insurance'];
                    $procurement['data']['isInsuranceCharged'] = 'Y';
                    $procurement['data']['isDutyCharged'] = !empty($shippingChargeDetails['isDutyCharged']) ? 'Y' : 'N';
                    $procurement['data']['totalCost'] = round($totalCost, 2);
                    $procurement['data']['shippingMethodName'] = $shippingMethod->shipping;
                    $procurement['data']['estimateDeliveryDate'] = \Carbon\Carbon::now()->addWeekDays($shippingMethod->days)->format('Y-m-d');
                }
            } else {
                $procurement['data']['shipmentMethodId'] = '';
                $procurement['data']['totalCost'] = $totalProcurementCost;
            }

            return response()->json([
                        'status' => '1',
                        'results' => 'success',
                        'procurementId' => '',
                        'procurement' => $procurement,
            ]);
        }
    }

    /**
     * Method used to recalculate and display Shipment Cost
     * @param object $request
     * @return boolean
     */
    public function recalculateshipmentcost(Request $request) {
        $totalItemCost = $totalProcessingFee = $totalUrgentCost = $totalCost = 0;
        $userId = $request->userId;
        $urgentCostCharged = false;

        $data = array();


        $userCartList = Usercart::where('userId', $userId)->where('savedForLater', 'N')->where('type', $request->type)->get()->toArray();
        if (!empty($userCartList)) {
            foreach ($userCartList as $count => $row) {
                foreach ($row as $key => $value) {
                    if ($key == 'cartContent') {
                        $cartContent = json_decode($value);
                        if (!empty($cartContent)) {
                            foreach ($cartContent as $key => $value) {
                                if ($key == 'itemTotalCost') {
                                    $totalItemCost += $value;
                                }
                            }
                        }
                    }
                }
            }

            /* Calculate procurement processing fee */
            $param = array('warehouseId' => $request->warehouseId, 'totalItemCost' => $totalItemCost);
            $totalProcessingFee = Procurement::calculateProcessingFee($param);
            $totalCost = $totalItemCost + $totalProcessingFee;

            /* Calculate urgent fee */
            $warehouse = \App\Model\Warehouse::find($request->warehouseId);
            $urgentPurchaseCostMin = $warehouse->urgentPurchaseCost;

            /* Calculate procurement urgent fee */
            if ($request->urgent == 'Y') {
                $urgentPurchaseCost = ($warehouse->urgentPurchase / 100) * $totalItemCost;

                if ($totalItemCost >= $urgentPurchaseCostMin) {
                    $totalUrgentCost = $urgentPurchaseCost;
                    $urgentCostCharged = true;
                }

                $totalCost = $totalCost + $totalUrgentCost;
            }

            /* SET RESPONSE DATA */
            $data['details'] = array(
                'totalItemCost' => customhelper::getCurrencySymbolFormat($totalItemCost),
                'totalProcessingFee' => customhelper::getCurrencySymbolFormat($totalProcessingFee),
                'totalUrgentCost' => customhelper::getCurrencySymbolFormat($totalUrgentCost),
                'urgentCostCharged' => $urgentCostCharged,
                'minUrgentCost' => customhelper::getCurrencySymbolFormat($urgentPurchaseCostMin),
                'totalCost' => customhelper::getCurrencySymbolFormat($totalCost),
            );

            return response()->json([
                        'status' => '1',
                        'results' => $data,
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => '',
            ]);
        }
    }

    /**
     * Method used to calculate and display Shipment Cost
     * @param object $request
     * @return boolean
     */
    public function calculateshippingcost(Request $request) {
        $totalItemQuantity = $totalWeight = $totalItemCost = $totalProcessingFee = $totalUrgentCost = $totalProcurementCost = 0;
        $data = $shippingMethodData = array();
        $userId = $request->userId;
        $requestShippingCost = array();

        $userCartList = Usercart::where('userId', $userId)->where('savedForLater', 'N')->where('type', $request->type)->get()->toArray();

        if (!empty($userCartList)) {
            foreach ($userCartList as $count => $row) {
                foreach ($row as $key => $value) {
                    if ($key == 'cartContent') {
                        $cartContent = json_decode($value);
                        if (!empty($cartContent)) {
                            $itemQuantity = $cartContent->itemQuantity;
                            if (!empty($cartContent->siteProductId)) {
                                $siteProduct = \App\Model\Siteproduct::find($cartContent->siteProductId);
                                $itemWeight = $siteProduct->weight;
                                $totalWeight += ($itemQuantity * $itemWeight);
                            }

                            foreach ($cartContent as $key => $value) {
                                if ($key == 'itemTotalCost') {
                                    $totalItemCost += $value;
                                }
                                if ($key == 'siteProductId') {
                                    if (!empty($value)) {
                                        $requestShippingCost[] = false;
                                    } else {
                                        $requestShippingCost[] = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (!empty($requestShippingCost) && !in_array(true, $requestShippingCost)) {
                /* Calculate procurement processing fee */

                /* $param = array('warehouseId' => $request->warehouseId, 'totalItemCost' => $totalItemCost);
                  $totalProcessingFee = Procurement::calculateProcessingFee($param);
                  $totalProcurementCost = $totalItemCost + $totalProcessingFee; */

                /* Calculate procurement urgent fee */
                /*  if ($request->urgent == 'Y') {
                  $warehouse = \App\Model\Warehouse::find($request->warehouseId);
                  $urgentPurchaseCostMin = $warehouse->urgentPurchaseCost;
                  $urgentPurchaseCost = ($warehouse->urgentPurchase / 100) * $totalItemCost;

                  /* $param = array('warehouseId' => $request->warehouseId, 'totalItemCost' => $totalItemCost);
                  $totalProcessingFee = Procurement::calculateProcessingFee($param);
                  $totalProcurementCost = $totalItemCost + $totalProcessingFee; */

                /* Calculate procurement urgent fee */
                /*  if ($request->urgent == 'Y') {
                  $warehouse = \App\Model\Warehouse::find($request->warehouseId);
                  $urgentPurchaseCostMin = $warehouse->urgentPurchaseCost;
                  $urgentPurchaseCost = ($warehouse->urgentPurchase / 100) * $totalItemCost;


                  if ($urgentPurchaseCost < $urgentPurchaseCostMin)
                  $totalUrgentCost = $urgentPurchaseCostMin;
                  else
                  $totalUrgentCost = $urgentPurchaseCost;


                  $totalProcurementCost = $totalProcurementCost + $totalUrgentCost;
                  } */

                /* Fetch User Addressbook Details */
                $addressBookData = User::find($userId)->addressbook()->where('isDefaultShipping', '1')->first();

                /* Fetch Warehouse Details */
                $warehouseData = \App\Model\Warehouse::find($request->warehouseId);

                /*  FETCH PROCUREMENT SHIPPING METHOD CHARGES DETAILS */
                $param = array(
                    'userId' => $userId,
                    'fromCountry' => $warehouseData->countryId,
                    'fromState' => $warehouseData->stateId,
                    'fromCity' => $warehouseData->cityId,
                    'toCountry' => $addressBookData->countryId,
                    'toState' => $addressBookData->stateId,
                    'toCity' => $addressBookData->cityId,
                    'totalWeight' => $totalWeight,
                    'totalProcurementCost' => $totalItemCost,
                    'totalQuantity' => count($userCartList),
                );

                $shippingMethodCharges = \App\Model\Shippingmethods::calculateShippingMethodCharges($param);



                if (!empty($shippingMethodCharges)) {
                    foreach ($shippingMethodCharges as $key => $row) {
                        foreach ($row as $field => $value) {
                            if ($field == 'companyLogo') {
                                $shippingMethodData[$key][$field] = asset('uploads/shipping/' . $value);
                            } else if ($field == 'shippingCost' || $field == 'duty' || $field == 'clearing' || $field == 'clearingDuty' || $field == 'totalShippingCost') {
                                $shippingMethodData[$key][$field] = customhelper::getCurrencySymbolFormat($value);
                            } else if ($field == 'days') {
                                $shippingMethodData[$key][$field] = $value;
                                $shippingMethodData[$key]['estimatedDeliveryDate'] = \Carbon\Carbon::now()->addWeekDays($value)->format('D, M dS Y');
                            } else {
                                $shippingMethodData[$key][$field] = $value;
                            }
                        }
                    }
                }
            }

            if (!empty($shippingMethodData)) {
                return response()->json([
                            'status' => '1',
                            'results' => 'success',
                            'data' => $shippingMethodData
                ]);
            } else {
                return response()->json([
                            'status' => '0',
                            'results' => 'success'
                ]);
            }
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => 'success'
            ]);
        }
    }

    /* Save Auto Parts as New Procurement */

    public function submitautopartsdata($userId, Request $request) {

        //print_r($request->all()); die;

        $totalWeight = 0;
        $shippingMethodId = (int) $request->shippingMethodId;
        $param = array();

        /* Fetch User Addressbook Details */
        $addressBookData = User::find($userId)->addressbook()->where('isDefaultShipping', '1')->first();

        /* Fetch Warehouse Details */
        $warehouseData = \App\Model\Warehouse::find($request->warehouseId);

        /* Fetch User Details */
        $userData = User::find($userId);

        $totalProcurementCost = (float) substr($request->paymentDetails['totalCost'], 1);



        if ($shippingMethodId == -1) {

            /* Insert Data into Main Procurement Table */
            $procurement = new Procurement;
            $procurement->userId = $userId;
            $procurement->warehouseId = $request->warehouseId;
            $procurement->fromCountry = $warehouseData->countryId;
            $procurement->fromState = $warehouseData->stateId;
            $procurement->fromCity = $warehouseData->cityId;
            $procurement->fromAddress = $warehouseData->address;
            $procurement->fromZipCode = $warehouseData->zipcode;
            $procurement->fromPhone = $userData->contactNumber;
            $procurement->fromName = $userData->firstName . " " . $userData->lastName;
            $procurement->fromEmail = $userData->email;
            $procurement->toCountry = $addressBookData->countryId;
            $procurement->toState = $addressBookData->stateId;
            $procurement->toCity = $addressBookData->cityId;
            $procurement->toAddress = $addressBookData->address;
            $procurement->toZipCode = $addressBookData->zipcode;
            $procurement->toPhone = $addressBookData->phone;
            $procurement->toName = $addressBookData->firstName . " " . $addressBookData->lastName;
            $procurement->toEmail = $addressBookData->email;
            $procurement->procurementType = 'autopart';
            $procurement->urgent = $request->urgent;
            $procurement->totalItemCost = trim(customhelper::getExtractCurrency($request->paymentDetails['totalItemCost']));
            $procurement->totalProcessingFee = trim(customhelper::getExtractCurrency($request->paymentDetails['totalProcessingFee']));
            $procurement->urgentPurchaseCost = trim(customhelper::getExtractCurrency($request->paymentDetails['totalUrgentCost']));
            $procurement->totalProcurementCost = $totalProcurementCost;
            $procurement->status = 'requestforcost';
            $procurement->paymentStatus = 'unpaid';
            $procurement->paymentReceivedOn = Config::get('constants.CURRENTDATE');
            $procurement->createdBy = $userId;
            $procurement->createdByType = 'user';
            $procurement->createdOn = Config::get('constants.CURRENTDATE');
            $procurement->orderDate = Config::get('constants.CURRENTDATE');
            $procurement->save();
            $procurementId = $procurement->id;
            /* Insert Data into Main Procurement Table */

            //print_r($request->items); die;


            if (!empty($request->items)) {
                foreach ($request->items as $item) {
                    if (!empty($item['siteProductId'])) {
                        $siteProduct = \App\Model\Siteproduct::find($item['siteProductId']);
                        $totalWeight += $siteProduct->weight;
                    }

                    /* INSERT DATA INTO PROCUREMENT ITEM TABLE */
                    $pocurementitem = new Procurementitem;
                    $pocurementitem->procurementId = $procurementId;
                    $pocurementitem->itemName = $item['itemName'];
                    $pocurementitem->websiteUrl = $item['websiteUrl'];
                    $pocurementitem->storeId = (!empty($item['storeId']) ? $item['storeId'] : 0);
                    $pocurementitem->siteCategoryId = $item['siteCategoryId'];
                    $pocurementitem->siteSubCategoryId = $item['siteSubCategoryId'];
                    $pocurementitem->siteProductId = (!empty($item['siteProductId']) ? $item['siteProductId'] : 0);
                    // $pocurementitem->options = $item['options'];
                    $pocurementitem->itemDescription = $item['itemDescription'];
                    $pocurementitem->year = $item['itemYear'];
                    $pocurementitem->itemMake = $item['itemMake'];
                    $pocurementitem->itemModel = $item['itemModel'];
                    $pocurementitem->itemImage = (empty($item['itemImage']) ? "" : $item['itemImage']);
                    $pocurementitem->itemPrice = trim(customhelper::getExtractCurrency($item['itemPrice']));
                    $pocurementitem->itemQuantity = $item['itemQuantity'];
                    $pocurementitem->itemShippingCost = trim(customhelper::getExtractCurrency($item['itemShippingCost']));
                    $pocurementitem->itemTotalCost = trim(customhelper::getExtractCurrency($item['itemTotalCost']));
                    $pocurementitem->save();

                    $procurementItemId = $pocurementitem->id;

                    /* INSERT DATA INTO STATUS LOG TABLE */
                    $procurementitemstatus = new Procurementitemstatus;
                    $procurementitemstatus->procurementId = $procurementId;
                    $procurementitemstatus->procurementItemId = $procurementItemId;
                    $procurementitemstatus->oldStatus = '';
                    $procurementitemstatus->status = 'submitted';
                    $procurementitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
                    $procurementitemstatus->save();

                    /* DELETE USER CART ITEMS */
                    $userCart = Usercart::find($item['id']);
                    $userCart->delete();
                }
            }

            /* Fetch Shipping Charge Details */

            $procurement = Procurement::find($procurementId);
            $procurement->totalWeight = $totalWeight;
            $procurement->shippingMethod = 'N';
            $procurement->totalCost = $totalProcurementCost;
            $procurement->procurementLocked = 'N';
            $procurement->save();


            return response()->json([
                        'status' => '1',
                        'results' => 'success',
                        'procurementId' => $procurementId,
                        'procurement' => '',
            ]);
        } else {
            $procurement = $procurement['items'] = array();

            $totalQuantity = 0;
            $procurement['isCurrencyChanged'] = 'N';
            $procurement['defaultCurrencySymbol'] = $procurement['currencySymbol'] = customhelper::getCurrencySymbolCode();
            $procurement['defaultCurrencyCode'] = $procurement['currencyCode'] = customhelper::getCurrencySymbolCode('', true);

            /* MAKE AN DATASET TO STORE DATA IN LOCAL STORAGE */

            // print_r($request->items); die;
            if (!empty($request->items)) {
                foreach ($request->items as $key => $item) {
                    if (!empty($item['siteProductId'])) {
                        $siteProduct = \App\Model\Siteproduct::find($item['siteProductId']);
                        $totalWeight += $siteProduct->weight;
                    }

                    $totalQuantity += 1;

                    /* SET DATA INTO SESSION */
                    $procurement['items'][$key]['itemName'] = $item['itemName'];
                    $procurement['items'][$key]['websiteUrl'] = $item['websiteUrl'];
                    $procurement['items'][$key]['storeId'] = (!empty($item['storeId']) ? $item['storeId'] : 0);
                    $procurement['items'][$key]['siteCategoryId'] = $item['siteCategoryId'];
                    $procurement['items'][$key]['siteSubCategoryId'] = $item['siteSubCategoryId'];
                    $procurement['items'][$key]['siteProductId'] = (!empty($item['siteProductId']) ? $item['siteProductId'] : 0);
                    //$procurement['items'][$key]['options'] = $item['options'];
                    $procurement['items'][$key]['itemDescription'] = $item['itemDescription'];
                    $procurement['items'][$key]['year'] = $item['itemYear'];
                    $procurement['items'][$key]['itemMake'] = $item['itemMake'];
                    $procurement['items'][$key]['itemModel'] = $item['itemModel'];
                    $procurement['items'][$key]['itemImage'] = (!empty($item['itemImage']) ? $item['itemImage'] : "");
                    $procurement['items'][$key]['itemPrice'] = trim(customhelper::getExtractCurrency($item['itemPrice']));
                    $procurement['items'][$key]['itemQuantity'] = $item['itemQuantity'];
                    $procurement['items'][$key]['itemShippingCost'] = trim(customhelper::getExtractCurrency($item['itemShippingCost']));
                    $procurement['items'][$key]['itemTotalCost'] = trim(customhelper::getExtractCurrency($item['itemTotalCost']));
                }
            }
            $procurement['data']['warehouseId'] = $request->warehouseId;
            $procurement['data']['urgent'] = $request->urgent;
            $procurement['data']['totalItemCost'] = trim(customhelper::getExtractCurrency($request->paymentDetails['totalItemCost']));
            $procurement['data']['totalProcessingFee'] = trim(customhelper::getExtractCurrency($request->paymentDetails['totalProcessingFee']));
            $procurement['data']['urgentPurchaseCost'] = trim(customhelper::getExtractCurrency($request->paymentDetails['totalUrgentCost']));
            $procurement['data']['totalQuantity'] = $totalQuantity;
            $procurement['data']['totalWeight'] = $totalWeight;
            $procurement['data']['totalProcurementCost'] = $totalProcurementCost;
            $procurement['data']['totalTax'] = 0;

            if ($shippingMethodId > 0) {
                $param['userId'] = $userId;
                $param['totalWeight'] = $totalWeight;
                $param['totalProcurementCost'] = $procurement['data']['totalItemCost'];
                $param['totalQuantity'] = count($request->items);
                $param['toCountry'] = $addressBookData->countryId;
                $param['toState'] = $addressBookData->stateId;
                $param['toCity'] = $addressBookData->cityId;
                $param['fromCountry'] = $warehouseData->countryId;
                $param['fromState'] = $warehouseData->stateId;
                $param['fromCity'] = $warehouseData->cityId;
                $param['shippingId'] = $shippingMethodId;

                $procurement['data']['shipmentMethodId'] = $shippingMethodId;
                $shippingChargeData = Shippingcharges::getShippingChargeData($param);
                $shippingMethod = \App\Model\Shippingmethods::where('shippingid', $shippingMethodId)->first();
                if (!empty($shippingChargeData)) {
                    $shippingChargeDetails = Shippingcharges::calculateShippingCharge($param, $shippingChargeData);
                    $procurement['data']['shippingCost'] = $shippingChargeDetails['shippingCost'];
                    $procurement['data']['totalInsurance'] = $shippingChargeDetails['insurance'];

                    if (!empty($shippingChargeDetails['isDutyCharged'])) {
                        $totalCost = trim($totalProcurementCost) + $shippingChargeDetails['shippingCost'] + $shippingChargeDetails['clearing'] + $shippingChargeDetails['duty'] + $shippingChargeDetails['insurance'];
                        $procurement['data']['totalClearingDuty'] = $shippingChargeDetails['clearing'] + $shippingChargeDetails['duty'];
                        $procurement['data']['totalShippingCost'] = $shippingChargeDetails['clearing'] + $shippingChargeDetails['duty'] + $shippingChargeDetails['shippingCost'];
                    } else {
                        $totalCost = trim($totalProcurementCost) + $shippingChargeDetails['shippingCost'] + $shippingChargeDetails['clearing'] + $shippingChargeDetails['insurance'];
                        $procurement['data']['totalClearingDuty'] = $shippingChargeDetails['clearing'];
                        $procurement['data']['totalShippingCost'] = $shippingChargeDetails['clearing'] + $shippingChargeDetails['shippingCost'];
                    }

                    $procurement['data']['isInsuranceCharged'] = 'Y';
                    $procurement['data']['isDutyCharged'] = !empty($shippingChargeDetails['duty']) ? 'Y' : 'N';
                    $procurement['data']['totalCost'] = round($totalCost, 2);
                    ;
                    $procurement['data']['shippingMethodName'] = $shippingMethod->shipping;
                    $procurement['data']['estimateDeliveryDate'] = \Carbon\Carbon::now()->addWeekDays($shippingMethod->days)->format('Y-m-d');
                }
            } else {
                $procurement['data']['shippingMethodId'] = '';
                $procurement['data']['totalCost'] = $totalProcurementCost;
            }



            return response()->json([
                        'status' => '1',
                        'results' => 'success',
                        'procurementId' => '',
                        'procurement' => $procurement,
            ]);
        }
    }

    /* Process the Procurement as paid */

    public function processprocurementpaid($id) {
        $procurement = Procurement::find($id);
        $procurement->paymentStatus = 'paid';
        $procurement->paymentMethodId = 1;
        $procurement->save();
        if ($id) {
            return response()->json([
                        'status' => '1',
                        'results' => 'success'
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => ''
            ]);
        }
    }

    /* Get the Procurement details */

    public function processprocurementdetails($id) {
        /* Call the Procurement Item Model */
        $procurementDataRaw = Procurementitem::getItemDetails($id);
        $dataItems = array();

        $totalQuantities = 0;
        $totalItemCost = 0;
        foreach ($procurementDataRaw as $key => $val) {
            $totalQuantities = $totalQuantities + $val->itemQuantity;
            $totalItemCost = $totalItemCost + $val->itemTotalCost;

            $itemPrice = customhelper::getCurrencySymbolFormat($val->itemPrice);
            $itemName = $val->itemName;
            $itemTotalCost = customhelper::getCurrencySymbolFormat($val->itemTotalCost);
            $itemQuantity = $val->itemQuantity;
            $dataItems[$key] = array(
                "itemName" => $itemName,
                "itemTotalCost" => $itemTotalCost,
                "itemQuantity" => $itemQuantity,
                "itemPrice" => $itemPrice
            );
        }

        /* Call the Procurement Model */
        $procurementMainDataRaw = Procurement::getShopForMeDetails($id);
        $discountedPrice = $procurementMainDataRaw->totalProcurementCost + $procurementMainDataRaw->totalProcessingFee + $procurementMainDataRaw->urgentPurchaseCost;
        $data = array(
            "totalProcurementCost" => customhelper::getCurrencySymbolFormat($procurementMainDataRaw->totalProcurementCost),
            "totalProcessingFee" => customhelper::getCurrencySymbolFormat($procurementMainDataRaw->totalProcessingFee),
            "urgentPurchaseCost" => customhelper::getCurrencySymbolFormat($procurementMainDataRaw->urgentPurchaseCost),
            "discountedPrice" => customhelper::getCurrencySymbolFormat($discountedPrice),
            "procurementId" => $procurementMainDataRaw->id,
            "totalQuantities" => $totalQuantities,
            "totalItemCost" => customhelper::getCurrencySymbolFormat($totalItemCost)
        );

        return response()->json([
                    'status' => '1',
                    'results' => 'success',
                    'data' => $data,
                    'dataItem' => $dataItems
        ]);
    }

    /* Get the Procurement shipment details for shop for me */

    public function getprocurementshipmentdetails(Request $request) {
        if (!empty($request->userId)) {
            $page = $request->currentPage;
            $perPage = ($request->perPage != null ? $request->perPage : $this->_perPage);
            $offset = ($page - 1) * $perPage;
            $procurementData = $requestForCostArray = array();
            $totalItems = $totaRequestForCostItems = '';

            if ($request->searchtype == "allshipment") {
                $where = "userId = $request->userId";
                if ($request->orderDate == "FR") {
                    if ($request->has('rangeDate')) {
                        $rangeDate = $request->rangeDate;
                        $str1 = $rangeDate[0];
                        $exStr1 = explode("-", $str1);
                        $exStr1_3 = explode("T", $exStr1[2]);

                        $str2 = $rangeDate[1];
                        $exStr2 = explode("-", $str2);
                        $exStr2_3 = explode("T", $exStr2[2]);

                        $string1 = $exStr1[0] . "-" . $exStr1[1] . "-" . $exStr1_3[0] . " 00:00:00";
                        $string2 = $exStr2[0] . "-" . $exStr2[1] . "-" . $exStr2_3[0] . " 23:59:59";

                        $where .= "  AND orderDate Between '" . $string1 . "' AND '" . $string2 . "'";
                    }
                } else {
                    if ($request->orderDate == "AD") {
                        $where .= "";
                    } if ($request->orderDate == "TM") {
                        $where .= "  AND orderDate Like '%" . date("Y-m") . "%'";
                    } if ($request->orderDate == "TW") {
                        $where .= "  AND orderDate BETWEEN " . $this->getCurrentWeek();
                    } if ($request->orderDate == "TO") {
                        $where .= "  AND orderDate Like '%" . date("Y-m-d") . "%'";
                    }
                }

                if ($request->has('orderId')) {
                    if ($request->orderId != '') {
                        $where .= "  AND id = " . $request->orderId;
                    }
                }

                if ($request->has('status')) {
                    if ($request->status == "S") {
                        $where .= "  AND status = 'submitted'";
                    } if ($request->status == "IR") {
                        $where .= "  AND status = 'itemreceived'";
                    } if ($request->status == "R") {
                        $where .= "  AND status = 'rejected'";
                    } if ($request->status == "C") {
                        $where .= "  AND status = 'completed'";
                    }
                }

                $statusExcept = "draft";
                $totalItems = Procurement::select('*')->where('status', '!=', "'" . $statusExcept . "'")->whereNotIn('status', ["requestforcost","byphone"])->where('deleted', '0')->where('procurementType', 'shopforme')->whereRaw($where)->count();
                $procurementList = Procurement::where('status', '!=', "'" . $statusExcept . "'")->whereNotIn('status', ["requestforcost","byphone"])->where('deleted', '0')->where('procurementType', 'shopforme')->whereRaw($where)->orderby('id', 'desc')->take($perPage)->skip($offset)->get();
                $procurementListArr = $procurementList->toArray();

                if (!empty($procurementListArr)) {
                    foreach ($procurementListArr as $count => $row) {
                        foreach ($row as $key => $value) {
                            if ($key == 'totalShippingCost')
                                $totalShippingCost = $value;
                            elseif ($key == 'totalClearingDuty')
                                $totalClearingDuty = $value;
                            elseif ($key == 'totalProcurementCost')
                                $totalProcurementCost = $value;
                            elseif ($key == 'totalCost')
                                $totalCost = $value;
                            elseif ($key == 'paidCurrencyCode')
                                $currencyCode = $value;
                            elseif ($key == 'exchangeRate')
                                $exchangeRate = $value;
                            else
                                $procurementData[$count][$key] = $value;
                        }
                        $procurementData[$count]['totalProcurementCost'] = !empty($exchangeRate) ? customhelper::getCurrencySymbolFormat($totalProcurementCost * $exchangeRate, $currencyCode) : customhelper::getCurrencySymbolFormat($totalProcurementCost, $currencyCode);
                        $procurementData[$count]['totalCost'] = !empty($exchangeRate) ? customhelper::getCurrencySymbolFormat($totalCost * $exchangeRate, $currencyCode) : customhelper::getCurrencySymbolFormat($totalCost, $currencyCode);
                        $procurementData[$count]['totalShippingCost'] = !empty($exchangeRate) ? customhelper::getCurrencySymbolFormat(($totalClearingDuty + $totalShippingCost) * $exchangeRate, $currencyCode) : customhelper::getCurrencySymbolFormat($totalClearingDuty + $totalShippingCost, $currencyCode);
                    }
                }

                return response()->json([
                            'status' => '1',
                            'results' => 'success',
                            'procurementShipment' => $procurementData,
                            'totalItems' => $totalItems,
                            'itemsPerPage' => $perPage,
                ]);
            }

            if ($request->searchtype == "requestforshipment") {
                $whereR = "userId = $request->userId";
                if ($request->requestforcostDate == "FR") {
                    if ($request->has('rangeDate')) {
                        $rangeDate = $request->rangeDate;
                        $str1 = $rangeDate[0];
                        $exStr1 = explode("-", $str1);
                        $exStr1_3 = explode("T", $exStr1[2]);

                        $str2 = $rangeDate[1];
                        $exStr2 = explode("-", $str2);
                        $exStr2_3 = explode("T", $exStr2[2]);

                        $string1 = $exStr1[0] . "-" . $exStr1[1] . "-" . $exStr1_3[0] . " 00:00:00";
                        $string2 = $exStr2[0] . "-" . $exStr2[1] . "-" . $exStr2_3[0] . " 23:59:59";

                        $whereR .= "  AND orderDate Between '" . $string1 . "' AND '" . $string2 . "'";
                    }
                } else {
                    if ($request->requestforcostDate == "AD") {
                        $whereR .= "";
                    } if ($request->requestforcostDate == "TM") {
                        $whereR .= "  AND orderDate Like '%" . date("Y-m") . "%'";
                    } if ($request->requestforcostDate == "TW") {
                        $whereR .= "  AND orderDate BETWEEN " . $this->getCurrentWeek();
                    } if ($request->requestforcostDate == "TO") {
                        $whereR .= "  AND orderDate Like '%" . date("Y-m-d") . "%'";
                    }
                }

                $totaRequestForCostItems = Procurement::select('*')->whereIn('status', ["requestforcost","byphone"])->where('deleted', '0')->where('procurementType', 'shopforme')->whereRaw($whereR)->count();
                $procurementRequestForCostItemsList = Procurement::whereIn('status', ["requestforcost","byphone"])->where('deleted', '0')->where('procurementType', 'shopforme')->whereRaw($whereR)->orderby('id', 'desc')->take($perPage)->skip($offset)->get()->toArray();



                if (!empty($procurementRequestForCostItemsList)) {
                    foreach ($procurementRequestForCostItemsList as $count => $row) {
                        foreach ($row as $key => $value) {
                            if ($key == 'totalShippingCost')
                                $totalShippingCost = $value;
                            elseif ($key == 'totalClearingDuty')
                                $totalClearingDuty = $value;
                            elseif ($key == 'totalProcurementCost')
                                $totalProcurementCost = $value;
                            elseif ($key == 'totalCost')
                                $totalCost = $value;
                            elseif ($key == 'paidCurrencyCode')
                                $currencyCode = $value;
                            elseif ($key == 'exchangeRate')
                                $exchangeRate = $value;
                            else
                                $requestForCostArray[$count][$key] = $value;
                        }
                        $requestForCostArray[$count]['totalProcurementCost'] = !empty($exchangeRate) ? customhelper::getCurrencySymbolFormat($totalProcurementCost * $exchangeRate, $currencyCode) : customhelper::getCurrencySymbolFormat($totalProcurementCost, $currencyCode);
                        $requestForCostArray[$count]['totalCost'] = !empty($exchangeRate) ? customhelper::getCurrencySymbolFormat($totalCost * $exchangeRate, $currencyCode) : customhelper::getCurrencySymbolFormat($totalCost, $currencyCode);
                        $requestForCostArray[$count]['totalShippingCost'] = !empty($exchangeRate) ? customhelper::getCurrencySymbolFormat(($totalClearingDuty + $totalShippingCost) * $exchangeRate, $currencyCode) : customhelper::getCurrencySymbolFormat($totalClearingDuty + $totalShippingCost, $currencyCode);

                        if ($requestForCostArray[$count]['status'] == 'requestforcost' || $requestForCostArray[$count]['status'] == 'byphone') {
                            $invoiceExsist = \App\Model\Invoice::where('procurementId', $requestForCostArray[$count]['id'])->count();
                            if (!empty($invoiceExsist))
                                $requestForCostArray[$count]['invoiceSent'] = 'Y';
                            else
                                $requestForCostArray[$count]['invoiceSent'] = 'N';
                        } else {
                            $requestForCostArray[$count]['invoiceSent'] = '';
                        }
                    }
                }

                return response()->json([
                            'status' => '1',
                            'results' => 'success',
                            'requestForCostShipment' => $requestForCostArray,
                            'totaRequestForCostItems' => $totaRequestForCostItems,
                            'itemsPerPage' => $perPage,
                ]);
            }
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => ''
            ]);
        }
    }

    /* Get the Procurement shipment details for auto parts */

    public function getprocurementshipmentforautopartsdetails(Request $request) {
        if (!empty($request->userId)) {
            $page = $request->currentPage;
            $perPage = ($request->perPage != null ? $request->perPage : $this->_perPage);
            $offset = ($page - 1) * $perPage;

            $procurementData = $requestForCostArray = array();
            $totalItems = $totaRequestForCostItems = 0;


            if ($request->searchtype == "allshipment") {
                $where = "userId = $request->userId";
                if ($request->orderDate == "FR") {
                    if ($request->has('rangeDate')) {

                        $rangeDate = $request->rangeDate;
                        $firstRangeRaw = strtotime($rangeDate[0] . ' +1 day');
                        $firstRangeRaw0 = date("Y-m-d h:i:s A T", $firstRangeRaw);
                        $firstRange = explode(" ", $firstRangeRaw0);

                        $secondRangeRaw = strtotime($rangeDate[1] . ' +1 day');
                        $secondRangeRaw0 = date("Y-m-d h:i:s A T", $secondRangeRaw);
                        $secondRange = explode(" ", $secondRangeRaw0);

                        $where .= "  AND orderDate Between '" . $firstRange[0] . " 00:00:00" . "' AND '" . $secondRange[0] . " 23:59:59" . "'";
                    }
                } else {
                    if ($request->orderDate == "AD") {
                        $where .= "";
                    } if ($request->orderDate == "TM") {
                        $where .= "  AND orderDate Like '%" . date("Y-m") . "%'";
                    } if ($request->orderDate == "TW") {
                        $where .= "  AND orderDate BETWEEN " . $this->getCurrentWeek();
                    } if ($request->orderDate == "TO") {
                        $where .= "  AND orderDate Like '%" . date("Y-m-d") . "%'";
                    }
                }




                if ($request->has('orderId')) {
                    if ($request->orderId != '') {
                        $where .= "  AND id = " . $request->orderId;
                    }
                }

                if ($request->has('status')) {
                    if ($request->status == "S") {
                        $where .= "  AND status = 'submitted'";
                    } if ($request->status == "IR") {
                        $where .= "  AND status = 'itemreceived'";
                    } if ($request->status == "R") {
                        $where .= "  AND status = 'rejected'";
                    } if ($request->status == "C") {
                        $where .= "  AND status = 'completed'";
                    }
                }

                $statusExcept = "draft";


                $totalItems = Procurement::where('status', '!=', "'" . $statusExcept . "'")->whereNotIn('status', ["requestforcost","byphone"])->where('deleted', '0')->where('procurementType', 'autopart')->whereRaw($where)->count();

                $procurementList = Procurement::where('status', '!=', "'" . $statusExcept . "'")->whereNotIn('status', ["requestforcost","byphone"])->where('deleted', '0')->where('procurementType', 'autopart')->whereRaw($where)->orderby('id', 'desc')->take($perPage)->skip($offset)->get();


                $procurementListArr = $procurementList->toArray();

                $procurementData = array();
                if (!empty($procurementListArr)) {
                    foreach ($procurementListArr as $count => $row) {
                        foreach ($row as $key => $value) {
                            if ($key == 'totalShippingCost')
                                $totalShippingCost = $value;
                            elseif ($key == 'totalClearingDuty')
                                $totalClearingDuty = $value;
                            elseif ($key == 'totalProcurementCost')
                                $totalProcurementCost = $value;
                            elseif ($key == 'totalCost')
                                $totalCost = $value;
                            elseif ($key == 'paidCurrencyCode')
                                $currencyCode = $value;
                            elseif ($key == 'exchangeRate')
                                $exchangeRate = $value;
                            else
                                $procurementData[$count][$key] = $value;
                        }
                        $procurementData[$count]['totalProcurementCost'] = !empty($exchangeRate) ? customhelper::getCurrencySymbolFormat($totalProcurementCost * $exchangeRate, $currencyCode) : customhelper::getCurrencySymbolFormat($totalProcurementCost, $currencyCode);
                        $procurementData[$count]['totalCost'] = !empty($exchangeRate) ? customhelper::getCurrencySymbolFormat($totalCost * $exchangeRate, $currencyCode) : customhelper::getCurrencySymbolFormat($totalCost, $currencyCode);
                        $procurementData[$count]['totalShippingCost'] = !empty($exchangeRate) ? customhelper::getCurrencySymbolFormat(($totalClearingDuty + $totalShippingCost) * $exchangeRate, $currencyCode) : customhelper::getCurrencySymbolFormat($totalClearingDuty + $totalShippingCost, $currencyCode);
                    }
                }

                if ($procurementData) {
                    return response()->json([
                                'status' => '1',
                                'results' => 'success',
                                'procurementShipment' => $procurementData,
                                'totalItems' => $totalItems,
                                'itemsPerPage' => $perPage,
                    ]);
                }
            }

            if ($request->searchtype == "requestforshipment") {
                $whereR = 'userId = ' . $request->userId . ' AND procurementType = "autopart" AND deleted = "0" AND (status= "requestforcost" OR status="byphone")';
                if ($request->requestforcostDate == "FR") {
                    if ($request->has('rangeDate')) {
                        $rangeDate = $request->rangeDate;
                        $str1 = $rangeDate[0];
                        $exStr1 = explode("-", $str1);
                        $exStr1_3 = explode("T", $exStr1[2]);

                        $str2 = $rangeDate[1];
                        $exStr2 = explode("-", $str2);
                        $exStr2_3 = explode("T", $exStr2[2]);

                        $string1 = $exStr1[0] . "-" . $exStr1[1] . "-" . $exStr1_3[0] . " 00:00:00";
                        $string2 = $exStr2[0] . "-" . $exStr2[1] . "-" . $exStr2_3[0] . " 23:59:59";

                        $whereR .= "  AND orderDate Between '" . $string1 . "' AND '" . $string2 . "'";
                    }
                } else {
                    if ($request->requestforcostDate == "AD") {
                        $whereR .= "";
                    } if ($request->requestforcostDate == "TM") {
                        $whereR .= "  AND orderDate Like '%" . date("Y-m") . "%'";
                    } if ($request->requestforcostDate == "TW") {
                        $whereR .= "  AND orderDate BETWEEN " . $this->getCurrentWeek();
                    } if ($request->requestforcostDate == "TO") {
                        $whereR .= "  AND orderDate Like '%" . date("Y-m-d") . "%'";
                    }
                }

                $totaRequestForCostItems = Procurement::select('*')->whereRaw($whereR)->count();


                $procurementRequestForCostItemsList = Procurement::whereRaw($whereR)->orderby('id', 'desc')->take($perPage)->skip($offset)->get()->toArray();

                //print_r($procurementRequestForCostItemsList) ; die;      

                $requestForCostArray = array();

                if (!empty($procurementRequestForCostItemsList)) {
                    foreach ($procurementRequestForCostItemsList as $count => $row) {
                        foreach ($row as $key => $value) {
                            if ($key == 'totalShippingCost')
                                $totalShippingCost = $value;
                            elseif ($key == 'totalClearingDuty')
                                $totalClearingDuty = $value;
                            elseif ($key == 'totalProcurementCost')
                                $totalProcurementCost = $value;
                            elseif ($key == 'totalCost')
                                $totalCost = $value;
                            elseif ($key == 'paidCurrencyCode')
                                $currencyCode = $value;
                            elseif ($key == 'exchangeRate')
                                $exchangeRate = $value;
                            else
                                $requestForCostArray[$count][$key] = $value;
                        }
                        $requestForCostArray[$count]['totalProcurementCost'] = !empty($exchangeRate) ? customhelper::getCurrencySymbolFormat($totalProcurementCost * $exchangeRate, $currencyCode) : customhelper::getCurrencySymbolFormat($totalProcurementCost, $currencyCode);
                        $requestForCostArray[$count]['totalCost'] = !empty($exchangeRate) ? customhelper::getCurrencySymbolFormat($totalCost * $exchangeRate, $currencyCode) : customhelper::getCurrencySymbolFormat($totalCost, $currencyCode);
                        $requestForCostArray[$count]['totalShippingCost'] = !empty($exchangeRate) ? customhelper::getCurrencySymbolFormat(($totalClearingDuty + $totalShippingCost) * $exchangeRate, $currencyCode) : customhelper::getCurrencySymbolFormat($totalClearingDuty + $totalShippingCost, $currencyCode);

                        if ($requestForCostArray[$count]['status'] == 'requestforcost' || $requestForCostArray[$count]['status'] == 'byphone') {
                            $invoiceExsist = \App\Model\Invoice::where('procurementId', $requestForCostArray[$count]['id'])->count();
                            if (!empty($invoiceExsist))
                                $requestForCostArray[$count]['invoiceSent'] = 'Y';
                            else
                                $requestForCostArray[$count]['invoiceSent'] = 'N';
                        } else {
                            $requestForCostArray[$count]['invoiceSent'] = '';
                        }
                    }
                }



                if ($requestForCostArray) {
                    return response()->json([
                                'status' => '1',
                                'results' => 'success',
                                'requestForCostShipment' => $requestForCostArray,
                                'totaRequestForCostItems' => $totaRequestForCostItems,
                                'itemsPerPage' => $perPage,
                    ]);
                }
            }
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => ''
            ]);
        }
    }

    public function updateusercartdata(Request $request) {
        $procurement = array();
        $fromCurrency = $request->fromCurrency;
        $toCurrency = $request->toCurrency;
        $deafultCurrency = customhelper::getCurrencySymbolCode('', true);

        $procurement['defaultCurrencySymbol'] = customhelper::getCurrencySymbolCode();
        $procurement['defaultCurrencyCode'] = $deafultCurrency;
        $procurement['isCurrencyChanged'] = ($request->toCurrency != $deafultCurrency) ? 'Y' : 'N';
        $procurement['currencySymbol'] = customhelper::getCurrencySymbolCode($request->toCurrency);
        $procurement['currencyCode'] = $request->toCurrency;
        $procurement['exchangeRate'] = $exchangeRate = \App\Model\Currency::currencyExchangeRate($deafultCurrency, $request->toCurrency);

        $userCart = $request->usercart;
        if (!empty($userCart['items'])) {
            foreach ($userCart['items'] as $key => $item) {
                /* SET DATA INTO SESSION */
                $procurement['items'][$key]['itemName'] = $item['itemName'];
                $procurement['items'][$key]['websiteUrl'] = $item['websiteUrl'];
                $procurement['items'][$key]['storeId'] = $item['storeId'];
                $procurement['items'][$key]['siteCategoryId'] = $item['siteCategoryId'];
                $procurement['items'][$key]['siteSubCategoryId'] = $item['siteSubCategoryId'];
                $procurement['items'][$key]['siteProductId'] = $item['siteProductId'];
                $procurement['items'][$key]['options'] = (!empty($item['options']) ? $item['options'] : '');
                $procurement['items'][$key]['itemDescription'] = (!empty($item['itemDescription']) ? $item['itemDescription'] : '');
                $procurement['items'][$key]['year'] = (!empty($item['itemYear']) ? $item['itemYear'] : '');
                $procurement['items'][$key]['itemMake'] = (!empty($item['itemMake']) ? $item['itemMake'] : '');
                $procurement['items'][$key]['itemModel'] = (!empty($item['itemModel']) ? $item['itemModel'] : '');
                $procurement['items'][$key]['itemQuantity'] = $item['itemQuantity'];

                $itemPrice = isset($item['defaultItemPrice']) ? $item['defaultItemPrice'] : $item['itemPrice'];
                $itemShippingCost = isset($item['defaultItemShippingCost']) ? $item['defaultItemShippingCost'] : $item['itemShippingCost'];
                $itemTotalCost = isset($item['defaultItemTotalCost']) ? $item['defaultItemTotalCost'] : $item['itemTotalCost'];

                if ($deafultCurrency != $request->toCurrency) {
                    $procurement['items'][$key]['itemPrice'] = \App\Model\Currency::currencyConverter($itemPrice, $exchangeRate);
                    $procurement['items'][$key]['itemShippingCost'] = \App\Model\Currency::currencyConverter($itemShippingCost, $exchangeRate);
                    $procurement['items'][$key]['itemTotalCost'] = \App\Model\Currency::currencyConverter($itemTotalCost, $exchangeRate);
                } else {
                    $procurement['items'][$key]['itemPrice'] = $item['defaultItemPrice'];
                    $procurement['items'][$key]['itemShippingCost'] = $item['defaultItemShippingCost'];
                    $procurement['items'][$key]['itemTotalCost'] = $item['defaultItemTotalCost'];
                }

                $procurement['items'][$key]['defaultItemPrice'] = $itemPrice;
                $procurement['items'][$key]['defaultItemShippingCost'] = $itemShippingCost;
                $procurement['items'][$key]['defaultItemTotalCost'] = $itemTotalCost;
            }
        }

        if (!empty($userCart['data'])) {
            $cartData = $userCart['data'];

            $procurement['data']['warehouseId'] = $cartData['warehouseId'];
            $procurement['data']['urgent'] = $cartData['urgent'];
            $procurement['data']['totalQuantity'] = $cartData['totalQuantity'];
            $procurement['data']['totalWeight'] = $cartData['totalWeight'];
            $procurement['data']['shipmentMethodId'] = !empty($cartData['shipmentMethodId']) ? $cartData['shipmentMethodId'] : '';

            $totalItemCost = isset($cartData['defaultTotalItemCost']) ? $cartData['defaultTotalItemCost'] : $cartData['totalItemCost'];
            $totalProcessingFee = isset($cartData['defaultTotalProcessingFee']) ? $cartData['defaultTotalProcessingFee'] : $cartData['totalProcessingFee'];
            $urgentPurchaseCost = isset($cartData['defaultUrgentPurchaseCost']) ? $cartData['defaultUrgentPurchaseCost'] : $cartData['urgentPurchaseCost'];
            $totalProcurementCost = isset($cartData['defaultTotalProcurementCost']) ? $cartData['defaultTotalProcurementCost'] : $cartData['totalProcurementCost'];
            $totalTax = isset($cartData['defaultTotalTax']) ? $cartData['defaultTotalTax'] : $cartData['totalTax'];
            $totalDiscount = isset($cartData['defaultTotalDiscount']) ? $cartData['defaultTotalDiscount'] : $cartData['totalDiscount'];
            $totalBDiscountCost = isset($cartData['defaultTotalBDiscountCost']) ? $cartData['defaultTotalBDiscountCost'] : $cartData['totalBDiscountCost'];
            $totalCost = isset($cartData['defaultTotalCost']) ? $cartData['defaultTotalCost'] : $cartData['totalCost'];

            if ($deafultCurrency != $request->toCurrency) {
                $procurement['data']['totalItemCost'] = \App\Model\Currency::currencyConverter($totalItemCost, $exchangeRate);
                $procurement['data']['totalProcessingFee'] = \App\Model\Currency::currencyConverter($totalProcessingFee, $exchangeRate);
                $procurement['data']['urgentPurchaseCost'] = \App\Model\Currency::currencyConverter($urgentPurchaseCost, $exchangeRate);
                $procurement['data']['totalProcurementCost'] = \App\Model\Currency::currencyConverter($totalProcurementCost, $exchangeRate);
                $procurement['data']['totalTax'] = \App\Model\Currency::currencyConverter($totalTax, $exchangeRate);
                $procurement['data']['totalDiscount'] = \App\Model\Currency::currencyConverter($totalDiscount, $exchangeRate);
                $procurement['data']['totalBDiscountCost'] = \App\Model\Currency::currencyConverter($totalBDiscountCost, $exchangeRate);
                $procurement['data']['totalCost'] = \App\Model\Currency::currencyConverter($totalCost, $exchangeRate);
            } else {
                $procurement['data']['totalItemCost'] = $cartData['defaultTotalItemCost'];
                $procurement['data']['totalProcessingFee'] = $cartData['defaultTotalProcessingFee'];
                $procurement['data']['urgentPurchaseCost'] = $cartData['defaultUrgentPurchaseCost'];
                $procurement['data']['totalProcurementCost'] = $cartData['defaultTotalProcurementCost'];
                $procurement['data']['totalTax'] = $cartData['defaultTotalTax'];
                $procurement['data']['totalDiscount'] = $cartData['defaultTotalDiscount'];
                $procurement['data']['totalBDiscountCost'] = $cartData['defaultTotalBDiscountCost'];
                $procurement['data']['totalCost'] = $cartData['defaultTotalCost'];
            }

            $procurement['data']['defaultTotalItemCost'] = $totalItemCost;
            $procurement['data']['defaultTotalProcessingFee'] = $totalProcessingFee;
            $procurement['data']['defaultUrgentPurchaseCost'] = $urgentPurchaseCost;
            $procurement['data']['defaultTotalProcurementCost'] = $totalProcurementCost;
            $procurement['data']['defaultTotalTax'] = $totalTax;
            $procurement['data']['defaultTotalDiscount'] = $totalDiscount;
            $procurement['data']['defaultTotalBDiscountCost'] = $totalBDiscountCost;
            $procurement['data']['defaultTotalCost'] = $totalCost;

            if (!empty($cartData['shipmentMethodId'])) {
                $procurement['data']['isDutyCharged'] = $cartData['isDutyCharged'];
                $procurement['data']['shippingMethodName'] = $cartData['shippingMethodName'];
                $procurement['data']['estimateDeliveryDate'] = $cartData['estimateDeliveryDate'];

                $shippingCost = isset($cartData['defaultShippingCost']) ? $cartData['defaultShippingCost'] : $cartData['shippingCost'];
                $totalShippingCost = isset($cartData['defaultTotalShippingCost']) ? $cartData['defaultTotalShippingCost'] : $cartData['totalShippingCost'];
                if (!empty($cartData['totalInsurance'])) {
                    $totalInsurance = isset($cartData['defaultTotalInsurance']) ? $cartData['defaultTotalInsurance'] : $cartData['totalInsurance'];
                } else {
                    $totalInsurance = isset($cartData['defaultTotalInsurance']) ? $cartData['defaultTotalInsurance'] : 0;
                }

                $totalClearingDuty = isset($cartData['defaultClearingDuty']) ? $cartData['defaultClearingDuty'] : $cartData['totalClearingDuty'];

                if ($deafultCurrency != $request->toCurrency) {
                    $procurement['data']['shippingCost'] = \App\Model\Currency::currencyConverter($shippingCost, $exchangeRate);
                    $procurement['data']['totalShippingCost'] = \App\Model\Currency::currencyConverter($totalShippingCost, $exchangeRate);
                    $procurement['data']['totalInsurance'] = \App\Model\Currency::currencyConverter($totalInsurance, $exchangeRate);
                    $procurement['data']['totalClearingDuty'] = \App\Model\Currency::currencyConverter($totalClearingDuty, $exchangeRate);
                } else {
                    $procurement['data']['shippingCost'] = $cartData['defaultShippingCost'];
                    $procurement['data']['totalShippingCost'] = $cartData['defaultTotalShippingCost'];
                    $procurement['data']['totalInsurance'] = $cartData['defaultTotalInsurance'];
                    $procurement['data']['totalClearingDuty'] = $cartData['defaultClearingDuty'];
                }

                $procurement['data']['defaultShippingCost'] = $shippingCost;
                $procurement['data']['defaultTotalShippingCost'] = $totalShippingCost;
                $procurement['data']['defaultTotalInsurance'] = $totalInsurance;
                $procurement['data']['defaultClearingDuty'] = $totalClearingDuty;
            }
        }

        return response()->json([
                    'status' => '1',
                    'results' => 'success',
                    'procurement' => $procurement
        ]);
    }

    /**
     * Method used to get the current week
     * @return string
     */
    public static function getCurrentWeek() {
        $monday = strtotime("last monday");
        $monday = date('w', $monday) == date('w') ? $monday + 7 * 86400 : $monday;

        $sunday = strtotime(date("Y-m-d", $monday) . " +6 days");

        $this_week_sd = date("Y-m-d", $monday);
        $this_week_ed = date("Y-m-d", $sunday);
        return "'" . $this_week_sd . "%' And '" . $this_week_ed . "%'";
    }

    /**
     * Method used to submit order details
     * 
     * @return string
     */
    public function submitorder($userId, Request $request) {

        $paymentStatus = 'unpaid';
        $transactionId = $couponCode = $discountAmount = $discountPoint = $discountType = '';
        $transactionData = $transactionErrorMsg = array();
        $couponData = array();
        $paymentMode = 'offline';
        $paymentErrorMessage = '';

        /* Fetch User Details */
        $userData = User::find($userId);
        $totalInsurance = 0;

        /* Fetch Warehouse Details */
        $warehouseData = \App\Model\Warehouse::find($request->data['warehouseId']);

        $totalItemCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalItemCost'] : $request->data['totalItemCost'];
        $totalProcessingFee = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalProcessingFee'] : $request->data['totalProcessingFee'];
        $urgentPurchaseCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultUrgentPurchaseCost'] : $request->data['urgentPurchaseCost'];
        $totalProcurementCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalProcurementCost'] : $request->data['totalProcurementCost'];
        $totalDiscount = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalDiscount'] : $request->data['totalDiscount'];
        $totalCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalCost'] : $request->data['totalCost'];
        $isInsuranceCharged = isset($request->data['isInsuranceCharged']) ? $request->data['isInsuranceCharged'] : 'N';
        $totalWeight = $request->data['totalWeight'];

        if (isset($request->coupondetails)) {
            $totalBDiscountCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalBDiscountCost'] : $request->data['totalBDiscountCost'];
            $couponCode = $request->coupondetails['couponCode'];
            $discountAmount = $request->coupondetails['discountAmount'];
            $discountPoint = $request->coupondetails['discountPoint'];
            $discountType = $request->coupondetails['discountType'];
            $offerData = \App\Model\Offer::select('id')->where('couponCode', $couponCode)->first();
            $couponId = $offerData['id'];
        }

        $totalTax = 0;
        if (isset($request->data['totalTax'])) {
            $totalTax = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalTax'] : $request->data['totalTax'];
            $totalCost = $totalCost + $totalTax;
        }

        /*  SET DATA FOR OFFLINE GATEWAYS */
        if ($request->paymentMethod['paymentMethodKey'] == 'bank_online_transfer' || $request->paymentMethod['paymentMethodKey'] == 'bank_pay_at_bank' || $request->paymentMethod['paymentMethodKey'] == 'bank_account_pay' || $request->paymentMethod['paymentMethodKey'] == 'check_cash_payment')
            $paymentStatus = 'paid';

        /*  SET DATA FOR WIRE TRANSFER PAYMENT */
        if ($request->paymentMethod['paymentMethodKey'] == 'wire_transfer') {
            $paymentStatus = 'paid';

            $trasactionData = json_encode(array(
                'poNumber' => $request->paymentdetails['poNumber'],
                'companyName' => $request->paymentdetails['companyName'],
                'buyerName' => $request->paymentdetails['buyerName'],
                'position' => $request->paymentdetails['position'],
                    )
            );
        }

        /*  PROCESS EWALLET PAYMENT */
        if ($request->paymentMethod['paymentMethodKey'] == 'ewallet') {
            $eawalletid = (int) $request->paymentdetails['id'];

            $userEwallet = \App\Model\Ewallet::find($eawalletid)->decrement('amount', $totalCost);

            $ewalletTransaction = new \App\Model\Ewallettransaction;
            $ewalletTransaction->userId = $userId;
            $ewalletTransaction->ewalletId = $eawalletid;
            $ewalletTransaction->amount = $totalCost;
            $ewalletTransaction->transactionType = 'debit';
            $ewalletTransaction->transactionOn = Config::get('constants.CURRENTDATE');

            if ($ewalletTransaction->save())
                $paymentStatus = 'paid';
            else
                $paymentStatus = 'failed';

            $paymentMode = 'online';

            $transactionData = json_encode(array(
                'ewalletId' => $request->paymentdetails['ewalletId'],
                    )
            );
        }

        /* PROCESS PAYSTACK DATA */
        if ($request->paymentMethod['paymentMethodKey'] == 'paystack_checkout') {
            if (!empty($request->paystackData['trans']))
                $findRecord = \App\Model\Paystackreferencehistory::whereRaw("referenceNo = $request->paystackCreatedReference and processed='0'")->first();
            else
                $findRecord = \App\Model\Paystackreferencehistory::whereRaw("referenceNo = $request->paystackCreatedReference")->first();
            if (!empty($findRecord)) {
                $paymentTransacionData = \App\Model\Paymenttransaction::where("transactionData", "like", '%"reference":"' . $request->paystackCreatedReference . '"%')->first();
                if (empty($paymentTransacionData) || (!empty($paymentTransacionData) && $paymentTransacionData->status == 'unpaid')) {
                    if (!empty($request->paystackData)) {
                        $checkoutReturn = Paymenttransaction::paystack($request->paystackCreatedReference);
                        if ($checkoutReturn) {
                            $paymentMode = 'online';
                            $paymentStatus = 'paid';
                            if (!empty($request->paystackData['trans']))
                                $trasactionId = $request->paystackData['trans'];
                            else if (!empty($request->paystackData['id']))
                                $transactionId = $request->paystackData['id'];
                            else
                                $transactionId = '123456789';
                            $transactionData = json_encode($request->paystackData);
                        }
                    } else
                        $paymentStatus = 'error';
                }
                else {
                    $paymentStatus = 'pastackProcessed';
                }
            } else {
                $paymentStatus = 'pastackProcessed';
            }

            if ($paymentStatus == 'pastackProcessed') {
                return response()->json([
                            'status' => '1',
                            'results' => 'success',
                ]);
            }
        }

        if ($request->paymentMethod['paymentMethodKey'] == 'payeezy') {
            if (!empty($request->payeezyData)) {
                $checkoutData['amount'] = $request->payeezyData["paidAmount"];
                $checkoutData['method'] = $request->payeezyData["paymentType"];
                $checkoutData['currency_code'] = "USD";
                $checkoutData['type'] = $request->payeezyData["cardType"];
                $checkoutData['cardholder_name'] = $request->payeezyData["cardHolderName"];
                $checkoutData['card_number'] = $request->payeezyData['ccardNumber'];
                $checkoutData['exp_date'] = $request->payeezyData['expiryMonth'] . substr($request->payeezyData['expiryYear'], 2);
                $checkoutData['cvv'] = $request->payeezyData['cvvCode'];
                $checkoutData['userUnit'] = (!empty($userData->unit) ? $userData->unit : "");
                $checkoutReturn = Paymenttransaction::processpayeezy($checkoutData);

                if (is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'success' && !empty($checkoutReturn['transaction_status']) && $checkoutReturn['transaction_status'] == 'approved') {
                    $paymentStatus = 'paid';
                    $paymentMode = 'online';
                    $transactionId = $checkoutReturn['transaction_id'];
                    $transactionData = json_encode($checkoutReturn);
                } else if (is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'failed') {
                    $paymentStatus = 'failed';
                    $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['Error']['messages'][0]['description'];
                } else {
                    $paymentStatus = 'failed';
                    $paymentErrorMessage = 'Payment failed. Something Went Wrong ';
                }
            } else {
                $paymentStatus = 'error';
            }
        }

        /*  PROCESS CREDIT CARD PAYMENT */
        if ($request->paymentMethod['paymentMethodKey'] == 'credit_debit_card') {
            $paymentMethod = \App\Model\Paymentmethod::where('paymentMethodKey', $request->paymentMethod['paymentMethodKey'])->first();
            if ($paymentMethod->paymentGatewayId == 1) {
                $checkoutData = array();
                $checkoutData['cardNumber'] = $request->paymentdetails['cardNumber'];
                $checkoutData['expMonth'] = $request->paymentdetails['expiryMonth'];
                $checkoutData['expYear'] = $request->paymentdetails['expiryYear'];
                $checkoutData['cardCode'] = $request->paymentdetails['cvvCode'];
                $checkoutData['customerFirstName'] = $userData->firstName;
                $checkoutData['customerLastName'] = $userData->lastName;
                $checkoutData['customerAddress'] = $request->personaldetails['billingAddress'];
                $checkoutData['customerCity'] = isset($request->personaldetails['billingCityName']) ? $request->personaldetails['billingCityName'] : "";
                $checkoutData['customerState'] = isset($request->personaldetails['billingStateName']) ? $request->personaldetails['billingStateName'] : "";
                $checkoutData['customerCountry'] = $request->personaldetails['billingCountryName'];
                $checkoutData['customerZip'] = $request->personaldetails['billingZipcode'];
                $checkoutData['amount'] = round($totalCost, 2);
                $checkoutData['defaultCurrency'] = $request->defaultCurrencyCode;

                $checkoutReturn = Paymenttransaction::paypaypalpro($checkoutData);
                if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'successWithWarning')) {
                    $paymentStatus = 'paid';
                    $paymentMode = 'online';
                    $transactionId = $checkoutReturn['TRANSACTIONID'];
                    $transactionData = json_encode($checkoutReturn);
                } else if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Failure')) {
                    $paymentStatus = 'failed';
                    $paymentErrorMessage = $checkoutReturn["L_LONGMESSAGE0"];
                    $transactionErrorMsg = json_encode($checkoutReturn);
                } else if ($checkoutReturn == 'error') {
                    $paymentStatus = 'failed';
                }
            } else {
                $checkoutData['cardNumber'] = $request->paymentdetails['cardNumber'];
                $checkoutData['cardNumber'] = $request->paymentdetails['cardNumber'];
                $checkoutData['expMonth'] = $request->paymentdetails['expiryMonth'];
                $checkoutData['expYear'] = $request->paymentdetails['expiryYear'];
                $checkoutData['cardCode'] = $request->paymentdetails['cvvCode'];
                $checkoutData['customerFirstName'] = $request->personaldetails['billingFirstName'];
                $checkoutData['customerLastName'] = $request->personaldetails['billingLastName'];
                $checkoutData['customerAddress'] = $request->personaldetails['billingAddress'];
                $checkoutData['customerCity'] = isset($request->personaldetails['billingCityName']) ? $request->personaldetails['billingCityName'] : "";
                $checkoutData['customerState'] = isset($request->personaldetails['billingStateName']) ? $request->personaldetails['billingStateName'] : "";
                $checkoutData['customerCountry'] = $request->personaldetails['billingCountryName'];
                $checkoutData['customerZip'] = !empty($request->personaldetails['billingZipcode']) ? $request->personaldetails['billingZipcode'] : "";
                $checkoutData['customerShippingAddress'] = $request->personaldetails['shippingAddress'];
                $checkoutData['customerShippingCity'] = isset($request->personaldetails['shippingCityName']) ? $request->personaldetails['shippingCityName'] : "";
                $checkoutData['customerShippingState'] = isset($request->personaldetails['shippingStateName']) ? $request->personaldetails['shippingStateName'] : "";
                $checkoutData['customerShippingCountry'] = $request->personaldetails['shippingCountryName'];
                $checkoutData['customerShippingZip'] = $request->personaldetails['shippingZipcode'];
                $checkoutData['amount'] = round($totalCost, 2);
                $checkoutData['defaultCurrency'] = $request->defaultCurrencyCode;
                $checkoutData['shippingFirstName'] = $request->personaldetails['shippingFirstName'];
                $checkoutData['shippingLastName'] = $request->personaldetails['shippingFirstName'];

                $checkoutReturn = Paymenttransaction::payauthorizedotnet($checkoutData);

                if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Ok') {
                    $paymentStatus = 'paid';
                    $paymentMode = 'online';
                    $transactionId = $checkoutReturn['transactionResponse']['transId'];
                    $transactionData = json_encode($checkoutReturn['transactionResponse']);
                } else if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Error') {
                    $paymentStatus = 'failed';

                    if (!empty($checkoutReturn['transactionResponse'])) {

                        $transactionErrorMsg = json_encode($checkoutReturn['transactionResponse']);

                        if (isset($checkoutReturn['transactionResponse']['errors']['error'][0]))
                            $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['transactionResponse']['errors']['error'][0]['errorText'];
                        else
                            $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['transactionResponse']['errors']['error']['errorText'];
                    }else {
                        if (!empty($checkoutReturn['messages']['message'])) {
                            $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['messages']['message']['text'];
                        }
                    }
                }
            }
        }

        if ($request->paymentMethod['paymentMethodKey'] == 'paypalstandard') {
            if (isset($request->paypalData)) {
                $paymentStatus = 'paid';
                $paymentMode = 'online';
                $transactionId = $request->paypalData['orderID'];
                $transactionData = json_encode($request->paypalData);
            } else {
                $paymentStatus = 'error';
            }
        }


        if (isset($request->id) && !empty($request->id)) {
            $isProcurementExist = true;

            $procurement = Procurement::find($request->id);
            $procurement->status = 'submitted';
            $procurement->procurementLocked = 'Y';
            $procurement->paymentMethodId = $request->paymentMethod['paymentMethodId'];
            $procurement->paymentStatus = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
            $procurement->paymentReceivedOn = Config::get('constants.CURRENTDATE');
            $procurement->isCurrencyChanged = $request->isCurrencyChanged;
            $procurement->defaultCurrencyCode = $request->defaultCurrencyCode;
            $procurement->paidCurrencyCode = ($request->isCurrencyChanged == 'Y') ? $request->currencyCode : $request->defaultCurrencyCode;
            $procurement->exchangeRate = ($request->isCurrencyChanged == 'Y') ? $request->exchangeRate : 1;
            $procurement->orderDate = Config::get('constants.CURRENTDATE');

            if (!empty($request->data['shipmentMethodId'])) {
                $totalShippingCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalShippingCost'] : $request->data['totalShippingCost'];
                $totalClearingDuty = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalClearingDuty'] : $request->data['totalClearingDuty'];
                $shippingCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultShippingCost'] : $request->data['shippingCost'];


                $procurement->shippingMethod = 'Y';
                $procurement->shippingMethodId = $request->data['shipmentMethodId'];
                $procurement->totalShippingCost = $shippingCost;
                $procurement->totalClearingDuty = $totalClearingDuty;
                $procurement->isDutyCharged = $request->data['isDutyCharged'];

                if ($isInsuranceCharged == 'Y') {
                    $totalInsurance = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalInsurance'] : $request->data['totalInsurance'];
                    $procurement->totalInsurance = $totalInsurance;
                }
            }

            $procurement->save();

            $procurementId = $request->id;
        } else {
            $isProcurementExist = false;

            /* Insert Data into Main Procurement Table */
            $procurement = new Procurement;
            $procurement->userId = $userId;
            $procurement->warehouseId = $request->data['warehouseId'];
            $procurement->fromCountry = $warehouseData->countryId;
            $procurement->fromState = $warehouseData->stateId;
            $procurement->fromCity = $warehouseData->cityId;
            $procurement->fromAddress = $warehouseData->address;
            $procurement->fromZipCode = $warehouseData->zipcode;
            //$procurement->fromPhone = $request->personaldetails['contactNumber'];
            if (isset($request->personaldetails['phcode']) && !empty($request->personaldetails['phcode'])) {
                $procurement->fromPhone = $request->personaldetails['phcode'] . $request->personaldetails['contactNumber'];
            } else {
                $procurement->fromPhone = $request->personaldetails['contactNumber'];
            }
            $procurement->fromName = $request->personaldetails['userTitle'] . " " . $request->personaldetails['firstName'] . " " . $request->personaldetails['lastName'];
            $procurement->fromEmail = $request->personaldetails['userEmail'];
            $procurement->toCountry = $request->personaldetails['shippingCountryId'];
            $procurement->toState = $request->personaldetails['shippingStateId'];
            $procurement->toCity = $request->personaldetails['shippingCityId'];
            $procurement->toAddress = $request->personaldetails['shippingAddress'];
            if (!empty($request->personaldetails['shippingZipcode']))
                $procurement->toZipCode = $request->personaldetails['shippingZipcode'];
            if (isset($request->personaldetails['shippingIsdcode']) && !empty($request->personaldetails['shippingIsdcode'])) {
                $procurement->toPhone = $request->personaldetails['shippingIsdcode'] . $request->personaldetails['shippingPhone'];
            } else {
                $procurement->toPhone = $request->personaldetails['shippingPhone'];
            }
            // $procurement->toPhone = $request->personaldetails['shippingPhone'];
            $procurement->toName = $request->personaldetails['shippingFirstName'] . " " . $request->personaldetails['shippingLastName'];
            $procurement->toEmail = $request->personaldetails['shippingEmail'];
            $procurement->procurementType = 'shopforme';
            $procurement->urgent = $request->data['urgent'];
            $procurement->totalItemCost = $totalItemCost;
            $procurement->totalProcessingFee = $totalProcessingFee;
            $procurement->urgentPurchaseCost = $urgentPurchaseCost;
            $procurement->totalProcurementCost = $totalProcurementCost;
            $procurement->status = 'submitted';
            $procurement->procurementLocked = 'Y';
            $procurement->totalTax = $totalTax;
            $procurement->totalCost = $totalCost;
            $procurement->totalWeight = $totalWeight;
            $procurement->paymentMethodId = $request->paymentMethod['paymentMethodId'];
            $procurement->paymentStatus = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
            $procurement->paymentReceivedOn = Config::get('constants.CURRENTDATE');
            $procurement->isCurrencyChanged = $request->isCurrencyChanged;
            $procurement->defaultCurrencyCode = $request->defaultCurrencyCode;
            $procurement->paidCurrencyCode = ($request->isCurrencyChanged == 'Y') ? $request->currencyCode : $request->defaultCurrencyCode;
            $procurement->exchangeRate = ($request->isCurrencyChanged == 'Y') ? $request->exchangeRate : 1;
            $procurement->createdBy = $userId;
            $procurement->createdByType = 'user';
            $procurement->createdOn = Config::get('constants.CURRENTDATE');
            $procurement->orderDate = Config::get('constants.CURRENTDATE');

            if (!empty($couponCode)) {
                if ($discountType == 'amount')
                    $procurement->totalDiscount = $totalDiscount;
                $procurement->couponcodeApplied = $couponCode;
            }

            if (!empty($request->data['shipmentMethodId'])) {
                $totalShippingCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalShippingCost'] : $request->data['totalShippingCost'];
                $totalClearingDuty = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultClearingDuty'] : $request->data['totalClearingDuty'];
                $shippingCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultShippingCost'] : $request->data['shippingCost'];


                $procurement->shippingMethod = 'Y';
                $procurement->shippingMethodId = $request->data['shipmentMethodId'];
                $procurement->totalShippingCost = $shippingCost;
                $procurement->totalClearingDuty = $totalClearingDuty;
                $procurement->isDutyCharged = $request->data['isDutyCharged'];

                if ($isInsuranceCharged == 'Y') {
                    $totalInsurance = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalInsurance'] : $request->data['totalInsurance'];
                    $procurement->totalInsurance = $totalInsurance;
                }
            }

            $procurement->save();
            $procurementId = $procurement->id;
        }

        if (!empty($procurementId)) {
            if (empty($isProcurementExist)) {
                /* Insert Data into Main Procurement Table */
                if (!empty($request->items)) {
                    $totalQuantity = 0;
                    $packageDetails = array();
                    foreach ($request->items as $key => $item) {
                        $totalQuantity += $item['itemQuantity'];

                        /* INSERT DATA INTO PROCUREMENT ITEM TABLE */
                        $pocurementitem = new Procurementitem;
                        $pocurementitem->procurementId = $procurementId;
                        $pocurementitem->itemName = $item['itemName'];
                        $pocurementitem->websiteUrl = $item['websiteUrl'];
                        $pocurementitem->storeId = !empty($item['storeId']) ? $item['storeId'] : "0";
                        $pocurementitem->siteCategoryId = !empty($item['siteCategoryId']) ? $item['siteCategoryId'] : "";
                        $pocurementitem->siteSubCategoryId = !empty($item['siteSubCategoryId']) ? $item['siteSubCategoryId'] : "";
                        $pocurementitem->siteProductId = !empty($item['siteProductId']) ? $item['siteProductId'] : "";
                        $pocurementitem->options = !empty($item['options']) ? $item['options'] : "";
                        $pocurementitem->itemPrice = ($request->isCurrencyChanged == 'Y') ? $item['defaultItemPrice'] : $item['itemPrice'];
                        $pocurementitem->itemQuantity = $item['itemQuantity'];
                        $pocurementitem->itemShippingCost = ($request->isCurrencyChanged == 'Y') ? $item['defaultItemShippingCost'] : $item['itemShippingCost'];
                        $pocurementitem->itemTotalCost = ($request->isCurrencyChanged == 'Y') ? $item['defaultItemTotalCost'] : $item['itemTotalCost'];
                        $pocurementitem->itemImage = isset($item['itemImage']) ? $item['itemImage'] : '';
                        $pocurementitem->save();

                        $procurementItemId = $pocurementitem->id;

                        /* INSERT DATA INTO STATUS LOG TABLE */
                        $procurementitemstatus = new Procurementitemstatus;
                        $procurementitemstatus->procurementId = $procurementId;
                        $procurementitemstatus->procurementItemId = $procurementItemId;
                        $procurementitemstatus->oldStatus = '';
                        $procurementitemstatus->status = 'submitted';
                        $procurementitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
                        $procurementitemstatus->save();

                        /* SET DATA FOR INVOICE PARTICULARS */
                        $packageDetails[$key] = array(
                            'id' => $procurementItemId,
                            'itemName' => $item['itemName'],
                            'websiteUrl' => $item['websiteUrl'],
                            'storeId' => !empty($item['storeId']) ? $item['storeId'] : "0",
                            'siteCategoryId' => !empty($item['siteCategoryId']) ? $item['siteCategoryId'] : "",
                            'siteSubCategoryId' => !empty($item['siteSubCategoryId']) ? $item['siteSubCategoryId'] : "",
                            'siteProductId' => !empty($item['siteProductId']) ? $item['siteProductId'] : "",
                            'options' => !empty($item['options']) ? $item['options'] : "",
                            'itemPrice' => ($request->isCurrencyChanged == 'Y') ? $item['defaultItemPrice'] : $item['itemPrice'],
                            'itemQuantity' => $item['itemQuantity'],
                            'itemShippingCost' => ($request->isCurrencyChanged == 'Y') ? $item['defaultItemShippingCost'] : $item['itemShippingCost'],
                            'itemTotalCost' => ($request->isCurrencyChanged == 'Y') ? $item['defaultItemTotalCost'] : $item['itemTotalCost'],
                        );
                    }
                }

                /* DELETE USER CART ITEMS */
                $userCart = Usercart::where('userId', $userId)->where('type', 'shopforme')->delete();
            } else {
                if (!empty($request->items)) {
                    $totalQuantity = 0;

                    foreach ($request->items as $key => $item) {
                        $totalQuantity += $item['itemQuantity'];

                        /* INSERT DATA INTO STATUS LOG TABLE */
                        $procurementitemstatus = new Procurementitemstatus;
                        $procurementitemstatus->procurementId = $procurementId;
                        $procurementitemstatus->procurementItemId = $item['id'];
                        $procurementitemstatus->oldStatus = '';
                        $procurementitemstatus->status = 'submitted';
                        $procurementitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
                        $procurementitemstatus->save();


                        /* SET DATA FOR INVOICE PARTICULARS */
                        $packageDetails[$key] = array(
                            'id' => $item['id'],
                            'itemName' => $item['itemName'],
                            'websiteUrl' => $item['websiteUrl'],
                            'storeId' => !empty($item['storeId']) ? $item['storeId'] : "0",
                            'siteCategoryId' => !empty($item['siteCategoryId']) ? $item['siteCategoryId'] : "",
                            'siteSubCategoryId' => !empty($item['siteSubCategoryId']) ? $item['siteSubCategoryId'] : "",
                            'siteProductId' => !empty($item['siteProductId']) ? $item['siteProductId'] : "",
                            'options' => !empty($item['options']) ? $item['options'] : "",
                            'itemPrice' => ($request->isCurrencyChanged == 'Y') ? $item['defaultItemPrice'] : $item['itemPrice'],
                            'itemQuantity' => $item['itemQuantity'],
                            'itemShippingCost' => ($request->isCurrencyChanged == 'Y') ? $item['defaultItemShippingCost'] : $item['itemShippingCost'],
                            'itemTotalCost' => ($request->isCurrencyChanged == 'Y') ? $item['defaultItemTotalCost'] : $item['itemTotalCost'],
                        );
                    }
                }
            }

            /*  COUPON DATA */
            if (isset($request->coupondetails) && !empty($couponCode)) {
                /*  INSERT DATA INTO COUPON TABLE */
                $data = array(
                    'couponId' => $couponId,
                    'code' => $couponCode,
                    'userId' => $userId,
                    'totalAmount' => $totalBDiscountCost,
                    'discountAmount' => $totalDiscount,
                    'discountPoint' => $discountPoint,
                );
                $couponLog = \App\Model\Couponlog::insertLog($data);

                /*  UPDATE DATA IF COUPON POINTS */
                if ($discountType == 'points') {
                    $fundpoint = \App\Model\Fundpoint::where('userId', $userId)->increment('point', $discountPoint);
                    $fundpointTransaction = new \App\Model\Fundpointtransaction;
                    $fundpointTransaction->userId = $userId;
                    $fundpointTransaction->point = $discountPoint;
                    $fundpointTransaction->date = Config::get('constants.CURRENTDATE');
                    $fundpointTransaction->save();
                }
            }

            /*  UPDATE RFERRAL POINTS */
            $param = array();
            $param['userId'] = $userId;
            $paidAmount = $totalProcessingFee;
            if (!empty($request->data['shipmentMethodId'])) {
                $paidAmount += $totalShippingCost;
            }
            $param['paidAmount'] = round(($paidAmount), 2);
            \App\Model\Fundpoint::calculateReferralBonus($param);

            if (empty($request->data['shipmentMethodId'])) {
                /* UPDATE ADDRESS BOOK DEFAULT BILLING */
                $billingAddress = array(
                    'firstName' => $request->personaldetails['billingFirstName'],
                    'lastName' => $request->personaldetails['billingLastName'],
                    'email' => $request->personaldetails['billingEmail'],
                    'address' => $request->personaldetails['billingAddress'],
                    'alternateAddress' => !empty($request->personaldetails['billingAlternateAddress']) ? $request->personaldetails['billingAlternateAddress'] : "",
                    'cityId' => $request->personaldetails['billingCityId'],
                    'stateId' => $request->personaldetails['billingStateId'],
                    'countryId' => $request->personaldetails['billingCountryId'],
                    'zipcode' => !empty($request->personaldetails['billingZipcode']) ? $request->personaldetails['billingZipcode'] : "",
                    'phone' => !empty($request->personaldetails['billingPhone']) ? $request->personaldetails['billingPhone'] : "",
                    'isdCode' => isset($request->personaldetails['billingIsdcode']) ? $request->personaldetails['billingIsdcode'] : '',
                );
                \App\Model\Addressbook::where('userId', $userId)
                        ->where('isDefaultBilling', '1')
                        ->update($billingAddress);

                /* UPDATE ADDRESS BOOK TABLE */
                if (empty($request->personaldetails['locationId'])) {
                    $shippingAddress = array(
                        'firstName' => $request->personaldetails['shippingFirstName'],
                        'lastName' => $request->personaldetails['shippingLastName'],
                        'email' => $request->personaldetails['shippingEmail'],
                        'address' => $request->personaldetails['shippingAddress'],
                        'alternateAddress' => !empty($request->personaldetails['shippingAlternateAddress']) ? $request->personaldetails['shippingAlternateAddress'] : "",
                        'cityId' => $request->personaldetails['shippingCityId'],
                        'stateId' => $request->personaldetails['shippingStateId'],
                        'countryId' => $request->personaldetails['shippingCountryId'],
                        'zipcode' => !empty($request->personaldetails['shippingZipcode']) ? $request->personaldetails['shippingZipcode'] : "",
                        'phone' => !empty($request->personaldetails['shippingPhone']) ? $request->personaldetails['shippingPhone'] : "",
                        'isdCode' => isset($request->personaldetails['shippingIsdcode']) ? $request->personaldetails['shippingIsdcode'] : '',
                    );
                    \App\Model\Addressbook::where('userId', $userId)
                            ->where('isDefaultShipping', '1')
                            ->update($shippingAddress);
                }
            }
            
            if(isset($request->tempDataId) && !empty($request->tempDataId)) {
                
                $tempCartData = \App\Model\Temporarycartdata::find($request->tempDataId);
                $tempCartData->isDataSaved = 'Y';
                $tempCartData->dataSavedId = $procurementId;
                $tempCartData->save();
            }
        }
        if ($paymentStatus == 'paid') {

            /*  PREPARE DATA FOR INVOICE PARTICULARS */
            $invoiceData = array(
                'shipment' => array(
                    'urgent' => $request->data['urgent'],
                    'totalItemCost' => $totalItemCost,
                    'totalProcessingFee' => $totalProcessingFee,
                    'urgentPurchaseCost' => $urgentPurchaseCost,
                    'totalProcurementCost' => $totalProcurementCost,
                    'totalTax' => $totalTax,
                    'isInsuranceCharged' => $isInsuranceCharged,
                    'totalInsurance' => $isInsuranceCharged == 'Y' ? $totalInsurance : '',
                    'totalCost' => $totalCost,
                    'totalWeight' => $request->data['totalWeight'],
                    'totalQuantity' => $totalQuantity,
                ),
                'warehouse' => array(
                    'fromAddress' => $warehouseData->address,
                    'fromZipCode' => $warehouseData->zipcode,
                    'fromCountry' => $warehouseData->countryId,
                    'fromState' => $warehouseData->stateId,
                    'fromCity' => $warehouseData->cityId,
                ),
                'shippingaddress' => array(
                    'toCountry' => $request->personaldetails['shippingCountryId'],
                    'toState' => $request->personaldetails['shippingStateId'],
                    'toCity' => $request->personaldetails['shippingCityId'],
                    'toAddress' => $request->personaldetails['shippingAddress'],
                    'toAlternateAddress' => '',
                    'toZipCode' => '',
                    'toName' => $request->personaldetails['shippingFirstName'] . " " . $request->personaldetails['shippingLastName'],
                    'toEmail' => $request->personaldetails['shippingEmail'],
                    'toPhone' => (isset($request->personaldetails['shippingIsdcode']) ? ($request->personaldetails['shippingIsdcode'] . $request->personaldetails['shippingPhone']) : $request->personaldetails['shippingPhone']),
                ),
                'packages' => $packageDetails,
                'payment' => array(
                    'paymentMethodId' => $request->paymentMethod['paymentMethodId'],
                    'paymentMethodName' => $request->paymentMethod['paymentMethodName'],
                ),
            );

            if (!empty($request->personaldetails['shippingAlternateAddress'])) {
                $invoiceData['shippingaddress']['toAlternateAddress'] = $request->personaldetails['shippingAlternateAddress'];
            }
            if (!empty($request->personaldetails['shippingZipcode'])) {
                $invoiceData['shippingaddress']['toZipCode'] = $request->personaldetails['shippingZipcode'];
            }

            if (isset($request->coupondetails) && !empty($couponCode)) {
                $invoiceData['shipment']['couponCode'] = $couponCode;
                $invoiceData['shipment']['discountAmount'] = $discountAmount;
                $invoiceData['shipment']['discountPoint'] = $discountPoint;
                $invoiceData['shipment']['discountType'] = $discountType;
            }

            if ($request->paymentMethod['paymentMethodKey'] == 'wire_transfer') {
                $invoiceData['payment']['poNumber'] = $request->paymentdetails['poNumber'];
                $invoiceData['payment']['companyName'] = $request->paymentdetails['companyName'];
                $invoiceData['payment']['buyerName'] = $request->paymentdetails['buyerName'];
                $invoiceData['payment']['position'] = $request->paymentdetails['position'];
            }
            if ($request->paymentMethod['paymentMethodKey'] == 'ewallet') {
                $invoiceData['payment']['ewalletId'] = $request->paymentdetails['ewalletId'];
            }

            if (!empty($request->data['shipmentMethodId'])) {
                $invoiceData['shippingcharges']['shippingMethod'] = 'Y';
                $invoiceData['shippingcharges']['shippingid'] = $request->data['shipmentMethodId'];
                $invoiceData['shippingcharges']['shipping'] = $request->data['shippingMethodName'];
                $invoiceData['shippingcharges']['estimateDeliveryDate'] = $request->data['estimateDeliveryDate'];
                $invoiceData['shippingcharges']['isDutyCharged'] = $request->data['isDutyCharged'];
                $invoiceData['shippingcharges']['shippingCost'] = $shippingCost;
                $invoiceData['shippingcharges']['totalClearingDuty'] = $totalClearingDuty;
                $invoiceData['shippingcharges']['totalShippingCost'] = $totalShippingCost;
            }

            /*  INSERT DATA INTO INVOICE TABLE */
            $invoiceUniqueId = 'REC' . $userData->unit . '-' . $procurementId . '-' . date('Ymd');
            $invoice = new \App\Model\Invoice;
            $invoice->invoiceUniqueId = $invoiceUniqueId;
            $invoice->procurementId = $procurementId;
            $invoice->invoiceType = 'receipt';
            $invoice->type = 'shopforme';
            $invoice->userUnit = $userData->unit;
            $invoice->userFullName = $request->personaldetails['userTitle'] . " " . $request->personaldetails['firstName'] . " " . $request->personaldetails['lastName'];
            $invoice->userEmail = $request->personaldetails['userEmail'];
            $invoice->userContactNumber = $request->personaldetails['contactNumber'];
            $invoice->billingName = $request->personaldetails['billingFirstName'] . ' ' . $request->personaldetails['billingLastName'];
            $invoice->billingEmail = $request->personaldetails['billingEmail'];
            $invoice->billingAddress = $request->personaldetails['billingAddress'];
            if (!empty($request->personaldetails['billingAlternateAddress']))
                $invoice->billingAlternateAddress = $request->personaldetails['billingAlternateAddress'];
            $invoice->billingCity = $request->personaldetails['billingCityId'];
            $invoice->billingState = $request->personaldetails['billingStateId'];
            $invoice->billingCountry = $request->personaldetails['billingCountryId'];
            if (!empty($request->personaldetails['billingZipcode']))
                $invoice->billingZipcode = $request->personaldetails['billingZipcode'];
            $invoice->billingPhone = (isset($request->personaldetails['billingIsdcode']) ? $request->personaldetails['billingIsdcode'] . $request->personaldetails['billingPhone'] : $request->personaldetails['billingPhone']);
            $invoice->totalBillingAmount = $totalCost;
            $invoice->paymentMethodId = $request->paymentMethod['paymentMethodId'];
            $invoice->paymentStatus = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
            ;
            $invoice->invoiceParticulars = json_encode($invoiceData);
            $invoice->createdOn = Config::get('constants.CURRENTDATE');
            $invoice->save();
            $invoiceId = $invoice->id;

            /*  INSERT DATA INTO PAYMENT TABLE */
            $paymentTransaction = new \App\Model\Paymenttransaction;
            $paymentTransaction->userId = $userId;
            $paymentTransaction->paymentMethodId = $request->paymentMethod['paymentMethodId'];
            $paymentTransaction->paidFor = 'shopforme';
            $paymentTransaction->paidForId = $procurementId;
            $paymentTransaction->invoiceId = $invoice->id;
            $paymentTransaction->amountPaid = $totalCost;
            $paymentTransaction->status = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
            if (!empty($transactionData))
                $paymentTransaction->transactionData = $transactionData;
            if (!empty($transactionId))
                $paymentTransaction->transactionId = $transactionId;
            $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
            $paymentTransaction->save();

            /*  EMAIL INVOICE */
            if (!empty($invoiceId)) {
                $data['invoice'] = \App\Model\Invoice::find($invoiceId);
                $fileName = "Invoice_" . $invoiceUniqueId . ".pdf";
                PDF::loadView('Administrator.procurement.invoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                $to = $request->personaldetails['userEmail'];
                $content = "<p>Dear " . $request->personaldetails['firstName'] . " " . $request->personaldetails['lastName'] . ",</p>";
                $content .= "<p>Attached is your invoice for Shop for Me #" . $procurementId . "<br>Thank you for choosing Shoptomydoor. We appreciate your business.</p>";
                Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($invoiceUniqueId, $to, $fileName) {
                    $message->from('contact@shoptomydoor.com', 'Shoptomydoor');
                    $message->subject("$invoiceUniqueId - Shop for Me Invoice");
                    $message->to($to);
                    $message->attach(public_path('exports/invoice/' . $fileName));
                });
            }
        } else if ($paymentStatus == 'failed') {
            /*  INSERT DATA INTO PAYMENT TABLE */
            $paymentTransaction = new \App\Model\Paymenttransaction;
            $paymentTransaction->userId = $userId;
            $paymentTransaction->paymentMethodId = $request->paymentMethod['paymentMethodId'];
            $paymentTransaction->paidFor = 'shopforme';
            $paymentTransaction->amountPaid = $totalCost;
            $paymentTransaction->status = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
            if (!empty($transactionErrorMsg))
                $paymentTransaction->errorMsg = $transactionErrorMsg;
            $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
            $paymentTransaction->save();
            /*  INSERT DATA INTO PAYMENT TABLE */
        }

        if ($paymentStatus == 'paid' & !empty($procurementId)) {
            return response()->json([
                        'status' => '1',
                        'results' => 'success',
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => 'payment_failed',
                        'msg' => !empty($paymentErrorMessage) ? $paymentErrorMessage.'. Please contact site admin.' : 'There seems to be some issue with payment. Please contact site admin.'
            ]);
        }
    }

    /**
     * Method used to validate coupon code
     * @param request $request
     * @return string
     */
    public static function validatecouponcode(Request $request) {

        /* INITALIZE THE VARIABLE AS FALSE */
        $match = false;

        /* GET THE COUPON CODE */
        $code = $request->coupon;

        /* FETCH USER DETAILS */
        $userId = $request->userId;
        $userDetails = \App\Model\User::where("id", $userId)->first();

        /* VALIDATE THE COUPON CODE */
        $currDate = date('m/d/Y');

        /* GET THE LOG */
        $couponLog = \App\Model\Couponlog::where('userId', $userId)->where('code', $code)->count();
        if ($couponLog == 1) {
            return response()->json([
                        'status' => '0',
                        'results' => 'invalid',
                        'message' => 'Coupon already used'
            ]);
        }

        /* GET THE OFFER ID */
        $offerIdQuery = \App\Model\Offer::where('status', '1')->where('deleted', '0')->where('couponCode', $code)->get();

        if (count($offerIdQuery) == 0) {
            return response()->json([
                        'status' => '0',
                        'results' => 'invalid',
                        'message' => 'Invalid coupon'
            ]);
        }

        $offerCondQuery = \App\Model\Offercondition::where('offerId', $offerIdQuery[0]->id)->whereIn('keyword', array('new_customer_reg_from_app', 'user_subscription'))->count();


        if ($offerCondQuery == 0) {
            /* QUERY FOR NOT REGISTERED BY APP */
            $where = "'$currDate' between offerRangeDateFrom and offerRangeDateTo";
            $ifCodeExist = \App\Model\Offer::where('status', '1')->where('deleted', '0')->where('couponCode', $code)->whereRaw($where)->get();
        } else {
            /* QUERY FOR REGISTERED BY APP */
            $ifCodeExist = \App\Model\Offer::where('status', '1')->where('deleted', '0')->where('couponCode', $code)->get();
        }


        if ($ifCodeExist->isEmpty()) {
            return response()->json([
                        'status' => '0',
                        'results' => 'invalid',
                        'message' => 'Invalid coupon'
            ]);
        } else {
            /* COUPON CODE IS VALID, DO SEARCH TO CHECK CONDITIONS */
            $offerId = $ifCodeExist[0]->id;

            /* IF REGISTERED BY APP */
            if ($offerCondQuery > 0) {
                if ($ifCodeExist[0]->keyword == 'new_customer_reg_from_app') {
                    $createdOn = explode(" ", $userDetails->createdOn);
                    $date1 = date_create($createdOn[0]);
                    $date2 = date_create(date('Y-m-d'));
                    $diff = date_diff($date1, $date2);
                    $exactDay = $diff->format("%d");

                    $getOffer = \App\Model\Offer::where('id', $offerIdQuery[0]->id)->first();
                    if ($exactDay > $getOffer->offerRangeDateFrom) {
                        return response()->json([
                                    'status' => '0',
                                    'results' => 'invalid',
                                    'message' => 'Invalid coupon'
                        ]);
                        exit;
                    } else {
                        //Nothing
                    }
                }
            }

            /* GET THE CONDITIONS */
            $offerConditions = \App\Model\Offercondition::where('offerId', $offerId)->orderby('id')->get();


            /* GET THE CUSTOMER ADDRESS DETAILS */
            $userAddress = \App\Model\Addressbook::where("userId", $userId)->where('deleted', '0')->get();

            /* GET THE CART ITEMS ARRAY */

            foreach ($offerConditions as $keyO => $valO) {

                /* CONDITION 3 */
                if ($valO->keyword == "discount_on_total_amount_of_shipping") {

                    $offerConditionByKeyword = \App\Model\Offercondition::where('offerId', $offerId)
                            ->where('keyword', 'discount_on_total_amount_of_shipping')
                            ->get();
                    $OfferCond = $offerConditionByKeyword[0]->jsonValues;
                    $decodedValues = json_decode($OfferCond);

                    /* GET THE SHIPPING AMOUNT */
                    $shippingAmount = $totalWeight = $request->usercart['data']['totalShippingCost'];

                    if ($shippingAmount == null) {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    }


                    if ($shippingAmount >= $decodedValues->shipping_cost->from && $shippingAmount <= $decodedValues->shipping_cost->to) {
                        /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                        $match = true;
                    } else {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    }
                }

                /* CONDITION 4 */
                if ($valO->keyword == "customer_has_not_purchased_any_goods_for_a_certain_timeframe") {

                    $offerConditionByKeyword = \App\Model\Offercondition::where('offerId', $offerId)
                            ->where('keyword', 'customer_has_not_purchased_any_goods_for_a_certain_timeframe')
                            ->get();
                    $OfferCond = $offerConditionByKeyword[0]->jsonValues;
                    $decodedValues = json_decode($OfferCond);

                    /* GET THE DATES FROM JSON DECODE */
                    $from = date_format(date_create($decodedValues->user_not_purchased->from), "Y-m-d");
                    $to = date_format(date_create($decodedValues->user_not_purchased->to), "Y-m-d");

                    /* GET THE LAST DATE OF THE PURCHASE */
                    $lastOrder = \App\Model\Order::where('userId', $userId)->where('status', '5')->orderby('id', 'desc')->first();
                    if (!empty($lastOrder)) {
                        $notPurchasedFrom = data('Y-m-d', strtotime($lastOrder->createdDate));

                        /* CALCULATE PART */
                        if (( $notPurchasedFrom >= $from ) && ( $notPurchasedFrom <= $to )) {
                            /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                            $match = false;
                            break;
                        } else {
                            /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                            $match = true;
                        }
                    }
                }

                /* CONDITION 5 */
                if ($valO->keyword == "customer_purchased_an_item_from_particular_store") {

                    /* GET THE STORE IDS FROM CART */
                    foreach ($request->usercart['items'] as $keycart => $valCart) {
                        $storeId[] = "'[" . '"' . $valCart['storeId'] . '"' . "]'";
                    }

                    $query = \App\Model\Offercondition::where('offerId', $offerId)->where('keyword', 'customer_purchased_an_item_from_particular_store');

                    $query->where(function($q) use ($storeId) {
                        foreach ($storeId as $keyQ => $condition) {
                            if ($keyQ == 0) {
                                $q->whereRaw('json_contains(jsonValues, ' . $condition . ', \'$.store_id\')');
                            } else {
                                $q->orWhereRaw('json_contains(jsonValues, ' . $condition . ', \'$.store_id\')');
                            }
                        }
                    });

                    if (count($query->get()) > 0) {
                        /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                        $match = true;
                    } else {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    }
                }


                /* CONDITION 6 */
                if ($valO->keyword == "discount_on_total_weight_of_shipping") {

                    /* MATCH THE WEIGHT WHICH MAY FALL UNDER THE RANGE */
                    $totalWeight = $request->usercart['data']['totalWeight'];

                    $offerConditionByKeyword = \App\Model\Offercondition::where('offerId', $offerId)
                            ->where('keyword', 'discount_on_total_weight_of_shipping')
                            ->get();
                    $OfferCond = $offerConditionByKeyword[0]->jsonValues;
                    $decodedValues = json_decode($OfferCond);


                    if ($totalWeight >= $decodedValues->shipping_weight->from && $totalWeight <= $decodedValues->shipping_weight->to) {
                        /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                        $match = true;
                    } else {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    }
                }

                /* CONDITION 7 */
                if ($valO->keyword == "discount_on_total_weight_of_shipping_for_any_specific_customer") {
                    $createdOn = date('Y-m-d', strtotime($userDetails->createdOn));

                    $date1 = date_create($createdOn[0]);
                    $date2 = date_create(date('Y-m-d'));
                    $diff = date_diff($date1, $date2);

                    /* GET THE ACTUAL MONTH */
                    $exactMonth = $diff->format("%m");

                    /* GET THE MONTH DEFINED IN THE CONDITION */
                    $offerConditionByKeyword = \App\Model\Offercondition::where('offerId', $offerId)
                            ->where('keyword', 'discount_on_total_weight_of_shipping_for_any_specific_customer')
                            ->get();
                    $monthOffer = $offerConditionByKeyword[0]->jsonValues;
                    $decodedValues = json_decode($monthOffer);
                    $duration = $decodedValues->shipping_weight_for_specific_user->duration;

                    if ($exactMonth >= $duration) {
                        /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                        $match = true;

                        /* NOW MATCH THE WEIGHT WHICH MAY FALL UNDER THE RANGE */
                        $totalWeight = $request->usercart['data']['totalWeight'];
                        if ($totalWeight >= $decodedValues->shipping_weight_for_specific_user->from && $totalWeight <= $decodedValues->shipping_weight_for_specific_user->to) {
                            /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                            $match = true;
                        } else {
                            /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                            $match = false;
                            break;
                        }
                    } else {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    }
                }

                /* CONDITION 8 */
                if ($valO->keyword == "customer_is_using_a_particular_shipping_method") {

                    /* GET THE SHIPPING METHOD ID */
                    $id = $request->usercart['data']['shipmentMethodId'];

                    /* IF NO METHOD FOUND, EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                    if ($id == null) {
                        $match = false;
                        break;
                    } else {
                        $shippingMethidId = '["' . $id . '"]';
                    }

                    $query = \App\Model\Offercondition::where('offerId', $offerId)->where('keyword', 'customer_is_using_a_particular_shipping_method')
                            ->whereRaw('json_contains(jsonValues, ' . "'" . $shippingMethidId . "'" . ', \'$.shipping_method\')');

                    if ($query->count() == 0) {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    } else {
                        /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                        $match = true;
                    }
                }

                /* CONDITION 9 */
                if ($valO->keyword == "new_customer_reg_from_app") {
                    if ($userDetails->registeredBy == 'app')
                        $appReg = '["Y"]';
                    else
                        $appReg = '["N"]';

                    $query = \App\Model\Offercondition::where('offerId', $offerId)->where('keyword', 'new_customer_reg_from_app')
                            ->whereRaw('json_contains(jsonValues, ' . "'" . $appReg . "'" . ', \'$.app_user_registration\')');
                    if ($query->count() == 0) {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    } else {
                        /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                        $match = true;
                    }
                }

                /* CONDITION 9 */
                if ($valO->keyword == "user_subscription") {
                    if ($userDetails->isSubscribed == 'Y')
                        $appReg = '["Y"]';
                    else
                        $appReg = '["N"]';

                    $query = \App\Model\Offercondition::where('offerId', $offerId)->where('keyword', 'user_subscription')
                            ->whereRaw('json_contains(jsonValues, ' . "'" . $appReg . "'" . ', \'$.user_subscription\')');
                    if ($query->count() == 0) {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    } else {
                        /* INITIALIZE THE VARIABLE AS TRUE WHIC LEAD TO THE NEXT CONDITION */
                        $match = true;
                    }
                }

                /* CONDITION 11 */
                if ($valO->keyword == "customer_get_discounts_and_special_offers_on_certain_product_or_categories") {
                    foreach ($request->usercart['items'] as $keycart => $valCart) {
                        if ($valCart['siteProductId'] != null) {
                            $pId[] = array('{"values":["' . $valCart['siteProductId'] . '"]}');
                        }
                        if ($valCart['siteCategoryId'] != null) {
                            $pId[] = array('{"values":"' . $valCart['siteCategoryId'] . '"}');
                        }
                    }

                    /* IF ARRAY IS BLANK THEN CONDITION IS NOT SATISFIED */
                    if (count($pId) == 0) {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    }

                    $query = \App\Model\Offercondition::where('offerId', $offerId)->where('keyword', 'customer_get_discounts_and_special_offers_on_certain_product_or_categories');
                    $query->where(function($q) use ($pId) {
                        foreach ($pId as $keyQ => $condition) {
                            if ($keyQ == 0) {
                                $q->whereRaw('json_contains(jsonValues, ' . "'" . $condition[0] . "'" . ')');
                            } else {
                                $q->orWhereRaw('json_contains(jsonValues, ' . "'" . $condition[0] . "'" . ')');
                            }
                        }
                    });

                    if (count($query->get()) > 0) {
                        $match = true;
                    } else {
                        /* EXIT FROM LOOP WHICH LEAD TO DISATISFY THE CONDITION */
                        $match = false;
                        break;
                    }
                }

                /* CONDITION 2 */
                if ($valO->keyword == "customer_of_a_specific_shipping_location") {

                    /* GET LOCATION DETAILS */
                    if (isset($request->usercart['personaldetails'])) {
                        $countryId = $request->usercart['personaldetails']['shippingCountryId'];
                        $stateId = $request->usercart['personaldetails']['shippingStateId'];
                        $cityId = $request->usercart['personaldetails']['shippingCityId'];
                    } else {
                        $countryId = $request->usercart['personalDetails']['shippingCountry'];
                        $stateId = $request->usercart['personalDetails']['shippingState'];
                        $cityId = $request->usercart['personalDetails']['shippingCity'];
                    }


                    if ($stateId == null) {
                        $stringState = "N/A";
                    } else {
                        $stringState = $stateId;
                    }

                    if ($cityId == null) {
                        $stringCity = "N/A";
                    } else {
                        $stringCity = $cityId;
                    }

                    $makeArray = '"countrySelected":"' . $countryId . '","stateSelected":"' . $stringState . '","citySelected":"' . $stringCity . '"';
                    $makeArray2 = '"countrySelected":"' . $countryId . '","stateSelected":"' . $stringState . '","citySelected":"N/A"';
                    $makeArray3 = '"countrySelected":"' . $countryId . '","stateSelected":"N/A","citySelected":"N/A"';

                    #DB::enableQueryLog(); 


                    $query = $condition2 = \App\Model\Offercondition::where('offerId', $offerId)->where('keyword', 'customer_of_a_specific_shipping_location');

                    $query->where(function($q) use ($makeArray, $makeArray2, $makeArray3) {

                        $q->whereRaw('json_contains(jsonValues, \'{' . $makeArray . '}\')');
                        $q->orWhereRaw('json_contains(jsonValues, \'{' . $makeArray2 . '}\')');
                        $q->orWhereRaw('json_contains(jsonValues, \'{' . $makeArray3 . '}\')');
                    });
                    //->get(); 
                    #dd(DB::getQueryLog());

                    if (count($query->get()) > 0) {
                        $match = true;
                    } else {
                        $match = false;
                        break;
                    }
                }
            }

            if ($match == false) {
                return response()->json([
                            'status' => '0',
                            'results' => 'invalid',
                            'message' => 'Invalid coupon'
                ]);
            } else {

                /* CALCULATE LOGIC */
                $bonus = unserialize($offerIdQuery[0]->bonusValuesSerialized);
                $countBonusArray = count($bonus);

                /* GET THE TOTAL SHIPPING DISCOUNT FROM LOCAL STORAGE */

                if ($request->usercart['isCurrencyChanged'] == 'Y'){
                    $totalShippingCost = $request->usercart['data']["defaultTotalShippingCost"];
                }
                else{
                    $totalShippingCost = empty($request->usercart['data']["totalShippingCost"]) ? 0 : $request->usercart['data']["totalShippingCost"];
                }


                /* GET THE AMOUNT */
                if ($countBonusArray == 4) {
                    $AmountOrPoint = "Amount";
                    $pointToBeDiscounted = 0.00;
                    $type = $bonus["type"];
                    if ($type == 'Absolute') {
                        if ($bonus["discount"] > $totalShippingCost) {
                            $amountToBeDiscounted = $totalShippingCost;
                        } else {
                            $amountToBeDiscounted = $bonus["discount"];
                        }
                    } else {
                        $discountNotExceed = $bonus["discountNotExceed"];
                        $getAmount = (($totalShippingCost * $bonus["discount"]) / 100);
                        if ($getAmount > $discountNotExceed) {
                            $amountToBeDiscounted = $discountNotExceed;
                        } else {
                            $amountToBeDiscounted = number_format($getAmount, 2);
                        }
                    }
                } else {
                    /* GET THE POINTS */
                    $AmountOrPoint = "Point";
                    $amountToBeDiscounted = 0.00;
                    if (isset($bonus['bonusPoint']) && $bonus['bonusPoint'] == 1) {
                        $pointToBeDiscounted = round($bonus["fixedAmount"]);
                    } else {
                        /* HANDLE DIVISION BY ZERO */
                        if (isset($bonus['bonusPer']) && $bonus["bonusPer"] > 0) {
                            $getPer = ($totalShippingCost / $bonus["bonusPer"]);
                            $pointToBeDiscounted = round($getPer * $bonus["amountPer"]);
                        } else {
                            $pointToBeDiscounted = 0;
                        }
                    }
                }

                return response()->json([
                            'status' => '0',
                            'results' => 'valid',
                            'message' => 'Coupon applied',
                            'amount_or_point' => $AmountOrPoint,
                            'amount_to_be_discounted' => $amountToBeDiscounted,
                            'point_to_be_discounted' => $pointToBeDiscounted
                ]);
            }
        }
    }

    /* Method to Submit oreder for Auto Parts

     */

    public function placeorder($userId, Request $request) {
        //print_r($request->all()); die;     

        $paymentStatus = 'unpaid';
        $transactionId = $couponCode = $discountAmount = $discountPoint = $discountType = '';
        $transactionData = array();
        $couponData = array();
        $paymentMode = 'offline';
        $paymentErrorMessage = '';

        /* Fetch User Details */
        $userData = \App\Model\User::find($userId);
        $totalInsurance = 0;

        /* Fetch Warehouse Details */
        $warehouseData = \App\Model\Warehouse::find($request->data['warehouseId']);

        $totalClearingDuty = 0;

        $totalItemCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalItemCost'] : $request->data['totalItemCost'];
        $totalProcessingFee = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalProcessingFee'] : $request->data['totalProcessingFee'];
        $urgentPurchaseCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultUrgentPurchaseCost'] : $request->data['urgentPurchaseCost'];
        $totalProcurementCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalProcurementCost'] : $request->data['totalProcurementCost'];
        $totalCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalCost'] : $request->data['totalCost'];
        $isInsuranceCharged = isset($request->data['isInsuranceCharged']) ? $request->data['isInsuranceCharged'] : 'N';

        $totalWeight = $request->data['totalWeight'];

        if (isset($request->coupondetails)) {
            if (isset($request->data['totalBDiscountCost'])) {
                $totalBDiscountCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalBDiscountCost'] : $request->data['totalBDiscountCost'];
            } else {
                $totalBDiscountCost = 0;
            }

            $couponCode = $request->coupondetails['couponCode'];
            $discountAmount = $request->coupondetails['discountAmount'];
            $discountPoint = $request->coupondetails['discountPoint'];
            $discountType = $request->coupondetails['discountType'];
            $offerData = \App\Model\Offer::select('id')->where('couponCode', $couponCode)->first();
            $couponId = $offerData['id'];
        }

        $totalTax = 0;
        if (isset($request->data['totalTax'])) {
            $totalTax = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalTax'] : $request->data['totalTax'];
            $totalCost = $totalCost + $totalTax;
        }

        /*  SET DATA FOR OFFLINE GATEWAYS */
        if ($request->paymentMethod['paymentMethodKey'] == 'bank_online_transfer' || $request->paymentMethod['paymentMethodKey'] == 'bank_pay_at_bank' || $request->paymentMethod['paymentMethodKey'] == 'bank_account_pay' || $request->paymentMethod['paymentMethodKey'] == 'check_cash_payment')
            $paymentStatus = 'paid';

        /*  SET DATA FOR WIRE TRANSFER PAYMENT */
        if ($request->paymentMethod['paymentMethodKey'] == 'wire_transfer') {
            $trasactionData = json_encode(array(
                'poNumber' => $request->paymentdetails['poNumber'],
                'companyName' => $request->paymentdetails['companyName'],
                'buyerName' => $request->paymentdetails['buyerName'],
                'position' => $request->paymentdetails['position'],
                    )
            );
        }

        /*  PROCESS EWALLET PAYMENT */
        if ($request->paymentMethod['paymentMethodKey'] == 'ewallet') {
            $eawalletid = (int) $request->paymentdetails['id'];

            $userEwallet = \App\Model\Ewallet::find($eawalletid)->decrement('amount', $totalCost);

            $ewalletTransaction = new \App\Model\Ewallettransaction;
            $ewalletTransaction->userId = $userId;
            $ewalletTransaction->ewalletId = $eawalletid;
            $ewalletTransaction->amount = $totalCost;
            $ewalletTransaction->transactionType = 'debit';
            $ewalletTransaction->transactionOn = Config::get('constants.CURRENTDATE');
            if ($ewalletTransaction->save())
                $paymentStatus = 'paid';
            else
                $paymentStatus = 'failed';

            $paymentMode = 'online';

            $transactionData = json_encode(array(
                'ewalletId' => $request->paymentdetails['ewalletId'],
                    )
            );
        }

        /*  PROCESS CREDIT CARD PAYMENT */
        if ($request->paymentMethod['paymentMethodKey'] == 'credit_debit_card') {
            $paymentMethod = \App\Model\Paymentmethod::where('paymentMethodKey', $request->paymentMethod['paymentMethodKey'])->first();
            if ($paymentMethod->paymentGatewayId == 1) {
                $checkoutData = array();
                $checkoutData['cardNumber'] = $request->paymentDetails['cardNumber'];
                $checkoutData['expMonth'] = $request->paymentDetails['expiryMonth'];
                $checkoutData['expYear'] = $request->paymentDetails['expiryYear'];
                $checkoutData['cardCode'] = $request->paymentDetails['cvvCode'];
                $checkoutData['customerFirstName'] = $userData->firstName;
                $checkoutData['customerLastName'] = $userData->lastName;
                $checkoutData['customerAddress'] = $request->personalDetails['billingAddress'];
                $checkoutData['customerCity'] = isset($request->personalDetails['billingCityName']) ? $request->personalDetails['billingCityName'] : "";
                $checkoutData['customerState'] = isset($request->personalDetails['billingStateName']) ? $request->personalDetails['billingStateName'] : "";
                $checkoutData['customerCountry'] = $request->personalDetails['billingCountryName'];
                $checkoutData['customerZip'] = isset($request->personalDetails['billingZipcode']) ? $request->personalDetails['billingZipcode'] : '';
                $checkoutData['amount'] = round($totalCost, 2);
                $checkoutData['defaultCurrency'] = $request->defaultCurrencyCode;

                $checkoutReturn = Paymenttransaction::paypaypalpro($checkoutData);
                if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'successWithWarning')) {
                    $paymentStatus = 'paid';
                    $paymentMode = 'online';
                    $transactionId = $checkoutReturn['TRANSACTIONID'];
                    $transactionData = json_encode($checkoutReturn);
                } else if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Failure')) {
                    $paymentStatus = 'failed';
                    $paymentErrorMessage = $checkoutReturn["L_LONGMESSAGE0"];
                    $transactionErrorMsg = json_encode($checkoutReturn);
                } else if ($checkoutReturn == 'error') {
                    $paymentStatus = 'failed';
                }
            } else {
                $checkoutData['cardNumber'] = $request->paymentdetails['cardNumber'];
                $checkoutData['cardNumber'] = $request->paymentdetails['cardNumber'];
                $checkoutData['expMonth'] = $request->paymentdetails['expiryMonth'];
                $checkoutData['expYear'] = $request->paymentdetails['expiryYear'];
                $checkoutData['cardCode'] = $request->paymentdetails['cvvCode'];
                $checkoutData['customerFirstName'] = $request->personalDetails['billingFirstName'];
                $checkoutData['customerLastName'] = $request->personalDetails['billingLastName'];
                $checkoutData['customerAddress'] = $request->personalDetails['billingAddress'];
                $checkoutData['customerCity'] = isset($request->personalDetails['billingCity']) ? $request->personalDetails['billingCity'] : "";
                $checkoutData['customerState'] = isset($request->personalDetails['billingState']) ? $request->personalDetails['billingState'] : "";
                $checkoutData['customerCountry'] = $request->personalDetails['billingCountry'];
                $checkoutData['customerZip'] = !empty($request->personaldetails['billingZipcode']) ? $request->personaldetails['billingZipcode'] : "";
                $checkoutData['customerShippingAddress'] = $request->personalDetails['shippingAddress'];
                $checkoutData['customerShippingCity'] = isset($request->personalDetails['shippingCity']) ? $request->personalDetails['shippingCity'] : "";
                $checkoutData['customerShippingState'] = isset($request->personalDetails['shippingState']) ? $request->personalDetails['shippingState'] : "";
                $checkoutData['customerShippingCountry'] = $request->personalDetails['shippingCountry'];
                $checkoutData['customerShippingZip'] = isset($request->personalDetails['shippingZipcode']) ? $request->personalDetails['shippingZipcode'] : '';
                $checkoutData['amount'] = round($totalCost, 2);
                $checkoutData['defaultCurrency'] = $request->defaultCurrencyCode;
                $checkoutData['shippingFirstName'] = $request->personalDetails['shippingFirstName'];
                $checkoutData['shippingLastName'] = $request->personalDetails['shippingFirstName'];

                $checkoutReturn = Paymenttransaction::payauthorizedotnet($checkoutData);

                if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Ok') {
                    $paymentStatus = 'paid';
                    $paymentMode = 'online';
                    $transactionId = $checkoutReturn['transactionResponse']['transId'];
                    $transactionData = json_encode($checkoutReturn['transactionResponse']);
                } else if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Error') {
                    $paymentStatus = 'failed';
                    if (!empty($checkoutReturn['transactionResponse'])) {

                        $transactionErrorMsg = json_encode($checkoutReturn['transactionResponse']);

                        if (isset($checkoutReturn['transactionResponse']['errors']['error'][0]))
                            $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['transactionResponse']['errors']['error'][0]['errorText'];
                        else
                            $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['transactionResponse']['errors']['error']['errorText'];
                    }else {
                        if (!empty($checkoutReturn['messages']['message'])) {
                            $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['messages']['message']['text'];
                        }
                    }
                }
            }
        }

        /* PROCESS PAYSTACK DATA */
        if ($request->paymentMethod['paymentMethodKey'] == 'paystack_checkout') {
            if (!empty($request->paystackData['trans']))
                $findRecord = \App\Model\Paystackreferencehistory::whereRaw("referenceNo = $request->paystackCreatedReference and processed='0'")->first();
            else
                $findRecord = \App\Model\Paystackreferencehistory::whereRaw("referenceNo = $request->paystackCreatedReference")->first();
            if (!empty($findRecord)) {
                $paymentTransacionData = \App\Model\Paymenttransaction::where("transactionData", "like", '%"reference":"' . $request->paystackCreatedReference . '"%')->first();
                if (empty($paymentTransacionData) || (!empty($paymentTransacionData) && $paymentTransacionData->status == 'unpaid')) {
                    \App\Model\Paystackreferencehistory::where("id", $findRecord->id)->update(["processed" => "1"]);
                    if (!empty($request->paystackData)) {
                        $checkoutReturn = Paymenttransaction::paystack($request->paystackCreatedReference);
                        if ($checkoutReturn) {
                            $paymentMode = 'online';
                            $paymentStatus = 'paid';
                            if (!empty($request->paystackData['trans']))
                                $trasactionId = $request->paystackData['trans'];
                            else if (!empty($request->paystackData['id']))
                                $transactionId = $request->paystackData['id'];
                            else
                                $transactionId = '123456789';
                            $transactionData = json_encode($request->paystackData);
                        } else {
                            $paymentStatus = 'error';
                        }
                    }
                } else {
                    $paymentStatus = 'pastackProcessed';
                }
            } else {
                $paymentStatus = 'pastackProcessed';
            }
            
            if ($paymentStatus == 'pastackProcessed') {
                return response()->json([
                            'status' => '1',
                            'results' => 'success',
                ]);
            }
            
        }

        if ($request->paymentMethod['paymentMethodKey'] == 'paypalstandard') {
            if (isset($request->paypalData)) {
                $paymentStatus = 'paid';
                $paymentMode = 'online';
                $transactionId = $request->paypalData['orderID'];
                $transactionData = json_encode($request->paypalData);
            } else {
                $paymentStatus = 'error';
            }
        }

        if ($request->paymentMethod['paymentMethodKey'] == 'payeezy') {
            if (!empty($request->payeezyData)) {
                $checkoutData['amount'] = $request->payeezyData["paidAmount"];
                $checkoutData['method'] = $request->payeezyData["paymentType"];
                $checkoutData['currency_code'] = "USD";
                $checkoutData['type'] = $request->payeezyData["cardType"];
                $checkoutData['cardholder_name'] = $request->payeezyData["cardHolderName"];
                $checkoutData['card_number'] = $request->payeezyData['ccardNumber'];
                $checkoutData['exp_date'] = $request->payeezyData['expiryMonth'] . substr($request->payeezyData['expiryYear'], 2);
                $checkoutData['cvv'] = $request->payeezyData['cvvCode'];
                $checkoutData['userUnit'] = (!empty($userData->unit) ? $userData->unit : "");
                $checkoutReturn = Paymenttransaction::processpayeezy($checkoutData);

                if (is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'success' && !empty($checkoutReturn['transaction_status']) && $checkoutReturn['transaction_status'] == 'approved') {
                    $paymentStatus = 'paid';
                    $paymentMode = 'online';
                    $transactionId = $checkoutReturn['transaction_id'];
                    $transactionData = json_encode($checkoutReturn);
                } else if (is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'failed') {
                    $paymentStatus = 'failed';
                    $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['Error']['messages'][0]['description'];
                } else {
                    $paymentStatus = 'failed';
                    $paymentErrorMessage = 'Payment failed. Something Went Wrong ';
                }
            } else {
                $paymentStatus = 'error';
            }
        }


        /* Save Auto Parts records to Main Procurement table */

        //DB::beginTransaction();
        
            if (isset($request->id) && !empty($request->id)) {
                $isProcurementExist = true;

                $procurement = Procurement::find($request->id);
                $procurement->status = 'submitted';
                $procurement->procurementLocked = 'Y';
                $procurement->paymentMethodId = $request->paymentMethod['paymentMethodId'];
                $procurement->isCurrencyChanged = $request->isCurrencyChanged;
                $procurement->defaultCurrencyCode = $request->defaultCurrencyCode;
                $procurement->paidCurrencyCode = ($request->isCurrencyChanged == 'Y') ? $request->currencyCode : $request->defaultCurrencyCode;
                $procurement->exchangeRate = ($request->isCurrencyChanged == 'Y') ? $request->exchangeRate : 1;
                $procurement->orderDate = Config::get('constants.CURRENTDATE');

                if (!empty($request->data['shipmentMethodId'])) {
                    $totalShippingCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalShippingCost'] : $request->data['totalShippingCost'];
                    $totalClearingDuty = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalClearingDuty'] : $request->data['totalClearingDuty'];
                    $shippingCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultShippingCost'] : $request->data['shippingCost'];


                    $procurement->shippingMethod = 'Y';
                    $procurement->shippingMethodId = $request->data['shipmentMethodId'];
                    $procurement->totalShippingCost = $shippingCost;
                    $procurement->totalClearingDuty = $totalClearingDuty;
                    $procurement->isDutyCharged = $request->data['isDutyCharged'];

                    if ($request->data['isInsuranceCharged'] == 'Y') {
                        $totalInsurance = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalInsurance'] : $request->data['totalInsurance'];
                        $procurement->totalInsurance = $totalInsurance;
                    }
                }

                $procurement->save();

                $procurementId = $request->id;
            } else {
                $isProcurementExist = false;

                $procurement = new Procurement;
                $procurement->userId = $userId;
                $procurement->warehouseId = $request->data['warehouseId'];
                $procurement->fromCountry = $warehouseData->countryId;
                $procurement->fromState = $warehouseData->stateId;
                $procurement->fromCity = $warehouseData->cityId;
                $procurement->fromAddress = $warehouseData->address;
                $procurement->fromZipCode = $warehouseData->zipcode;
                if (isset($request->personalDetails['phcode']) && !empty($request->personalDetails['phcode'])) {
                    $procurement->fromPhone = $request->personalDetails['phcode'] . $request->personalDetails['phone'];
                } else {
                    $procurement->fromPhone = $request->personalDetails['phone'];
                }
                $procurement->fromZipCode = $warehouseData->zipcode;
                $procurement->fromName = $request->personalDetails['firstName'] . " " . $request->personalDetails['lastName'];
                $procurement->fromEmail = $request->personalDetails['userEmail'];
                $procurement->toCountry = $request->personalDetails['shippingCountry'];
                $procurement->toState = $request->personalDetails['shippingState'];
                $procurement->toCity = $request->personalDetails['shippingCity'];
                $procurement->toAddress = $request->personalDetails['shippingAddress'];
                if (!empty($request->personalDetails['shippingZipCode']))
                    $procurement->toZipCode = $request->personalDetails['shippingZipCode'];

                if (isset($request->personalDetails['shippingIsdcode']) && !empty($request->personalDetails['shippingIsdcode'])) {
                    $procurement->toPhone = $request->personalDetails['shippingIsdcode'] . $request->personalDetails['shippingContact'];
                } else {
                    $procurement->toPhone = $request->personalDetails['shippingContact'];
                }
                //$procurement->toPhone = $request->personalDetails['shippingContact'];
                $procurement->toName = $request->personalDetails['shippingFirstName'] . " " . $request->personalDetails['shippingLastName'];
                $procurement->toEmail = $request->personalDetails['shippingEmail'];
                $procurement->procurementType = 'autopart';
                $procurement->urgent = $request->data['urgent'];
                $procurement->totalItemCost = $totalItemCost;
                $procurement->totalProcessingFee = $totalProcessingFee;
                $procurement->urgentPurchaseCost = $urgentPurchaseCost;
                $procurement->totalProcurementCost = $totalProcurementCost;
                $procurement->status = 'submitted';
                $procurement->procurementLocked = 'Y';
                $procurement->totalCost = $totalCost;
                $procurement->totalWeight = $totalWeight;
                $procurement->paymentMethodId = $request->paymentMethod['paymentMethodId'];
                $procurement->paymentStatus = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
                $procurement->paymentReceivedOn = Config::get('constants.CURRENTDATE');
                $procurement->paymentMethodId = $request->paymentMethod['paymentMethodId'];
                $procurement->isCurrencyChanged = (empty($request->isCurrencyChanged) ? 'N' : $request->isCurrencyChanged);
                $procurement->defaultCurrencyCode = $request->defaultCurrencyCode;
                $procurement->paidCurrencyCode = $request->currencyCode;
                $procurement->exchangeRate = ($request->isCurrencyChanged == 'Y') ? $request->exchangeRate : 1;
                $procurement->createdBy = $userId;
                $procurement->createdByType = 'user';
                $procurement->createdOn = Config::get('constants.CURRENTDATE');
                $procurement->orderDate = Config::get('constants.CURRENTDATE');

                if (!empty($couponCode)) {
                    if ($discountType == 'amount')
                        $procurement->totalDiscount = $totalDiscount;
                    $procurement->couponcodeApplied = $couponCode;
                }

                if (!empty($request->data['shipmentMethodId'])) {
                    $totalShippingCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalShippingCost'] : $request->data['totalShippingCost'];
                    $totalClearingDuty = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultClearingDuty'] : $request->data['totalClearingDuty'];
                    $shippingCost = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultShippingCost'] : $request->data['shippingCost'];

                    $procurement->shippingMethod = 'Y';
                    $procurement->shippingMethodId = $request->data['shipmentMethodId'];
                    $procurement->totalShippingCost = $totalShippingCost;
                    $procurement->totalClearingDuty = $totalClearingDuty;
                    $procurement->isDutyCharged = $request->data['isDutyCharged'];

                    if (isset($request->data['isInsuranceCharged']) && $request->data['isInsuranceCharged'] == 'Y') {
                        $totalInsurance = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultTotalInsurance'] : $request->data['totalInsurance'];
                        $procurement->totalInsurance = $totalInsurance;
                    }
                }

                $procurement->save();
                $procurementId = $procurement->id;
            }

            /* Save Auto Parts records to Main Procurement table */


            if (!empty($procurementId)) {
                if (empty($isProcurementExist)) {
                    if (!empty($request->items)) {
                        $totalQuantity = 0;
                        $packageDetails = array();
                        foreach ($request->items as $key => $item) {
                            if (!empty($item['siteProductId'])) {
                                $siteProduct = \App\Model\Siteproduct::find($item['siteProductId']);
                                $totalWeight += $siteProduct->weight;
                            }

                            $totalQuantity += $item['itemQuantity'];

                            /* INSERT DATA INTO PROCUREMENT ITEM TABLE */
                            $pocurementitem = new Procurementitem;
                            $pocurementitem->procurementId = $procurementId;
                            $pocurementitem->itemName = $item['itemName'];
                            $pocurementitem->websiteUrl = $item['websiteUrl'];
                            $pocurementitem->storeId = (!empty($item['storeId']) ? $item['storeId'] : "0");
                            $pocurementitem->siteCategoryId = (!empty($item['siteCategoryId']) ? $item['siteCategoryId'] : "0");
                            $pocurementitem->siteSubCategoryId = (!empty($item['siteSubCategoryId']) ? $item['siteSubCategoryId'] : "0");
                            $pocurementitem->siteProductId = (!empty($item['siteProductId']) ? $item['siteProductId'] : "0");
                            $pocurementitem->itemDescription = $item['itemDescription'];
                            $pocurementitem->year = $item['year'];
                            $pocurementitem->itemMake = $item['itemMake'];
                            $pocurementitem->itemModel = $item['itemModel'];
                            $pocurementitem->itemImage = (empty($item['itemImage']) ? "" : $item['itemImage']);
                            $pocurementitem->itemPrice = ($request->isCurrencyChanged == 'Y') ? $item['defaultItemPrice'] : $item['itemPrice'];
                            $pocurementitem->itemQuantity = $item['itemQuantity'];
                            $pocurementitem->itemShippingCost = ($request->isCurrencyChanged == 'Y') ? $item['defaultItemShippingCost'] : $item['itemShippingCost'];
                            $pocurementitem->itemTotalCost = ($request->isCurrencyChanged == 'Y') ? $item['defaultItemTotalCost'] : $item['itemTotalCost'];
                            $pocurementitem->save();

                            $procurementItemId = $pocurementitem->id;

                            /* INSERT DATA INTO STATUS LOG TABLE */
                            $procurementitemstatus = new Procurementitemstatus;
                            $procurementitemstatus->procurementId = $procurementId;
                            $procurementitemstatus->procurementItemId = $procurementItemId;
                            $procurementitemstatus->oldStatus = '';
                            $procurementitemstatus->status = 'submitted';
                            $procurementitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
                            $procurementitemstatus->save();

                            /* SET DATA FOR INVOICE PARTICULARS */
                            $packageDetails[$key] = array(
                                'procurementId' => $procurementId,
                                'itemName' => $item['itemName'],
                                'websiteUrl' => $item['websiteUrl'],
                                'storeId' => (!empty($item['storeId']) ? $item['storeId'] : "0"),
                                'siteCategoryId' => (!empty($item['siteCategoryId']) ? $item['siteCategoryId'] : "0"),
                                'siteSubCategoryId' => (!empty($item['siteSubCategoryId']) ? $item['siteSubCategoryId'] : "0"),
                                'siteProductId' => (!empty($item['siteProductId']) ? $item['siteProductId'] : "0"),
                                'itemDescription' => $item['itemDescription'],
                                'year' => $item['year'],
                                'itemMake' => $item['itemMake'],
                                'itemModel' => $item['itemModel'],
                                'itemImage' => (empty($item['itemImage']) ? "" : $item['itemImage']),
                                'itemPrice' => ($request->isCurrencyChanged == 'Y') ? $item['defaultItemPrice'] : $item['itemPrice'],
                                'itemQuantity' => $item['itemQuantity'],
                                'itemShippingCost' => ($request->isCurrencyChanged == 'Y') ? $item['defaultItemShippingCost'] : $item['itemShippingCost'],
                                'itemTotalCost' => ($request->isCurrencyChanged == 'Y') ? $item['defaultItemTotalCost'] : $item['itemTotalCost'],
                            );
                        }
                    }


                    /* DELETE USER CART ITEMS */
                    $userCart = Usercart::where('userId', $userId)->where('type', 'autoparts')->delete();
                } else {
                    if (!empty($request->items)) {
                        $totalQuantity = $totalWeight = 0;
                        foreach ($request->items as $key => $item) {
                            $totalQuantity += $item['itemQuantity'];

                            /*  PREPARE DATA FOR INVOICE PARTICULARS */

                            /* INSERT DATA INTO STATUS LOG TABLE */
                            $procurementitemstatus = new Procurementitemstatus;
                            $procurementitemstatus->procurementId = $procurementId;
                            $procurementitemstatus->procurementItemId = $item['id'];
                            $procurementitemstatus->oldStatus = '';
                            $procurementitemstatus->status = 'submitted';
                            $procurementitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
                            $procurementitemstatus->save();

                            /* SET DATA FOR INVOICE PARTICULARS */
                            $packageDetails[$key] = array(
                                'id' => $item['id'],
                                'itemName' => $item['itemName'],
                                'websiteUrl' => $item['websiteUrl'],
                                'storeId' => $item['storeId'],
                                'siteCategoryId' => $item['siteCategoryId'],
                                'siteSubCategoryId' => $item['siteSubCategoryId'],
                                'siteProductId' => $item['siteProductId'],
                                'itemDescription' => $item['itemDescription'],
                                'year' => $item['year'],
                                'itemMake' => $item['itemMake'],
                                'itemModel' => $item['itemModel'],
                                'itemImage' => (empty($item['itemImage']) ? "" : $item['itemImage']),
                                'itemPrice' => ($request->isCurrencyChanged == 'Y') ? $item['defaultItemPrice'] : $item['itemPrice'],
                                'itemQuantity' => $item['itemQuantity'],
                                'itemShippingCost' => ($request->isCurrencyChanged == 'Y') ? $item['defaultItemShippingCost'] : $item['itemShippingCost'],
                                'itemTotalCost' => ($request->isCurrencyChanged == 'Y') ? $item['defaultItemTotalCost'] : $item['itemTotalCost'],
                            );
                        }
                    }
                }




                

                /*  COUPON DATA */
                if (isset($request->coupondetails) && !empty($couponCode)) {
                    /*  INSERT DATA INTO COUPON TABLE */
                    $data = array(
                        'couponId' => $couponId,
                        'code' => $couponCode,
                        'userId' => $userId,
                        'totalAmount' => $totalBDiscountCost,
                        'discountAmount' => $totalDiscount,
                        'discountPoint' => $discountPoint,
                    );
                    $couponLog = \App\Model\Couponlog::insertLog($data);

                    /*  UPDATE DATA IF COUPON POINTS */
                    if ($discountType == 'points') {
                        $fundpoint = \App\Model\Fundpoint::where('userId', $userId)->increment('point', $discountPoint);

                        $fundpointTransaction = new \App\Model\Fundpointtransaction;
                        $fundpointTransaction->userId = $userId;
                        $fundpointTransaction->point = $discountPoint;
                        $fundpointTransaction->date = Config::get('constants.CURRENTDATE');
                        $fundpointTransaction->save();
                    }
                }

                /*  UPDATE RFERRAL POINTS */
                $param = array();
                $param['userId'] = $userId;
                $paidAmount = $totalProcessingFee;
                if (!empty($request->data['shipmentMethodId'])) {
                    $paidAmount += $totalShippingCost;
                }
                $param['paidAmount'] = round(($paidAmount), 2);
                \App\Model\Fundpoint::calculateReferralBonus($param);

                // /* UPDATE PROCUREMENT TABLE */
                // if ($paymentStatus == 'paid') {
                //     $procurement = Procurement::find($procurementId);
                //     $procurement->paymentStatus = $paymentStatus;
                //     $procurement->paymentReceivedOn = Config::get('constants.CURRENTDATE');
                //     $procurement->save();
                // }

                if (empty($request->data['shipmentMethodId'])) {
                    /* UPDATE ADDRESS BOOK DEFAULT BILLING */
                    $billingAddress = array(
                        'firstName' => $request->personalDetails['billingFirstName'],
                        'lastName' => $request->personalDetails['billingLastName'],
                        'email' => $request->personalDetails['billingEmail'],
                        'address' => $request->personalDetails['billingAddress'],
                        'alternateAddress' => (!empty($request->personalDetails['billingAddress2']) ? $request->personalDetails['billingAddress2'] : ""),
                        'cityId' => $request->personalDetails['billingCity'],
                        'stateId' => $request->personalDetails['billingState'],
                        'countryId' => $request->personalDetails['billingCountry'],
                        'zipcode' => (!empty($request->personalDetails['billingZipCode']) ? $request->personalDetails['billingZipCode'] : ""),
                        'phone' => $request->personalDetails['billingContact'],
                        'isdCode' => isset($request->personalDetails['billingIsdcode']) ? $request->personalDetails['billingIsdcode'] : '',
                    );
                    \App\Model\Addressbook::where('userId', $userId)
                            ->where('isDefaultBilling', '1')
                            ->update($billingAddress);

                    /* UPDATE ADDRESS BOOK TABLE */
                    if (empty($request->personalDetails['locationId'])) {
                        $shippingAddress = array(
                            'firstName' => $request->personalDetails['shippingFirstName'],
                            'lastName' => $request->personalDetails['shippingLastName'],
                            'email' => $request->personalDetails['shippingEmail'],
                            'address' => $request->personalDetails['shippingAddress'],
                            'alternateAddress' => (!empty($request->personalDetails['shippingAddress2']) ? $request->personalDetails['shippingAddress2'] : ""),
                            'cityId' => $request->personalDetails['shippingCity'],
                            'stateId' => $request->personalDetails['shippingState'],
                            'countryId' => $request->personalDetails['shippingCountry'],
                            'zipcode' => (!empty($request->personalDetails['shippingZipCode']) ? $request->personalDetails['shippingZipCode'] : ""),
                            'phone' => $request->personalDetails['shippingContact'],
                            'isdCode' => isset($request->personalDetails['shippingIsdcode']) ? $request->personaldetails['shippingIsdcode'] : '',
                        );
                        \App\Model\Addressbook::where('userId', $userId)
                                ->where('isDefaultShipping', '1')
                                ->update($shippingAddress);
                    }
                }
                
                if(isset($request->tempDataId) && !empty($request->tempDataId)) {
                
                    $tempCartData = \App\Model\Temporarycartdata::find($request->tempDataId);
                    $tempCartData->isDataSaved = 'Y';
                    $tempCartData->dataSavedId = $procurementId;
                    $tempCartData->save();
                }
            }
            
        if ($paymentStatus == 'paid') {
            
            /*  PREPARE DATA FOR INVOICE PARTICULARS */


                $invoiceData = array(
                    'shipment' => array(
                        'urgent' => $request->data['urgent'],
                        'totalItemCost' => $totalItemCost,
                        'totalProcessingFee' => $totalProcessingFee,
                        'urgentPurchaseCost' => $urgentPurchaseCost,
                        'totalProcurementCost' => $totalProcurementCost,
                        'totalTax' => $totalTax,
                        'isInsuranceCharged' => isset($request->data['isInsuranceCharged']) ? $request->data['isInsuranceCharged'] : 'N',
                        'totalInsurance' => (isset($request->data['isInsuranceCharged']) && $request->data['isInsuranceCharged'] == 'Y') ? $totalInsurance : '',
                        'totalCost' => $totalCost,
                        'totalWeight' => $request->data['totalWeight'],
                        'totalQuantity' => $totalQuantity
                    ),
                    'warehouse' => array(
                        'fromAddress' => $warehouseData->address,
                        'fromZipCode' => $warehouseData->zipcode,
                        'fromCountry' => $warehouseData->countryId,
                        'fromState' => $warehouseData->stateId,
                        'fromCity' => $warehouseData->cityId,
                    ),
                    'shippingaddress' => array(
                        'toCountry' => $request->personalDetails['shippingCountry'],
                        'toState' => $request->personalDetails['shippingState'],
                        'toCity' => $request->personalDetails['shippingCity'],
                        'toAddress' => $request->personalDetails['shippingAddress'],
                        'toAlternateAddress' => (!empty($request->personalDetails['shippingAddress2']) ? $request->personalDetails['shippingAddress2'] : ""),
                        'toZipCode' => '',
                        'toName' => $request->personalDetails['shippingFirstName'] . " " . $request->personalDetails['shippingLastName'],
                        'toEmail' => $request->personalDetails['shippingEmail'],
                        'toPhone' => (isset($request->personalDetails['shippingIsdcode']) ? ($request->personalDetails['shippingIsdcode'] . $request->personalDetails['shippingContact']) : $request->personalDetails['shippingContact']),
                    ),
                    'packages' => $packageDetails,
                    'payment' => array(
                        'paymentMethodId' => $request->paymentMethod['paymentMethodId'],
                        'paymentMethodName' => $request->paymentMethod['paymentMethodName'],
                    ),
                );

                if (!empty($request->personalDetails['shippingZipCode'])) {
                    $invoiceData['shippingaddress']['toZipCode'] = $request->personalDetails['shippingZipCode'];
                }

                if (isset($request->coupondetails) && !empty($couponCode)) {
                    $invoiceData['shipment']['couponCode'] = $couponCode;
                    $invoiceData['shipment']['discountAmount'] = $discountAmount;
                    $invoiceData['shipment']['discountPoint'] = $discountPoint;
                    $invoiceData['shipment']['discountType'] = $discountType;
                }

                if ($request->paymentMethod['paymentMethodKey'] == 'wire_transfer') {
                    $invoiceData['payment']['poNumber'] = $request->wiretransfer['poNumber'];
                    $invoiceData['payment']['companyName'] = $request->wiretransfer['companyName'];
                    $invoiceData['payment']['buyerName'] = $request->wiretransfer['buyerName'];
                    $invoiceData['payment']['position'] = $request->wiretransfer['position'];
                }
                if ($request->paymentMethod['paymentMethodKey'] == 'ewallet') {
                    $invoiceData['payment']['ewalletId'] = $request->paymentdetails['ewalletId'];
                }

                if (!empty($request->data['shipmentMethodId'])) {
                    $invoiceData['shippingcharges']['shippingMethod'] = 'Y';
                    $invoiceData['shippingcharges']['shippingid'] = $request->data['shipmentMethodId'];
                    $invoiceData['shippingcharges']['shipping'] = $request->data['shippingMethodName'];
                    $invoiceData['shippingcharges']['estimateDeliveryDate'] = $request->data['estimateDeliveryDate'];
                    $invoiceData['shippingcharges']['isDutyCharged'] = $request->data['isDutyCharged'];
                    $invoiceData['shippingcharges']['shippingCost'] = ($request->isCurrencyChanged == 'Y') ? $request->data['defaultShippingCost'] : $request->data['shippingCost'];
                    $invoiceData['shippingcharges']['totalClearingDuty'] = $totalClearingDuty;
                    $invoiceData['shippingcharges']['totalShippingCost'] = $totalShippingCost;
                }


                /*  INSERT DATA INTO INVOICE TABLE */
                $invoiceUniqueId = 'INV' . $userData->unit . '-' . $procurementId . '-' . date('Ymd');
                $invoice = new \App\Model\Invoice;
                $invoice->invoiceUniqueId = $invoiceUniqueId;
                $invoice->procurementId = $procurementId;
                $invoice->type = 'autopart';
                $invoice->invoiceType = 'receipt';
                $invoice->userUnit = $userData->unit;
                $invoice->userFullName = $request->personalDetails['title'] . " " . $request->personalDetails['firstName'] . " " . $request->personalDetails['lastName'];
                $invoice->userEmail = $request->personalDetails['userEmail'];
                $invoice->userContactNumber = $request->personalDetails['phcode'] . $request->personalDetails['phone'];
                $invoice->billingName = ucfirst($request->personalDetails['billingFirstName']) . ' ' . ucfirst($request->personalDetails['billingLastName']);
                $invoice->billingEmail = $request->personalDetails['billingEmail'];
                $invoice->billingAddress = $request->personalDetails['billingAddress'];
                $invoice->billingAlternateAddress = (!empty($request->personalDetails['billingAddress2']) ? $request->personalDetails['billingAddress2'] : "");
                $invoice->billingCity = $request->personalDetails['billingCity'];
                $invoice->billingState = $request->personalDetails['billingState'];
                $invoice->billingCountry = $request->personalDetails['billingCountry'];
                if (!empty($request->personalDetails['billingZipCode']))
                    $invoice->billingZipcode = $request->personalDetails['billingZipCode'];
                $invoice->billingPhone = (isset($request->personalDetails['billingIsdcode']) ? $request->personalDetails['billingIsdcode'] . $request->personalDetails['billingContact'] : $request->personalDetails['billingContact']);
                $invoice->totalBillingAmount = $totalCost;
                $invoice->paymentMethodId = $request->paymentMethod['paymentMethodId'];
                $invoice->invoiceParticulars = json_encode($invoiceData);
                $invoice->createdOn = Config::get('constants.CURRENTDATE');
                $invoice->paymentStatus = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
                $invoice->save();
                $invoiceId = $invoice->id;

                /*  EMAIL INVOICE */
                if (!empty($invoiceId)) {
                    $data['invoice'] = \App\Model\Invoice::find($invoiceId);
                    $fileName = "Invoice_" . $invoiceUniqueId . ".pdf";
                    PDF::loadView('Administrator.autoparts.invoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                    $to = $request->personalDetails['userEmail'];
                    $content = "Dear " . $request->personaldetails['firstName'] . " " . $request->personaldetails['lastName'];
                    $content .= "Attached is your invoice for Auto Parts #" . $procurementId . "<br>Thank you for choosing Shoptomydoor. We appreciate your business.";
                    Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($invoiceUniqueId, $to, $fileName) {
                        $message->from('contact@shoptomydoor.com', 'Shoptomydoor');
                        $message->subject($invoiceUniqueId . " - Auto Parts Invoice");
                        $message->to($to);
                        $message->attach(public_path('exports/invoice/' . $fileName));
                    });
                }

                /*  INSERT DATA INTO PAYMENT TABLE */
                $paymentTransaction = new \App\Model\Paymenttransaction;
                $paymentTransaction->userId = $userId;
                $paymentTransaction->paymentMethodId = $request->paymentMethod['paymentMethodId'];
                $paymentTransaction->paidFor = 'autopart';
                $paymentTransaction->paidForId = $procurementId;
                $paymentTransaction->invoiceId = $invoice->id;
                $paymentTransaction->amountPaid = $totalCost;
                $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                $paymentTransaction->status = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
                if (!empty($transactionData))
                    $paymentTransaction->transactionData = $transactionData;
                if (!empty($transactionId))
                    $paymentTransaction->transactionId = $transactionId;

                $paymentTransaction->save();
            
        } else if ($paymentStatus == 'failed') {
            /*  INSERT DATA INTO PAYMENT TABLE */
            $paymentTransaction = new \App\Model\Paymenttransaction;
            $paymentTransaction->userId = $userId;
            $paymentTransaction->paymentMethodId = $request->paymentMethod['paymentMethodId'];
            $paymentTransaction->paidFor = 'shopforme';
            $paymentTransaction->amountPaid = $totalCost;
            $paymentTransaction->status = ($paymentMode == 'offline') ? 'unpaid' : $paymentStatus;
            if (!empty($transactionErrorMsg))
                $paymentTransaction->errorMsg = $transactionErrorMsg;
            $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
            $paymentTransaction->save();
            /*  INSERT DATA INTO PAYMENT TABLE */

        }
        
        if ($paymentStatus == 'paid') {
            return response()->json([
                        'status' => '1',
                        'results' => 'success',
            ]);
        } else {
            return response()->json([
                        'status' => '0',
                        'results' => 'payment_failed',
                        'msg' => !empty($paymentErrorMessage) ? $paymentErrorMessage.'. Please contact site admin.'  : 'There seems to be some issue with payment. Please contact site admin.'
            ]);
        }
    }

    public function getprocurementshippingcost(Request $request) {
        $shippingMethodData = $procurementItemData = array();
        $invoiceData = \App\Model\Invoice::where('procurementId', $request->procurementId)->where('invoiceType', 'invoice')->where('extraCostCharged', 'N')->where('deleted', '0')->first();

        if (!empty($invoiceData)) {
            $invoicParticulars = json_decode($invoiceData->invoiceParticulars, true);
            if (!empty($invoicParticulars)) {
                foreach ($invoicParticulars as $key => $row) {
                    if ($key != 'shippingcharges') {
                        $shippingMethodData[$key] = $row;
                    } else {
                        foreach ($row as $count => $shippingCharge) {
                            foreach ($shippingCharge as $field => $value) {
                                if ($field == 'companyLogo' && !empty($value)) {
                                    $shippingMethodData[$key][$count][$field] = asset('uploads/shipping/' . $value);
                                } else if ($field == 'shippingCost' || $field == 'duty' || $field == 'clearingDuty' || $field == 'clearing' || $field == 'totalShippingCost' || $field == 'total') {
                                    if ($field == 'clearing') {
                                        $clearingDuty = round($value, 2);
                                        $shippingMethodData[$key][$count][$field] = customhelper::getCurrencySymbolFormat(round($value, 2));
                                    } else if ($field == 'duty') {
                                        $clearingDuty = round($value, 2) + $clearingDuty;
                                        // $shippingMethodData[$key][$count]['clearingDuty'] = customhelper::getCurrencySymbolFormat($clearingDuty);
                                        $shippingMethodData[$key][$count]['isDutyCharged'] = !empty($value) ? true : false;
                                        $shippingMethodData[$key][$count][$field] = customhelper::getCurrencySymbolFormat(round($value, 2));
                                    } else
                                        $shippingMethodData[$key][$count][$field] = customhelper::getCurrencySymbolFormat(round($value, 2));
                                } else if ($field == 'days') {
                                    $shippingMethodData[$key][$count][$field] = $value;
                                    $shippingMethodData[$key][$count]['estimatedDeliveryDate'] = \Carbon\Carbon::now()->addWeekDays($value)->format('D, M dS Y');
                                } else {
                                    $shippingMethodData[$key][$count][$field] = $value;
                                }
                            }
                        }
                    }
                }
            }
        }

        return response()->json([
                    'data' => $shippingMethodData,
        ]);
    }

    /**
     * Method used to submit procurement data
     * @param integer $userId
     * @param object $request
     * @return boolean
     */
    public function submitrequestforcostdata($userId, Request $request) {

        $totalWeight = 0;
        $shippingId = (int) $request->shippingId;
        $param = array();

        /* Fetch User Addressbook Details */
        $addressBookData = User::find($userId)->addressbook()->where('isDefaultShipping', '1')->first();

        /* Fetch Procurement Details */
        $procurementData = Procurement::getShopForMeDetails($request->procurementId);

        /* Fetch Procurement Item Details */
        $procurementItemData = Procurementitem::getItemDetails($request->procurementId);

        /* Fetch Warehouse Details */
        $warehouseData = \App\Model\Warehouse::find($procurementData->warehouseId);

        /* Fetch User Details */
        $userData = User::find($userId);

        $totalProcurementCost = customhelper::getExtractCurrency($request->paymentDetails['totalProcurementCost']);

        $procurement = $procurement['items'] = array();
        $totalQuantity = 0;

        $procurement['id'] = $request->procurementId;
        $procurement['isCurrencyChanged'] = 'N';
        $procurement['defaultCurrencySymbol'] = $procurement['currencySymbol'] = customhelper::getCurrencySymbolCode();
        $procurement['defaultCurrencyCode'] = $procurement['currencyCode'] = customhelper::getCurrencySymbolCode('', true);
        if (!empty($procurementItemData)) {
            foreach ($procurementItemData as $key => $item) {
                $totalQuantity += $item['itemQuantity'];

                /* SET DATA INTO SESSION */
                $procurement['items'][$key]['id'] = $item['id'];
                $procurement['items'][$key]['itemName'] = $item['itemName'];
                $procurement['items'][$key]['websiteUrl'] = $item['websiteUrl'];
                $procurement['items'][$key]['storeId'] = $item['storeId'];
                $procurement['items'][$key]['siteCategoryId'] = $item['siteCategoryId'];
                $procurement['items'][$key]['siteSubCategoryId'] = $item['siteSubCategoryId'];
                $procurement['items'][$key]['siteProductId'] = $item['siteProductId'];
                $procurement['items'][$key]['options'] = $item['options'];
                $procurement['items'][$key]['itemPrice'] = customhelper::getExtractCurrency($item['itemPrice']);
                $procurement['items'][$key]['itemQuantity'] = $item['itemQuantity'];
                $procurement['items'][$key]['itemShippingCost'] = customhelper::getExtractCurrency($item['itemShippingCost']);
                $procurement['items'][$key]['itemTotalCost'] = customhelper::getExtractCurrency($item['itemTotalCost']);
            }
        }

        $procurement['data']['warehouseId'] = $procurementData->warehouseId;
        $procurement['data']['urgent'] = $procurementData->urgent;
        $procurement['data']['totalItemCost'] = customhelper::getExtractCurrency($request->paymentDetails['totalItemCost']);
        $procurement['data']['totalProcessingFee'] = customhelper::getExtractCurrency($request->paymentDetails['totalProcessingFee']);
        $procurement['data']['urgentPurchaseCost'] = customhelper::getExtractCurrency($request->paymentDetails['urgentPurchaseCost']);
        $procurement['data']['totalQuantity'] = $procurementData->totalQuantity;
        $procurement['data']['totalWeight'] = $procurementData->totalWeight;
        $procurement['data']['totalProcurementCost'] = $totalProcurementCost;
        $procurement['data']['isInsuranceCharged'] = 'N';
        $procurement['data']['totalTax'] = 0;
        $procurement['data']['totalDiscount'] = 0;
        $procurement['data']['totalBDiscountCost'] = 0;

        if ($shippingId > 0) {
            $param['userId'] = $userId;
            $param['totalWeight'] = $procurementData->totalWeight;
            $param['totalProcurementCost'] = $procurementData->totalItemCost;
            $param['totalQuantity'] = $procurementData->totalQuantity;
            $param['toCountry'] = $addressBookData->countryId;
            $param['toState'] = $addressBookData->stateId;
            $param['toCity'] = $addressBookData->cityId;
            $param['fromCountry'] = $warehouseData->countryId;
            $param['fromState'] = $warehouseData->stateId;
            $param['fromCity'] = $warehouseData->cityId;
            $param['shippingId'] = $shippingId;

            $procurement['data']['shipmentMethodId'] = $shippingId;
            $shippingChargeData = Shippingcharges::getShippingChargeData($param);
            $shippingMethod = \App\Model\Shippingmethods::where('shippingid', $shippingId)->first();

            if (!empty($shippingChargeData)) {
                $shippingChargeDetails = Shippingcharges::calculateShippingCharge($param, $shippingChargeData);
                $totalCost = trim($totalProcurementCost) + $shippingChargeDetails['shippingCost'] + $shippingChargeDetails['clearing'] + $shippingChargeDetails['duty'];

                $procurement['data']['shippingCost'] = $shippingChargeDetails['shippingCost'];
                $procurement['data']['totalClearingDuty'] = $shippingChargeDetails['clearing'] + $shippingChargeDetails['duty'];
                $procurement['data']['totalShippingCost'] = $shippingChargeDetails['clearing'] + $shippingChargeDetails['duty'] + $shippingChargeDetails['shippingCost'];
                $procurement['data']['totalInsurance'] = $shippingChargeDetails['insurance'];
                $procurement['data']['isInsuranceCharged'] = 'N';
                $procurement['data']['isDutyCharged'] = !empty($shippingChargeDetails['duty']) ? 'Y' : 'N';
                $procurement['data']['totalCost'] = $totalCost;
                $procurement['data']['shippingMethodName'] = $shippingMethod->shipping;
                $procurement['data']['estimateDeliveryDate'] = \Carbon\Carbon::now()->addWeekDays($shippingMethod->days)->format('Y-m-d');
            }
        } else {
            $procurement['data']['shipmentMethodId'] = '';
            $procurement['data']['totalCost'] = $totalProcurementCost;
        }


        return response()->json([
                    'status' => '1',
                    'results' => 'success',
                    'procurement' => $procurement,
        ]);
    }

    public function getinvoice(Request $request) {
        if (!empty($request->procurementId)) {
            $invoiceData = \App\Model\Invoice::where('procurementId', $request->procurementId)
                    ->where('extraCostCharged', 'N')
                    ->orderby('id', 'desc')
                    ->where('deleted', '0')
                    ->first();
            if (!empty($invoiceData)) {
                $data['invoice'] = $invoiceData;

                if ($invoiceData->type == 'buycarforme' && $invoiceData->invoiceType == 'invoice') {
                    $data['invoiceFor'] = 'frontend';
                    $returnHTML = view('Administrator.autoshipment.buyacarinvoice')->with($data)->render();
                } elseif ($invoiceData->type == 'buycarforme' && $invoiceData->invoiceType == 'receipt') {
                    $data['invoiceFor'] = 'frontend';
                    $returnHTML = view('Administrator.autoshipment.buyacarreceipt')->with($data)->render();
                } elseif ($invoiceData->type == 'autoshipment' && $invoiceData->invoiceType == 'invoice') {
                    $data['invoiceFor'] = 'frontend';
                    $returnHTML = view('Administrator.autoshipment.buyacarinvoice')->with($data)->render();
                } elseif ($invoiceData->type == 'autoshipment' && $invoiceData->invoiceType == 'receipt') {
                    $data['invoiceFor'] = 'frontend';
                    $returnHTML = view('Administrator.autoshipment.buyacarreceipt')->with($data)->render();
                } elseif ($invoiceData->type == 'autoshipment' && $invoiceData->invoiceType == 'receipt') {
                    $data['invoiceFor'] = 'frontend';
                    $returnHTML = view('Administrator.autoshipment.buyacarreceipt')->with($data)->render();
                } elseif ($invoiceData->type == 'autopart')
                    $returnHTML = view('Administrator.autoparts.invoice')->with($data)->render();
                elseif ($invoiceData->type == 'shopforme')
                    $returnHTML = view('Administrator.procurement.invoice')->with($data)->render();
                else
                    $returnHTML = view('Administrator.shipments.invoice')->with($data)->render();

                return response()->json([
                            'status' => '1',
                            'results' => 'success',
                            'data' => $returnHTML,
                ]);
            } else {
                return response()->json([
                            'status' => '0',
                            'results' => 'failed',
                ]);
            }
        }
    }

    public function getautopartsinvoice(Request $request) {
        if (!empty($request->procurementId)) {
            $invoiceData = \App\Model\Invoice::where('procurementId', $request->procurementId)
                    ->where('invoiceType', 'invoice')
                    ->where('extraCostCharged', 'N')
                    ->orderby('id', 'desc')
                    ->first();

            $data['invoice'] = \App\Model\Invoice::find($invoiceData->id);
            //print_r($data); die;
            $returnHTML = view('Administrator.autoparts.invoice')->with($data)->render();

            return response()->json([
                        'status' => '1',
                        'results' => 'success',
                        'data' => $returnHTML,
            ]);
        }
    }

    public function updateusercartautoparts(Request $request) {

        // print_r($request->all()); die;

        $procurement = array();
        $totalWeight = 0;
        $fromCurrency = $request->fromCurrency;
        $toCurrency = $request->toCurrency;
        $deafultCurrency = customhelper::getCurrencySymbolCode('', true);
        $procurement['defaultCurrencySymbol'] = customhelper::getCurrencySymbolCode();
        $procurement['defaultCurrencyCode'] = $deafultCurrency;
        $procurement['isCurrencyChanged'] = ($request->toCurrency != $deafultCurrency) ? 'Y' : 'N';
        $procurement['currencySymbol'] = customhelper::getCurrencySymbolCode($request->toCurrency);
        $procurement['currencyCode'] = $request->toCurrency;
        $procurement['exchangeRate'] = $exchangeRate = \App\Model\Currency::currencyExchangeRate($deafultCurrency, $request->toCurrency);


        $userCart = $request->usercart;

        if (!empty($userCart['items'])) {
            foreach ($userCart['items'] as $key => $item) {
                /* SET DATA INTO SESSION */
                $procurement['items'][$key]['itemName'] = $item['itemName'];
                $procurement['items'][$key]['websiteUrl'] = $item['websiteUrl'];
                $procurement['items'][$key]['storeId'] = $item['storeId'];
                $procurement['items'][$key]['siteCategoryId'] = $item['siteCategoryId'];
                $procurement['items'][$key]['siteSubCategoryId'] = $item['siteSubCategoryId'];
                $procurement['items'][$key]['siteProductId'] = $item['siteProductId'];
                $procurement['items'][$key]['options'] = (!empty($item['options']) ? $item['options'] : '');
                $procurement['items'][$key]['itemDescription'] = (!empty($item['itemDescription']) ? $item['itemDescription'] : '');
                $procurement['items'][$key]['year'] = (!empty($item['itemYear']) ? $item['itemYear'] : '');
                $procurement['items'][$key]['itemMake'] = (!empty($item['itemMake']) ? $item['itemMake'] : '');
                $procurement['items'][$key]['itemModel'] = (!empty($item['itemModel']) ? $item['itemModel'] : '');
                $procurement['items'][$key]['itemQuantity'] = $item['itemQuantity'];

                $itemPrice = isset($item['defaultItemPrice']) ? $item['defaultItemPrice'] : $item['itemPrice'];
                $itemShippingCost = isset($item['defaultItemShippingCost']) ? $item['defaultItemShippingCost'] : $item['itemShippingCost'];
                $itemTotalCost = isset($item['defaultItemTotalCost']) ? $item['defaultItemTotalCost'] : $item['itemTotalCost'];

                if ($deafultCurrency != $request->toCurrency) {
                    $procurement['items'][$key]['itemPrice'] = \App\Model\Currency::currencyConverter($itemPrice, $exchangeRate);
                    $procurement['items'][$key]['itemShippingCost'] = \App\Model\Currency::currencyConverter($itemShippingCost, $exchangeRate);
                    $procurement['items'][$key]['itemTotalCost'] = \App\Model\Currency::currencyConverter($itemTotalCost, $exchangeRate);
                } else {
                    $procurement['items'][$key]['itemPrice'] = $item['defaultItemPrice'];
                    $procurement['items'][$key]['itemShippingCost'] = $item['defaultItemShippingCost'];
                    $procurement['items'][$key]['itemTotalCost'] = $item['defaultItemTotalCost'];
                }

                $procurement['items'][$key]['defaultItemPrice'] = $itemPrice;
                $procurement['items'][$key]['defaultItemShippingCost'] = $itemShippingCost;
                $procurement['items'][$key]['defaultItemTotalCost'] = $itemTotalCost;
            }
        }

        if (!empty($userCart['data'])) {
            $cartData = $userCart['data'];

            $cartData['totalDiscount'] = 0;
            $procurement['data']['warehouseId'] = $cartData['warehouseId'];
            $procurement['data']['urgent'] = $cartData['urgent'];
            $procurement['data']['totalQuantity'] = $cartData['totalQuantity'];
            $procurement['data']['totalWeight'] = $cartData['totalWeight'];
            $procurement['data']['shipmentMethodId'] = !empty($cartData['shipmentMethodId']) ? $cartData['shipmentMethodId'] : '';

            $totalItemCost = isset($cartData['defaultTotalItemCost']) ? $cartData['defaultTotalItemCost'] : $cartData['totalItemCost'];
            $totalProcessingFee = isset($cartData['defaultTotalProcessingFee']) ? $cartData['defaultTotalProcessingFee'] : $cartData['totalProcessingFee'];
            $urgentPurchaseCost = isset($cartData['defaultUrgentPurchaseCost']) ? $cartData['defaultUrgentPurchaseCost'] : $cartData['urgentPurchaseCost'];
            $totalProcurementCost = isset($cartData['defaultTotalProcurementCost']) ? $cartData['defaultTotalProcurementCost'] : $cartData['totalProcurementCost'];
            $totalTax = isset($cartData['defaultTotalTax']) ? $cartData['defaultTotalTax'] : $cartData['totalTax'];
            $totalDiscount = isset($cartData['defaultTotalDiscount']) ? $cartData['defaultTotalDiscount'] : $cartData['totalDiscount'];
            $totalCost = isset($cartData['defaultTotalCost']) ? $cartData['defaultTotalCost'] : $cartData['totalCost'];

            if ($deafultCurrency != $request->toCurrency) {
                $procurement['data']['totalItemCost'] = \App\Model\Currency::currencyConverter($totalItemCost, $exchangeRate);
                $procurement['data']['totalProcessingFee'] = \App\Model\Currency::currencyConverter($totalProcessingFee, $exchangeRate);
                $procurement['data']['urgentPurchaseCost'] = \App\Model\Currency::currencyConverter($urgentPurchaseCost, $exchangeRate);
                $procurement['data']['totalProcurementCost'] = \App\Model\Currency::currencyConverter($totalProcurementCost, $exchangeRate);
                $procurement['data']['totalTax'] = \App\Model\Currency::currencyConverter($totalTax, $exchangeRate);
                $procurement['data']['totalDiscount'] = \App\Model\Currency::currencyConverter($totalDiscount, $exchangeRate);
                $procurement['data']['totalCost'] = \App\Model\Currency::currencyConverter($totalCost, $exchangeRate);
            } else {
                $procurement['data']['totalItemCost'] = $cartData['defaultTotalItemCost'];
                $procurement['data']['totalProcessingFee'] = $cartData['defaultTotalProcessingFee'];
                $procurement['data']['urgentPurchaseCost'] = $cartData['defaultUrgentPurchaseCost'];
                $procurement['data']['totalProcurementCost'] = $cartData['defaultTotalProcurementCost'];
                $procurement['data']['totalTax'] = $cartData['defaultTotalTax'];
                $procurement['data']['totalDiscount'] = $cartData['defaultTotalDiscount'];
                $procurement['data']['totalCost'] = $cartData['defaultTotalCost'];
            }

            $procurement['data']['defaultTotalItemCost'] = $totalItemCost;
            $procurement['data']['defaultTotalProcessingFee'] = $totalProcessingFee;
            $procurement['data']['defaultUrgentPurchaseCost'] = $urgentPurchaseCost;
            $procurement['data']['defaultTotalProcurementCost'] = $totalProcurementCost;
            $procurement['data']['defaultTotalTax'] = $totalTax;
            $procurement['data']['defaultTotalDiscount'] = $totalDiscount;
            $procurement['data']['defaultTotalCost'] = $totalCost;

            if (!empty($cartData['shipmentMethodId'])) {
                $procurement['data']['isDutyCharged'] = $cartData['isDutyCharged'];
                $procurement['data']['shippingMethodName'] = $cartData['shippingMethodName'];
                $procurement['data']['estimateDeliveryDate'] = $cartData['estimateDeliveryDate'];

                $shippingCost = isset($cartData['defaultShippingCost']) ? $cartData['defaultShippingCost'] : $cartData['shippingCost'];
                $totalShippingCost = isset($cartData['defaultTotalShippingCost']) ? $cartData['defaultTotalShippingCost'] : $cartData['totalShippingCost'];
                if (!empty($cartData['totalInsurance'])) {
                    $totalInsurance = isset($cartData['defaultTotalInsurance']) ? $cartData['defaultTotalInsurance'] : $cartData['totalInsurance'];
                } else {
                    $totalInsurance = isset($cartData['defaultTotalInsurance']) ? $cartData['defaultTotalInsurance'] : 0;
                }

                $totalClearingDuty = isset($cartData['defaultClearingDuty']) ? $cartData['defaultClearingDuty'] : $cartData['totalClearingDuty'];

                if ($deafultCurrency != $request->toCurrency) {
                    $procurement['data']['shippingCost'] = \App\Model\Currency::currencyConverter($shippingCost, $exchangeRate);
                    $procurement['data']['totalShippingCost'] = \App\Model\Currency::currencyConverter($totalShippingCost, $exchangeRate);
                    $procurement['data']['totalInsurance'] = \App\Model\Currency::currencyConverter($totalInsurance, $exchangeRate);
                    $procurement['data']['totalClearingDuty'] = \App\Model\Currency::currencyConverter($totalClearingDuty, $exchangeRate);
                } else {
                    $procurement['data']['shippingCost'] = $cartData['defaultShippingCost'];
                    $procurement['data']['totalShippingCost'] = $cartData['defaultTotalShippingCost'];
                    $procurement['data']['totalInsurance'] = $cartData['defaultTotalInsurance'];
                    $procurement['data']['totalClearingDuty'] = $cartData['defaultClearingDuty'];
                }

                $procurement['data']['defaultShippingCost'] = $shippingCost;
                $procurement['data']['defaultTotalShippingCost'] = $totalShippingCost;
                $procurement['data']['defaultTotalInsurance'] = $totalInsurance;
                $procurement['data']['defaultClearingDuty'] = $totalClearingDuty;
            }
        }

        return response()->json([
                    'status' => '1',
                    'results' => 'success',
                    'procurement' => $procurement
        ]);
    }

    /**
     * Method used get procurement(Auto Parts) data
     * @param integer $userId
     * @param object $request
     * @return boolean
     */
    public function submitrequestforcostautopartsdata($userId, Request $request) {

        $totalWeight = 0;
        $shippingId = (int) $request->shippingId;
        $param = array();

        /* Fetch User Addressbook Details */
        $addressBookData = User::find($userId)->addressbook()->where('isDefaultShipping', '1')->first();

        /* Fetch Procurement Details */
        $procurementData = Procurement::getAutopartsDetails($request->procurementId);

        /* Fetch Procurement Item Details */
        $procurementItemData = Procurementitem::getAutoPartsItemDetails($request->procurementId);

        /* Fetch Warehouse Details */
        $warehouseData = \App\Model\Warehouse::find($procurementData->warehouseId);

        /* Fetch User Details */
        $userData = User::find($userId);

        $totalProcurementCost = customhelper::getExtractCurrency($request->paymentDetails['totalProcurementCost']);

        $procurement = $procurement['items'] = array();
        $totalQuantity = 0;

        $procurement['id'] = $request->procurementId;
        $procurement['isCurrencyChanged'] = 'N';
        $procurement['defaultCurrencySymbol'] = $procurement['currencySymbol'] = customhelper::getCurrencySymbolCode();
        $procurement['defaultCurrencyCode'] = $procurement['currencyCode'] = customhelper::getCurrencySymbolCode('', true);
        if (!empty($procurementItemData)) {
            foreach ($procurementItemData as $key => $item) {
                $totalQuantity += $item['itemQuantity'];

                /* SET DATA INTO SESSION */
                $procurement['items'][$key]['id'] = $item['id'];
                $procurement['items'][$key]['itemName'] = $item['itemName'];
                $procurement['items'][$key]['websiteUrl'] = $item['websiteUrl'];
                $procurement['items'][$key]['storeId'] = $item['storeId'];
                $procurement['items'][$key]['siteCategoryId'] = $item['siteCategoryId'];
                $procurement['items'][$key]['siteSubCategoryId'] = $item['siteSubCategoryId'];
                $procurement['items'][$key]['siteProductId'] = $item['siteProductId'];
                $procurement['items'][$key]['options'] = $item['options'];
                $procurement['items'][$key]['itemDescription'] = (!empty($item['itemDescription']) ? $item['itemDescription'] : '');
                $procurement['items'][$key]['year'] = (!empty($item['itemYear']) ? $item['itemYear'] : '');
                $procurement['items'][$key]['itemMake'] = (!empty($item['itemMake']) ? $item['itemMake'] : '');
                $procurement['items'][$key]['itemModel'] = (!empty($item['itemModel']) ? $item['itemModel'] : '');
                $procurement['items'][$key]['itemPrice'] = customhelper::getExtractCurrency($item['itemPrice']);
                $procurement['items'][$key]['itemQuantity'] = $item['itemQuantity'];
                $procurement['items'][$key]['itemShippingCost'] = customhelper::getExtractCurrency($item['itemShippingCost']);
                $procurement['items'][$key]['itemTotalCost'] = customhelper::getExtractCurrency($item['itemTotalCost']);
            }
        }

        $procurement['data']['warehouseId'] = $procurementData->warehouseId;
        $procurement['data']['urgent'] = $procurementData->urgent;
        $procurement['data']['totalItemCost'] = customhelper::getExtractCurrency($request->paymentDetails['totalItemCost']);
        $procurement['data']['totalProcessingFee'] = customhelper::getExtractCurrency($request->paymentDetails['totalProcessingFee']);
        $procurement['data']['urgentPurchaseCost'] = customhelper::getExtractCurrency($request->paymentDetails['urgentPurchaseCost']);
        $procurement['data']['totalQuantity'] = $procurementData->totalQuantity;
        $procurement['data']['totalWeight'] = $procurementData->totalWeight;
        $procurement['data']['totalProcurementCost'] = $totalProcurementCost;
        $procurement['data']['isInsuranceCharged'] = 'N';
        $procurement['data']['totalTax'] = 0;

        if ($shippingId > 0) {
            $param['userId'] = $userId;
            $param['totalWeight'] = $procurementData->totalWeight;
            $param['totalProcurementCost'] = $procurementData->totalItemCost;
            $param['totalQuantity'] = $procurementData->totalQuantity;
            $param['toCountry'] = $addressBookData->countryId;
            $param['toState'] = $addressBookData->stateId;
            $param['toCity'] = $addressBookData->cityId;
            $param['fromCountry'] = $warehouseData->countryId;
            $param['fromState'] = $warehouseData->stateId;
            $param['fromCity'] = $warehouseData->cityId;
            $param['shippingId'] = $shippingId;

            $procurement['data']['shipmentMethodId'] = $shippingId;
            $shippingChargeData = Shippingcharges::getShippingChargeData($param);
            $shippingMethod = \App\Model\Shippingmethods::where('shippingid', $shippingId)->first();

            if (!empty($shippingChargeData)) {
                $shippingChargeDetails = Shippingcharges::calculateShippingCharge($param, $shippingChargeData);
                $totalCost = trim($totalProcurementCost) + $shippingChargeDetails['shippingCost'] + $shippingChargeDetails['clearing'] + $shippingChargeDetails['duty'];

                $procurement['data']['shippingCost'] = $shippingChargeDetails['shippingCost'];
                $procurement['data']['totalClearingDuty'] = $shippingChargeDetails['clearing'] + $shippingChargeDetails['duty'];
                $procurement['data']['totalShippingCost'] = $shippingChargeDetails['clearing'] + $shippingChargeDetails['duty'] + $shippingChargeDetails['shippingCost'];
                $procurement['data']['totalInsurance'] = $shippingChargeDetails['insurance'];
                $procurement['data']['isInsuranceCharged'] = 'N';
                $procurement['data']['isDutyCharged'] = !empty($shippingChargeDetails['duty']) ? 'Y' : 'N';
                $procurement['data']['totalCost'] = $totalCost;
                $procurement['data']['shippingMethodName'] = $shippingMethod->shipping;
                $procurement['data']['estimateDeliveryDate'] = \Carbon\Carbon::now()->addWeekDays($shippingMethod->days)->format('Y-m-d');
            }
        } else {
            $procurement['data']['shipmentMethodId'] = '';
            $procurement['data']['totalCost'] = $totalProcurementCost;
        }


        return response()->json([
                    'status' => '1',
                    'results' => 'success',
                    'procurement' => $procurement,
        ]);
    }

    public function getprocurementItems(Request $request) {

        $param = array();

        $totalQuantity = 0;

        /* Fetch Procurement Details */
        $procurementData = Procurement::getShopForMeDetails($request->procurementId);

        /* Fetch Procurement Item Details */
        $procurementItemData = Procurementitem::getItemDetails($request->procurementId);

        /* Fetch User Details */
        $userData = User::find($request->userId);

        if (!empty($procurementItemData)) {
            foreach ($procurementItemData as $key => $item) {
                $totalQuantity += $item['itemQuantity'];

                /* SET DATA INTO SESSION */
                $procurement['items'][$key]['id'] = $item['id'];
                $procurement['items'][$key]['itemName'] = $item['itemName'];
                $procurement['items'][$key]['websiteUrl'] = $item['websiteUrl'];
                $procurement['items'][$key]['storeId'] = $item['storeId'];
                $procurement['items'][$key]['siteCategoryId'] = $item['siteCategoryId'];
                $procurement['items'][$key]['siteSubCategoryId'] = $item['siteSubCategoryId'];
                $procurement['items'][$key]['siteProductId'] = $item['siteProductId'];
                $procurement['items'][$key]['options'] = $item['options'];
                $procurement['items'][$key]['itemDescription'] = (!empty($item['itemDescription']) ? $item['itemDescription'] : '');
                $procurement['items'][$key]['year'] = (!empty($item['itemYear']) ? $item['itemYear'] : '');
                $procurement['items'][$key]['itemMake'] = (!empty($item['itemMake']) ? $item['itemMake'] : '');
                $procurement['items'][$key]['itemModel'] = (!empty($item['itemModel']) ? $item['itemModel'] : '');
                $procurement['items'][$key]['itemPrice'] = customhelper::getCurrencySymbolFormat($item['itemPrice'], $procurementData->paidCurrencyCode);
                $procurement['items'][$key]['itemQuantity'] = $item['itemQuantity'];
                $procurement['items'][$key]['itemShippingCost'] = customhelper::getCurrencySymbolFormat($item['itemShippingCost'], $procurementData->paidCurrencyCode);
                $procurement['items'][$key]['itemTotalCost'] = customhelper::getCurrencySymbolFormat($item['itemTotalCost'], $procurementData->paidCurrencyCode);

                $procurement['items'][$key]['status'] = $item['status'];
            }
        }

        return response()->json([
                    'status' => '1',
                    'results' => 'success',
                    'procurement' => $procurement,
        ]);
    }

    public function getautopartsItems(Request $request) {

        $param = array();

        $totalQuantity = 0;

        /* Fetch Procurement Details */
        $procurementData = Procurement::getAutopartsDetails($request->procurementId);

        /* Fetch Procurement Item Details */
        $procurementItemData = Procurementitem::getAutoPartsItemDetails($request->procurementId);



        /* Fetch User Details */
        $userData = User::find($request->userId);

        if (!empty($procurementItemData)) {
            foreach ($procurementItemData as $key => $item) {
                $totalQuantity += $item['itemQuantity'];

                /* SET DATA INTO SESSION */
                $procurement['items'][$key]['id'] = $item['id'];
                $procurement['items'][$key]['itemName'] = $item['itemName'];
                $procurement['items'][$key]['websiteUrl'] = $item['websiteUrl'];
                $procurement['items'][$key]['storeId'] = $item['storeId'];
                $procurement['items'][$key]['siteCategoryId'] = $item['siteCategoryId'];
                $procurement['items'][$key]['siteSubCategoryId'] = $item['siteSubCategoryId'];
                $procurement['items'][$key]['siteProductId'] = $item['siteProductId'];
                $procurement['items'][$key]['options'] = $item['options'];
                $procurement['items'][$key]['itemPrice'] = customhelper::getCurrencySymbolFormat($item['itemPrice'], $procurementData->paidCurrencyCode);
                $procurement['items'][$key]['itemQuantity'] = $item['itemQuantity'];
                $procurement['items'][$key]['itemShippingCost'] = customhelper::getCurrencySymbolFormat($item['itemShippingCost'], $procurementData->paidCurrencyCode);
                $procurement['items'][$key]['itemTotalCost'] = customhelper::getCurrencySymbolFormat($item['itemTotalCost'], $procurementData->paidCurrencyCode);
                $procurement['items'][$key]['status'] = $item['status'];
            }
        }

        return response()->json([
                    'status' => '1',
                    'results' => 'success',
                    'procurement' => $procurement,
        ]);
    }

}
