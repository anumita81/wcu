<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Customerhelpcategories;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;

class SendussdController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {

        $data = array();

        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Sendussdcode'), Auth::user()->id);
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('USSDCODE');
            \Session::push('USSDCODE.searchDisplay', $searchDisplay);
            \Session::push('USSDCODE.field', $field);
            \Session::push('USSDCODE.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('USSDCODE.field');
            $sortType = \Session::get('USSDCODE.type');
            $searchDisplay = \Session::get('USSDCODE.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
//        $sort = array(
//            'name' => array('current' => 'sorting'),
//            'orders' => array('current' => 'sorting'),
//            'status' => array('current' => 'sorting'),
//        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETUSSDCODE make LIST  */
        $ussdCodeLogData = \App\Model\Customerussdcodelog::orderBy($param['field'], $param['type'])->paginate($param['searchDisplay']);
        if (count($ussdCodeLogData) > 0) {
            $data['page'] = $ussdCodeLogData->currentPage();
        } else {
            $data['page'] = 1;
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Send USSD Code to Customer";
        $data['contentTop'] = array('breadcrumbText' => 'Send USSD Code to Customer', 'contentTitle' => 'Send USSD Code to Customer', 'pageInfo' => 'This section allows you to Send USSD Code to Customer');
        $data['pageTitle'] = "Send USSD Code to Customer";
        $data['ussdCodeLogData'] = $ussdCodeLogData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavContent', 'menuSub' => 'leftNavCustomerHelp2', 'menuSubSub' => 'leftNavManageCategories111');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.customerussdcode.index', $data);
    }
    
    public function createCode($id, $page, Request $request) {
        
        $data = array();
        if (\Request::isMethod('post')) {
            $userDeatils = \App\Model\User::where('unit',$request->userUnit)->first();
            if(!empty($userDeatils)) {
                $ussdCodeDetails = \App\Model\UssdPaymentSettings::find($request->bank);
                if(!empty($ussdCodeDetails)) {
                    $ussdCode = $ussdCodeDetails->bankUssdCode.'*'.$request->amount.'*'.$ussdCodeDetails->bankAccountNumber.'#';
                    
                    $sendCodeLog = new \App\Model\Customerussdcodelog();
                    $sendCodeLog->userId = $userDeatils->id;
                    $sendCodeLog->unit = $request->userUnit;
                    $sendCodeLog->user = $userDeatils->firstName.' '.$userDeatils->lastName;
                    $sendCodeLog->amount = $request->amount;
                    $sendCodeLog->bank = $request->bank;
                    $sendCodeLog->ussdcode = $ussdCode;
                    $sendCodeLog->createdBy = Auth::user()->email;
                    $sendCodeLog->createdOn = Config::get('constants.CURRENTDATE');
                    if($sendCodeLog->save()) {
                        
                        $to = $userDeatils->email;
                        $replace['[NAME]'] = $userDeatils->firstName . " " . $userDeatils->lastName;
                        $replace['[USSD_CODE]'] = $ussdCode;
                        $emailTemplate = \App\Model\Emailtemplate::where('templateKey','ussdcode_to_customer')->first();
                        $sendMail = customhelper::SendMail($emailTemplate, $replace, $to);
                        
                        $smsTemplate = \App\Model\Smstemplate::where('templateKey','ussdcode_to_customer')->first();
                        $toMobile = trim($userDeatils->isdCode.$userDeatils->contactNumber);
                        $isSendMsg = customhelper::sendMSG($toMobile,$replace,$smsTemplate);
                        
                        return redirect('/administrator/sendussdcode?page=' . $page)->with('successMessage', 'USSD code succesfully send to user.');
                    } else {
                        return redirect('/administrator/sendussdcode?page=' . $page)->with('errorMessage', 'Something Went wrong. Unable to send USSD code.');
                    }
                } else {
                    return redirect('/administrator/sendussdcode?page=' . $page)->with('errorMessage', 'Something Went wrong. Unable to fetch USSD code.');
                }
            } else {
                return redirect('/administrator/sendussdcode?page=' . $page)->with('errorMessage', 'Unable to get user details. Please verify unit number');
            }
            
        }
        $ussedSettingsData = \App\Model\UssdPaymentSettings::where("status","1")->where("deleted","0")->get();
        $data['ussedSettingsData'] = $ussedSettingsData;
        $data['id'] = $id;
        $data['page'] = $page;
        return view('Administrator.customerussdcode.createcode', $data);
    }
}

