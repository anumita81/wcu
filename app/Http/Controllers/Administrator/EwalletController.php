<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Ewallet;
use App\Model\User;
use App\Model\Emailtemplate;
use App\Model\Generalsettings;
use Auth;

use Illuminate\Contracts\Auth\Authenticatable;
use customhelper;
use Config;

/*use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;*/

class EwalletController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 30;
    }


    public function index (Request $request) {
        
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Ewallet'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        
        $param = array();
        $dataObj = new Ewallet;
        \Session::forget('RECORD');
        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */

            $sortField = \Input::get('field', 'id');
            $sortOrder = \Input::get('type', 'desc');
            $perpage = \Input::get('searchDisplay', $this->_perPage);
            $searchData = \Input::get('searchData', "");
            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('RECORD');
            \Session::push('RECORD.searchData', $searchData);
            \Session::push('RECORD.searchDisplay', $perpage);
            \Session::push('RECORD.field', $sortField);
            \Session::push('RECORD.type', $sortOrder);

        }
        else
        {
            $sortField = \Session::get('RECORD.field');
            $sortType = \Session::get('RECORD.type');
            $perpage = \Session::get('RECORD.searchDisplay');
            $searchData = \Session::get('RECORD.searchData');

            $sortField = !empty($sortField) ? $sortField[0] : 'id';
            $sortOrder = !empty($sortType) ? $sortType[0] : 'desc';
            $perpage = !empty($perpage) ? $perpage[0] : $this->_perPage;
            $searchData = !empty($searchData) ? $searchData[0] : "";

        }
        
        $param['sortField'] = $sortField;
        $param['sortOrder'] = $sortOrder;
        $param['searchDisplay'] = $perpage;
        $param['searchData'] = $searchData;

        $data['param'] = $param;
        $records = $dataObj->getData($param);
        $data['records'] = $records;
        $data['allStatus'] = $dataObj->getAllStatus();
        $data['page'] = $records->currentPage();
        $data['pageTitle'] = 'E-Wallet';
        $data['title'] = "E-Wallet :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText'=>array('Orders','E-Wallet'),'contentTitle'=>'E-Wallet','pageInfo'=>'This section allows you to browse through the E-Wallet ordered at your store');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.ewallet.index',$data);
    }

    public function addData($id = '-1',$page = 1) {

        $data = array();

        $data['title'] = "Homepage Banners :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText' => 'Banner', 'contentTitle' => 'Homepage Banner', 'pageInfo' => 'This section allows you to manage homepage banners');
        $data['page'] = $page;
        if ($id != -1) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $bannerRecord = Ewallet::find($id);
            $data['bannerRecord'] = $bannerRecord;
        } else {
            $data['action'] = 'Add';
            $data['bannerRecord'] = new Ewallet;
        }
        $data['emailTemplate'] = Emailtemplate::where('templateKey','=','e-wallet')->get();
        return view('Administrator.ewallet.addData', $data);
    }

    public function getDetails($id = '-1', $page = 1) {

        $data = array();
        $dataObj = new Ewallet;
        $userInfo = new User;
        if($id != '-1')
        {
            $dataObj = $dataObj->find($id);
            $userInfo = User::find($dataObj->recipient);
        }

        $data['record'] = $dataObj;
        $data['userInfo'] = $userInfo;
        return view('Administrator.ewallet.details', $data);

    }

    public function saveData($page = 1,Request $request) {

        $validator = Validator::make($request->all(), [
                    'recipient' => 'required',
                    'amount' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            
            if($request->input('userId')!='')
            {
                $userInfo = User::find($request->input('userId'));
            }
            $checkEwallet = Ewallet::where('recipientEmail','=',$userInfo->email)->get();
            //echo count($checkEwallet);exit;
            if(count($checkEwallet)>0)
            {
                return redirect(route('ewallet').'?page='.$page)->withErrors('E-Wallet already exist with this user');
            }
            else
            {

                $emailTemplate = Emailtemplate::where('templateKey', 'e-wallet')->first();
                $emailTemplate->templateBody = $request->input('message');
                $to = $userInfo->email;
                $replace['[NAME]'] = $request->input('recipient');
                $isSend = customhelper::SendMail($emailTemplate, $replace, $to);
                if($isSend)
                {
                    $dataObj = new Ewallet;
                    $dataObj->ewalletId = strtoupper(md5(uniqid(rand())));
                    $dataObj->purchaser = 'Shoptomydoor';
                    $dataObj->recipient = $userInfo->id;
                    $dataObj->recipientEmail = $userInfo->email;
                    $dataObj->message = addslashes($request->input('message'));
                    $dataObj->amount = $request->input('amount');
                    $dataObj->debit = $request->input('amount');
                    $dataObj->status = '1';
                    $dataObj->addDate = date('Y-m-d');

                    if($dataObj->save())
                    {
                        $ewalletTransaction = new \App\Model\Ewallettransaction;
                        $ewalletTransaction->ewalletId = $dataObj->id;
                        $ewalletTransaction->userType = 'admin';
                        $ewalletTransaction->userId = Auth::user()->id;
                        $ewalletTransaction->amount = $request->input('amount');
                        $ewalletTransaction->transactionType = 'credit';
                        $ewalletTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                        $ewalletTransaction->save();
                    }
                    
                    return redirect(route('ewallet').'?page='.$page)->with('successMessage', 'Information saved successfuly.');
                }
                else
                {
                    return redirect(route('ewallet').'?page='.$page)->with('errorMessage', 'Check your email settings.');
                }
            }

        }
    }

    public function deleteData($id = '-1', $page = 1) {

        $dataObj = new Ewallet;
        if($id != '-1')
        {
            $dataObj = $dataObj->find($id);
            if($dataObj->delete())
                return redirect(route('ewallet').'?page='.$page)->with('successMessage', 'Information deleted successfuly.');
        }
    }

    public function deleteselected(Request $request) {

        $dataObj = new Ewallet;
        $idsToDelete = $request->input('deleteData');
        foreach ($idsToDelete as $eachId) {
            $dataObj = $dataObj->find($eachId);
            if($dataObj->delete())
                $dataObj = new Ewallet;
        }
        return redirect(route('ewallet'))->with('successMessage', 'Information deleted successfuly.');
    }

    public function updatestatus($page = 1, $id='-1',Request $request) {

        $dataObj = new Ewallet;
        if($id != '-1')
            $dataObj = $dataObj->find($id);
        $dataObj->status = $request->input('status');
        if($dataObj->save())
            return redirect(route('ewallet').'?page='.$page)->with('successMessage', 'Status updated successfuly.');

    }

    public function getSettings() {

        $data = array();
        $records = Generalsettings::getSettingsData('ewalletSettings');
        $data['records'] = $records;
        $data['pageTitle'] = 'E-Wallet Settings';
        $data['title'] = "E-Wallet Settings :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText'=>array('Orders','E-Wallet Settings'),'contentTitle'=>'E-Wallet Settings','pageInfo'=>'This section allows you to manage E-Wallet settings');

        return view('Administrator.ewallet.settings',$data);

    }

    public function editSettings(Request $request) {


        $postData = $request->input('data');
        $error = '';
        //print_r($postData);exit;
        foreach($postData as $dataId => $dataVal)
        {
            $dataObj = new Generalsettings;
            $dataObj = $dataObj->find($dataId);

            $validator = Validator::make($dataVal, [
                        'settingsValue' => 'required|numeric',
            ]);
            if ($validator->fails()) {
                $dataErr= $validator->errors(); 
                //echo $dataErr;
                foreach ($dataErr->all() as $eacherror)
                    $error .= str_replace('The settings value', $dataObj->settingsTitle, $eacherror).' ';
                continue;
            }
            
            $dataObj->oldValue = $dataObj->settingsValue;
            $dataObj->settingsValue = $dataVal['settingsValue'];
            $dataObj->save();

        }
        if($error !='')
        {
            //$error = '<ul>'.$error.'</ul>';
            return redirect(route('ewalletSettings'))->withErrors($error);
        }
        else
            return redirect(route('ewalletSettings'))->with('successMessage', 'Information updated successfuly.');
    }
}