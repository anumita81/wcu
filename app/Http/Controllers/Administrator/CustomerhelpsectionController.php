<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Customerhelpsection;
use App\Model\Customerhelpcategories;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;

class CustomerhelpsectionController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {

        $data = array();

        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.CustomerHelpSections'), Auth::user()->id);
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $searchByCat = \Input::get('searchByCat', '');

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('CHS');
            \Session::push('CHS.searchDisplay', $searchDisplay);
            \Session::push('CHS.field', $field);
            \Session::push('CHS.type', $type);
            \Session::push('CHS.searchByCat', $searchByCat);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
            $param['searchByCat'] = $searchByCat;
        } else {
            $sortField = \Session::get('CHS.field');
            $sortType = \Session::get('CHS.type');
            $searchDisplay = \Session::get('CHS.searchDisplay');
            $searchByCat = \Session::get('CHS.searchByCat');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
            $param['searchByCat'] = !empty($searchByCat) ? $searchByCat[0] : '';
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'title' => array('current' => 'sorting'),
            'orders' => array('current' => 'sorting'),
            'catName' => array('current' => 'sorting'),
            'status' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH make LIST  */
        /*$chCatData = Customerhelpsection::where('deleted', '0')->orderBy($param['field'], $param['type'])->paginate($param['searchDisplay']);
        if (count($chCatData) > 0) {
            $data['page'] = $chCatData->currentPage();
        } else {
            $data['page'] = 1;
        }*/

        $chCatData = Customerhelpsection::getList($param);
        if (count($chCatData) > 0) {
            $data['page'] = $chCatData->currentPage();
        } else{
            $data['page'] = 1;
        }
        $data['catList'] = Customerhelpcategories::where('deleted', '0')->where("status", '1')->get();

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Customer Help Sections";
        $data['contentTop'] = array('breadcrumbText' => 'Customer Help Sections', 'contentTitle' => 'Customer Help Sections', 'pageInfo' => 'This section allows you to manage Customer Help Sections');
        $data['pageTitle'] = "Customer Help Sections";
        $data['chCatData'] = $chCatData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavContent', 'menuSub' => 'leftNavCustomerHelp2', 'menuSubSub' => 'leftNavManageSections111');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.customerhelp.sections.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $id
     * @param string $page
     * @return array
     */
    public function addedit($id = '0', $page = '') {
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.CustomerHelpSections'), Auth::user()->id); 
        $data = array();
        $data['title'] = "Administrative Panel :: Customer Help Sections";
        $data['contentTop'] = array('breadcrumbText' => 'Customer Help Sections', 'contentTitle' => 'Customer Help Sections', 'pageInfo' => 'This section allows you to manage Customer Help Sections');
        $data['page'] = !empty($page) ? $page : '1';
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavContent', 'menuSub' => 'leftNavCustomerHelp2', 'menuSubSub' => 'leftNavManageSections111');
        /* FETCH category LIST  */
        $data['catList'] = Customerhelpcategories::where('deleted', '0')->where("status", '1')->get();

        if (!empty($id)) {
            if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['pageTitle'] = "Edit Customer Help Section";
            $data['chCatId'] = $id;
            //$data['page'] = $page;
            $data['action'] = 'Edit';
            $data['chCatData'] = Customerhelpsection::find($id);
        } else {
            if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add New Section for Customer Help";
            //$data['page'] = $page;
        }
        return view('Administrator.customerhelp.sections.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $id
     * @param string $page
     * @param Request $request
     * @return array
     */
    public function savedata($id, $page, Request $request) {

        $data = array();
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.CustomerHelpSections'), Auth::user()->id);
        $data['page'] = !empty($page) ? $page : '1';
        $chCat = new Customerhelpsection;

        /* VALIDATATION */
        $validator = Validator::make(\Input::all(), [
            'title' => 'required',
            //'content' => 'required',
        ]);

        /* IF VALIDATATIONS FAILS */
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

            /*  EDIT  */
            if ($id != 0) { 
                if($findRole['canEdit'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }

                /* Check Whether any leading zero */
                $orders = ltrim($request->displayOrder, 0);

                if($orders == ''){
                    return redirect('/administrator/customerhelpsections?page=' . $page)->with('errorMessage', 'Order cannot be 0');
                }

                $chCat = Customerhelpsection::find($id);
                $chCat->updatedBy = Auth::user()->id;
                $chCat->updatedOn = Config::get('constants.CURRENTDATE');
                $chCat->orders = $orders;
            } 
            /*  ADD  */
            else {
                if($findRole['canAdd'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
                
                $getLastOrders = Customerhelpsection::orderBy('id', 'desc')->first();
                $lastOrder = (count($getLastOrders) > 0 ? $getLastOrders->orders+1 : 1);
                $newOrder = $lastOrder;
                $chCat->orders = $newOrder;
                $chCat->createdBy = Auth::user()->id;
                $chCat->createdOn = Config::get('constants.CURRENTDATE');
                $chCat->slug = strtolower(str_replace(' ','-',preg_replace('/[^\w\s]/', '', $request->input('title'))));
                
            }
            $chCat->title = $request->title;
            $chCat->content = $request->content;
            $chCat->catId = $request->catId;

            /* SAVING THE DATA */
            $chCat->save();
        }


        return redirect('/administrator/customerhelpsections?page=' . $page)->with('successMessage', 'Section saved successfuly.');
    }


    /**
     * Method used to change status
     * @param integer $id
     * @param string $page
     * @param string $status
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        /* CHECK AT LEAST ONE SECTION LIVE */
        $checkLive = Customerhelpsection::where("deleted", '0')->where("status", "1")->count();
        if($checkLive == 1 && $status == 0){
            return \Redirect::to('administrator/customerhelpsections/?page=' . $page)->with('errorMessage', 'At least one help section should activated');
        }

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Customerhelpsection::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/customerhelpsections/?page=' . $page)->with('successMessage', 'Status changed successfully.');
            } else {
                return \Redirect::to('administrator/customerhelpsections/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/customerhelpsections/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }



    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.CustomerHelpSections'), Auth::user()->id); 
        if($findRole['canDelete'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $page = !empty($page) ? $page : '1';

        /* CHECK AT LEAST ONE SECTION LIVE */
        $checkLive = Customerhelpsection::where("deleted", '0')->where("status", "1")->count();
        if($checkLive == 1){
            return \Redirect::to('administrator/customerhelpsections/?page=' . $page)->with('errorMessage', 'At least one help section should activated');
        }

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Customerhelpsection::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/customerhelpsections?page=' . $page)->with('successMessage', 'Record deleted successfully.');
            } else {
                return \Redirect::to('administrator/customerhelpsections?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/customerhelpsections?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    
}
