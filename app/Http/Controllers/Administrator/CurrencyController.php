<?php
namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Currency;
use App\Model\Defaultcurrency;
use App\Model\Generalsettings;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class CurrencyController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Currency'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('CURRENCYDATA');
            \Session::push('CURRENCYDATA.searchDisplay', $searchDisplay);
            \Session::push('CURRENCYDATA.field', $field);
            \Session::push('CURRENCYDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('CURRENCYDATA.field');
            $sortType = \Session::get('CURRENCYDATA.type');
            $searchDisplay = \Session::get('CURRENCYDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'name';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
            'code' => array('current' => 'sorting'),
            'exchangeRate' => array('current' => 'sorting'),
            'modifiedOn' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH CURRENCY LIST  */
        $currencyData = Currency::getCurrencyList($param);

        /* GET THE VALUES FROM GENERAL SETTINGS FOR CRON JOB */
        $selectGeneralSettingsSetCron = \App\Model\Generalsettings::where('groupName', 'CurrencySettings')->where('settingsKey', 'auto_update')->get();
        $selectGeneralSettingsCromTime = \App\Model\Generalsettings::where('groupName', 'CurrencySettings')->where('settingsKey', 'update_scheduled_time')->get();

        $data['auto_update'] = $selectGeneralSettingsSetCron[0]->settingsValue;
        $data['update_scheduled_time'] = $selectGeneralSettingsCromTime[0]->settingsValue;

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Currencies";
        $data['contentTop'] = array('breadcrumbText' => 'Currencies', 'contentTitle' => 'Currencies', 'pageInfo' => 'This section allows you to manage currencies');
        $data['pageTitle'] = "Currencies";
        $data['page'] = $currencyData->currentPage();
        $data['currencyData'] = $currencyData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.currency.index', $data);
    }

    /**
     * Method for add edit currency
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['page'] = !empty($page) ? $page : '1';
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $data['pageTitle'] = "Edit Currency";
            $currency = Currency::find($id);
            $data['defaultCurrency'] = Defaultcurrency::find($currency['currencyId']);
            $data['currency'] = $currency;
            $data['formatList'] = Config::get('constants.currencyFormat');
            return view('Administrator.currency.edit', $data);
        } else {
            $data['defaultCurrencyList'] = Defaultcurrency::all();
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add Currency";
            return view('Administrator.currency.add', $data);
        }
    }

    /*
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $currency = new Currency;

        if (empty($id)) {
            $validator = Validator::make($request->all(), [
                        'currencyId' => 'required|unique:' . $currency->table . ',currencyId,"1",deleted',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                        'format' => 'required',
                        'exchangeRate' => 'required',
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $currency = Currency::find($id);
                $currency->format = $request->format;
                $currency->symbol = $request->symbol;
                $currency->exchangeRate = $request->exchangeRate;
                $currency->modifiedBy = Auth::user()->id;
                $currency->modifiedOn = Config::get('constants.CURRENTDATE');
            } else {
                $currency->currencyId = $request->currencyId;
                $currency->createdBy = Auth::user()->id;
                $currency->createdOn = Config::get('constants.CURRENTDATE');
            }

            $currency->save();
            $currencyId = $currency->id;
            echo $currencyId; die;
            return redirect('/administrator/currency?page=' . $page)->with('successMessage', 'Currency information saved successfuly.');
        }
    }

    /**
     * Method for managing currency exchange rate settings
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function settings($id = '0', $page = '') {
        $data = array();
        $data['page'] = !empty($page) ? $page : '1';
        $data['pageTitle'] = "Manage Settings";
        $settings = Generalsettings::where('groupname', 'currencySettings')->get();

        if (!empty($settings)) {
            foreach ($settings as $row) {
                $data['settings'][$row['settingsKey']] = $row['settingsValue'];
            }
        }

        return view('Administrator.currency.settings', $data);
    }

    /*
     * Method used to save currency exchange rate settings
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savesettings($page, Request $request) {
        $generalsettings = new Generalsettings;

        $request->except('_token');

        $settingsdata = $request->settings;

        foreach ($settingsdata as $key => $val) {
            //If post new value and db settings value are not same then db old value will updated with settings value 
            if ($key != "_token") {
                $settings = Generalsettings::where('settingsKey', $key)->get(['id', 'settingsValue']);
                $settingsId = $settings[0]['id'];

                $generalsettings = Generalsettings::find($settingsId);
                $generalsettings->settingsValue = $val;
                $generalsettings->oldValue = $settings[0]['settingsValue'];
                $generalsettings->save();
            }
        }

        return \Redirect::to('administrator/currency/?page=' . $page)->with('successMessage', 'Currency settings updated successfully.');
    }

    /**
     * Method used to set primary currency
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function setdefault($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Currency::setDefault($id, $createrModifierId)) {
                $this->updatecurrencyDefault();
                return \Redirect::to('administrator/currency/?page=' . $page)->with('successMessage', 'Currency set as default successfully.');
            } else {
                return \Redirect::to('administrator/currency/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/currency/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to change status
     * @param integer $id
     * @param integer $page
     *  @param integer $status
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Currency::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/currency/?page=' . $page)->with('successMessage', 'Currency status changed successfully.');
            } else {
                return \Redirect::to('administrator/currency/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/currency/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Currency::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/currency/?page=' . $page)->with('successMessage', 'Currency deleted successfully.');
            } else {
                return \Redirect::to('administrator/currency/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/currency/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method for update currency rates
     * @param Request $request
     * @return string
     */
    public function updatecurrency(Request $request) {

        /* GET THE SELECTED API */
        $selectedApi = $request->has('converterApi') ? $request->converterApi : '1';
        
        /* GET THE Default Currency */
        $defaultCurrency = Currency::getDefaultCurrency();
        
        /* GET ALL SET CURRENCIES EXCEPT DEFAULT */
        $data['allSetCurrency'] = Currency::getAllCurrencyListExceptDefault();
        
        foreach($data['allSetCurrency'] as $key => $val){
            
            $curl = curl_init();
            $fromCountry = $defaultCurrency[0]->code;

            /* SET EXCEPTIONS */
            if($val->code == 'GHC') {
                $toSetCountry = 'GHS';
            } else {
                $toSetCountry = $val->code;
            }
            if($selectedApi ==  1){

                /* GOOGLE CURRENCY API CALL */
                $toCountry = $toSetCountry;
                $combined = $fromCountry."_".$toCountry;
                
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => 'http://free.currencyconverterapi.com/api/v3/convert?q='.$combined,
                ));
                
                $resp = curl_exec($curl);
                curl_close($curl);
                $decoded = json_decode($resp);
            }

            /* IF EXCHANGE RATE NOT FOUND */
            if($decoded->query->count == 0){
                $exchangeRate = 0.00;
            } else {
                $exchangeRate = $decoded->results->$combined->val;
            }
           
            /* SAVING THE RATES */
            $currency = Currency::find($val->id);
            $currency->exchangeRate = $exchangeRate;
            $currency->save();
        }

        return \Redirect::to('administrator/currency/')->with('successMessage', 'Rates applied successfully.');

    }



    /**
     * Method for update currency rates when no request set
     * @return string
     */
    public function updatecurrencyDefault() {

        /* GET THE SELECTED API */
        $selectedApi = '1';
        
        /* GET THE Default Currency */
        $defaultCurrency = Currency::getDefaultCurrency();
        
        /* GET ALL SET CURRENCIES EXCEPT DEFAULT */
        $data['allSetCurrency'] = Currency::getAllCurrencyListExceptDefault();
        
        foreach($data['allSetCurrency'] as $key => $val){
            
            $curl = curl_init();
            $fromCountry = $defaultCurrency[0]->code;

            /* SET EXCEPTIONS */
            if($val->code == 'GHC') {
                $toSetCountry = 'GHS';
            } else {
                $toSetCountry = $val->code;
            }
            if($selectedApi ==  1){

                /* GOOGLE CURRENCY API CALL */
                $toCountry = $toSetCountry;
                $combined  = $fromCountry."_".$toCountry;
                
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => 'http://free.currencyconverterapi.com/api/v3/convert?q='.$combined,
                ));
                
                $resp = curl_exec($curl);
                curl_close($curl);
                $decoded = json_decode($resp);
            }

            /* IF EXCHANGE RATE NOT FOUND */
            if($decoded->query->count == 0){
                $exchangeRate = 0.00;
            } else {
                $exchangeRate = $decoded->results->$combined->val;
            }
           
            /* SAVING THE RATES */
            $currency = Currency::find($val->id);
            $currency->exchangeRate = $exchangeRate;
            $currency->save();
        }

        
    }


     /**
     * Method for update cron time
     * @return string
     */
    public function updatecurrencycrontime(Request $request) {

        /* GET AND SET THE VARIABLES */
        $currency_cron_set = $request->has('cronCurrencySet') ? $request->cronCurrencySet : 'off';
            if($currency_cron_set == 'on'){
                $currency_cron_set =  '1';
            } else {
                $currency_cron_set =  '0';
            }
            
            $timeHH = $request->timeHH;
            $timeMM = $request->timeMM;
            $combined = $timeHH.":".$timeMM;

            $selectGeneralSettingsSetCron = \App\Model\Generalsettings::where('groupName', 'CurrencySettings')->where('settingsKey', 'auto_update')->get(['id', 'settingsValue']);
            $settingsId = $selectGeneralSettingsSetCron[0]['id'];
            
            $updateGeneralSettingsSetCron = \App\Model\Generalsettings::find($settingsId);
            $updateGeneralSettingsSetCron->settingsValue = $currency_cron_set;
            $updateGeneralSettingsSetCron->oldValue = $selectGeneralSettingsSetCron[0]->settingsValue;
            $updateGeneralSettingsSetCron->save();

            $selectGeneralSettingsCromTime = \App\Model\Generalsettings::where('groupName', 'CurrencySettings')->where('settingsKey', 'update_scheduled_time')->get(['id', 'settingsValue']);
            $settingsId = $selectGeneralSettingsCromTime[0]['id'];

            $updateGeneralSettingsCromTime = \App\Model\Generalsettings::find($settingsId);
            $updateGeneralSettingsCromTime->settingsValue = $combined;
            $updateGeneralSettingsCromTime->oldValue = $selectGeneralSettingsCromTime[0]->settingsValue;
            $updateGeneralSettingsCromTime->save();

            return \Redirect::to('administrator/currency/')->with('successMessage', 'Cron settings updated successfully.');
    }


///////////////////////////////////////////////////Currency Conversion/////////////////////////////////////////////////////////


    public function currencyconversion(Route $route, Request $request)
    {
        
            $data = array();
            $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Currencyconversion'), Auth::user()->id); // call the helper function


            if($findRole['canView'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }

            if (\Request::isMethod('post')) {
                /* GET POST VALUE  */
                $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

                $field = \Input::get('field', 'id');
                $type = \Input::get('type', 'desc');

                /*  SET SESSION VALUE FOR SORTING  */
                \Session::forget('CURRENCYDATA');
                \Session::push('CURRENCYDATA.searchDisplay', $searchDisplay);
                \Session::push('CURRENCYDATA.field', $field);
                \Session::push('CURRENCYDATA.type', $type);

                $param['field'] = $field;
                $param['type'] = $type;
                $param['searchDisplay'] = $searchDisplay;
            } else {
                $sortField = \Session::get('CURRENCYDATA.field');
                $sortType = \Session::get('CURRENCYDATA.type');
                $searchDisplay = \Session::get('CURRENCYDATA.searchDisplay');

                $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
                $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
                $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
            }

            /* BUILD SORTING ARRAY */
            $sort = array(
                'name' => array('current' => 'sorting'),
                'code' => array('current' => 'sorting'),
                'exchangeRate' => array('current' => 'sorting'),
                'modifiedOn' => array('current' => 'sorting'),
            );

            /* SET SORTING ARRAY  */
            $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

            /* FETCH CURRENCY LIST  */
            $currencyData = \App\Model\Currencyconversion::getCurrencyList($param);


            /* SET DATA FOR VIEW  */
            $data['title'] = "Administrative Panel :: Currencies";
            $data['contentTop'] = array('breadcrumbText' => 'Currencies', 'contentTitle' => 'Currencies', 'pageInfo' => 'This section allows you to manage currencies');
            $data['pageTitle'] = "Currency Exchange";
            $data['page'] = $currencyData->currentPage();
            $data['currencyData'] = $currencyData;
            $data['searchData'] = $param;
            $data['sort'] = $sort;
            $data['canView'] = $findRole['canView'];
            $data['canAdd'] = $findRole['canAdd'];
            $data['canEdit'] = $findRole['canEdit'];
            $data['canDelete'] = $findRole['canDelete'];

            return view('Administrator.currency.currencyconversion', $data);

    }

    public function addexchangerate($id = '0', $page = '')
    {
        $data = array();
        $currencyData = new Currency;
        $currencyDataTable = $currencyData->table;
        $defaultCurrencyData = new Defaultcurrency;
        $defaultCurrencyDataTable = $defaultCurrencyData->table;

        $data['page'] = !empty($page) ? $page : '1';
            $data['id'] = $id;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add Currency Exchange Rate";

            $data['defaultCurrencyList'] = Defaultcurrency::select(array('code','name'))->leftJoin($currencyDataTable, $currencyDataTable.".currencyId", "=", $defaultCurrencyDataTable.".id")->orderBy($currencyDataTable.".deleted", "desc")->get(); 
            
            return view('Administrator.currency.addexchangerate', $data);  
    }

    public function editexchangerate($id , $page = '')
    {
        $data = array();
        $currencyData = new Currency;
        $currencyDataTable = $currencyData->table;
        $defaultCurrencyData = new Defaultcurrency;
        $defaultCurrencyDataTable = $defaultCurrencyData->table;

        $data['page'] = !empty($page) ? $page : '1';
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $data['pageTitle'] = "Edit Currency";
            $currency = \App\Model\Currencyconversion::find($id);
            $data['currency'] = $currency;

            $data['defaultCurrencyList'] = Defaultcurrency::select(array('code','name'))->leftJoin($currencyDataTable, $currencyDataTable.".currencyId", "=", $defaultCurrencyDataTable.".id")->orderBy($currencyDataTable.".deleted", "desc")->get(); 
            
            return view('Administrator.currency.editexchangerate', $data);        

    }

    public function saveexchangerate($id, $page, Request $request)
    {
        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $currency = new \App\Model\Currencyconversion;
        $currencyDataTable = $currency->table;

        if (empty($id)) {
            $validator = Validator::make($request->all(), [
                        'fromCurrency' => 'required',
                        'toCurrency' => 'required',
                        'exchangeRate' => 'required',
                       
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                        'fromCurrency' => 'required',
                        'toCurrency' => 'required',
                        'exchangeRate' => 'required',
           ]);
        }


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $currency = \App\Model\Currencyconversion::find($id);
                $currency->fromCurrency = $request->fromCurrency;
                $currency->toCurrency = $request->toCurrency;
                $currency->exchangeRate = $request->exchangeRate;
                $currency->status = '1';
                $currency->deleted = '0';
                $currency->updatedBy = Auth::user()->id;
                $currency->updatedOn = Config::get('constants.CURRENTDATE');
            } else {
                if(!empty($request->fromCurrency) && !empty($request->toCurrency))
                {
                    $validunique = \App\Model\Currencyconversion:: where('fromCurrency', $request->fromCurrency)->where('toCurrency', $request->toCurrency)->where('deleted', '0')->get();

                    if(count($validunique)>0)
                    {
                        return redirect('/administrator/currencyconversion')->with('errorMessage', 'Rate for selcted currency combination is exist. Please select others or edit the same.');
                    }else{

                        $currency->fromCurrency = $request->fromCurrency;
                        $currency->toCurrency = $request->toCurrency;
                        $currency->exchangeRate = $request->exchangeRate;
                        $currency->status = '1';
                        $currency->deleted = '0';
                        $currency->createdBy = Auth::user()->id;
                        $currency->createdOn = Config::get('constants.CURRENTDATE');
                    }

                }
                
            }
                
           

            $currency->save();

            return redirect('/administrator/currencyconversion')->with('successMessage', 'Currency conversion information saved successfuly.');
        }

    }

     /**
     * Method used to change status
     * @param integer $id
     * @param integer $page
     *  @param integer $status
     * @return type
     */
    public function changeratestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (\App\Model\Currencyconversion::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/currencyconversion/?page=' . $page)->with('successMessage', 'Currency status changed successfully.');
            } else {
                return \Redirect::to('administrator/currencyconversion/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/currencyconversion/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
