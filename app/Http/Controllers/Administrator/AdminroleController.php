<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserAdmin;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class AdminroleController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {
        $data = array();

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Adminrole'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }


        if (\Request::isMethod('post')) { 
            /* GET POST VALUE  */
            
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('ADMINROLEDATA');
            \Session::push('ADMINROLEDATA.searchDisplay', $searchDisplay);
            \Session::push('ADMINROLEDATA.field', $field);
            \Session::push('ADMINROLEDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('ADMINROLEDATA.field');
            $sortType = \Session::get('ADMINROLEDATA.type');
            $searchDisplay = \Session::get('ADMINROLEDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'userTypeName' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /*  CHECK USER TYPE  */
        if(session()->get('admin_session_user_userType') != 1){
            return \Redirect::to('administrator/dashboard/')->with('errorMessage', 'You don\'t have required permission');
        }

        $adminRoleData = UserAdmin::fetchAdminUserType($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Admin Roles";
        $data['contentTop'] = array('breadcrumbText' => 'Admin Roles', 'contentTitle' => 'Admin Roles', 'pageInfo' => 'This section allows you to manage admin profile');
        $data['pageTitle'] = "Admin Roles";
        $data['adminRoleData'] = $adminRoleData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers', 'menuSub' => 'leftNavAdmin4', 'menuSubSub' => 'leftNavAdminRoles57');

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        
        return view('Administrator.role.index', $data);
    }

    

    /**
     * Method used to change status
     * @param integer $id
     * @param integer $status
     * @return object
     */
    public function changestatus($id, $status) {

        /* GET USER ID  */
        $createrModifierId = Auth::user()->id;

        /* CHECK WHETHER THE USER ID IS NOT EMPTY  */
        if (!empty($id)) {

            /* CHANGE STATUS  */
            if (UserAdmin::changeStatusUserType($id, $createrModifierId, $status)) {

                /* STATUS HAS CHANGED SUCCESSFULLY  */
                return \Redirect::to('administrator/adminrole/')->with('successMessage', 'Role status changed successfully.');
            } else {

                /* UNSUCCESSFULLY ATTEMPT */
                return \Redirect::to('administrator/adminrole/')->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/adminrole/')->with('errorMessage', 'Error in operation!');
        }
    }
   

}
