<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\libraries\dbHelpers;
use Auth;
use App\Model\User;
use App\Model\Ewallet;
use App\Model\Emailtemplate;
use Hash;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Routing\Route;
use Config;
use customhelper;

class FundwalletController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
    }

    public function index() {
        $data = array();

        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.FundWallet'), Auth::user()->id);

        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Fund Gift Card :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Fund Gift Card";
        $data['contentTop'] = array('breadcrumbText' => 'Fund Gift Card', 'contentTitle' => 'Fund Gift Card', 'pageInfo' => 'This section allows you to fund E-Wallet');

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers', 'menuSub' => '', 'menuSubSub' => 'leftNavFundUserEWallet4');
        return view('Administrator.fundwallet.index', $data);
    }

    /**
     * Method for get the information
     * @return array
     */
    public function store(Request $request) {
        $data = array();
        $data['unit'] = $request->unit;
        $data['email'] = $request->email;

        /*  REDIRECT IF NOTHING ENTERED  */
        if ($request->unit == '' && $request->email == '') {
            return \Redirect::to('administrator/fundwallet/')->with('errorMessage', 'Please enter any one of the below field');
        } else {
            /*  FIND USER WALLET INFO EITHER BY UNIT NUMBER OR BY EMAIL  */
            $customerRecord = User::where('id', '-1');
            if ($request->unit != '')
                $customerRecord->orWhere('unit', $request->unit);
            if ($request->email != '')
                $customerRecord->orWhere('email', $request->email);
            $customerRecord = $customerRecord->first();
        }

        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.FundWallet'), Auth::user()->id);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Fund Gift Card :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Fund Gift Card";
        $data['contentTop'] = array('breadcrumbText' => 'Fund Gift Card', 'contentTitle' => 'Fund Gift Card', 'pageInfo' => 'This section allows you to fund E-Wallet');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers0', 'menuSub' => '', 'menuSubSub' => 'leftNavFundUserEWallet4');


        /* NO USER WALLET RECORD FOUND  */
        if (empty($customerRecord)) {
            return \Redirect::to('administrator/fundwallet/')->with('errorMessage', 'No record found!');
        } else {
            /* FIND E-WALLET INFO  */
            $data['customerRecord'] = $customerRecord;
            $ewalletRecord = Ewallet::where('recipientEmail', $customerRecord->email)->first();
            if (count($ewalletRecord) > 0)
                $data['ewalletRecord'] = $ewalletRecord;
            else
                $data['ewalletRecord'] = array();
        }

        return view('Administrator.fundwallet.index', $data);
    }

    /**
     * Method for show wallet info
     * @param integer $id
     * @return array
     */
    public function show($id) {
        /*  SET SESSION VALUE  */
        $uniqueId = \Session::get('FUNDWALLET.unit');
        $email = \Session::get('FUNDWALLET.email');

        $data = array();
        $data['unit'] = $uniqueId[0];
        $data['email'] = $email[0];

        /*  FIND USER WALLET INFO EITHER BY UNIT NUMBER OR BY EMAIL  */
        if ($data['unit'] != '') {
            $customerRecord = User::where('uniqueId', $uniqueId[0])->get();
        } else {
            $customerRecord = User::where('email', $email[0])->get();
        }
        /* SET DATA FOR VIEW  */
        $data['title'] = "Fund Gift Card :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Fund Gift Card";
        $data['contentTop'] = array('breadcrumbText' => 'Fund Gift Card', 'contentTitle' => 'Fund Gift Card', 'pageInfo' => 'This section allows you to fund E-Wallet');
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers0', 'menuSub' => '', 'menuSubSub' => 'leftNavFundUserEWallet4');

        if (!empty($customerRecord)) {
            if (!isset($customerRecord[0])) {
                // DO NOTHING
            }
            if (isset($customerRecord[0])) {
                $data['customerRecord'] = $customerRecord;
                /* GET THE AMOUNT  */
                $amount = FundWallet::where('userid', $customerRecord[0]->id)->get();
                $data['amount'] = $amount;
            }
        }

        return view('Administrator.fundwallet.index', $data);
    }

    /**
     * Method for update the wallet amount
     * @param integer $id
     * @return array
     */
    public function update(Request $request, $id) {

        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.FundWallet'), Auth::user()->id);

        $dataObj = new Ewallet;
        $userObj = new User;

        $userId = $request->userId;
        $ewalletId = $request->ewalleid;

        /*  FIND USER ID  */
        if ($userId != '')
            $userDetails = $userObj->find($userId);

        if ($ewalletId != 0) {
            $dataObj = $dataObj->find($ewalletId);
            $dataObj->amount = $dataObj->amount + $request->amount;
        } else {
            $dataObj->ewalletId = strtoupper(md5(uniqid(rand())));
            $dataObj->purchaser = 'Shoptomydoor';
            $dataObj->recipient = $userDetails->id;
            $dataObj->recipientEmail = $userDetails->email;
            $dataObj->message = addslashes($request->input('message'));
            $dataObj->amount = $request->input('amount');
            $dataObj->debit = '0.00';
            $dataObj->status = '1';
            $dataObj->addDate = time();
        }

        /*  MAIL SEND TO RESPECTIVE USER  */
        $emailTemplate = Emailtemplate::where('templateKey', 'e-wallet')->first();
        $to = $userDetails->email;
        $replace['[NAME]'] = $userDetails->firstName . ' ' . $userDetails->lastName;
        $isSend = customhelper::SendMail($emailTemplate, $replace, $to);


        $data['user'] = User::find($id);
        $mailTemplatevars = Config::get('constants.emailTemplateVariables');

        $arrayvalues = [];
        foreach ($mailTemplatevars as $key => $val) {
            if (isset($replace[$val]) == $val) {
                $arrayvalues[] = $replace[$val];
            } else {
                $arrayvalues[] = '';
            }
        }

        $content = str_replace($mailTemplatevars, $arrayvalues, stripslashes($emailTemplate->templateBody));

        /* INSERT DATA INTO NOTIFICATION TABLE */
        customhelper::insertUserNotification($id, Auth::user()->id, $content, $type = 3, $emailTemplate->templateSubject);


        $isSend = true;
        if ($isSend) {
            /*  WALLET INFO UPDATED  */
            if ($dataObj->save()) {
                $ewalletTransaction = new \App\Model\Ewallettransaction;
                $ewalletTransaction->ewalletId = $dataObj->id;
                $ewalletTransaction->userType = 'admin';
                $ewalletTransaction->userId = Auth::user()->id;
                $ewalletTransaction->amount = $request->input('amount');
                $ewalletTransaction->transactionType = 'credit';
                $ewalletTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                $ewalletTransaction->save();

                return redirect(url('/administrator/fundwallet'))->with('successMessage', 'Information saved successfuly.');
            }
        } else {
            return redirect('/administrator/fundwallet')->with('errorMessage', 'Check your email settings.');
        }
    }

}
