<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\UserAdmin;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Illuminate\Routing\Route;
use customhelper;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Response;

class DbbackupController extends Controller {
    
    public function __construct() {
        $this->middleware('auth:admin');
    }
    
    public function index() {
        
        $data = array();
        
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.DBbackup-restore'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        
        $data['dbBackupData'] = \App\Model\Dbbackuplog::orderBy('id','desc')->get();
        $data['title'] = "Administrative Panel :: DB Backup & Restore";
        $data['contentTop'] = array('breadcrumbText' => 'DB Backup & Restore', 'contentTitle' => 'DB Backup & Restore', 'pageInfo' => 'This section allows you to manage DB Backup & Restore');
        $data['pageTitle'] = "DB Backup & Restore";
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        $data['page'] = 0;
        
        return view('Administrator.dbbackup.index', $data);
    }
    
    public function showbackup($id = 0,$page = 0) {
        return view('Administrator.dbbackup.createbackup');
    }


    public function createbackup() {
//        $backup = exec('php /var/www/html/backend/stmd/artisan db:backup');
//        echo var_dump($backup);
//        $backup = \Artisan::call('db:backup');
//        echo var_dump($backup);
        
        $newFileName = 'backup_'.date('Y-m-d-h-i-s').'.sql';
        $this->process = new Process(sprintf(
            'mysqldump -u%s -p%s %s > %s',
            config('database.connections.mysql.username'),
            config('database.connections.mysql.password'),
            config('database.connections.mysql.database'),
            public_path('/backups/'.$newFileName)
        ));
        
        if($this->process->mustRun())
        {
            $dbbackup = new \App\Model\Dbbackuplog;
            $dbbackup->backupFile = $newFileName;
            $dbbackup->createdByType = 'user';
            $dbbackup->createdBy = Auth::user()->email;
            $dbbackup->createdOn = Config::get('constants.CURRENTDATE');
            
            if($dbbackup->save())
            {
                return redirect()->back()->with('successMessage', 'Backup Created Successfully');
            }
            else
            {
                return redirect()->back()->with('errorMessage', 'Unable to create backup. Please try again later');
            }
            
        }
    }
    
    public function downloadbackup($id) {
        $fileData = \App\Model\Dbbackuplog::find($id);
        //$filepath = public_path('uploads/image/')."abc.jpg";
        $filepath = public_path('backups/'.$fileData->backupFile);
        return Response::download($filepath);
    }
    
    public function deletebackup($id) {
        
        $fileData = \App\Model\Dbbackuplog::find($id);
        $filepath = public_path('backups/'.$fileData->backupFile);
        if(file_exists($filepath))
        {
            unlink($filepath);
        }
        
        $fileData->delete();
        return redirect()->back()->with('successMessage', 'Backup File Deleted Successfully');
    }
    
    public function configurescheduler($id = 0,$page = 0, Request $request) {
        
        if (\Request::isMethod('post')) {
            $eventBy = $request->event_by;
            if($eventBy == "daily")
            {
                $eventOn = implode("_",$request->dailyval);
            }
            elseif($eventBy == "weekly")
            {
                $eventOn = $request->weeklyval;
            }
            elseif($eventBy == "monthly")
            {
                $eventOn = $request->monthlyval;
            }
            else
            {
                return redirect()->back()->with('errorMessage', 'Please select any of eveny by options');
            }
            
            \App\Model\Generalsettings::where('settingsKey', 'event_by')->update(['settingsValue'=>$eventBy]);
            \App\Model\Generalsettings::where('settingsKey', 'event_on')->update(['settingsValue'=>$eventOn]);
            
            return redirect()->back()->with('successMessage', 'Confirations Saved Successfully');
        }
        $schedularConfigValues = array();
        $eventBy = \App\Model\Generalsettings::where('settingsKey', 'event_by')->first();
        $eventOn = \App\Model\Generalsettings::where('settingsKey', 'event_on')->first();
        
        $data['eventBy'] = $eventBy->settingsValue;
        $data['eventOn'] = explode('_',$eventOn->settingsValue);
        return view('Administrator.dbbackup.configurescheduler',$data);
    }
}