<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Blockcontent;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use customhelper;

class BlockcontentController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
    }

    public function index(Request $request) {

        $data = array();

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Blockcontent'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $blockContentObj = new Blockcontent();
        $search = '';
        if(isset($request->searchDisplay))
        {
            $search = $request->searchDisplay;
        }

        $data['allTypes'] = Blockcontent::allBlockcontentType();
        $data['records'] = $blockContentObj->getData('-1',$search);
        $data['search'] = $search;
            
        $data['pageTitle'] = 'Block Content';
        $data['title'] = "Block Content :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText' => array('Home Page Content', 'Block Content'), 'contentTitle' => 'Block Content', 'pageInfo' => 'This section allows you to manage homepage block contents');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.content.blockcontent', $data);
    }

    public function geteditcontent($id = '-1',$slug = 'why_shoptomydoor') {

        $data = array();

        $blockContentObj = new Blockcontent();

        $data['slug'] = $slug;
        $data['record'] = $blockContentObj->getData($id);
        $data['pageTitle'] = 'Block Content';
        $data['title'] = "Block Content :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText' => array('Home Page Content', 'Block Content'), 'contentTitle' => 'Block Content', 'pageInfo' => 'This section allows you to manage homepage block contents');
        return view('Administrator.content.editblockcontent', $data);
    }

    public function updtecontent($id = '-1', Request $request) {

        $blockContentObj = new Blockcontent();
        if ($id != '-1')
            $blockContentObj = $blockContentObj->find($id);
        if ($request->hasFile('contentImage')) {
                $this->validate($request, [
                    'contentImage' => 'required|image|mimes:jpeg,png,jpg|max:2048',
                ]);
                
                $image = $request->file('contentImage');
                $name = time() . '_' . $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/blockcontent');
                if ($image->move($destinationPath, $name)) {
                    if(file_exists(public_path('/uploads/blockcontent/'.$blockContentObj->contentImage)))
                        unlink(public_path('/uploads/blockcontent/'.$blockContentObj->contentImage));
                    customhelper::bannerCrop('/uploads/blockcontent/', '/uploads/blockcontent/', $name, "243", "243");
                    $blockContentObj->contentImage = $name;
                }
        }

        $blockContentObj->contentTitle = $request->input('contentTitle');
        if($request->input('contentSubTitle') && $request->input('contentSubTitle')!='')
            $blockContentObj->contentSubTitle = $request->input('contentSubTitle');
        $blockContentObj->content = addslashes($request->input('content'));
        if ($blockContentObj->save())
            return redirect(url('/administrator/blockcontent'))->with('successMessage', 'Information saved successfuly.');
    }

    public function editstatus($id = '-1', $status) {

        $blockContentObj = new Blockcontent();
        if ($id != -1)
            $blockContentObj = $blockContentObj->find($id);
        $blockContentObj->status = $status;

        if ($blockContentObj->save())
            return json_encode(array('updated' => true));
        else
            return json_encode(array('updated' => false));
    }

    public function showaddform() {
        $data['pageTitle'] = 'Block Content';
        $data['title'] = "Block Content :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText' => array('Home Page Content', 'Block Content'), 'contentTitle' => 'Block Content', 'pageInfo' => 'This section allows you to manage homepage block contents');
        return view('Administrator.content.addblockcontent', $data);
    }

    public function addData(Request $request) {
        if (\Request::isMethod('post')) {

            $this->validate($request, [
                'contentImage' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ]);

            if ($request->hasFile('contentImage')) {
                $image = $request->file('contentImage');
                $name = time() . '_' . $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/blockcontent');
                if ($image->move($destinationPath, $name)) {
                    customhelper::bannerCrop('/uploads/blockcontent/', '/uploads/blockcontent/', $name, "243", "243");
                    $blockContent = new Blockcontent();

                    $blockContent->slug = 'why_shoptomydoor';
                    $blockContent->blockTitle = 'Why Shoptomydoor';
                    $blockContent->contentTitle = $request->input('contentTitle');
                    $blockContent->contentImage = $name;
                    $blockContent->content = htmlspecialchars($request->input('content'));
                    if($blockContent->save())
                        return redirect(url('/administrator/blockcontent'))->with('successMessage', 'Information saved successfuly.');
                }
            }
        }
    }
    
    public function deleteData($id) {
        
        $whyShoptomydoorDataCount = Blockcontent::where('slug','why_shoptomydoor')->count();
        if($whyShoptomydoorDataCount == 1)
            return redirect()->back()->with('errorMessage', 'There has to be atleast one content for this block');
        else {
            $contentData = Blockcontent::find($id);
            $filePath = public_path('/uploads/blockcontent/' . $contentData->contentImage);
            if(file_exists($filePath))
            {
                chmod($filePath, '0777');
                unlink(public_path('/uploads/blockcontent/' . $contentData->contentImage));
            }
            Blockcontent::where('id',$id)->delete();
            return redirect()->back()->with('successMessage', 'Record deleted successfully');
        }
    }

}
