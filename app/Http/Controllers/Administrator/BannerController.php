<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Banner;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use customhelper;

class BannerController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    /**
     * This function use to show homepage banners list
     * @param Request $request
     * @return type <HTML>
     */
    public function showHomepageBanner(Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Homepagebanner'), Auth::user()->id);
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $searchDisplay = $this->_perPage; //Initialize default record display count
        $sort_order = 'asc';
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage); // Initialize record display count if changes 
        }
        if ($request->input('sort_order') && $request->input('sort_order') == 'asc')
            $sort_order = 'desc';
        // Fetch homepage banner data
        $bannerRecords = Banner::where('type', '=', 1)->orderBy('displayOrder', $sort_order)->paginate($searchDisplay);
        $data['pageTitle'] = 'Homepage Banner';
        $data['sort_order'] = $sort_order;
        $data['searchDisplay'] = $searchDisplay;
        $data['page'] = $bannerRecords->currentPage();
        $data['bannerRecords'] = $bannerRecords;
        $data['title'] = "Homepage Banners :: ADMIN - Shoptomydoor";
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        $data['contentTop'] = array('breadcrumbText' => 'Banner', 'contentTitle' => 'Homepage Banner', 'pageInfo' => 'This section allows you to manage homepage banners');
        return view('Administrator.content.banner', $data);
    }

    /**
     * This function use to show registration banners list
     * @param Request $request
     * @return type <HTML>
     */
    public function showRegistrationBanner(Request $request) {
        $data = array();

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Registrationbanner'), Auth::user()->id);
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }


        $searchDisplay = $this->_perPage; //Initialize default record display count
        $sort_order = 'asc';
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage); // Initialize record display count if changes
        }
        if ($request->input('sort_order') && $request->input('sort_order') == 'asc')
            $sort_order = 'desc';
        // Fetch registration banner data
        $bannerRecords = Banner::where('type', '=', 2)->orderBy('displayOrder', $sort_order)->paginate($searchDisplay);
        $data['page'] = $bannerRecords->currentPage();
        $data['sort_order'] = $sort_order;
        $data['frontEndUrl'] = Config::get('constants.frontendUrl');
        $data['pageTitle'] = 'Registration Banner';
        $data['searchDisplay'] = $searchDisplay;
        $data['bannerRecords'] = $bannerRecords;
        $data['title'] = "Registration Banners :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText' => 'Banner', 'contentTitle' => 'Registration Banner', 'pageInfo' => 'This section allows you to manage registration banners');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.content.registrationBanner', $data);
    }

    /**
     * This function use to show warehouse banners list
     * @param Request $request
     * @return type <HTML>
     */
    public function showWarehouseBanner(Request $request) {

        $data = array();

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Warehousebanner'), Auth::user()->id);
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }


        $searchDisplay = $this->_perPage; //Initialize default record display count
        $sort_order = 'asc';
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage); // Initialize record display count if changes
        }
        if ($request->input('sort_order') && $request->input('sort_order') == 'asc')
            $sort_order = 'desc';
        // Fetch warehouse banner data
        $bannerRecords = Banner::where('type', '=', 3)->orderBy('displayOrder', $sort_order)->paginate($searchDisplay);
        $data['pageTitle'] = 'Warehouse Banner';
        $data['sort_order'] = $sort_order;
        $data['searchDisplay'] = $searchDisplay;
        $data['page'] = $bannerRecords->currentPage();
        $data['bannerRecords'] = $bannerRecords;
        $data['title'] = "Warehouse Banners :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText' => 'Banner', 'contentTitle' => 'Warehouse Banner', 'pageInfo' => 'This section allows you to manage customer warehouse banners');
        
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.content.warehouseBanner', $data);
    }

    /**
     * This function use to show Homepage ad banner list
     * @param Request $request
     * @return type <HTML>
     */
    public function showadbanner(Request $request) {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Adbanner'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $searchDisplay = $this->_perPage; //Initialize default record display count
        $sort_order = 'asc';
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage); // Initialize record display count if changes
        }
        if ($request->input('sort_order') && $request->input('sort_order') == 'asc')
            $sort_order = 'desc';
        // Fetch warehouse banner data
        $bannerRecords = Banner::where('type', '=', 7)->orderBy('displayOrder', $sort_order)->paginate($searchDisplay);
        $data['pageTitle'] = 'Homepage Ad Banner';
        $data['sort_order'] = $sort_order;
        $data['searchDisplay'] = $searchDisplay;
        $data['page'] = $bannerRecords->currentPage();
        $data['bannerRecords'] = $bannerRecords;
        $data['title'] = "Homepage Ad Banners :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText' => 'Banner', 'contentTitle' => 'Homepage Ad Banner', 'pageInfo' => 'This section allows you to manage homepage ad banner');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.content.homepageadbanner', $data);
    }

    /**
     * This function use to show auto banners list
     * @param Request $request
     * @return type <HTML>
     */
    public function showAutoBanner(Request $request) {
        $data = array();
        $searchDisplay = $this->_perPage; //Initialize default record display count
        $sort_order = 'asc';
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage); // Initialize record display count if changes
        }
        if ($request->input('sort_order') && $request->input('sort_order') == 'asc')
            $sort_order = 'desc';
        // Fetch auto banner data
        $bannerRecords = Banner::where('type', '=', 6)->orderBy('displayOrder', $sort_order)->paginate($searchDisplay);
        $data['pageTitle'] = 'Auto Banner';
        $data['sort_order'] = $sort_order;
        $data['searchDisplay'] = $searchDisplay;
        $data['page'] = $bannerRecords->currentPage();
        $data['bannerRecords'] = $bannerRecords;
        $data['title'] = "Auto Banners :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText' => 'Banner', 'contentTitle' => 'Auto Banner', 'pageInfo' => 'This section allows you to manage customer auto banners');
        return view('Administrator.content.autoBanner', $data);
    }

    /**
     * This function use to show shopforme banners list
     * @param Request $request
     * @return type <HTML>
     */
    public function showShopformeBanner(Request $request) {

        $data = array();
        $searchDisplay = $this->_perPage; //Initialize default record display count
        $sort_order = 'asc';
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage); // Initialize record display count if changes
        }
        if ($request->input('sort_order') && $request->input('sort_order') == 'asc')
            $sort_order = 'desc';
        // Fetch shop for me banner data
        $bannerRecords = Banner::where('type', '=', 5)->orderBy('displayOrder', $sort_order)->paginate($searchDisplay);
        $data['pageTitle'] = 'Shop For Me Banner';
        $data['sort_order'] = $sort_order;
        $data['searchDisplay'] = $searchDisplay;
        $data['page'] = $bannerRecords->currentPage();
        $data['bannerRecords'] = $bannerRecords;
        $data['title'] = "Shop For Me Banners :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText' => 'Banner', 'contentTitle' => 'Shop For Me Banner', 'pageInfo' => 'This section allows you to manage customer shop for me banners');
        return view('Administrator.content.shopformeBanner', $data);
    }

    /**
     * This function use to show Fill and ship banners list
     * @param Request $request
     * @return type <HTML>
     */
    public function showFillnshipBanner(Request $request) {

        $data = array();
        $searchDisplay = $this->_perPage; //Initialize default record display count
        $sort_order = 'asc';
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage); // Initialize record display count if changes
        }
        if ($request->input('sort_order') && $request->input('sort_order') == 'asc')
            $sort_order = 'desc';
        // Fetch fillandship banner data
        $bannerRecords = Banner::where('type', '=', 4)->orderBy('displayOrder', $sort_order)->paginate($searchDisplay);
        $data['pageTitle'] = 'Fill & Ship Banner';
        $data['sort_order'] = $sort_order;
        $data['searchDisplay'] = $searchDisplay;
        $data['page'] = $bannerRecords->currentPage();
        $data['bannerRecords'] = $bannerRecords;
        $data['title'] = "Fill & Ship Banners :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText' => 'Banner', 'contentTitle' => 'Fill & Ship Banner', 'pageInfo' => 'This section allows you to manage customer fillnship banners');
        return view('Administrator.content.fillnshipBanner', $data);
    }

    /**
     * This function use to show Fill and ship banners list
     * @param Request $request
     * @return type <HTML>
     */
    public function showSpecialofferBanner(Request $request) {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Specialofferbanner'), Auth::user()->id);
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $searchDisplay = $this->_perPage; //Initialize default record display count
        $sort_order = 'asc';
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage); // Initialize record display count if changes
        }
        if ($request->input('sort_order') && $request->input('sort_order') == 'asc')
            $sort_order = 'desc';
        // Fetch specialoffer banner data
        $bannerRecords = Banner::where('type', '=', 8)->orderBy('displayOrder', $sort_order)->paginate($searchDisplay);
        $data['pageTitle'] = 'Special Offers Banner';
        $data['sort_order'] = $sort_order;
        $data['searchDisplay'] = $searchDisplay;
        $data['page'] = $bannerRecords->currentPage();
        $data['bannerRecords'] = $bannerRecords;
        $data['title'] = "Special Offers Banners :: ADMIN - Shoptomydoor";
        $data['blockcontent'] = \App\Model\Blockcontent::where('slug', 'special_offers')->first();
        $data['contentTop'] = array('breadcrumbText' => 'Banner', 'contentTitle' => 'Special Offers Banner', 'pageInfo' => 'This section allows you to manage special offer banners');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.content.specialofferBanner', $data);
    }

    /**
     * This function is used to show modal to add or update homepage banner data
     * @param type <int> $id
     * @param type <int> $page
     * @return type <HTML>
     */
    public function addedithomepagebanner($id = -1, $page = 1) {
        $data = array();

        $data['title'] = "Homepage Banners :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText' => 'Banner', 'contentTitle' => 'Homepage Banner', 'pageInfo' => 'This section allows you to manage homepage banners');
        $data['page'] = $page;
        // Check if banner record exist of not
        if ($id != -1) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $bannerRecord = Banner::find($id); // Fetch banner data for edit
            $data['bannerRecord'] = $bannerRecord;
        } else {
            $data['action'] = 'Add';
            $data['bannerRecord'] = new Banner;
        }

        return view('Administrator.content.addedithomepagebanner', $data);
    }

    /**
     * This function used to generate banner record list
     * @param type <int> $id
     * @return type <JSON>
     */
    public function getBannerInfo($id) {

        $bannerRecord = Banner::find($id);
        return json_encode($bannerRecord->getAttributes());
    }

    /**
     * This function getting used to add new banner record
     * @param type $type
     * @param type $page
     * @param Request $request
     * @return type <HTML>
     */
    public function addBanner($type = 2, $page = 1, Request $request) {
        $banner = new Banner();
        // Validation rules
        if ($type == '7') {
            $this->validate($request, [

            'bannerImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:min_width=350,min_height=56,max_width=350,max_height=56',
                
                
            ]);
        } else if ($type == '8') {
            $this->validate($request, [

            'bannerImage' => 'required|image|mimes:jpeg,png,jpg|max:2048|dimensions:min_width=350,min_height=220',
                
            ]);
        } else {
            $this->validate($request, [

            'bannerImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:min_width=1600,min_height=626',
                
            ]);
        }
        $validator = Validator::make(\Input::all(), [
                    'displayOrder' => 'required|integer',
                    
        ]);
        // If validation fails redirect to list page with errors
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if ($type == 8) {
                $count = Banner::where('type', $type)->where('status', '1')->count();
                if ($count >= 3)
                    return redirect()->back()->with('errorMessage', 'Maximum of 3 banners can be added under Special Offers.');
            }

            if($type == 1){
                /* Upload banner image */
                if ($request->hasFile('bannerImage')) {
                    $image = $request->file('bannerImage');
                    $name = time() . '_' . $image->getClientOriginalName();
                    $name = preg_replace('/\s+/', '_', $name);
                    $destinationPath = public_path('/uploads/banner');
                    $image->move($destinationPath, $name);
                    customhelper::bannerCrop('/uploads/banner/', '/uploads/banner/', $name, "1600", "626");
                }
            } else if($type == 8) {
                if ($request->hasFile('bannerImage')) {
                    $image = $request->file('bannerImage');
                    $name = time() . '_' . $image->getClientOriginalName();
                    $name = preg_replace('/\s+/', '_', $name);
                    $destinationPath = public_path('/uploads/banner');
                    $image->move($destinationPath, $name);

                    customhelper::bannerCrop('/uploads/banner/', '/uploads/banner/', $name, "350", "220");
                }

                /*if ($im2 !== FALSE) {
                    imagepng($im2, 'example-cropped.png');
                    imagedestroy($im2);
                }*/
            } else {
                /* Upload banner image */
                if ($request->hasFile('bannerImage')) {
                    $image = $request->file('bannerImage');
                    $name = time() . '_' . $image->getClientOriginalName();
                    $name = preg_replace('/\s+/', '_', $name);
                    $destinationPath = public_path('/uploads/banner');
                    $image->move($destinationPath, $name);
                }
            }

            $banner->type = $type;
            // For homepage banner type can be image/video, for others it always will be image
            if ($type == 2 || $type == 3 || $type == 4 || $type == 5 || $type == 6 || $type == 7 || $type == 8)
                $banner->bannerType = 'image';
            else
                $banner->bannerType = $request->input('bannerType');
            if ($type == 2) {
                $isDefaultSet = Banner::where('isDefault', '1')->get();
                if (count($isDefaultSet) == 0) {
                    $banner->isDefault = '1';
                }
            }
            $banner->imagePath = $name;
            if ($request->input('bannerContent') && $request->input('bannerContent') != '')
                $banner->bannerContent = addslashes($request->input('bannerContent'));
            if ($request->input('videoPath') && $request->input('videoPath') != '')
                $banner->videoPath = $request->input('videoPath');

            $banner->displayOrder = $request->input('displayOrder');
            $banner->slug = $request->input('slug');
            $banner->status = '1'; // status is active when add new banner
            $banner->pageLink = $request->input('warehousePageLink');

            // Redirection condition as there there are different actions for different banners 
            $redirect_url = url('/') . '/administrator/homepagebanner?page=' . $page;
            if ($type == 2)
                $redirect_url = url('/') . '/administrator/registrationbanner?page=' . $page;
            else if ($type == 3)
                $redirect_url = url('/') . '/administrator/warehousebanner?page=' . $page;
            else if ($type == 4)
                $redirect_url = url('/') . '/administrator/fillnshipbanner?page=' . $page;
            else if ($type == 5)
                $redirect_url = url('/') . '/administrator/shopformebanner?page=' . $page;
            else if ($type == 6)
                $redirect_url = url('/') . '/administrator/autobanner?page=' . $page;
            else if ($type == 7)
                $redirect_url = url('/') . '/administrator/adbanner?page=' . $page;
            else if ($type == 8)
                $redirect_url = url('/') . '/administrator/specialofferbanner?page=' . $page;
            if ($banner->save())
                return redirect($redirect_url)->with('successMessage', 'Information saved successfuly.');
        }
    }

    /**
     * This function used to update banner records
     * @param type $type
     * @param type $id
     * @param type $page
     * @param Request $request
     * @return type <HTML>
     */
    public function editBanner($type = 2, $id = -1, $page = 1, Request $request) {

        $banner = new Banner();
        if ($id != -1)
            $banner = $banner->find($id); // Initialize object with old banner records
            
// Validation rules
        $validator = Validator::make(\Input::all(), [
                    'displayOrder' => 'required|integer',
                    // 'warehousePageLink' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
                        //'bannerImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:min_width=1600,min_height=626',
        ]);

        // Redirect to  list page with errors if validation fails
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

            if ($request->hasFile('bannerImage')) {
                if ($type == '7') {
                    $this->validate($request, [
                        'bannerImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:min_width=350,min_height=56,max_width=350,max_height=56',
                    ]);
                } else if ($type == '8') {
                    $this->validate($request, [

                        'bannerImage' => 'required|image|mimes:jpeg,png,jpg|max:2048|dimensions:min_width=350,min_height=220',
                    ]);
                } else {
                    $this->validate($request, [
                        'bannerImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:min_width=1600,min_height=626',
                        'bannerImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:min_width=1600,min_height=626',
                    ]);
                }

                if($type == 1){
                    $image = $request->file('bannerImage');
                    $name = time() . '_' . $image->getClientOriginalName();
                    $name = preg_replace('/\s+/', '_', $name);
                    $destinationPath = public_path('/uploads/banner');
                    $image->move($destinationPath, $name);
                    if (file_exists(public_path('/uploads/banner/' . $banner->imagePath)))
                        unlink(public_path('/uploads/banner/' . $banner->imagePath));
                    customhelper::bannerCrop('/uploads/banner/', '/uploads/banner/', $name, "1600", "626");
                    $banner->imagePath = $name;
                } else if($type == 8){
                    //
                    if ($request->hasFile('bannerImage')) {
                    $image = $request->file('bannerImage');
                    $name = time() . '_' . $image->getClientOriginalName();
                    $name = preg_replace('/\s+/', '_', $name);
                    $destinationPath = public_path('/uploads/banner');
                    $image->move($destinationPath, $name);
                    customhelper::bannerCrop('/uploads/banner/', '/uploads/banner/', $name, "350", "220");
                    $banner->imagePath = $name;
                    }
                } else {
                    $image = $request->file('bannerImage');
                    $name = time() . '_' . $image->getClientOriginalName();
                    $name = preg_replace('/\s+/', '_', $name);
                    $destinationPath = public_path('/uploads/banner');
                    $image->move($destinationPath, $name);
                    if (file_exists(public_path('/uploads/banner/' . $banner->imagePath)))
                        unlink(public_path('/uploads/banner/' . $banner->imagePath));
                    $banner->imagePath = $name;
                }
            }
            if ($type == 1) {
                $banner->bannerType = $request->input('bannerType');
                if ($request->input('bannerType') == 'image')
                    $banner->videoPath = '';
                if ($request->input('videoPath') && $request->input('videoPath') != '')
                    $banner->videoPath = $request->input('videoPath');
                if ($request->input('bannerContent') && $request->input('bannerContent') != '')
                    $banner->bannerContent = addslashes($request->input('bannerContent'));
                else
                    $banner->bannerContent = '';
            }

            $banner->displayOrder = $request->input('displayOrder');
            $banner->slug = $request->input('slug');
            $banner->pageLink = $request->input('warehousePageLink');

            if ($type == 2) {
                $isDefaultSet = Banner::where('isDefault', '1')->get();
                if (count($isDefaultSet) == 0) {
                    $banner->isDefault = '1';
                }
            }
            // Redirection condition as there there are different actions for different banners 
            $redirect_url = url('/') . '/administrator/homepagebanner?page=' . $page;
            if ($type == 2)
                $redirect_url = url('/') . '/administrator/registrationbanner?page=' . $page;
            else if ($type == 3)
                $redirect_url = url('/') . '/administrator/warehousebanner?page=' . $page;
            else if ($type == 4)
                $redirect_url = url('/') . '/administrator/fillnshipbanner?page=' . $page;
            else if ($type == 5)
                $redirect_url = url('/') . '/administrator/shopformebanner?page=' . $page;
            else if ($type == 6)
                $redirect_url = url('/') . '/administrator/autobanner?page=' . $page;
            else if ($type == 7)
                $redirect_url = url('/') . '/administrator/adbanner?page=' . $page;
            else if ($type == 8)
                $redirect_url = url('/') . '/administrator/specialofferbanner?page=' . $page;
            if ($banner->save())
                return redirect($redirect_url);
        }
    }

    /**
     * This function used to update status of banner records
     * @param type $id
     * @param type $status
     * @return type <JSON>
     */
    public function editBannerStatus($id = -1, $status) {


        $banner = new Banner();
        if ($id != -1)
            $banner = $banner->find($id); // Initialize object with old banner record
        $banner->status = $status; // Set new status value
        // Update
        if ($banner->save())
            return json_encode(array('updated' => true));
        else
            return json_encode(array('updated' => false));
    }

    /**
     * This function used to delete banner records individually
     * @param type $id
     * @param type $type
     * @param type $page
     * @return type <HTML>
     */
    public function bannerDelete($id = -1, $type = 2, $page = 1) {

        $banner = new Banner();
        if ($id != -1)
            $banner = $banner->find($id); // Initialize object with old banner record






            
// Remove banner images
        if (file_exists(public_path('/uploads/banner/' . $banner->imagePath)))
            unlink(public_path('/uploads/banner/' . $banner->imagePath));
        $banner->delete(); //Delete banner record from table
        // Redirection condition as there there are different actions for different banners 
        $redirect_url = url('/') . '/administrator/homepagebanner?page=' . $page;
        if ($type == 2)
            $redirect_url = url('/') . '/administrator/registrationbanner?page=' . $page;
        else if ($type == 3)
            $redirect_url = url('/') . '/administrator/warehousebanner?page=' . $page;
        else if ($type == 4)
            $redirect_url = url('/') . '/administrator/fillnshipbanner?page=' . $page;
        else if ($type == 5)
            $redirect_url = url('/') . '/administrator/shopformebanner?page=' . $page;
        else if ($type == 6)
            $redirect_url = url('/') . '/administrator/autobanner?page=' . $page;
        else if ($type == 7)
            $redirect_url = url('/') . '/administrator/adbanner?page=' . $page;
        else if ($type == 8)
            $redirect_url = url('/') . '/administrator/specialofferbanner?page=' . $page;
        return redirect($redirect_url)->with('successMessage', 'Information deleted successfuly.');
    }

    /**
     * This function used to delete multiple banner records at a time
     * @param type $type
     * @param Request $request
     * @return type <HTML>
     */
    public function bannerdeletemultiple($type = 2, Request $request) {

        $idsToDelete = $request->input('deleteData'); //Initialize ids to delete
        // Loop through all ids
        foreach ($idsToDelete as $eachId) {
            $banner = new Banner();
            $banner = $banner->find($eachId); // Initialize object with old banner record
            // Remove banner images
            if (file_exists(public_path('/uploads/banner/' . $banner->imagePath)))
                unlink(public_path('/uploads/banner/' . $banner->imagePath));
            $banner->delete();
        }

        // Redirection condition as there there are different actions for different banners     
        $redirect_url = url('/') . '/administrator/homepagebanner';
        if ($type == 2)
            $redirect_url = url('/') . '/administrator/registrationbanner';
        else if ($type == 3)
            $redirect_url = url('/') . '/administrator/warehousebanner';
        else if ($type == 4)
            $redirect_url = url('/') . '/administrator/fillnshipbanner';
        else if ($type == 5)
            $redirect_url = url('/') . '/administrator/shopformebanner';
        else if ($type == 6)
            $redirect_url = url('/') . '/administrator/autobanner';
        else if ($type == 7)
            $redirect_url = url('/') . '/administrator/adbanner';
        else if ($type == 8)
            $redirect_url = url('/') . '/administrator/specialofferbanner';
        return redirect($redirect_url)->with('successMessage', 'Information deleted successfuly.');
    }

    public function editbannerdefaultstatus($id = '-1', $page, $type = '2') {
        if ($id != '-1') {
            Banner::where('type', $type)->update(['isDefault' => 0]);
            Banner::where('id', $id)->update(['isDefault' => 1]);
        }
        if ($type == '2')

        //$redirect_url = url('/') . '/administrator/registrationbanner?page='.$page;
            $redirect_url = url('/') . '/administrator/registrationbanner?page=' . $page;
        else
            $redirect_url = url('/') . '/administrator/adbanner?page=' . $page;

        return redirect($redirect_url)->with('successMessage', 'Banner set to default successfuly.');
    }

}
