<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\libraries\dbHelpers;
use Auth;
use App\Model\User;
use App\Model\Fundpoint;
use App\Model\Fundpointtransaction;
use Hash;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Routing\Route;
use Config;
use customhelper;

class FundUserPointController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
    }

    public function index() {
        $data = array();

        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.FundPoint'), Auth::user()->id);

        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Fund My Point :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Fund My Point";
        $data['contentTop'] = array('breadcrumbText' => 'Fund My Point', 'contentTitle' => 'Fund My Point', 'pageInfo' => 'This section allows you to fund point');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers', 'menuSub' => '', 'menuSubSub' => 'leftNavFundUserPoint4');
        return view('Administrator.fundpoint.index', $data);
    }

    /**
     * Method for get the information
     * @return array
     */
    public function store(Request $request) {
        $data = array();
        $data['unit'] = $request->unit;
        $data['email'] = $request->email;
        /*  FIND FUND POINT INFO EITHER BY UNIT NUMBER OR BY EMAIL  */
        if ($request->unit != '') {
            $customerRecord = User::where('unit', $request->unit)->get();
            if (count($customerRecord) == 0) {
                $customerRecord = User::where('email', $request->email)->get();
            }
        } else {
            $customerRecord = User::where('email', $request->email)->get();
        }

        /*  REDIRECT IF NOTHING ENTERED  */
        if ($request->unit == '' && $request->email == '') {
            return \Redirect::to('administrator/fundpoint/')->with('errorMessage', 'Please enter any one of the below field');
        }

        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.FundPoint'), Auth::user()->id); // call the helper function

        /* SET DATA FOR VIEW  */
        $data['title'] = "Fund My Point :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Fund My Point";
        $data['contentTop'] = array('breadcrumbText' => 'Fund My Point', 'contentTitle' => 'Fund My Point', 'pageInfo' => 'This section allows you to fund Point');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers', 'menuSub' => '', 'menuSubSub' => 'leftNavFundUserPoint4');

        /* SET EXPIRY DATE */
        $date = date('m-d-Y');
        $date1 = str_replace('-', '/', $date);

        $getGeneralSettings = \App\Model\Generalsettings::where('settingsKey', 'point_expiry_period')->first();
        $oneMonthAfter = date('m/d/Y', strtotime($date1 . "+ " . $getGeneralSettings->settingsValue . "days"));
        $data['oneMonthAfter'] = $oneMonthAfter;


        if (!empty($customerRecord)) {
            /* NO FUND POINT RECORD FOUND  */
            if (!isset($customerRecord[0])) {
                return \Redirect::to('administrator/fundpoint/')->with('errorMessage', 'No record found!');
            }
            if (isset($customerRecord[0])) {
                $data['customerRecord'] = $customerRecord;
                /* FIND FUND POINT INFO  */
                $point = Fundpoint::where('userId', $customerRecord[0]->id)->get();
                if (count($point) == 0) {
                    /* IF NO FUND POINT, THEN ADD A NEW FUND POINT WITH 0 FOR THE USER  */
                    $fp = new Fundpoint;
                    $fp->point = 0;
                    $fp->userId = $customerRecord[0]->id;
                    $fp->save();
                    $point = Fundpoint::where('userid', $customerRecord[0]->id)->get();

                    $fpt = new Fundpointtransaction;
                    $fpt->point = 0;
                    $fpt->userId = $customerRecord[0]->id;
                    $fpt->reason = Config::get('constants.Points.REGISTRATION');
                    $fpt->senderId = Auth::user()->id;
                    $fpt->date = Config::get('constants.CURRENTDATE');
                    $fpt->type = 'A';
                    $fpt->save();
                }

                $data['point'] = $point;
            }
        }
        return view('Administrator.fundpoint.index', $data);
    }

    /**
     * Method for show fund point info
     * @param integer $id
     * @return array
     */
    public function show($id, Request $request) {
        /*  SET SESSION VALUE  */
        $unit = \Session::get('FUNDPOINT.unit');
        $email = \Session::get('FUNDPOINT.email');

        $data = array();
        /*  FIND FUND POINT INFO EITHER BY UNIT NUMBER OR BY EMAIL  */
        $data['unit'] = $unit[0];
        $data['email'] = $email[0];
        if ($data['unit'] != '') {
            $customerRecord = User::where('uniqueId', $data['unit'])->get();
        } else {
            $customerRecord = User::where('email', $email[0])->get();
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Fund My Point :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Fund My Point";
        $data['contentTop'] = array('breadcrumbText' => 'Fund My Point', 'contentTitle' => 'Fund My Point', 'pageInfo' => 'This section allows you to fund point');


        if (!empty($customerRecord)) {
            if (!isset($customerRecord[0])) {
                // DO NOTHING
            }
            if (isset($customerRecord[0])) {
                $data['customerRecord'] = $customerRecord;
                /* GET THE FUND POINT  */
                $point = Fundpoint::where('userid', $customerRecord[0]->id)->get();
                $data['point'] = $point;
            }
        }

        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.FundPoint'), Auth::user()->id); // call the helper function
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavUsers', 'menuSub' => '', 'menuSubSub' => 'leftNavFundUserPoint4');
        /* SET EXPIRY DATE */
        $date = date('m-d-Y');
        $date1 = str_replace('-', '/', $date);
        $oneMonthAfter = date('m/d/Y', strtotime($date1 . "+30 days"));
        $data['oneMonthAfter'] = $oneMonthAfter;

        return view('Administrator.fundpoint.index', $data);
    }

    /**
     * Method for update the fund point
     * @param integer $id
     * @return array
     */
    public function update(Request $request, $id) {

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.FundPoint'), Auth::user()->id);

        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $fundpoint = new Fundpoint;

        $data = array();

        $data['unit'] = $request->unit;
        $data['email'] = $request->email;
        \Session::forget('FUNDPOINT');
        \Session::push('FUNDPOINT.unit', $request->unit);
        \Session::push('FUNDPOINT.email', $request->email);

        /*  GET INFO EITHER BY UNIT NUMBER OR BY EMAIL  */
        if ($request->unit != '') {
            $customerRecord = User::where('unit', $request->unit)->get();
        } else {
            $customerRecord = User::where('email', $request->email)->get();
        }

        /*  REDIRECT IF NOTHING ENTERED  */
        if ($request->unit == '' && $request->email == '') {
            return \Redirect::to('administrator/fundpoint/')->with('errorMessage', 'Please enter any one of the below field');
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Fund My Point :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Fund My Point";
        $data['contentTop'] = array('breadcrumbText' => 'Fund My Point', 'contentTitle' => 'Fund My Point', 'pageInfo' => 'This section allows you to fund point');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        if (!empty($customerRecord)) {
            if (!isset($customerRecord[0])) {
                
            }
            if (isset($customerRecord[0])) {
                $data['customerRecord'] = $customerRecord;
                /* GET THE FUND POINT  */
                $point = Fundpoint::where('userid', $customerRecord[0]->id)->get();
                $data['point'] = $point;
            }
        }

        if (is_numeric($request->point)) {

            $formattedExpiredDate = $request->expiredDate;
            $formattedExpiredDate = date("Y-m-d", strtotime($formattedExpiredDate));

            /* UPDATE THE FUND POINT  */
            $newPoint = $request->point + $request->existing_point;
            $fundpoint = Fundpoint::where('userId', $id)->update(['point' => $newPoint]);

            $fpt = new Fundpointtransaction;
            $fpt->point = $request->point;
            $fpt->userId = $customerRecord[0]->id;
            $fpt->reason = Config::get('constants.Points.ADDEDBYADMIN');
            $fpt->senderId = Auth::user()->id;
            $fpt->date = Config::get('constants.CURRENTDATE');
            $fpt->type = 'A';
            $fpt->expiredDate = $formattedExpiredDate;
            $fpt->save();

            $data['user'] = User::find($id);

            /* Save & Send Notification */
            User::sendPushNotification($data['user']->id, 'user_point', Auth::user()->id);

            /* ++++++++++ email functionality ++++++++ */
            $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'user_point')->first();
            $replace['[NAME]'] = $data['user']->firstName . " " . $data['user']->lastName;
            $replace['[POINT]'] = html_entity_decode($request->point);
            $to = $data['user']->email;
            // $mail = customhelper::SendMail($emailTemplate, $replace, $to);
            /* ++++++++++ end of email functionality ++++ */

            $mailTemplatevars = Config::get('constants.emailTemplateVariables');

            $arrayvalues = [];
            foreach ($mailTemplatevars as $key => $val) {
                if (isset($replace[$val]) == $val) {
                    $arrayvalues[] = $replace[$val];
                } else {
                    $arrayvalues[] = '';
                }
            }

            $content = str_replace($mailTemplatevars, $arrayvalues, stripslashes($emailTemplate->templateBody));

            /* INSERT DATA INTO NOTIFICATION TABLE */
            customhelper::insertUserNotification($customerRecord[0]->id, Auth::user()->id, $content, $type = 2, $emailTemplate->templateSubject);

            $data['SuccessBalance'] = 'Point successfully added';
            return \Redirect::to('administrator/fundpoint/' . $id)->with('successMessage', 'Point successfully added');
        }

        /* SET EXPIRY DATE */
        $date = date('m-d-Y');
        $date1 = str_replace('-', '/', $date);
        $oneMonthAfter = date('m/d/Y', strtotime($date1 . "+30 days"));
        $data['oneMonthAfter'] = $oneMonthAfter;

        return view('Administrator.fundpoint.index', $data);
    }

}
