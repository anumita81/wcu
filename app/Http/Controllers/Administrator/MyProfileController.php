<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserAdmin;
//use App\libraries\helpers;
//use App\libraries\dbHelpers;
use Auth;
use App\Model\User;
use Hash;
use Illuminate\Contracts\Auth\Authenticatable;

class MyProfileController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
    }

    
    public function index() {
        $data = array();

        /* GET DATA  */
        $userRecord = UserAdmin::where('id', '=', Auth::user()->id)->get();

        /* SET DATA FOR VIEW  */
        $data['userRecord'] = $userRecord;
        $data['title'] = "Profile :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Profile";
        $data['contentTop'] = array('breadcrumbText'=>'Profile','contentTitle'=>'Profile','pageInfo'=>'');
        return view('Administrator.profile.profile',$data);

    }

    /**
     * Method used to show data
     * @param integer $id
     * @return object
     */
    public function show($id)
    {
        $data = array();

        /* GET DATA  */
        $userRecord = UserAdmin::where('id', '=', Auth::user()->id)->get();

        /* SET DATA FOR VIEW  */
        $data['userRecord'] = $userRecord;
        $data['title'] = "Profile :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Profile";
        $data['contentTop'] = array('breadcrumbText'=>'Profile','contentTitle'=>'Profile','pageInfo'=>'');
        return view('Administrator.profile.profile',$data);
    }

    /**
     * Method used to update data
     * @param integer $id
     * @param string $type
     * @return object
     */
    public function update(Request $request, $id=1, $type = 'content'){
        if($request->get('type') == 'content'){
            /* VALIDATION */
            $data = $request->validate([
                'firstName' => 'required|min:3|max:35',
                'lastName' => 'required|min:3|max:35',
                'company' => 'required|min:3|max:100',
                'website' => 'required|min:3|max:100'
            ]);
            $UserAdmin = UserAdmin::findOrFail(Auth::user()->id);
            /* IF TYPE IS CONTENT THEN UPDATE THE PROFILE INFO WITHOUT PASSWORD  */
            $UserAdmin->update($data);
            return redirect()->back()->with('success', 'All settings has been submitted successfully');
        }
        else {
            /* VALIDATION */
            $validator = Validator::make($request->all(), [
            'old_password'     => 'required',
            'new_password'     => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
            ]);

            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()->all()]);
            }    
            else { 
                $data = $request->all();
         
                $user = UserAdmin::find(Auth::user()->id);
                
                /* HASHING THE OLD PASSWORD */
                $checkResult = Hash::check($data['old_password'], $user->password);
                if(!$checkResult || empty($checkResult)){
                    /* ERROR AS USER ENTER OLD PASSWORD WRONGLY */
                    return response()->json(array(
                    'error'   =>  array(0 =>'The specified password does not match the old password')
                ));
                }else{
                    /* UPDATE PASSWORD */
                    $credentials['email'] = auth()->user()->email;
                    $credentials['password'] = $data['confirm_password'];
                    $user = auth()->user();
                    $user->password = Hash::make($credentials['password']);
                    $user->save();
                    return response()->json(['success'=>'Password has changed successfuly.']);
                }
            }
        }
    }



}
