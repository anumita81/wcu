<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\County;
use App\Model\State;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class CountyController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request, $stateId) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.State'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('STATEDATA');
            \Session::push('STATEDATA.searchDisplay', $searchDisplay);
            \Session::push('STATEDATA.field', $field);
            \Session::push('STATEDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('STATEDATA.field');
            $sortType = \Session::get('STATEDATA.type');
            $searchDisplay = \Session::get('STATEDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'name';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        $param['searchByState'] = $stateId;

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTY LIST  */
        $countyData = County::getCountyList($param);

        /* SET DATA FOR VIEW  */
        $data['stateId'] = $stateId;
        $stateName = State::where('id', $stateId)->first()->name;
        $data['title'] = "Administrative Panel :: Counties";
        $data['contentTop'] = array('breadcrumbText' => 'Counties', 'contentTitle' => 'Counties', 'pageInfo' => 'This section allows you to counties');
        $data['pageTitle'] = "Counties - " . $stateName;
        $data['page'] = $countyData->currentPage();
        $data['countyData'] = $countyData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.county.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function addedit($stateId, $id = '0', $page = '') {
        $data = array();
        $data['page'] = !empty($page) ? $page : '1';
        $data['stateId'] = $stateId;
        $stateName = State::where('id', $stateId)->first()->name;
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $county = County::find($id);
            $data['county'] = $county;
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['county'] = array();
        }

        $data['pageTitle'] = $data['action'] . " County - " . $stateName;

        return view('Administrator.county.addedit', $data);
    }

    /*     * Edit
     * Method used to save content information
     * @param integer $id
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $county = new County;

        $validator = Validator::make($request->all(), [
                    'name' => 'required|regex:/^[\pL\s\-]+$/u',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $county = County::find($id);
                $county->modifiedBy = Auth::user()->id;
                $county->modifiedOn = Config::get('constants.CURRENTDATE');
            } else {
                $county->stateId = $request->stateId;
                $county->createdBy = Auth::user()->id;
                $county->createdOn = Config::get('constants.CURRENTDATE');
            }
            $county->name = $request->name;
            $county->save();
            $countyId = $county->id;

            $stateId = $request->stateId;

            return redirect('/administrator/county/' . $stateId)->with('successMessage', 'County information saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function changestatus($stateId, $id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (County::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to("administrator/county/$stateId?page=" . $page)->with('successMessage', 'County status changed successfully.');
            } else {
                return \Redirect::to("administrator/county/$stateId?page=" . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to("administrator/county/$stateId?page=" . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($stateId, $id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (County::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to("administrator/county/$stateId?page=" . $page)->with('successMessage', 'County deleted successfully.');
            } else {
                return \Redirect::to("administrator/county/$stateId?page=" . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to("administrator/county/$stateId?page=" . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
