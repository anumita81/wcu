<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Shipment;
use App\Model\Shipmentdelivery;
use App\Model\Shipmentpackage;
use App\Model\Shipmentwarehouselocation;
use App\Model\Shipmentothercharges;
use App\Model\Deliverycompany;
use App\Model\Dispatchcompany;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\Viewshipment;
use App\Model\ViewShipmentDetails;
use App\Model\Shippingmethods;
use App\Model\Order;
use App\Model\Paymentmethod;
use App\Model\Packagedetails;
use App\Model\Warehousemessage;
use App\Model\Shipmentwarehousemessage;
use App\Model\Warehouse;
use App\Model\Addressbook;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Excel;
use PDF;
use Carbon\Carbon;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Hash;
use Mail;
use customhelper;
use Spatie\Activitylog\Models\Activity;
use App\Model\Schedulenotification;
use Response;

class ShipmentController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $isSearched = 0;
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Shipments'), Auth::user()->id); // call the helper function
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        \Session::forget('SHIPMENTDETAILS');
        $searchShipmentArr = array(
            'idFrom' => '',
            'idTo' => "",
            'userUnit' => '',
            'shipmentStatus' => '',
            'prepaid' => '',
            'deliveryCompanyId' => '',
            'dispatchCompanyId' => '',
            'shippingMethod' => '',
            'shipmentType' => '',
            'totalFrom' => '',
            'totalTo' => '',
            'itemCostFrom' => '',
            'itemCostTo' => '',
            'user' => '',
            'receiver' => '',
            'paymentStatus' => '',
            'toCountry' => '',
            'warehouseId' => '',
            'labelType' => '',
            'itemType' => '',
            'store' => '',
            'category' => '',
            'subcategory' => '',
            'product' => '',
            'packed' => '',
            'rows' => '',
            'zones' => ''
        );

        //  \Session::forget('SHIPMENTDATA');

        if (\Request::isMethod('post')) {

            $isSearched = 1;
            /* GET POST VALUE  */
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchShipment = \Input::get('searchShipment', $searchShipmentArr);
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('SHIPMENTDATA');
            \Session::push('SHIPMENTDATA.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('SHIPMENTDATA.searchDisplay', $searchDisplay);
            \Session::push('SHIPMENTDATA.searchByDate', $searchByDate);
            \Session::push('SHIPMENTDATA.searchShipment', $searchShipment);
            \Session::push('SHIPMENTDATA.field', $field);
            \Session::push('SHIPMENTDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchShipment'] = $searchShipment;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('SHIPMENTDATA.field');
            $sortType = \Session::get('SHIPMENTDATA.type');
            $searchByCreatedOn = \Session::get('SHIPMENTDATA.searchByCreatedOn');
            $searchByDate = \Session::get('SHIPMENTDATA.searchByDate');
            $searchShipment = \Session::get('SHIPMENTDATA.searchShipment');
            $searchDisplay = \Session::get('SHIPMENTDATA.searchDisplay');
            if (!empty($searchShipment[0])) {
                $isSearched = 1;
            }

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchShipment'] = !empty($searchShipment) ? $searchShipment[0] : $searchShipmentArr;
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }


        /* BUILD SORTING ARRAY */
        $sort = array(
            'id' => array('current' => 'sorting'),
            'fromName' => array('current' => 'sorting'),
            'deliveryCompany' => array('current' => 'sorting'),
            'dispatchCompany' => array('current' => 'sorting'),
            'total' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';
        if (!empty($request->page))
            $isSearched = 1;
        $param['isSearched'] = $isSearched;
        /* FETCH USER LIST  */

        $shipmentData = Shipment::getShipmentList($param);

        $userData = collect(User::all());
        $data['userList'] = $userData->mapWithKeys(function($item) {
            return [$item['id'] => $item['firstName'] . " " . $item['lastName']];
        });

        $countryData = collect(Country::all());
        $data['countryList'] = $countryData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });

        $stateData = collect(State::all());
        $data['stateList'] = $stateData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });

        $cityData = collect(City::all());
        $data['cityList'] = $cityData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        //echo "<pre>"; print_r($data['countryList']);
        /* FETCH DELIVERY COMPANY LIST  */
        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();
        /* FETCH DISPATCH COMPANY LIST  */
        $data['dispatchCompanyList'] = Dispatchcompany::where('status', '1')->orderby('name', 'asc')->get();
        /* FETCH COUNTRY LIST  */
        $data['countryRec'] = Country::where('status', '1')->orderby('name', 'asc')->get();
        /* FETCH Warehouse LIST  */
        $data['warehouseList'] = Shipment::getWarehouseList();
        /* Fetch shipment types */
        $data['shipmentTypeList'] = Shipment::allParticularShipmenttype();
        /* FETCH SHIPPING METHOD LIST  */
        $shippingMethodData = collect(Shippingmethods::where('active', 'Y')->where('deleted', '0')->orderby('shipping', 'asc')->get());
        $data['shippingMethodList'] = $shippingMethodData->mapWithKeys(function($item) {
            return [$item['shippingid'] => $item['shipping']];
        });
        /* FETCH STORE LIST  */
        $data['storeList'] = \App\Model\Stores::where('deleted', '0')->where('status', '1')->orderby('storeName', 'asc')->get();

        /* FETCH CATEGORY LIST  */
        $data['categoryList'] = \App\Model\Sitecategory::getCategoryList('parent');

        $data['warehouseRowList'] = \App\Model\Warehouselocation::where('type', 'R')->where('status', '1')->orderby('name', 'asc')->get();
        if (!empty($param['searchShipment']['rows']))
            $data['warehouseZoneList'] = \App\Model\Warehouselocation::where('type', 'Z')->where('status', '1')->where('parentId', $param['searchShipment']['rows'])->get();

        /* SET DATA FOR VIEW  */
        $data['subcategoryList'] = array();
        if (!empty($param['searchShipment']['category'])) {
            $data['subcategoryList'] = \App\Model\Sitecategory::getSubCategory($param['searchShipment']['category']);
        }
        $data['productList'] = array();
        if (!empty($param['searchShipment']['subcategory'])) {
            $data['productList'] = \App\Model\Siteproduct::getListBySubCatid($param['searchShipment']['subcategory']);
        }

        $data['title'] = "Administrative Panel :: Shipments";
        $data['contentTop'] = array('breadcrumbText' => 'Shipments', 'contentTitle' => 'Users', 'pageInfo' => 'This section allows you to manage shipments');
        $data['pageTitle'] = "Shipments";
        $data['page'] = $shipmentData->currentPage();
        $data['shipmentData'] = $shipmentData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];


        return view('Administrator.shipments.index', $data);
    }

    /**
     * Method for add edit shipment
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();

        $shipmentDetailsViewedSessionData = \Session::get('SHIPMENTDETAILS.isviewed');
        $shipmentDetailsViewedShipment = \Session::get('SHIPMENTDETAILS.shipmentId');
        $shipmentDetailsViewed = ((!empty($shipmentDetailsViewedSessionData) && ($shipmentDetailsViewedShipment[0] == $id)) ? $shipmentDetailsViewedSessionData[0] : "0");
        $data['shipmentDetailsViewed'] = $shipmentDetailsViewed;

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Shipments'), Auth::user()->id); // call the helper function
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $data['adminRole'] = Auth::user()->userType;
        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Shipments";
        $data['contentTop'] = array('breadcrumbText' => 'Shipments', 'contentTitle' => 'Users', 'pageInfo' => 'This section allows you to manage shipments');
        $data['page'] = !empty($page) ? $page : '1';

        /* FETCH WAREHOUSE LIST  */
        $data['warehouseList'] = Shipment::getWarehouseList();

        /* FETCH DELIVERY COMPANY LIST  */
        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();

        /* FETCH STORE LIST  */
        $data['storeList'] = \App\Model\Stores::where('deleted', '0')->where('status', '1')->orderby('storeName', 'asc')->get();

        /* FETCH CATEGORY LIST  */
        $data['categoryList'] = \App\Model\Sitecategory::getCategoryList('parent');

        /* FETCH WAREHOUSE ROW LIST  */
        $data['warehouseRowList'] = \App\Model\Warehouselocation::where('type', 'R')->where('status', '1')->orderby('name', 'asc')->get();

        /* FETCH OTHER CHARGES LIST */
        $data['otherChargesList'] = \App\Model\Othershipmentcharges::where('deleted', '0')->where('status', '1')->orderby('orderby', 'asc')->get();

        $data['particularShipmentTypeList'] = Shipment::allParticularShipmenttype();

        if (!empty($id)) {
            $data['id'] = $id;

            $shipment = Shipment::find($id);
            $userData = User::find($shipment->userId);

            $data['shipment'] = $shipment;
            $data['userData'] = $userData;
            $data['shipment']['warehouselocation'] = Shipment::getWareHouseLocationList($id);

            $countryData = collect(Country::all());
            $data['countryList'] = $countryData->mapWithKeys(function($item) {
                return [$item['id'] => $item['name']];
            });

            $stateData = collect(State::all());
            $data['stateList'] = $stateData->mapWithKeys(function($item) {
                return [$item['id'] => $item['name']];
            });

            $cityData = collect(City::all());
            $data['cityList'] = $cityData->mapWithKeys(function($item) {
                return [$item['id'] => $item['name']];
            });

            /* Fetch Warehouse Location History */
            $id = (int) $id;
            $data['shipment']['warehouseLocationLog'] = Shipment::getShipmentHistory($id);

            /* Fetch  Shipment Delivery Data */
            //$deliveryData = ViewShipmentDetails::where("shipmentId", $id)->where('deleted', '0')->where('packageDeleted', '0')->where('packageType', 'I')->orderBy('deliveryId', 'asc')->get()->toarray();
            $deliveryData = array();
            //$data['shipment']['delivery'] = Shipment::getDeliveryDetails($deliveryData);
            $data['shipment']['delivery'] = array();
            $totalShipmentStorageCharge = '0.00';
            $deliveryStorageDetails = Shipmentdelivery::select("maxStorageDate", "storageCharge")->where("shipmentId", $id)->where("deleted", "0")->get();
            if (!empty($deliveryStorageDetails)) {
                foreach ($deliveryStorageDetails as $delivery) {
                    /* Storage charge calculation */
                    $maxStorageDateRaw = isset($delivery->maxStorageDate) ? $delivery->maxStorageDate : '';
                    $maxStorageDate = Carbon::parse($maxStorageDateRaw);
                    $now = Carbon::now();
                    $diff = $maxStorageDate->diffInDays($now, FALSE);
                    if ($diff > 0 && !empty($delivery->storageCharge)) {
                        $totalShipmentStorageCharge += $diff * $delivery->storageCharge;
                    }
                }
            }

            $data['totalShipmentStorageCharge'] = $totalShipmentStorageCharge;

            $data['priceUpdateLog'] = \App\Model\Shipmentitempriceupdatelog::getLogData($id);
        
            $paymentMethodData = collect(Paymentmethod::orderby('orderby', 'asc')->get());


            $data['paymentMethodList'] = $paymentMethodData->mapWithKeys(function($item) {
                return [$item['id'] => $item['paymentMethod']];
            });
            
            $data['shipment']['deleteddelivery'] = array();

            /* Fetch Warehouse Message */
            $data['warehousemessages'] = Warehousemessage::where('status', '1')->get();

            $data['shipment']['invoices'] = collect(DB::select('select * from stmd_invoices where shipmentId = ' . $id . ' and (type = "' . $shipment->shipmentType . '" or  type ="itemReturn") and deleted="0"'));

            $deliveryAddress = array();
            if ($shipment->paymentStatus == 'paid') {
                $paidInvoice = \App\Model\Invoice::where('shipmentId', $id)->where('type', $shipment->shipmentType)->where("type", $shipment->shipmentType)->first();
                if (!empty($paidInvoice)) {
                    $invoiceData = json_decode($paidInvoice->invoiceParticulars, true);
                    if (!empty($invoiceData['shippingaddress'])) {
                        $shippingAddress = $invoiceData['shippingaddress'];
                        $deliveryAddress['toName'] = $shippingAddress['toName'];
                        $deliveryAddress['toCountry'] = $shippingAddress['toCountry'];
                        $deliveryAddress['toState'] = $shippingAddress['toState'];
                        $deliveryAddress['toCity'] = $shippingAddress['toCity'];
                        $deliveryAddress['toAddress'] = $shippingAddress['toAddress'];
                        $deliveryAddress['toEmail'] = $shippingAddress['toEmail'];
                        $deliveryAddress['toPhone'] = $shippingAddress['toPhone'];
                    }
                }
            }

            if (empty($deliveryAddress)) {
                $deliveryAddress['toName'] = $shipment->toName;
                $deliveryAddress['toCountry'] = $shipment->toCountry;
                $deliveryAddress['toState'] = $shipment->toState;
                $deliveryAddress['toCity'] = $shipment->toCity;
                $deliveryAddress['toAddress'] = $shipment->toAddress;
                $deliveryAddress['toEmail'] = $shipment->toEmail;
                $deliveryAddress['toPhone'] = $shipment->toPhone;
            }

            $data['deliveryAddress'] = $deliveryAddress;
            // dd($data['shipment']['invoices']);
            $data['extraChargeExist'] = \App\Model\Invoice::checkIfExtraInvoiceExist($data['shipment']['invoices']);

            /* Fetch Shippinglabel generate log */
            $data['shippingLabelLog'] = \App\Model\Shipmentlabelprintlog::where('shipmentId', $id)->get();

//            $shipmentData = $shipment->toArray();

            /* Fetch all shipping methods and shipping costs */
            //$allShippingMethodsCharges = Shippingmethods::getShippingMethodsAndCharges($shipmentData, $deliveryData);
            //$data['allShippingMethodsCharges'] = $this->getDeliverywiseShippingcharge($shipmentData, $data['shipment']['delivery']);
            /* Fetch all active payment methods */
            //$data['allPaymentMethod'] = Paymentmethod::where('status', '1')->orderBy('orderby', 'asc')->get();
            /* Fetch order details data if exist */
            $data['orderData'] = Order::where('shipmentId', $id)->first();
            /* Fetch packaging packaging records if exist */
            $data['packageDetailsData'] = \App\Model\Shipmentspackaging::where("shipmentId", $id)->with('packageaddedby')->get();

            /* Calculate total chargebale weight of all packaging records */
            $packageTotalChargebleWeight = Shipmentpackage::calculatetotalchargebleweight($data['packageDetailsData']);
            /* Get difference between total weight of deliveries and total chargeable weight of packaging records */
            $deliveryTotalChargeableWeight = Shipmentdelivery::selectRaw("sum(weight) as totalChargeableWeight")->where("shipmentId", $id)->where("deleted", "0")->groupBy("shipmentId")->get();

            $totalChargeableWeight = '0.00';
            if ($deliveryTotalChargeableWeight->count() > 0)
                $totalChargeableWeight = $deliveryTotalChargeableWeight[0]->totalChargeableWeight;
            $data['packageAcceptableLimit'] = Shipmentpackage::getpackageLimit($packageTotalChargebleWeight, $totalChargeableWeight);
            /* Fetch all statuses of orders */
            $data['allOrderStatus'] = Order::allOrderStatus();
            /* Fetch package details comments data */
            $data['packageDetailsCommentData'] = Packagedetails::where('shipmentId', $id)->get();
            /* Fetch all shipping methods */
            //$data['allShippingMethods'] = Shippingmethods::getAllShippingMethods();
            /* Fetch snapshot record */
            //$data['deliverySnapshotCount'] = \App\Model\Shipmentsnapshot::deliverywiseSnapshot($id);
            $data['deliverySnapshotCount'] = array();
            /* FETCH WAREHOUSE NOTES */
            $data['warehouseNotes'] = \App\Model\Shipmentwarehousenotes::where('shipmentId', $id)->where('type', 'S')->orderBy('id', 'desc')->limit(5)->get()->toArray();
            /* Fetch deliverywise status log data */
            //$data['statusLog'] = \App\Model\Shipmentstatuslog::getDeliverywiseStatusLog($id);
            $data['statusLog'] = array();
            $data['packageStatusLog'] = \App\Model\Shipmentpackagestatuslog::where('shipmentId',$id)->first();
            /* Fetch wrong invoice data */
            $data['wrongInvoice'] = \App\Model\Shipmentfile::where('shipmentId', $id)->where('type', 'wronginvoice')->first();
            //dd($data['statusLog']);
            $data['action'] = 'Edit';
            $data['pageTitle'] = "Shipment Details";

            if ($shipment->shipmentType == 'shopforme')
                return view('Administrator.shipments.editshopforme', $data);
            else if ($shipment->shipmentType == 'autopart')
                return view('Administrator.shipments.editautopart', $data);
            else
                return view('Administrator.shipments.editothershipment', $data);
        } else {
            if (\Session::has('newShipmentDeliveryid')) {
                $deliveryInfo = \Session::get('newShipmentDeliveryid');
                $deliveryId = $deliveryInfo[0];
                /* Fetch Shipment Other Charges */
                $data['othercharges'] = Shipmentothercharges::getDeliverywiseNewCharges($deliveryId);
                /* Fetch Shipment Other Charges */
                $data['manualcharges'] = Shipmentothercharges::getDeliverywiseNewCharges($deliveryId, 'manual');
            }
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Create New Shipment";
            return view('Administrator.shipments.add', $data);
        }
    }

    public function getstorelist($warehouseId) {
        $data = \App\Model\Stores::getAllTypeStores(array('warehouseId' => $warehouseId));

        echo json_encode($data);
    }

    public function getcategorylist($storeId) {
        $data = \App\Model\Stores::getAllCategoriesByStore(array('type' => '', 'storeId' => $storeId));

        echo json_encode($data);
    }

    public function methodwiseshippingcharge(Request $request) {
        $shipmentId = $request->shipmentId;
        $deliveryId = $request->deliveryId;
        $shippingMethod = $request->shippingMethodId;

        $shipment = Viewshipment::find($shipmentId);
        $shipmentData = $shipment->toArray();
        $deliveryRecord = ViewShipmentDetails::where("shipmentId", $shipmentId)->where('deliveryId', $deliveryId)->where('deleted', '0')->where('packageDeleted', '0')->where('packageType', 'I')->get()->toarray();
        $deliveryData = Shipment::getDeliveryDetails($deliveryRecord);

        $shippingCharges = $this->getDeliverywiseShippingcharge($shipmentData, $deliveryData, $shippingMethod);
        return view('Administrator.shipments.methodwiseshippingcharge', array('shipmentCharges' => $shippingCharges[$deliveryId][0]));
    }

    public function getDeliverywiseShippingcharge($shipmentData, $deliveryData, $shippingMethod = '-1') {
        $result = array();
        $param['fromCountry'] = $shipmentData['fromCountry'];
        $param['fromState'] = $shipmentData['fromState'];
        $param['fromCity'] = $shipmentData['fromCity'];
        $param['toCountry'] = $shipmentData['toCountry'];
        $param['toState'] = $shipmentData['toState'];
        $param['toCity'] = $shipmentData['toCity'];
        $param['shipmentId'] = $shipmentData['id'];
        $param['userId'] = $shipmentData['userId'];
        $param['totalProcurementCost'] = $shipmentData['totalProcurementCost'];

        if ($shippingMethod != '-1')
            $param['shippingId'] = $shippingMethod;
        if (!empty($deliveryData['deliveries'])) {
            foreach ($deliveryData['deliveries'] as $deliveryId => $eachDelivery) {
                $param['totalQuantity'] = $eachDelivery['totalQty'];
                $param['totalWeight'] = $eachDelivery['chargeableWeight'];
                $param['deliveryId'] = $deliveryId;
                $result[$deliveryId] = Shippingmethods::calculateShippingMethodCharges($param);
            }
        }
        return $result;
        //dd($result);
    }

    private function getShipmentTotalCost($shipmentId) {
        $shipment = Viewshipment::find($shipmentId);
        $totalCharge = 0;
        if (!empty($shipment->storageCharge) && !empty($shipment->totalOtherCharges)) {
            $totalCharge = $shipment->storageCharge + $shipment->totalOtherCharges;
        }

        return (new \App\Helpers\customhelper)->getCurrencySymbolFormat($totalCharge);
    }

    /**
     * Method to fetch sub category list
     * @param integer $categoryId
     * @return json
     */
    public function getsubcategorylist($categoryId) {
        $subCategoryList = \App\Model\Sitecategory::getSubCategory($categoryId);

        echo json_encode($subCategoryList);
    }

    /**
     * Method to fetch sub category list
     * @param integer $categoryId
     * @return json
     */
    public function getproductlist($categoryId) {
        $productList = \App\Model\Siteproduct::getListBySubCatid($categoryId);

        echo json_encode($productList);
    }

    /**
     * Method to add /' edit shipment delivery details
     * @param integer $shipmentid
     * @param integer $id
     * @param object $request
     * @return string
     */
    public function addeditdelivery($shipmentid, $id = 0, Request $request) {
        if (\Request::isMethod('post')) {
            $received = 'N';
            $chargeableWeight = $shipmentDeliveryId = $totalItemCost = 0;
            $shipment = new Shipment;
            $shipment = Shipment::find($shipmentid);

            /* Insert Data into Delivery Mapping Table */
            $shipmentDelivery = new Shipmentdelivery;

            $settings = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();

            $shipmentDelivery->shipmentId = $shipmentid;
            $shipmentDelivery->deliveryNote = $request->note;
            $shipmentDelivery->deliveredOn = $request->delivered;
            $shipmentDelivery->deliveryCompanyId = $request->deliveryCompanyId;
            $shipmentDelivery->tracking = $request->trackingNumber;
            $shipmentDelivery->tracking2 = $request->trackingNumber2;
            $shipmentDelivery->deliveryNote = $request->note;
            $shipmentDelivery->deliveredOn = \Carbon\Carbon::parse($request->estimateDeliveryDate)->format('Y-m-d');
            $shipmentDelivery->length = $request->length;
            $shipmentDelivery->width = $request->width;
            $shipmentDelivery->height = $request->height;
            $shipmentDelivery->weight = $request->weight;
            $chargeableWeight = max($request->weight, (($request->length * $request->height * $shipmentDelivery->width) / $settings->settingsValue));
            $shipmentDelivery->chargeableWeight = $chargeableWeight;
            $shipmentDelivery->shippingCost = '0.00';
            $shipmentDelivery->clearingDutyCost = '0.00';
            $shipmentDelivery->otherChargeCost = '0.00';
            $shipmentDelivery->received = 'Y';
            $shipmentDelivery->createdBy = Auth::user()->id;
            $shipmentDelivery->createdByType = 'admin';
            $shipmentDelivery->createdOn = Config::get('constants.CURRENTDATE');

            $shipmentDelivery->save();
            $shipmentDeliveryId = $shipmentDelivery->id;
            /* Insert Data into Delivery Mapping Table */

            /* Insert Data into Shipment Packages Mapping Table */
            if (!empty($shipmentDeliveryId)) {
                $total = 0;
                for ($i = 0; $i < count($request->shipment['item']); $i++) {

                    $productWeight = '0.00';
                    if ($request->shipment['siteProductId'][$i] != "0")
                        $productWeight = \App\Model\Siteproduct::find($request->shipment['siteProductId'][$i])->weight;

                    $shipmentPackage = new Shipmentpackage;

                    $shipmentPackage->shipmentId = $shipmentid;
                    $shipmentPackage->storeId = $request->shipment['storeId'][$i];
                    $shipmentPackage->siteCategoryId = $request->shipment['siteCategoryId'][$i];
                    $shipmentPackage->siteSubCategoryId = $request->shipment['siteSubCategoryId'][$i];
                    $shipmentPackage->siteProductId = $request->shipment['siteProductId'][$i];
                    $shipmentPackage->weight = $productWeight;
                    $shipmentPackage->itemName = $request->shipment['item'][$i];
                    $shipmentPackage->type = "I";
                    $shipmentPackage->deliveryNotes = $request->note;
                    $shipmentPackage->deliveryCompanyId = $request->deliveryCompanyId;
                    $shipmentPackage->deliveredOn = $request->delivered;
                    $shipmentPackage->tracking = $request->trackingNumber;
                    $shipmentPackage->tracking2 = $request->trackingNumber2;
                    $shipmentPackage->itemMinPrice = $request->shipment['minValue'][$i];
                    $shipmentPackage->itemPrice = $request->shipment['value'][$i];
                    $shipmentPackage->itemQuantity = $request->shipment['quantity'][$i];
                    $shipmentPackage->itemTotalCost = $request->shipment['total'][$i];
                    $shipmentPackage->deliveryId = $shipmentDeliveryId;


                    $totalItemCost += $request->shipment['total'][$i];


                    $shipmentPackage->save();
                }
            }

            /* Update Total Amount into Shipment Mapping Table */
            $shipment = Shipment::find($shipmentid);
            $shipmentDeliveryData = Shipmentdelivery::find($shipmentDeliveryId);

            $maxStorageDateSettings = \App\Model\Generalsettings::where('settingsKey', 'max_storage_days')->first();
            $chargePerUnitSettings = \App\Model\Generalsettings::where('settingsKey', 'unit_weight_charge')->first();
            $inventoryChargeSettings = \App\Model\Generalsettings::where('settingsKey', 'other_delivery_charge')->first();
            $maxStorageDate = date('Y-m-d', strtotime('+' . $maxStorageDateSettings->settingsValue . ' days', time()));
            $storageCharge = $chargeableWeight * $chargePerUnitSettings->settingsValue;
            $inventoryChargeCost = !empty($inventoryChargeSettings->settingsValue) ? $inventoryChargeSettings->settingsValue : '0.00';

            $shipment->firstReceived = date('Y-m-d h:i:s');
            $shipment->maxStorageDate = $maxStorageDate;
            $shipment->storageCharge = $shipment->storageCharge + $storageCharge;
            $shipment->save();

            $shipmentDeliveryData->totalItemCost = $totalItemCost;
            $shipmentDeliveryData->inventoryCharge = $inventoryChargeCost;
            $shipmentDeliveryData->totalCost = $inventoryChargeCost;
            $shipmentDeliveryData->maxStorageDate = $maxStorageDate;
            $shipmentDeliveryData->storageCharge = $storageCharge;
            $shipmentDeliveryData->save();

            if (\Session::has('newShipmentDeliveryid')) {
                /* Get new delivery other charges and update shipment id and delivery id in other charges table */
                $newDeliveryData = \Session::get('newShipmentDeliveryid');
                $deliveryId = $newDeliveryData[0];
                Shipmentothercharges::where("deliveryId", $deliveryId)->update(["deliveryId" => $shipmentDeliveryId]);
                \Session::forget('newShipmentDeliveryid'); // Delete old random delivery id

                /* Update added other charges in delivery table */
                $totalOthercharge = Shipmentothercharges::where("shipmentId", $shipmentid)->where("deliveryId", $shipmentDeliveryId)->sum("otherChargeAmount");
                Shipmentdelivery::where('id', $shipmentDeliveryId)->increment('otherChargeCost', $totalOthercharge);
                Shipmentdelivery::where('id', $shipmentDeliveryId)->increment('totalCost', $totalOthercharge);
            }

            $shipmentstauslog = new \App\Model\Shipmentstatuslog;
            $shipmentstauslog->shipmentId = $shipmentid;
            $shipmentstauslog->deliveryId = $shipmentDeliveryId;
            $shipmentstauslog->oldStatus = 'none';
            $shipmentstauslog->status = 'in_warehouse';
            $shipmentstauslog->updatedOn = Config::get('constants.CURRENTDATE');
            $shipmentstauslog->save();

//            Shipment::notificationEmail($shipmentid,'create_delivery_notification');
//            Shipment::notificationSMS($shipmentid,$shipmentDeliveryId);

            $shipmentEmailNotification = new \App\Model\Shipmentemailnotification;
            $shipmentEmailNotification->shipmentId = $shipmentid;
            $shipmentEmailNotification->deliveryId = $shipmentDeliveryId;
            $shipmentEmailNotification->action = 'create_delivery';
            $shipmentEmailNotification->save();

            User::sendPushNotification($shipment->userId, 'create_delivery_notification', Auth::user()->id);


            return redirect()->back()->with('successMessage', 'Shipment delivery added successfuly.');
        }

        /* FETCH DELIVERY COMPANY LIST  */
        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();

        /* FETCH CATEGORY LIST  */
        $data['categoryList'] = \App\Model\Sitecategory::getCategoryList('parent');

        /* FETCH STORE LIST  */
        $data['storeList'] = \App\Model\Stores::where('deleted', '0')->where('status', '1')->orderby('storeName', 'asc')->get();

        /* FETCH OTHER CHARGES LIST */
        $data['otherChargesList'] = \App\Model\Othershipmentcharges::where('deleted', '0')->where('status', '1')->orderby('orderby', 'asc')->get();

        if (\Session::has('newShipmentDeliveryid')) {
            $deliveryInfo = \Session::get('newShipmentDeliveryid');
            $deliveryId = $deliveryInfo[0];
            /* Fetch Shipment Other Charges */
            $data['othercharges'] = Shipmentothercharges::getDeliverywiseNewCharges($deliveryId);
            /* Fetch Shipment Other Charges */
            $data['manualcharges'] = Shipmentothercharges::getDeliverywiseNewCharges($deliveryId, 'manual');
        }


        $shipment = Shipment::find($shipmentid);
        $userSettings = \App\Model\Usershipmentsettings::select('fieldType', 'fieldValue')->where('userId', $shipment->userId)->whereNotNull('fieldValue')->get();
        if (!$userSettings->isEmpty()) {
            foreach ($userSettings as $row) {
                $data['userSettings'][$row->fieldType] = $row->fieldValue;
            }
        } else {
            $data['userSettings']['remove_shoe_box'] = '';
            $data['userSettings']['original_box'] = '';
            $data['userSettings']['original_box_all'] = '';
            $data['userSettings']['take_photo'] = '';
            $data['userSettings']['scan_invoice'] = '';
            $data['userSettings']['item_inventory'] = '';
            $data['userSettings']['special_instruction'] = 'N/A';
        }

        /* SET DATA FOR VIEW  */
        $data['shipmentId'] = $shipmentid;
        $data['id'] = $id;
        $data['action'] = 'Add';
        $data['pageTitle'] = "Add New Delivery";
        return view('Administrator.shipments.addeditdelivery', $data);
    }

    /**
     * Method to add new shipment package details
     * @param object $request
     * @return boolean
     */
    public function addshipmentpackage(Request $request) {
        if (\Request::isMethod('post')) {
            $received = 'N';
            $shipmentPackage = new Shipmentpackage;
            for ($i = 0; $i < count($request->data); $i++) {
                $name = $request->data[$i]['name'];

                if ($name == 'shipmentId')
                    $shipmentId = $request->data[$i]['value'];
                if ($name != 'received')
                    $shipmentPackage->$name = $request->data[$i]['value'];

                if ($name == 'received' && $request->data[$i]['value'] == 'Y')
                    $received = 'Y';
            }
            $shipmentPackage->type = 'I';
            $shipmentPackage->createdBy = Auth::user()->id;
            $shipmentPackage->createdOn = Config::get('constants.CURRENTDATE');
            $shipmentPackage->save();

            $shipment = Shipment::find($shipmentId);
            /* If new product received update storage charges */
            if ($received == 'Y' && $shipment->isManualChargeEnabled == 'N') {
                $chargeableWeight = 0;
                $deliveryData = Shipmentdelivery::select("chargeableWeight")->where('shipmentId', $shipmentId)->get();
                if (count($deliveryData) > 0) {
                    foreach ($deliveryData as $eachDeliveryData) {
                        $chargeableWeight += $eachDeliveryData->chargeableWeight;
                    }
                }
                $maxStorageDate = \App\Model\Generalsettings::where('settingsKey', 'max_storage_days')->first();
                $chargePerUnit = \App\Model\Generalsettings::where('settingsKey', 'unit_weight_charge')->first();
                $shipment->firstReceived = date('Y-m-d h:i:s');
                $shipment->maxStorageDate = date('Y-m-d', strtotime('+' . $maxStorageDate->settingsValue . ' days', time()));

                $shipment->storageCharge = $chargeableWeight * $chargePerUnit->settingsValue;
            }
            $shipment->save();

            $shipmentDeliveryId = $shipmentPackage->deliveryId;

            return $shipmentDeliveryId;
        }
    }

    /**
     * Method to edit shipment package details
     * @param object $request
     * @return boolean
     */
    public function editshipmentpackage(Request $request) {
        if (\Request::isMethod('post')) {
            $shipmentPackage = new Shipmentpackage;
            $total = 0;
            for ($i = 0; $i < count($request->data); $i++) {
                $name = $request->data[$i]['name'];
                if ($name == '_token')
                    continue;
                if ($name == 'packageId') {
                    $packageId = $request->data[$i]['value'];
                    $shipmentPackage = Shipmentpackage::find($packageId);
                } else if ($name != 'packagesubcategoryId' && $name != 'packageproductId' && $name != 'trackingNumber') {
                    $shipmentPackage->$name = $request->data[$i]['value'];
                }
            }
            $shipmentPackage->modifiedBy = Auth::user()->id;
            $shipmentPackage->modifiedOn = Config::get('constants.CURRENTDATE');
            $shipmentPackage->save();

//            $shipment = Shipment::find($shipmentPackage->shipmentId);
//            $shipment->total = $total;
//            $shipment->save();
            Shipment::updateDeliveryFields($shipmentPackage->deliveryId,"totalItemCost");
            $shipmentDeliveryId = $shipmentPackage->deliveryId;

            return $shipmentDeliveryId;
        }
    }

    /**
     * Method to upload shipment snapshot image
     * @param object $request
     * @return boolean
     */
    public function uploadimage(Request $request) {
        if (\Request::isMethod('post')) {
            $shipmentId = $request->shipmentId;
            $packageId = $request->packageId;
            $deliveryId = $request->deliveryId;
            //print_r($request->upload_cont_img);exit;
            if ($request->hasFile('upload_cont_img')) {
                foreach ($request->file('upload_cont_img') as $image) {
                    if (substr($image->getMimeType(), 0, 5) == 'image') {
                        $name = time() . '_' . $image->getClientOriginalName();
                        $destinationPath = public_path("/uploads/shipments/$shipmentId");
                        if (!file_exists($destinationPath)) {
                            mkdir($destinationPath);
                            chmod($destinationPath, 0777);
                        }
                        $image->move($destinationPath, $name);
                    }

                    //Insert Data Into Table
                    if (!empty($name)) {
                        $shipmentSnap = new \App\Model\Shipmentsnapshot;
                        $shipmentSnap->shipmentId = $shipmentId;
                        $shipmentSnap->deliveryId = $deliveryId;
                        $shipmentSnap->packageId = $packageId;
                        $shipmentSnap->imageName = $name;
                        $shipmentSnap->addedBy = Auth::user()->id;
                        $shipmentSnap->save();
                    }
                }

                return redirect()->back()->with('successMessage', 'Shipment snapshot uploaded successfuly.');
            } else {
                return redirect()->back()->withErrors('Please Upload any image');
            }
        } else {
            return redirect()->back()->withErrors('Error in operation!');
        }
    }

    public function getsnapshots($shipmentId, $deliveryId, $packageId) {
        //if($deliveryId == 0)
        //$data = \App\Model\Shipmentsnapshot::where('shipmentId',$shipmentId)->where('deliveryId',0)->where('packageId',$packageId)->get();
        if ($packageId == 0)
            $data = \App\Model\Shipmentsnapshot::where('shipmentId', $shipmentId)->where('deliveryId', $deliveryId)->get();
        else
            $data = \App\Model\Shipmentsnapshot::where('shipmentId', $shipmentId)->where('deliveryId', $deliveryId)->where('packageId', $packageId)->get();
        return view('Administrator.shipments.snapshotimages', array('data' => $data));
    }

    /**
     * Method used to delete delivery package
     * @param integer $id
     * @return boolean
     */
    public function deletedeliveryitem($id) {
        if (!empty($id)) {
            $createrModifierId = Auth::user()->id;
            if (Shipmentpackage::deleteRecord($id, $createrModifierId)) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Method used to delete shipment delivery
     * @param integer $id
     * @return boolean
     */
    public function deletedelivery($id) {
        if (!empty($id)) {
            $createrModifierId = Auth::user()->id;
            if (Shipmentdelivery::deleteRecord($id, $createrModifierId)) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Method to add new shipment charge details
     * @param integer $shipmentId
     * @param object $request
     * @return boolean
     */
    public function addshipmentcharge($shipmentId, $deliveryId, Request $request) {
        if (\Request::isMethod('post')) {
            $otherChargeId = $request->otherChargeId;
            if (!empty($shipmentId) && !empty($deliveryId) && !empty($otherChargeId)) {
                $otherCharge = \App\Model\Othershipmentcharges::find($otherChargeId);

                $shipmentOtherCharge = new Shipmentothercharges;
                $shipmentOtherCharge->shipmentId = $shipmentId;
                $shipmentOtherCharge->deliveryId = $deliveryId;
                $shipmentOtherCharge->otherChargeId = $otherChargeId;
                $shipmentOtherCharge->otherChargeName = $otherCharge->name;
                $shipmentOtherCharge->otherChargeAmount = $otherCharge->amount;
                $shipmentOtherCharge->notes = $request->notes;
                $shipmentOtherCharge->createdBy = Auth::user()->email;
                $shipmentOtherCharge->createdOn = Config::get('constants.CURRENTDATE');
                $shipmentOtherCharge->save();

                $shipment = Shipment::find($shipmentId);
                $shipment->totalOtherCharges = $shipment->totalOtherCharges + $otherCharge->amount;
                $shipment->save();

                Shipmentdelivery::where('id', $deliveryId)->increment('otherChargeCost', $otherCharge->amount);
                Shipmentdelivery::where('id', $deliveryId)->increment('totalCost', $otherCharge->amount);

                return 1;
            } else {
                if ($request->type == 'manual') {
                    $shipmentOtherCharge = new Shipmentothercharges;
                    $shipmentOtherCharge->shipmentId = $shipmentId;
                    $shipmentOtherCharge->deliveryId = $deliveryId;
                    $shipmentOtherCharge->otherChargeName = $request->otherChargeName;
                    $shipmentOtherCharge->otherChargeAmount = $request->otherChargeAmount;
                    $shipmentOtherCharge->notes = $request->notes;
                    $shipmentOtherCharge->createdBy = Auth::user()->email;
                    $shipmentOtherCharge->createdOn = Config::get('constants.CURRENTDATE');
                    $shipmentOtherCharge->save();

                    $shipment = Shipment::find($shipmentId);
                    $shipment->totalOtherCharges = $shipment->totalOtherCharges + $request->otherChargeAmount;
                    $shipment->save();

                    Shipmentdelivery::where('id', $deliveryId)->increment('otherChargeCost', $request->otherChargeAmount);
                    Shipmentdelivery::where('id', $deliveryId)->increment('totalCost', $request->otherChargeAmount);

                    return 1;
                } else
                    return 0;
            }
        }
    }

    /**
     * Method to add new shipment charge details
     * @param integer $shipmentId
     * @param object $request
     * @return boolean
     */
    public function addshipmentchargenewdelivery($shipmentId = 0, Request $request) {
        if (\Request::isMethod('post')) {
            $otherChargeId = $request->otherChargeId;
            if (!\Session::has('newShipmentDeliveryid')) {
                $deliveryId = substr(time(), 0, 11);
                \Session::push('newShipmentDeliveryid', $deliveryId);
            } else {
                $newDeliveryData = \Session::get('newShipmentDeliveryid');
                $deliveryId = $newDeliveryData[0];
            }
            if (!empty($otherChargeId)) {
                $otherCharge = \App\Model\Othershipmentcharges::find($otherChargeId);

                $shipmentOtherCharge = new Shipmentothercharges;
                $shipmentOtherCharge->shipmentId = $shipmentId;
                $shipmentOtherCharge->deliveryId = $deliveryId;
                $shipmentOtherCharge->otherChargeId = $otherChargeId;
                $shipmentOtherCharge->otherChargeName = $otherCharge->name;
                $shipmentOtherCharge->otherChargeAmount = $otherCharge->amount;
                $shipmentOtherCharge->notes = $request->notes;
                $shipmentOtherCharge->createdBy = Auth::user()->email;
                $shipmentOtherCharge->createdOn = Config::get('constants.CURRENTDATE');
                $shipmentOtherCharge->save();

//                $shipment = Shipment::find($shipmentId);
//                $shipment->totalOtherCharges = $shipment->totalOtherCharges + $otherCharge->amount;
//                $shipment->save();
//
//                Shipmentdelivery::where('id', $deliveryId)->increment('otherChargeCost', $otherCharge->amount);
//                Shipmentdelivery::where('id', $deliveryId)->increment('totalCost', $otherCharge->amount);

                return 1;
            } else {
                if ($request->type == 'manual') {
                    $shipmentOtherCharge = new Shipmentothercharges;
                    $shipmentOtherCharge->shipmentId = $shipmentId;
                    $shipmentOtherCharge->deliveryId = $deliveryId;
                    $shipmentOtherCharge->otherChargeName = $request->otherChargeName;
                    $shipmentOtherCharge->otherChargeAmount = $request->otherChargeAmount;
                    $shipmentOtherCharge->notes = $request->notes;
                    $shipmentOtherCharge->createdBy = Auth::user()->email;
                    $shipmentOtherCharge->createdOn = Config::get('constants.CURRENTDATE');
                    $shipmentOtherCharge->save();

//                    $shipment = Shipment::find($shipmentId);
//                    $shipment->totalOtherCharges = $shipment->totalOtherCharges + $request->otherChargeAmount;
//                    $shipment->save();
//
//                    Shipmentdelivery::where('id', $deliveryId)->increment('otherChargeCost', $request->otherChargeAmount);
//                    Shipmentdelivery::where('id', $deliveryId)->increment('totalCost', $request->otherChargeAmount);

                    return 1;
                } else
                    return 0;
            }
        }
    }

    /**
     * Method to get shipment other charges list
     * @param integer $shipmentId
     * @return string
     */
    public function getothershipmentcharges($type, $shipmentId, $deliveryId = '-1') {
        if (!empty($shipmentId)) {
            if ($type == 'other')
                $data['otherCharges'] = Shipmentothercharges::where('shipmentId', $shipmentId)->where('deliveryId', $deliveryId)->where('otherChargeId', '!=', '0')->get();
            else
                $data['otherCharges'] = Shipmentothercharges::where('shipmentId', $shipmentId)->where('deliveryId', $deliveryId)->where('otherChargeId', '0')->get();
            return view('Administrator.shipments.othershipmentcharges', $data);
        }
    }

    /**
     * Method to get shipment other charges list
     * @param integer $shipmentId
     * @return string
     */
    public function getothershipmentchargesnewdelivery($type, $shipmentId = 0) {

        if (\Session::has('newShipmentDeliveryid')) {
            $newDeliveryData = \Session::get('newShipmentDeliveryid');
            $deliveryId = $newDeliveryData[0];
        }
        if (!empty($deliveryId)) {
            if ($type == 'other')
                $data['otherCharges'] = Shipmentothercharges::where('shipmentId', $shipmentId)->where('deliveryId', $deliveryId)->where('otherChargeId', '!=', '0')->get();
            else
                $data['otherCharges'] = Shipmentothercharges::where('shipmentId', $shipmentId)->where('deliveryId', $deliveryId)->where('otherChargeId', '0')->get();
            return view('Administrator.shipments.othershipmentchargesnewdelivery', $data);
        }
    }

    /**
     * Method used to delete shipment other charge
     * @param integer $id
     * @return boolean
     */
    public function deleteothercharge($id) {
        if (!empty($id)) {
            $otherCharge = Shipmentothercharges::find($id);
            if ($otherCharge->delete()) {
                $shipment = Shipment::find($otherCharge->shipmentId);

                if (!empty($shipment->totalOtherCharges)) {
                    $shipment->totalOtherCharges = $shipment->totalOtherCharges - $otherCharge->otherChargeAmount;
                    $shipment->save();
                    Shipmentdelivery::where('id', $otherCharge->deliveryId)->decrement('otherChargeCost', $otherCharge->otherChargeAmount);
                    Shipmentdelivery::where('id', $otherCharge->deliveryId)->decrement('totalCost', $otherCharge->otherChargeAmount);
                }

                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Method used to delete shipment other charge
     * @param integer $id
     * @return boolean
     */
    public function deleteotherchargenewdelivery($id) {
        if (!empty($id)) {
            $otherCharge = Shipmentothercharges::find($id);
            if ($otherCharge->delete()) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Method used to update shipment
     * @param integer $id
     * @return type
     */
    public function updateshipment($id, Request $request) {
        if (\Request::isMethod('post')) {

            if (!empty($id)) {
                $shipment = new Shipment;
                $field = $request->field;

                $shipment = Shipment::find($id);
                $shipment->$field = $request->$field;
                $shipment->modifiedBy = Auth::user()->id;
                $shipment->modifiedByType = 'admin';
                $shipment->modifiedOn = Config::get('constants.CURRENTDATE');

                if ($field == 'storageCharge') {
                    $shipment->isManualChargeEnabled = 'Y';
                    $shipment->storageChargeModifiedBy = Auth::user()->email;
                    $shipment->storageChargeModifiedOn = Config::get('constants.CURRENTDATE');
                }

                if ($shipment->save())
                    return 1;
                else
                    return 0;
            } else
                return 0;
        }
    }

    /**
     * Method used to fetch and return storage charge details
     * @param integer $id
     * @return string
     */
    public function updatestoragecharge($id) {
        $shipment = Viewshipment::find($id);

        $html = "Storage Charge - Maximum storage date exceeded - " . (new \App\Helpers\customhelper)->getCurrencySymbolFormat($shipment->storageCharge) . "<br>";
        $html .= '<small class="text-black">Changed By: ' . $shipment->storageChargeModifiedBy . ' ' . \Carbon\Carbon::parse($shipment->storageChargeModifiedOn)->format('m-d-Y | H:i') . '</small>';

        return $html;
    }

    /**
     * Method used to fetch and return other charge details
     * @param integer $shipmentId
     * @return string
     */
    public function gettotalshipmentcharge($shipmentId) {
        return $this->getShipmentTotalCost($shipmentId);
    }

    /**
     * Method used to save create shipment information 
     * @param integer $id
     * @param string $page
     * @param object $request
     * @return boolean
     */
    public function savedata($id, $page, Request $request) {
        if (\Request::isMethod('post')) {
            //print_r($request->all());exit;
            /* Set received to default "No" and chargeable weight to 0 */
            $received = 'N';
            $chargeableWeight = 0;
            $total = 0;
            /* Fetch User Details */
            $userData = User::where('unit', $request->userUnit)->first();
            $userId = $userData->id;

            /* Fetch User Addressbook Details */
            $addressBookData = User::find($userId)->addressbook()->where('isDefaultShipping', '1')->first();

            /* Fetch Warehouse Details */
            $warehouseData = \App\Model\Warehouse::find($request->warehouseId);

            /* Insert Data into Main Shipment Table */
            $shipment = new Shipment;
            $shipment->userId = $userId;
            $shipment->warehouseId = $request->warehouseId;
            $shipment->shipmentType = 'othershipment';
            $shipment->partucilarShipmentType = $request->partucilarShipmentType;
            $shipment->fromCountry = $warehouseData->countryId;
            $shipment->fromState = $warehouseData->stateId;
            $shipment->fromCity = $warehouseData->cityId;
            $shipment->fromAddress = $warehouseData->address;
            $shipment->fromPhone = $userData->contactNumber;
            $shipment->fromName = $userData->firstName . " " . $userData->lastName;
            $shipment->fromEmail = $userData->email;
            $shipment->toCountry = $addressBookData->countryId;
            $shipment->toState = $addressBookData->stateId;
            $shipment->toCity = $addressBookData->cityId;
            $shipment->toAddress = $addressBookData->address;
            $shipment->toPhone = $addressBookData->phone;
            $shipment->toName = $addressBookData->firstName . " " . $addressBookData->lastName;
            $shipment->toEmail = $addressBookData->email;
            $shipment->shipmentStatus = 1;
            $shipment->firstReceived = Config::get('constants.CURRENTDATE');
            $shipment->lockedShippingmethodId = (!empty($request->selectedShippingMethod) ? $request->selectedShippingMethod : '');
            /* $shipment->urgent = $procurementData->urgent;
              $shipment->urgentPurchaseCost = $procurementData->urgentPurchaseCost;
              $shipment->totalItemCost = $procurementData->totalItemCost;
              $shipment->totalProcessingFee = $procurementData->totalProcessingFee;
              $shipment->totalProcurementCost = $procurementData->totalProcurementCost;
              $shipment->totalWeight = $procurementData->totalWeight;
              $shipment->totalShippingCost = $procurementData->totalShippingCost;
              $shipment->totalClearingDuty = $procurementData->totalClearingDuty;
              $shipment->isDutyCharged = $procurementData->isDutyCharged;
              $shipment->totalInsurance = $procurementData->totalInsurance;
              $shipment->totalTax = $procurementData->totalTax;
              $shipment->totalDiscount = $procurementData->totalDiscount;
              $shipment->totalCost = $procurementData->totalCost; */
            $shipment->prepaid = 'N';
            $shipment->paymentStatus = 'unpaid';
            $shipment->createdBy = Auth::user()->id;
            $shipment->createdByType = 'admin';
            $shipment->createdOn = Config::get('constants.CURRENTDATE');
            $shipment->save();
            $shipmentId = $shipment->id;
            //echo $shipmentId;exit;
            /* Insert Data into Main Shipment Table */

            if (!empty($shipmentId)) {
                /* Insert Data into Warehouse Location Mapping Table */
                $shipmentWarehouse = new Shipmentwarehouselocation;
                $shipmentWarehouse->shipmentId = $shipmentId;
                $shipmentWarehouse->warehouseRowId = $request->warehouseRowId;
                $shipmentWarehouse->warehouseZoneId = $request->warehouseZoneId;
                $shipmentWarehouse->createdBy = Auth::user()->id;
                $shipmentWarehouse->createdByType = 'admin';
                $shipmentWarehouse->createdOn = Config::get('constants.CURRENTDATE');
                $shipmentWarehouse->save();
                /* Insert Data into Warehouse Location Mapping Table */

                /* Insert Data into Delivery Mapping Table */
                $shipmentDelivery = new Shipmentdelivery;
                $chargeableWeightFactor = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();

                $shipmentDelivery->shipmentId = $shipmentId;
                $shipmentDelivery->deliveredOn = $request->delivered;
                $shipmentDelivery->deliveryCompanyId = $request->deliveryCompanyId;
                $shipmentDelivery->tracking = $request->trackingNumber;
                $shipmentDelivery->tracking2 = $request->trackingNumber2;
                $shipmentDelivery->deliveryNote = $request->note;
                $shipmentDelivery->length = $request->length;
                $shipmentDelivery->width = $request->width;
                $shipmentDelivery->height = $request->height;
                $shipmentDelivery->weight = $request->weight;
                $chargeableWeight = max($request->weight, (($request->length * $request->height * $shipmentDelivery->width) / $chargeableWeightFactor->settingsValue));
                $shipmentDelivery->chargeableWeight = $chargeableWeight;
                $shipmentDelivery->shippingCost = '0.00';
                $shipmentDelivery->clearingDutyCost = '0.00';
                $shipmentDelivery->otherChargeCost = '0.00';
                $shipmentDelivery->received = 'Y';
                $shipmentDelivery->createdBy = Auth::user()->id;
                $shipmentDelivery->createdByType = 'admin';
                $shipmentDelivery->createdOn = Config::get('constants.CURRENTDATE');

                $shipmentDelivery->save();
                $shipmentDeliveryId = $shipmentDelivery->id;
                /* Insert Data into Delivery Mapping Table */

                /* Insert Data into Shipment Packages Mapping Table */
                $totalItemCost = 0;
                if (!empty($shipmentDeliveryId)) {

                    for ($i = 0; $i < count($request->shipment['item']); $i++) {
                        $productWeight = '0.00';
                        if ($request->shipment['siteProductId'][$i] != "0")
                            $productWeight = \App\Model\Siteproduct::find($request->shipment['siteProductId'][$i])->weight;

                        $shipmentPackage = new Shipmentpackage;
                        $shipmentPackage->shipmentId = $shipmentId;
                        $shipmentPackage->deliveryId = $shipmentDeliveryId;
                        $shipmentPackage->storeId = $request->shipment['store'][$i];
                        $shipmentPackage->siteCategoryId = $request->shipment['siteCategoryId'][$i];
                        $shipmentPackage->siteSubCategoryId = $request->shipment['siteSubCategoryId'][$i];
                        $shipmentPackage->siteProductId = $request->shipment['siteProductId'][$i];
                        $shipmentPackage->weight = $productWeight;
                        $shipmentPackage->itemName = $request->shipment['item'][$i];
                        $shipmentPackage->type = "I";
                        $shipmentPackage->deliveryNotes = $request->note;
                        $shipmentPackage->itemMinPrice = $request->shipment['minValue'][$i];
                        $shipmentPackage->itemPrice = $request->shipment['value'][$i];
                        $shipmentPackage->itemQuantity = $request->shipment['quantity'][$i];
                        $shipmentPackage->itemTotalCost = $request->shipment['total'][$i];
                        $shipmentPackage->deliveryCompanyId = $request->deliveryCompanyId;
                        $shipmentPackage->deliveredOn = $request->delivered;
                        $shipmentPackage->tracking = $request->trackingNumber;
                        $shipmentPackage->tracking2 = $request->trackingNumber2;
                        $shipmentPackage->createdBy = Auth::user()->id;
                        $shipmentPackage->createdOn = Config::get('constants.CURRENTDATE');
                        $totalItemCost += $request->shipment['total'][$i];
                        $shipmentPackage->save();
                    }
                }
                /* Insert Data into Shipment Packages Mapping Table */


                /* Update Total Amount into Shipment Mapping Table */
                $shipment = Shipment::find($shipmentId);
                $shipmentDeliveryData = Shipmentdelivery::find($shipmentDeliveryId);

                $maxStorageDateSettings = \App\Model\Generalsettings::where('settingsKey', 'max_storage_days')->first();
                $chargePerUnitSettings = \App\Model\Generalsettings::where('settingsKey', 'unit_weight_charge')->first();
                //$inventoryChargeSettings = \App\Model\Generalsettings::where('settingsKey', 'other_delivery_charge')->first();
                $inventoryChargeSettings = \App\Model\Generalsettings::where('settingsKey', 'first_delivery_charge')->first();
                $maxStorageDate = date('Y-m-d', strtotime('+' . $maxStorageDateSettings->settingsValue . ' days', time()));
                $storageCharge = $chargeableWeight * $chargePerUnitSettings->settingsValue;
                $inventoryChargeCost = !empty($inventoryChargeSettings->settingsValue) ? $inventoryChargeSettings->settingsValue : '0.00';

                $shipment->firstReceived = date('Y-m-d h:i:s');
                $shipment->maxStorageDate = $maxStorageDate;
                $shipment->storageCharge = $storageCharge;
                $shipment->save();

                $shipmentDeliveryData->totalItemCost = $totalItemCost;
                $shipmentDeliveryData->inventoryCharge = $inventoryChargeCost;
                $shipmentDeliveryData->totalCost = $inventoryChargeCost;
                $shipmentDeliveryData->maxStorageDate = $maxStorageDate;
                $shipmentDeliveryData->storageCharge = $storageCharge;
                $shipmentDeliveryData->save();

                if (\Session::has('newShipmentDeliveryid')) {
                    /* Get new delivery other charges and update shipment id and delivery id in other charges table */
                    $newDeliveryData = \Session::get('newShipmentDeliveryid');
                    $deliveryId = $newDeliveryData[0];
                    Shipmentothercharges::where("deliveryId", $deliveryId)->update(["shipmentId" => $shipmentId, "deliveryId" => $shipmentDeliveryId]);
                    \Session::forget('newShipmentDeliveryid'); // Delete old random delivery id

                    /* Update added other charges in delivery table */
                    $totalOthercharge = Shipmentothercharges::where("shipmentId", $shipmentId)->where("deliveryId", $shipmentDeliveryId)->sum("otherChargeAmount");
                    Shipmentdelivery::where('id', $shipmentDeliveryId)->increment('otherChargeCost', $totalOthercharge);
                    Shipmentdelivery::where('id', $shipmentDeliveryId)->increment('totalCost', $totalOthercharge);
                }

                $shipmentstauslog = new \App\Model\Shipmentstatuslog;
                $shipmentstauslog->shipmentId = $shipmentId;
                $shipmentstauslog->deliveryId = $shipmentDeliveryId;
                $shipmentstauslog->oldStatus = 'none';
                $shipmentstauslog->status = 'in_warehouse';
                $shipmentstauslog->updatedOn = Config::get('constants.CURRENTDATE');
                $shipmentstauslog->save();

                $shipmentEmailNotification = new \App\Model\Shipmentemailnotification;
                $shipmentEmailNotification->shipmentId = $shipmentId;
                $shipmentEmailNotification->deliveryId = $shipmentDeliveryId;
                $shipmentEmailNotification->action = 'create_shipment';
                $shipmentEmailNotification->save();

                User::sendPushNotification($userId, 'create_shipment_notification', Auth::user()->id);
            }

            return redirect('/administrator/shipments?page=' . $page)->with('successMessage', 'Shipment information saved successfuly.');
        } else {
            return \Redirect::to('administrator/shipments/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    public function savedataOld($id, $page, Request $request) {
        if (\Request::isMethod('post')) {
            //print_r($request->all());
            // exit;
            /* Set received to default "No" and chargeable weight to 0 */
            $received = 'N';
            $chargeableWeight = 0;
            $total = 0;
            /* Fetch User Details */
            $userData = User::where('unit', $request->userUnit)->first();
            $userId = $userData->id;

            /* Fetch User Addressbook Details */
            $addressBookData = User::find($userId)->addressbook()->where('isDefaultShipping', '1')->first();

            /* Fetch Warehouse Details */
            $warehouseData = \App\Model\Warehouse::find($request->warehouseId);

            /* Insert Data into Main Shipment Table */
            $shipment = new Shipment;
            $shipment->userId = $userId;
            $shipment->fromCountry = $warehouseData->countryId;
            $shipment->fromState = $warehouseData->stateId;
            $shipment->fromCity = $warehouseData->cityId;
            $shipment->fromAddress = $warehouseData->address;
            $shipment->fromPhone = $userData->contactNumber;
            $shipment->fromName = $userData->firstName . " " . $userData->lastName;
            $shipment->fromEmail = $userData->email;
            $shipment->toCountry = $addressBookData->countryId;
            $shipment->toState = $addressBookData->stateId;
            $shipment->toCity = $addressBookData->cityId;
            $shipment->toAddress = $addressBookData->address;
            $shipment->toPhone = $addressBookData->phone;
            $shipment->toName = $addressBookData->firstName . " " . $addressBookData->lastName;
            $shipment->toEmail = $addressBookData->email;
            $shipment->warehouseId = $request->warehouseId;
            $shipment->firstReceived = Config::get('constants.CURRENTDATE');
            $shipment->createdBy = Auth::user()->id;
            $shipment->createdByType = 'admin';
            $shipment->createdOn = Config::get('constants.CURRENTDATE');

            $shipment->save();
            $shipmentId = $shipment->id;
            /* Insert Data into Main Shipment Table */

            if (!empty($shipmentId)) {
                /* Insert Data into Warehouse Location Mapping Table */
                $shipmentWarehouse = new Shipmentwarehouselocation;
                $shipmentWarehouse->shipmentId = $shipmentId;
                $shipmentWarehouse->warehouseRowId = $request->warehouseRowId;
                $shipmentWarehouse->warehouseZoneId = $request->warehouseZoneId;
                $shipmentWarehouse->createdBy = Auth::user()->id;
                $shipmentWarehouse->createdByType = 'admin';
                $shipmentWarehouse->createdOn = Config::get('constants.CURRENTDATE');
                $shipmentWarehouse->save();
                /* Insert Data into Warehouse Location Mapping Table */

                /* Insert Data into Delivery Mapping Table */
                $shipmentDelivery = new Shipmentdelivery;
                $chargeableWeightFactor = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();

                $shipmentDelivery->shipmentId = $shipmentId;
                $shipmentDelivery->tracking = $request->trackingNumber;
                $shipmentDelivery->storeId = $request->storeId;
                $shipmentDelivery->deliveryCompanyId = $request->deliveryCompanyId;
                $shipmentDelivery->deliveredOn = \Carbon\Carbon::parse($request->estimateDeliveryDate)->format('Y-m-d');
                $shipmentDelivery->length = $request->length;
                $shipmentDelivery->width = $request->width;
                $shipmentDelivery->height = $request->height;
                $shipmentDelivery->weight = $request->weight;
                $chargeableWeight = ($request->length * $request->height * $shipmentDelivery->width) / $chargeableWeightFactor->settingsValue;
                $shipmentDelivery->chargeableWeight = $chargeableWeight;
                $shipmentDelivery->createdBy = Auth::user()->id;
                $shipmentDelivery->createdByType = 'admin';
                $shipmentDelivery->createdOn = Config::get('constants.CURRENTDATE');

                $shipmentDelivery->save();
                $shipmentDeliveryId = $shipmentDelivery->id;
                /* Insert Data into Delivery Mapping Table */

                /* Insert Data into Shipment Packages Mapping Table */
                if (!empty($shipmentDeliveryId)) {

                    for ($i = 0; $i < count($request->shipment['item']); $i++) {
                        $shipmentPackage = new Shipmentpackage;

                        $shipmentPackage->shipmentId = $shipmentId;

                        $shipmentPackage->itemName = $request->shipment['item'][$i];
                        $shipmentPackage->type = "I";
                        $shipmentPackage->note = $request->shipment['note'][$i];
                        $shipmentPackage->value = $request->shipment['value'][$i];
                        $shipmentPackage->quantity = $request->shipment['quantity'][$i];
                        $shipmentPackage->deliveryId = $shipmentDeliveryId;
                        $shipmentPackage->itemType = $request->shipment['itemType'][$i];
                        $shipmentPackage->received = $request->shipment['received'][$i];
                        if ($request->shipment['received'][$i] == 'Y')
                            $received = 'Y';
                        $total += $request->shipment['total'][$i];

                        $shipmentPackage->save();
                    }
                }
                /* Insert Data into Shipment Packages Mapping Table */


                /* Update Total Amount into Shipment Mapping Table */
                $shipment = Shipment::find($shipmentId);
                $shipment->total = $total;
                if ($received == 'Y') {
                    $maxStorageDate = \App\Model\Generalsettings::where('settingsKey', 'max_storage_days')->first();
                    $chargePerUnit = \App\Model\Generalsettings::where('settingsKey', 'unit_weight_charge')->first();
                    $shipment->firstReceived = date('Y-m-d h:i:s');
                    $shipment->maxStorageDate = date('Y-m-d', strtotime('+' . $maxStorageDate->settingsValue . ' days', time()));

                    $shipment->storageCharge = $chargeableWeight * $chargePerUnit->settingsValue;
                }
                $shipment->save();
            }

            return redirect('/administrator/shipments?page=' . $page)->with('successMessage', 'Shipment information saved successfuly.');
        } else {
            return \Redirect::to('administrator/shipments/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method for user unit verification
     * @return string
     */
    public function verifyuser($unit = '') {
        if (!empty($unit)) {
            $userExist = User::where('unit', $unit)->exists();

            if (!empty($userExist)) {
                $userInfo = User::where('unit', $unit)->first();
                $data['user'] = $userInfo;
                $userWarehousedata = Warehouse::getUserWarehouseList($userInfo->id)->toArray();

                foreach ($userWarehousedata as $userwarehouse) {
                    $warehouseData[$userwarehouse['id']] = $userwarehouse['name'] . "(" . $userwarehouse['cityName'] . ")";
                }
                $data['userWarehouse'] = $warehouseData;
                $data['unpaidShipment'] = Shipment::typeWiseUserShipment($userInfo->id);
                $data['shipmentTypes'] = Shipment::allParticularShipmenttype();
                return view('Administrator.shipments.userverification', $data);
            } else {
                return 0;
            }
        }
    }

    public function getuserwarehouse($unit = '') {
        $return = array();
        $i = 0;
        if (!empty($unit)) {
            $userExist = User::where('unit', $unit)->exists();
            if (!empty($userExist)) {
                $userId = User::where('unit', $unit)->first()->id;
                $userWarehouse = Warehouse::getUserWarehouseList($userId);
                if (!empty($userWarehouse)) {
                    foreach ($userWarehouse as $eachWarehouse) {
                        $return[$i]['id'] = $eachWarehouse->id;
                        $return[$i]['name'] = $eachWarehouse->name;
                        $return[$i]['cityName'] = $eachWarehouse->cityName;
                        $i++;
                    }
                }

                $userSettings = \App\Model\Usershipmentsettings::select('fieldType', 'fieldValue')->where('userId', $userId)->whereNotNull('fieldValue')->get();
            }
        }

        return json_encode(array('data' => $return, 'userSettings' => $userSettings));
    }

    /**
     * Method for fetcing warehouse zone list
     * @param $rowId
     * @return string
     */
    public function getwarehousezonelist($rowId = '') {
        $zoneList = array();
        if (!empty($rowId)) {
            $zoneList = \App\Model\Warehouselocation::where('parentId', $rowId)->where('status', '1')->orderby(DB::raw("ABS('name')"), 'asc')->get();
        }
        echo json_encode($zoneList);
    }

    /**
     * Method used to change status
     * @param integer $contentId
     * @param integer $page
     * @param Request $request
     * @return string
     */
    public function bulkchangestatus($id, $page, Request $request) {
        if (\Request::isMethod('post')) {
            $validator = Validator::make($request->all(), [
                        'shipmentStatus' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            } else {
                $deliveryStatusList = Shipmentdelivery::deliveryStatus();
                $deliveryUpdateStatus = array_keys($deliveryStatusList);
                if (!empty($id)) {
                    $idArr = explode('^', $id);
                    foreach ($idArr as $id) {
                        $shipment = new Shipment;
                        $shipment = Shipment::find($id);
                        $shipment->shipmentStatus = $request->shipmentStatus;
                        $shipment->modifiedBy = Auth::user()->id;
                        $shipment->modifiedOn = Config::get('constants.CURRENTDATE');
                        $shipment->save();
                        $deliveryData = Shipmentdelivery::where('shipmentId', $id)->where('deleted', '0')->get();

                        if (!empty($deliveryData)) {
                            foreach ($deliveryData as $eachDeliveryData) {
                                $isDeliveryReturned = Shipmentpackage::allItemReturned(0, $eachDeliveryData->id);
                                if ($isDeliveryReturned == 0) {
                                    $deliveryOldStatus = \App\Model\Shipmentstatuslog::where('shipmentId', $id)->where('deliveryId', $eachDeliveryData->id)->orderBy('id', 'desc')->first();
                                    $shipmentStatusLog = new \App\Model\Shipmentstatuslog;
                                    $shipmentStatusLog->shipmentId = $id;
                                    $shipmentStatusLog->deliveryId = $eachDeliveryData->id;
                                    $shipmentStatusLog->oldStatus = $deliveryOldStatus->status;
                                    $shipmentStatusLog->status = $deliveryUpdateStatus[$request->shipmentStatus - 1];
                                    $shipmentStatusLog->updatedOn = Config::get('constants.CURRENTDATE');
                                    $shipmentStatusLog->save();
                                }
                            }
                        }

                        $replace = array();
                        $customer = User::find($shipment->userId);
                        $orderInfo = Order::where('shipmentId', $id)->first();
                        $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'shipment_status_change')->first();
                        $statusIndex = $deliveryUpdateStatus[$request->shipmentStatus - 1];
                        $replace['[NAME]'] = $customer->firstName . " " . $customer->lastName;
                        $replace['[SHIPMENTID]'] = $id;
                        $replace['[SHIPMENT_STATUS]'] = $deliveryStatusList[$statusIndex];
                        if (!empty($orderInfo))
                            $replace['[ORDERID]'] = $orderInfo->orderNumber;
                        else
                            $replace['[ORDERID]'] = "";
                        $replace['[WEBSITE_LINK]'] = "<a href='https://shoptomydoor.com/'>shoptomydoor.com</a>";


                        $to = $customer->email;
                        $sendMail = customhelper::SendMail($emailTemplate, $replace, $to);

                        User::sendPushNotification($shipment->userId, 'shipment_status_change', Auth::user()->id, $replace);
                    }
                    return \Redirect::to('administrator/shipments/?page=' . $page)->with('successMessage', 'Shipment status updated successfully.');
                }
            }
        }


        /* SET DATA FOR VIEW  */
        $data['pageTitle'] = "Update Status";
        $data['id'] = $id;
        $data['page'] = $page;
        $idArr = explode('^', $id);
        $data['shipmentStatus'] = Shipment::where('id', $idArr[0])->first()->shipmentStatus;
        $data['shipmentStatusList'] = array(
            1 => 'In warehouse',
            2 => 'In transit',
            3 => 'Customs clearing',
            4 => 'In destination warehouse',
            5 => 'Out for delivery',
            6 => 'Delivered',
        );

        return view('Administrator.shipments.bulkchangestatus', $data);
    }

    /**
     * Method used to change status
     * @param integer $id
     * @param integer $page
     * @param Request $request
     * @return string
     */
    public function changestatus($id, $page, Request $request) {
        // if (\Request::isMethod('post')) {
        //     $shipment = new Shipment;
        //     $validator = Validator::make($request->all(), [
        //                 'shipmentStatus' => 'required',
        //     ]);
        //     if ($validator->fails()) {
        //         return redirect()->back()->withErrors($validator->errors());
        //     } else {
        //         if (!empty($id)) {
        //             $shipment = Shipment::find($id);
        //             $shipment->shipmentStatus = $request->shipmentStatus;
        //             $shipment->modifiedBy = Auth::user()->id;
        //             $shipment->modifiedOn = Config::get('constants.CURRENTDATE');
        //             $shipment->save();
        //             if ($shipment->paymentStatus == 'paid' && $request->shipmentStatus == '6') {
        //                 $order = \App\Model\Order::where('shipmentId', $id)->where('type', 'shipment')->get()->toArray();
        //                 \App\Model\Order::where('id', $order[0]['id'])->update(['status' => "5"]);
        //             }
        //             return \Redirect::to('administrator/shipments/?page=' . $page)->with('successMessage', 'Shipment status updated successfully.');
        //         }
        //     }
        // }


        /* SET DATA FOR VIEW  */
        $data['pageTitle'] = "Update Status";
        $data['id'] = $id;
        $data['page'] = $page;
        $data['shipmentStatus'] = Shipment::where('id', $id)->first()->shipmentStatus;
        // $allShipmentStatus = Shipment::allStatus();
        // $data['shipmentStatusList'] = array(
        //     $nextStatus => $allShipmentStatus[$nextStatus],
        // );

        $data['delivery'] = Shipmentdelivery::where('shipmentId', $id)->get();

        for ($i = 0; $i < count($data['delivery']); $i++) {
            $data['delivery'][$i]['status'] = \App\Model\Shipmentstatuslog::where("shipmentId", $id)->where("deliveryId", $data['delivery'][$i]['id'])->orderBy("id", "desc")->pluck('status')->first();
        }


        return view('Administrator.shipments.changestatus', $data);
    }

    /**
     * Method used to send shipment notifications
     * @param integer $id
     * @param integer $page
     * @param Request $request
     * @return type
     */
    public function schedulenotification($id, $page, Request $request) {
        if (\Request::isMethod('post')) {

            $validator = Validator::make($request->all(), [
                        'scheduleDate' => 'required',
                        'emailtemplateKey' => 'required',
                        'smstemplateKey' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            } else {
                $shipmentId = explode('^', $id);

                foreach ($shipmentId as $shipment) {
                    $shipment = Shipment::find($shipment)->toArray();

                    $schedule = new Schedulenotification;
                    $schedule->referenceId = $shipment['id'];
                    $schedule->customerId = $shipment['userId'];
                    $schedule->emailTemplateId = $request->emailtemplateKey;
                    $schedule->smsTemplateId = $request->smstemplateKey;
                    $schedule->notificationType = 'S';
                    $schedule->scheduleDate = Carbon::parse($request->scheduleDate);
                    $schedule->createdBy = Auth::user()->id;
                    $schedule->createdOn = Config::get('constants.CURRENTDATE');

                    $schedule->save();
                }

                return \Redirect::to('administrator/shipments/?page=' . $page)->with('successMessage', 'Shipment notification scheduled successfully.');
            }
        }
        /* SET DATA FOR VIEW  */
        $data['pageTitle'] = "Schedule Notification";
        $data['id'] = $id;
        $data['page'] = $page;
        $emailTemplateKeys = collect(\App\Model\Emailtemplatekey::where('templateType', 'O')->get());
        $data['templateKeysList'] = $emailTemplateKeys->mapWithKeys(function($item) {
            return [$item['keyname'] => $item['name']];
        });
        $data['emailNotificationList'] = \App\Model\Emailtemplate::where('templateType', 'O')->get();
        $data['smsNotificationList'] = \App\Model\Smstemplate::where('templateType', 'O')->get();

        return view('Administrator.shipments.schedulenotification', $data);
    }

    /**
     * Method used to change address
     * @param integer $id
     * @param integer $page
     * @param Request $request
     * @return string
     */
    public function changeaddress($id, $userid, Request $request) {
        if (\Request::isMethod('post')) {
            $addressBookId = $request->userAddressBookId;
            $shipment = new Shipment;

            if (!empty($addressBookId)) {
                $addressBookData = \App\Model\Addressbook::find($addressBookId);
                $shipment = Shipment::find($id);
                $shipment->toCountry = $addressBookData->countryId;
                $shipment->toState = $addressBookData->stateId;
                $shipment->toCity = $addressBookData->cityId;
                $shipment->toAddress = $addressBookData->address;
                $shipment->toPhone = $addressBookData->phone;
                $shipment->toName = $addressBookData->firstName . " " . $addressBookData->lastName;
                $shipment->toEmail = $addressBookData->email;
                $shipment->save();
            } else {
                $addressBook = new \App\Model\Addressbook;
                $addressBook->userId = $userid;
                $addressBook->title = $request->title;
                $addressBook->firstName = $request->firstName;
                $addressBook->lastName = $request->lastName;
                $addressBook->email = $request->email;
                $addressBook->address = $request->address;
                $addressBook->alternateAddress = $request->alternateAddress;
                $addressBook->cityId = $request->cityId;
                $addressBook->stateId = $request->stateId;
                $addressBook->countryId = $request->countryId;
                $addressBook->zipcode = $request->zipcode;
                $addressBook->phone = $request->phone;
                $addressBook->alternatePhone = $request->alternatePhone;
                $addressBook->modifiedBy = Auth::user()->id;
                $addressBook->modifiedOn = Config::get('constants.CURRENTDATE');
                $addressBook->save();

                $shipment = Shipment::find($id);
                $shipment->toCountry = $request->countryId;
                $shipment->toState = $request->stateId;
                $shipment->toCity = $request->cityId;
                $shipment->toAddress = $request->address;
                $shipment->toPhone = $request->phone;
                $shipment->toName = $request->firstName . " " . $request->lastName;
                $shipment->toEmail = $request->email;
                $shipment->save();
            }

            return 1;
        }


        /* SET DATA FOR VIEW  */
        $data['pageTitle'] = "Change Delivery Address";
        $data['id'] = $id;
        $data['userId'] = $userid;
        $data['addressBookData'] = User::find($userid)->addressbook()->where('deleted', '0')->get(['id', 'firstName', 'lastName']);
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

        return view('Administrator.shipments.changeaddress', $data);
    }

    /**
     * Method used to fetch updated address address
     * @param integer $id
     * @return string
     */
    public function getaddressdetails($id = '') {
        $data['id'] = $id;

        $shipment = Viewshipment::find($id);
        $data['shipment'] = $shipment;

        return view('Administrator.shipments.addressdetails', $data);
    }

    /**
     * Method used to fetch address details
     * @param integer $id
     * @return type
     */
    public function getaddressbookdetails($id = '') {
        $addressBook = \App\Model\Addressbook::find($id)->toArray();
        echo json_encode($addressBook);
    }

    /**
     * Method used to add/edit warehouse location
     * @param integer $id
     * @param integer $shipmentid
     * @param object $request
     * @return html
     */
    public function addeditlocation($id, $shipmentid, Request $request) {
        if (\Request::isMethod('post')) {

            $warehouseLocation = new Shipmentwarehouselocation;

            if (!empty($id)) {
                $warehouseLocation = Shipmentwarehouselocation::find($id);
                $warehouseLocation->modifiedBy = Auth::user()->id;
                $warehouseLocation->modifiedOn = Config::get('constants.CURRENTDATE');
            } else {
                $warehouseLocation->shipmentId = $shipmentid;
                $warehouseLocation->createdBy = Auth::user()->id;
                $warehouseLocation->createdOn = Config::get('constants.CURRENTDATE');
            }
            $warehouseLocation->warehouseRowId = $request->warehouseRowId;
            $warehouseLocation->warehouseZoneId = $request->warehouseZoneId;

            if ($warehouseLocation->save()) {
                return 1;
            } else {
                return 0;
            }
        }

        /* SET DATA FOR VIEW  */
        $data['pageTitle'] = "Edit Warehouse Location";
        $data['id'] = $id;
        $data['shipmentId'] = $shipmentid;

        /* FETCH WAREHOUSE ROW LIST  */
        $data['warehouseRowList'] = \App\Model\Warehouselocation::where('type', 'R')->where('status', '1')->orderby('name', 'asc')->get();

        if (!empty($id)) {
            $warehouseData = Shipmentwarehouselocation::find($id);
            if (!empty($warehouseData->warehouseZoneId))
                $data['warehouseZoneList'] = \App\Model\Warehouselocation::where('parentId', $warehouseData->warehouseRowId)->where('status', '1')->get();

            $data['warehouseData'] = $warehouseData;
        }

        return view('Administrator.shipments.addeditlocation', $data);
    }

    /**
     * Method used to fetch warehouse locations
     * @param integer $id
     * @param integer $page
     * @return html
     */
    public function getwarehouselocations($id, $page = 0) {
        $data['warehouselocation'] = Shipment::getWareHouseLocationList($id);

        /* Fetch Warehouse Location History */
        $id = (int) $id;
        $data['warehouseLocationLog'] = Shipment::getShipmentHistory($id);
        $data['id'] = $id;

        return view('Administrator.shipments.warehouselocations', $data);
    }

    /**
     * Method used to delete warehouse location
     * @param integer $id
     * @param integer $shipmentid
     * @return boolean
     */
    public function deletelocation($id, $shipmentid) {
        if (!empty($id)) {
            $warehouseData = Shipmentwarehouselocation::find($id);
            if ($warehouseData->delete()) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Method used to delete ship ment
     * @param integer $id
     * @param integer $page
     * @return html
     */
    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Shipment::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/shipments/?page=' . $page)->with('successMessage', 'Shipments deleted successfully.');
            } else {
                return \Redirect::to('administrator/shipments/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/shipments/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to delete multiple records
     * @param object $request
     * @return html
     */
    public function deleteall(Request $request) {
        $createrModifierId = Auth::user()->id;
        $checkedval = $request->checkedval;

        if (!empty($checkedval)) {
            $idArray = explode('^', $checkedval);

            foreach ($idArray as $id) {
                Shipment::deleteRecord($id, $createrModifierId);
            }
            return \Redirect::to('administrator/shipments')->with('successMessage', 'Shipments deleted successfully.');
        } else {
            return \Redirect::to('administrator/shipments/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to clear search history
     * @return html
     */
    public function showall() {
        \Session::forget('SHIPMENTDATA');
        return \Redirect::to('administrator/shipments');
    }

    public function getshippingmethods($shipmentId, $page = 1) {
        /* Fetch shipment record */
        $shipmentData = Viewshipment::find($shipmentId)->toArray();
        /* Fetch shipment packages and delivery record */
        $shipmentDetailsData = ViewShipmentDetails::where("shipmentId", $shipmentId)->where('packageType', 'I')->get()->toarray();
        /* Fetch all those shipping methods for which shipping charges exist 
          between source and destination zone */
        $availableShippingMethods = Shippingmethods::getShippingMethods($shipmentData, $shipmentDetailsData);
        $data['shippingMethods'] = $availableShippingMethods;
        $data['shipmentId'] = $shipmentId;
        $data['page'] = $page;
        return view('Administrator.shipments.showshippingmethod', $data);
    }

    public function createorder($shipmentId, $page = 1) {

        $shipmentData = Shipment::find($shipmentId);
        $totalCost = \App\Model\Invoice::where('shipmentId', $shipmentId)->sum('totalBillingAmount');
        $orderData = Order::where('shipmentId', $shipmentId)->where('type', 'shipment')->get();
        if ($orderData->count() > 0) {
            return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/' . $page)->with('errorMessage', 'Order already exist for this shipment');
        } else {
            $orderObj = new Order;
            $orderObj->shipmentId = $shipmentId;
            $orderObj->userId = $shipmentData->userId;
            $orderObj->totalCost = $totalCost;
            $orderObj->status = '2';
            $orderObj->type = 'shipment';
            $orderObj->createdBy = Auth::user()->id;
            $orderObj->save();
            $orderNumber = \App\Model\Order::generateOrderNumber($orderObj->id, $orderObj->userId);
            $orderObj->orderNumber = $orderNumber;
            $orderObj->save();

            return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/' . $page)->with('successMessage', 'Order for shipment created successfully.');
        }
    }

    /**
     * This function is used to insert order records of a shipment with shipping costs
     * @param type $shipmentId
     * @param type $page
     * @param Request $request
     * @return type
     */
    public function createOrderOld($shipmentId, $page = 1, Request $request) {

        /* Fetch shipment record */
        $shipmentData = Viewshipment::find($shipmentId)->toArray();
        /* Fetch shipment packages and delivery record */
        $shipmentDetailsData = ViewShipmentDetails::where("shipmentId", $shipmentId)->where('packageType', 'I')->get()->toarray();
        $warehouseDetails = Warehouse::find($shipmentData['warehouseId']);
        $data['warehouseDetails'] = $warehouseDetails;

        $data['shipmentData'] = $shipmentData;
        $data['billingAddr'] = Addressbook::where('userId', $shipmentData['userId'])->where('isDefaultBilling', '1')->first();
        $data['shippingAddr'] = Addressbook::where('userId', $shipmentData['userId'])->where('isDefaultShipping', '1')->first();
        $data['deliveryPackageData'] = Shipment::getDeliveryDetails($shipmentDetailsData);
        $data['allOrderStatus'] = Order::allOrderStatus();
        $data['userDetails'] = User::find($shipmentData['userId'])->toArray();
        //return view('Administrator.shipments.invoice',$data);
        /* $fileName = "Invoice_".time().".pdf";               
          PDF::loadView('Administrator.shipments.invoice',$data)->save(public_path('exports/invoice/'.$fileName))->stream('download.pdf');exit; */
        //dd($data['orderData']);

        /* Fetch payment method for tax calculation */
        //$paymentMethod = Paymentmethod::where('status', '1')->orderBy('orderby', 'asc')->first();
        $paymentMethod = array();
        /* Set payment method for tax calculation */
        if (count($paymentMethod) > 0)
            $paymentMethodId = $paymentMethod->id;
        else
            $paymentMethodId = '-1';

        $shippingId = '-1';
        if ($request->input('shippingMethod') != '')
            $shippingId = $request->input('shippingMethod');

        $orderObj = new Order;
        /* check if order record already exists or not */
        if ($request->input('orderId') == '') {
            /* Fetch shipment method wise shipping charges */
            $getShippingMethodsCharges = Shippingmethods::getShippingMethodsAndCharges($shipmentData, $shipmentDetailsData, $paymentMethodId, '', $shippingId);
            $orderObj->userId = $shipmentData['userId'];
            $orderObj->date = date('Y-m-d h:i:s');
            $orderObj->status = '1';
            $orderObj->flag = 'N';
            $orderObj->shipmentId = $shipmentId;
            $orderObj->taxExempt = 'N';
            $orderObj->paymentId = $paymentMethodId;
        } else {

            $paymentMethod = $request->input('paymentMethod');
            $orderObj = $orderObj->find($request->input('orderId'));
            /* Fetch shipment method wise shipping charges */
            $getShippingMethodsCharges = Shippingmethods::getShippingMethodsAndCharges($shipmentData, $shipmentDetailsData, $paymentMethod, '', $shippingId);
            $orderObj->paymentId = $request->input('paymentMethod');
            $orderObj->status = $request->input('orderStatus');
        }

        if (!empty($getShippingMethodsCharges[0])) {
            /* If shipping charges exist set charges values to order fields for insert or update */
            $orderObj->shippingId = $getShippingMethodsCharges[0]['shippingId'];
            $orderObj->tsDays = strtotime('+' . $getShippingMethodsCharges[0]['days'] . ' day');
            $orderObj->shippingCost = $getShippingMethodsCharges[0]['shippingCost'];
            $orderObj->subtotal = $getShippingMethodsCharges[0]['shippingCost'];
            $orderObj->paymentSurcharge = '0.00';
            $orderObj->duty = $getShippingMethodsCharges[0]['duty'];
            $orderObj->clearing = $getShippingMethodsCharges[0]['clearing'];
            $orderObj->otherCharges = $getShippingMethodsCharges[0]['otherCharges'];
            $orderObj->totalInsurance = $getShippingMethodsCharges[0]['insurance'];
            $orderObj->packagingInventory = $getShippingMethodsCharges[0]['packagingInventory'];
            $orderObj->totalExtra = 0;
            $orderObj->codeDiscount = '';
            $orderObj->tax = $getShippingMethodsCharges[0]['tax'];
            $orderObj->taxesApplied = '';
            $orderObj->total = $getShippingMethodsCharges[0]['total'];
            //echo $orderObj->shippingId;exit;
            if ($orderObj->save()) {
                $orderId = $orderObj->id;
                $data['orderData'] = Order::find($orderId)->toArray();

                //return view('Administrator.shipments.invoice',$data); 
                $to = $shipmentData['customerEmail'];
                $fileName = "Invoice_" . time() . ".pdf";
                PDF::loadView('Administrator.shipments.invoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                $to = 'somnath.pahari@indusnet.co.in';
                Mail::send(['html' => 'mail'], ['content' => 'Test invoice'], function ($message) use($shipmentData, $to, $fileName) {
                    $message->from('contact@shoptomydoor.com', 'Shoptomydoor');
                    $message->subject('Shipment Invoice');
                    $message->to($to);
                    $message->attach(public_path('exports/invoice/' . $fileName));
                });
                return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/' . $page)->with('successMessage', 'Order for shipment created successfully.');
            } else
                return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/' . $page)->with('errorMessage', 'Error in operation!');
        } else
            return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/' . $page)->with('errorMessage', 'No shipping charges set for source and destination zone');
    }

    /**
     * This function is used to open popup when startpackaging is clicked
     * @param type $shipmentId
     * @param type $page
     * @return type
     */
    public function validatepackage($shipmentId, $page) {

        $deliveryCount = 0;
        $deliveryNum = 1;
        $deliveryTobeScaned = array();

        $deliveryData = Shipmentdelivery::where('shipmentId', $shipmentId)->where('deleted', '0')->get();
        if (!empty($deliveryData)) {
            foreach ($deliveryData as $eachDeliveryData) {
                
                $isDeliveryReturned = Shipmentpackage::allItemReturned(0, $eachDeliveryData->id);
                
                if ($isDeliveryReturned != 1) {
                    $isItemDeleted = Shipmentpackage::allItemDeleted(0, $eachDeliveryData->id);

                    if($isItemDeleted != 1) {
                        $deliveryCount++;
                        $deliveryTobeScaned[] = "D" . $deliveryNum;
                    }
                }

                $deliveryNum++;
            }
        }
        $data['shipmentId'] = $shipmentId;
        $data['deliveryCount'] = $deliveryCount;
        $data['deliveryTobeScaned'] = (!empty($deliveryTobeScaned) ? implode('^', $deliveryTobeScaned) : "");

        return view('Administrator.shipments.validatepackage', $data);
    }

    /**
     * This function is used to enable packaging if all condition matches
     * @param <int> $shipmentId
     * @param <int> $page
     * @param Request $request
     * @return type
     */
    public function startpackaging($shipmentId, $page, Request $request) {

        $packageShipment = $request->input('packageShipment');
        if ($packageShipment == $shipmentId) {

            $dataObj = Shipment::where('id', $shipmentId)->update(array('allowedPackaging' => 'Y', 'packed' => 'started'));
            return json_encode(array('message' => 'success'));
        } else
            return json_encode(array('message' => 'error'));
    }

    /**
     * This is used to Add new packaging records
     * @param <int> $shipmentId
     * @param <int> $page
     * @param Request $request
     * @return type
     */
    public function addpackage($shipmentId, $page, Request $request) {

        $validator = Validator::make($request->all(), [
                    'length' => 'required|numeric',
                    'width' => 'required|numeric',
                    'height' => 'required|numeric',
                    'weight' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            $shipmentPackageObj = New \App\Model\Shipmentspackaging;
            $chargeableWeightFactor = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();

            $shipmentPackageObj->length = $request->input('length');
            $shipmentPackageObj->width = $request->input('width');
            $shipmentPackageObj->height = $request->input('height');
            $shipmentPackageObj->weight = $request->input('weight');
            $shipmentPackageObj->shipmentId = $shipmentId;
            $shipmentPackageObj->chargeableWeight = max($shipmentPackageObj->weight, (($shipmentPackageObj->length * $shipmentPackageObj->width * $shipmentPackageObj->height) / $chargeableWeightFactor->settingsValue));
            $shipmentPackageObj->createdBy = Auth::user()->id;
            if ($shipmentPackageObj->save())
                return json_encode(array('message' => 'success'));
            else
                return json_encode(array('message' => 'error'));
            /* if ($shipmentPackageObj->save())
              return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/' . $page)->with('successMessage', 'Packaging data saved.');
              else
              return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/' . $page)->with('errorMessage', 'Something went wrong.'); */
        }
    }

    /**
     * This function is used to update or delete packaging records
     * @param <int> $shipmentId
     * @param <int> $page
     * @param Request $request
     * @return type
     */
    public function editpackage($shipmentId, $page, Request $request) {

        $updateArr = $request->input('update');
        /* Chargeable weight factor from settings */
        $chargeableWeightFactor = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();
        $recordUpdate = 0;
        foreach ($updateArr as $eachId => $eachArr) {

            if (!empty($eachArr['cb'])) {
                $shipmentPackageObj = New \App\Model\Shipmentspackaging;
                $shipmentPackageObj = $shipmentPackageObj->find($eachId);

                if ($request->input('action') == 'update') {
                    $shipmentPackageObj->length = $eachArr['length'];
                    $shipmentPackageObj->width = $eachArr['width'];
                    $shipmentPackageObj->height = $eachArr['height'];
                    $shipmentPackageObj->weight = $eachArr['weight'];
                    $shipmentPackageObj->chargeableWeight = max($shipmentPackageObj->weight, (($shipmentPackageObj->length * $shipmentPackageObj->width * $shipmentPackageObj->height) / $chargeableWeightFactor->settingsValue));

                    if ($shipmentPackageObj->save())
                        $recordUpdate ++;
                }
                else if ($request->input('action') == 'delete') {
                    if ($shipmentPackageObj->delete())
                        $recordUpdate ++;
                }
            }
        }
        if ($request->input('fromSource') == 'ajax') {
            if ($recordUpdate > 0)
                return json_encode(array('message' => 'success'));
            else
                return json_encode(array('message' => 'error'));
        }
        else {
            if ($recordUpdate > 0)
                return redirect()->back()->with('successMessage', 'Record updated/deleted successfully');

            return redirect()->back()->with('errorMessage', 'Please select any record to update/delete');
        }
    }

    /**
     * This function is used to add packaging comments
     * @param <int> $shipmentId
     * @param <int> $page
     * @param Request $request
     * @return type
     */
    public function addpackagedetails($shipmentId, $page, Request $request) {

        if ($request->input('packageDetails') != '') {
            $dataObj = new Packagedetails;
            $dataObj->shipmentId = $shipmentId;
            $dataObj->message = $request->input('packageDetails');
            $dataObj->user = Auth::user()->email;
            //$dataObj->user = Auth::user()->email;

            $dataObj->save();
            return redirect()->back()->with('successMessage', 'Record added successfully');
        }
        return redirect()->back()->with('errorMessage', 'Please enter package details');
    }

    /**
     * This function is used to print manifest
     * @param <int> $shipmentId
     * @param <int> $page
     * @return type
     */
    public function printmanifest($shipmentId, $page) {

        $data = array();
        /* Fetch shipemnt record */
        $shipmentData = Shipment::find($shipmentId);

        $data['shipmentData'] = $shipmentData;
        /* Fetch  Shipment Delivery Data */
        $deliveryData = ViewShipmentDetails::where("shipmentId", $shipmentId)->where('deleted', '0')->where('packageType', 'I')->get()->toarray();
        $data['delivery'] = Shipment::getDeliveryDetails($deliveryData);

        $deliveryAddress = array();
        if ($shipmentData->paymentStatus == 'paid') {
            $paidInvoice = \App\Model\Invoice::where('shipmentId', $shipmentId)->where('type', $shipmentData->shipmentType)->where("type", $shipmentData->shipmentType)->first();
            if (!empty($paidInvoice)) {
                $invoiceData = json_decode($paidInvoice->invoiceParticulars, true);
                if (!empty($invoiceData['shippingaddress'])) {
                    $shippingAddress = $invoiceData['shippingaddress'];
                    $deliveryAddress['toName'] = $shippingAddress['toName'];
                    $deliveryAddress['toCountryName'] = \App\Helpers\customhelper::getCountryStateCityName($shippingAddress['toCountry'], 'country');
                    $deliveryAddress['toStateName'] = \App\Helpers\customhelper::getCountryStateCityName($shippingAddress['toState'], 'state');
                    $deliveryAddress['toCityName'] = \App\Helpers\customhelper::getCountryStateCityName($shippingAddress['toCity'], 'city');
                    $deliveryAddress['toAddress'] = $shippingAddress['toAddress'];
                    $deliveryAddress['toEmail'] = $shippingAddress['toEmail'];
                    $deliveryAddress['toPhone'] = $shippingAddress['toPhone'];
                    $deliveryAddress['toZipCode'] = (!empty($shippingAddress['toZipCode']) ? $shippingAddress['toZipCode'] : "");
                    $deliveryAddress['fromCityName'] = \App\Helpers\customhelper::getCountryStateCityName($invoiceData['warehouse']['fromCity'], 'city');
                    $deliveryAddress['fromStateName'] = \App\Helpers\customhelper::getCountryStateCityName($invoiceData['warehouse']['fromCountry'], 'country');
                    $deliveryAddress['fromCountryName'] = \App\Helpers\customhelper::getCountryStateCityName($invoiceData['warehouse']['fromState'], 'state');
                }
            }
        }

        if (empty($deliveryAddress)) {
            $deliveryAddress['toName'] = $shipmentData->toName;
            $deliveryAddress['toCountryName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->toCountry, 'country');
            $deliveryAddress['toStateName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->toState, 'state');
            $deliveryAddress['toCityName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->toCity, 'city');
            $deliveryAddress['toAddress'] = $shipmentData->toAddress;
            $deliveryAddress['toEmail'] = $shipmentData->toEmail;
            $deliveryAddress['toPhone'] = $shipmentData->toPhone;
            $deliveryAddress['toZipCode'] = $shipmentData->toZipCode;
            $deliveryAddress['fromCityName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->fromCity, 'city');
            $deliveryAddress['fromStateName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->fromCountry, 'country');
            $deliveryAddress['fromCountryName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->fromState, 'state');
        }
        //print_r($data['delivery']);exit;
        /* Fetch packaging records */
        $packageDetailsData = \App\Model\Shipmentspackaging::where("shipmentId", $shipmentId)->get();
        $data['packageDetailsData'] = $packageDetailsData;
        $data['packageCount'] = $packageDetailsData->count();
        if ($packageDetailsData->count() > 0) {
            $data['orderId'] = Order::where("shipmentId", $shipmentId)->first()->orderNumber;
        }
        $data['deliveryAddress'] = $deliveryAddress;


        return view('Administrator.shipments.manifest', $data);
    }

    public function notifycustomer($messageId, $shipmnentId) {
        $data = array();

        $data['messageId'] = $messageId;

        $data['shipmentId'] = $shipmnentId;

        $data['warehousemsg'] = Warehousemessage::select('message')->where('id', $messageId)->get()->toArray();
        return view('Administrator.shipments.notifycustomer', $data);
    }

    public function savenotification($messageId, $shipmnentId, Request $request) {
        $data = array();

        $warehouseMessage = new Shipmentwarehousemessage;

        $warehouseMsg = Warehousemessage::find($messageId);

        $warehouseMessage->shipmentId = $shipmnentId;
        $warehouseMessage->messageId = $messageId;
        $warehouseMessage->sentBy = Auth::user()->id; //
        $warehouseMessage->sentOn = Config::get('constants.CURRENTDATE');


        /* ++++++++++ email functionality ++++++++ */
        $userId = Shipment::find($shipmnentId);
        $customer = User::find($userId->userId);

        $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'notification_admin')->first();
        $replace['[NAME]'] = $customer->firstName . " " . $customer->lastName;
        $replace['[NOTIFICATION]'] = nl2br($request->message);

        $to = $customer->email;
        $sendMail = customhelper::SendMail($emailTemplate, $replace, $to);

        $replace['[TOPIC]'] = $warehouseMsg->name;
        User::sendPushNotification($userId->userId, 'warehouse_message_notification', Auth::user()->id, $replace);
        /* ++++++++++ end of email functionality ++++ */


        if ($sendMail) {
            if ($warehouseMessage->save()) {
                return redirect()->back()->with('successMessage', 'Notification sent successfully');
            } else {
                return redirect()->back()->with('errorMessage', 'Record not added');
            }
        } else {
            return redirect()->back()->with('errorMessage', 'Notification not sent');
        }
    }

    public function addcomment($shipmentId, Request $request) {
        $data = array();

        $message = $request->comment;

        $shipmentnotes = new \App\Model\Shipmentwarehousenotes;

        $shipmentnotes->shipmentId = $shipmentId;
        $shipmentnotes->message = $message;
        $shipmentnotes->type = "S";
        $shipmentnotes->sentBy = Auth::user()->id;
        $shipmentnotes->sentOn = Config::get('constants.CURRENTDATE');

        $userdetail = \App\Model\UserAdmin::select(array('firstname', 'lastname', 'email'))->where('id', Auth::user()->id)->first()->toArray();


        if ($shipmentnotes->save()) {
            $userdetail = array_merge($userdetail, array('sentOn' => date('Y-m-d', strtotime($shipmentnotes->sentOn)), 'noteId' => $shipmentnotes->id, 'shipmentId' => $shipmentId));
            return json_encode($userdetail);
        } else {
            return 0;
        }
    }

    public function sendcomment($id, $shipmentId) {
        $data = array();

        $data['shipmentId'] = $shipmentId;

        $data['customerEmailList'] = \App\Model\UserAdmin::select(array('id', 'email'))->where(array('deleted' => '0', 'status' => '1'))->get()->toArray();

        return view('Administrator.shipments.sendcomment', $data);
    }

    public function saveandnotify($shipmentId, Request $request) {

        $shipmentnotes = new \App\Model\Shipmentwarehousenotes;

        $emailto[] = $request->admin;



        $shipmentnotes->shipmentId = $shipmentId;
        $shipmentnotes->message = $request->warehousemsg;
        $shipmentnotes->type = "S";
        $shipmentnotes->sentBy = Auth::user()->id;
        $shipmentnotes->sentOn = Config::get('constants.CURRENTDATE');

        $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'notification_admin')->first();
        $replace['[NAME]'] = "Admin";
        $replace['[NOTIFICATION]'] = $request->warehousemsg;

        foreach ($emailto[0] as $email) {
            $sendMail = customhelper::SendMail($emailTemplate, $replace, $email);
        }
        if ($sendMail) {
            if ($shipmentnotes->save()) {

                return redirect()->back()->with('successMessage', 'Notes saved and sent successfully');
            } else {

                return redirect()->back()->with('errorMessage', 'Record not added');
            }
        } else {
            return redirect()->back()->with('errorMessage', 'Notes not sent');
        }
    }

    public function getshippinglabelfields($shipmentId, $page) {

        $data['shipmentId'] = $shipmentId;
        $data['page'] = $page;
        return view('Administrator.shipments.shippinglabelfields', $data);
    }

    /**
     * This function is used to print shipping labels
     * @param <int>  $shipmentId
     * @param <int> $page
     * @param Request $request
     * @return type
     */
    public function printshippinglabel($shipmentId, $page, Request $request) {

        $data = array();
        /* Fetch shipment record */
        $shipmentData = Shipment::find($shipmentId);
        $userData = User::find($shipmentData->userId);
        $deliveryShippingMethod = array();

        $data['shipmentData'] = $shipmentData;
        $data['userData'] = $userData;
        /* Fetch  Shipment Delivery Data */
        $deliveryData = Shipmentdelivery::where("shipmentId", $shipmentId)->where('deleted', '0')->get();
        //$data['delivery'] = Shipment::getDeliveryDetails($deliveryData);
        $shippingMethodIndex = 1;
        foreach ($deliveryData as $eachDeliveryData) {
            $allItemReturned = Shipmentpackage::allItemReturned(0, $eachDeliveryData['id']);
            if ($allItemReturned == 1)
                continue;
            $shippingMethodDetails = Shippingmethods::select('shipping', 'companyLogo')->where('shippingid', $eachDeliveryData['shippingMethodId'])->first();
            if (!empty($shippingMethodDetails)) {
                $deliveryShippingMethod[$shippingMethodIndex]['name'] = $shippingMethodDetails->shipping;
                $deliveryShippingMethod[$shippingMethodIndex]['logo'] = $shippingMethodDetails->companyLogo;
                $shippingMethodIndex++;
            }
        }

        $deliveryAddress = array();
        if ($shipmentData->paymentStatus == 'paid') {
            $paidInvoice = \App\Model\Invoice::where('shipmentId', $shipmentId)->where('type', $shipmentData->shipmentType)->where("type", $shipmentData->shipmentType)->first();
            if (!empty($paidInvoice)) {
                $invoiceData = json_decode($paidInvoice->invoiceParticulars, true);
                if (!empty($invoiceData['shippingaddress'])) {
                    $shippingAddress = $invoiceData['shippingaddress'];
                    $deliveryAddress['toName'] = $shippingAddress['toName'];
                    $deliveryAddress['toCountryName'] = \App\Helpers\customhelper::getCountryStateCityName($shippingAddress['toCountry'], 'country');
                    $deliveryAddress['toStateName'] = \App\Helpers\customhelper::getCountryStateCityName($shippingAddress['toState'], 'state');
                    $deliveryAddress['toCityName'] = \App\Helpers\customhelper::getCountryStateCityName($shippingAddress['toCity'], 'city');
                    $deliveryAddress['toAddress'] = $shippingAddress['toAddress'];
                    $deliveryAddress['toEmail'] = $shippingAddress['toEmail'];
                    $deliveryAddress['toPhone'] = $shippingAddress['toPhone'];
                    $deliveryAddress['toZipCode'] = (!empty($shippingAddress['toZipCode']) ? $shippingAddress['toZipCode'] : "");
                }
            }
        }

        if (empty($deliveryAddress)) {
            $deliveryAddress['toName'] = $shipmentData->toName;
            $deliveryAddress['toCountryName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->toCountry, 'country');
            $deliveryAddress['toStateName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->toState, 'state');
            $deliveryAddress['toCityName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->toCity, 'city');
            $deliveryAddress['toAddress'] = $shipmentData->toAddress;
            $deliveryAddress['toEmail'] = $shipmentData->toEmail;
            $deliveryAddress['toPhone'] = $shipmentData->toPhone;
            $deliveryAddress['toZipCode'] = $shipmentData->toZipCode;
        }

        $deliveryAddress['fromCityName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->fromCity, 'city');
        $deliveryAddress['fromStateName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->fromCountry, 'country');
        $deliveryAddress['fromCountryName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->fromState, 'state');

        /* Fetch packaging records */
        $packageDetailsData = \App\Model\Shipmentspackaging::where("shipmentId", $shipmentId)->get();
        $data['orderId'] = Order::where("shipmentId", $shipmentId)->first()->orderNumber;
        $data['packageDetailsData'] = $packageDetailsData;
        $data['packageCount'] = $packageDetailsData->count();
        $data['deliveryShippingMethod'] = $deliveryShippingMethod;
        $data['deliveryAddress'] = $deliveryAddress;



        #dd($data['delivery']['deliveries'][10]['id']);

        $data['contents'] = $request->input('contents');
        $data['type'] = $request->input('type');
        \App\Model\Shipmentlabelprintlog::logEntry($shipmentId, 'Shipping Label');
        return view('Administrator.shipments.shippinglabel', $data);
    }

    /**
     * Method used to print Red Star label Pop up
     * @param integer $shipmentId
     * @param integer $page 
     * @return html
     */
    public function redstarlabel($shipmentId, $page) {

        $data = array();
        $shipmentData = Shipment::find($shipmentId);
        $userData = User::find($shipmentData->userId);

        $data['shipmentData'] = $shipmentData;
        $data['userData'] = $userData;

        $shipmentItems = Shipmentpackage::where('shipmentId', $shipmentId)->where("itemReturn", "0")->where("deleted", "0")->get();
        $data['noOfItem'] = $shipmentItems->count();

        $deliveryAddress = array();
        if ($shipmentData->paymentStatus == 'paid') {
            $paidInvoice = \App\Model\Invoice::where('shipmentId', $shipmentId)->where('type', $shipmentData->shipmentType)->where("type", $shipmentData->shipmentType)->first();
            if (!empty($paidInvoice)) {
                $invoiceData = json_decode($paidInvoice->invoiceParticulars, true);
                if (!empty($invoiceData['shippingaddress'])) {
                    $shippingAddress = $invoiceData['shippingaddress'];
                    $deliveryAddress['toName'] = $shippingAddress['toName'];
                    $deliveryAddress['toCountryName'] = \App\Helpers\customhelper::getCountryStateCityName($shippingAddress['toCountry'], 'country');
                    $deliveryAddress['toStateName'] = \App\Helpers\customhelper::getCountryStateCityName($shippingAddress['toState'], 'state');
                    $deliveryAddress['toCityName'] = \App\Helpers\customhelper::getCountryStateCityName($shippingAddress['toCity'], 'city');
                    $deliveryAddress['toAddress'] = $shippingAddress['toAddress'];
                    $deliveryAddress['toEmail'] = $shippingAddress['toEmail'];
                    $deliveryAddress['toPhone'] = $shippingAddress['toPhone'];
                    $deliveryAddress['toZipCode'] = (!empty($shippingAddress['toZipCode']) ? $shippingAddress['toZipCode'] : "");
                }
            }
        }

        if (empty($deliveryAddress)) {
            $deliveryAddress['toName'] = $shipmentData->toName;
            $deliveryAddress['toCountryName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->toCountry, 'country');
            $deliveryAddress['toStateName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->toState, 'state');
            $deliveryAddress['toCityName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->toCity, 'city');
            $deliveryAddress['toAddress'] = $shipmentData->toAddress;
            $deliveryAddress['toEmail'] = $shipmentData->toEmail;
            $deliveryAddress['toPhone'] = $shipmentData->toPhone;
            $deliveryAddress['toZipCode'] = $shipmentData->toZipCode;
        }

        $packageDetailsData = \App\Model\Shipmentspackaging::where("shipmentId", $shipmentId)->get();
        $data['packageDetailsData'] = $packageDetailsData;
        $data['packageCount'] = $packageDetailsData->count();
        $data['deliveryAddress'] = $deliveryAddress;

        /* GET ORDER ID FROM SHIPMENT ID */
        $orderId = Order::where("shipmentId", $shipmentId)->get();
        $data['orderId'] = $orderId[0]->orderNumber;
        \App\Model\Shipmentlabelprintlog::logEntry($shipmentId, 'Red Star Label');
        return view('Administrator.shipments.redstarlabel', $data);
    }

    /**
     * Method used to print Nations Delivery pop up Pop up
     * @param integer $shipmentId
     * @param integer $page 
     * @return html
     */
    public function nationsdelivery($shipmentId, $page) {

        $data = array();
        $shipmentData = Shipment::find($shipmentId);
        $userData = User::find($shipmentData->userId);

        $data['shipmentData'] = $shipmentData;
        $data['userData'] = $userData;

        $shipmentItems = Shipmentpackage::where('shipmentId', $shipmentId)->where("itemReturn", "0")->where("deleted", "0")->get();
        $data['noOfItem'] = $shipmentItems->count();

        $packageDetailsData = \App\Model\Shipmentspackaging::where("shipmentId", $shipmentId)->get();
        $data['packageDetailsData'] = $packageDetailsData;
        $data['packageCount'] = $packageDetailsData->count();
        $deliveryAddress = array();
        if ($shipmentData->paymentStatus == 'paid') {
            $paidInvoice = \App\Model\Invoice::where('shipmentId', $shipmentId)->where('type', $shipmentData->shipmentType)->where("type", $shipmentData->shipmentType)->first();
            if (!empty($paidInvoice)) {
                $invoiceData = json_decode($paidInvoice->invoiceParticulars, true);
                if (!empty($invoiceData['shippingaddress'])) {
                    $shippingAddress = $invoiceData['shippingaddress'];
                    $deliveryAddress['toName'] = $shippingAddress['toName'];
                    $deliveryAddress['toCountryName'] = \App\Helpers\customhelper::getCountryStateCityName($shippingAddress['toCountry'], 'country');
                    $deliveryAddress['toStateName'] = \App\Helpers\customhelper::getCountryStateCityName($shippingAddress['toState'], 'state');
                    $deliveryAddress['toCityName'] = \App\Helpers\customhelper::getCountryStateCityName($shippingAddress['toCity'], 'city');
                    $deliveryAddress['toAddress'] = $shippingAddress['toAddress'];
                    $deliveryAddress['toEmail'] = $shippingAddress['toEmail'];
                    $deliveryAddress['toPhone'] = $shippingAddress['toPhone'];
                    $deliveryAddress['toZipCode'] = (!empty($shippingAddress['toZipCode']) ? $shippingAddress['toZipCode'] : "");
                }
            }
        }

        if (empty($deliveryAddress)) {
            $deliveryAddress['toName'] = $shipmentData->toName;
            $deliveryAddress['toCountryName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->toCountry, 'country');
            $deliveryAddress['toStateName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->toState, 'state');
            $deliveryAddress['toCityName'] = \App\Helpers\customhelper::getCountryStateCityName($shipmentData->toCity, 'city');
            $deliveryAddress['toAddress'] = $shipmentData->toAddress;
            $deliveryAddress['toEmail'] = $shipmentData->toEmail;
            $deliveryAddress['toPhone'] = $shipmentData->toPhone;
            $deliveryAddress['toZipCode'] = $shipmentData->toZipCode;
        }
        #dd($data['delivery']['deliveries'][10]['id']);

        /* GET ORDER ID FROM SHIPMENT ID */
        $orderId = Order::where("shipmentId", $shipmentId)->get();
        $data['orderId'] = $orderId[0]->orderNumber;
        $data['deliveryAddress'] = $deliveryAddress;
        \App\Model\Shipmentlabelprintlog::logEntry($shipmentId, 'Nations Delivery Label');
        return view('Administrator.shipments.nationsdelivery', $data);
    }

    /**
     * Method used to print location label
     * @param integer $id
     * @param integer $page 
     * @return html
     */
    public function printlocationlabel($id, $page = 0) {
        $data['pageTitle'] = "Print Location Label";

        $data['warehouselocation'] = Shipment::getWareHouseLocationList($id);
        /* Fetch shipment types */
        $data['shipmentTypeList'] = Shipment::allParticularShipmenttype();
        $data['shipment'] = Shipment::where('id', $id)->first(['id', 'userId','partucilarShipmentType']);
        $data['unit'] = User::find($data['shipment']->userId)->unit;

        return view('Administrator.shipments.locationlabel', $data);
    }

    /**
     * Method to update shipment delivery details
     * @param object $request 
     * @return boolean   
     */
    public function updatedelivery($shipmentId, $deliveryId, Request $request) {
        if (\Request::isMethod('post')) {
            $shipment = Shipment::find($shipmentId);

            $chargeableWeight = 0;
            $deliveryChargeableWeight = 0;
            $chargeableWeightFactor = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();
            $shipmentDelivery = new Shipmentdelivery;
            $shipmentDelivery = ShipmentDelivery::find($deliveryId);

            if ($shipment->shipmentType == 'shopforme' || $shipment->shipmentType == 'autopart') {
                //$shipmentDelivery->shipmentId = $shipmentId;
                //$shipmentDelivery->deliveryCompanyId = $request->companyId;
                //$shipmentDelivery->tracking = $request->trackingId;
                //$shipmentDelivery->tracking2 = $request->trackingId2;
                //$shipmentDelivery->deliveredOn = $request->deliveredOn;
                $shipmentDelivery->weight = $request->deliveryWeight;
                $shipmentDelivery->chargeableWeight = $request->deliveryWeight;
            } else {
                //$shipmentDelivery->shipmentId = $shipmentId;
                //$shipmentDelivery->storeId = $request->storeId;
                //$shipmentDelivery->deliveryCompanyId = $request->companyId;
                //$shipmentDelivery->tracking = $request->trackingId;
                //$shipmentDelivery->tracking2 = $request->trackingId2;
                $shipmentDelivery->length = $request->deliveryLength;
                $shipmentDelivery->width = $request->deliveryWidth;
                $shipmentDelivery->height = $request->deliveryHeight;
                $shipmentDelivery->weight = $request->deliveryWeight;
                $deliveryChargeableWeight = max($request->deliveryWeight, ($request->deliveryLength * $request->deliveryHeight * $request->deliveryWidth) / $chargeableWeightFactor->settingsValue);
                $shipmentDelivery->chargeableWeight = round($deliveryChargeableWeight, 2);
            }
            $chargePerUnitSettings = \App\Model\Generalsettings::where('settingsKey', 'unit_weight_charge')->first();
            $shipmentDelivery->storageCharge = round(($shipmentDelivery->chargeableWeight * $chargePerUnitSettings->settingsValue), 2);
            $shipmentDelivery->save();
            return 1;
        }
    }

    /**
     * Method used to fetch & display delivery package details
     * @param integer $id
     * @return string
     */
    public function getpackagedetails($id) {
        /* FETCH CATEGORY LIST  */
        $data['categoryList'] = \App\Model\Sitecategory::getCategoryList('parent');

        /* Fetch  Shipment Delivery Data */
        $deliveryData = ViewShipmentDetails::where("deliveryId", $id)->where('deleted', '0')->where('packageDeleted', '0')->where('packageType', 'I')->get()->toarray();

        $shipmentType = Viewshipment::select('shipmentType')->where("id", $id)->get();

        $data['shipment'] = Shipment::getDeliveryDetails($deliveryData);
        /* FETCH STORE LIST  */
        $data['storeList'] = \App\Model\Stores::where('deleted', '0')->where('status', '1')->orderby('storeName', 'asc')->get();
        $data['id'] = $id;

        if ($shipmentType == 'shopforme')
            return view('Administrator.shipments.shopformedetails', $data);
        else
            return view('Administrator.shipments.packagedetails', $data);
    }

    /**
     * Method used to fetch & display the shipment details
     * @param integer $id
     * @return string
     */
    public function getshipmentdetails($id) {
        /* Fetch  Shipment Delivery Data */
        $packageData = ViewShipmentDetails::where("shipmentId", $id)->where('deleted', '0')->where('packageDeleted', '0')->where('packageType', 'I')->get()->toarray();

        $data['shipment'] = Shipment::getDeliveryDetails($packageData);
        $data['id'] = $id;

        return view('Administrator.shipments.shipmentdetails', $data);
    }

    /**
     * Method used to fetch & display shipment notes
     * @param integer $notesId
     * @param integer $shipmentId
     * @return string
     */
    public function shownotes($notesId, $shipmentId) {
        $data = array();

        $data['shipmentId'] = $shipmentId;

        $data['notes'] = \App\Model\Shipmentwarehousenotes::select('message')->where('id', $notesId)->where('type', 'S')->get()->toArray();

        return view('Administrator.shipments.shownotes', $data);
    }

    /**
     * Method used to print delivery labels
     * @param integer $id
     * @param integer $deliveryId 
     * @return html
     */
    public function printdeliverylabel($id, $deliveryId, $delivenum = 0) {
        /* FETCH SHIPMENT PACKAGES DETAILS */
        $data['delivery']['pacakges'] = Shipmentpackage::where('deliveryId', $deliveryId)->where('deleted', '0')->where('type', 'I')->sum('itemQuantity');

        /* FETCH SHIPMENT PACKAGES DETAILS */
        $data['delivery']['items'] = Shipmentpackage::where('deliveryId', $deliveryId)->where('deleted', '0')->where('type', 'I')->get();

        /* SET DATA FOR VIEW  */
        $data['deliveryId'] = $delivenum;
        if ($delivenum == 0)
            $data['deliveryId'] = $deliveryId;
        $data['pageTitle'] = "Print Delivery Label";
        $data['warehouselocation'] = Shipment::getWareHouseLocationList($id);
        $data['id'] = $id;
        $shipmentDetails = Shipment::find($id);
        $data['userUnit'] = User::find($shipmentDetails->userId)->unit;
        $shipmentTypeList = Shipment::allParticularShipmenttype();
        if(!empty($shipmentTypeList[$shipmentDetails->partucilarShipmentType])) {
            $data['shipmentType'] = $shipmentTypeList[$shipmentDetails->partucilarShipmentType];
        } else {
            $data['shipmentType'] = "";
        }

        return view('Administrator.shipments.deliverylabel', $data);
    }

    /**
     * Method used to print dropoff slip
     * @param integer $id
     * @param integer $page 
     * @return string
     */
    public function printdropslip($id, $page) {
        /* Fetch  Shipment Delivery Data */
        $deliveryData = ViewShipmentDetails::where("shipmentId", $id)->where('deleted', '0')->where('packageDeleted', '0')->where('packageType', 'I')->get()->toarray();

        /* SET DATA FOR VIEW  */
        $data['shipment']['delivery'] = Shipment::getDeliveryDetails($deliveryData);
        $data['pageTitle'] = "Print Drop Off Slip";
        $data['warehouselocation'] = Shipment::getWareHouseLocationList($id);
        $data['shipment'] = Viewshipment::where('id', $id)->first();

        return view('Administrator.shipments.dropoffslip', $data);
    }

    /**
     * Method used to print invoice
     * @param integer $id
     * @param integer $invoiceId 
     * @return string
     */
    public function printinvoice($id, $invoiceId) {
        /* Fetch  Invoice Data */
        $data['invoice'] = \App\Model\Invoice::find($invoiceId);

        $orderData = \App\Model\Order::where('shipmentId', $id)->where('type', 'shipment')->first();

        if (!empty($orderData))
            $data['orderNumber'] = $orderData->orderNumber;

        $data['pageTitle'] = "Print Invoice";
        if ($data['invoice']['type'] == 'itemReturn') {
            return view('Administrator.shipments.printreturninvoice', $data);
        } else {
            return view('Administrator.shipments.printinvoice', $data);
        }
    }

    /**
     * Method to update shipment payment details
     * @param integer $shipmentId
     * @param object $request 
     * @return boolean   
     */
    public function updatepaymentstatus($shipmentId, $shipmentType, Request $request) {
        if (empty($shipmentId))
            return 0;

        $paymentStatus = $request->paymentStatus;
        $shipment = Shipment::find($shipmentId);
        $shipment->paymentStatus = $paymentStatus;
        $shipment->save();

        if ($request->paymentStatus == 'paid') {

            $invoiceData = \App\Model\Invoice::where('shipmentId', $shipmentId)->where('type', $shipmentType)->where('invoiceType', 'invoice')->where('deleted', '0')->first();

            $shipmentStatusData = \App\Model\Paymenttransaction::where('paidForId', $shipmentId)->first();
            if(!empty($shipmentStatusData))
            {
                \App\Model\Paymenttransaction::where('paidForId', $shipmentId)->update(['status'=>'paid']);
            }else{

                $shipmentStatusData = new \App\Model\Paymenttransaction;
                $shipmentStatusData->paymentMethodId = $invoiceData->paymentMethodId;
                $shipmentStatusData->paidFor = 'othershipment';
                $shipmentStatusData->paidForId = $shipmentId;
                $shipmentStatusData->amountPaid = $shipment->totalCost;
                $shipmentStatusData->transactionOn = Config::get('constants.CURRENTDATE');
                $shipmentStatusData->status = 'paid';

                $save = $shipmentStatusData->save();


            }


            /*  INSERT DATA INTO INVOICE TABLE */
            $invoiceUniqueId = 'REC' . $invoiceData->userUnit . '-' . $shipmentId . '-' . date('Ymd');
            $invoice = new \App\Model\Invoice;
            $invoice->invoiceUniqueId = $invoiceUniqueId;
            $invoice->shipmentId = $shipmentId;
            $invoice->type = $shipmentType;
            $invoice->invoiceType = 'receipt';
            $invoice->userUnit = $invoiceData->userUnit;
            $invoice->userFullName = $invoiceData->userFullName;
            $invoice->userEmail = $invoiceData->userEmail;
            $invoice->userContactNumber = $invoiceData->userContactNumber;
            $invoice->billingName = $invoiceData->billingName;
            $invoice->billingEmail = $invoiceData->billingEmail;
            $invoice->billingAddress = $invoiceData->billingAddress;
            $invoice->billingAlternateAddress = $invoiceData->billingAlternateAddress;
            $invoice->billingCity = isset($invoiceData->billingCity) ? $invoiceData->billingCity : '';
            $invoice->billingState = isset($invoiceData->billingState) ? $invoiceData->billingState : '';
            $invoice->billingCountry = $invoiceData->billingCountry;
            $invoice->billingZipcode = $invoiceData->billingZipcode;
            $invoice->billingPhone = $invoiceData->billingPhone;
            $invoice->billingAlternatePhone = $invoiceData->billingAlternatePhone;
            $invoice->totalBillingAmount = $invoiceData->totalBillingAmount;
            $invoice->invoiceParticulars = $invoiceData->invoiceParticulars;
            $invoice->paymentMethodId = $invoiceData->paymentMethodId;
            $invoice->paymentStatus = 'paid';
            $invoice->createdOn = Config::get('constants.CURRENTDATE');
            $invoice->save();
            $invoiceId = $invoice->id;

            \App\Model\Invoice::where('id', $invoiceData->id)->update(['deleted' => '1']);

            $orderData = Order::where('shipmentId', $shipmentId)->first();

            if(!isset($orderData) && empty($orderData))
            {

                $orderObj = new Order;
                $orderObj->shipmentId = $shipmentId;
                $orderObj->userId = $shipment->userId;
                $orderObj->totalCost = $shipment->totalCost;
                $orderObj->status = '2';
                $orderObj->type = 'shipment';
                $orderObj->createdBy = Auth::user()->id;
                $orderObj->save();
                $orderNumber = \App\Model\Order::generateOrderNumber($orderObj->id, $orderObj->userId);
                $orderObj->orderNumber = $orderNumber;
                $orderObj->save();
            }
        }

        return 1;
    }
    
    public function updateinvoicepaymentstatus($invoiceId, Request $request) {
        if (empty($invoiceId))
            return 0;


        if ($request->paymentStatus == 'paid') {

            $invoiceData = \App\Model\Invoice::find($invoiceId);
            $invoiceData->paymentStatus = 'paid';
            $invoiceData->createdOn = Config::get('constants.CURRENTDATE');
            $invoiceData->save();
        }

        return 1;
    }

    /**
     * Method used to print DHL Commercial Invoice Pop up
     * @param integer $shipmentId
     * @param integer $page 
     * @return string
     */
    public function dhlci($shipmentId, $page) {

        $data = array();
        $dhlLabelInfo = \App\Model\Shipmentdhllabelprint::where("shipmentId",$shipmentId)->first();
        if(!empty($dhlLabelInfo)) {
            $shipmentData = Shipment::find($shipmentId);
            $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

            $data['shipmentData'] = $shipmentData;
            /* Fetch  Shipment Delivery Data */
            $deliveryData = ViewShipmentDetails::where("shipmentId", $shipmentId)->where('deleted', '0')->where('packageDeleted','0')->where('itemReturn','0')->where('packageType', 'I')->get()->toarray();
            $data['delivery'] = Shipment::getDeliveryDetails($deliveryData);

            $deliveryAddress = array();
            if ($shipmentData->paymentStatus == 'paid') {
                $paidInvoice = \App\Model\Invoice::where('shipmentId', $shipmentId)->where('type', $shipmentData->shipmentType)->where("type", $shipmentData->shipmentType)->first();
                if (!empty($paidInvoice)) {
                    $invoiceData = json_decode($paidInvoice->invoiceParticulars, true);
                    if (!empty($invoiceData['shippingaddress'])) {
                        $shippingAddress = $invoiceData['shippingaddress'];
                        $deliveryAddress['toName'] = $shippingAddress['toName'];
                        $deliveryAddress['toCountry'] = $shippingAddress['toCountry'];
                        $deliveryAddress['toState'] = $shippingAddress['toState'];
                        $deliveryAddress['toCity'] = $shippingAddress['toCity'];
                        $deliveryAddress['toAddress'] = $shippingAddress['toAddress'];
                        $deliveryAddress['toEmail'] = $shippingAddress['toEmail'];
                        $deliveryAddress['toPhone'] = $shippingAddress['toPhone'];
                        $deliveryAddress['toZipCode'] = (!empty($shippingAddress['toZipCode']) ? $shippingAddress['toZipCode'] : "");
                    }
                }
            }

            if (empty($deliveryAddress)) {
                $deliveryAddress['toName'] = $shipmentData->toName;
                $deliveryAddress['toCountryName'] = $shipmentData->toCountryName;
                $deliveryAddress['toStateName'] = $shipmentData->toStateName;
                $deliveryAddress['toCityName'] = $shipmentData->toCityName;
                $deliveryAddress['toAddress'] = $shipmentData->toAddress;
                $deliveryAddress['toEmail'] = $shipmentData->toEmail;
                $deliveryAddress['toPhone'] = $shipmentData->toPhone;
                $deliveryAddress['toZipCode'] = $shipmentData->toZipCode;
            }

            $packageDetailsData = \App\Model\Shipmentspackaging::where("shipmentId", $shipmentId);
            $data['totalPackageWeight'] = (!empty($dhlLabelInfo->grossWeight) ? $dhlLabelInfo->grossWeight : $packageDetailsData->sum('weight'));
            $data['totalChargeableWeight'] = (!empty($dhlLabelInfo->chargeableWeight) ? $dhlLabelInfo->chargeableWeight : $packageDetailsData->sum('chargeableWeight'));
            $data['packageDetailsData'] = $packageDetailsData->get();
            $data['packageCount'] = $packageDetailsData->count();
            $data['deliveryAddress'] = $deliveryAddress;
            $data['dhlLabelInfo'] = $dhlLabelInfo;
            $productScheduleData = collect(\App\Model\Siteproduct::get());
            $data['productScheduleList'] = $productScheduleData->mapWithKeys(function($item){ return [$item['id']=>$item['scheduleBNumber']]; });
            
            $subCategoryScheduleData = collect(\App\Model\Sitecategory::where("parentCategoryId","!=","-1")->get());
            $data['subCategoryScheduleList'] = $subCategoryScheduleData->mapWithKeys(function($item){ return [$item['id']=>$item['scheduleBNumber']]; });
            //\App\Model\Shipmentlabelprintlog::logEntry($shipmentId, 'DHL Comercial Invoice Label');
        } else {
            $data['shipmentData'] = array();
        }
        
        return view('Administrator.shipments.dhlci', $data);
    }

    /**
     * Method used to print DHL Commercial Invoice
     * @return string
     */
    public function dhlcipost($shipmentId = 1, $page = 1, Request $request) {

        if (\Request::isMethod('post')) {
            $str = $request->data;

            $dataExtract = explode("&", $str); #dd($dataExtract);
            //echo '<pre>';print_r($dataExtract);echo '</pre>';exit;
            $data['fromAddress'] = urldecode(str_replace('fromAddress=', '', $dataExtract[2]));
            $data['toAddress'] = urldecode(str_replace('toAddress=', '', $dataExtract[4]));
            $data['fromAddress2'] = urldecode(str_replace('', '', $dataExtract[4]));
            $data['toAddress2'] = str_replace('toAddress2=', '', $dataExtract[5]);
            $data['fromCountry'] = $request->input('fromCountry');
            $data['toCountry'] = $request->input('toCountry');
            $data['fromState'] = $request->input('fromState');
            $data['toState'] = $request->input('toState');
            $data['fromCity'] = $request->input('fromCity');
            $data['toCity'] = $request->input('toCity');
            $data['fromZip'] = urldecode(str_replace('fromZip=', '', $dataExtract[12]));
            $data['toZip'] = urldecode(str_replace('toZip=', '', $dataExtract[13]));
            $data['fromPhone'] = urldecode(str_replace('fromPhone=', '', $dataExtract[14]));
            $data['toPhone'] = urldecode(str_replace('toPhone=', '', $dataExtract[15]));
            $data['fromEmail'] = urldecode(str_replace('fromEmail=', '', $dataExtract[16]));
            $data['toEmail'] = urldecode(str_replace('toEmail=', '', $dataExtract[17]));
            $data['fromName'] = urldecode(str_replace('fromName=', '', $dataExtract[18]));
            $data['toName'] = urldecode(str_replace('toName=', '', $dataExtract[19]));
            $data['fromCompanyName'] = urldecode(str_replace('fromCompanyName=', '', $dataExtract[20]));
            $data['toCompanyName'] = urldecode(str_replace('toCompanyName=', '', $dataExtract[21]));

            $data['airWaybillNo'] = urldecode(str_replace('airWaybillNo=', '', $dataExtract[22]));
            $data['incoterms'] = urldecode(str_replace('incoterms=', '', $dataExtract[23]));
            #$data['Pieces'] = str_replace('Pieces=', '', $dataExtract[39]);
            $data['Pieces'] = str_replace('Pieces=', '', $dataExtract[28]);
            #$data['invoiceDate'] = urldecode(str_replace('invoiceDate=', '', $dataExtract[42]));
            $data['invoiceDate'] = urldecode(str_replace('invoiceDate=', '', $dataExtract[26]));
            #$data['invoiceNo'] = str_replace('invoiceNo=', '', $dataExtract[43]);
            $data['invoiceNo'] = str_replace('invoiceNo=', '', $dataExtract[27]);
            #$data['grossWeight'] = str_replace('grossWeight=', '', $dataExtract[40]);
            $data['grossWeight'] = str_replace('grossWeight=', '', $dataExtract[24]);
            #$data['netWeight'] = str_replace('netWeight=', '', $dataExtract[41]);
            $data['netWeight'] = str_replace('netWeight=', '', $dataExtract[25]);
            #$data['totalPrice'] = urldecode(str_replace('totalPrice=', '', $dataExtract[44]));
            //$data['totalPrice'] = urldecode(str_replace('totalPrice=', '', $dataExtract[34]));

            $data['qty'] = $request->input('qty');
            $data['price'] = $request->input('price');
            $data['manufCountry'] = $request->input('manufCountry');
            $totalPrice = 0;
            foreach($request->input('price') as $index => $eachPrice) {
                $totalPrice += $eachPrice*$data['qty'][$index];
            }
            $data['totalPrice'] = $totalPrice;

            $data['descr'] = $request->descr;
            $data['ccc'] = $request->ccc;
            //$data['manufCountry'] = $request->manufCountry;

            $data['fromState'] = ($data['fromState'] == 'Select any one' ? '-' : $data['fromState']);
            $data['fromCity'] = ($data['fromCity'] == 'Select any one' ? '-' : $data['fromCity']);
            $data['toState'] = ($data['toState'] == 'Select any one' ? '-' : $data['toState']);
            $data['toCity'] = ($data['toCity'] == 'Select any one' ? '-' : $data['toCity']);

            $dataExtract = explode("&", $str);
            $data['fromAddress'] = urldecode(str_replace('fromAddress=', '', $dataExtract[2]));
            $data['toAddress'] = urldecode(str_replace('toAddress=', '', $dataExtract[4]));
            $data['fromAddress2'] = urldecode(str_replace('', '', $dataExtract[4]));
            $data['toAddress2'] = str_replace('toAddress2=', '', $dataExtract[5]);
            $data['fromCountry'] = $request->input('fromCountry');
            $data['toCountry'] = $request->input('toCountry');
            $data['fromState'] = $request->input('fromState');
            $data['toState'] = $request->input('toState');
            $data['fromCity'] = $request->input('fromCity');
            $data['toCity'] = $request->input('toCity');
            $data['fromZip'] = urldecode(str_replace('fromZip=', '', $dataExtract[12]));
            $data['toZip'] = urldecode(str_replace('toZip=', '', $dataExtract[13]));
            $data['fromPhone'] = urldecode(str_replace('fromPhone=', '', $dataExtract[14]));
            $data['toPhone'] = urldecode(str_replace('toPhone=', '', $dataExtract[15]));
            $data['fromEmail'] = urldecode(str_replace('fromEmail=', '', $dataExtract[16]));
            $data['toEmail'] = urldecode(str_replace('toEmail=', '', $dataExtract[17]));
            $data['fromName'] = urldecode(str_replace('fromName=', '', $dataExtract[18]));
            $data['toName'] = urldecode(str_replace('toName=', '', $dataExtract[19]));
            $data['fromCompanyName'] = urldecode(str_replace('fromCompanyName=', '', $dataExtract[20]));
            $data['toCompanyName'] = urldecode(str_replace('toCompanyName=', '', $dataExtract[21]));
            $data['fromState'] = ($data['fromState'] == 'Select any one' ? '-' : $data['fromState']);
            $data['fromCity'] = ($data['fromCity'] == 'Select any one' ? '-' : $data['fromCity']);
            $data['toState'] = ($data['toState'] == 'Select any one' ? '-' : $data['toState']);
            $data['toCity'] = ($data['toCity'] == 'Select any one' ? '-' : $data['toCity']);
            //$data['shipmentInfo'] = Shipment::find($data['invoiceNo']);
            $dhlLabelData = \App\Model\Shipmentdhllabelprint::where("shipmentId",$data['invoiceNo'])->first();
            $data['dhlAccountInfo'] = \App\Model\Dhlaccountsettings::find($dhlLabelData->dhlAccountNumber);
            $shipmentData = Shipment::find($data['invoiceNo']);
            $userData = User::find($shipmentData->userId);

            $data['shipmentData'] = $shipmentData;
            $data['userData'] = $userData;
            
            $destinationPath = "/uploads/shipments/" . $data['invoiceNo'];
            $fileName = $data['invoiceNo'].'-'.time().'.pdf';
            PDF::loadView('Administrator.shipments.dhlci-post-pdf', $data)->save(public_path($destinationPath.'/' . $fileName))->stream('download.pdf');
            \App\Model\Shipmentlabelprintlog::logEntry($data['invoiceNo'], 'DHL Comercial Invoice Label',$fileName);

            return view('Administrator.shipments.dhlci-post', $data);
        }
        return view('Administrator.shipments.dhlci-post');
    }

    /**
     * This function used to generate and export Selected shipment records
     * @param type $page
     * @param Request $request
     * @return type
     */
    public function exportselected($page, Request $request) {

        $shipmentObj = new Shipment;
        /* Get data for export */
        $exportData = $shipmentObj->exportData(array(), $request->selected, $request->selectedField);



        $excelName = "Shipment-Data" . Carbon::now();
        ob_end_clean();
        ob_start();
        /* Generate Excel */
        Excel::create($excelName, function($excel) use($exportData) {
            $excel->sheet('Sheet 1', function($sheet) use($exportData) {
                $sheet->fromArray($exportData['shipments']);
            });
        })->store('xlsx', public_path('exports'));
        ob_flush();
        $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
        return response()->json(['path' => $creatingPath]);
    }

    /**
     * This function used to generate and export All shipment records
     * @param type $page
     * @param Request $request
     */
    public function exportall($page, Request $request) {

        $shipmentObj = new Shipment;
        $param = array();
        /* Initialize search field data */
        $searchShipmentArr = array(
            'idFrom' => '',
            'idTo' => '',
            'unitTo' => '',
            'unitFrom' => '',
            'shipmentStatus' => '',
            'prepaid' => '',
            'deliveryCompanyId' => '',
            'dispatchCompanyId' => '',
            'shipmentType' => '',
            'totalFrom' => '',
            'totalTo' => '',
            'user' => '',
            'receiver' => '',
            'paymentStatus' => '',
            'toCountry' => '',
            'warehouseId' => '',
            'labelType' => '',
            'itemType' => '',
            'location' => '',
            'packed' => ''
        );
        $searchByCreatedOn = \Session::get('SHIPMENTDATA.searchByCreatedOn');
        $searchByDate = \Session::get('SHIPMENTDATA.searchByDate');
        $searchShipment = \Session::get('SHIPMENTDATA.searchShipment');

        $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
        $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
        $param['searchShipment'] = !empty($searchShipment) ? $searchShipment[0] : $searchShipmentArr;
        /* Get data to export */
        $exportData = $shipmentObj->exportData($param, array(), $request->selectall);

        $excelName = "Shipment-Data" . Carbon::now();
        ob_end_clean();
        ob_start();
        /* Generate excell */
        Excel::create($excelName, function($excel) use($exportData) {
            $excel->sheet('Sheet 1', function($sheet) use($exportData) {
                $sheet->fromArray($exportData['shipments']);
            });
        })->export('xlsx');
        ob_flush();
    }

    public function getpackagingdata($id, $page) {

        $shipment = Viewshipment::find($id);
        $data['shipment'] = $shipment;
        /* Fetch  Shipment Delivery Data */
        $deliveryData = ViewShipmentDetails::where("shipmentId", $id)->where('deleted', '0')->where('packageDeleted', '0')->where('packageType', 'I')->get()->toarray();
        $data['shipment']['delivery'] = Shipment::getDeliveryDetails($deliveryData);
        /* Fetch packaging packaging records if exist */
        $data['packageDetailsData'] = \App\Model\Shipmentspackaging::where("shipmentId", $id)->get();
        /* Calculate total chargebale weight of all packaging records */
        $packageTotalChargebleWeight = Shipmentpackage::calculatetotalchargebleweight($data['packageDetailsData']);
        /* Get difference between total weight of deliveries and total chargeable weight of packaging records */
        $data['packageAcceptableLimit'] = Shipmentpackage::getpackageLimit($packageTotalChargebleWeight, $data['shipment']['delivery']['totalChargeableWeight']);
        $data['page'] = $page;
        return view('Administrator.shipments.packaging', $data);
    }

    /**
     * Method used to print DHL Label Pop up
     * @param integer $shipmentId
     * @param integer $page 
     * @return html
     */
    public function dhl($shipmentId, $page) {

        $data = array();
        $shipmentData = Shipment::find($shipmentId);
        $userData = User::find($shipmentData->userId);
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

        $data['shipmentData'] = $shipmentData;
        $data['userData'] = $userData;
        /* Fetch  Shipment Delivery Data */
        $deliveryData = ViewShipmentDetails::where("shipmentId", $shipmentId)->where('deleted', '0')->where('packageType', 'I')->get()->toarray();
        $data['delivery'] = Shipment::getDeliveryDetails($deliveryData);
        $data['dhlAccountNumber'] = \App\Model\Dhlaccountsettings::where("deleted","0")->get();
        $deliveryAddress = array();
        if ($shipmentData->paymentStatus == 'paid') {
            $paidInvoice = \App\Model\Invoice::where('shipmentId', $shipmentId)->where('type', $shipmentData->shipmentType)->where("type", $shipmentData->shipmentType)->first();
            if (!empty($paidInvoice)) {
                $invoiceData = json_decode($paidInvoice->invoiceParticulars, true);
                if (!empty($invoiceData['shippingaddress'])) {
                    $shippingAddress = $invoiceData['shippingaddress'];
                    $deliveryAddress['toName'] = $shippingAddress['toName'];
                    $deliveryAddress['toCountry'] = $shippingAddress['toCountry'];
                    $deliveryAddress['toState'] = $shippingAddress['toState'];
                    $deliveryAddress['toCity'] = $shippingAddress['toCity'];
                    $deliveryAddress['toAddress'] = $shippingAddress['toAddress'];
                    $deliveryAddress['toEmail'] = $shippingAddress['toEmail'];
                    $deliveryAddress['toPhone'] = $shippingAddress['toPhone'];
                    $deliveryAddress['toZipCode'] = (!empty($shippingAddress['toZipCode']) ? $shippingAddress['toZipCode'] : "N/A");
                }
            }
        }

        if (empty($deliveryAddress)) {
            $deliveryAddress['toName'] = $shipmentData->toName;
            $deliveryAddress['toCountryName'] = $shipmentData->toCountryName;
            $deliveryAddress['toStateName'] = $shipmentData->toStateName;
            $deliveryAddress['toCityName'] = $shipmentData->toCityName;
            $deliveryAddress['toAddress'] = $shipmentData->toAddress;
            $deliveryAddress['toEmail'] = $shipmentData->toEmail;
            $deliveryAddress['toPhone'] = $shipmentData->toPhone;
            $deliveryAddress['toZipCode'] = $shipmentData->toZipCode;
        }

        $packageDetailsData = \App\Model\Shipmentspackaging::where("shipmentId", $shipmentId)->get();
        $data['packageDetailsData'] = $packageDetailsData;
        $data['packageCount'] = $packageDetailsData->count();
        $data['deliveryAddress'] = $deliveryAddress;
        $data['termsOfTrades'] = \App\Model\Dhltradetermssettings::where("deleted","0")->get();
        //dd($packageDetailsData);
//        \App\Model\Shipmentlabelprintlog::logEntry($shipmentId, 'DHL Label');
        return view('Administrator.shipments.dhl', $data);
    }

    function func_dhl_filter_output($ab_response) {
        $ab_response = explode("
        ", $ab_response);

        $_ab_response = '';
        foreach ($ab_response as $k => $v) {
            $elem = trim($v);
            if (strlen($elem) > 4 && $elem != '0fe8')
                $_ab_response .= $elem;
        }
        $ab_response = $_ab_response;
        return $ab_response;
    }

    /**
     * Method used to print DHL Label functionality
     * @param integer $page 
     * @return html
     */
    public function dhlpost($page = 1, Request $request) {
        if (\Request::isMethod('post')) {
            $getDefaultCurrency = \App\Model\Currency::getDefaultCurrency();
            $currenctSymbol = $getDefaultCurrency[0]->code;

            $validator = Validator::make($request->all(), [
                        'width' => 'required',
                        'height' => 'required',
                        'length' => 'required',
                        'weight' => 'required',
                        'cweight' => 'required',
                        'contents' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            } else {
                $str = $request->data;
                $dataExtract = explode("&", $str);
                $datafromAddress = urldecode(str_replace('fromAddress=', '', $dataExtract[2]));
                $datatoAddress = urldecode(str_replace('toAddress=', '', $dataExtract[4]));
                $datafromAddress2 = urldecode(str_replace('', '', $dataExtract[4]));
                $datatoAddress2 = str_replace('toAddress2=', '', $dataExtract[5]);
                $datafromCountry = $request->input('fromCountry');
                $datatoCountry = $request->input('toCountry');
                $datafromState = $request->input('fromState');
                $datatoState = $request->input('toState');
                $datafromCity = $request->input('fromCity');
                $datatoCity = $request->input('toCity');
                $datafromZip = urldecode(str_replace('fromZip=', '', $dataExtract[12]));
                $datatoZip = urldecode(str_replace('toZip=', '', $dataExtract[13]));
                $datafromPhone = urldecode(str_replace('fromPhone=', '', $dataExtract[14]));
                $datatoPhone = urldecode(str_replace('toPhone=', '', $dataExtract[15]));
                $datafromEmail = urldecode(str_replace('fromEmail=', '', $dataExtract[16]));
                $datatoEmail = urldecode(str_replace('toEmail=', '', $dataExtract[17]));
                $datafromName = urldecode(str_replace('fromName=', '', $dataExtract[18]));
                $datatoName = urldecode(str_replace('toName=', '', $dataExtract[19]));
                $datafromCompanyName = urldecode(str_replace('fromCompanyName=', '', $dataExtract[20]));
                $datatoCompanyName = urldecode(str_replace('toCompanyName=', '', $dataExtract[21]));
                $termsOfTrade = urldecode(str_replace('termsOfTrade=', '', $dataExtract[22]));
                $datashipmentId = urldecode(str_replace('invoiceId=', '', $dataExtract[24]));
                $piecesCount = urldecode(str_replace('piecesCount=', '', $dataExtract[25]));
                //$total_weight = urlencode(str_replace('grossWeight=', '', $dataExtract[25]));
                //$total_Chweight = urlencode(str_replace('grossChWeight=', '', $dataExtract[26]));
                //$totalPrice = urlencode(str_replace('totalPrice=', '', $dataExtract[27]));
                $totalPrice = urlencode(str_replace('totalValue=', '', $dataExtract[23]));
                $dhlAccountId = urlencode(str_replace('dhlaccountid=', '', $dataExtract[29]));
                $customerNumber = urlencode(str_replace('customerPhoneNumber=', '', $dataExtract[30]));
                $dhlAcccountDetails = \App\Model\Dhlaccountsettings::find($dhlAccountId);
                $dhlAccountNo = $dhlAcccountDetails->dhlAccountNo;

                $data_width = (!empty($request->width) ? $request->width : 1);
                $data_height = (!empty($request->height) ? $request->height : 1);
                $data_length = (!empty($request->length) ? $request->length : 1);
                $data_weight = (!empty($request->weight) ? $request->weight : 1);
                $data_cweight = (!empty($request->cweight) ? $request->cweight : 1);
                $data_contents = (!empty($request->contents) ? $request->contents : 1);
                //print_r($request->all());exit;

                $datatoCountryQuery = \App\Model\Country::where('name', $datatoCountry)->get();
                if (count($datatoCountryQuery)) {
                    $datatoCountryCode = $datatoCountryQuery[0]->code;
                } else {
                    $datatoCountryCode = '';
                }

                $datatoStateQuery = \App\Model\State::where('name', $datatoState)->get();
                if (count($datatoStateQuery)) {
                    $datatoStateDivisionCode = $datatoStateQuery[0]->code;
                    $datatoStateDivisionCode = substr($datatoStateDivisionCode,0,2);
                } else {
                    $datatoStateDivisionCode = '';
                }

                $datafromCountryQuery = \App\Model\Country::where('name', $datafromCountry)->get();
                if (count($datafromCountryQuery)) {
                    $datafromCountryCode = $datafromCountryQuery[0]->code;
                } else {
                    $datafromCountryCode = '';
                }

                $datafromStateQuery = \App\Model\State::where('name', $datafromState)->get();
                if (count($datafromStateQuery)) {
                    $datafromStateDivisionCode = $datafromStateQuery[0]->code;
                    $datafromStateDivisionCode = substr($datafromStateDivisionCode,0,2);
                } else {
                    $datafromStateDivisionCode = '';
                }

                if ($datatoCompanyName == '') {
                    $datatoCompanyName = '-';
                }
                $datafromAddress = substr($datafromAddress, 0, 35);
                $datatoAddress = substr($datatoAddress, 0, 35);
                $message_ref = '';
                for ($i = 0; $i < 30; $i++) {
                    $message_ref .= rand(0, 9);
                }
                $message_time = date("Y-m-d") . "T" . date("H:i:sP");
                $ab_date = date("Y-m-d", strtotime("+1 day"));

                $logo_image = base64_encode(file_get_contents(asset('public/administrator/img/logo.png')));
                

                $pieces = '';
                $total_weight = '0.00';
                $total_Chweight = '0.00';
                
                for ($i = 0; $i < $piecesCount; $i++) {
                    $no = $i + 1;



                    $data_width1 = round($data_width[$i], 1);
                    $data_height1 = round($data_height[$i], 1);
                    $data_length1 = round($data_length[$i], 1);
                    $data_weight1 = round($data_weight[$i], 1);
                    $data_cweight1 = round($data_cweight[$i], 1);
                    $total_weight += $data_weight1;
                    $total_Chweight += $data_cweight1;

                    $pieces .= <<<EOT
                        <Piece>
                        <PieceID>$no</PieceID>
                        <PackageType>EE</PackageType>
                        <Weight>$data_weight1</Weight>
                        <DimWeight>$data_cweight1</DimWeight>
                        <Width>$data_width1</Width>
                        <Height>$data_height1</Height>
                        <Depth>$data_length1</Depth>
                        <PieceContents>$data_contents[$i]</PieceContents>
                        </Piece>
EOT;
                }
                
                if ($total_weight == '0.00') {
                    $total_weight = round($data_weight[0], 1);
                } else {
                    $total_weight = round($total_weight, 1);
                }


                $query = <<<EOT
<?xml version="1.0" encoding="ISO-8859-1"?>
<req:ShipmentValidateRequest xmlns:req="http://www.dhl.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dhl.com ship-val-req.xsd">
<Request>
<ServiceHeader>
<MessageTime>$message_time</MessageTime>
<MessageReference>$message_ref</MessageReference>
<SiteID>AmericanAirS</SiteID>
<Password>7UcSzTXnsR</Password>
</ServiceHeader>
</Request>
<RequestedPickupTime>N</RequestedPickupTime>
<NewShipper>N</NewShipper>
<LanguageCode>en</LanguageCode>
<PiecesEnabled>Y</PiecesEnabled>
<Billing>
<ShipperAccountNumber>$dhlAccountNo</ShipperAccountNumber>
<ShippingPaymentType>S</ShippingPaymentType>
<BillingAccountNumber>$dhlAccountNo</BillingAccountNumber>
<DutyPaymentType>R</DutyPaymentType>
</Billing>
<Consignee>
<CompanyName>$datatoCompanyName</CompanyName>
<AddressLine>$datatoAddress</AddressLine>
<City>$datatoCity</City>
<Division>$datatoState</Division>
<DivisionCode>$datatoStateDivisionCode</DivisionCode>
<PostalCode>$datatoZip</PostalCode>
<CountryCode>$datatoCountryCode</CountryCode>
<CountryName>$datatoCountry</CountryName>
<Contact>
<PersonName>$datatoName</PersonName>
<PhoneNumber>$datatoPhone</PhoneNumber>
<PhoneExtension></PhoneExtension>
</Contact>
</Consignee>
<Dutiable>
<DeclaredValue>$totalPrice</DeclaredValue>
<DeclaredCurrency>$currenctSymbol</DeclaredCurrency>
<TermsOfTrade>$termsOfTrade</TermsOfTrade>
</Dutiable>
<Reference>
<ReferenceID>$datashipmentId</ReferenceID>
<ReferenceType>St</ReferenceType>
</Reference>
<ShipmentDetails>
<NumberOfPieces>$piecesCount</NumberOfPieces>
<Pieces>$pieces</Pieces>
<Weight>$total_weight</Weight>
<WeightUnit>L</WeightUnit>
<GlobalProductCode>P</GlobalProductCode>
<Date>$ab_date</Date>
<Contents>SHIPMENT #$datashipmentId</Contents>
<DoorTo>DD</DoorTo>
<DimensionUnit>I</DimensionUnit>
<PackageType>EE</PackageType>
<IsDutiable>N</IsDutiable>
<CurrencyCode>$currenctSymbol</CurrencyCode>
</ShipmentDetails>
<Shipper>
<ShipperID>$dhlAccountNo</ShipperID>
<CompanyName>$datafromCompanyName</CompanyName>
<RegisteredAccount>848177056</RegisteredAccount>
<AddressLine>$datafromAddress</AddressLine>
<City>$datafromCity</City>
<Division>$datafromState</Division>
<DivisionCode>$datafromStateDivisionCode</DivisionCode>
<PostalCode>$datafromZip</PostalCode>
<CountryCode>$datafromCountryCode</CountryCode>
<CountryName>$datafromCountry</CountryName>
<Contact>
<PersonName>$datafromName</PersonName>
<PhoneNumber>$datafromPhone</PhoneNumber>
<PhoneExtension></PhoneExtension>
</Contact>
</Shipper>
<EProcShip>N</EProcShip>
<LabelImageFormat>PDF</LabelImageFormat>
<RequestArchiveDoc>Y</RequestArchiveDoc>
<Label>
<LabelTemplate>8X4_thermal</LabelTemplate>
<Logo>Y</Logo>
<CustomerLogo>
<LogoImage>$logo_image</LogoImage>
<LogoImageFormat>PNG</LogoImageFormat>
</CustomerLogo>
<Resolution>200</Resolution>
</Label>
</req:ShipmentValidateRequest>
EOT;
//dd($query);
                $url = "https://xmlpi-ea.dhl.com/XMLShippingServlet?isUTF8Support=true";

                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
                curl_setopt($ch, CURLOPT_POSTFIELDS, "$query");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $output = curl_exec($ch);
                curl_close($ch);

                $output = $this->func_dhl_filter_output($output);
                
                preg_match("/<ConditionData>(.*?)<\/ConditionData>/", $output, $matches);
                if (count($matches) > 0) {
                    $message = $matches[1];
                }

                preg_match("/<OutputImage>(.*?)<\/OutputImage>/", $output, $matches);
                if (count($matches) > 0) {

                    // Load the XML
                    $xmlResponse = simplexml_load_string($output);

                    // JSON encode the XML, and then JSON decode to an array.
                    $responseArray = json_decode(json_encode($xmlResponse), true);

                    if(!empty($responseArray['AirwayBillNumber'])) {
                        $airwayBillNumber = $responseArray['AirwayBillNumber'];
                        $dhlLabelData = \App\Model\Shipmentdhllabelprint::where("shipmentId",$datashipmentId)->get();
                        if($dhlLabelData->count() > 0)
                            $dhlLabelPrint = \App\Model\Shipmentdhllabelprint::find($dhlLabelData[0]->id);
                        else {
                            $dhlLabelPrint = new \App\Model\Shipmentdhllabelprint();
                            $dhlLabelPrint->createdOn = date('Y-m-d h:i:s');
                            $dhlLabelPrint->createdBy = Auth::user()->email;
                            $dhlLabelPrint->shipmentId = $datashipmentId;
                        }
                        $dhlLabelPrint->dhlWaybillNumber = $airwayBillNumber;
                        $dhlLabelPrint->termsOfTrade = $termsOfTrade;
                        $dhlLabelPrint->dhlAccountNumber = $dhlAccountId;
                        $dhlLabelPrint->grossWeight = $total_weight;
                        $dhlLabelPrint->chargeableWeight = $total_Chweight;
                        $dhlLabelPrint->totalValue = $totalPrice;
                        
                        $dhlLabelPrint->save();
                    }

                    $image = base64_decode($matches[1]);

                    $filename = $datashipmentId .'-'.time(). '.pdf';

                    if (file_exists(public_path("/uploads/shipments/" . $request->shipmentId))) {
                        // do nothing
                    } else {
                        mkdir(public_path("/uploads/shipments/" . $request->shipmentId));
                        copy((public_path("/uploads/demo.pdf")), public_path("/uploads/shipments/" . $request->shipmentId . "/" . $filename));
                    }

                    $destinationPath = public_path("/uploads/shipments/" . $request->shipmentId);
                    \PDF::loadView('Administrator.shipments.dhl-post')->save($destinationPath . "/" . $filename)->stream('download.pdf');

                    $file = $destinationPath . "/" . $filename;
                    file_put_contents($file, $image);
                    
                    \App\Model\Shipmentlabelprintlog::logEntry($datashipmentId, 'DHL Label',$filename);

                    $data['file'] = $filename;
                    $data['shipId'] = $datashipmentId;

                    return view('Administrator.shipments.dhl-post', $data);
                } else {
                    $data['message'] = urldecode($message);
                    $data['shipId'] = $datashipmentId;
                    return view('Administrator.shipments.dhl-post', $data);
                }
            }
        }
    }

    public function deliveryshippingcost($shipmentId, $deliveryId) {
        /* Fetch  Shipment Delivery Data */
        $deliveryData = ViewShipmentDetails::where("shipmentId", $shipmentId)->where('deliveryId', $deliveryId)->where('deleted', '0')->where('packageDeleted', '0')->where('packageType', 'I')->get()->toarray();
        $data['deliveryData'] = Shipment::getDeliveryDetails($deliveryData);
        $shipment = Viewshipment::find($shipmentId);
        /* FETCH SHIPPING METHOD LIST  */
        $data['shippingMethodList'] = \App\Model\Shippingmethods::where('deleted', '0')->where('active', 'Y')->orderby('shipping', 'asc')->get();
        /* FETCH PAYMENT METHOD LIST  */
        $data['paymentMethodList'] = \App\Model\Paymentmethod::where('deleted', '0')->where('status', '1')->orderby('paymentMethod', 'asc')->get();
        $data['shipment'] = $shipment;

        return view('Administrator.shipments.deliveryshippingcostbox', $data);
    }

    public function updatestage($shipmentId, $deliveryId, Request $request) {
        if (!empty($request->status)) {
            //
            $shipment = Shipmentdelivery::where('shipmentId', $shipmentId)->get();

            if (!empty($shipment[0]->shippingMethodId)) {
                $shippingMethod = Shippingmethods::where('shippingId', $shipment[0]->shippingMethodId)->get();

                $expectedDispatchDate = \Carbon\Carbon::now()->addWeekDays($shippingMethod[0]->days)->format('Y-m-d h:i:s');
                $dispatchCompanyId = $request->assignCompany;

                $delivery = Shipmentdelivery::find($deliveryId);

                $delivery->dispatchCompanyId = $dispatchCompanyId;
                $delivery->expectedDispatchDate = $expectedDispatchDate;
                $delivery->save();
            }

            $statusPost = $request->status;
            foreach ($statusPost as $eachStatus => $dates) {
                if ($dates != '') {
                    $shipmentStatusData = \App\Model\Shipmentstatuslog::where("shipmentId", $shipmentId)->where("deliveryId", $deliveryId)->orderBy("id", "desc")->first();
                    $shipmentStatus = new \App\Model\Shipmentstatuslog;
                    $shipmentStatus->shipmentId = $shipmentId;
                    $shipmentStatus->deliveryId = $deliveryId;
                    $shipmentStatus->oldStatus = (!empty($shipmentStatusData->status) ? $shipmentStatusData->status : 'none');
                    $shipmentStatus->status = $eachStatus;
                    $shipmentStatus->updatedOn = date('Y-m-d h:i:s', strtotime($dates));
                    $shipmentStatus->save();
                }
            }

            //Search final status for all delivery of same shipment. If same update Shipment delivery status else break;

            $countDelivery = count($shipment);
            $counter = 1;
            $stausSaveNo = 0;
            $j = 0;
            $storeStatusName = "";
            for ($i = 0; $i < $countDelivery; $i++) {

                $shipmentStatusName[$i] = \App\Model\Shipmentstatuslog::where("shipmentId", $shipmentId)->where("deliveryId", $shipment[$i]['id'])->orderBy("id", "desc")->pluck('status')->first();
                if ($i == 0) {
                    $counter = 1;
                    $storeStatusName = $shipmentStatusName[$i];
                }
                if ($i >= 1) {
                    $j = ($i - 1);

                    if ($shipmentStatusName[$i] == $shipmentStatusName[$j]) {
                        $counter+=1;
                        $storeStatusName = $shipmentStatusName[$i];
                    } else {
                        $storeStatusName = "";
                    }
                }
            }

            // print_r($shipment); echo $counter; die;

            if ($countDelivery == $counter) {
                //echo "here123"; echo $storeStatusName; die;
                //Update Shipment table

                $shimentOrg = Shipment::find($shipmentId);
                if ($storeStatusName == 'in_warehouse') {
                    $stausSaveNo = "1";
                } else if ($storeStatusName == 'in_transit') {
                    $stausSaveNo = "2";
                } else if ($storeStatusName == 'custom_clearing') {
                    $stausSaveNo = "3";
                } else if ($storeStatusName == 'destination_warehouse') {
                    $stausSaveNo = "4";
                } else if ($storeStatusName == 'out_for_delivery') {
                    $stausSaveNo = "5";
                } else if ($storeStatusName == 'delivered') {
                    $stausSaveNo = "6";
                } else {
                    $stausSaveNo = "0";
                }

                $shimentOrg->shipmentStatus = $stausSaveNo;
                $shimentOrg->save();
//                echo $stausSaveNo;exit;
                $deliveryStatusList = Shipmentdelivery::deliveryStatus();
                $deliveryUpdateStatus = array_keys($deliveryStatusList);
                $replace = array();
                $customer = User::find($shimentOrg->userId);
                $orderInfo = Order::where('shipmentId', $shipmentId)->first();
                $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'shipment_status_change')->first();

                $smsTemplate = \App\Model\Smstemplate::where('templateType', 'shipment_status_change')->get();

                $statusIndex = $deliveryUpdateStatus[$stausSaveNo - 1];
                $replace['[NAME]'] = $customer->firstName . " " . $customer->lastName;
                $replace['[SHIPMENTID]'] = $shipmentId;
                $replace['[SHIPMENT_STATUS]'] = $deliveryStatusList[$statusIndex];
                if (!empty($orderInfo))
                    $replace['[ORDERID]'] = $orderInfo->orderNumber;
                else
                    $replace['[ORDERID]'] = "";
                $replace['[WEBSITE_LINK]'] = "<a href='https://shoptomydoor.com/'>shoptomydoor.com</a>";


                $to = $customer->email;
                $toMobile = $customer->contactNumber;

                $sendMail = customhelper::SendMail($emailTemplate, $replace, $to);

                $smsSent = customhelper::sendMSG($toMobile, $replace, $smsTemplate);

                User::sendPushNotification($shimentOrg->userId, 'shipment_status_change', Auth::user()->id, $replace);
            }


            return 1;
        } else {
            return 0;
        }
    }

    /**
     * This function is user to update tracking number of shipment and its lock status
     * @param type $shipmentPackage
     * @param Request $request
     * @return type
     */
    public function updatetracking($shipmentPackage, Request $request) {
        $updateArray = array();
        $validationError = '1';
        if ($request->trackingNumber != '') {
            $updateArray['tracking'] = $request->trackingNumber;
            $validationError = '0';
        }
        if ($request->tracking2 != '') {
            $updateArray['tracking2'] = $request->tracking2;
            $validationError = '0';
        }
        if ($request->action == 'lock')
            $updateArray['trackingLock'] = '1';
        if ($validationError == 0) {
            Shipmentpackage::where('id', $shipmentPackage)->update($updateArray);
            $shipmentId = Shipmentpackage::find($shipmentPackage)->shipmentId;
            return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/1')->with('successMessage', 'Shipment updated successfully.');
        } else
            return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/1')->with('errorMessage', 'Please enter tracking number');
    }

    public function createextrainvoice($shipmentid, Request $request) {
        #print_r($request->all());
        #exit;

        Shipment::where('id', $shipmentid)->increment('totalItemCost', $request->extraCost);
        Shipment::where('id', $shipmentid)->increment('totalCost', $request->extraCost);
        $shipemntData = Shipment::find($shipmentid);
        $userInfo = User::find($shipemntData->userId);
        $to = $userInfo->email;

        $invoiceDetails = \App\Model\Invoice::where('shipmentId', $shipmentid)->where('type', 'othershipment')->where('deleted', '0')->where('extraCostCharged', 'N')->first();

        if (!empty($invoiceDetails)) {

            $invoicePreviousData = json_decode($invoiceDetails->invoiceParticulars, true);
            //print_r($invoicePreviousData);exit;
            $invoiceParticulars = $invoicePreviousData;
            $invoiceParticulars['shipment']['totalItemCost'] = $shipemntData->totalItemCost;
            $invoiceParticulars['shipment']['totalTax'] = '0.00';
            $invoiceParticulars['shipment']['isInsuranceCharged'] = 'N';
            $invoiceParticulars['shipment']['totalInsurance'] = '0.00';
            $invoiceParticulars['shipment']['inventoryCharge'] = '0.00';
            $invoiceParticulars['shipment']['otherChargeCost'] = '0.00';
            $invoiceParticulars['shipment']['storageCharge'] = '0.00';
            $invoiceParticulars['shipment']['maxStorageDate'] = 'N/A';
            $invoiceParticulars['shipment']['discount'] = '0.00';
            $invoiceParticulars['shipment']['shippingCost'] = '0.00';
            $invoiceParticulars['shipment']['clearingDutyCost'] = '0.00';
            $invoiceParticulars['shipment']['isDutyCharged'] = '0';
            $invoiceParticulars['shipment']['totalCost'] = $request->extraCost;
            //unset($invoiceParticulars['shipment']['shippingCost']);
            $invoiceParticulars['extracharge'] = array(
                'extraCost' => $request->extraCost,
                'notes' => $request->notes,
            );

            $totalCost = (!empty($shipemntData->totalCost) ? 0.00 : $shipemntData->totalCost);

            /*  INSERT DATA INTO INVOICE TABLE */
            $invoiceUniqueId = 'INV-EXC-' . $userInfo->unit . '-' . $shipemntData->id . '-' . date('Ymd');
            $newInvoice = $invoiceDetails->replicate();
            $newInvoice->invoiceUniqueId = $invoiceUniqueId;
            $newInvoice->extraCostCharged = 'Y';
            $newInvoice->extraCostAmount = $request->extraCost;
            $newInvoice->invoiceParticulars = json_encode($invoiceParticulars);
            $newInvoice->totalBillingAmount = (!empty($totalCost) ? $totalCost : 0);
            $newInvoice->invoiceType = 'invoice';
            $newInvoice->paymentMethodId = null;
            $newInvoice->paymentStatus = 'unpaid';
            $newInvoice->save();
            $invoiceId = $newInvoice->id;


            if (!empty($invoiceId)) {
                $data['invoice'] = \App\Model\Invoice::find($invoiceId);
                $fileName = "Invoice_" . $invoiceUniqueId . ".pdf";
                $data['pageTitle'] = "Print Invoice";
                PDF::loadView('Administrator.shipments.invoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                $to = $shipemntData->fromEmail;
                $frontendUrl = Config::get('constants.frontendUrl') . "payment/invoice/" . $invoiceId;
                $content = "Invoice for Extra Cost Charged for Shipment #" . $shipmentid . " is attached. Please find the cost page at <a href=" . $frontendUrl . ">" . $frontendUrl . "</a>";
                Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($invoiceUniqueId, $to, $fileName) {
                    $message->from('contact@shoptomydoor.com', 'Shoptomydoor');
                    $message->subject("$invoiceUniqueId - Invoice Details");
                    $message->to($to);
                    $message->attach(public_path('exports/invoice/' . $fileName));
                });

                //return 1;
                return \Redirect::to('administrator/shipments/addedit/' . $shipmentid . '/1')->with('successMessage', 'Extra invoice created successfully.');
            }
        }
    }

    public function wronginvoicedownload($shipmentId) {

        $fileName = \App\Model\Shipmentfile::where('shipmentId', $shipmentId)->first()->filename;
        $filePath = public_path() . '/uploads/shipments/' . $shipmentId . '/' . $fileName;
        return response()->download($filePath);
    }

    public function updatepackedstatus($shipmentid) {

        Shipment::where('id', $shipmentid)->update(['packed' => 'packing_complete']);
        return 1;
    }

    public function assignDispatchCompany($deliveryid, $page = '', Request $request) {
        if (\Request::isMethod('post')) {
            $shipment = new Shipment;
            $validator = Validator::make($request->all(), [
                        'shippingMethod' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            } else {

                if (!empty($shipmentid)) {
                    $shipment = Shipment::find($shipmentid);
                    if (!empty($shipment->shippingMethodId)) {
                        //return \Redirect::to('administrator/shipments/?page=' . $page)->with('successMessage', 'Dispatch company assigned successfully.');

                        return 1;
                    } else {
                        // return \Redirect::to('administrator/shipments/?page=' . $page)->withErrors('Dispatch company can only be assigned after assiging Shipping Method.');
                        return 0;
                    }
                }
            }
        }


        /* SET DATA FOR VIEW  */
        $data['pageTitle'] = "Assign Dispatch Company";
        $data['id'] = $deliveryid;
        $data['page'] = $page;
        $data['assignblock'] = 'delivery';
        $data['dispatchCompany'] = Dispatchcompany::where('status', '1')->orderby('name', 'asc')->get();

        return view('Administrator.shipments.assigndispatchcompany', $data);
    }
    
    public function assignpackagedispatchcompany($shipmentId, $page = '', Request $request) {
        if (\Request::isMethod('post')) {
            $shipment = new Shipment;
            $validator = Validator::make($request->all(), [
                        'shippingMethod' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            } else {

                if (!empty($shipmentid)) {
                    $shipment = Shipment::find($shipmentid);
                    if (!empty($shipment->shippingMethodId)) {
                        //return \Redirect::to('administrator/shipments/?page=' . $page)->with('successMessage', 'Dispatch company assigned successfully.');

                        return 1;
                    } else {
                        // return \Redirect::to('administrator/shipments/?page=' . $page)->withErrors('Dispatch company can only be assigned after assiging Shipping Method.');
                        return 0;
                    }
                }
            }
        }


        /* SET DATA FOR VIEW  */
        $data['pageTitle'] = "Assign Dispatch Company";
        $data['id'] = $shipmentId;
        $data['page'] = $page;
        $data['assignblock'] = 'package';
        $data['dispatchCompany'] = Dispatchcompany::where('status', '1')->orderby('name', 'asc')->get();

        return view('Administrator.shipments.assigndispatchcompany', $data);
    }

    public function downloaddiscountedinvoice($packageId) {
        $fileData = Shipmentpackage::find($packageId);
        //$filepath = public_path('uploads/image/')."abc.jpg";
        $filepath = public_path('/uploads/discounted_invoice/' . $fileData->itemDiscountedInvoiceFile);
        return Response::download($filepath);
    }

    public function downloadreturnlabel($shipmentId, $packageId) {
        $fileData = Shipmentpackage::find($packageId);
        //$filepath = public_path('uploads/image/')."abc.jpg";
        $filepath = public_path('/uploads/shipments/' . $shipmentId . '/' . $fileData->returnLabel);
        return Response::download($filepath);
    }

    public function updatecomment($deliveryId, $statusName) {

        $data['pageTitle'] = "Update Comments";
        $delivery = explode("-", $deliveryId);
        $data['deliveryId'] = $delivery[0];
        $data['shipmentId'] = $delivery[1];
        $data['statusName'] = $statusName;

        $shipmentStatusData = \App\Model\Shipmentstatuslog::where("shipmentId", $data['shipmentId'])->where("deliveryId", $deliveryId)->where('status', $statusName)->first();


        if (count($shipmentStatusData) == 0) {
            $data['msg'] = "Please update delivery date for this stage first";
        } else {
            if (!empty($shipmentStatusData['comments'])) {
                $data['comments'] = $shipmentStatusData['comments'];
            } else {
                $data['comments'] = "";
            }
            $data['msg'] = "";
        }


        return view('Administrator.shipments.updatecomment', $data);
    }

    public function savestatuscomment($deliveryId, $shipmentId, $statusName, Request $request) {
        $comments = $request['message'];

        $shipmentStatusData = \App\Model\Shipmentstatuslog::where("shipmentId", $shipmentId)->where("deliveryId", $deliveryId)->where('status', $statusName)->first();


        $update = \App\Model\Shipmentstatuslog::where('id', $shipmentStatusData['id'])->update(['comments' => $comments]);

        if ($update) {
            return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/1')->with('successMessage', 'Shipment updated successfully.');
        } else {

            return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/1')->with('errorMessage', 'Please enter comments');
        }
    }
    
    public function updatestatuscomment($shipmentId, $statusName) {

        $data['pageTitle'] = "Update Comments";
        $data['shipmentId'] = $shipmentId;
        $data['statusName'] = $statusName;
        $shipmentStatusData = array();
        $shipmentDelivery = Shipmentdelivery::where('shipmentId',$shipmentId)->where("deleted","0")->orderBy('id','asc')->first();
        if(!empty($shipmentDelivery))
        {
            $deliveryId = $shipmentDelivery->id;
            $shipmentStatusData = \App\Model\Shipmentstatuslog::where("shipmentId", $data['shipmentId'])->where("deliveryId", $deliveryId)->where('status', $statusName)->first();
        }
        
        if (count($shipmentStatusData) == 0) {
            $data['msg'] = "Please update delivery date for this stage first";
        } else {
            if (!empty($shipmentStatusData['comments'])) {
                $data['comments'] = $shipmentStatusData['comments'];
            } else {
                $data['comments'] = "";
            }
            $data['msg'] = "";
        }


        return view('Administrator.shipments.updatecommentpackage', $data);
    }

    public function savestatuscommentpackage($shipmentId, $statusName, Request $request) {
        $comments = $request['message'];
        
        $deliveryDetails = Shipmentdelivery::select("id")->where("deleted","0")->where("shipmentId",$shipmentId)->get();
        foreach ($deliveryDetails as $eachDelivery) {
            
            $deliveryId = $eachDelivery->id;
            $shipmentStatusData = \App\Model\Shipmentstatuslog::where("shipmentId", $shipmentId)->where("deliveryId", $deliveryId)->where('status', $statusName)->first();
            $update = \App\Model\Shipmentstatuslog::where('id', $shipmentStatusData['id'])->update(['comments' => $comments]);        
        }

        

        if ($update) {
            return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/1')->with('successMessage', 'Shipment updated successfully.');
        } else {

            return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/1')->with('errorMessage', 'Please enter comments');
        }
    }

    public function processreturn($shipmentId, $deliveryId, $packageId, $page = 0, Request $request) {

        $data = array();
        $allItemReturned = 0;

//        $numDeliveryPackage = Shipmentpackage::where('deliveryId',$deliveryId)->where('deleted','0')->get()->count();
//        $numDeliveryPackageReturned = Shipmentpackage::where('itemReturn','!=','0')->where('deliveryId',$deliveryId)->where('deleted','0')->get()->count();
        $allItemReturned = Shipmentpackage::allItemReturned(0, $deliveryId, "delivery", true);

        if (\Request::isMethod('post')) {

            if ($allItemReturned == "1") {
                $chargeableWeight = "0.00";
            } else {
                $chargeableWeight = $request->chargeableWt;
            }

            $shipmentPackageData = Shipmentpackage::find($packageId);
            $shipmentDeliveryData = Shipmentdelivery::find($deliveryId);

            $chargePerUnitSettings = \App\Model\Generalsettings::where('settingsKey', 'unit_weight_charge')->first();
            $storageCharge = round(($chargeableWeight * $chargePerUnitSettings->settingsValue), 2);
            $storageChargeToDeductFromShipment = round(($shipmentDeliveryData->storageCharge - $storageCharge), 2);


            Shipment::find($shipmentId)->decrement('storageCharge', $storageChargeToDeductFromShipment);

            $shipmentDeliveryData->chargeableWeight = $chargeableWeight;
            $shipmentDeliveryData->totalItemCost = round(($shipmentDeliveryData->totalItemCost - $shipmentPackageData->itemTotalCost), 2);
            if ($allItemReturned == "1") {
                $shipmentDeliveryData->totalCost = round(($shipmentDeliveryData->totalCost - $shipmentDeliveryData->inventoryCharge), 2);
                $shipmentDeliveryData->inventoryCharge = "0.00";
            }
            $shipmentDeliveryData->storageCharge = $storageCharge;
            $shipmentDeliveryData->save();

            $shipmentPackageData->itemReturn = "2";
            $shipmentPackageData->save();

            $allItemReturnedOfShipment = Shipmentpackage::allItemReturned($shipmentId, 0, "shipment");
            if ($allItemReturnedOfShipment == 1) {
                Shipment::where('id', $shipmentId)->update(['hideFromCustomer' => 'Y']);
            }

            return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/1')->with('successMessage', 'Shipment updated successfully.');
        }

        if ($allItemReturned == "1") {
            $chargeableWeight = "0.00";
        } else {
            $deliveryInfo = Shipmentdelivery::select('chargeableWeight')->find($deliveryId);
            $chargeableWeight = $deliveryInfo->chargeableWeight;
        }

        $data['shipmentId'] = $shipmentId;
        $data['deliveryId'] = $deliveryId;
        $data['packageId'] = $packageId;
        $data['page'] = $page;
        $data['chargeableWeight'] = $chargeableWeight;
        $data['allItemReturned'] = $allItemReturned;

        return view('Administrator.shipments.confirmreturn', $data);
    }

    ////////////////////////////////////////Received Payment offline//////////////////////////////////////////

    public function receivedPayment($shipmentId, $page = 0, Request $request) {
        $data = array();


        $data['pageTitle'] = "Payment Details";
        $data['id'] = $shipmentId;
        $data['page'] = $page;
        $data['paymentMethod'] = Paymentmethod::where("status", "1")->where("deleted", "0")->orderBy('status', 'desc')->get();
        /* FETCH SHIPPING METHOD LIST  */
        $data['shippingMethod'] = Shippingmethods::where('active', 'Y')->where('deleted', '0')->orderby('shipping', 'asc')->get();

        return view('Administrator.shipments.receivedPayment', $data);
    }

    public function savePaymentDetails($shipmentId, $page = 0, Request $request) {

        //$comments = $request['message'];
        //fetch shipment details

        $shipment = Shipment::find($shipmentId);

        $userInfo = User::find($shipment->userId);


        //fetch delivery details

        $deliveryDetails = array();

        $deliveryPackageData = Shipmentdelivery::where('shipmentId', $shipmentId)->get();
        if (!empty($deliveryPackageData)) {
            foreach ($deliveryPackageData as $key => $delivery) {
                $shipmentPackageData = Shipmentpackage::where('shipmentId', $shipmentId)->where('deliveryId', $delivery['id'])->get();
                $deliveryDetails[$key] = array(
                    'id' => $delivery['id'],
                    'totalQuantity' => $delivery['totalQty'],
                    'shippingMethodId' => $delivery['shippingMethodId'],
                    'shippingMethod' => $delivery['shippingMethod'],
                    'totalItemCost' => $delivery['totalItemCost'],
                    'shippingCost' => $delivery['shippingCost'],
                    'clearingDutyCost' => $delivery['clearingDutyCost'],
                    'isDutyCharged' => $delivery['isDutyCharged'],
                    'inventoryCharge' => $delivery['inventoryCharge'],
                    'otherChargeCost' => $delivery['otherChargeCost'],
                    'totalCost' => $delivery['totalDeliveryCost'],
                );

                foreach ($shipmentPackageData as $count => $shipmentPackage) {
                    $deliveryDetails[$key]['packages'][$count] = array(
                        'id' => $shipmentPackage->id,
                        'itemName' => $shipmentPackage->itemName,
                        'websiteUrl' => $shipmentPackage->websiteUrl,
                        'options' => $shipmentPackage->options,
                        'itemPrice' => $shipmentPackage->itemPrice,
                        'itemQuantity' => $shipmentPackage->itemQuantity,
                        'itemShippingCost' => $shipmentPackage->itemShippingCost,
                        'itemTotalCost' => $shipmentPackage->itemTotalCost,
                        'trackingNumber' => $shipmentPackage->tracking,
                    );
                }
            }
        }


        /*  PREPARE DATA FOR INVOICE PARTICULARS */
        $invoiceData = array(
            'shipment' => array(
                'totalItemCost' => $shipment->totalItemCost,
                'totalShippingCost' => $shipment->totalShippingCost,
                'totalClearingDuty' => $shipment->totalClearingDuty,
                'isDutyCharged' => $shipment->isDutyCharged,
                'totalInsurance' => $shipment->totalInsurance,
                'totalTax' => $shipment->totalTax,
                'totalCost' => (!empty($request->paidAmount) ? $request->paidAmount : $shipment->totalCost),
                'totalWeight' => $shipment->totalWeight,
                'totalQuantity' => $shipment->itemQuantity,
                'totalOtherCharges' => $shipment->totalOtherCharges,
                'totalDiscount' => $shipment->totalDiscount,
                'storageCharge' => $shipment->storageCharge
            ),
            'warehouse' => array(
                'fromAddress' => $shipment->fromAddress,
                'fromZipCode' => $shipment->fromZipCode,
                'fromCountry' => $shipment->fromCountry,
                'fromState' => $shipment->fromState,
                'fromCity' => $shipment->fromCity,
            ),
            'shippingaddress' => array(
                'toCountry' => $shipment->toCountry,
                'toState' => $shipment->toState,
                'toCity' => $shipment->toCity,
                'toAddress' => $shipment->toAddress,
                'toZipCode' => $shipment->toZipCode,
                'toName' => $shipment->toName,
                'toEmail' => $shipment->toEmail,
                'toPhone' => $shipment->toPhone,
            ),
            'deliveries' => $deliveryDetails,
            'payment' => array(
                'paymentMethodId' => $request->paymentMethodId,
                'paymentMethodName' => Paymentmethod::find($request->paymentMethodId)->paymentMethod,
            ),
        );

        //print_r($invoiceData); exit;
        //Insert row in Invoice//

        $invoice = new \App\Model\Invoice;

        $invoice->invoiceUniqueId = "REC" . $userInfo->unit . "-" . $shipment->id . '-' . date('Ymd');
        $invoice->shipmentId = $shipmentId;
        $invoice->type = $shipment->shipmentType;
        $invoice->invoiceType = "receipt";
        $invoice->userUnit = $userInfo->unit;
        $invoice->userFullName = $shipment->fromName;
        $invoice->userEmail = $shipment->fromEmail;
        $invoice->userContactNumber = $shipment->fromPhone;

        /*  FETCH ADDRESS BOOK DETAILS */
        $addressBookData = \App\Model\Addressbook::where('userId', $shipment->userId)->where('isDefaultBilling', '1')->where('deleted', '0')->with('country', 'state', 'city')->first();
        $invoice->billingName = $addressBookData->title . ' ' . $addressBookData->firstName . ' ' . $addressBookData->lastName;
        $invoice->billingEmail = $addressBookData->email;
        $invoice->billingAddress = $addressBookData->address;
        $invoice->billingAlternateAddress = $addressBookData->alternateAddress;
        $invoice->billingCity = isset($addressBookData->city) ? $addressBookData->city->name : '';
        $invoice->billingState = isset($addressBookData->state) ? $addressBookData->state->name : '';
        $invoice->billingCountry = $addressBookData->country->name;
        $invoice->billingZipcode = $addressBookData->zipcode;
        $invoice->billingPhone = $addressBookData->phone;
        $invoice->billingAlternatePhone = $addressBookData->alternatePhone;
        $invoice->totalBillingAmount = !empty($request->paidAmount) ? $request->paidAmount : $shipment->totalCost;
        $invoice->paymentMethodId = $request->paymentMethodId;
        $invoice->paymentStatus = 'paid';
        $invoice->invoiceParticulars = json_encode($invoiceData);
        $invoice->createdOn = Config::get('constants.CURRENTDATE');
        $invoice->save();

        $invoiceId = $invoice->id;


        if (!empty($invoiceId)) {
            $data['invoice'] = \App\Model\Invoice::find($invoiceId);
            $fileName = "Receipt_" . $invoice->invoiceUniqueId . ".pdf";

            PDF::loadView('Administrator.shipments.invoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');

            $frontendUrl = Config::get('constants.frontendUrl') . "payment/invoice/" . $invoiceId;
            $content = "Receipt for Offline Payment for Shipment #" . $shipmentId . " is attached. Please find the cost page at <a href=" . $frontendUrl . ">" . $frontendUrl . "</a>";
            $to = $userInfo->email;

            Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($shipment, $to, $fileName) {
                $message->from('contact@shoptomydoor.com', 'Shoptomydoor');
                $message->subject('Shipment Receipt');
                $message->to($to);
                $message->attach(public_path('exports/invoice/' . $fileName));
            });
        }
        /*  UPDATE PAYMENT STATUS IN SHIPMENT TABLE */

        $shipment->paymentStatus = 'paid';
        $shipment->paymentMethodId = $request->paymentMethodId;
        $shipment->paymentReceivedOn = $request->paidOn;
        $shipment->totalCost = $request->paidAmount;
        $shipment->paidByUserType = 'admin';
        $shipment->paidByUserId = Auth::user()->id;
        $shipment->save();

        Shipmentdelivery::where("shipmentId", $shipmentId)->where("deleted", "0")->update(["shippingMethodId" => $request->shippingMethodId]);

        $orderObj = new Order;
        $orderObj->shipmentId = $shipmentId;
        $orderObj->userId = $shipment->userId;
        $orderObj->totalCost = $request->paidAmount;
        $orderObj->status = '2';
        $orderObj->type = 'shipment';
        $orderObj->createdBy = Auth::user()->id;
        $orderObj->save();
        $orderNumber = \App\Model\Order::generateOrderNumber($orderObj->id, $orderObj->userId);
        $orderObj->orderNumber = $orderNumber;
        //Newly added 09/09/2019
        $orderObj->createdDate = Config::get('constants.CURRENTDATE');
        $orderObj->save();

        //Insert record in Payment Transaction////

        $shipmentStatusData = new \App\Model\Paymenttransaction;

        $shipmentStatusData->paymentMethodId = $request->paymentMethodId;
        $shipmentStatusData->paidFor = 'othershipment';
        $shipmentStatusData->paidForId = $shipmentId;
        $shipmentStatusData->amountPaid = $request->paidAmount;
        $shipmentStatusData->transactionOn = $request->paidOn;
        $shipmentStatusData->status = 'paid';


        $save = $shipmentStatusData->save();

        if ($save) {
            Shipment::notificationEmail($shipmentId);

            $replace = array();
            $replace['[SHIPMENTID]'] = $shipmentId;
            $replace['[ORDERID]'] = $orderNumber;
            User::sendPushNotification($shipment->userId, 'order_created', Auth::user()->id, $replace);

            return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/1')->with('successMessage', 'Shipment updated successfully.');
        } else {

            return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/1')->with('errorMessage', 'Please enter comments');
        }
    }

    ////////////////////////////////////////Received Payment offline//////////////////////////////////////////

    public function getwerehouselocations(Request $request) {

        $listIds = str_replace('^', ',', $request->listIds);
        $locationRecords = \App\Model\Shipment::getShipmentLocations($listIds);
        $deliveryCompanyRecords = \App\Model\ViewShipmentDetails::select('deliveryCompany', 'dispatchCompany', 'shipmentId')->whereRaw("shipmentId in (" . $listIds . ")")->get();

        $returnLocationArr = array();
        $returnDeliveryArr = array();
        $returndispatchCompany = array();
        if (!empty($locationRecords)) {
            foreach ($locationRecords as $shipment) {
                $locations = explode(",", $shipment->shipmentLocation);
                $printedLocations = array();
                $displayLocation = '';
                foreach ($locations as $eachLocations) {
                    if (!in_array($eachLocations, $printedLocations)) {
                        $displayLocation .= $eachLocations . ',';
                        $printedLocations[] = $eachLocations;
                    }
                }
                $displayLocation = substr($displayLocation, 0, '-1');
                $returnLocationArr[$shipment->id] = $displayLocation;
            }
        }
        if (!empty($deliveryCompanyRecords)) {
            foreach ($deliveryCompanyRecords as $delivery) {
                $returnDeliveryArr[$delivery->shipmentId] = $delivery->deliveryCompany;
                $returndispatchCompany[$delivery->shipmentId] = $delivery->dispatchCompany;
            }
        }
        //print_r($returndispatchCompany); die;

        if (!empty($returnLocationArr)) {
            return json_encode(array('returnLocationArr' => $returnLocationArr, 'returnDeliveryArr' => $returnDeliveryArr, 'returndispatchCompany' => $returndispatchCompany));
        } else {
            return json_encode(array());
        }
    }

    public function generateInvoice($shipmentId, $shipmentType) {
        $shipment = Shipment::find($shipmentId);

        $userInfo = User::find($shipment->userId);

        //Payment transaction data if available

        $paymentTransactionData = \App\Model\Paymenttransaction::where('paidFor', $shipmentType)->where('paidForId', $shipmentId)->get();

        //fetch delivery details

        $deliveryDetails = array();

        $deliveryPackageData = Shipmentdelivery::where('shipmentId', $shipmentId)->get();
        if (!empty($deliveryPackageData)) {
            foreach ($deliveryPackageData as $key => $delivery) {
                $shipmentPackageData = Shipmentpackage::where('shipmentId', $shipmentId)->where('deliveryId', $delivery['id'])->get();
                $deliveryDetails[$key] = array(
                    'id' => $delivery['id'],
                    'totalQuantity' => $delivery['totalQty'],
                    'shippingMethodId' => $delivery['shippingMethodId'],
                    'shippingMethod' => $delivery['shippingMethod'],
                    'totalItemCost' => $delivery['totalItemCost'],
                    'shippingCost' => $delivery['shippingCost'],
                    'clearingDutyCost' => $delivery['clearingDutyCost'],
                    'isDutyCharged' => $delivery['isDutyCharged'],
                    'inventoryCharge' => $delivery['inventoryCharge'],
                    'otherChargeCost' => $delivery['otherChargeCost'],
                    'totalCost' => $delivery['totalDeliveryCost'],
                );

                foreach ($shipmentPackageData as $count => $shipmentPackage) {
                    $deliveryDetails[$key]['packages'][$count] = array(
                        'id' => $shipmentPackage->id,
                        'itemName' => $shipmentPackage->itemName,
                        'websiteUrl' => $shipmentPackage->websiteUrl,
                        'options' => $shipmentPackage->options,
                        'itemPrice' => $shipmentPackage->itemPrice,
                        'itemQuantity' => $shipmentPackage->itemQuantity,
                        'itemShippingCost' => $shipmentPackage->itemShippingCost,
                        'itemTotalCost' => $shipmentPackage->itemTotalCost,
                        'trackingNumber' => $shipmentPackage->tracking,
                    );
                }
            }
        }


        /*  PREPARE DATA FOR INVOICE PARTICULARS */
        $invoiceData = array(
            'shipment' => array(
                'totalItemCost' => $shipment->totalItemCost,
                'totalShippingCost' => $shipment->totalShippingCost,
                'totalClearingDuty' => $shipment->totalClearingDuty,
                'isDutyCharged' => $shipment->isDutyCharged,
                'totalInsurance' => $shipment->totalInsurance,
                'totalTax' => $shipment->totalTax,
                'totalCost' => (!empty($request->paidAmount) ? $request->paidAmount : $shipment->totalCost),
                'totalWeight' => $shipment->totalWeight,
                'totalQuantity' => $shipment->itemQuantity,
                'totalOtherCharges' => $shipment->totalOtherCharges,
                'totalDiscount' => $shipment->totalDiscount,
                'storageCharge' => $shipment->storageCharge
            ),
            'warehouse' => array(
                'fromAddress' => $shipment->fromAddress,
                'fromZipCode' => $shipment->fromZipCode,
                'fromCountry' => $shipment->fromCountry,
                'fromState' => $shipment->fromState,
                'fromCity' => $shipment->fromCity,
            ),
            'shippingaddress' => array(
                'toCountry' => $shipment->toCountry,
                'toState' => $shipment->toState,
                'toCity' => $shipment->toCity,
                'toAddress' => $shipment->toAddress,
                'toZipCode' => $shipment->toZipCode,
                'toName' => $shipment->toName,
                'toEmail' => $shipment->toEmail,
                'toPhone' => $shipment->toPhone,
            ),
            'deliveries' => $deliveryDetails,
            'payment' => array(
                'paymentMethodId' => $shipment->paymentMethodId,
                'paymentMethodName' => Paymentmethod::find($shipment->paymentMethodId)->paymentMethod,
            ),
        );

        //print_r($invoiceData); exit;
        //Insert row in Invoice//

        $invoice = new \App\Model\Invoice;

        $invoice->invoiceUniqueId = "REC" . $userInfo->unit . "-" . $shipment->id . '-' . date('Ymd');
        $invoice->shipmentId = $shipmentId;
        $invoice->type = $shipment->shipmentType;
        $invoice->invoiceType = "receipt";
        $invoice->userUnit = $userInfo->unit;
        $invoice->userFullName = $shipment->fromName;
        $invoice->userEmail = $shipment->fromEmail;
        $invoice->userContactNumber = $shipment->fromPhone;

        /*  FETCH ADDRESS BOOK DETAILS */
        $addressBookData = \App\Model\Addressbook::where('userId', $shipment->userId)->where('isDefaultBilling', '1')->where('deleted', '0')->with('country', 'state', 'city')->first();
        $invoice->billingName = $addressBookData->title . ' ' . $addressBookData->firstName . ' ' . $addressBookData->lastName;
        $invoice->billingEmail = $addressBookData->email;
        $invoice->billingAddress = $addressBookData->address;
        $invoice->billingAlternateAddress = $addressBookData->alternateAddress;
        $invoice->billingCity = isset($addressBookData->city) ? $addressBookData->city->name : '';
        $invoice->billingState = isset($addressBookData->state) ? $addressBookData->state->name : '';
        $invoice->billingCountry = $addressBookData->country->name;
        $invoice->billingZipcode = $addressBookData->zipcode;
        $invoice->billingPhone = $addressBookData->phone;
        $invoice->billingAlternatePhone = $addressBookData->alternatePhone;
        $invoice->totalBillingAmount = (!empty($shipment->totalCost) ? $shipment->totalCost : 0);
        $invoice->paymentMethodId = $shipment->paymentMethodId;
        $invoice->paymentStatus = 'paid';
        $invoice->invoiceParticulars = json_encode($invoiceData);
        $invoice->createdOn = Config::get('constants.CURRENTDATE');

        $save = $invoice->save();

        if (empty($paymentTransactionData)) {
            $shipmentStatusData = new \App\Model\Paymenttransaction;
            $shipmentStatusData->paymentMethodId = $request->paymentMethodId;
            $shipmentStatusData->paidFor = 'othershipment';
            $shipmentStatusData->paidForId = $shipmentId;
            $shipmentStatusData->amountPaid = $shipment->totalCost;
            $shipmentStatusData->status = 'paid';
            $shipmentStatusData->paidFor = $shipmentType;
            $shipmentStatusData->save();
        }
        $invoiceId = $invoice->id;


        if (!empty($invoiceId)) {
            $data['invoice'] = \App\Model\Invoice::find($invoiceId);
            $fileName = "Receipt_" . $invoice->invoiceUniqueId . ".pdf";

            PDF::loadView('Administrator.shipments.invoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
        }

        if ($save) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getdeliverydetails(Request $request) {

        $id = $request->shipmentId;

        \Session::forget('SHIPMENTDETAILS');
        \Session::push('SHIPMENTDETAILS.isviewed', '1');
        \Session::push('SHIPMENTDETAILS.shipmentId', $id);


        $deliveryData = ViewShipmentDetails::where("shipmentId", $id)->where('deleted', '0')->where('packageType', 'I')->orderBy('deliveryId', 'asc')->get()->toarray();
        $deletedPackages = ViewShipmentDetails::where("shipmentId", $id)->where('deleted', '0')->where('packageDeleted', '1')->where('packageType', 'I')->orderBy('deliveryId', 'asc')->get()->toarray();
        $shipment = Shipment::find($id);
        $data['shipment'] = $shipment;
        $data['shipment']['delivery'] = Shipment::getDeliveryDetails($deliveryData);
        $data['shipment']['deletedPackage'] = array();
        if(!empty($deletedPackages))
        {
            $data['shipment']['deletedPackage'] = Shipment::getDeliveryDetails($deletedPackages);
        }
        //exit;
        $data['deliverySnapshotCount'] = \App\Model\Shipmentsnapshot::deliverywiseSnapshot($id);
        $data['statusLog'] = \App\Model\Shipmentstatuslog::getDeliverywiseStatusLog($id);
        /* Fetch Shipment Other Charges */
        $data['shipment']['othercharges'] = Shipmentothercharges::getDeliverywiseCharges($id);
        /* Fetch Shipment Other Charges */
        $data['shipment']['manualcharges'] = Shipmentothercharges::getDeliverywiseCharges($id, 'manual');

        /* Fetch Shipment Total Charges */
        //$data['shipment']['totalCharge'] = $this->getShipmentTotalCost($id);
        //echo '<pre>';print_r($data['shipment']);echo '</pre>';
        $data['orderData'] = Order::where('shipmentId', $id)->first();

        /* FETCH STORE LIST  */
        $data['storeList'] = \App\Model\Stores::where('deleted', '0')->where('status', '1')->orderby('storeName', 'asc')->get();

        /* FETCH CATEGORY LIST  */
        $data['categoryList'] = \App\Model\Sitecategory::getCategoryList('parent');
        $data['shipment']['deleteddelivery'] = Shipment::getDeletedDelivery($id);

        $shippingMethodData = collect(Shippingmethods::all());
        $data['shippingMethodList'] = $shippingMethodData->mapWithKeys(function($item) {
            return [$item['shippingid'] => $item['shipping']];
        });

        $data['shippingMethodLogoList'] = $shippingMethodData->mapWithKeys(function($item) {
            return [$item['shippingid'] => $item['companyLogo']];
        });

        //exit;

        /* FETCH OTHER CHARGES LIST */
        $data['otherChargesList'] = \App\Model\Othershipmentcharges::where('deleted', '0')->where('status', '1')->orderby('orderby', 'asc')->get();
        $data['page'] = '1';
        if ($request->shipmentType == 'othershipment')
            return view('Administrator.shipments.deliverydetails', $data);
        else if ($request->shipmentType == 'autoparts')
            return view('Administrator.shipments.deliverydetailsautopart', $data);
        else if ($request->shipmentType == 'shopforme')
            return view('Administrator.shipments.deliverydetailsshopforme', $data);
    }

    public function getavailableshipping(Request $request) {

        $userDetails = User::where("unit", $request->userunit)->first();
        $userShippingAddress = Addressbook::where('userId', $userDetails->id)->where('deleted', '0')->where('isDefaultShipping', '1')->first();
        $warehouseData = \App\Model\Warehouse::find($request->warehouseId);

        $shipmentdata = array(
            'toCountry' => $userShippingAddress->countryId,
            'toState' => $userShippingAddress->stateId,
            'toCity' => $userShippingAddress->cityId,
            'warehouseCountry' => $warehouseData->countryId,
            'warehouseState' => $warehouseData->stateId,
            'warehouseCity' => $warehouseData->cityId,
        );
        $chargeableWeightFactor = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();
        $chargeableWeight = ($request->length * $request->height * $request->width) / $chargeableWeightFactor->settingsValue;
        $shipmentDetailsData = array(0 => array("deliveryChargeableWeight" => $chargeableWeight, "itemTotalCost" => $request->totalCost));
        $availableShippingMethods = Shippingmethods::getDeliveryShippingMethods($shipmentdata, $shipmentDetailsData);

        if (empty($availableShippingMethods)) {
            return json_encode(array('status' => 0, 'data' => array()));
        } else {
            return json_encode(array('status' => 1, 'data' => $availableShippingMethods));
        }
    }
    
    public function updatepackagestage($shipmentId,Request $request) {
//        print_r($request->all());exit;
        
        $deliveryDetails = Shipmentdelivery::select("id","shippingMethodId")->where("deleted","0")->where("shipmentId",$shipmentId)->get();
        
        foreach ($request->status as $eachStatus => $dates) {
            if (!empty($dates)) {
                foreach ($deliveryDetails as $eachDelivery) {
                    $deliveryId = $eachDelivery->id;

                    if (!empty($eachDelivery->shippingMethodId)) {
                        $shippingMethod = Shippingmethods::where('shippingId', $eachDelivery->shippingMethodId)->get();

                        $expectedDispatchDate = \Carbon\Carbon::now()->addWeekDays($shippingMethod[0]->days)->format('Y-m-d h:i:s');
                        $dispatchCompanyId = $request->assignPackageCompany;

                        $delivery = Shipmentdelivery::find($eachDelivery->id);

                        $delivery->dispatchCompanyId = $dispatchCompanyId;
                        $delivery->expectedDispatchDate = $expectedDispatchDate;
                        $delivery->save();
                    }


                    $shipmentStatusData = \App\Model\Shipmentstatuslog::where("shipmentId", $shipmentId)->where("deliveryId", $deliveryId)->orderBy("id", "desc")->first();
                    $shipmentStatus = \App\Model\Shipmentstatuslog::where("shipmentId", $shipmentId)->where("deliveryId", $deliveryId)->where("status", $eachStatus)->orderBy("id", "desc")->first();
                    if (empty($shipmentStatus))
                        $shipmentStatus = new \App\Model\Shipmentstatuslog;
                    $shipmentStatus->shipmentId = $shipmentId;
                    $shipmentStatus->deliveryId = $deliveryId;
                    $shipmentStatus->oldStatus = (!empty($shipmentStatusData->status) ? $shipmentStatusData->status : 'none');
                    $shipmentStatus->status = $eachStatus;
                    $shipmentStatus->updatedOn = date('Y-m-d h:i:s', strtotime($dates));
                    $shipmentStatus->save();
                    $storeStatusName = $eachStatus;
                }

                $shipmentPackageStatus = \App\Model\Shipmentpackagestatuslog::where('shipmentId', $shipmentId)->first();
                if (empty($shipmentPackageStatus)) {
                    $shipmentPackageStatus = new \App\Model\Shipmentpackagestatuslog;
                    $shipmentPackageStatus->createdOn = Config::get('constants.CURRENTDATE');
                    $shipmentPackageStatus->createdBy = Auth::user()->id;
                    $shipmentPackageStatus->shipmentId = $shipmentId;
                }
                $shipmentPackageStatus->$eachStatus = date('Y-m-d h:i:s', strtotime($dates));
                $shipmentPackageStatus->save();
            }
        }

        $shimentOrg = Shipment::find($shipmentId);
        if ($storeStatusName == 'in_warehouse') {
            $stausSaveNo = "1";
        } else if ($storeStatusName == 'in_transit') {
            $stausSaveNo = "2";
        } else if ($storeStatusName == 'custom_clearing') {
            $stausSaveNo = "3";
        } else if ($storeStatusName == 'destination_warehouse') {
            $stausSaveNo = "4";
        } else if ($storeStatusName == 'out_for_delivery') {
            $stausSaveNo = "5";
        } else if ($storeStatusName == 'delivered') {
            $stausSaveNo = "6";
        } else {
            $stausSaveNo = "0";
        }

        $shimentOrg->shipmentStatus = $stausSaveNo;
        $shimentOrg->save();
//                echo $stausSaveNo;exit;
        $deliveryStatusList = Shipmentdelivery::deliveryStatus();
        $deliveryUpdateStatus = array_keys($deliveryStatusList);
        $replace = array();
        $customer = User::find($shimentOrg->userId);
        $orderInfo = Order::where('shipmentId', $shipmentId)->first();
        $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'shipment_status_change')->first();

        $smsTemplate = \App\Model\Smstemplate::where('templateType', 'shipment_status_change')->get();

        $statusIndex = $deliveryUpdateStatus[$stausSaveNo - 1];
        $replace['[NAME]'] = $customer->firstName . " " . $customer->lastName;
        $replace['[SHIPMENTID]'] = $shipmentId;
        $replace['[SHIPMENT_STATUS]'] = $deliveryStatusList[$statusIndex];
        if (!empty($orderInfo))
            $replace['[ORDERID]'] = $orderInfo->orderNumber;
        else
            $replace['[ORDERID]'] = "";
        $replace['[WEBSITE_LINK]'] = "<a href='https://shoptomydoor.com/'>shoptomydoor.com</a>";


        $to = $customer->email;
        $toMobile = $customer->contactNumber;
        if(!empty($emailTemplate))
            $sendMail = customhelper::SendMail($emailTemplate, $replace, $to);
        if($smsTemplate->count()>0)
            $smsSent = customhelper::sendMSG($toMobile, $replace, $smsTemplate);

        User::sendPushNotification($shimentOrg->userId, 'shipment_status_change', Auth::user()->id, $replace);
        return 1;
    }
    
    public function downloadlabel($labelId) {
        
        $fileData = \App\Model\Shipmentlabelprintlog::find($labelId);
        //$filepath = public_path('uploads/image/')."abc.jpg";
        $filepath = public_path('/uploads/shipments/' . $fileData->shipmentId . '/' . $fileData->labelFile);
        return Response::download($filepath);
    }

}
