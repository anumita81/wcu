<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Sitepage;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use customhelper;

class SitepageController extends Controller {

    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function staticpage($pagetype = 'staticpage') {

        $data = array();

        $staticpageObj = new Sitepage();

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */



            $sortField = \Input::get('field', 'id');
            $sortOrder = \Input::get('type', 'desc');
            $perpage = \Input::get('searchDisplay', $this->_perPage);
            $searchData = \Input::get('searchData', "");

            if ($pagetype == 'landingpage') {
                /*  SET SESSION VALUE FOR SORTING  */

                \Session::forget('RECORD');
                \Session::push('RECORD.searchData', $searchData);
                \Session::push('RECORD.searchDisplay', $perpage);
                \Session::push('RECORD.field', $sortField);
                \Session::push('RECORD.type', $sortOrder);

                $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Landingpage'), Auth::user()->id); // call the helper function
                if($findRole['canView'] == 0){
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }


            } else {
                \Session::forget('RECORD2');
                \Session::push('RECORD2.searchData', $searchData);
                \Session::push('RECORD2.searchDisplay', $perpage);
                \Session::push('RECORD2.field', $sortField);
                \Session::push('RECORD2.type', $sortOrder);

                $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Staticpage'), Auth::user()->id); // call the helper function
                if($findRole['canView'] == 0){
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
            }
        } else {
            if ($pagetype == 'landingpage') {
                $sortField = \Session::get('RECORD.field');
                $sortType = \Session::get('RECORD.type');
                $perpage = \Session::get('RECORD.searchDisplay');
                $searchData = \Session::get('RECORD.searchData');

                $sortField = !empty($sortField) ? $sortField[0] : 'id';
                $sortOrder = !empty($sortType) ? $sortType[0] : 'desc';
                $perpage = !empty($perpage) ? $perpage[0] : $this->_perPage;
                $searchData = !empty($searchData) ? $searchData[0] : "";

                $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Landingpage'), Auth::user()->id); // call the helper function
                if($findRole['canView'] == 0){
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }


            } else {
                $sortField = \Session::get('RECORD2.field');
                $sortType = \Session::get('RECORD2.type');
                $perpage = \Session::get('RECORD2.searchDisplay');
                $searchData = \Session::get('RECORD2.searchData');

                $sortField = !empty($sortField) ? $sortField[0] : 'id';
                $sortOrder = !empty($sortType) ? $sortType[0] : 'desc';
                $perpage = !empty($perpage) ? $perpage[0] : $this->_perPage;
                $searchData = !empty($searchData) ? $searchData[0] : "";

                $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Staticpage'), Auth::user()->id); // call the helper function
                if($findRole['canView'] == 0){
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
            }
        }

        $param['sortField'] = $sortField;
        $param['sortOrder'] = $sortOrder;
        $param['searchDisplay'] = $perpage;
        $param['searchData'] = $searchData;
        $param['type'] = '1';
        if ($pagetype == 'landingpage')
            $param['type'] = 2;
        $records = $staticpageObj->getData($param);

        $data['param'] = $param;
        $data['page'] = $records->currentPage();
        $data['records'] = $records;
        $data['pagetype'] = $pagetype;
        $data['forntendUrl'] = Config::get('constants.frontendUrl');
        if ($pagetype == 'staticpage') {
            $data['pageTitle'] = 'Static Pages';
            $data['title'] = "Static Pages :: ADMIN - Shoptomydoor";
            $data['contentTop'] = array('breadcrumbText' => array('Site Content', 'Static Pages'), 'contentTitle' => 'Static Pages', 'pageInfo' => 'This section allows you to manage static pages');
        } else {
            $data['pageTitle'] = 'Landing Pages';
            $data['title'] = "Landing Pages :: ADMIN - Shoptomydoor";
            $data['contentTop'] = array('breadcrumbText' => array('Site Content', 'Landing Pages'), 'contentTitle' => 'Landing Pages', 'pageInfo' => 'This section allows you to manage landing pages');
        }

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.sitepage.index', $data);
    }

    public function addeditpage($id = -1, $pagetype = 'staticpage', $page = 1) {

        $staticpageObj = new Sitepage();
        if ($id != -1) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $record = $staticpageObj->find($id);
            $data['record'] = $record;
        } else {
            $data['action'] = 'Add';
            $data['record'] = $staticpageObj;
        }
        $data['pagetype'] = $pagetype;
        $data['type'] = 1;
        $data['page'] = $page;
        if ($pagetype == 'landingpage')
            $data['type'] = 2;
        if ($pagetype == 'staticpage') {
            $data['pageTitle'] = $data['action'] . ' Static Pages';
            $data['title'] = "Static Pages :: ADMIN - Shoptomydoor";
            $data['contentTop'] = array('breadcrumbText' => array('Site Content', 'Static Pages'), 'contentTitle' => 'Static Pages', 'pageInfo' => 'This section allows you to manage static pages');
        } else {
            $data['pageTitle'] = $data['action'] . ' Landing Pages';
            $data['title'] = "Landing Pages :: ADMIN - Shoptomydoor";
            $data['contentTop'] = array('breadcrumbText' => array('Site Content', 'Landing Pages'), 'contentTitle' => 'Landing Pages', 'pageInfo' => 'This section allows you to manage static pages');
        }
        return view('Administrator.sitepage.addeditpage', $data);
    }

    public function addeditpagerecord($id = -1, $pagetype = 'staticpage', $page = '-1', Request $request) {

        $staticpageObj = new Sitepage();
        $uploadImage = 0;

        if ($request->logo) {
            $this->validate($request, [
                'logo' => 'image|mimes:jpeg,png,jpg,gif|max:4000|dimensions:min_width=1600,min_height=626',
            ]);

            $validator = Validator::make(\Input::all(), [
                        'pageHeader' => 'required',
                        'logo' => 'image|mimes:jpeg,png,jpg,gif|max:4000|dimensions:min_width=1600,min_height=626',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            } else {

                /* Upload banner image */
                if ($request->hasFile('logo')) {
                    $image = $request->file('logo');
                    $name = time() . '_' . $image->getClientOriginalName();
                    $destinationPath = public_path('/uploads/site_page');
                    $image->move($destinationPath, $name);
                    $staticpageObj->pageBanner = $name;
                    $uploadImage = 1;
                }
            }
        }

        if ($id != '-1') {
            $staticpageObj = $staticpageObj->find($id);
            if ($uploadImage == 1)
                $staticpageObj->pageBanner = $name;
        }
        $staticpageObj->pageType = $request->input('pageType');
        if ($id == '-1')
            $staticpageObj->slug = strtolower(str_replace(' ', '_', preg_replace('/[^\w\s]/', '', $request->input('pageHeader'))));
        $staticpageObj->pageHeader = $request->input('pageHeader');
        $staticpageObj->pageContent = ($request->input('pageContent'));
        $staticpageObj->pageTitle = $request->input('pageTitle');
        $staticpageObj->metaKeyword = $request->input('metaKeyword');
        $staticpageObj->metaDescription = addslashes($request->input('metaDescription'));
        if ($uploadImage)
            $staticpageObj->pageBanner = $name;



        if ($staticpageObj->save())
            return redirect(route('pagelist', ['pagetype' => $pagetype]) . '?page=' . $page)->with('successMessage', 'Information saved successfuly.');
    }

    public function editstatus($id = -1, $status) {

        $staticpageObj = new Sitepage();
        if ($id != -1)
            $staticpageObj = $staticpageObj->find($id);
        $staticpageObj->status = $status;

        if ($staticpageObj->save())
            return json_encode(array('updated' => true));
        else
            return json_encode(array('updated' => false));
    }

    public function deleterecord($id = -1, $pagetype = 'staticpage', $page = 1) {

        $staticpageObj = new Sitepage();

        if ($id != '-1') {
            $staticpageObj = $staticpageObj->find($id);
            if ($staticpageObj->delete())
                return redirect(route('pagelist', ['pagetype' => $pagetype]) . '?page=' . $page)->with('successMessage', 'Information deleted successfuly.');
        }
    }

}
