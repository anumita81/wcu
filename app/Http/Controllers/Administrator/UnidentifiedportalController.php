<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class UnidentifiedportalController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Request $request) {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Unidentifiedportal'), Auth::user()->id); // call the helper function
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $searchUnidentifiedArr = array(
            'trackingNumber' => '',
        );

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchUnidentified = \Input::get('searchUnidentified', $searchUnidentifiedArr);

            $field = \Input::get('field', 'itemId');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('UNINDENTIFIED');
            \Session::push('UNINDENTIFIED.searchDisplay', $searchDisplay);
            \Session::push('UNINDENTIFIED.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('UNINDENTIFIED.searchByDate', $searchByDate);
            \Session::push('UNINDENTIFIED.field', $field);
            \Session::push('UNINDENTIFIED.type', $type);
            \Session::push('UNINDENTIFIED.searchUnidentified', $searchUnidentified);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchDisplay'] = $searchDisplay;
            $param['searchUnidentified'] = $searchUnidentified;
        } else {
            $sortField = \Session::get('UNINDENTIFIED.field');
            $sortType = \Session::get('UNINDENTIFIED.type');
            $searchDisplay = \Session::get('UNINDENTIFIED.searchDisplay');
            $searchByCreatedOn = \Session::get('UNINDENTIFIED.searchByCreatedOn');
            $searchByDate = \Session::get('UNINDENTIFIED.searchByDate');
            $searchUnidentified = \Session::get('UNINDENTIFIED.searchUnidentified');


            $param['field'] = !empty($sortField) ? $sortField[0] : 'itemId';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
            $param['searchUnidentified'] = !empty($searchUnidentified) ? $searchUnidentified[0] : $searchUnidentifiedArr;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'itemId' => array('current' => 'sorting'),
            'dateOfDelivery' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH STATE LIST  */
        $unidentifiedItemList = \App\Model\Unidentifieditemdetails::getItemlList($param);

        $deliveryCompanyData = collect(\App\Model\Deliverycompany::all());
        $data['deliveryCompanyList'] = $deliveryCompanyData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        $warehouseRowData = collect(\App\Model\Warehouselocation::where('type', 'R')->get());
        $data['warehouseRowList'] = $warehouseRowData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        $warehouseZoneData = collect(\App\Model\Warehouselocation::where('type', 'z')->get());
        $data['warehouseZoneList'] = $warehouseZoneData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });

        $data['unidentifiedItemList'] = $unidentifiedItemList;
        $data['page'] = $unidentifiedItemList->currentPage();
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['title'] = "Administrative Panel :: Unidentified Portal";
        $data['pageTitle'] = "Unidentified Portal Management";
        $data['contentTop'] = array('breadcrumbText' => 'Unidentified Portal Management', 'contentTitle' => 'Unidentified Portal Management', 'pageInfo' => 'This section allows you to manage unidentified items');

        $userId = Auth::id();

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.unidentifiedportal.index', $data);
    }

    public function fraudportal(Request $request) {

        $data = array();


        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Fraudportal'), Auth::user()->id); // call the helper function

        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $searchUnidentifiedArr = array(
            'trackingNumber' => '',
        );

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchUnidentified = \Input::get('searchUnidentified', $searchUnidentifiedArr);

            $field = \Input::get('field', 'itemId');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('FRAUDPORTAL');
            \Session::push('FRAUDPORTAL.searchDisplay', $searchDisplay);
            \Session::push('FRAUDPORTAL.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('FRAUDPORTAL.searchByDate', $searchByDate);
            \Session::push('FRAUDPORTAL.field', $field);
            \Session::push('FRAUDPORTAL.type', $type);
            \Session::push('FRAUDPORTAL.searchUnidentified', $searchUnidentified);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchDisplay'] = $searchDisplay;
            $param['searchUnidentified'] = $searchUnidentified;
        } else {
            $sortField = \Session::get('FRAUDPORTAL.field');
            $sortType = \Session::get('FRAUDPORTAL.type');
            $searchDisplay = \Session::get('FRAUDPORTAL.searchDisplay');
            $searchByCreatedOn = \Session::get('FRAUDPORTAL.searchByCreatedOn');
            $searchByDate = \Session::get('FRAUDPORTAL.searchByDate');
            $searchUnidentified = \Session::get('FRAUDPORTAL.searchUnidentified');


            $param['field'] = !empty($sortField) ? $sortField[0] : 'itemId';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
            $param['searchUnidentified'] = !empty($searchUnidentified) ? $searchUnidentified[0] : $searchUnidentifiedArr;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'itemId' => array('current' => 'sorting'),
            'dateOfDelivery' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH STATE LIST  */
        $unidentifiedItemList = \App\Model\Unidentifieditemdetails::getFraudList($param);

        $deliveryCompanyData = collect(\App\Model\Deliverycompany::all());
        $data['deliveryCompanyList'] = $deliveryCompanyData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        $warehouseRowData = collect(\App\Model\Warehouselocation::where('type', 'R')->get());
        $data['warehouseRowList'] = $warehouseRowData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        $warehouseZoneData = collect(\App\Model\Warehouselocation::where('type', 'z')->get());
        $data['warehouseZoneList'] = $warehouseZoneData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });

        $data['unidentifiedItemList'] = $unidentifiedItemList;
        $data['page'] = $unidentifiedItemList->currentPage();
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['title'] = "Administrative Panel :: Fraud Portal";
        $data['pageTitle'] = "Fraud Portal Management";
        $data['contentTop'] = array('breadcrumbText' => 'Fraud Portal Management', 'contentTitle' => 'Fraud Portal Management', 'pageInfo' => 'This section allows you to manage fraud items');

        $userId = Auth::id();




        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.unidentifiedportal.fraudportal', $data);
    }

    public function addedit($id = 0, $page = 1, Request $request) {


        if ($id != '0') {
            $pageTitle = "Edit Item";
            $unidentifiedItem = \App\Model\Unidentifieditemdetails::find($id);
        } else {
            $pageTitle = "Add Item";
            $unidentifiedItem = new \App\Model\Unidentifieditemdetails;
        }



        if (\Request::isMethod('post')) {
            if ($id != '0') {
                $this->validate($request, [
                    'shippingLabelImage' => 'image|mimes:jpeg,png,jpg,gif,svg|max:4096',
                    'itemImage' => 'image|mimes:jpeg,png,jpg,gif,svg|max:4096',
                    'invoiceImage' => 'image|mimes:jpeg,png,jpg,gif,svg|max:4096',
                ]);
            } else {
                $this->validate($request, [
                    'shippingLabelImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
                    'itemImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
                    'invoiceImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
                ]);
            }
            $validator = Validator::make(\Input::all(), [
                        'trackingNumber' => 'required',
                        'deliveryCompany' => 'required|integer',
                        'store' => 'required',
                        'senderName' => 'required',
                        'qty' => 'required|integer',
                        'dateOfDelivery' => 'required',
                        'warehouseRow' => 'required',
                        'warehouseZone' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            } else {
                $shippingLabelImage = "";
                $itemImage = "";
                $invoiceImage = "";

                if ($request->hasFile('shippingLabelImage')) {
                    if (!empty($unidentifiedItem->shippingLabelImage)) {
                        if (file_exists(public_path('/uploads/unidentified_item/' . $unidentifiedItem->shippingLabelImage))) {
                            unlink(public_path('/uploads/unidentified_item/' . $unidentifiedItem->shippingLabelImage));
                        }
                    }
                    $image = $request->file('shippingLabelImage');
                    $shippingLabelImage = time() . '_' . $image->getClientOriginalName();
                    $shippingLabelImage = preg_replace('/\s+/', '_', $shippingLabelImage);
                    $destinationPath = public_path('/uploads/unidentified_item');
                    $image->move($destinationPath, $shippingLabelImage);
                    $unidentifiedItem->shippingLabelImage = $shippingLabelImage;
                }

                if ($request->hasFile('itemImage')) {
                    if (!empty($unidentifiedItem->itemImage)) {
                        if (file_exists(public_path('/uploads/unidentified_item/' . $unidentifiedItem->itemImage))) {
                            unlink(public_path('/uploads/unidentified_item/' . $unidentifiedItem->itemImage));
                        }
                    }
                    $image = $request->file('itemImage');
                    $itemImage = time() . '_' . $image->getClientOriginalName();
                    $itemImage = preg_replace('/\s+/', '_', $itemImage);
                    $destinationPath = public_path('/uploads/unidentified_item');
                    $image->move($destinationPath, $itemImage);
                    $unidentifiedItem->itemImage = $itemImage;
                }

                if ($request->hasFile('invoiceImage')) {
                    if (!empty($unidentifiedItem->invoiceImage)) {
                        if (file_exists(public_path('/uploads/unidentified_item/' . $unidentifiedItem->invoiceImage))) {
                            unlink(public_path('/uploads/unidentified_item/' . $unidentifiedItem->invoiceImage));
                        }
                    }
                    $image = $request->file('invoiceImage');
                    $invoiceImage = time() . '_' . $image->getClientOriginalName();
                    $invoiceImage = preg_replace('/\s+/', '_', $invoiceImage);
                    $destinationPath = public_path('/uploads/unidentified_item');
                    $image->move($destinationPath, $invoiceImage);
                    $unidentifiedItem->invoiceImage = $invoiceImage;
                }

                if ($request->hasFile('extra-images')) {
                    $itemImageList = '';
                    $files = $request->file('extra-images');
                    foreach ($files as $file) {
                        $filename = time() . '_' . $file->getClientOriginalName();
                        $filename = preg_replace('/\s+/', '_', $filename);
                        $destinationPath = public_path('/uploads/unidentified_item');
                        $file->move($destinationPath, $filename);
                        $itemImageList .= $filename . ',';
                    }

                    if (!empty($id) && !empty($unidentifiedItem->extraImage))
                        $unidentifiedItem->extraImage = $unidentifiedItem->extraImage . ',' . rtrim($itemImageList, ',');
                    else
                        $unidentifiedItem->extraImage = rtrim($itemImageList, ',');
                }

                $unidentifiedItem->senderEmail = $request->senderEmail;
                $unidentifiedItem->trackingNumber = $request->trackingNumber;
                $unidentifiedItem->trackingNumber2 = $request->trackingNumber2;
                $unidentifiedItem->unitOnPackage = $request->unitOnPackage;
                $unidentifiedItem->deliveryCompany = $request->deliveryCompany;
                $unidentifiedItem->store = $request->store;
                $unidentifiedItem->senderName = $request->senderName;
                $unidentifiedItem->description = $request->description;
                $unidentifiedItem->qty = $request->qty;
                $unidentifiedItem->dateOfDelivery = $request->dateOfDelivery;
                $unidentifiedItem->length = $request->length;
                $unidentifiedItem->weight = $request->weight;
                $unidentifiedItem->width = $request->width;
                $unidentifiedItem->height = $request->height;
                $unidentifiedItem->warehouseRow = $request->warehouseRow;
                $unidentifiedItem->warehouseZone = $request->warehouseZone;
                if (empty($id))
                    $unidentifiedItem->createdBy = Auth::user()->id;

                if ($unidentifiedItem->save()) {
                    if (!empty($request->senderEmail) && $id == 0) {
                        $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'unidentified_item_received')->first();
                        $to = $request->senderEmail;
                        $replace['[TRACKING_NUMBER]'] = $request->trackingNumber;
                        $replace['[DELIVERY_DETAILS]'] = \App\Model\Deliverycompany::find($request->deliveryCompany)->name;
                        $replace['[STORE_NAME]'] = $request->store;
                        $replace['[ITEM_QTY]'] = $request->qty;
//$cc = 'rosy.chakraborty@indusnet.co.in';
                        $cc = 'customerservice@shoptomydoor.com';

                        $isSend = \App\Helpers\customhelper::SendMailWithCC($emailTemplate, $replace, $to, $cc);
                    }
                    if ($id != 0)
                        return redirect('/administrator/unidentifiedportal?page=' . $page)->with('successMessage', 'Item updated successfully.');
                    else
                        return redirect('/administrator/unidentifiedportal?page=' . $page)->with('successMessage', 'Item added successfully.');
                } else
                    return redirect('/administrator/unidentifiedportal?page=' . $page)->with('errorMessage', 'Something went wrong! Please try again.');
            }
        }

        $data['unidentifiedItem'] = $unidentifiedItem;
        $data['deliveryCompanyList'] = \App\Model\Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();
        $data['warehouseRowList'] = \App\Model\Warehouselocation::where('type', 'R')->where('status', '1')->orderby('name', 'asc')->get();
        if (!empty($unidentifiedItem->warehouseRow))
            $data['warehouseZoneList'] = \App\Model\Warehouselocation::where('type', 'Z')->where('status', '1')->where('parentId', $unidentifiedItem->warehouseRow)->get();
        $data['pageTitle'] = $pageTitle;
        $data['id'] = $id;
        $data['page'] = $page;

        return view('Administrator.unidentifiedportal.addedit', $data);
    }

    public function fraudportaladdedit($id = 0, $page = 1, Request $request) {


        if ($id != '0') {
            $pageTitle = "Edit Item";
            $unidentifiedItem = \App\Model\Unidentifieditemdetails::find($id);
        } else {
            $pageTitle = "Add Item";
            $unidentifiedItem = new \App\Model\Unidentifieditemdetails;
        }

        if (\Request::isMethod('post')) {
            if ($id != '0') {
                $this->validate($request, [
                    'shippingLabelImage' => 'image|mimes:jpeg,png,jpg,gif,svg|max:4096',
                    'itemImage' => 'image|mimes:jpeg,png,jpg,gif,svg|max:4096',
                    'invoiceImage' => 'image|mimes:jpeg,png,jpg,gif,svg|max:4096',
                ]);
            } else {
                $this->validate($request, [
                    'shippingLabelImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
                    'itemImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
                    'invoiceImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
                ]);
            }
            $validator = Validator::make(\Input::all(), [
                        'trackingNumber' => 'required',
                        'deliveryCompany' => 'required|integer',
                        'store' => 'required',
                        'senderName' => 'required',
                        'qty' => 'required|integer',
                        'dateOfDelivery' => 'required',
                        'warehouseRow' => 'required',
                        'warehouseZone' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            } else {
                $shippingLabelImage = "";
                $itemImage = "";
                $invoiceImage = "";

                if ($request->hasFile('shippingLabelImage')) {
                    if (!empty($unidentifiedItem->shippingLabelImage)) {
                        if (file_exists(public_path('/uploads/unidentified_item/' . $unidentifiedItem->shippingLabelImage))) {
                            unlink(public_path('/uploads/unidentified_item/' . $unidentifiedItem->shippingLabelImage));
                        }
                    }
                    $image = $request->file('shippingLabelImage');
                    $shippingLabelImage = time() . '_' . $image->getClientOriginalName();
                    $shippingLabelImage = preg_replace('/\s+/', '_', $shippingLabelImage);
                    $destinationPath = public_path('/uploads/unidentified_item');
                    $image->move($destinationPath, $shippingLabelImage);
                    $unidentifiedItem->shippingLabelImage = $shippingLabelImage;
                }

                if ($request->hasFile('itemImage')) {
                    if (!empty($unidentifiedItem->itemImage)) {
                        if (file_exists(public_path('/uploads/unidentified_item/' . $unidentifiedItem->itemImage))) {
                            unlink(public_path('/uploads/unidentified_item/' . $unidentifiedItem->itemImage));
                        }
                    }
                    $image = $request->file('itemImage');
                    $itemImage = time() . '_' . $image->getClientOriginalName();
                    $itemImage = preg_replace('/\s+/', '_', $itemImage);
                    $destinationPath = public_path('/uploads/unidentified_item');
                    $image->move($destinationPath, $itemImage);
                    $unidentifiedItem->itemImage = $itemImage;
                }

                if ($request->hasFile('invoiceImage')) {
                    if (!empty($unidentifiedItem->invoiceImage)) {
                        if (file_exists(public_path('/uploads/unidentified_item/' . $unidentifiedItem->invoiceImage))) {
                            unlink(public_path('/uploads/unidentified_item/' . $unidentifiedItem->invoiceImage));
                        }
                    }
                    $image = $request->file('invoiceImage');
                    $invoiceImage = time() . '_' . $image->getClientOriginalName();
                    $invoiceImage = preg_replace('/\s+/', '_', $invoiceImage);
                    $destinationPath = public_path('/uploads/unidentified_item');
                    $image->move($destinationPath, $invoiceImage);
                    $unidentifiedItem->invoiceImage = $invoiceImage;
                }

                if ($request->hasFile('extra-images')) {
                    $itemImageList = '';
                    $files = $request->file('extra-images');
                    foreach ($files as $file) {
                        $filename = time() . '_' . $file->getClientOriginalName();
                        $filename = preg_replace('/\s+/', '_', $filename);
                        $destinationPath = public_path('/uploads/unidentified_item');
                        $file->move($destinationPath, $filename);
                        $itemImageList .= $filename . ',';
                    }

                    if (!empty($id))
                        $unidentifiedItem->extraImage = $unidentifiedItem->extraImage . ',' . rtrim($itemImageList, ',');
                    else
                        $unidentifiedItem->extraImage = rtrim($itemImageList, ',');
                }

                $unidentifiedItem->senderEmail = $request->senderEmail;
                $unidentifiedItem->trackingNumber = $request->trackingNumber;
                $unidentifiedItem->trackingNumber2 = $request->trackingNumber2;
                $unidentifiedItem->unitOnPackage = $request->unitOnPackage;
                $unidentifiedItem->deliveryCompany = $request->deliveryCompany;
                $unidentifiedItem->store = $request->store;
                $unidentifiedItem->senderName = $request->senderName;
                $unidentifiedItem->description = $request->description;
                $unidentifiedItem->qty = $request->qty;
                $unidentifiedItem->dateOfDelivery = $request->dateOfDelivery;
                $unidentifiedItem->warehouseRow = $request->warehouseRow;
                $unidentifiedItem->warehouseZone = $request->warehouseZone;
                $unidentifiedItem->type = "fraud";
                $unidentifiedItem->length = $request->length;
                $unidentifiedItem->weight = $request->weight;
                $unidentifiedItem->width = $request->width;
                $unidentifiedItem->height = $request->height;

                if (empty($id))
                    $unidentifiedItem->createdBy = Auth::user()->id;

                if ($unidentifiedItem->save()) {
                    if (!empty($request->senderEmail) && $id == 0) {

                        $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'fraud_item_received')->first();

                        $to = $request->senderEmail;
                        $replace['[TRACKING_NUMBER]'] = $request->trackingNumber;
                        $replace['[DELIVERY_DETAILS]'] = \App\Model\Deliverycompany::find($request->deliveryCompany)->name;
                        $replace['[STORE_NAME]'] = $request->store;
                        $replace['[ITEM_QTY]'] = $request->qty;
//$cc = 'rosy.chakraborty@indusnet.co.in';
                        $cc = 'customerservice@shoptomydoor.com';

                        $isSend = \App\Helpers\customhelper::SendMailWithCC($emailTemplate, $replace, $to, $cc);
                    }
                    if ($id != 0)
                        return redirect('/administrator/fraudportal?page=' . $page)->with('successMessage', 'Item updated successfully.');
                    else
                        return redirect('/administrator/fraudportal?page=' . $page)->with('successMessage', 'Item added successfully.');
                } else
                    return redirect('/administrator/fraudportal?page=' . $page)->with('errorMessage', 'Something went wrong! Please try again.');
            }
        }

        $data['unidentifiedItem'] = $unidentifiedItem;
        $data['deliveryCompanyList'] = \App\Model\Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();
        $data['warehouseRowList'] = \App\Model\Warehouselocation::where('type', 'R')->where('status', '1')->orderby('name', 'asc')->get();
        if (!empty($unidentifiedItem->warehouseRow))
            $data['warehouseZoneList'] = \App\Model\Warehouselocation::where('type', 'Z')->where('status', '1')->where('parentId', $unidentifiedItem->warehouseRow)->get();
        $data['pageTitle'] = $pageTitle;
        $data['id'] = $id;
        $data['page'] = $page;

        return view('Administrator.unidentifiedportal.fraudportaladdedit', $data);
    }

    /**
     * Method used to unset search session data
     *
     */
    public function cleardata() {
        \Session::forget('UNINDENTIFIED');
        return \Redirect::to('administrator/unidentifiedportal');
    }

    public function fraudportalcleardata() {
        \Session::forget('FRAUDPORTAL');
        return \Redirect::to('administrator/fraudportal');
    }

    public function delete($id, $page = "1") {

        $unidentifiedItem = \App\Model\Unidentifieditemdetails::find($id);
        $unidentifiedItem->deleted = "1";
        $unidentifiedItem->deletedBy = Auth::user()->email;
        $unidentifiedItem->deletedOn = Config::get('constants.CURRENTDATE');
        $unidentifiedItem->save();

        return \Redirect::to('administrator/unidentifiedportal/?page=' . $page)->with('successMessage', 'Record deleted successfuly.');
    }

    public function fraudportaldelete($id, $page = "1") {

        $unidentifiedItem = \App\Model\Unidentifieditemdetails::find($id);
        $unidentifiedItem->deleted = "1";
        $unidentifiedItem->deletedBy = Auth::user()->email;
        $unidentifiedItem->deletedOn = Config::get('constants.CURRENTDATE');
        $unidentifiedItem->save();

        return \Redirect::to('administrator/fraudportal/?page=' . $page)->with('successMessage', 'Record deleted successfuly.');
    }

    public function view($id, $page = "1") {

        $data = array();

        $unidentifiedItemDetails = \App\Model\Unidentifieditemdetails::find($id);
        $unidentifiedNotes = \App\Model\Unidentifiednotes::where('itemId', $id)->get();
        $resolveNote = array();
        if ($unidentifiedItemDetails->status == 'resolved') {
            $resolveNote = \App\Model\Unidentifiednotes::where('itemId', $id)->where("type", "resolvenote")->first();
        }

        if (!empty($unidentifiedItemDetails->createdBy))
            $unidentifiedItemDetails->uploadedBy = \App\Model\UserAdmin::select('firstName', 'lastName')->where('id', $unidentifiedItemDetails->createdBy)->first();

        $deliveryCompanyData = collect(\App\Model\Deliverycompany::all());
        $data['deliveryCompanyList'] = $deliveryCompanyData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        $warehouseRowData = collect(\App\Model\Warehouselocation::where('type', 'R')->get());
        $data['warehouseRowList'] = $warehouseRowData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        $warehouseZoneData = collect(\App\Model\Warehouselocation::where('type', 'z')->get());
        $data['warehouseZoneList'] = $warehouseZoneData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });

        $data['adminusers'] = \App\Model\UserAdmin::where('status', '1')->where('deleted', '0')->get();


        $data['unidentifiedItemDetails'] = $unidentifiedItemDetails;
        $data['unidentifiedNotes'] = $unidentifiedNotes;
        $data['resolveNote'] = $resolveNote;
        $data['id'] = $id;
        $data['page'] = $page;
        $data['title'] = "Administrative Panel :: Unidentified Portal";
        $data['pageTitle'] = "Unidentified Portal Item";
        $data['contentTop'] = array('breadcrumbText' => 'Unidentified Portal Item', 'contentTitle' => 'Unidentified Portal Item', 'pageInfo' => 'This section allows you to manage unidentified items');

        return view('Administrator.unidentifiedportal.view', $data);
    }

    public function fraudportalview($id, $page = "1") {

        $data = array();

        $unidentifiedItemDetails = \App\Model\Unidentifieditemdetails::find($id);
        $unidentifiedNotes = \App\Model\Unidentifiednotes::where('itemId', $id)->get();
        $resolveNote = array();
        if ($unidentifiedItemDetails->status == 'resolved') {
            $resolveNote = \App\Model\Unidentifiednotes::where('itemId', $id)->where("type", "resolvenote")->first();
        }

        if (!empty($unidentifiedItemDetails->createdBy))
            $unidentifiedItemDetails->uploadedBy = \App\Model\UserAdmin::select('firstName', 'lastName')->where('id', $unidentifiedItemDetails->createdBy)->first();


        $deliveryCompanyData = collect(\App\Model\Deliverycompany::all());
        $data['deliveryCompanyList'] = $deliveryCompanyData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        $warehouseRowData = collect(\App\Model\Warehouselocation::where('type', 'R')->get());
        $data['warehouseRowList'] = $warehouseRowData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        $warehouseZoneData = collect(\App\Model\Warehouselocation::where('type', 'z')->get());
        $data['warehouseZoneList'] = $warehouseZoneData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });

        $data['adminusers'] = \App\Model\UserAdmin::where('status', '1')->where('deleted', '0')->get();

        $data['unidentifiedItemDetails'] = $unidentifiedItemDetails;
        $data['unidentifiedNotes'] = $unidentifiedNotes;
        $data['resolveNote'] = $resolveNote;
        $data['id'] = $id;
        $data['page'] = $page;
        $data['title'] = "Administrative Panel :: Fraud Portal";
        $data['pageTitle'] = "Fraud Portal Item";
        $data['contentTop'] = array('breadcrumbText' => 'Fraud Portal Item', 'contentTitle' => 'Fraud Portal Item', 'pageInfo' => 'This section allows you to manage fraud items');

        return view('Administrator.unidentifiedportal.fraudportalview', $data);
    }

    public function updatestatus(Request $request) {
//print_r($request->all());exit;

        $itemImage = "";
        $imageSave = array();
        $id = $request->shipmentId;


        if ($request->hasFile('resolveLabel')) {
            if (count($request->file('resolveLabel')) <= 4) {
                foreach ($request->file('resolveLabel') as $eachImage) {
                    $image = $eachImage;
                    $itemImage = time() . '_' . $image->getClientOriginalName();
                    $itemImage = preg_replace('/\s+/', '_', $itemImage);
                    $imageSave[] = $itemImage;
                    $destinationPath = public_path('/uploads/unidentified_item');
                    $image->move($destinationPath, $itemImage);
                }
            } else {
                return \Redirect::to('administrator/unidentifiedportal/view/' . $id . '/1')->with('errorMessage', 'Please Upload upto 4 label images.');
            }
        }

        $unidentifiedItemDetails = \App\Model\Unidentifieditemdetails::find($request->shipmentId);
        $unidentifiedItemDetails->status = $request->status;
        $unidentifiedItemDetails->save();

        if ($request->status == 'resolved') {
            $note = array();
            $note['unitNumber'] = $request->unitNumber;
            $note['notifyEmail'] = $request->notifyEmail;
            $note['extraAmount'] = $request->extraAmount;
            $note['note'] = $request->resolutionNote;
            $note['labelImage'] = $imageSave;

            $unidentifiedItemNote = new \App\Model\Unidentifiednotes;
            $unidentifiedItemNote->itemId = $id;
            $unidentifiedItemNote->note = json_encode($note);
            $unidentifiedItemNote->type = "resolvenote";
            $unidentifiedItemNote->createdBy = Auth::user()->email;
            $unidentifiedItemNote->save();

            $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'unidentified_item_resolved')->first();
            $emailTemplate->templateSubject = str_replace('[NUMBER]', '#' . $request->shipmentId, $emailTemplate->templateSubject);

            $to = $request->notifyEmail;
            $replace['[UNIT]'] = $request->unitNumber;
            $replace['[EXTRACOST]'] = $request->extraAmount;
            $evenHistory = "";
            $unidentifiedNotes = \App\Model\Unidentifiednotes::where('itemId', $id)->get();
            foreach ($unidentifiedNotes as $notes) {
                $noteMessage = $notes->note;
                if ($notes->type == "resolvenote") {
                    $noteDetails = json_decode($notes->note, true);
                    $noteMessage = $noteDetails['note'];
                }
                $evenHistory .= "<p>Comment: $noteMessage , By: $notes->createdBy , on $notes->createdOn</p>";
            }
            $replace['[UNIDENTIFIED_HISTORY]'] = $evenHistory;

//            if($itemImage == "")
//            {
            $isSend = \App\Helpers\customhelper::SendMail($emailTemplate, $replace, $to);
//            }
//            else
//            {
//                $isSend = \App\Helpers\customhelper::SendMailWithAttachment($emailTemplate, $replace, $to,$destinationPath."/".$itemImage);
//            }
        }
//echo "1";

        return \Redirect::to('administrator/unidentifiedportal/view/' . $id . '/1')->with('successMessage', 'Note updated successfully.');
    }

    public function updatefrauditemstatus(Request $request) {
//print_r($request->all());exit;

        $itemImage = "";
        $imageSave = array();
        $id = $request->shipmentId;


        if ($request->hasFile('resolveLabel')) {
            if (count($request->file('resolveLabel')) <= 4) {
                foreach ($request->file('resolveLabel') as $eachImage) {
                    $image = $eachImage;
                    $itemImage = time() . '_' . $image->getClientOriginalName();
                    $itemImage = preg_replace('/\s+/', '_', $itemImage);
                    $imageSave[] = $itemImage;
                    $destinationPath = public_path('/uploads/unidentified_item');
                    $image->move($destinationPath, $itemImage);
                }
            } else {
                return \Redirect::to('administrator/unidentifiedportal/view/' . $id . '/1')->with('errorMessage', 'Please Upload upto 4 label images.');
            }
        }

        $unidentifiedItemDetails = \App\Model\Unidentifieditemdetails::find($request->shipmentId);
        $unidentifiedItemDetails->status = $request->status;
        $unidentifiedItemDetails->save();

        if ($request->status == 'resolved') {
            $note = array();
            $note['unitNumber'] = $request->unitNumber;
            $note['notifyEmail'] = $request->notifyEmail;
            $note['extraAmount'] = $request->extraAmount;
            $note['note'] = $request->resolutionNote;
            $note['labelImage'] = $imageSave;

            $unidentifiedItemNote = new \App\Model\Unidentifiednotes;
            $unidentifiedItemNote->itemId = $id;
            $unidentifiedItemNote->note = json_encode($note);
            $unidentifiedItemNote->type = "resolvenote";
            $unidentifiedItemNote->createdBy = Auth::user()->email;
            $unidentifiedItemNote->save();

            $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'fraud_item_resolved')->first();
            $emailTemplate->templateSubject = str_replace('[NUMBER]', '#' . $id, $emailTemplate->templateSubject);

            $to = $request->notifyEmail;
            $replace['[UNIT]'] = $request->unitNumber;
            $replace['[EXTRACOST]'] = $request->extraAmount;
            $evenHistory = "";
            $unidentifiedNotes = \App\Model\Unidentifiednotes::where('itemId', $id)->get();
            foreach ($unidentifiedNotes as $notes) {
                $noteMessage = $notes->note;
                if ($notes->type == "resolvenote") {
                    $noteDetails = json_decode($notes->note, true);
                    $noteMessage = $noteDetails['note'];
                }
                $evenHistory .= "<p>Comment: $noteMessage , By: $notes->createdBy , on $notes->createdOn</p>";
            }
            $replace['[UNIDENTIFIED_HISTORY]'] = $evenHistory;

//            if($itemImage == "")
//            {
            $isSend = \App\Helpers\customhelper::SendMail($emailTemplate, $replace, $to);
//            }
//            else
//            {
//                $isSend = \App\Helpers\customhelper::SendMailWithAttachment($emailTemplate, $replace, $to,$destinationPath."/".$itemImage);
//            }
        }
//echo "1";

        return \Redirect::to('administrator/fraudportal/view/' . $id . '/1')->with('successMessage', 'Note updated successfully.');
    }

    public function savenotes($id, $page = 1, Request $request) {

        $notifyUserDetails = \App\Model\UserAdmin::where("email",$request->adminUserEmail)->first();
        
        $unidentifiedItemNote = new \App\Model\Unidentifiednotes;
        $unidentifiedItemNote->itemId = $id;
        $unidentifiedItemNote->note = $request->note;
        $unidentifiedItemNote->notifiedUser = (!empty($notifyUserDetails) ? $notifyUserDetails->firstName." ".$notifyUserDetails->lastName : "");
        $unidentifiedItemNote->createdBy = Auth::user()->email;
        $unidentifiedItemNote->save();

        $isSend = \App\Helpers\customhelper::SendMailWithStaticContent($request->note, "Unidentified Item #$id Notification", $request->adminUserEmail);

        return \Redirect::to('administrator/unidentifiedportal/view/' . $id . '/' . $page)->with('successMessage', 'Note saved successfully.');
    }

    public function fraudportalsavenotes($id, $page = 1, Request $request) {

        $notifyUserDetails = \App\Model\UserAdmin::where("email",$request->adminUserEmail)->first();
        
        $unidentifiedItemNote = new \App\Model\Unidentifiednotes;
        $unidentifiedItemNote->itemId = $id;
        $unidentifiedItemNote->note = $request->note;
        $unidentifiedItemNote->notifiedUser = (!empty($notifyUserDetails) ? $notifyUserDetails->firstName." ".$notifyUserDetails->lastName : "");
        $unidentifiedItemNote->createdBy = Auth::user()->email;
        $unidentifiedItemNote->save();

        $isSend = \App\Helpers\customhelper::SendMailWithStaticContent($request->note, "Unidentified Item #$id Notification", $request->adminUserEmail);

        return \Redirect::to('administrator/fraudportal/view/' . $id . '/' . $page)->with('successMessage', 'Note saved successfully.');
    }

    public function uploadextraimage($id = 0, $page = 1, Request $request) {
        if ($request->hasFile('extra-images')) {
            $unidentifiedItem = \App\Model\Unidentifieditemdetails::find($id);
            $itemImageList = '';
            $files = $request->file('extra-images');
            foreach ($files as $file) {
                $filename = time() . '_' . $file->getClientOriginalName();
                $filename = preg_replace('/\s+/', '_', $filename);
                $destinationPath = public_path('/uploads/unidentified_item');
                $file->move($destinationPath, $filename);
                $itemImageList .= $filename . ',';
            }

            if (!empty($id) && !empty($unidentifiedItem->extraImage))
                $unidentifiedItem->extraImage = $unidentifiedItem->extraImage . ',' . rtrim($itemImageList, ',');
            else
                $unidentifiedItem->extraImage = rtrim($itemImageList, ',');

            if ($unidentifiedItem->save())
                return redirect('/administrator/unidentifiedportal?page=' . $page)->with('successMessage', 'Item updated successfully.');
            else
                return redirect('/administrator/unidentifiedportal?page=' . $page)->with('errorMessage', 'Something went wrong! Please try again.');
        } else
            return redirect('/administrator/unidentifiedportal?page=' . $page)->with('errorMessage', 'Something went wrong! Please try again.');
    }

    public function uploadfraudextraimage($id = 0, $page = 1, Request $request) {
        if ($request->hasFile('extra-images')) {
            $unidentifiedItem = \App\Model\Unidentifieditemdetails::find($id);
            $itemImageList = '';
            $files = $request->file('extra-images');
            foreach ($files as $file) {
                $filename = time() . '_' . $file->getClientOriginalName();
                $filename = preg_replace('/\s+/', '_', $filename);
                $destinationPath = public_path('/uploads/unidentified_item');
                $file->move($destinationPath, $filename);
                $itemImageList .= $filename . ',';
            }

            if (!empty($id) && !empty($unidentifiedItem->extraImage))
                $unidentifiedItem->extraImage = $unidentifiedItem->extraImage . ',' . rtrim($itemImageList, ',');
            else
                $unidentifiedItem->extraImage = rtrim($itemImageList, ',');

            if ($unidentifiedItem->save())
                return redirect('/administrator/fraudportal?page=' . $page)->with('successMessage', 'Item updated successfully.');
            else
                return redirect('/administrator/fraudportal?page=' . $page)->with('errorMessage', 'Something went wrong! Please try again.');
        } else
            return redirect('/administrator/fraudportal?page=' . $page)->with('errorMessage', 'Something went wrong! Please try again.');
    }

}
