<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Country;
use App\Model\Packagingcharges;
use App\Model\Warehouse;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class PackagingchargesController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Packagingcharges'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */

            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('PACKAGECHARGESDATA');
            \Session::push('PACKAGECHARGESDATA.searchDisplay', $searchDisplay);
            \Session::push('PACKAGECHARGESDATA.field', $field);
            \Session::push('PACKAGECHARGESDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('PACKAGECHARGESDATA.field');
            $sortType = \Session::get('PACKAGECHARGESDATA.type');
            $searchDisplay = \Session::get('PACKAGECHARGESDATA.searchDisplay');


            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'weightFrom'=>array('current' => 'sorting'), 'weightTo'=>array('current' => 'sorting'), 'amount'  => array('current' => 'sorting')
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTRY LIST  */
        $feesData = Packagingcharges::getPackageChargesList($param);
        
        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Packaging Charges";
        $data['contentTop'] = array('breadcrumbText' => 'Packaging Charges', 'contentTitle' => 'Packaging Charges', 'pageInfo' => 'This section allows you to add/edit packages');
        $data['pageTitle'] = "Packaging Charges";
        $data['page'] = $feesData->currentPage();
        $data['feesData'] = $feesData;    
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        
        return view('Administrator.packagingcharges.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Packaging Charges";
        $data['contentTop'] = array('breadcrumbText' => 'Packaging Charges', 'contentTitle' => 'Packaging Charges', 'pageInfo' => 'This section allows you to add/edit packages');
        $data['page'] = !empty($page) ? $page : '1';
        
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $packagingcharges = Packagingcharges::find($id);
            $data['packagingcharges'] = $packagingcharges;
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add New';

        }
        return view('Administrator.packagingcharges.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $packagingcharges = new Packagingcharges;

               
        
        $validator = Validator::make($request->all(), [
                    'weightFrom' => 'required|numeric',
                    'weightTo' => 'required|numeric',
                    'amount'=> 'required|numeric'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

            if (!empty($id)) {
                $validateNumbers = Packagingcharges::validateNumbers($request->weightFrom, $request->weightTo, $id);
            }else{
                 $validateNumbers = Packagingcharges::validateNumbers($request->weightFrom, $request->weightTo, '');
            }

            if(empty($validateNumbers) || count($validateNumbers) == 0)
            {
                if (!empty($id)) {
                $packagingcharges = Packagingcharges::find($id);
                $packagingcharges->updatedBy = Auth::user()->id;
                $packagingcharges->updatedOn = Config::get('constants.CURRENTDATE');
                }
                else{
                    $packagingcharges->createdBy = Auth::user()->id;
                    $packagingcharges->createdOn = Config::get('constants.CURRENTDATE');
                }
                $packagingcharges->weightFrom = $request->weightFrom;
                $packagingcharges->weightTo = $request->weightTo;
                $packagingcharges->amount = $request->amount;
                $packagingcharges->save();
                $packagingchargesId = $packagingcharges->id;

                return redirect('administrator/packagingcharges')->with('successMessage', 'Packaging Charges saved successfuly.');
                
            }else{
                 return redirect('administrator/packagingcharges')->with('errorMessage', 'Weight must not be within existing range.');
            }
            
        }
    }

    public function deletedata($id = '',$page='') {

        $packagingcharges= new Packagingcharges();

        $createrModifierId = Auth::user()->id;
        if(!empty($id)){
        $packagingcharges = $packagingcharges->find($id);        

        if (Packagingcharges::deleteRecord($id, $createrModifierId)) {
               return \Redirect::to('administrator/packagingcharges/?page='.$page)->with('successMessage', 'Packaging Charges deleted successfuly.');
            } else {
                return \Redirect::to('administrator/packagingcharges/?page=' .$page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/packagingcharges/?page=' .$page)->with('errorMessage', 'Error in operation!');
        }
    }

    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Packagingcharges::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/packagingcharges/?page=' . $page)->with('successMessage', 'Packaging Charges status changed successfully.');
            } else {
                return \Redirect::to('administrator/packagingcharges/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/packagingcharges/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

 



}
