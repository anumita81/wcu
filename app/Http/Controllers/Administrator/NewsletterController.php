<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Newsletter;
use App\Model\Emailtemplate;
use App\Model\Subscriber;
use App\Model\Newslettersubscribermapping;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use Mail;
use customhelper;

class NewsletterController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index() {
        $data = array();

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Newsletter'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchText = trim(\Input::get('search', ''));
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('NEWSLETTERDATA');
            \Session::push('NEWSLETTERDATA.field', $field);
            \Session::push('NEWSLETTERDATA.type', $type);
            \Session::push('NEWSLETTERDATA.searchDisplay', $searchDisplay);


            $param['field'] = !empty($field) ? $field : 'id';
            $param['type'] = !empty($type) ? $type : 'desc';
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('NEWSLETTERDATA.field');
            $sortType = \Session::get('NEWSLETTERDATA.type');
            $searchDisplay = \Session::get('NEWSLETTERDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'createdOn' => array('current' => 'sorting')
        );


        /* SET SORTING ARRAY  */

        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        $newsletterData = Newsletter::getNewsletterList($param);
        
       

        /* BUILD DATA FOR VIEW  */
        $data['title'] = "Newsletter Management :: ADMIN - Shoptomydoor";
        $data = array();
        $data['pageTitle'] = "Newsletter Management";
        $data['contentTop'] = array('breadcrumbText' => 'Newsletter', 'contentTitle' => 'Newsletter', 'pageInfo' => 'This section allows you to manage Newsletter');
        $data['page'] = $newsletterData->currentPage();
        $data['newsletterData'] = $newsletterData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.newsletter.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Newsletter'), Auth::user()->id); // call the helper function
        if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $data['title'] = "Add Newsletter :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Newsletter :: Add";
        $data['contentTop'] = array('breadcrumbText' => 'Dashboard', 'contentTitle' => 'Newsletter', 'pageInfo' => '');
        $data['page'] = !empty($page) ? $page : '0';
        
        $data['emailTemplate'] = Emailtemplate::where('templateType', 'N')->where('status', '1')->where('deleted', '0')->get();

        if (!empty($id)) {
            $data['id'] = $id;
            $data['title'] = "ADMIN - Shoptomydoor :: Newsletter Template :: Update Newsletter";
            $data['pageTitle'] = "Update Newsletter Template";
            $data['contentTop'] = array('breadcrumbText' => 'Dashboard', 'contentTitle' => 'Newsletter', 'pageInfo' => '');
            $content = Newsletter::find($id);
            $data['content'] = $content;
        } else {
            $data['id'] = 0;
            $data['pageTitle'] = "Add Newsletter";
            $data['content'] = array();
        }

        return view('Administrator.newsletter.addedit', $data);
    }

    /**
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '0';
        $data['id'] = 0;

        $newsletter = new Newsletter;

            $validator = Validator::make($request->all(), [
                        'emailtemplate'=> 'required',
                        'newsletterName' => 'required',
                        'scheduleDate' => 'required',
                        'newsletterDescription' => 'required',
                        'scheduleRepeat' => 'required',
                        'allowedHtmlTags' => 'required'
            ]);
      

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

      
           $newsletter->createdOn = \Carbon\Carbon::parse(date('Y-m-d H:i:s'))->format('Y-m-d H:i:s');
           
           $newsletter->createdBy = Auth::user()->id;
           

            $newsletter->emailTemplateId = $request->emailtemplate;
            $newsletter->newsletterName = !empty($request->newsletterName) ? $request->newsletterName : '';
            $newsletter->newsletterDescription = !empty($request->newsletterDescription) ? $request->newsletterDescription : '';
            $newsletter->scheduleDate = !empty($request->scheduleDate) ? \Carbon\Carbon::parse($request->scheduleDate)->format('Y-m-d') : '';
            $newsletter->scheduleRepeat = !empty($request->scheduleRepeat) ? $request->scheduleRepeat : 'N';
            $newsletter->allowedHtmlTags = !empty($request->allowedHtmlTags) ? $request->allowedHtmlTags : 'N';
            
            $newsletter->save();

            return redirect('/administrator/newsletter/index')->with('successMessage', 'Newsletter Template saved successfuly.');
        }
    }

    /**
     * Method used to unset search session data
     */
    public function cleardata() {
        \Session::forget('NEWSLETTERDATA');
        return \Redirect::to('administrator/newsletter');
    }
    
    /**
     * Method used to send message forcefully
     * @param $id 
     * @return 
     */
    public function sendmessage($id, $page)
    {
       $data = $selectedSubscriberList=array();
      
       
       $data['id'] = $id;
       $data['page'] = $page;       
       $data['pageTitle'] = "Newsletter :: Send";
       
        
       $data['subscriberList'] = Subscriber::where('status', '1')->get();
       
       $data['customerList'] = \App\Model\User::where('deleted', '0')->get(); 
       
      $selectedSubscriberList = Newslettersubscribermapping::select('userId')->where('newsletterId', $id)->get()->toArray();
       
       if(count($selectedSubscriberList)>0)
       {
           $data['selectedSubscriberList'] = $selectedSubscriberList;
       }else{
           $data['selectedSubscriberList'] = array();
       }
       
       
       return view('Administrator.newsletter.subscriber', $data);
        
    }
    
    /**
     * Method used to save subscriber list to send message through corn
     * @param $newsletterId
     * @return 
     */
     public function savesubscriber($newsletterId, $page, Request $request)
     {
       if (\Request::isMethod('post')) {
         $data = array();

            $data['page'] = !empty($page) ? $page : '0';
            $data['id'] = 0;

            $senderlist = array_merge($request->subscriberlist, $request->customerlist);

            $newsletter = new Newslettersubscribermapping;
        
            foreach($senderlist as $sender)
            {
               $newsletter->newsletterId =  $newsletterId;
               $newsletter->userId =  $sender;
               $newsletter->sendStatus =  '0';
               $newsletter->sendDate =  \Carbon\Carbon::parse(date('Y-m-d H:i:s'))->format('Y-m-d H:i:s');
               
               $newsletter->save();
            }

            
        
            return redirect('/administrator/newsletter/index')->with('successMessage', 'Subscriber added successfuly.');
       }else{
            return redirect('/administrator/newsletter/index')->with('errorMessage', 'Subscriber not added.');
       }
     }
     
}
