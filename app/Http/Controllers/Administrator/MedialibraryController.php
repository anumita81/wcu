<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\libraries\dbHelpers;
use Auth;
use App\Model\Medialibrary;
use Hash;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Routing\Route;
use Config;
use customhelper;
use Carbon\Carbon;

class MedialibraryController extends Controller {
    public $_perPage;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Request $request) {
        $data = array();
        
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.MediaLibrary'), Auth::user()->id); 
        
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            \Session::forget('LIBRARY');
            \Session::push('LIBRARY.searchDisplay', $searchDisplay);
            \Session::push('LIBRARY.field', $field);
            \Session::push('LIBRARY.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('LIBRARY.field');
            $sortType = \Session::get('LIBRARY.type');
            $searchDisplay = \Session::get('LIBRARY.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'fileType';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'type' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* GET THE MEDIA RECORD  */
        $mediaData = Medialibrary::where('deleted', '0')->orderBy($param['field'], $param['type'])->paginate($param['searchDisplay']);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Media Library :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Media Library";
        $data['contentTop'] = array('breadcrumbText' => 'Media Library', 'contentTitle' => 'Media Library', 'pageInfo' => 'This section allows you to manage images for media purpose');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavContent', 'menuSub' => 'leftNavSiteContent2', 'menuSubSub' => 'leftNavMediaLibrary16');
        $data['mediaData'] = $mediaData;
        $data['page'] = $mediaData->currentPage();
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        return view('Administrator.medialibrary.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $id
     * @param string $page
     * @return array
     */
    public function addedit($id = '0', $page = '') {
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.MediaLibrary'), Auth::user()->id);
        $data = array();
        $data['title'] = "Administrative Panel :: Media Library";
        $data['contentTop'] = array('breadcrumbText' => 'Media Library', 'contentTitle' => 'Media Library', 'pageInfo' => '');
        $data['page'] = !empty($page) ? $page : '1';

        if (!empty($id)) {
            if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            /* SET DATA FOR VIEW  */
            $data['pageTitle'] = "Edit File";
            $data['mediaId'] = $id;
            $data['page'] = $page;
            $data['action'] = 'Edit';
            $data['mediaData'] = Medialibrary::find($id);
        } else {
            if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            /* SET DATA FOR VIEW  */
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add File";
            $data['page'] = $page;
        }
        return view('Administrator.medialibrary.addedit', $data);
    }


    /**
     * Method used to save information
     * @param integer $id
     * @param string $page
     * @param Request $request
     * @return array
     */
    public function savedata($id, $page, Request $request) {

        $data = array();
        
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.MediaLibrary'), Auth::user()->id);
        $data['page'] = !empty($page) ? $page : '1';
        $media = new Medialibrary;

        /*  SET CUSTOM MESSAGE FOR VALIDATION  */
        $messages = [
            'fileName.max' => 'Image maximum size exceed. ',
        ];
        
        $validator = Validator::make($request->all(), [
            'fileName' => 'max:4096',
        ], $messages);


        /*  IF VALIDATION FAIL */
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->customMessages);
        }

        /*  EDIT */
        if ($id != 0) {
            if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $media = Medialibrary::find($id);
            $media->modifiedBy = Auth::user()->id;
            $media->modifiedOn = Config::get('constants.CURRENTDATE');
            
        } else {
            /*  ADD */
            if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            
            /* SET THE IMAGE UPLOADED  */
            if ($request->hasFile('fileName')) {
                $image = $request->file('fileName');

                if (substr($image->getMimeType(), 0, 5) == 'image') {

                    $info = pathinfo($image->getClientOriginalName());
                    if($info['extension'] == 'bmp' || $info['extension'] == 'svg'){
                        return \Redirect::to('administrator/media?page=' . $page)->with('errorMessage', 'Please upload an image with jpeg, jpg, gif, png extension');
                    }

                    $name = time() . '_' . $image->getClientOriginalName();
                    $destinationPath = public_path('/uploads/media/original');
                    $image->move($destinationPath, $name);
                    
                    /* RESIZE THE IMAGE AND SAVED IN DIFFERENT PATHS */
                    $res100 = $this->createThumbnail($name, 100, 100, public_path('/uploads/media/original'), public_path('/uploads/media/thumb/'));
                    $res300 = $this->createThumbnail($name, 300, 300, public_path('/uploads/media/original'), public_path('/uploads/media/medium/'));

                    /* SAVING MEDIA RECORD  */
                    $media->fileName = $name;
                    $media->fileType = $request->fileType;
                    $media->createdBy = Auth::user()->id;
                    $media->createdOn = Config::get('constants.CURRENTDATE');
                    $media->save();
                } else {
                    return \Redirect::to('administrator/media?page=' . $page)->with('errorMessage', 'Please upload an image.');
                }
            } else {
                return \Redirect::to('administrator/media?page=' . $page)->with('errorMessage', 'Please upload an image.');
            }
            
        }
        

        return redirect('/administrator/media?page=' . $page)->with('successMessage', 'File saved successfuly.');
    }


    /**
     * Method used to resize image and save the same
     * @param string $image_name
     * @param integer $new_width
     * @param integer $new_height
     * @param string $uploadDir
     * @param string $moveToDir
     * @return array
     */
    public function createThumbnail($image_name,$new_width,$new_height,$uploadDir,$moveToDir)
    { 

        /* CREATING THE PATH OF THE IMAGE */
        $path = $uploadDir . '/' . $image_name;

        $mime = getimagesize($path);


        if($mime['mime']=='image/png') { 
            $src_img = imagecreatefrompng($path);
        }
        if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
            $src_img = imagecreatefromjpeg($path);
        } 
        if($mime['mime']=='image/gif') { 
            $src_img = imagecreatefromgif($path);
        }
        if($mime['mime']=='image/x-ms-bmp') { 
            $src_img = imagecreatefrombmp($path);
        }  

        $old_x          =   imageSX($src_img);
        $old_y          =   imageSY($src_img);

        if($old_x > $old_y) 
        {
            $thumb_w    =   $new_width;
            $thumb_h    =   $old_y*($new_height/$old_x);
        }

        if($old_x < $old_y) 
        {
            $thumb_w    =   $old_x*($new_width/$old_y);
            $thumb_h    =   $new_height;
        }

        if($old_x == $old_y) 
        {
            $thumb_w    =   $new_width;
            $thumb_h    =   $new_height;
        }

        $dst_img        =   ImageCreateTrueColor($thumb_w,$thumb_h);

        imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 


        /* NEW SAVE LOCATION */
        $new_thumb_loc = $moveToDir . $image_name;

        if($mime['mime']=='image/png') {
            $result = imagepng($dst_img,$new_thumb_loc,8);
        }
        if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg' || $mime['mime']=='image/gif' || $mime['mime']=='image/x-ms-bmp') {
            $result = imagejpeg($dst_img,$new_thumb_loc,80);
        }

        imagedestroy($dst_img); 
        imagedestroy($src_img);

        return $result;
    }


    /**
     * Method used to delete
     * @param integer $id
     * @param string $page
     * @return array
     */
    public function delete($id, $page) {
        
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.MediaLibrary'), Auth::user()->id);
        if($findRole['canDelete'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {

            /* GET THE RECORD */
            $getValue = Medialibrary::where('id', $id)->get();
            $fileName = $getValue[0]->fileName;
            $destinationPath = public_path('/uploads/media/original/');
            $path = $destinationPath. $fileName;


            /*  DELETE A MEDIA RECORD  */
            if (Medialibrary::deleteRecord($id, $createrModifierId)) {
                unlink($path);
                
                return \Redirect::to('administrator/media?page=' . $page)->with('successMessage', 'File deleted successfully.');
            } else {
                return \Redirect::to('administrator/media?page=' . $page)->with('errorMessage', 'File in operation!');
            }
        } else {
            return \Redirect::to('administrator/media?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }


    /**
     * Method for open popup to use media files
     * @param integer $id
     * @param string $page
     * @return array
     */
    public function popup($id = '0', $page = '') {
        
        $data = array();
        $data['title'] = "Administrative Panel :: Media Library";
        $data['contentTop'] = array('breadcrumbText' => 'Media Library', 'contentTitle' => 'Media Library', 'pageInfo' => '');
        $data['page'] = !empty($page) ? $page : '1';

        
            
            /* SET DATA FOR VIEW  */
            $data['pageTitle'] = "Select Media Files";
            $data['page'] = $page;
            $data['action'] = 'Select';
            $data['mediaData'] = Medialibrary::where('deleted', '0')->orderBy('id', 'desc')->limit(4)->get();

            if(count($data['mediaData']) > 0){

            /* FIND LAST RECORD */
            $count = count($data['mediaData']);

            /* SET THE DEFAULT LAST RECORD ID */
            $data['lastId'] = $data['mediaData'][$count-1]->id;

            /* SET EDITOR ID */
            $data['editorName'] = 'editor'.$page;

            } else {
            $data['lastId'] = 0; }
        
        return view('Administrator.medialibrary.media', $data);
    }


    /**
     * Method for load more data
     * @param Request $request
     * @return array
     */
    public function loaddataajax(Request $request)
    {
        $output = '';
        $id = $request->id;

        /* FIND NEXT SET OF RECORD */
        $mediaData = Medialibrary::where('id','<',$id)->where('deleted', '0')->orderBy('id','desc')->limit(4)->get();
        
        /* GENERATE HTML */
        foreach($mediaData as $media){
            $image = asset('public/uploads/media/thumb/')."/".$media->fileName;
            $output .= "<img id=\"$media->id\" class=\"ui-widget-content\" src=\"$image\" />";
            
        }

        if(count($mediaData) > 0){
        /* GENERATE LOAD BUTTON */
        $output .= '<div id="remove-row"><button id="btn-more" data-id="'.$media->id.'" class="nounderline btn-block mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" > <span class="Cicon" ><i class="fa fa-paper-plane"></i></span> Load More </button></div>';
        } else {
            $output .= '';
        }
        echo $output;

    }

    /**
     * Method for load selected data
     * @param Request $request
     * @return array
     */
    public function loaddataselected(Request $request)
    {
        $output = '';
        /* IMAGE NO RECEIVED AND MAKE AN ARRAY */
        $valuesreceived = rtrim($request->valuesreceived, ',');
        $arrayCreate = (explode(',', $valuesreceived));
        
        /* FIND RECORDS */
        $mediaData = Medialibrary::whereIn('id',$arrayCreate)->where('deleted', '0')->orderBy('id','desc')->get();
        
        /* GENERATE HTML TO BE INSERTED INTO THE WYSIWYG */
        foreach($mediaData as $media){
            $image = asset('public/uploads/media/original/')."/".$media->fileName;
            $output .= "<p><img class=\"ui-widget-content\" src=\"$image\" /></p>";
        }
        
        echo $output;

    }

}
