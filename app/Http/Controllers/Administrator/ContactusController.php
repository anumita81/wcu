<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Ewallet;
use App\Model\User;
use App\Model\Emailtemplate;
use App\Model\Generalsettings;
use App\Model\Contactus;
use App\Model\Contactreason;
use Auth;
use Mail;
use Illuminate\Contracts\Auth\Authenticatable;
use customhelper;
use Config;

/* use Excel;
  use Carbon\Carbon;
  use Illuminate\Support\Facades\Storage; */

class ContactusController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 30;
    }

    public function index(Request $request) {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Contactus'), Auth::user()->id); // call the helper function
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $param = array();
        $dataObj = new Contactus;
        \Session::forget('RECORD');
        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */

            $sortField = \Input::get('field', 'id');
            $sortOrder = \Input::get('type', 'desc');
            $perpage = \Input::get('searchDisplay', $this->_perPage);
            $searchData = \Input::get('searchData', "");
            $searchByReason = \Input::get('searchByReason', '');
            $searchByType = \Input::get('searchByType', '');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('RECORD');
            \Session::push('RECORD.searchData', $searchData);
            \Session::push('RECORD.searchDisplay', $perpage);
            \Session::push('RECORD.field', $sortField);
            \Session::push('RECORD.type', $sortOrder);
            \Session::push('RECORD.searchByReason', $searchByReason);
            \Session::push('RECORD.searchByType', $searchByType);
        } else {

            if (!empty($_GET['type'])) {
                $searchByType = \Input::get('searchByType', '');
                \Session::push('RECORD.searchByType', $_GET['type']);
            }


            $sortField = \Session::get('RECORD.field');
            $sortType = \Session::get('RECORD.type');
            $perpage = \Session::get('RECORD.searchDisplay');
            $searchData = \Session::get('RECORD.searchData');
            $searchByReason = \Session::get('RECORD.searchByReason');
            $searchByType = \Session::get('RECORD.searchByType');

            $sortField = !empty($sortField) ? $sortField[0] : 'id';
            $sortOrder = !empty($sortType) ? $sortType[0] : 'desc';
            $perpage = !empty($perpage) ? $perpage[0] : $this->_perPage;
            $searchData = !empty($searchData) ? $searchData[0] : "";
            $searchByReason = !empty($searchByReason) ? $searchByReason[0] : "";
            $searchByType = !empty($searchByType) ? $searchByType[0] : "";
        }

        $param['sortField'] = $sortField;
        $param['sortOrder'] = $sortOrder;
        $param['searchDisplay'] = $perpage;
        $param['searchData'] = $searchData;
        $param['searchByReason'] = $searchByReason;
        $param['searchByType'] = $searchByType;

        $data['param'] = $param;
        //dd($data['param']);
        $records = $dataObj->getData($param);
        $data['records'] = $records;
        $data['allStatus'] = $dataObj->getAllStatus();
        $data['page'] = $records->currentPage();
        $data['pageTitle'] = 'Contact Us Requests';
        $data['title'] = "Contact Us :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText' => array('Contact Us Requests'), 'contentTitle' => 'Contact Us Requests', 'pageInfo' => 'This section allows you to manage contact us requests');
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavMessageBoard', 'menuSub' => '', 'menuSubSub' => 'leftNavContactUsRequests11');
        $data['reasons'] = Contactreason::select('name', 'id')->where('status', '1')->whereNotIn('id', [11, 12])->orderby('id', 'desc')->get();
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.contactus.index', $data);
    }

    public function getdetails($id = '-1', $page = 1) {

        $data = array();
        $dataObj = new Contactus;
        if ($id != '-1')
            $dataObj = $dataObj->find($id);
        if (empty($dataObj)) {
            return redirect(route('contactuslist') . '?page=' . $page)->with('errorMessage', 'No Data');
        }
        if ($dataObj->status <= 2) {
            Contactus::where('id', $id)->update(['status' => '2']);
        }

        /* GET THE REPLY DATA */
        $replied = \App\Model\Contactreply::getList($id);
        if (empty($replied)) {
            $data['replied'] = $replied;
        } else {
            $data['replied'] = $replied;
        }

        $data['record'] = $dataObj;
        $data['page'] = $page;
        $data['id'] = $id;
        $data['pageTitle'] = 'Contact Us Requests';
        $data['title'] = "Contact Us :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText' => array('Contact Us Requests'), 'contentTitle' => 'Contact Us Requests', 'pageInfo' => 'This section allows you to manage contact us requests');

        return view('Administrator.contactus.details', $data);
    }

    public function reply($id, $page, Request $request) {

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Contactus'), Auth::user()->id); // call the helper function
        if ($findRole['canAdd'] == 0 || $findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $dataObj = Contactus::find($id);
        $user = User::where('email', $dataObj->email)->where('deleted', '0')->first();

        /* Save & Send Notification */
        User::sendPushNotification($user->id, 'contact_us_reply', Auth::user()->id);

        $to = $dataObj->email;
        $content = $request->reply;
        $subject = $dataObj->subject;
        $send = Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($subject, $to) {
                    $message->from('contact@shoptomydoor.com', 'Shoptomydoor');
                    $message->subject($subject);
                    $message->to($to);
                });

        /* STORE DATA IN CONTACT REPLY TABLE */
        $replied = \App\Model\Contactreply::insert(['contactusId' => $id, 'replyText' => $request->reply, 'repliedBy' => Auth::user()->id, 'repliedOn' => Config::get('constants.CURRENTDATE')]);

        Contactus::where('id', $id)->update(['status' => '3', 'repliedOn' => date('Y-m-d h:i:s')]);
        return redirect(route('contactuslist') . '?page=' . $page)->with('successMessage', 'Reply sent successfuly.');
    }

    public function deletedata($id = '-1', $page = 1) {

        $dataObj = new Contactus;
        if ($id != '-1') {
            #$dataObj = $dataObj->find($id);
            #if($dataObj->delete())
            $row = Contactus::where('id', $id)->update(array('deleted' => '1', 'deletedBy' => Auth::user()->id, 'deletedOn' => Config::get('constants.CURRENTDATE')));
            return redirect(route('contactuslist') . '?page=' . $page)->with('successMessage', 'Information deleted successfuly.');
        }
    }

    public function deleteselected(Request $request) {

        $dataObj = new Contactus;
        $idsToDelete = $request->input('deleteData');
        foreach ($idsToDelete as $eachId) {
            /* $dataObj = $dataObj->find($eachId);
              if($dataObj->delete())
              $dataObj = new Contactus; */
            $row = Contactus::where('id', $eachId)->update(array('deleted' => '1', 'deletedBy' => Auth::user()->id, 'deletedOn' => Config::get('constants.CURRENTDATE')));
        }
        return redirect(route('contactuslist'))->with('successMessage', 'Information deleted successfuly.');
    }

    public function resolved($id, $page = 1) {
        if (empty($id)) {
            return redirect(route('contactuslist'))->with('errorMessage', 'No Info');
        }

        $dataObj = new Contactus;
        Contactus::where('id', $id)->update(['status' => '4']);
        return redirect(route('contactuslist') . '?page=' . $page)->with('successMessage', 'This issue has been resolved.');
    }

}
