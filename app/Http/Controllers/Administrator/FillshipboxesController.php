<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Fillshipbox;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;


class FillshipboxesController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index() {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.FillShipBoxes'), Auth::user()->id); // call the helper function
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $data = array();
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('FILLLSHIPBOXESDATA');
            \Session::push('FILLLSHIPBOXESDATA.searchDisplay', $searchDisplay);
            \Session::push('FILLLSHIPBOXESDATA.field', $field);
            \Session::push('FILLLSHIPBOXESDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('FILLLSHIPBOXESDATA.field');
            $sortType = \Session::get('FILLLSHIPBOXESDATA.type');
            $searchDisplay = \Session::get('FILLLSHIPBOXESDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
            'dimension' => array('current' => 'sorting'),
            'clearingAndDuty' => array('current' => 'sorting'),
            'warehousing' => array('current' => 'sorting'),
            'orderby' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH rate LIST  */
        $boxData = Fillshipbox::getList($param);
        if (count($boxData) > 0) {
            $data['page'] = $boxData->currentPage();
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Fill And Ship Boxes";
        $data['contentTop'] = array('breadcrumbText' => 'Fill And Ship Boxes', 'contentTitle' => 'Fill And Ship Boxes', 'pageInfo' => 'This section allows you to manage fill and ship box details');
        $data['pageTitle'] = "Fill And Ship Boxes";
        $data['boxData'] = $boxData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavFillShip', 'menuSub' => '', 'menuSubSub' => 'leftNavFillShipBoxes8');

        return view('Administrator.fillshipboxes.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Fill And Ship Boxes";
        $data['contentTop'] = array('breadcrumbText' => 'Fill And Ship Boxes', 'contentTitle' => 'Fill And Ship Boxes', 'pageInfo' => 'This section allows you to manage fill and ship box details');
        $data['page'] = !empty($page) ? $page : '1';


        if ($id != 0) {
            $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.FillShipBoxes'), Auth::user()->id); // call the helper function
            if ($findRole['canEdit'] == 0) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['pageTitle'] = "Edit Fill And Ship Boxes";
            $data['id'] = $id;
            $data['page'] = $page;
            $data['action'] = 'Edit';
            $boxdata = Fillshipbox::getListById($id);
            if (count($boxdata) == 0) {
                return redirect('/administrator/fillshipboxes')->with('errorMessage', 'No data found!');
            }
            $data['boxData'] = $boxdata[0];
        } else {
            $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.FillShipBoxes'), Auth::user()->id); // call the helper function
            if ($findRole['canAdd'] == 0) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add Fill And Ship Boxes";
            $data['page'] = $page;
        }
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavFillShip', 'menuSub' => '', 'menuSubSub' => 'leftNavFillShipBoxes8');
        return view('Administrator.fillshipboxes.add', $data);
    }

    /*     * Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.FillShipBoxes'), Auth::user()->id); // call the helper function
        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $fillshipboxes = new Fillshipbox;

        $validator = Validator::make($request->all(), [
                    'name' => 'required:' . $fillshipboxes->table . ',name,' . $id,
                    'length' => 'required|numeric:' . $fillshipboxes->table . ',length,' . $id,
                    'width' => 'required|numeric:' . $fillshipboxes->table . ',width,' . $id,
                    'height' => 'required|numeric:' . $fillshipboxes->table . ',height,' . $id,
                    'clearingAndDuty' => 'required|numeric:' . $fillshipboxes->table . ',clearingAndDuty,' . $id,
                    'warehousing' => 'required|numeric:' . $fillshipboxes->table . ',warehousing,' . $id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (($id != 0)) { // edit
                if ($findRole['canEdit'] == 0) {
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
                $fillshipboxes = Fillshipbox::find($id);
                $fillshipboxes->modifiedBy = Auth::user()->id;
                $fillshipboxes->modifiedOn = Config::get('constants.CURRENTDATE');
            }
            if ($findRole['canAdd'] == 0) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }

            $settings = \App\Model\Generalsettings::select('settingsValue')->where('settingsKey', 'weight_factor')->first();

            $chargeableWeight = ($request->length * $request->width * $request->height) / $settings->settingsValue;

            $fillshipboxes->name = $request->name;
            $fillshipboxes->length = $request->length;
            $fillshipboxes->width = $request->width;
            $fillshipboxes->height = $request->height;
            $fillshipboxes->chargeableWeight = $chargeableWeight;
            $fillshipboxes->clearingAndDuty = $request->clearingAndDuty;
            $fillshipboxes->warehousing = $request->warehousing;
            $fillshipboxes->description = $request->description;
            $fillshipboxes->customerNotes = $request->customerNotes;
            $fillshipboxes->warehousing = $request->warehousing;
            $fillshipboxes->orderby = $request->orderby;

            if ($request->hasFile('upload_cont_img')) {
                $image = $request->file('upload_cont_img');
                if (substr($image->getMimeType(), 0, 5) == 'image') {
                    $name = time() . '_' . $image->getClientOriginalName();
                    $destinationPath = public_path('/uploads/fillship');
                    $image->move($destinationPath, $name);
                    $fillshipboxes->uploadedImage = $name;
                }
            }

            $fillshipboxes->save();

            return redirect('/administrator/fillshipboxes')->with('successMessage', 'Information saved successfuly.');
        }
    }

    /**
     * Method for Show page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function showData($id = '0') {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.FillShipBoxes'), Auth::user()->id); // call the helper function
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $data['title'] = "Administrative Panel :: Fill And Ship Boxes";
        $data['contentTop'] = array('breadcrumbText' => 'Fill And Ship Boxes', 'contentTitle' => 'Fill And Ship Boxes', 'pageInfo' => 'This section allows you to manage fill and ship box details');


        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.FillShipBoxes'), Auth::user()->id); // call the helper function
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $data['action'] = 'Show';
        $data['pageTitle'] = "Add Fill And Ship Boxes";
        $boxdata = Fillshipbox::getListById($id);
        //dd($boxdata[0]->name);
        if (count($boxdata) == 0) {
            return redirect('/administrator/fillshipboxes')->with('errorMessage', 'No data found!');
        }
        $data['boxData'] = $boxdata[0];
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavFillShip', 'menuSub' => '', 'menuSubSub' => 'leftNavFillShipBoxes8');
        return view('Administrator.fillshipboxes.view', $data);
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.FillShipBoxes'), Auth::user()->id); // call the helper function
        if ($findRole['canDelete'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Fillshipbox::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/fillshipboxes/?page=' . $page)->with('successMessage', 'Record deleted successfully.');
            } else {
                return \Redirect::to('administrator/fillshipboxes/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/fillshipboxes/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
