<?php
namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use App\Model\Consolidatedtracking;
use App\Model\Consolidatedtrackingnumber;
use App\Model\Consolidatedtrackingnotes;
use App\Model\Consolidatedtrackingfiles;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Excel;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Hash;
use Mail;
use Carbon\Carbon;
use customhelper;
use PDF;
use Response;
use DB;

class ConsolidatedtrackingController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Consolidatedtracking'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

         $searchTrackingArr = array(
            'trackingName' => '',
            'trackingNumber' => '',
        );
         
        if (\Request::isMethod('post')) {

            /* GET POST VALUE  */ 
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $searchTracking = \Input::get('searchTracking', $searchTrackingArr);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('CONSOLIDATEDDATA');
            \Session::push('CONSOLIDATEDDATA.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('CONSOLIDATEDDATA.searchDisplay', $searchDisplay);
            \Session::push('CONSOLIDATEDDATA.searchTracking', $searchTracking);
            \Session::push('CONSOLIDATEDDATA.field', $field);
            \Session::push('CONSOLIDATEDDATA.type', $type);
            

            $param['field'] = $field;
            $param['type'] = $type;            
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchDisplay'] = $searchDisplay;
            $param['searchTracking'] = $searchTracking;
        } else {
            $sortField = \Session::get('CONSOLIDATEDDATA.field');
            $sortType = \Session::get('CONSOLIDATEDDATA.type');
            $searchDisplay = \Session::get('CONSOLIDATEDDATA.searchDisplay');
            $searchTracking = \Session::get('CONSOLIDATEDDATA.searchTracking');            
            $searchByCreatedOn = \Session::get('CONSOLIDATEDDATA.searchByCreatedOn');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;            
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchTracking'] = !empty($searchTracking) ? $searchTracking[0] : $searchTrackingArr;
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
        }


        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
            'email' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting')
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH TRACKING LIST  */
        $trackingData = Consolidatedtracking::getTrackingList($param, '');

        \Session::forget('CONSOLIDATEDDATA');

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Consolidated Tracking";
        $data['contentTop'] = array('breadcrumbText' => 'Consolidated Tracking', 'contentTitle' => 'Consolidated Tracking', 'pageInfo' => 'This section allows you to manage consolidated tracking');
        $data['pageTitle'] = "Consolidated Tracking";
        $data['page'] = $trackingData->currentPage();
        $data['trackingData'] = $trackingData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        
        return view('Administrator.consolidatedtracking.index', $data);
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Consolidatedtracking::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/consolidatedtracking/?page=' . $page)->with('successMessage', 'Tracking Number deleted successfully.');
            } else {
                return \Redirect::to('administrator/consolidatedtracking/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/consolidatedtracking/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    
    
    /**
     * Method used to view tracking numbers
     * @param integer $id
     * @param integer $page
     * @return html
     */
    public function viewtracking($id, $page) {
        $data['pageTitle'] = "View Tracking Numbers";
        $shipmentTrackingNum = new Consolidatedtrackingnumber;
        $data['trackingNumData'] = Consolidatedtrackingnumber::where('trackingId', $id)->get();

        return view('Administrator.consolidatedtracking.viewtracking', $data);
    }

     /**
     * Method for add edit page
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        
        $data['pageTitle'] = "Consolidated Tracking";
        $data['page'] = !empty($page) ? $page : '1';

        $data['id'] = !empty($id) ? $id : "0";
        $data['action'] = 'Add';
        $data['consolidatedtracking'] = Consolidatedtracking::find($id);
        $data['consolidatedLog'] = \App\Model\Consolidatedtrackinglog::where('trackingId',$id)->get();
        $data['trackingNumData'] = Consolidatedtrackingnumber::where('trackingId', $id)->get();
        
        return view('Administrator.consolidatedtracking.addtracking', $data);
    }

    /**
     * Method used to save tarcking information
     * @param integer $id
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {
        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;
        
        $consolidatedtracking = new Consolidatedtracking;
        $consolidatedLogData = array();

        if (!empty($id)) {
            $validator = Validator::make($request->all(), [
                        'trackingName' => 'required|unique:'.$consolidatedtracking->table.',name,'.$id,
            ]);
        } else {
            $validator = Validator::make($request->all(), [                        
                        'fileName' => 'required',
                        'fileName.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                        'deliveredOn' => 'required',
                        'trackingName' => 'required|unique:'.$consolidatedtracking->table.',name',
                        'trackingNum' => 'required',
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $consolidatedtracking = Consolidatedtracking::find($id);
                $consolidatedtracking->modifiedBy = Auth::user()->id;
                $consolidatedtracking->modifiedOn = Config::get('constants.CURRENTDATE');
            } else {
                $consolidatedtracking->createdBy = Auth::user()->id;
                $consolidatedtracking->createdOn = Config::get('constants.CURRENTDATE');
            }
            $consolidatedtracking->deliveredOn = Carbon::parse($request->deliveredOn);
            $consolidatedtracking->name = $request->trackingName;
            $consolidatedtracking->notes = $request->notes;
            if(isset($request->action) && $request->action == 'submit_close')
                $consolidatedtracking->isClosed = "1";
            $consolidatedLogData['notes'] = $request->notes;
            $consolidatedtracking->save();
            $consolidatedTrackingId = $consolidatedtracking->id;

            if (!empty($consolidatedTrackingId)) {                
                /* Shipment Tracking Files */
                if ($request->hasFile('fileName')) {
                    foreach ($request->file('fileName') as $image) {
                        $fileName = time() . '_' . $image->getClientOriginalName();
                        $image->move(public_path() . '/uploads/consolidatedtracking', $fileName);
                        $consolidatedLogData['image'][] = $fileName;
                        $trackingFile = new Consolidatedtrackingfiles;
                        $trackingFile->trackingId = $consolidatedTrackingId;
                        $trackingFile->fileName = $fileName;
                        $trackingFile->createdBy = Auth::user()->id;
                        $trackingFile->createdOn = Config::get('constants.CURRENTDATE');
                        $trackingFile->save();
                    }
                }
                
                /* Shipment Tracking Numbers */
                $trackingNumbers = preg_split('/\r\n|[\r\n]/', $request->trackingNum);

                if (!empty($trackingNumbers)) {
                    if(!empty($id))
                    {
                        Consolidatedtrackingnumber::where('trackingId', $id)->delete();
                    }
                    foreach ($trackingNumbers as $tracking) {
                        $trackingNumber = new Consolidatedtrackingnumber;
                        $trackingNumber->trackingId = $consolidatedTrackingId;
                        $trackingNumber->trackingNumber = $tracking;
                        $trackingNumber->createdBy = Auth::user()->id;
                        $trackingNumber->createdOn = Config::get('constants.CURRENTDATE');
                        $consolidatedLogData['trackingNumber'][] = $tracking;
                        $trackingNumber->save();
                    }
                }
                
                $consolidateLog = new \App\Model\Consolidatedtrackinglog;
                $consolidateLog->trackingId = $consolidatedTrackingId;
                $consolidateLog->adminEmail = Auth::user()->email;
                $consolidateLog->logData = json_encode($consolidatedLogData);
                $consolidateLog->createdOn = Config::get('constants.CURRENTDATE');
                $consolidateLog->save();
                
            }
            
            if(isset($request->action) && $request->action == 'submit_close')
            {
                return json_encode(array('id'=>$consolidatedtracking->id));
            }
            else
                return redirect('/administrator/consolidatedtracking?page=' . $page)->with('successMessage', 'Shipment tracking information saved successfuly.');
        }
    }
    
    public function getsubmitcloseoption($id,$page = 0) {
        
        $data = array();
        
        $data['id'] = $id;
        $data['page'] = $page;
        $data['submitCloseOptions'] = Consolidatedtracking::submitCloseOptions();
        return view('Administrator.consolidatedtracking.trackingcloseoptions', $data);
    }
    
    public function savetackingoptions($id,$page = 0,Request $request) {
        
        $optionsValues = array();
        
        $optionsValues['shipmentType'] = $request->shipmentType;
        $optionsValues['consolidatedOptions'] = ($request->shipmentType == 'air')?$request->air:$request->sea;
        if($request->shipmentType == 'sea')
        {
            //print_r($request->sea['finalview_container']['image_finalview']);
            foreach($request->sea as $mainoptionField => $mainOption)
            {
                foreach($mainOption as $suboptionIndex=>$subOptionVal)
                {
                    if(strstr($suboptionIndex,'image'))
                    {
                        $sourcePath = $request->sea[$mainoptionField][$suboptionIndex]->getPathName();
                        $fileName = time() . '_' . $request->sea[$mainoptionField][$suboptionIndex]->getClientOriginalName();
                        move_uploaded_file($sourcePath, public_path() . '/uploads/consolidatedtracking/'.$fileName);
                        $optionsValues['consolidatedOptions'][$mainoptionField][$suboptionIndex] = $fileName;
                    }
                }
            }
        }

        $consolidatedOption = json_encode($optionsValues);
        \App\Model\Consolidatedtracking::where('id',$id)->update(['consolidatedOptions'=>$consolidatedOption]);
        return \Redirect::to('administrator/consolidatedtracking?page='.$page)->with('successMessage', 'Data saved successfully');
    }
    
    public function viewcloseoptions($id,$page = 0) {
        
        $data = array();
        
        $closeOptions = Consolidatedtracking::select('consolidatedOptions')->find($id);
        $savedOptions = json_decode($closeOptions->consolidatedOptions,true);
        
        $data['consolidatedOptionSettings'] = Consolidatedtracking::submitCloseOptions();
        $data['consolidatedOptions'] = $savedOptions['consolidatedOptions'];
        $data['shipmentType'] = $savedOptions['shipmentType'];
        $data['imageUploadPath'] = url('/public/uploads/consolidatedtracking/');
  
        return view('Administrator.consolidatedtracking.consolidateoptions', $data);
    }

    public function printconsolidateddata($id,$page = 0) {
        
        $data = array();

        $data['pageTitle'] = "Print Consolidated Tracking";
        $data['consolidatedtracking'] = Consolidatedtracking::find($id);
        $data['trackingNumData'] = Consolidatedtrackingnumber::where('trackingId', $id)->get();
        $data['warehouseDetails'] = \App\Model\Warehouse::where('id','1')->with('country','state','city')->first();
        //print_r($data['warehouseDetails']);exit;
        return view('Administrator.consolidatedtracking.printconsolidated', $data);
    }

        /**
     * Method used to generate and export all tracking records
     * @param integer $page
     * @param Request $request
     * @return string
     */
    public function exportall($page, Request $request) {

        $consolidatedtracking = new Consolidatedtracking;
        $param = array();

        $searchTrackingArr = array(
            'trackingName' => '',
            'trackingNumber' => '',
        );
        
       
        $searchByCreatedOn = \Session::get('CONSOLIDATEDDATA.searchByCreatedOn');
        $searchByDate = \Session::get('CONSOLIDATEDDATA.searchByDate');
        $searchTracking = \Session::get('CONSOLIDATEDDATA.searchTracking');

        $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
        $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
        $param['searchTracking'] = !empty($searchTracking) ? $searchTracking[0] : $searchTrackingArr;
        
        
        $exportData = $consolidatedtracking->exportData($param, array(), $request->selectall);
        
        //print_r($exportData); die;

        $excelName = "Consolidated-Tracking-Data" . Carbon::now();
        
        ob_end_clean();
        ob_start();
        
        Excel::create($excelName, function($excel) use($exportData) {
            $excel->sheet('Sheet 1', function($sheet) use($exportData) {
                $sheet->fromArray($exportData);
            });
        })->export('xlsx');
        
        ob_flush();
        
        return \Redirect::to('administrator/consolidatedtracking/')->with('successMessage', 'Excel file created and downloaded');
    }
    
    /**
     * Method used to delete multiple records
     * @return type
     */
    public function deleteall(Request $request) {
        $createrModifierId = Auth::user()->id;
        $checkedval = $request->checkedval;

        if (!empty($checkedval)) {
            $idArray = explode('^', $checkedval);

            foreach ($idArray as $id) {
                Consolidatedtracking::deleteRecord($id, $createrModifierId);
            }
            return \Redirect::to('administrator/consolidatedtracking')->with('successMessage', 'Consolidated tracking deleted successfully.');
        } else {
            return \Redirect::to('administrator/consolidatedtracking/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    
    /**
     * Method used to view images files
     * @param integer $id
     * @param integer $page
     * @return html
     */
    public function viewfiles($id, $page) {
        $data['pageTitle'] = "View Files";
        
        $data['trackingFileData'] = Consolidatedtrackingfiles::where('trackingId', $id)->get();

        return view('Administrator.consolidatedtracking.viewfiles', $data);
    }
    
    public function commercialinvoice(Request $request) {
        
        \Session::forget('QUICKSHIPOUTDATA');
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Commercialinvoice'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

         $searchTrackingArr = array(
            'originCountry' => '',
            'shipperInfo' => '',
            'email' => '',
            'airBillNumber' => '',
            'shipoutLocked' => '',
            'trackingNumber' => '',
            'unitNumber' => '',
        );
         
        if (\Request::isMethod('post')) {

            /* GET POST VALUE  */ 
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $searchTracking = \Input::get('searchTracking', $searchTrackingArr);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('CONSOLIDATEDDATA');
            \Session::push('CONSOLIDATEDDATA.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('CONSOLIDATEDDATA.searchDisplay', $searchDisplay);
            \Session::push('CONSOLIDATEDDATA.searchTracking', $searchTracking);
            \Session::push('CONSOLIDATEDDATA.field', $field);
            \Session::push('CONSOLIDATEDDATA.type', $type);
            

            $param['field'] = $field;
            $param['type'] = $type;            
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchDisplay'] = $searchDisplay;
            $param['searchTracking'] = $searchTracking;
        } else {
            $sortField = \Session::get('CONSOLIDATEDDATA.field');
            $sortType = \Session::get('CONSOLIDATEDDATA.type');
            $searchDisplay = \Session::get('CONSOLIDATEDDATA.searchDisplay');
            $searchTracking = \Session::get('CONSOLIDATEDDATA.searchTracking');            
            $searchByCreatedOn = \Session::get('CONSOLIDATEDDATA.searchByCreatedOn');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;            
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchTracking'] = !empty($searchTracking) ? $searchTracking[0] : $searchTrackingArr;
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
        }


        /* BUILD SORTING ARRAY */
        $sort = array(
            'invoiceName' => array('current' => 'sorting'),
            'email' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting')
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH TRACKING LIST  */
        $trackingData = \App\Model\Commercialinvoice::getData($param);
        

        /* SET DATA FgeDetails(OR VIEW  */
        $data['title'] = "Administrative Panel :: Quick Ship Out - Air";
        $data['contentTop'] = array('breadcrumbText' => 'Quick Ship Out - Air', 'contentTitle' => 'Quick Ship Out', 'pageInfo' => 'This section allows you to manage quick ship out');
        $data['pageTitle'] = "Quick Ship Out - Air";
        $data['page'] = $trackingData->currentPage();
        $data['trackingData'] = $trackingData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        
        return view('Administrator.consolidatedtracking.commercialinvoice', $data);
    }
    
    
    public function createcommercialinvoice(Request $request) {
        
        //print_r($request->all());
        $isDataSearched = 1;
        $data = array();
        $invoiceDetails = array();
        $orderInfo = "";
        $invoiceName = (isset($request->invoiceName) && $request->invoiceName!="") ? $request->invoiceName : "";
        $shipperInfo = (isset($request->shipperInfo) && $request->shipperInfo!="") ? $request->shipperInfo : "";
        $consigneeInfo = (isset($request->consigneeInfo) && $request->consigneeInfo!="") ? $request->consigneeInfo : "";
        if(!\Request::isMethod('post'))
        {
            $isDataSearched = 0;
            \Session::forget('invoceDetailsData');
        }
        if(\Session::has('invoceDetailsData'))
        {
            $invoiceData = \Session::get('invoceDetailsData');
            $invoiceDetails = json_decode($invoiceData[0],true);
        }
        if(!empty($request->checkboxselected))
        {
            $isDataSearched = 0;
            foreach($request->checkboxselected as $eachPackage)
            {
                $shipmentPackage = \App\Model\Shipmentpackage::find($eachPackage);
//                $shipmentPackage->addedtoCommercialInvoice = "1";
//                $shipmentPackage->save();
                
                $postOrderId = $request->invoicePackage[$eachPackage]['orderId'];
                $invoiceDetails[$postOrderId]['orderNumber'] = $request->invoicePackage[$eachPackage]['orderNumber'];
                $invoiceDetails[$postOrderId]['destination'] = $request->invoicePackage[$eachPackage]['destination'];
                $invoiceDetails[$postOrderId]['origin'] = $request->invoicePackage[$eachPackage]['origin'];
                $invoiceDetails[$postOrderId]['receiver'] = $request->invoicePackage[$eachPackage]['receiver'];
                $invoiceDetails[$postOrderId]['packages'][$eachPackage]['orderId'] = $request->invoicePackage[$eachPackage]['orderId'];
                $invoiceDetails[$postOrderId]['packages'][$eachPackage]['itemName'] = $request->invoicePackage[$eachPackage]['itemName'];
                $invoiceDetails[$postOrderId]['packages'][$eachPackage]['storeName'] = $request->invoicePackage[$eachPackage]['storeName'];
                $invoiceDetails[$postOrderId]['packages'][$eachPackage]['itemQuantity'] = $request->invoicePackage[$eachPackage]['itemQuantity'];
                $invoiceDetails[$postOrderId]['packages'][$eachPackage]['itemPrice'] = $request->invoicePackage[$eachPackage]['itemPrice'];
                $invoiceDetails[$postOrderId]['packages'][$eachPackage]['totalPrice'] = $request->invoicePackage[$eachPackage]['totalPrice'];
                $invoiceDetails[$postOrderId]['packages'][$eachPackage]['productSchedule'] = $request->invoicePackage[$eachPackage]['productSchedule'];
                $invoiceDetails[$postOrderId]['packages'][$eachPackage]['weight'] = $request->invoicePackage[$eachPackage]['weight'];
            }
            \Session::forget('invoceDetailsData');
            \Session::push('invoceDetailsData', json_encode($invoiceDetails));
            $request->orderInfo = "";
        }
        
        if(isset($request->orderInfo) && $request->orderInfo!='')
        {
            $orderDetails = \App\Model\Order::getPackedOrders($request->orderInfo);
            if(!empty($orderDetails))
            {
                $orderNumber = $request->orderInfo;
                $shipmentId = $orderDetails->shipmentId;
                $orderId = $orderDetails->id;
                $shipmentData = \App\Model\Viewshipment::select('fromCountryName','toCountryName','toCityName','toName')->where('id',$shipmentId)->first();
                /* Fetch  Shipment Delivery Data */
                $deliveryData = \App\Model\ViewShipmentDetails::where("shipmentId", $shipmentId)->where('deleted', '0')->where('packageDeleted', '0')->where('packageType', 'I')->where('addedtoCommercialInvoice','0')->orderBy('deliveryId', 'asc')->get()->toarray();
                $productScheduleData = collect(\App\Model\Siteproduct::get());
                $data['productScheduleList'] = $productScheduleData->mapWithKeys(function($item){ return [$item['id']=>$item['scheduleBNumber']]; });
                $data['shipmentInfo'] = $shipmentData;
                $data['packageInfo'] = \App\Model\Shipment::getDeliveryDetails($deliveryData);
                $data['orderNumber'] = $orderNumber;
                $data['shipmentNumber'] = $orderNumber;
                $data['orderId'] = $orderId;
            }
        }
        //$order = \App\Model\Order::getPackedOrders();
        $data['invoiceName'] = $invoiceName;
        $data['shipperInfo'] = $shipperInfo;
        $data['consigneeInfo'] = $consigneeInfo;
        $data['invoiceDetails'] = $invoiceDetails;
        $data['orderInfo'] = $orderInfo;
        $data['isDataSearched'] = $isDataSearched;
        $data['title'] = "Administrative Panel :: Quick Ship Out - Air";
        $data['contentTop'] = array('breadcrumbText' => 'Quick Ship Out - Air', 'contentTitle' => 'Quick Ship Out', 'pageInfo' => 'This section allows you to manage quick ship outs');
        $data['pageTitle'] = "Quick Ship Out - Air";
        
        return view('Administrator.consolidatedtracking.createcommercialinvoice', $data);
    }
    
    public function createquickshipout() {        
        $shipmentTracking = new \App\Model\Shipmenttracking;
        $shipmentTracking->name = "New quick shipout";
        $shipmentTracking->deleted = "1";
        $shipmentTracking->createdBy = Auth::user()->id;
        $shipmentTracking->createdOn = Config::get('constants.CURRENTDATE');
        $shipmentTracking->save();
        \Session::push('QUICKSHIPOUTDATA.shipmentTrackingId',$shipmentTracking->id);

        $quickshipout = new \App\Model\Commercialinvoice;
        $quickshipout->shipmentTrackingId = $shipmentTracking->id;
        $quickshipout->deleted = "1";
        $quickshipout->createdBy = Auth::user()->id;
        $quickshipout->createdOn = Config::get('constants.CURRENTDATE');
        $quickshipout->save();
        $qucikShipoutId = $quickshipout->id;
        
        return \Redirect::to('administrator/consolidated/quick-shipout/edit/'.$qucikShipoutId);
        
    }


    public function editquickshipout($qucikShipoutId = 0) {
        
        $data = array();
        $quickShipoutDetails = \App\Model\Commercialinvoice::find($qucikShipoutId);
        $data['shipmentType'] = \App\Model\Shipment::allParticularShipmenttype();
        $data['qucikShipoutId'] = $qucikShipoutId;
        $data['shipoutDetails'] = $quickShipoutDetails;
        $data['shipmentTrackingId'] = $quickShipoutDetails->shipmentTrackingId;
        //print_r($data['shipoutDetails']); die;
        $data['notificationLog'] = \App\Model\Quickshipoutnotification::getNotificationHistory($qucikShipoutId);
        //print_r($data['notificationLog']);exit;
        $data['subcategoryArr'] = \App\Model\Sitecategory::getMappedSubcategory();
        $data['productArr'] = \App\Model\Siteproduct::getSubcategoryMappedProduct();
        $data['quickshipoutInfo'] = \App\Model\Quickshipoutinfo::where("status","1")->where("deleted","0")->get();
        
        $cityArr = array();
        if(!empty($data['shipoutDetails']['destinationCountryId']))
        {
            $countryData = \App\Model\Country::find($data['shipoutDetails']['destinationCountryId']);
            $cityArr = \App\Model\City::where('countryCode',$countryData->code)->where('status','1')->where('deleted','0')->orderby('name', 'asc')->get();
        }
        
        /* FETCH STORE LIST  */
        $data['storeList'] = \App\Model\Stores::where('deleted', '0')->where('status', '1')->orderby('storeName', 'asc')->get();

        /* FETCH CATEGORY LIST  */
        $data['categoryList'] = \App\Model\Sitecategory::getCategoryList('parent');
        $data['countryList'] = \App\Model\Country::where('status', '1')->orderby('name', 'asc')->get();
        $data['destinationCityList'] = $cityArr;
        $data['title'] = "Administrative Panel :: Quick Ship Out - Air";
        $data['contentTop'] = array('breadcrumbText' => 'Quick Ship Out', 'contentTitle' => 'Quick Ship Out', 'pageInfo' => 'This section allows you to manage quick ship outs');
        $data['pageTitle'] = "Quick Ship Out - Air";
        
        return view('Administrator.consolidatedtracking.createquickshipout', $data);
    }

    public function savequickshipout(Request $request) {
        //print_r($request->all());die;
        $dataSave = 0;
        $invoiceDetails = $findStatus = array();
        $validator = Validator::make($request->all(), [
                    'shipoutName' => 'required',
                    'shipperInfo' => 'required',
                    'consigneeInfo' => 'required',
        ]);
        
        if ($validator->fails()) {
            //return redirect()->back()->withErrors($validator)->withInput();
        }
            

        $commonIndex = 0;
        $originCountry = \App\Model\Country::find($request->originCountry)->name;
        $destinationCountry = \App\Model\Country::find($request->destinationCountry)->name;
        $destinationCity = \App\Model\City::find($request->destinationCity)->name;
        
        $shipmentTypeArr = \App\Model\Shipment::allParticularShipmenttype();
        
        $shipmentTrackingid = $request->shipmentTrackingId;
        $quickShipoutAddedId = $request->quickShipoutId;
        if (!empty($shipmentTrackingid) && !empty($quickShipoutAddedId)) {
            \App\Model\Shipmenttrackingnumber::where('trackingId', $shipmentTrackingid[0])->delete();

            for ($index = 0; $index < count($request->shipout['tracking']); $index++) {
                if (!empty($request->shipout['tracking'][$index])) {
                    $store = $siteCategory = $siteSubcategory = "Not Listed";
                    $siteProduct = "";
                    if ($request->shipout['store'][$index] != "0")
                        $store = \App\Model\Stores::find($request->shipout['store'][$index])->storeName;
                    if ($request->shipout['siteCategoryId'][$index] != "0")
                        $siteCategory = \App\Model\Sitecategory::find($request->shipout['siteCategoryId'][$index])->categoryName;
                    if ($request->shipout['siteSubCategoryId'][$index] != "0")
                        $siteSubcategory = \App\Model\Sitecategory::find($request->shipout['siteSubCategoryId'][$index]);
                    if ($request->shipout['siteProductId'][$index] != "0")
                        $siteProduct = \App\Model\Siteproduct::find($request->shipout['siteProductId'][$index]);

                    $invoiceDetails[$commonIndex]['packages'][$index]['tracking'] = $request->shipout['tracking'][$index];
                    $invoiceDetails[$commonIndex]['packages'][$index]['unit'] = $request->shipout['unit'][$index];
                    $invoiceDetails[$commonIndex]['packages'][$index]['shipmentType'] = $request->shipout['shipmentType'][$index];
                    $invoiceDetails[$commonIndex]['packages'][$index]['category'] = $siteCategory;
                    $invoiceDetails[$commonIndex]['packages'][$index]['categoryId'] = $request->shipout['siteCategoryId'][$index];
                    $invoiceDetails[$commonIndex]['packages'][$index]['subCategory'] = (isset($siteSubcategory->categoryName) ? $siteSubcategory->categoryName : "");
                    $invoiceDetails[$commonIndex]['packages'][$index]['subCategoryId'] = $request->shipout['siteSubCategoryId'][$index];
                    $invoiceDetails[$commonIndex]['packages'][$index]['productname'] = (isset($siteProduct->productName) ? $siteProduct->productName : (isset($siteSubcategory->categoryName) ? $siteSubcategory->categoryName : ""));
                    $invoiceDetails[$commonIndex]['packages'][$index]['productId'] = $request->shipout['siteProductId'][$index];
                    $invoiceDetails[$commonIndex]['packages'][$index]['itemName'] = (isset($request->shipout['item'][$index]) ? $request->shipout['item'][$index] : "");
                    $invoiceDetails[$commonIndex]['packages'][$index]['storeName'] = $store;
                    $invoiceDetails[$commonIndex]['packages'][$index]['storeId'] = $request->shipout['store'][$index];
                    $invoiceDetails[$commonIndex]['packages'][$index]['itemQuantity'] = $request->shipout['quantity'][$index];
                    $invoiceDetails[$commonIndex]['packages'][$index]['itemPrice'] = $request->shipout['value'][$index];
                    $invoiceDetails[$commonIndex]['packages'][$index]['totalPrice'] = $request->shipout['total'][$index];
                    $invoiceDetails[$commonIndex]['packages'][$index]['productSchedule'] = (isset($siteProduct->scheduleBNumber) ? $siteProduct->scheduleBNumber : (isset($siteSubcategory->scheduleBNumber) ? $siteSubcategory->scheduleBNumber : ""));
                    $invoiceDetails[$commonIndex]['packages'][$index]['length'] = $request->shipout['length'][$index];
                    $invoiceDetails[$commonIndex]['packages'][$index]['width'] = $request->shipout['width'][$index];
                    $invoiceDetails[$commonIndex]['packages'][$index]['height'] = $request->shipout['height'][$index];
                    $invoiceDetails[$commonIndex]['packages'][$index]['weight'] = $request->shipout['weight'][$index];

                    $shipmentTracking = new \App\Model\Shipmenttrackingnumber;
                    $shipmentTracking->trackingId = $shipmentTrackingid[0];
                    $shipmentTracking->trackingNumber = $request->shipout['tracking'][$index];
                    $shipmentTracking->createdBy = Auth::user()->id;
                    $shipmentTracking->createdOn = Config::get('constants.CURRENTDATE');
                    $shipmentTracking->save();
                    
                    $userInfo = \App\Model\User::where('unit',$request->shipout['unit'][$index])->first();
                    $isNotificationExist = \App\Model\Quickshipoutnotification::where("quickshipoutId",$request->quickShipoutId)->where("userId",$userInfo->id)->where("trackingNumber",$request->shipout['tracking'][$index])->get()->toArray();

                    if (empty($isNotificationExist)) {
                        
                        if(!empty($shipmentTypeArr[$request->shipout['shipmentType'][$index]]))
                            $shipmentType = $shipmentTypeArr[$request->shipout['shipmentType'][$index]];
                        else
                            $shipmentType = "packed";

                        $quickshipoutNotification = new \App\Model\Quickshipoutnotification;
                        $quickshipoutNotification->quickshipoutId = $request->quickShipoutId;
                        $quickshipoutNotification->userId = $userInfo->id;
                        $quickshipoutNotification->unitNumber = $request->shipout['unit'][$index]; 
                        $quickshipoutNotification->trackingNumber = $request->shipout['tracking'][$index];
                        $quickshipoutNotification->shipmentType = $shipmentType;
                        $quickshipoutNotification->storeName = $store;
                        $quickshipoutNotification->itemQty = $request->shipout['quantity'][$index];
                        $quickshipoutNotification->productName = (isset($request->shipout['item'][$index]) ? $request->shipout['item'][$index] : "");
                        $quickshipoutNotification->emailSent = "0";
                        $quickshipoutNotification->smsSent = "0";

                        $quickshipoutNotification->save();
                    }
                }
            }

            $commercialInvoice = \App\Model\Commercialinvoice::find($request->quickShipoutId);

            $commercialInvoice->invoiceName = $request->shipoutName;
            $shippersInfo = "";
            $consigneeInfo = "";
            $commercialInvoice->shippersInfoid = $request->shipperInfo;
            $commercialInvoice->consigneeInfoid = $request->consigneeInfo;
            if(!empty($request->shipperInfo))
            {
                $shippersInfo = \App\Model\Quickshipoutinfo::find($request->shipperInfo);
            }
            $commercialInvoice->shipperInfo = nl2br($shippersInfo->addressDetails);
            if(!empty($request->consigneeInfo))
            {
                $consigneeInfo = \App\Model\Quickshipoutinfo::find($request->consigneeInfo);
            }
            $commercialInvoice->consigneeInfo = nl2br($consigneeInfo->addressDetails);
            $commercialInvoice->originCountry = $originCountry;
            $commercialInvoice->destinationCountry = $destinationCountry;
            $commercialInvoice->destinationCity = $destinationCity;
            $commercialInvoice->originCountryId = $request->originCountry;
            $commercialInvoice->destinationCountryId = $request->destinationCountry;
            $commercialInvoice->destinationCityId = $request->destinationCity;
            $commercialInvoice->basicInfoAdded = "1";
            $commercialInvoice->deleted = "0";
            $commercialInvoice->piecesCount = $request->piecesCount;
            $commercialInvoice->airBillNumber = $request->airBillNumber;
            $commercialInvoice->departureDate = $request->departureDate;
            $commercialInvoice->quicknotes = $request->quicknotes;
            $commercialInvoice->invoiceDetails = json_encode($invoiceDetails);

            if ($request->submit == 'finish_lock') {
                $commercialInvoice->completedOn = Config::get('constants.CURRENTDATE');
                $commercialInvoice->shipoutLocked = "1";
            }

            $dataSave = $commercialInvoice->save();

            $data['invoiceData'] = $commercialInvoice;
            $data['createdBy'] = Auth::user()->firstName . ' ' . Auth::user()->lastName;

            $invoiceUniqueId = 'COMINV' . $commercialInvoice->id . date('Ymd');
            $invFileName = "INV_" . $invoiceUniqueId . ".pdf";
            PDF::loadView('Administrator.consolidatedtracking.pdfcreatecommercialinvoice', $data)->save(public_path('exports/invoice/' . $invFileName))->stream('download.pdf');

            $invoiceUniqueId = 'SHIPMAN' . $commercialInvoice->id . date('Ymd');
            $manFileName = "INV_" . $invoiceUniqueId . ".pdf";
            PDF::loadView('Administrator.consolidatedtracking.pdfcreateshipmentmanifest', $data)->save(public_path('exports/invoice/' . $manFileName))->stream('download.pdf');

            $invoiceUniqueId = 'INTERNAL' . $commercialInvoice->id . date('Ymd');
            $internalFileName = "INV_" . $invoiceUniqueId . ".pdf";
            PDF::loadView('Administrator.consolidatedtracking.pdfinternalmanifest', $data)->save(public_path('exports/invoice/' . $internalFileName))->stream('download.pdf');

            $commercialInvoice->invoiceFile = $invFileName;
            $commercialInvoice->manifestFile = $manFileName;
            $commercialInvoice->internalFile = $internalFileName;
            $commercialInvoice->save();


            // Insert 1 row in quickshipout status log 

            //Find record exist in statusLog
            
            $findStatus = \App\Model\Quickshipoutstatuslog::where('commercialInvoiceId', $request->quickShipoutId)->where('status', 'in_warehouse')->where('oldStatus', 'none')->first();

            if(count($findStatus)==0)
            {
                $statusLog = new \App\Model\Quickshipoutstatuslog;
                $statusLog->commercialInvoiceId = $request->quickShipoutId;
                $statusLog->oldStatus = 'none';
                $statusLog->status = 'in_warehouse';
                $statusLog->updatedOn = Config::get('constants.CURRENTDATE');
                $statusLog->save();

            }

            // Insert 1 row in quickshipout status log 

            //print_r($request->submit); die;
            if($dataSave)
            {
                if($request->submit == "finish" || $request->submit == "finish_lock")
                {

                    // return \Redirect::to('administrator/consolidated/quick-shipout')->with('successMessage', 'Quick Shipout created successfully.');
                     $redirectPath = url('/') . "/administrator/consolidated/quick-shipout";
                    return response()->json(['path' => $redirectPath]);
                }           
                else{
                    return "1";
                }
            }
            else
            {
                return "0";
            }
        } else{
            return \Redirect::to('administrator/consolidated/quick-shipout')->with('successMessage', 'Quick Shipout created successfully.');
        }
    }

    public function savecommercialinvoice(Request $request) {
        
        if(\Session::has('invoceDetailsData'))
        {
            $validator = Validator::make($request->all(), [
                        'invoiceName' => 'required',
                        'shipperInfo' => 'required',
                        'consigneeInfo' => 'required',
            ]);
            if ($validator->fails()) {
                echo "0";exit;
            }
            
            $ifNameExist = \App\Model\Commercialinvoice::where('invoiceName',$request->invoiceName)->where('deleted','0')->count();
            if($ifNameExist>0)
            {
                echo "-1";exit;
            }
            
            $invoiceData = \Session::get('invoceDetailsData');
            $commercialInvoice = new \App\Model\Commercialinvoice;
            $commercialInvoice->invoiceName = $request->invoiceName;
            $commercialInvoice->shipperInfo = nl2br(htmlentities($request->shipperInfo));
            $commercialInvoice->consigneeInfo = nl2br(htmlentities($request->consigneeInfo));
            $commercialInvoice->invoiceDetails = $invoiceData[0];
            $commercialInvoice->createdBy = Auth::user()->id;
            $commercialInvoice->createdOn = Config::get('constants.CURRENTDATE'); 
            $commercialInvoice->save();
            
            $data['invoiceData'] = $commercialInvoice;
            $invoiceUniqueId = 'COMINV' . $commercialInvoice->id . date('Ymd');
            $invFileName = "INV_" . $invoiceUniqueId . ".pdf";
            PDF::loadView('Administrator.consolidatedtracking.pdfcreatecommercialinvoice', $data)->save(public_path('exports/invoice/' . $invFileName))->stream('download.pdf');
            
            $invoiceUniqueId = 'SHIPMAN' . $commercialInvoice->id . date('Ymd');
            $manFileName = "INV_" . $invoiceUniqueId . ".pdf";
            PDF::loadView('Administrator.consolidatedtracking.pdfcreateshipmentmanifest', $data)->save(public_path('exports/invoice/' . $manFileName))->stream('download.pdf');
            
            $commercialInvoice->invoiceFile = $invFileName;
            $commercialInvoice->manifestFile = $manFileName;
            $commercialInvoice->save();

            \Session::forget('invoceDetailsData');
            $invoiceData = \Session::get('invoceDetailsData');

            if(!\Session::has('invoceDetailsData'))
            {
                echo "1";exit;
            }
            
        }
        else
        {
            echo "0";exit;
        }
    }
    
    public function saveshipmentmanifest(Request $request) {
        
        if(\Session::has('manifestDetailsData'))
        {
            $invoiceData = \Session::get('manifestDetailsData');
            $shipmentManifest = new \App\Model\Shipmentmanifest;
            $shipmentManifest->invoiceName = $request->invoiceName;
            $shipmentManifest->shipperInfo = nl2br(htmlentities($request->shipperInfo));
            $shipmentManifest->consigneeInfo = nl2br(htmlentities($request->consigneeInfo));
            $shipmentManifest->invoiceDetails = $invoiceData[0];
            $shipmentManifest->createdBy = Auth::user()->id;
            $shipmentManifest->createdOn = Config::get('constants.CURRENTDATE'); 
            $shipmentManifest->save();
            $data['invoiceData'] = $shipmentManifest;
            $invoiceUniqueId = 'SHIPMAN' . $shipmentManifest->id . date('Ymd');
            $fileName = "INV_" . $invoiceUniqueId . ".pdf";
            $shipmentManifest->invoiceFile = $fileName;
            $shipmentManifest->save();
            PDF::loadView('Administrator.consolidatedtracking.pdfcreateshipmentmanifest', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
            \Session::forget('invoceDetailsData');
            $invoiceData = \Session::get('manifestDetailsData');
            if(!\Session::has('manifestDetailsData'))
            {
                echo "1";exit;
            }
            
        }
        else
        {
            echo "0";exit;
        }
    }
    
    public function printcommercialinvoice($invoiceId,$page=0) {
        
        $invoiceData = \App\Model\Commercialinvoice::find($invoiceId);
        $data = array();
        $data['invoiceData'] = $invoiceData;
        return view('Administrator.consolidatedtracking.printcreatecommercialinvoice', $data);
    }
    
    public function printshipmentmanifest($invoiceId,$page=0) {
        
        $invoiceData = \App\Model\Shipmentmanifest::find($invoiceId);
        $data = array();
        $data['invoiceData'] = $invoiceData;
        return view('Administrator.consolidatedtracking.printcreateshipmentmanifest', $data);
    }
    public function printFile($type,$invoiceId) {
        
        $invoiceData = \App\Model\Commercialinvoice::find($invoiceId);
        $data = array();
        $data['invoiceData'] = $invoiceData;
        $data['createdBy'] = \App\Model\UserAdmin::find($invoiceData->createdBy);
        if($type == 'invoice')
        {
            return view('Administrator.consolidatedtracking.printcreatecommercialinvoice', $data);
        }
        else if($type == 'internal')
        {
            return view('Administrator.consolidatedtracking.printinternalmanifest', $data);
        }
        else
        {
            return view('Administrator.consolidatedtracking.printcreateshipmentmanifest', $data);
        }
    }

    public function filetypeSelection($type,$invoiceId) {
        $data = array();
        $data['invoiceId'] = $invoiceId;
        $data['type'] = $type;
        return view('Administrator.consolidatedtracking.filetypeselection', $data);
    }
    
    public function downloadFile($invoiceId, Request $request) {
        
        $invoiceData = \App\Model\Commercialinvoice::find($invoiceId);
        if($request->downloadType == 'invoice')
        {
            $filepath = public_path('exports/invoice/'.$invoiceData->invoiceFile);
        }
        else if($request->downloadType == 'internal')
        {
            $filepath = public_path('exports/invoice/'.$invoiceData->internalFile);
        }
        else
        {
            $filepath = public_path('exports/invoice/'.$invoiceData->manifestFile);
        }
        return Response::download($filepath);
    }

    public function downloadcommercialinvoice($invoiceId) {
        
        $invoiceData = \App\Model\Commercialinvoice::find($invoiceId);
        $filepath = public_path('exports/invoice/'.$invoiceData->invoiceFile);
        return Response::download($filepath);
    }
    
    public function downloadshipmentmanifest($invoiceId) {
        
        $invoiceData = \App\Model\Shipmentmanifest::find($invoiceId);
        $filepath = public_path('exports/invoice/'.$invoiceData->invoiceFile);
        return Response::download($filepath);
    }
    
    public function deletecommercialinvoice($invoiceId,$page = 1) {
        
        $invoiceData = \App\Model\Commercialinvoice::find($invoiceId);
        $invoiceData->deleted = "1";
        $invoiceData->deletedBy = Auth::user()->id;
        $invoiceData->deletedOn = Config::get('constants.CURRENTDATE');
        $invoiceData->save();
        
        return \Redirect::to('administrator/consolidated/quick-shipout')->with('successMessage', 'Quickshipout deleted successfully.');
    }
    
    public function deleteshipmentmanifest($invoiceId,$page = 1) {
        
        $invoiceData = \App\Model\Shipmentmanifest::find($invoiceId);
        $invoiceData->deleted = "1";
        $invoiceData->deletedBy = Auth::user()->id;
        $invoiceData->deletedOn = Config::get('constants.CURRENTDATE');
        $invoiceData->save();
        
        return \Redirect::to('administrator/consolidated/shipment-manifest/?page='.$page)->with('successMessage', 'Invoice deleted successfully.');
    }
    
    public function getusersettings($unit,$page = 0) {
        
        $data = array();
        $userSettingsInfo = array();
        $userInfo = \App\Model\User::where('unit',$unit)->first();
        $name = "";
        $address = "";
        if(empty($userInfo))
            $userId = "-1";
        else {
            $userId = $userInfo->id;
            $name = $userInfo->firstName." ".$userInfo->lastName;
            $userShippingAddress = \App\Model\Addressbook::where("userId",$userId)->where("isDefaultShipping","1")->with('country','city')->first();
            if(!empty($userShippingAddress))
            {
                $address = $userShippingAddress->city->name.", ".$userShippingAddress->country->name;
            }
        }
            
        
        $userSettingsData = \App\Model\Usershipmentsettings::where('userId', $userId)->get();
        if (count($userSettingsData) > 0) {
            foreach ($userSettingsData as $eachData) {
                $userSettingsInfo[$eachData->fieldType] = $eachData->fieldValue;
            }
        }
        $userSettingsHistory = \App\Model\Usershipmentsettingshistory::where('modifiedBy', $userId)->get();
        $userSettingsFields = array(
            'remove_shoe_box' => 'Always remove shoe boxes. (Select Yes to save money)',
            'original_box' => 'Ship all my items in their original boxes (Select No to save money)',
            'quick_shipout' => 'Quick ship my items. (For subscribed customers shipping via DHL Priority)',
//            'remove_shoe_box' => 'Always remove shoe boxes (If Yes, we will ship all shoes without the boxes – This saves you money)',
//            'original_box' => 'Ship all non electronics in the original box (If Yes, we will ship such items in the box they came in)',
//            'original_box_all' => 'Ship all electronics in the original box (If Yes, we will ship such items in the box they came in)',
//            'take_photo' => 'Take pictures of my items before shipping (If Yes, items are inventoried in the US/UK and a $9.99 charge applies)',
//            'scan_invoice' => 'Scan all my invoices/receipts (If Yes, a $4.99 charge per delivery invoice applies)',
//            'item_inventory' => 'Inventory my items in the US/UK (If Yes, a $7.99 charge per delivery is added)'
        );
        $data['userId'] = $userId;
        $data['name'] = $name;
        $data['userAddress'] = $address;
        $data['userSettingsInfo'] = $userSettingsInfo;
        $data['userSettingsHistory'] = $userSettingsHistory;
        $data['userSettingsFields'] = $userSettingsFields;
        $data['userFillnshipInfo'] = array();
        $userWarehousedata = \App\Model\Warehouse::warehouseData();
                foreach($userWarehousedata as $userwarehouse)
                {
                    $warehouseData[$userwarehouse['id']] = $userwarehouse['name']."(".$userwarehouse['cityName'].")";
                }
        $data['userWarehouse'] = $warehouseData;

        $userFillnship = \App\Model\Fillship::select('id', 'warehouseId')->where('userId', $userId)->where('shippOutOpt', '0')->where('deleted', '0')->get();
        if(!empty($userFillnship))
        {
            $fillnShipRecord =array();
            foreach($userFillnship as $fillnshipData)
            {
                $fillnShipRecord[$fillnshipData->warehouseId][] = $fillnshipData->id;
            }

            $data['userFillnshipInfo'] = $fillnShipRecord;

        }
        

       // print_r($data['userFillnshipInfo'] ); die;
        return view('Administrator.users.usershipmentsettings', $data);
    }
    
    public function notifycustomer(Request $request) {
        
        $userInfo = \App\Model\User::where('unit',$request->unit)->first();
        $emailSent = $smsSent = "0";
        
        if(!empty($userInfo)) {
            
            $replace['[NAME]'] = $userInfo->firstName . " " . $userInfo->lastName;
            $replace['[STORE_NAME]'] = $request->store;
            $replace['[TRACKING_NUMBER]'] = $request->tracking;
            $replace['[ITEM_QTY]'] = $request->itemQty;
            $replace['[PRODUCT_NAME]'] = ($request->addedItem!="" ? $request->addedItem :(($request->productName !='Not Listed') ? $request->productName : ($request->subcategoryName !='Not Listed' ? $request->subcategoryName : "Products")));
            
            $shipmentType = strtolower($request->shipmentType);
            if($shipmentType=='packed shipments')
            {
                $shipmentType = "packed";
            }
            if($shipmentType=='fiil and ship')
            {
                $shipmentType = "fillship";
            }
            $templateKey = $shipmentType."_quickshipout_customer_notification";
            $emailTemplate = \App\Model\Emailtemplate::where('templateKey',$templateKey)->first();
            $to = $userInfo->email;
            $sendMail = customhelper::SendMail($emailTemplate, $replace, $to);
            if($sendMail)
                $emailSent = "1";                
            
            $smsTemplate = \App\Model\Smstemplate::where('templateKey',$templateKey)->first(); 
            $toMobile = trim($userInfo->isdCode.$userInfo->contactNumber);
            $isSendMsg = customhelper::sendMSG($toMobile,$replace,$smsTemplate);
            if($isSendMsg)
                $smsSent = "1";
            
            $replace['[QUICKSHIPOUT_TYPE]'] = $request->shipmentType;
            \App\Model\User::sendPushNotification($userInfo->id, 'quickshipout_notification', Auth::user()->id, $replace);
            
//            if($request->dataNotification!="0")
//                $quickShipoutLog = \App\Model\Quickshipoutlog::find($request->dataNotification);
//            else
//                $quickShipoutLog = new \App\Model\Quickshipoutlog;
//            $quickShipoutLog->quickshipoutId = $request->quickshipoutId;
//            $quickShipoutLog->emailSent = $emailSent;
//            $quickShipoutLog->smsSent = $smsSent;
//            $quickShipoutLog->unitNumber = $request->unit;
//            $quickShipoutLog->trackingNumber = $request->tracking;
//            $quickShipoutLog->product = $request->productName;
//            $quickShipoutLog->save();
            
            $isNotificationExist = \App\Model\Quickshipoutnotification::where("quickshipoutId",$request->quickshipoutId)->where("userId",$userInfo->id)->where("trackingNumber",$request->tracking)->first();
            
            if(!empty($isNotificationExist))
                $quickshipoutNotification = $isNotificationExist;
            else
                $quickshipoutNotification = new \App\Model\Quickshipoutnotification;
            
            $quickshipoutNotification->quickshipoutId = $request->quickshipoutId;
            $quickshipoutNotification->userId = $userInfo->id;
            $quickshipoutNotification->unitNumber = $request->unit;
            $quickshipoutNotification->trackingNumber = $request->tracking;
            $quickshipoutNotification->shipmentType = $shipmentType;
            $quickshipoutNotification->storeName = $request->store;
            $quickshipoutNotification->itemQty = $request->itemQty;
            $quickshipoutNotification->productName = ($request->addedItem!="" ? $request->addedItem :(($request->productName !='Not Listed') ? $request->productName : ($request->subcategoryName !='Not Listed' ? $request->subcategoryName : "Products")));

            if($emailSent == "1")
            {
                $quickshipoutNotification->emailSent = "2";
                $quickshipoutNotification->emailSentOn = date("Y-m-d H:i:s");
            }
            if($smsSent == "1")
            {
                $quickshipoutNotification->smsSent = "2";
                $quickshipoutNotification->smsSentOn = date("Y-m-d H:i:s");
            }
            $quickshipoutNotification->save();
            
            if($emailSent)
            {
                return 1;
            }
            else
            {
                return 2;
            }
        }
        else
            return 3;
        
    }
    
    public function generatequickshipoutfiles($shipoutId = 0) {
        
        if($shipoutId != 0)
        {
            $commercialInvoice = \App\Model\Commercialinvoice::find($shipoutId);
            $data['invoiceData'] = $commercialInvoice;
            $adminInfo = \App\Model\UserAdmin::find($commercialInvoice->createdBy);
            $data['createdBy'] = $adminInfo->firstName.' '.$adminInfo->lastName;

            $invoiceUniqueId = 'COMINV' . $commercialInvoice->id . date('Ymd');
            $invFileName = "INV_" . $invoiceUniqueId . ".pdf";
            PDF::loadView('Administrator.consolidatedtracking.pdfcreatecommercialinvoice', $data)->save(public_path('exports/invoice/' . $invFileName))->stream('download.pdf');

            $invoiceUniqueId = 'SHIPMAN' . $commercialInvoice->id . date('Ymd');
            $manFileName = "INV_" . $invoiceUniqueId . ".pdf";
            PDF::loadView('Administrator.consolidatedtracking.pdfcreateshipmentmanifest', $data)->save(public_path('exports/invoice/' . $manFileName))->stream('download.pdf');

            $invoiceUniqueId = 'INTERNAL' . $commercialInvoice->id . date('Ymd');
            $internalFileName = "INV_" . $invoiceUniqueId . ".pdf";
            PDF::loadView('Administrator.consolidatedtracking.pdfinternalmanifest', $data)->save(public_path('exports/invoice/' . $internalFileName))->stream('download.pdf');

            $commercialInvoice->invoiceFile = $invFileName;
            $commercialInvoice->manifestFile = $manFileName;
            $commercialInvoice->internalFile = $internalFileName;
            $commercialInvoice->save();
        }
        
    }
    
    public function quickshipoutinfo() {
        
        \Session::forget('QUICKSHIPOUTINFODATA');
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Quickshipoutinfo'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $searchTrackingArr = array(
            'heading' => '',
            'createdOn' => '',
        );
        if (\Request::isMethod('post')) {

            /* GET POST VALUE  */ 
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $searchTracking = \Input::get('searchTracking', $searchTrackingArr);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('QUICKSHIPOUTINFODATA');
            \Session::push('QUICKSHIPOUTINFODATA.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('QUICKSHIPOUTINFODATA.searchDisplay', $searchDisplay);
            \Session::push('QUICKSHIPOUTINFODATA.searchTracking', $searchTracking);
            \Session::push('QUICKSHIPOUTINFODATA.field', $field);
            \Session::push('QUICKSHIPOUTINFODATA.type', $type);
            

            $param['field'] = $field;
            $param['type'] = $type;            
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchDisplay'] = $searchDisplay;
            $param['searchTracking'] = $searchTracking;
        } else {
            $sortField = \Session::get('QUICKSHIPOUTINFODATA.field');
            $sortType = \Session::get('QUICKSHIPOUTINFODATA.type');
            $searchDisplay = \Session::get('QUICKSHIPOUTINFODATA.searchDisplay');
            $searchTracking = \Session::get('QUICKSHIPOUTINFODATA.searchTracking');            
            $searchByCreatedOn = \Session::get('QUICKSHIPOUTINFODATA.searchByCreatedOn');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;            
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchTracking'] = !empty($searchTracking) ? $searchTracking[0] : $searchTrackingArr;
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
        }


        /* BUILD SORTING ARRAY */
        $sort = array(
            'heading' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting')
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH TRACKING LIST  */
        $infoData = \App\Model\Quickshipoutinfo::getData($param);

        /* SET DATA FgeDetails(OR VIEW  */
        $data['title'] = "Administrative Panel :: Quick Ship Out Info";
        $data['contentTop'] = array('breadcrumbText' => 'Quick Ship Out Info', 'contentTitle' => 'Quick Ship Out Info', 'pageInfo' => 'This section allows you to manage quick ship out info');
        $data['pageTitle'] = "Quick Ship Out Info";
        $data['page'] = $infoData->currentPage();
        $data['infoData'] = $infoData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        
        return view('Administrator.consolidatedtracking.quickshipoutinfo', $data);
    }
    
    public function quickshipoutinfoaddedit($id = 0, $page = 1, Request $request) {
        
        $quickShipoutinfo = array();
        $data = array();
        $modalTitle = 'Add New Quick Ship Out Info';
        
        if($id!=0)
        {
            $quickShipoutinfo = \App\Model\Quickshipoutinfo::find($id);
            $modalTitle = 'Edit Quick Ship Out Info';
        }
        
        if (\Request::isMethod('post')) {
            
            if($id == 0)
            {
                $quickShipoutinfo = new \App\Model\Quickshipoutinfo;
                $quickShipoutinfo->createdBy = Auth::user()->email;
                $quickShipoutinfo->createdOn = Config::get('constants.CURRENTDATE');
                
            }
            
            $quickShipoutinfo->heading = $request->heading;
            $quickShipoutinfo->addressDetails = $request->addressDetails;
            $quickShipoutinfo->save();
            
            return \Redirect::to('administrator/consolidated/quick-shipout-info/?page='.$page)->with('successMessage', 'Quickshipout Info Saved Successfully.');
        }
        
        $data['quickShipoutinfo'] = $quickShipoutinfo;
        $data['id'] = $id;
        $data['page'] = $page;
        $data['modalTitle'] = $modalTitle;
        
        return view('Administrator.consolidatedtracking.quickshipoutinfoaddedit', $data);
    }
    
    public function updatequickshipoutstatus($id = 0, $page = 1, $status = 0) {
        
        if($id != 0) {
            \App\Model\Quickshipoutinfo::where("id",$id)->update(["status"=>$status]);
            
        }
        
        return \Redirect::to('administrator/consolidated/quick-shipout-info/?page='.$page)->with('successMessage', 'Status Updated Successfully.');
    }
    
    public function deletequickshipout($id = 0, $page = 1) {
        
        if($id != 0) {
            \App\Model\Quickshipoutinfo::where("id",$id)->update(["deleted"=>"1","deletedBy"=>Auth::user()->email,"deletedOn"=>Config::get('constants.CURRENTDATE')]); 
        }
        
        return \Redirect::to('administrator/consolidated/quick-shipout-info/?page='.$page)->with('successMessage', 'Record Deleted Successfully.');
    }


    public function showdeliverystatus($id, $page)
    {
        $data = array();
        $modalTitle = 'Set delivery stages';

        $data['statusLog'] = \App\Model\Quickshipoutstatuslog::getStatusLog($id);
        $data['id'] = $id;
        $data['page'] = $page;
        $data['modalTitle'] = $modalTitle;
        
        return view('Administrator.consolidatedtracking.quickshipoutgetstatus', $data);
    }
   
    public function savestatus($id, $page, Request $request)
    {
        if (!empty($request->status)) {

            $statusPost = $request->status;

            ////////////Get User Info from Comercial Invoice Json data ///////////////////

            $comercialInvoicedetails = \App\Model\Commercialinvoice::find($id);           

            $invoicedetails = json_decode($comercialInvoicedetails->invoiceDetails);

            //print_r($invoicedetails); die;

            $invoicePackage = $invoicedetails[0]->packages;

            foreach($invoicePackage as $package)
            {
                $userInfo[] = \App\Model\User::where('unit',$package->unit)->first();

            }

            ////////////Get User Info from Comercial Invoice Json data ///////////////////


            foreach ($statusPost as $eachStatus => $dates) {
                if ($dates != '') {
                    $shipmentStatusData = \App\Model\Quickshipoutstatuslog::where("commercialInvoiceId", $id)->orderBy("id", "desc")->first();
                    if(count($shipmentStatusData) >0)
                    { 
                      
                         if($shipmentStatusData->status!=$eachStatus)
                            {
                                $shipmentStatus = new \App\Model\Quickshipoutstatuslog;
                                $shipmentStatus->commercialInvoiceId = $id;
                                $shipmentStatus->oldStatus = (!empty($shipmentStatusData->status) ? $shipmentStatusData->status : 'none');
                                $shipmentStatus->status = $eachStatus;
                                $shipmentStatus->updatedOn = date('Y-m-d h:i:s', strtotime($dates));
                                $shipmentStatus->save();

                            }

                    }else{
                        $shipmentStatus = new \App\Model\Quickshipoutstatuslog;
                                $shipmentStatus->commercialInvoiceId = $id;
                                $shipmentStatus->oldStatus = 'none';
                                $shipmentStatus->status = $eachStatus;
                                $shipmentStatus->updatedOn = date('Y-m-d h:i:s', strtotime($dates));
                                $shipmentStatus->save();
                    }

                    foreach($userInfo as $user)
                    {
                       //DB::enableQueryLog();
                        $isNotificationExist = \App\Model\Quickshipoutstatusnotification::where("quickshipoutId",$id)->where("useremail", $user['email'])->get()->toArray();
                       // dd(DB::getQueryLog());

                        //print_r($isNotificationExist); 

                        if (!empty($isNotificationExist)) {
                            //$quickshipoutstatusnotification = $isNotificationExist;
                            
                        }
                        else{
                            //echo $user['contactNumber'];
                            $quickshipoutstatusnotification = new \App\Model\Quickshipoutstatusnotification;
                            $quickshipoutstatusnotification->quickshipoutId = $id;
                            $quickshipoutstatusnotification->useremail = $user['email'];
                            $quickshipoutstatusnotification->usercontact = $user['contactNumber']; 
                            $quickshipoutstatusnotification->templateKey = "quickshipoutair_status_change";
                            $quickshipoutstatusnotification->quickshipoutType = "0";
                            $quickshipoutstatusnotification->emailSentStatus = "0";
                            $quickshipoutstatusnotification->smsSentStatus = "0";
                            $quickshipoutstatusnotification->save();

                        }
                   
                    }
           
                    //die;

            return 1;
        }else{
            return 0;
        }

        }

    }

}
    
    public function quickshipoutshowall() {
        
        \Session::forget('CONSOLIDATEDDATA');
        return \Redirect::to('administrator/consolidated/quick-shipout');
    }

/////////////////////////////////////////////////////Quick Shipout -- OCEAN//////////////////////////////////////////////

    public function quickshipoutocean(Request $request) {
        
        \Session::forget('QUICKSHIPOUTDATA');
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Quickshipoutocean'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $searchTrackingArr = array(
            'trackingName' => '',
            'trackingNumber' => '',
        );
         
        if (\Request::isMethod('post')) {

            /* GET POST VALUE  */ 
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $searchTracking = \Input::get('searchTracking', $searchTrackingArr);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('CONSOLIDATEDDATA');
            \Session::push('CONSOLIDATEDDATA.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('CONSOLIDATEDDATA.searchDisplay', $searchDisplay);
            \Session::push('CONSOLIDATEDDATA.searchTracking', $searchTracking);
            \Session::push('CONSOLIDATEDDATA.field', $field);
            \Session::push('CONSOLIDATEDDATA.type', $type);
            

            $param['field'] = $field;
            $param['type'] = $type;            
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchDisplay'] = $searchDisplay;
            $param['searchTracking'] = $searchTracking;
        } else {
            $sortField = \Session::get('CONSOLIDATEDDATA.field');
            $sortType = \Session::get('CONSOLIDATEDDATA.type');
            $searchDisplay = \Session::get('CONSOLIDATEDDATA.searchDisplay');
            $searchTracking = \Session::get('CONSOLIDATEDDATA.searchTracking');            
            $searchByCreatedOn = \Session::get('CONSOLIDATEDDATA.searchByCreatedOn');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;            
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchTracking'] = !empty($searchTracking) ? $searchTracking[0] : $searchTrackingArr;
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
        }


        /* BUILD SORTING ARRAY */
        $sort = array(
            'invoiceName' => array('current' => 'sorting'),
            'email' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting')
        );

        
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        $trackingData = \App\Model\Quickshipoutocean::getData($param);
        

        
        $data['title'] = "Administrative Panel :: Quick Ship Out - Ocean";
        $data['contentTop'] = array('breadcrumbText' => 'Quick Ship Out - Ocean', 'contentTitle' => 'Quick Ship Out', 'pageInfo' => 'This section allows you to manage quick ship out');
        $data['pageTitle'] = "Quick Ship Out - Ocean";
        $data['page'] = $trackingData->currentPage();
        $data['trackingData'] = $trackingData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        
        return view('Administrator.consolidatedtracking.quickshipoutocean', $data);

    }

    public function createquickshipoutocean(Request $request) {
        $quickshipout = new \App\Model\Quickshipoutocean;
        $quickshipout->deleted = "1";
        $quickshipout->createdBy = Auth::user()->id;
        $quickshipout->createdOn = Config::get('constants.CURRENTDATE');
        $quickshipout->save();
        $qucikShipoutId = $quickshipout->id;
        
        return \Redirect::to('administrator/consolidated/quick-shipout-ocean/edit/'.$qucikShipoutId);
    }
    

    public function editquickshipoutocean($qucikShipoutId = 0) {  

        $data = array();
        $quickShipoutDetails = \App\Model\Quickshipoutocean::find($qucikShipoutId);
        $data['shipmentType'] = \App\Model\Quickshipoutocean::shipmenttype();
        $data['qucikShipoutId'] = $qucikShipoutId;
        $data['shipoutDetails'] = $quickShipoutDetails;
        $data['notificationLog'] = \App\Model\Quickshipoutnotification::getNotificationHistory($qucikShipoutId);

        //print_r($data['notificationLog']);die;
        $data['quickshipoutInfo'] = \App\Model\Quickshipoutinfo::where("status","1")->where("deleted","0")->get();
        
        $cityArr = array();
        if(!empty($data['shipoutDetails']['destinationCountryId']))
        {
            $countryData = \App\Model\Country::find($data['shipoutDetails']['destinationCountryId']);
            $cityArr = \App\Model\City::where('countryCode',$countryData->code)->where('status','1')->where('deleted','0')->orderby('name', 'asc')->get();
        }
        
        $data['countryList'] = \App\Model\Country::where('status', '1')->orderby('name', 'asc')->get();
        $data['destinationCityList'] = $cityArr;

        //$invoiceDetails = json_decode($data['shipoutDetails']->invoiceDetails);

        //print_r($invoiceDetails); die;
        
        $data['title'] = "Administrative Panel :: Quick Ship Out - Ocean";
        $data['contentTop'] = array('breadcrumbText' => 'Quick Ship Out - Ocean', 'contentTitle' => 'Quick Ship Out', 'pageInfo' => 'This section allows you to manage quick ship outs');
        $data['pageTitle'] = "Quick Ship Out - Ocean";
        
        return view('Administrator.consolidatedtracking.createquickshipoutocean', $data);
    }

    public function savequickshipoutocean(Request $request) {
        $dataSave = 0;
        $invoiceDetails = $findStatus = array();
        $validator = Validator::make($request->all(), [
                    'shipoutName' => 'required',
                    'shipperInfo' => 'required',
                    'consigneeInfo' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
            

        $commonIndex = 0;
        $originCountry = \App\Model\Country::find($request->originCountry)->name;
        $destinationCountry = \App\Model\Country::find($request->destinationCountry)->name;
        $destinationCity = \App\Model\City::find($request->destinationCity)->name;
        
        $shipmentTypeArr = \App\Model\Quickshipoutocean::shipmenttype();
        
        $quickShipoutAddedId = $request->quickShipoutId;

       // if (!empty($shipmentTrackingid) && !empty($quickShipoutAddedId)) {

        $shipmentId = 0;

            for ($index = 0; $index < count($request->shipout['ordernumber']); $index++) {
                if (!empty($request->shipout['ordernumber'][$index])) {
                    //Check order number present or not

                    $findOrdernumber = \App\Model\Order::where('orderNumber', $request->shipout['ordernumber'][$index])->get();
                    $userInfo = \App\Model\User::where('id',$findOrdernumber[0]['userId'])->first();

                    //print_r($findOrdernumber); die;
                    if(!empty($findOrdernumber))
                    {
                        $invoiceDetails[$commonIndex]['packages'][$index]['orderNumber'] = $request->shipout['ordernumber'][$index];
                        $invoiceDetails[$commonIndex]['packages'][$index]['unit'] =$userInfo->unit;
                        
                        $invoiceDetails[$commonIndex]['packages'][$index]['shipmentType'] = $request->shipout['shipmentType'][$index];                        
                        $invoiceDetails[$commonIndex]['packages'][$index]['itemName'] = (isset($request->shipout['item'][$index]) ? $request->shipout['item'][$index] : "");                        
                        $invoiceDetails[$commonIndex]['packages'][$index]['itemQuantity'] = $request->shipout['quantity'][$index];
                        $invoiceDetails[$commonIndex]['packages'][$index]['boxes'] = $request->shipout['boxes'][$index];
                        $invoiceDetails[$commonIndex]['packages'][$index]['totalbox'] = $request->shipout['totalbox'][$index];
                        $invoiceDetails[$commonIndex]['packages'][$index]['totalPrice'] = $request->shipout['total'][$index];

                        
                        $isNotificationExist = \App\Model\Quickshipoutnotification::where("quickshipoutId",$request->quickShipoutId)->where("userId",$userInfo->id)->get()->toArray();

                        $shipmentId = $findOrdernumber[0]['shipmentId'];

                        if (empty($isNotificationExist)) {
                           
                            $shipmentType = $shipmentTypeArr[$request->shipout['shipmentType'][$index]];

                            $shipmentType = strtolower(str_replace(" ", "", str_replace("–", " ", $shipmentType)));

                            $quickshipoutNotification = new \App\Model\Quickshipoutnotification;
                            $quickshipoutNotification->quickshipoutId = $request->quickShipoutId;
                            $quickshipoutNotification->userId = $userInfo->id;
                            $quickshipoutNotification->unitNumber = $userInfo->unit;
                            $quickshipoutNotification->shipmentType = $shipmentType;
                            $quickshipoutNotification->itemQty = $request->shipout['quantity'][$index];
                            $quickshipoutNotification->productName = (isset($request->shipout['item'][$index]) ? $request->shipout['item'][$index] : "");
                            $quickshipoutNotification->emailSent = "0";
                            $quickshipoutNotification->smsSent = "0";
                            $quickshipoutNotification->quickshipoutType = "1";

                            $quickshipoutNotification->save();
                        }

                    }//Check order number present or not
                    else{
                        break;
                        return "0";
                    }
                   
                }
            }

            $quickshipoutocean = \App\Model\Quickshipoutocean::find($request->quickShipoutId);

            $quickshipoutocean->invoiceName = $request->shipoutName;
            $shippersInfo = "";
            $consigneeInfo = "";
            $quickshipoutocean->shippersInfoid = $request->shipperInfo;
            $quickshipoutocean->consigneeInfoid = $request->consigneeInfo;
            if(!empty($request->shipperInfo))
            {
                $shippersInfo = \App\Model\Quickshipoutinfo::find($request->shipperInfo);
            }
            $quickshipoutocean->shipperInfo = nl2br($shippersInfo->addressDetails);
            if(!empty($request->consigneeInfo))
            {
                $consigneeInfo = \App\Model\Quickshipoutinfo::find($request->consigneeInfo);
            }
            $quickshipoutocean->consigneeInfo = nl2br($consigneeInfo->addressDetails);
            $quickshipoutocean->originCountry = $originCountry;
            $quickshipoutocean->destinationCountry = $destinationCountry;
            $quickshipoutocean->destinationCity = $destinationCity;
            $quickshipoutocean->originCountryId = $request->originCountry;
            $quickshipoutocean->destinationCountryId = $request->destinationCountry;
            $quickshipoutocean->destinationCityId = $request->destinationCity;
            $quickshipoutocean->basicInfoAdded = "1";
            $quickshipoutocean->deleted = "0";
            $quickshipoutocean->containerTrackingUrl = $request->containerTrackingUrl;
            $quickshipoutocean->containerNumber = $request->containerNumber;
            $quickshipoutocean->departureDate = $request->departureDate;
            $quickshipoutocean->invoiceDetails = json_encode($invoiceDetails);
            $quickshipoutocean->quicknotes = $request->quicknotes;

            if ($request->submit == 'finish_lock') {
                $quickshipoutocean->completedOn = Config::get('constants.CURRENTDATE');
                $quickshipoutocean->shipoutLocked = "1";
            }

            $dataSave = $quickshipoutocean->save();

            $data['invoiceData'] = $quickshipoutocean;
            $data['createdBy'] = Auth::user()->firstName . ' ' . Auth::user()->lastName;

            $invoiceUniqueId = 'QUICKOCEAN' . $quickshipoutocean->id . date('Ymd');

            $invFileName = "INV_" . $invoiceUniqueId . ".pdf";

            PDF::loadView('Administrator.consolidatedtracking.pdfoceaninvoice', $data)->save(public_path('exports/invoice/' . $invFileName))->stream('download.pdf');

            $quickshipoutocean->invoiceFile = $invFileName;
            $quickshipoutocean->save();

           

            // Insert 1 row in quickshipout status log 

            //Find record exist in statusLog
            
            $findStatus = \App\Model\Quickshipoutoceanstatuslog::where('quickshipoutId', $request->quickShipoutId)->where('status', 'in_warehouse')->where('oldStatus', 'none')->first();

            if(count($findStatus)==0)
            {
                $statusLog = new \App\Model\Quickshipoutoceanstatuslog;
                $statusLog->quickshipoutId = $request->quickShipoutId;
                $statusLog->oldStatus = 'none';
                $statusLog->status = 'in_warehouse';
                $statusLog->updatedOn = Config::get('constants.CURRENTDATE');
                $statusLog->save();

            }

            // Insert 1 row in quickshipout status log 

            if($dataSave)
            {
                if($request->submit == "finish" || $request->submit == "finish_lock")
                {
                    $redirectPath = url('/') . "/administrator/consolidated/quick-shipout-ocean";
                    return response()->json(['path' => $redirectPath]);
                }           
                else{
                    return "1";
                }
            }
            else
            {
                return "0";
            }
        // } else{
        //     return \Redirect::to('administrator/consolidated/quick-shipout')->with('successMessage', 'Quick Shipout created successfully.');
        // }
    }

    public function notifycustomerocean(Request $request)
    {

         $findOrdernumber = \App\Model\Order::where('orderNumber', $request->orderNumber)->get();

        if(!empty($findOrdernumber) && count($findOrdernumber)>0)
        {
           $userInfo = \App\Model\User::where('id',$findOrdernumber[0]['userId'])->first();
           $emailSent = $smsSent = "0";
        
            if(!empty($userInfo)) {
                
                $replace['[NAME]'] = $userInfo->firstName . " " . $userInfo->lastName;
                $replace['[ORDER_NUMBER]'] = $request->orderNumber;
                $replace['[ITEM_QTY]'] = $request->itemQty;
                $replace['[PRODUCT_NAME]'] = ($request->addedItem!="" ? $request->addedItem : "");
                
                $shipmentType = strtolower(str_replace(" ", "", str_replace("–", " ", $request->shipmentType)));
                
                $templateKey = $shipmentType."_quickshipoutocean_customer_notification";
                $emailTemplate = \App\Model\Emailtemplate::where('templateKey',$templateKey)->first();
                $to = $userInfo->email;
                $sendMail = customhelper::SendMail($emailTemplate, $replace, $to);
                if($sendMail)
                    $emailSent = "1";                
                
                $smsTemplate = \App\Model\Smstemplate::where('templateKey',$templateKey)->first(); 
                $toMobile = trim($userInfo->isdCode.$userInfo->contactNumber);
                $isSendMsg = customhelper::sendMSG($toMobile,$replace,$smsTemplate);
                if($isSendMsg)
                    $smsSent = "1";
                
                $isNotificationExist = \App\Model\Quickshipoutnotification::where("quickshipoutId", $request->quickshipoutId)->where("userId", $userInfo->id)->where("quickshipoutType", "1")->first();
                
                if(!empty($isNotificationExist))
                    $quickshipoutNotification = $isNotificationExist;
                else
                    $quickshipoutNotification = new \App\Model\Quickshipoutnotification;
                
                $quickshipoutNotification->quickshipoutId = $request->quickshipoutId;
                $quickshipoutNotification->userId = $userInfo->id;
                $quickshipoutNotification->unitNumber = $userInfo->unit;
                $quickshipoutNotification->shipmentType = $shipmentType;
                $quickshipoutNotification->itemQty = $request->itemQty;
                $quickshipoutNotification->productName = ($request->addedItem!="" ? $request->addedItem : "");
                $quickshipoutNotification->quickshipoutType = "1";

                if($emailSent == "1")
                {
                    $quickshipoutNotification->emailSent = "2";
                    $quickshipoutNotification->emailSentOn = date("Y-m-d H:i:s");
                }
                if($smsSent == "1")
                {
                    $quickshipoutNotification->smsSent = "2";
                    $quickshipoutNotification->smsSentOn = date("Y-m-d H:i:s");
                }
                $quickshipoutNotification->save();
                
                if($emailSent)
                {
                    return 1;
                }
                else
                {
                    return 2;
                }
            }
            else{
                return 3;
            }

        }else{
            return 4;
        }

       
    }

    public function downloadOceanInvoice($invoiceId) {
        
        $invoiceData = \App\Model\Quickshipoutocean::find($invoiceId);
        $filepath = public_path('exports/invoice/'.$invoiceData->invoiceFile);
        return Response::download($filepath);
    }

    public function printOceanInvoice($invoiceId) {
        
        $invoiceData = \App\Model\Quickshipoutocean::find($invoiceId);
        $data = array();
        $data['invoiceData'] = $invoiceData;
        $data['createdBy'] = \App\Model\UserAdmin::find($invoiceData->createdBy);
        
        return view('Administrator.consolidatedtracking.printoceaninvoice', $data);
        
    }

    public function deleteOceanInvoice($invoiceId,$page = 1) {
        
        $invoiceData = \App\Model\Quickshipoutocean::find($invoiceId);
        $invoiceData->deleted = "1";
        $invoiceData->deletedBy = Auth::user()->id;
        $invoiceData->deletedOn = Config::get('constants.CURRENTDATE');
        $invoiceData->save();
        
        return \Redirect::to('administrator/consolidated/quick-shipout-ocean')->with('successMessage', 'Quickshipout deleted successfully.');
    }


    public function showoceandeliverystatus($id, $page)
    {
        $data = array();
        $modalTitle = 'Set delivery stages';



        $data['statusLog'] = \App\Model\Quickshipoutoceanstatuslog::getStatusLog($id);
        $data['id'] = $id;
        $data['page'] = $page;
        $data['modalTitle'] = $modalTitle;
        
        return view('Administrator.consolidatedtracking.quickshipoutoceangetstatus', $data);
    }
   
    public function saveoceanstatus($id, $page, Request $request)
    {
        if (!empty($request->status)) {

            $statusPost = $request->status;

            $quickshipoutInvoicedetails = \App\Model\Quickshipoutocean::find($id);

            ////////////Get User Info from Quick Shipout Invoice Json data ///////////////////

            $invoicedetails = json_decode($quickshipoutInvoicedetails->invoiceDetails);

            //print_r($invoicedetails); die;

            $invoicePackage = $invoicedetails[0]->packages;

            foreach($invoicePackage as $package)
            {
                $userInfo[] = \App\Model\User::where('unit',$package->unit)->first();

                $noofboxes[] = $package->boxes;

                $totalboxes[] = $package->totalbox;

                ////Find Shipment of same order number and update every delivery with same status////

                    $shipmentOrder = \App\Model\Order::where('orderNumber', $package->orderNumber)->first();

                    $findDeliveryarray = \App\Model\Shipmentdelivery::where("shipmentId", $shipmentOrder['shipmentId'])->leftJoin('shippingmethods', 'shipment_deliveries.shippingMethodId', '=', 'shippingmethods.shippingid')->where('shipment_deliveries.deleted', '0')->where('shippingmethods.sea', 'Y')->get();

                    foreach($findDeliveryarray as $delivery)
                    {

                        $shipmentStatus = \App\Model\Shipmentstatuslog::where('shipmentId', $shipmentOrder['shipmentId'])->where('deliveryId', $delivery->id)->orderBy('id', "desc")->get();

                        if(!empty($shipmentStatus))
                        {
                            //delete all status for the shipment id
                            \App\Model\Shipmentstatuslog::where('shipmentId', $shipmentOrder['shipmentId'])->where('deliveryId', $delivery->id)->delete();


                            $updateShipmentStatus = new \App\Model\Shipmentstatuslog;
                            $counter =0;

                            foreach ($statusPost as $eachStatus => $dates) {
                                

                                if($counter >0)
                                {
                                    $statusData = \App\Model\Shipmentstatuslog::where('shipmentId', $shipmentOrder['shipmentId'])->where('deliveryId', $delivery->id)->orderBy("id", "desc")->first();

                                    $oldStatus = $statusData['status'];

                                }else{
                                    $oldStatus = 'in_warehouse';
                                }

                                $updateShipmentStatus->shipmentId = $shipmentOrder['shipmentId'];
                                $updateShipmentStatus->shipmentId = $delivery->id;
                                $updateShipmentStatus->oldStatus = $oldStatus;
                                $updateShipmentStatus->status = $eachStatus;
                                $updateShipmentStatus->updatedOn = date('Y-m-d h:i:s', strtotime($dates));
                                $updateShipmentStatus->save();
                                $counter ++;
                            }

                        }else{
                            $counter =0;
                            foreach ($statusPost as $eachStatus => $dates) {

                                if($counter >0)
                                {
                                    $statusData = \App\Model\Shipmentstatuslog::where('shipmentId', $shipmentOrder['shipmentId'])->where('deliveryId', $delivery->id)->orderBy("id", "desc")->first();

                                    $oldStatus = $statusData['status'];

                                }else{
                                    $oldStatus = 'in_warehouse';
                                }

                                $updateShipmentStatus->shipmentId = $shipmentOrder['shipmentId'];
                                $updateShipmentStatus->shipmentId = $delivery->id;
                                $updateShipmentStatus->oldStatus = 'in_warehouse';
                                $updateShipmentStatus->status = $eachStatus;
                                $updateShipmentStatus->updatedOn = date('Y-m-d h:i:s', strtotime($dates));
                                $updateShipmentStatus->save();
                                $counter ++;

                            }

                        }
                        

                    }

                   
                    //$updateShipmentStatus->status


               ////Find Shipment of same order number and update every delivery with same status////

            }

            

            ////////////Get User Info from Quick Shipout Invoice Json data ///////////////////


            foreach ($statusPost as $eachStatus => $dates) {
                if ($dates != '') {
                    $shipmentStatusData = \App\Model\Quickshipoutoceanstatuslog::where("quickshipoutId", $id)->orderBy("id", "desc")->first();
                    if(count($shipmentStatusData) >0)
                    {
                         if($shipmentStatusData->status!=$eachStatus)
                            {
                                $shipmentStatus = new \App\Model\Quickshipoutoceanstatuslog;
                                $shipmentStatus->quickshipoutId = $id;
                                $shipmentStatus->oldStatus = (!empty($shipmentStatusData->status) ? $shipmentStatusData->status : 'none');
                                $shipmentStatus->status = $eachStatus;
                                $shipmentStatus->updatedOn = date('Y-m-d h:i:s', strtotime($dates));
                                $shipmentStatus->save();

                            }

                    }else{
                        $shipmentStatus = new \App\Model\Quickshipoutoceanstatuslog;
                                $shipmentStatus->quickshipoutId = $id;
                                $shipmentStatus->oldStatus = 'none';
                                $shipmentStatus->status = $eachStatus;
                                $shipmentStatus->updatedOn = date('Y-m-d h:i:s', strtotime($dates));
                                $shipmentStatus->save();
                    }

                    $i=0;
                    
                    foreach($userInfo as $user)
                    {
                        
                        $isNotificationExist = \App\Model\Quickshipoutstatusnotification::where("quickshipoutId",$id)->where("useremail", $user->email)->get()->toArray();

                        if (!empty($isNotificationExist)) {
                            $quickshipoutstatusnotification = $isNotificationExist;                            
                        }
                        else{
                            $quickshipoutstatusnotification = new \App\Model\Quickshipoutstatusnotification;
                            $quickshipoutstatusnotification->quickshipoutId = $id;
                            $quickshipoutstatusnotification->useremail = $user->email;
                            $quickshipoutstatusnotification->usercontact = $user->contactNumber; 
                            $quickshipoutstatusnotification->noofBoxesShipped = $noofboxes[$i];
                            $quickshipoutstatusnotification->totalNoofBoxes = $totalboxes[$i];
                            $quickshipoutstatusnotification->templateKey = "quickshipoutocean_status_change";
                            $quickshipoutstatusnotification->quickshipoutType = "1";
                            $quickshipoutstatusnotification->emailSentStatus = "0";
                            $quickshipoutstatusnotification->smsSentStatus = "0";
                            $quickshipoutstatusnotification->save();
                        }
                        $i++;
                    }

            return 1;

        }else{

            return 0;

        }

        }

    }

}

















    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
}

 
