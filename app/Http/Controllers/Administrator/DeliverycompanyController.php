<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Deliverycompany;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class DeliverycompanyController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Deliverycompany'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        
        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('DELIVERYCOMPANYDATA');
            \Session::push('DELIVERYCOMPANYDATA.searchDisplay', $searchDisplay);
            \Session::push('DELIVERYCOMPANYDATA.field', $field);
            \Session::push('DELIVERYCOMPANYDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('DELIVERYCOMPANYDATA.field');
            $sortType = \Session::get('DELIVERYCOMPANYDATA.type');
            $searchDisplay = \Session::get('DELIVERYCOMPANYDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTRY LIST  */
        $deliveryCompanyData = Deliverycompany::getCompanyList($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Delivery Companies";
        $data['contentTop'] = array('breadcrumbText' => 'Delivery Companies', 'contentTitle' => 'Delivery Companies', 'pageInfo' => 'This section allows you to manage delivery companies');
        $data['pageTitle'] = "Delivery Companies";
        $data['page'] = $deliveryCompanyData->currentPage();
        $data['deliveryCompanyData'] = $deliveryCompanyData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.deliverycompany.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['page'] = !empty($page) ? $page : '1';
        if (!empty($id)) {
            $data['id'] = $id;
            $data['pageTitle'] = 'Edit Delivery Company';
            $deliverycompany = Deliverycompany::find($id);
            $data['deliverycompany'] = $deliverycompany;
        } else {
            $data['id'] = 0;
            $data['pageTitle'] = 'Add Delivery Company';
        }
        return view('Administrator.deliverycompany.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $deliverycompany = new Deliverycompany;

        $validator = Validator::make($request->all(), [
                    'name' => 'required|unique:' . $deliverycompany->table . ',name,' . $id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $deliverycompany = Deliverycompany::find($id);
                $deliverycompany->modifiedBy = Auth::user()->id;
                $deliverycompany->modifiedOn = Config::get('constants.CURRENTDATE');
            } else {
                $deliverycompany->createdBy = Auth::user()->id;
                $deliverycompany->createdOn = Config::get('constants.CURRENTDATE');
            }
            $deliverycompany->name = $request->name;
            $deliverycompany->save();
            $deliverycompanyId = $deliverycompany->id;

            return redirect('/administrator/deliverycompany?page=' . $page)->with('successMessage', 'Delivery company information saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Deliverycompany::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/deliverycompany/?page=' . $page)->with('successMessage', 'Delivery company status changed successfully.');
            } else {
                return \Redirect::to('administrator/deliverycompany/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/deliverycompany/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
