<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\libraries\dbHelpers;
use Auth;
use App\Model\Tax;
use Hash;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Routing\Route;
use Config;
use customhelper;
use Excel;
use Carbon\Carbon;

class TaxsettingsController extends Controller {
    public $_perPage;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Request $request) {
        $data = array();
        
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.TaxSettings'), Auth::user()->id); // call the helper function
        
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            \Session::forget('TAXDATA');
            \Session::push('TAXDATA.searchDisplay', $searchDisplay);
            \Session::push('TAXDATA.field', $field);
            \Session::push('TAXDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('TAXDATA.field');
            $sortType = \Session::get('TAXDATA.type');
            $searchDisplay = \Session::get('TAXDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'taxName';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'taxName' => array('current' => 'sorting'),
            'addressType' => array('current' => 'sorting'),
            'priority' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* GET THE TAX RECORD  */
        $taxData = Tax::getTaxList($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Tax Settings :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Tax Settings";
        $data['contentTop'] = array('breadcrumbText' => 'Tax Settings', 'contentTitle' => 'Tax Settings', 'pageInfo' => 'In this section you can define taxes to be used within your store');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavSettings', 'menuSub' => 'leftNavShippingChargesSettings3', 'menuSubSub' => 'leftNavTaxSystem43');
        $data['taxlist'] = $taxData;
        $data['page'] = $taxData->currentPage();
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        return view('Administrator.taxsettings.index', $data);
    }


    /**
     * Method for create tax record
     * @return object
     */
    public function create()
    { 
        $data = array();
        /* SET DATA FOR VIEW  */
        $data['pageTitle'] = "Add New Tax";
        $data['page'] = !empty($page) ? $page : '1';
        return view('Administrator.taxsettings.add', $data);
    }

    

    /**
     * Method for add or update tax record
     * @return object
     */
    public function store(Request $request){
        
        $data = array();
        if (\Request::isMethod('post')) {
            
            if($request->addTax == 'addTax'){
                /* FOR ADD  */
                $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.TaxSettings'), Auth::user()->id); // call the helper function
                if($findRole['canAdd'] == 0){
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
                $priority = ($request->priority == '' ? 0: $request->priority);
                /* ADD NEW RECORD  */
                $tax = new Tax;
                $tax->taxName = trim($request->taxName);
                $tax->taxDisplayName = trim($request->taxDisplayName);
                $tax->formula = $request->formula;
                $tax->addressType = $request->addressType;
                $tax->regNumber = trim($request->regNumber);
                $tax->priority = $priority;
                $tax->formulaArrays =  ltrim($request->arrays, ',');
                $tax->createdBy = Auth::user()->id;
                $tax->createdOn = Config::get('constants.CURRENTDATE');
                $tax->save();
                return redirect('/administrator/taxsettings')->with('successMessage', 'Tax information added successfuly.');
            } 
            if($request->addTax == 'editTax'){
                /* FOR EDIT  */
                $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.TaxSettings'), Auth::user()->id); // call the helper function
                if($findRole['canEdit'] == 0){
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
                $priority = ($request->priority == '' ? 0: $request->priority);
                /* UPDATE EXISTING RECORD  */
                $tax = Tax::find($request->taxId);
                $tax->taxName = trim($request->taxName);
                $tax->taxDisplayName = trim($request->taxDisplayName);
                $tax->formula = $request->formula;
                $tax->addressType = $request->addressType;
                $tax->regNumber = trim($request->regNumber);
                $tax->priority = $priority;
                $tax->formulaArrays =  ltrim($request->arrays, ',');
                $tax->modifiedBy = Auth::user()->id;
                $tax->modifiedOn = Config::get('constants.CURRENTDATE');
                $tax->save();
                return redirect('/administrator/taxsettings')->with('successMessage', 'Tax information updated successfuly.');
            }

            /* SET DATA FOR VIEW  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            \Session::forget('TAXDATA');
            \Session::push('TAXDATA.searchDisplay', $searchDisplay);
            \Session::push('TAXDATA.field', $field);
            \Session::push('TAXDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('TAXDATA.field');
            $sortType = \Session::get('TAXDATA.type');
            $searchDisplay = \Session::get('TAXDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'taxName';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'taxName' => array('current' => 'sorting'),
            'addressType' => array('current' => 'sorting'),
            'priority' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* GET THE TAX RECORD  */
        $taxData = Tax::getTaxList($param);
        
        /* SET DATA FOR VIEW  */
        $data['title'] = "Tax Settings :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Tax Settings";
        $data['contentTop'] = array('breadcrumbText' => 'Tax Settings', 'contentTitle' => 'Tax Settings', 'pageInfo' => 'In this section you can define taxes to be used within your store');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavSettings', 'menuSub' => 'leftNavShippingChargesSettings3', 'menuSubSub' => 'leftNavTaxSystem43');
        $data['taxlist'] = $taxData;
        $data['page'] = $taxData->currentPage();
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        return view('Administrator.taxsettings.index', $data);
    }

    /**
     * Method for edit
     * @param integer $id
     * @return object
     */
    public function edit($id = 0, Request $request)
    {
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.TaxSettings'), Auth::user()->id); 
        if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $data = array();
        
        /* GET THE TAX RECORD  */
        $data['taxRow'] = Tax::find($id);
        /* SET DATA FOR VIEW  */
        $data['pageTitle'] = "Edit Tax";
        $data['page'] = !empty($page) ? $page : '1';
        return view('Administrator.taxsettings.add', $data);
    }


    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @param integer $status
     * @return array
     */
    public function changeStatus($id = 0, $status = 1, $page){
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.TaxSettings'), Auth::user()->id);
        if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            /*  TO CHANGE THE STATUS  */
            if (Tax::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/taxsettings/?page=' . $page)->with('successMessage', 'Tax status changed successfully.');
            } else {
                return \Redirect::to('administrator/taxsettings/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/taxsettings/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }


    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return array
     */
    public function delete($id, $page) {
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.TaxSettings'), Auth::user()->id); // call the helper function
        if($findRole['canDelete'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            /*  DELETING RECORD  */
            if (Tax::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/taxsettings/?page=' . $page)->with('successMessage', 'Tax deleted successfully.');
            } else {
                return \Redirect::to('administrator/taxsettings/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/taxsettings/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }


    /**
     * Method used to Export All with selected fields
     * @param integer $request
     * @param integer $page
     * @return array
     */
    public function exportall($page, Request $request) {

        if (count($request->selectall) == 0) {
            return \Redirect::to('administrator/taxsettings/')->with('errorMessage', 'Select atleast one field during the time of export');
        }
        if (\Request::isMethod('post')) {
            /*  GET RECORDS  */
            $taxDataExcel = Tax::getAdminUserListExcel($request->selectall)->get();
            /*  GENERATE EXCEL FILE  */
            ob_end_clean();
            ob_start();
            Excel::create("TaxSystem" . Carbon::now(), function($excel) use($taxDataExcel) {
                $excel->sheet('Sheet 1', function($sheet) use($taxDataExcel) {
                    $sheet->fromArray($taxDataExcel);
                });
            })->export('xlsx');
            ob_flush();
            return \Redirect::to('administrator/taxsettings/')->with('successMessage', 'Excel file created and downloaded');
        }
    }

}
