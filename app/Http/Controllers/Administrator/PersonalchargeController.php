<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Personalcharge;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;

class PersonalchargeController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Personalcharge'), Auth::user()->id); // call the helper function
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            $searchByMake = \Input::get('searchByMake', '');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('SHIPPINGCOSTDATA');
            \Session::push('SHIPPINGCOSTDATA.searchByMake', $searchByMake);
            \Session::push('SHIPPINGCOSTDATA.searchDisplay', $searchDisplay);
            \Session::push('SHIPPINGCOSTDATA.field', $field);
            \Session::push('SHIPPINGCOSTDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByMake'] = $searchByMake;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('SHIPPINGCOSTDATA.field');
            $sortType = \Session::get('SHIPPINGCOSTDATA.type');
            $searchByMake = \Session::get('SHIPPINGCOSTDATA.searchByMake');
            $searchDisplay = \Session::get('SHIPPINGCOSTDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchByMake'] = !empty($searchByMake) ? $searchByMake[0] : '';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'makeName' => array('current' => 'sorting'),
            'modelName' => array('current' => 'sorting'),
            'shippingOption' => array('current' => 'sorting'),
            'cost' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH model LIST  */
        $personalChargeData = Personalcharge::getList($param);

        $data['makeData'] = \App\Model\Automake::where('deleted', '0')->orderBy('name', 'ASC')->get();
        $data['personalChargeData'] = $personalChargeData;
        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Personal Items Clearing Charge";
        $data['contentTop'] = array('breadcrumbText' => 'Personal Items Clearing Charge', 'contentTitle' => 'Personal Items Clearing Charge', 'pageInfo' => 'This section allows you to manage clearing charges');
        $data['pageTitle'] = "Personal Items Clearing Charge";
        $data['page'] = $personalChargeData->currentPage();
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavAuto', 'menuSub' => '', 'menuSubSub' => 'leftNavPersonalItemsClearingCharge9');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.personalcharge.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Personalcharge'), Auth::user()->id); // call the helper function
        $data = array();
        $data['title'] = "Administrative Panel :: Personal Items Clearing Charge";
        $data['contentTop'] = array('breadcrumbText' => 'Personal Items Clearing Charge', 'contentTitle' => 'Personal Items Clearing Charge', 'pageInfo' => 'This section allows you to manage clearing charges');
        $data['page'] = !empty($page) ? $page : '1';

        $data['makeList'] = \App\Model\Automake::where('deleted', '0')->orderBy('name', 'ASC')->get();

        if (!empty($id)) {
            if ($findRole['canEdit'] == 0) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['pageTitle'] = "Edit Clearing Charge";
            $data['id'] = $id;
            $data['page'] = $page;
            $data['action'] = 'Edit';
            $personalCharge = $data['personalCharge'] = Personalcharge::find($id);

            $model = \App\Model\Automodel::where('id', $personalCharge->modelId)->first();
            $data['modelList'] = \App\Model\Automodel::where('deleted', '0')->where('makeId', $model->makeId)->orderBy('name', 'ASC')->get();
            $data['makeId'] = $model->makeId;
        } else {
            if ($findRole['canAdd'] == 0) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add New Clearing Charge";
            $data['page'] = $page;

            $data['modelList'] = array();
        }
        return view('Administrator.personalcharge.addedit', $data);
    }

    /**
     * Method to fetch model list
     * @return array
     */
    public function getmodellist($makeId = '') {
        $modelList = array();
        if (isset($makeId) && !empty($makeId)) {
            $modelList = \App\Model\Automodel::where('deleted', '0')
                    ->where('makeId', $makeId)
                    ->orderby('name', 'asc')
                    ->get();
        }
        echo json_encode($modelList);
        exit;
    }

    /**
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Personalcharge'), Auth::user()->id); // call the helper function
        $data['page'] = !empty($page) ? $page : '1';
        $personalCharge = new Personalcharge;

        $validator = Validator::make($request->all(), [
                    'modelId' => 'required',
                    'charge' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if ($id != 0) { // Edit
                if ($findRole['canEdit'] == 0) {
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
                $personalCharge = Personalcharge::find($id);
                $personalCharge->modifiedBy = Auth::user()->id;
                $personalCharge->modifiedOn = Config::get('constants.CURRENTDATE');
            } else { // Add
                if ($findRole['canAdd'] == 0) {
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }

                $personalCharge->createdBy = Auth::user()->id;
                $personalCharge->createdOn = Config::get('constants.CURRENTDATE');
            }

            $personalCharge->modelId = $request->modelId;
            $personalCharge->charge = $request->charge;
            $personalCharge->save();

            return redirect('/administrator/personalcharge?page=' . $page)->with('successMessage', 'Personal clearing charge information saved successfuly.');
        }
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Personalcharge'), Auth::user()->id); // call the helper function
        if ($findRole['canDelete'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Personalcharge::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/personalcharge/?page=' . $page)->with('successMessage', 'Personal clearing charge deleted successfully.');
            } else {
                return \Redirect::to('administrator/personalcharge/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/personalcharge/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
