<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserAdmin;
//use App\libraries\helpers;
//use App\libraries\dbHelpers;
use Auth;
use App\Model\User;
use App\Model\Permission;
use App\Model\UserTypeDefaultPermission;
use Illuminate\Contracts\Auth\Authenticatable;
use customhelper;
use Config;

class AuthenticationrequiredController extends Controller {

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        $this->middleware('auth:admin');
    }
    public function index() {
        $data['title'] = "Authentication Required :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Authentication Required";
        $data['contentTop'] = array('breadcrumbText'=>'Authentication Required','contentTitle'=>'Authentication Required','pageInfo'=>'You don\'t have the required permission');
        return view('Administrator.authenication.index', $data);
    }


    


}
