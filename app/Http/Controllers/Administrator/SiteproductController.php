<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Siteproduct;
use App\Model\Sitecategory;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use customhelper;
use Config;

class SiteproductController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Request $request) {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Settingsproducts'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }


        $param = array();
        $siteproductObj = new Siteproduct();
        $sitecategoryObj = new Sitecategory();
        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */

            $sortField = \Input::get('field', 'id');
            $sortOrder = \Input::get('type', 'desc');
            $perpage = \Input::get('searchDisplay', $this->_perPage);
            $searchData = \Input::get('searchData', "");
            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('RECORDPROD');
            \Session::push('RECORDPROD.searchData', $searchData);
            \Session::push('RECORDPROD.searchDisplay', $perpage);
            \Session::push('RECORDPROD.field', $sortField);
            \Session::push('RECORDPROD.type', $sortOrder);
        } else {
            $sortField = \Session::get('RECORDPROD.field');
            $sortType = \Session::get('RECORDPROD.type');
            $perpage = \Session::get('RECORDPROD.searchDisplay');
            $searchData = \Session::get('RECORDPROD.searchData');

            $sortField = !empty($sortField) ? $sortField[0] : 'id';
            $sortOrder = !empty($sortType) ? $sortType[0] : 'desc';
            $perpage = !empty($perpage) ? $perpage[0] : $this->_perPage;
            $searchData = !empty($searchData) ? $searchData[0] : "";
        }

        $param['sortField'] = $sortField;
        $param['sortOrder'] = $sortOrder;
        $param['searchDisplay'] = $perpage;
        $param['searchData'] = $searchData;

        /* Fetch Data For Procurement Iems & Shipment Packages */
        $data['productIdExists'] = array();
        $procurementProductId = \App\Model\Procurementitem::fetchUsedFieldIds('siteProductId');
        $shipmentProductId = \App\Model\Shipmentpackage::fetchUsedFieldIds('siteProductId');
        $data['productIdExists'] = array_unique(array_merge($procurementProductId, $shipmentProductId));

        $data['param'] = $param;
        $records = $siteproductObj->getData($param);
        $data['records'] = $records;
        $data['page'] = $records->currentPage();
        $data['siteCategory'] = $sitecategoryObj->getCategoryList('all');
        $data['parentCategory'] = $sitecategoryObj->getCategoryList('parent');
        $data['pageTitle'] = 'Site Products';
        $data['title'] = "Site Products :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText' => array('Shipping Settings', 'Site Products'), 'contentTitle' => 'Site Products', 'pageInfo' => 'This section allows you to manage site products');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.siteproduct.index', $data);
    }

    public function addeditsiteproduct($id = -1, $page = 1) {

        $data = array();
        $siteproductObj = new Siteproduct();
        $sitecategoryObj = new Sitecategory();

        if ($id != -1) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $record = $siteproductObj->find($id);
            $data['record'] = $record;
            $data['subCategory'] = $sitecategoryObj->getSubCategory($record->categoryId);
        } else {
            $data['action'] = 'Add';
            $data['record'] = $siteproductObj;
            $data['subCategory'] = array();
        }
        $data['page'] = $page;
        $data['siteCategory'] = $sitecategoryObj->getCategoryList('parent');
        $data['pageTitle'] = $data['action'] . ' Site Products';
        $data['title'] = "Site Products :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText' => array('Shipping Settings', 'Site Products'), 'contentTitle' => 'Site Products', 'pageInfo' => 'This section allows you to ' . strtolower($data['action']) . ' site products');
        return view('Administrator.siteproduct.addeditsiteproduct', $data);
    }

    public function addeditproductrecord($id = '-1', $page = 1, Request $request) {
        $siteproductObj = new Siteproduct();
        if ($id != -1)
            $siteproductObj = $siteproductObj->find($id);
        if ($id == '-1') {
            $this->validate($request, [
                'productImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'weight' => 'required',
            ]);
        } else {
            $this->validate($request, [
                'productImage' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'weight' => 'required',
            ]);
        }

        if ($request->hasFile('productImage')) {
            $image = $request->file('productImage');
            //$name = time().'.'.$image->getClientOriginalExtension();
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/site_products');
            $image->move($destinationPath, $name);
            if ($id != '-1') {
                if(file_exists('/uploads/site_products/'.$siteproductObj->image)){
                    unlink(public_path('/uploads/site_products/' . $siteproductObj->image));
                }
            }
            $siteproductObj->image = $name;
        }

        $siteproductObj->productName = $request->input('productName');
        $siteproductObj->categoryId = $request->input('categoryId');
        $siteproductObj->subCategoryId = $request->input('subCategoryId');
        $siteproductObj->amount = 0;
        $siteproductObj->weight = !empty($request->weight) ? $request->weight : '0.00';
        $siteproductObj->message = addslashes($request->input('pageContent'));
        $siteproductObj->quoteMessage = $request->input('quoteMessage');
        $siteproductObj->scheduleBNumber = $request->input('scheduleBNumber');


        if ($siteproductObj->save())
            return redirect(route('siteproductlist') . '?page=' . $page)->with('successMessage', 'Information saved successfuly.');
    }

    public function editstatus($id = -1, $status) {

        $siteproductObj = new Siteproduct();
        if ($id != -1)
            $siteproductObj = $siteproductObj->find($id);
        $siteproductObj->status = $status;

        if ($siteproductObj->save())
            return json_encode(array('updated' => true));
        else
            return json_encode(array('updated' => false));
    }

    public function deletesiteproduct($id = -1, $page = 1) {
        /* Fetch Data For Procurement Iems & Shipment Packages */
        $procurementProductId = \App\Model\Procurementitem::fetchUsedFieldIds('siteProductId');
        $shipmentProductId = \App\Model\Shipmentpackage::fetchUsedFieldIds('siteProductId');
        $productIdExists = array_unique(array_merge($procurementProductId, $shipmentProductId));

        $siteproductObj = new Siteproduct();
        if ($id != -1) {
            if (!in_array($id, $productIdExists)) {
                $siteproductObj = $siteproductObj->find($id);
                if(file_exists(public_path('/uploads/site_products/' . $siteproductObj->image)))
                {
                   unlink(public_path('/uploads/site_products/' . $siteproductObj->image)); 
                }
                
                $siteproductObj->delete();
                return redirect(route('siteproductlist'))->with('successMessage', 'Information deleted successfuly.');
            } else {
                return redirect(route('siteproductlist'))->with('errorMessage', 'Product cannot be deleted. Dependency exists.');
            }
        } else {
            return redirect(route('siteproductlist'))->with('errorMessage', 'Failed to delete product information.');
        }
    }

    public function exportproduct() {

        $data = array();
        $param = array();
        $siteproductObj = new Siteproduct();
        $sitecategoryObj = new Sitecategory();

        $sortField = \Session::get('RECORDPROD.field');
        $sortType = \Session::get('RECORDPROD.type');
        $perpage = \Session::get('RECORDPROD.searchDisplay');
        $searchData = \Session::get('RECORDPROD.searchData');

        $sortField = !empty($sortField) ? $sortField[0] : 'id';
        $sortOrder = !empty($sortType) ? $sortType[0] : 'desc';
        $perpage = 10000000;
        $searchData = !empty($searchData) ? $searchData[0] : "";



        $param['sortField'] = $sortField;
        $param['sortOrder'] = $sortOrder;
        $param['searchDisplay'] = $perpage;
        $param['searchData'] = $searchData;

        $data['param'] = $param;
        $records = Siteproduct::whereRaw(1)
                ->select('productName', 'weight', \DB::raw('(select categoryName from stmd_sitecategories where stmd_sitecategories.id = stmd_siteproducts.categoryId) as Category'), \DB::raw('(select categoryName from stmd_sitecategories where stmd_sitecategories.id = stmd_siteproducts.subCategoryId) as "Sub Category"'))
                ->get();

        //dd($records);
        ob_end_clean();
        ob_start();
        //$adminUserDataExcel = UserAdmin::getAdminUserListExcel($request->selectall)->get();
        //dd($records);

        ob_end_clean();
        ob_start();
        \Excel::create("Admin-Users" . Carbon::now(), function($excel) use($records) {
            $excel->sheet('Sheet 1', function($sheet) use($records) {
                $sheet->fromArray($records);
            });
        })->export('xlsx');
        ob_flush();
        return \Redirect::to('administrator/settingsproducts/')->with('successMessage', 'Excel file created and downloaded');
    }

    public function getsiteproduct($subCategoryId) {

        $productData = array();
        $siteProducts = Siteproduct::getListBySubCatid($subCategoryId);
        if (!empty($siteProducts)) {
            $i = 0;
            foreach ($siteProducts as $eachProduct) {
                $productData[$i]['id'] = $eachProduct->id;
                $productData[$i]['product'] = $eachProduct->productName;
                $productData[$i]['weight'] = $eachProduct->weight;
                $i++;
            }
        }
        return json_encode(array('data' => $productData));
    }

    public function getsiteproductweight($productId) {

        $productInfo = Siteproduct::find($productId);
        if (!empty($productInfo))
            return $productInfo->weight;
        else
            return '0';
    }

}
