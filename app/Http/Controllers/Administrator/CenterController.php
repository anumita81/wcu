<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Center;
use App\Model\Usernotification;
use App\Model\Addressbook;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\Document;
use App\Model\Emailtemplate;
use App\Model\Notificationtemplate;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Excel;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Mail;
use PDF;
use customhelper;
use Carbon\Carbon;

ini_set('max_execution_time', 0); // for infinite time of execution 

class CenterController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        //$this->_perPage = 10;

        $this->_perPage = 30; //Updated on 27-11-2018
    }

    public function index(Route $route, Request $request) {
        $data = array();


        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Center'), Auth::user()->id);

        $findAdminUser = \App\Model\UserAdmin::find(Auth::user()->id);

     

        // call the helper function
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }


        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchBycenterName = \Input::get('searchBycenterName', '');
            $searchByEmail = \Input::get('searchByEmail', '');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('CENTERDATA');
            \Session::push('CENTERDATA.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('CENTERDATA.searchDisplay', $searchDisplay);
            \Session::push('CENTERDATA.searchByDate', $searchByDate);
            \Session::push('CENTERDATA.searchBycenterName', $searchBycenterName);
            \Session::push('CENTERDATA.field', $field);
            \Session::push('CENTERDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchBycenterName'] = $searchBycenterName;
            $param['searchByDate'] = $searchByDate;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('CENTERDATA.field');
            $sortType = \Session::get('CENTERDATA.type');
            $searchByCreatedOn = \Session::get('CENTERDATA.searchByCreatedOn');
            $searchByDate = \Session::get('CENTERDATA.searchByDate');
            $searchBycenterName = \Session::get('CENTERDATA.searchBycenterName');
            $searchDisplay = \Session::get('CENTERDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchBycenterName'] = !empty($searchBycenterName) ? $searchBycenterName[0] : '';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'centerName' => array('current' => 'sorting'),
            'centerCode' => array('current' => 'sorting'),
            'contactPersonName' => array('current' => 'sorting'),
            'contactPersonPhone' => array('current' => 'sorting'),
            'contactPersomEmail' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';


        /* FETCH USER LIST  */
       
        
        $centerData = Center::getCenterList($param);
        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Center";
        $data['contentTop'] = array('breadcrumbText' => 'Center', 'contentTitle' => 'Center', 'pageInfo' => 'This section allows you to manage profile of your site users');
        $data['pageTitle'] = "Center";
        $data['page'] = $centerData->currentPage();
        $data['centerData'] = $centerData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.center.index', $data);
    }

    /**
     * Method for add edit center
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Center";
        $data['contentTop'] = array('breadcrumbText' => 'Center', 'contentTitle' => 'Center', 'pageInfo' => 'This section allows you to manage profile of your site center');
        $data['page'] = !empty($page) ? $page : '1';

        if (!empty($id)) {
            $data['id'] = $id;
            $center = Center::find($id);
            $data['center'] = $center;

            $data['action'] = 'Edit';
            $data['pageTitle'] = "Edit Center";

            return view('Administrator.center.edit', $data);
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add Center";
            return view('Administrator.center.add', $data);
        }
    }

    /**
     * Method to fetch state list
     * @return array
     */
    public function getstatelist($countryid = '', $json = TRUE) {
        $country = Country::where('id', $countryid)->orderby('name', 'asc')->get(['code']);
        $countryCode = $country[0]['code'];
        $stateList = array();
        if (isset($countryCode) && !empty($countryCode)) {
            $stateList = State::where('status', '1')
                    ->where('deleted', '0')
                    ->where('countryCode', $countryCode)
                    ->orderby('name', 'asc')
                    ->get();
        }
        if ($json == TRUE) {
            echo json_encode($stateList);
            exit;
        } else {
            return $stateList;
        }
    }

    /**
     * Method to fetch city list
     * @return array
     */
    public function getcitylist($stateid = '', $json = TRUE) {
        $state = State::where('id', $stateid)->get(['code', 'countryCode']);
        $stateCode = $state[0]['code'];
        $countryCode = $state[0]['countryCode'];

        $cityList = array();
        if (isset($stateCode) && !empty($stateCode)) {
            $cityList = City::where('status', '1')
                    ->where('deleted', '0')
                    ->where('stateCode', $stateCode)
                    ->where('countryCode', $countryCode)
                    ->orderby('name', 'asc')
                    ->get();
        }
        if ($json == TRUE) {
            echo json_encode($cityList);
            exit;
        } else {
            return $cityList;
        }
    }

    /**
     * Method to fetch city list by users
     * @return array
     */
    public function getcitylistbycountry($countryid = '', $json = TRUE) {
        $users = Country::where('id', $countryid)->get(['code']);
        $countryCode = $users[0]['code'];
        $cityList = array();
        if (isset($countryCode) && !empty($countryCode)) {
            $cityList = City::where('status', '1')
                    ->where('deleted', '0')
                    ->where('countryCode', $countryCode)
                    ->orderby('name', 'asc')
                    ->get();
        }
        if ($json == TRUE) {
            echo json_encode($cityList);
            exit;
        } else {
            return $cityList;
        }
    }

    /** Method used to check the password patern
     * @param string $password
     * @return boolean
     */
    public function is_valid_password($password) {
        //return preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$_!%*?&-]{8,20}$/', $password) ? true : false;
        return preg_match('/^.{8,20}$/', $password) ? true : false;
    }

    /** Edit
     * Method used to save user information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $center = new Center;

        if (empty($id)) {

            /* PASSWORD PATTERN MATCHIN */
            if ($this->is_valid_password($request->password) == false) {
                $err = 'Password must contains minimum 8 characters.';
                return redirect()->back()->withInput(\Input::all())->with('errorMessage', 'Password must contains minimum 8 characters.');
            }


            $validator = Validator::make($request->all(), [
                        'centerName' => 'required|alpha',
                        'centerCode' => 'required',
                        'password' => 'required',
                        'contactPersomEmail' => 'required|unique:' . $center->table . ',contactPersomEmail,' . $id . '|email',
            ]);
        } else {
            if ($request->password != '') {
                /* PASSWORD PATTERN MATCHIN */
                if ($this->is_valid_password($request->password) == false) {
                    $err = 'Password must contains minimum 8 characters.';
                    return redirect()->back()->withInput(\Input::all())->with('errorMessage', 'Password must contains minimum 8 characters.');
                }
            }

            $validator = Validator::make($request->all(), [
                        'centerName' => 'required|alpha',
                        'centerCode' => 'required',
                        'contactPersomEmail' => 'required|unique:' . $center->table . ',contactPersomEmail,' . $id . '|email',
            ]);
        }
        //print_r($request->all()); die;
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $center = Center::find($id);
                $center->modifiedBy = Auth::user()->id;
                $center->modifiedOn = Config::get('constants.CURRENTDATE');
            } else {
                $center->status = '1';
                $center->password = Hash::make($request->password);
                $center->createdBy = Auth::user()->id;
                $center->createdOn = Config::get('constants.CURRENTDATE');
                $center->contactPersonImage = 'default.jpg';
            }
            $center->centerName = $request->centerName;
            $center->centerCode = $request->centerCode;
            $center->contactPersonName = $request->contactPersonName;
            $center->contactPersonPhone = $request->contactPersonPhone;
            $center->contactPersonAlterPhone = !empty($request->contactPersonAlterPhone)? $request->contactPersonAlterPhone: "";
            $center->address = $request->address;
            $center->contactPersomEmail = $request->contactPersomEmail;

            $center->save();
            $centerId = $center->id;

           
            return redirect('/administrator/center?page=' . $page)->with('successMessage', 'Center information saved successfuly.');
            
        }
    }


    /**
     * Method used to change status
     * @param integer $id
     * @param integer $page
     * @param integer $status
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::center()->id;
        if (!empty($id)) {
            if (User::changeStatus($id, $createrModifierId, $status)) {

                $center = Center::where('id', $id)->first();

                if ($status == 1) {
                    /* Save & Send Notification */
                    User::sendPushNotification($id, 'admin_approval', $createrModifierId);

                    $emailTemplate = Emailtemplate::where('templateKey', 'admin_approval')->first();
                    $replace = $center->firstName . " " . $center->lastName;
                    $to = $center->email;
                   

                    $content = str_replace('[NAME]', $replace, stripslashes($emailTemplate['templateBody']));
                    
                    Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($emailTemplate, $to) {
                        $message->from($emailTemplate['fromEmail'], $emailTemplate['fromName']);
                        $message->subject($emailTemplate['templateSubject']);
                        $message->to($to);
                    });
                } else if ($status == 3) {
                    /* Save & Send Notification */
                    Center::sendPushNotification($id, 'admin_decline', $createrModifierId);

                    $emailTemplate = Emailtemplate::where('templateKey', 'admin_decline')->first();
                    $replace = $center->firstName . " " . $center->lastName;
                    $to = $center->email;
                    $content = str_replace('[NAME]', $replace, stripslashes($emailTemplate['templateBody']));

                    Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($emailTemplate, $to) {
                        $message->from($emailTemplate['fromEmail'], $emailTemplate['fromName']);
                        $message->subject($emailTemplate['templateSubject']);
                        $message->to($to);
                    });
                }

                return response()->json(['status' => '1', 'message' => 'Center status changed successfully.']);
            } else {
                return response()->json(['status' => '0', 'message' => 'Error in operation!']);
            }
        } else {
            return response()->json(['status' => '0', 'message' => 'Error in operation!']);
        }
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::center()->id;
        if (!empty($id)) {
            if (Center::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/center/?page=' . $page)->with('successMessage', 'User deleted successfully.');
            } else {
                return \Redirect::to('administrator/center/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/center/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to delete
     * @param integer $userid
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function deleteaddressbook($userid, $id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Addressbook::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to("administrator/users/addedit/$userid/$page")->with('successMessage', 'Address information deleted successfully.');
            } else {
                return \Redirect::to("administrator/users/addedit/$userid/$page")->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to("administrator/users/addedit/$userid/$page")->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to clear search history
     * @return type
     */
    public function showall() {
        \Session::forget('USERDATA');
        return \Redirect::to('administrator/center');
    }

    /**
     * Method used to Export All with selected fields
     * @param integer $page
     * @return type
     */
    public function exportall($page, Request $request) {
        #dd(session()->all());
        if (count($request->selectall) == 0) {
            return \Redirect::to('administrator/center/')->with('errorMessage', 'Select atleast one field during the time of export');
        }
        if (\Request::isMethod('post')) {

            $sortField = \Session::get('USERDATA.field');
            $sortType = \Session::get('USERDATA.type');
            $searchByCreatedOn = \Session::get('USERDATA.searchByCreatedOn');
            $searchByDate = \Session::get('USERDATA.searchByDate');
            $searchByfirstName = \Session::get('USERDATA.searchByfirstName');
            $searchBylastName = \Session::get('USERDATA.searchBylastName');
            $searchByEmail = \Session::get('USERDATA.searchByEmail');
            $searchByStatus = \Session::get('USERDATA.searchByStatus');
            $searchByCompany = \Session::get('USERDATA.searchByCompany');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchByfirstName'] = !empty($searchByfirstName) ? $searchByfirstName[0] : '';
            $param['searchBylastName'] = !empty($searchBylastName) ? $searchBylastName[0] : '';
            $param['searchByEmail'] = !empty($searchByEmail) ? $searchByEmail[0] : '';
            $param['searchByStatus'] = !empty($searchByStatus) ? $searchByStatus[0] : '';
            $param['searchByCompany'] = !empty($searchByCompany) ? $searchByCompany[0] : '';

            $recordList = User::getUserList($param, 'export')->toArray();

            $userData = array();
            if (!empty($recordList)) {
                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {
                            if ($count == 0) {

                                if ($key == 'unit')
                                    $cellValue = 'Unit';
                                if ($key == 'title')
                                    $cellValue = 'Title';
                                elseif ($key == 'firstName')
                                    $cellValue = 'First Name';
                                elseif ($key == 'lastName')
                                    $cellValue = 'Last Name';
                                elseif ($key == 'email')
                                    $cellValue = 'Email Address';
                                elseif ($key == 'contactNumber')
                                    $cellValue = 'Contact Number';
                                elseif ($key == 'company')
                                    $cellValue = 'Company';
                                elseif ($key == 'dateOfBirth')
                                    $cellValue = 'Date Of Birth';
                                elseif ($key == 'cityName')
                                    $cellValue = 'City';
                                elseif ($key == 'stateName')
                                    $cellValue = 'State';
                                elseif ($key == 'countryName')
                                    $cellValue = 'Country';
                                elseif ($key == 'status')
                                    $cellValue = 'User Status';
                                elseif ($key == 'createdOn')
                                    $cellValue = 'Registered On';
                                $headers[$key] = $cellValue;
                            }
                        }
                    }
                }

                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {

                            if ($key == 'status')
                                $value = !empty($value) ? 'Active' : 'Inactive';
                            if ($key == 'dateOfBirth')
                                $value = \Carbon\Carbon::parse($value)->format('Y-m-d');
                            if($key == 'contactNumber')
                                $value = preg_replace("/[^a-zA-Z0-9_ -]/s"," ",$value);

                            $userData[$count + 1][$headers[$key]] = preg_replace("/[^a-zA-Z0-9_ -@.]/s"," ",$value);
                        }
                    }
                }
            }

            ob_end_clean();
            ob_start();
            Excel::create("Users-" . \Carbon\Carbon::now(), function($excel) use($userData) {
                $excel->sheet('Sheet 1', function($sheet) use($userData) {
                    $sheet->fromArray($userData);
                });
            })->export('xlsx');
            ob_flush();


            return \Redirect::to('administrator/users/')->with('successMessage', 'Excel file created and downloaded');
        }
    }

    /**
     * Method used to Export selected with selected fields
     * @param integer $page
     * @return type
     */
    public function exportselected($page, Request $request) {
        /* if (count($request->selectall) == 0) {
          return \Redirect::to('administrator/users/')->with('errorMessage', 'Select atleast one field during the time of export');
          } */
        if (\Request::isMethod('post')) {

            $sortField = \Session::get('CENTERDATA.field');
            $sortType = \Session::get('CENTERDATA.type');
            $searchByCreatedOn = \Session::get('CENTERDATA.searchByCreatedOn');
            $searchByDate = \Session::get('CENTERDATA.searchByDate');
            $searchBycenterName = \Session::get('CENTERDATA.searchBycenterName');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchBycenterName'] = !empty($searchBycenterName) ? $searchBycenterName[0] : '';

            $recordList = Center::getCenterList($param, 'export')->toArray();

            $userData = array();
            if (!empty($recordList)) {
                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {
                            if ($count == 0) {

                                if ($key == 'unit')
                                    $cellValue = 'Unit';
                                if ($key == 'title')
                                    $cellValue = 'Title';
                                elseif ($key == 'firstName')
                                    $cellValue = 'First Name';
                                elseif ($key == 'lastName')
                                    $cellValue = 'Last Name';
                                elseif ($key == 'email')
                                    $cellValue = 'Email Address';
                                elseif ($key == 'contactNumber')
                                    $cellValue = 'Contact Number';
                                elseif ($key == 'company')
                                    $cellValue = 'Company';
                                elseif ($key == 'dateOfBirth')
                                    $cellValue = 'Date Of Birth';
                                elseif ($key == 'cityName')
                                    $cellValue = 'City';
                                elseif ($key == 'stateName')
                                    $cellValue = 'State';
                                elseif ($key == 'countryName')
                                    $cellValue = 'Country';
                                elseif ($key == 'status')
                                    $cellValue = 'User Status';
                                elseif ($key == 'createdOn')
                                    $cellValue = 'Registered On';

                                $headers[$key] = $cellValue;
                            }
                        }
                    }
                }

                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {

                            if ($key == 'status')
                                $value = !empty($value) ? 'Active' : 'Inactive';
                            if ($key == 'dateOfBirth')
                                $value = \Carbon\Carbon::parse($value)->format('Y-m-d');

                            $userData[$count + 1][$headers[$key]] = $value;
                        }
                    }
                }
            }

            $excelName = "Center-" . \Carbon\Carbon::now();
            $path = public_path('export/' . $excelName);

            ob_end_clean();
            ob_start();
            Excel::create($excelName, function($excel) use($userData) {
                $excel->sheet('Sheet 1', function($sheet) use($userData) {
                    $sheet->fromArray($userData);
                });
            })->store('xlsx', public_path('exports'));
            ob_flush();
            $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
            return response()->json(['path' => $creatingPath]);
        }
    }

    /**
     * Method for changing user pasword
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function changepassword($id = '0', $page = '', Request $request) {
        $data = array();
        $data['page'] = !empty($page) ? $page : '1';
        $data['pageTitle'] = "Change Center Passowrd";
        $data['id'] = $id;

        if (\Request::isMethod('post')) {

            $center = new Center;

            /* PASSWORD PATTERN MATCHIN */
            if ($this->is_valid_password($request->password) == false) {
                $err = 'Password must contains minimum 8 characters, atleast one digit, one special character, one uppercase letter';
                return redirect()->back()->withInput(\Input::all())->with('errorMessage', 'Password must contains minimum 8 characters, atleast one digit, one special character, one uppercase letter');
            }

            $validator = Validator::make($request->all(), [
                        'password' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            } else {
                if (!empty($id)) {
                    $center = User::find($id);
                    $center->password = Hash::make($request->password);
                    $center->modifiedBy = Auth::center()->id;
                    $center->modifiedOn = Config::get('constants.CURRENTDATE');

                    $center->save();

                    return \Redirect::to('administrator/center/?page=' . $page)->with('successMessage', 'User password changed successfully.');
                }
            }
        }

        return view('Administrator.center.changepassword', $data);
    }

    /**
     * Method for sending center notification
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function sendnotification($id = '0', $page = '', Request $request) {
        $data = array();
        $data['page'] = !empty($page) ? $page : '1';
        $data['pageTitle'] = "Send Notification";
        $data['id'] = $id;
        $data['user'] = User::find($id);

        if (\Request::isMethod('post')) {
            $userNotification = new Usernotification;

            $validator = Validator::make($request->all(), [
                        'email' => 'required',
                        'message' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            } else {
                if (!empty($id)) {
                    /* ++++++++++ email functionality ++++++++ */
                    $emailTemplate = Emailtemplate::where('templateKey', 'notification_admin')->first();
                    $replace['[NAME]'] = $data['user']->firstName . " " . $data['user']->lastName;
                    $replace['[NOTIFICATION]'] = html_entity_decode($request->message);
                    $to = $data['user']->email;
                    customhelper::SendMail($emailTemplate, $replace, $to);
                    /* ++++++++++ end of email functionality ++++ */

                    /* INSERT DATA INTO NOTIFICATION TABLE */
                    customhelper::insertUserNotification($id, Auth::user()->id, $request->message, $type = 1, $emailTemplate->templateSubject);

                    return \Redirect::to('administrator/users/?page=' . $page)->with('successMessage', 'Notification sent successfully.');
                }
            }
        }

        return view('Administrator.users.sendnotification', $data);
    }

    public function notificationhistory($id = '0', $userpage = 1, $page = '', Request $request) {
        $data = array();
        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('USERNOTIFICATIONDATA');
            \Session::push('USERNOTIFICATIONDATA.searchDisplay', $searchDisplay);

            \Session::push('USERNOTIFICATIONDATA.field', $field);
            \Session::push('USERNOTIFICATIONDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('USERNOTIFICATIONDATA.field');
            $sortType = \Session::get('USERNOTIFICATIONDATA.type');
            $searchDisplay = \Session::get('USERNOTIFICATIONDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'sentOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'sentOn' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        $param['userId'] = $id;

        /* FETCH USER LIST  */
        $userNotificationData = Usernotification::getNotificationList($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Notification History";
        $data['contentTop'] = array('breadcrumbText' => 'Notification History', 'contentTitle' => 'Users', 'pageInfo' => 'This section allows you to view notification history ');
        $data['pageTitle'] = "View Notifications";
        $data['page'] = $userNotificationData->currentPage();
        $data['userNotificationData'] = $userNotificationData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['userpage'] = $userpage;

        return view('Administrator.users.notificationhistory', $data);
    }

    public function viewnotification($id = 0, $page = 0) {
        $data['pageTitle'] = "View Notification";

        $data['userNotificationData'] = Usernotification::find($id);

        return view('Administrator.users.viewnotification', $data);
    }

    public function searchuser($searchVal) {

        $data = array();
        $userList = User::searchBasic($searchVal);
        $data['userList'] = $userList;
        return view('Administrator.users.userautocomplete', $data);
    }

    public function getusershipmentsettings($userId, $page) {

        $data = array();
        $userSettingsInfo = array();
        $userSettingsData = \App\Model\Usershipmentsettings::where('userId', $userId)->get();
        if (count($userSettingsData) > 0) {
            foreach ($userSettingsData as $eachData) {
                $userSettingsInfo[$eachData->fieldType] = $eachData->fieldValue;
            }
        }
        $userSettingsHistory = \App\Model\Usershipmentsettingshistory::where('modifiedBy', $userId)->get();
        $userSettingsFields = array(
            'remove_shoe_box' => 'Always remove shoe boxes. (Select Yes to save money)',
            'original_box' => 'Ship all my items in their original boxes (Select No to save money)',
            'quick_shipout' => 'Quick ship my items. (For subscribed customers shipping via DHL Priority)',
//            'original_box_all' => 'Ship all electronics in the original box (If Yes, we will ship such items in the box they came in)',
//            'take_photo' => 'Take pictures of my items before shipping (If Yes, items are inventoried in the US/UK and a $9.99 charge applies)',
//            'scan_invoice' => 'Scan all my invoices/receipts (If Yes, a $4.99 charge per delivery invoice applies)',
//            'item_inventory' => 'Inventory my items in the US/UK (If Yes, a $7.99 charge per delivery is added)'
        );

        $data['userSettingsInfo'] = $userSettingsInfo;
        $data['userSettingsHistory'] = $userSettingsHistory;
        $data['userSettingsFields'] = $userSettingsFields;


        return view('Administrator.users.usershipmentsettings', $data);
    }

    /**
     * Method used to delete uploaded documents
     * @param integer $id
     * @return type
     */
    public function documentdelete($id) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (User::deleteDocument($id, $createrModifierId)) {

                /* FILE DELETE */

                $findFile = Document::find($id);
                $path = public_path() . "/uploads/documents/" . $findFile->userId;
                if (file_exists($path . "/" . $findFile->filename)) {
                    if (!unlink($path . "/" . $findFile->filename)) {
                        return \Redirect::to('administrator/users/?page=' . $page)->with('successMessage', 'Document deleted');
                    } else {
                        return \Redirect::to('administrator/users/?page=' . $page)->with('successMessage', 'Document deleted successfully.');
                    }
                }

                return \Redirect::to('administrator/users/?page=' . $page)->with('successMessage', 'Document deleted successfully.');
            } else {
                return \Redirect::to('administrator/users/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/users/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /* Method to verify user from admin and send mail to user
      Updated on 27-11-2018
     */

    public function verifyuser($id, $page) {
        $data = array();

        $data['page'] = !empty($page) ? $page : '1';

        $user = User::where('id', $id)->first();

        if (count($user) > 0) {
            User::where('id', $id)->update(['status' => '1', 'activationToken' => NULL, 'verifiedCode' => NULL]);

            /* ++++++++++ email functionality ++++++++ */

            $emailTemplate = Emailtemplate::where('templateKey', 'successfull_activation')->first();
            $replace['[NAME]'] = $user->firstName . " " . $user->lastName;
            $to = $user->email;
            $isSend = customhelper::SendMail($emailTemplate, $replace, $to);

            /* ++++++++++ end of email functionality ++++ */


            if ($isSend) {
                return \Redirect::to('administrator/users/?page=' . $page)->with('successMessage', 'Customer verified successfully.');
            }
        }
    }

    /* Method to resend activation mail to user
      Updated on 27-11-2018
     */

    public function resendActivationMail(Request $request) {
        $data = array();

        $userId = $request->userId;
        $page = $request->page;

        $user = User::where('id', $userId)->first();

        if (count($user) > 0) {
            $activationToken = strtoupper(md5(uniqid(rand())));

            User::where('id', $userId)->update(['activationToken' => $activationToken]);

            $emailTemplate = Emailtemplate::where('templateKey', 'registration_active')->first();

            $to = $user->email;
            $replace['[NAME]'] = $user->firstName . ' ' . $user->lastName;
            $activationLink = Config::get('constants.frontendUrl') . 'join/' . $userId . '/' . $activationToken;

            //$replace['[ACTIVATION_LINK]'] = '<a href="' . $activationLink . '">Activate</a>';
            $replace['[ACTIVATION_LINK]'] = "<a href=" . $activationLink . ">Verify</a>";

            $isSend = customhelper::SendMail($emailTemplate, $replace, $to);

            if ($isSend) {
                return response()->json(['path' => 'users/?page=' . $page]);
            }
        }
    }

    ///////////////////////////////////////////////Assign Account Manager/////////////////////////////////////////////////


    public function assignrepresentative($id = '0', $page = '') {

        $data = array();

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Assign Account Manager";
        $data['contentTop'] = array('breadcrumbText' => 'Assign Account Manager', 'contentTitle' => 'Assign Account Manager', 'pageInfo' => 'This section allows you to assign account manager');
        $data['page'] = !empty($page) ? $page : '1';

        $data['customerid'] = $id;
        $userData = User::find($id);
        $repuser = \App\Model\UserAdmin::where('userType', '11')->get();
        $data['repuser'] = $repuser;
        $data['representativeId'] = $userData->salesRepresentativeId;

        $data['action'] = 'Edit';
        $data['pageTitle'] = "Edit User";

        return view('Administrator.users.assignrepresentative', $data);
    }

    public function saverepresentative($customerId, $representativeId) {
        $data = array();

        $user = User::find($customerId);

        $user->salesRepresentativeId = $representativeId;
        
        if ($user->save()) {
            
            $accountManagerDetails = \App\Model\UserAdmin::find($representativeId);
            
            /* Mail to customer to notify manager assignment */
            $emailTemplate = Emailtemplate::where('templateKey', 'account_manager_assignment_notification_user')->first();

            $to = $user->email;
            $replace['[NAME]'] = $user->firstName . ' ' . $user->lastName;
            $replace['[EMAIL]'] = $accountManagerDetails->email;
            $replace['[CONTACT_NUMBER]'] = $accountManagerDetails->contactno;

            $isSend = customhelper::SendMail($emailTemplate, $replace, $to);
            
            /* Mail to account manager */
            $emailTemplate = Emailtemplate::where('templateKey', 'account_manager_assignment_notification')->first();

            $to = $accountManagerDetails->email;
            $replace['[NAME]'] = $user->firstName . ' ' . $user->lastName;
            $replace['[EMAIL]'] = $user->email;
            $replace['[ADMIN_USER]'] = $accountManagerDetails->firstName.' '.$accountManagerDetails->lastName;

            $isSend = customhelper::SendMail($emailTemplate, $replace, $to);
            
            return response()->json(['status' => '1', 'message' => 'Account Manager added!']);
        } else {
            return response()->json(['status' => '0', 'message' => 'Error in operation!']);
        }
    }

    ///////////////////////////////////////////////Assign Account Manager/////////////////////////////////////////////////
    
    public function confirmsubscrition($subscriptionId = '-1',$page='1') {
        
        if($subscriptionId!='-1') {
            
            $defaultCurrencyCode = \App\Helpers\customhelper::getCurrencySymbolCode('', true);


            
            $userSubscription = \App\Model\Usersubscription::find($subscriptionId);
            $userSubscription->status = 'active';
            $userSubscription->paymentStatus = 'paid';
            $userSubscription->save();
            
            $user = User::where('id',$userSubscription->userId)->first();
            $user->isSubscribed = 'Y';
            $user->save();

            $userAddress = \App\Model\Addressbook::where('isDefaultBilling', 1)->where('userId', $user->id)->first();

            $paymentransaction = \App\Model\Paymenttransaction::where('userId', $userSubscription->userId)->where('paidFor', 'Subscription')->orderBy('id', 'desc')->first();
            if(!empty($paymentransaction))
            {
               \App\Model\Paymenttransaction::where("id", $paymentransaction->id)->update(["status" => "paid"]);
            }
             
            $invoiceDetails = \App\Model\Invoice::where('shipmentId', $subscriptionId)->where('type','Subscription')->where('deleted', '0')->first();

          
            ///Generation of Invoice for subscrition payment///

            if(!empty($invoiceDetails))
            {
                $invoiceParticulars = json_decode($invoiceDetails->invoiceParticulars, true);
            }

            $invoiceUniqueId = 'SUBSCRIPTION-REC-' . $user->unit . '-' . $userSubscription->id . '-' . date('Ymd');
            $newInvoice =new \App\Model\Invoice;
            $newInvoice = $invoiceDetails->replicate();
            $newInvoice->invoiceUniqueId = $invoiceUniqueId;
            $newInvoice->invoiceParticulars = json_encode($invoiceParticulars);
            $newInvoice->totalBillingAmount = $userSubscription->paidAmount;
            $newInvoice->invoiceType = 'receipt';
            $newInvoice->paymentStatus = 'paid';
            $newInvoice->save();

            $invoiceId = $newInvoice->id;


            if(!empty($invoiceId))
            {
                $fileName = "Subscription_" . $invoiceUniqueId . ".pdf";
                $data['invoice'] = \App\Model\Invoice::find($invoiceId); 
                 $data['invoiceType'] = Ucfirst($newInvoice->invoiceType);
                 $data['pageTitle'] = "Print Invoice";

                PDF::loadView('Administrator.users.subscriptioninvoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream($fileName);
                $filepath = public_path('exports/invoice/' . $fileName);
                $to = $user->email;
                $replace['[NAME]'] = $user->firstName . ' ' . $user->lastName;
                $replace['[DURATION]'] = (($userSubscription->subscribedFor>1)?$userSubscription->subscribedFor.' Months':$userSubscription->subscribedFor.' Month');
                if (isset($userSubscription['amount'])) {
                    $replace['[AMOUNT]'] = $defaultCurrencyCode . " " . $userSubscription->paidAmount;
                }

                $replace['[DATE]'] = Carbon::now()->addMonths($userSubscription->subscribedFor + 1);
                
                $emailTemplate = Emailtemplate::where('templateKey', 'subscription_payment_success')->first();

                customhelper::SendMailWithAttachment($emailTemplate, $replace, $to, $filepath);
                
                return redirect()->back()->with('success', ['Payment Status Updated Successfully']);

            }
            
        }
        
    }


    public function getsubscriptions(Request $request)
    {
        $userSubscription = array();
        $listIds = explode('^', $request->listIds);

        foreach($listIds as $listId)
        {
           $userSubscription[] = \App\Model\Usersubscription::select('status','subscribedFor','subscribedOn','expiryDate','userId')->whereRaw("userId =".$listId."")->orderBy('id', 'desc')->first();

        }
         

        if(empty($userSubscription))
        {
            return json_encode(array());
        }else{
            return json_encode($userSubscription);
        }
        
    }
}
