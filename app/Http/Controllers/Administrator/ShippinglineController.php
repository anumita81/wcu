<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Shippingline;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;

class ShippinglineController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Shippingline'), Auth::user()->id); // call the helper function
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('SHIPPINGLINEDATA');
            \Session::push('SHIPPINGLINEDATA.searchDisplay', $searchDisplay);
            \Session::push('SHIPPINGLINEDATA.field', $field);
            \Session::push('SHIPPINGLINEDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByMake'] = $searchByMake;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('SHIPPINGLINEDATA.field');
            $sortType = \Session::get('SHIPPINGLINEDATA.type');
            $searchDisplay = \Session::get('SHIPPINGLINEDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'displayOrder';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchByMake'] = !empty($searchByMake) ? $searchByMake[0] : '';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'line' => array('current' => 'sorting'),
            'displayOrder' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH model LIST  */
        $shippingLineData = Shippingline::getList($param);

        $data['shippingLineData'] = $shippingLineData;
        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Shipping Lines";
        $data['contentTop'] = array('breadcrumbText' => 'Shipping Lines', 'contentTitle' => 'Shipping Lines', 'pageInfo' => 'This section allows you to manage different shipping lines');
        $data['pageTitle'] = "Shipping Lines";
        $data['page'] = $shippingLineData->currentPage();
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavAuto', 'menuSub' => '', 'menuSubSub' => 'leftNavShippingLines9');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.shippingline.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Shippingline'), Auth::user()->id); // call the helper function
        $data = array();
        $data['title'] = "Administrative Panel :: Shipping Lines";
        $data['contentTop'] = array('breadcrumbText' => 'Shipping Lines', 'contentTitle' => 'Shipping Lines', 'pageInfo' => 'This section allows you to manage different shipping lines');
        $data['page'] = !empty($page) ? $page : '1';

        if (!empty($id)) {
            if ($findRole['canEdit'] == 0) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['pageTitle'] = "Edit Shipping Line";
            $data['id'] = $id;
            $data['page'] = $page;
            $data['action'] = 'Edit';
            $data['shippingLine'] = Shippingline::find($id);
        } else {
            if ($findRole['canAdd'] == 0) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add New  Shipping Line";
            $data['page'] = $page;
        }
        return view('Administrator.shippingline.addedit', $data);
    }

    /**
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Shippingline'), Auth::user()->id); // call the helper function
        $data['page'] = !empty($page) ? $page : '1';
        $shippingLine = new Shippingline;

        if (!empty($id)) {
            $validator = Validator::make($request->all(), [
                        'line' => 'required|unique:' . $shippingLine->table . ',line,' . $id,
                        'fileName' => 'file|mimes:jpg,jpeg,png,gif,csv,xls,xlsx,doc,pdf,docx'
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                        'line' => 'required|unique:' . $shippingLine->table . ',line,' . $id,
                        'fileName' => 'required|file|mimes:jpg,jpeg,png,gif,csv,xls,xlsx,doc,pdf,docx'
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if ($id != 0) { // Edit
                if ($findRole['canEdit'] == 0) {
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
                $shippingLine = Shippingline::find($id);
                $shippingLine->modifiedBy = Auth::user()->id;
                $shippingLine->modifiedOn = Config::get('constants.CURRENTDATE');
            } else { // Add
                if ($findRole['canAdd'] == 0) {
                    return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }

                $shippingLine->createdBy = Auth::user()->id;
                $shippingLine->createdOn = Config::get('constants.CURRENTDATE');
            }
            $shippingLine->line = $request->line;

            if ($request->hasFile('fileName')) {
                $fileName = $request->file('fileName');
                $name = time() . '_' . $fileName->getClientOriginalName();
                $destinationPath = public_path('/uploads/shippingline');
                $fileName->move($destinationPath, $name);
                $shippingLine->fileName = $name;
            }

            $shippingLine->save();
            $shippingLineId = $shippingLine->id;

            if (empty($id)) {
                $shippingLine = Shippingline::find($shippingLineId);
                $shippingLine->displayOrder = $shippingLineId;
                $shippingLine->save();
            }

            return redirect('/administrator/shippingline?page=' . $page)->with('successMessage', 'Shipping line information saved successfuly.');
        }
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Shippingline'), Auth::user()->id); // call the helper function
        if ($findRole['canDelete'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Shippingline::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/shippingline/?page=' . $page)->with('successMessage', 'Shipping Line deleted successfully.');
            } else {
                return \Redirect::to('administrator/shippingline/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/shippingline/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
