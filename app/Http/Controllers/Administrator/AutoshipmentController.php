<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Autoshipment;
use App\Model\Autoshipmentitem;
use App\Model\Autoshipmentitemimage;
use App\Model\Warehouse;
use App\Model\Deliverycompany;
use App\Model\Dispatchcompany;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\User;
use App\Model\Autowebsite;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use Response;
use PDF;
use Mail;
use customhelper;

class AutoshipmentController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Autoshipment'), Auth::user()->id); // call the helper function
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        /* BUILD DEFAULT SEARCH ARRAY */
        $searchAutoshipmentArr = array(
            'unit' => '',
            'user' => '',
            'shipment' => '',
            'shipmentStatus' => '',
            'useremail' => '',
            'vinNumber' => '',
            'make' => '',
            'model' => '',
            'year' => '',
            'color' => '',
            'carCondition' => '',
        );
        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchAutoshipment = \Input::get('searchAutoshipment', $searchAutoshipmentArr);
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('AUTOSHIPMENTDATA');
            \Session::push('AUTOSHIPMENTDATA.searchDisplay', $searchDisplay);
            \Session::push('AUTOSHIPMENTDATA.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('AUTOSHIPMENTDATA.searchByDate', $searchByDate);
            \Session::push('AUTOSHIPMENTDATA.searchAutoshipment', $searchAutoshipment);
            \Session::push('AUTOSHIPMENTDATA.field', $field);
            \Session::push('AUTOSHIPMENTDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchAutoshipment'] = $searchAutoshipment;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('AUTOSHIPMENTDATA.field');
            $sortType = \Session::get('AUTOSHIPMENTDATA.type');
            $searchByCreatedOn = \Session::get('AUTOSHIPMENTDATA.searchByCreatedOn');
            $searchByDate = \Session::get('AUTOSHIPMENTDATA.searchByDate');
            $searchAutoshipment = \Session::get('AUTOSHIPMENTDATA.searchAutoshipment');
            $searchDisplay = \Session::get('AUTOSHIPMENTDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchAutoshipment'] = !empty($searchAutoshipment) ? $searchAutoshipment[0] : $searchAutoshipmentArr;
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'id' => array('current' => 'sorting'),
            'shipmentCost' => array('current' => 'sorting'),
            'pickupCost' => array('current' => 'sorting'),
            'insuranceCost' => array('current' => 'sorting'),
            'totalCost' => array('current' => 'sorting'),
            'shipmentStatus' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTRY LIST  */
        $autoshipmentData = Autoshipment::getAutoshipmentList($param);

        /* Get auto shipment status list */
        $autoshipmentStatus = Autoshipment::allStatus();

        /* Fetch Make List */
        $data['makeList'] = \App\Model\Automake::where('deleted', '0')->get();
        if (!empty($param['searchAutoshipment']['make'])) {
            $data['modelList'] = \App\Model\Automodel::where('makeId', $param['searchAutoshipment']['make'])->where('deleted', '0')->get();
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Autoshipment";
        $data['contentTop'] = array('breadcrumbText' => 'Autoshipment', 'contentTitle' => 'Autoshipment', 'pageInfo' => 'This section allows you to manage auto shipments');
        $data['pageTitle'] = "Autoshipment";
        $data['page'] = $autoshipmentData->currentPage();
        $data['autoshipmentData'] = $autoshipmentData;
        $data['autoshipmentStatus'] = $autoshipmentStatus;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.autoshipment.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['pageTitle'] = 'Create New Autoshipment';
        $data['title'] = "Administrative Panel :: Autoshipment";
        $data['contentTop'] = array('breadcrumbText' => 'Autoshipments', 'contentTitle' => 'Autoshipments', 'pageInfo' => 'This section allows you to create new auto shipments');
        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = $id;
        //$data['action'] = 'Edit';
//        $autoshipment = Autoshipment::find($id);
//        $data['shipmentData'] = $autoshipment;

        /* FETCH AUTO MAKE LIST  */
        $data['autoMakeList'] = \App\Model\Automake::where('type', '0')->where('deleted', '0')->orderby('name', 'asc')->get();

        $data['pickupContryList'] = \App\Model\Country::where('status', '1')->where('code', 'US')->orderby('name', 'asc')->get();
        $data['destinationCountryList'] = \App\Model\Country::where('status', '1')->where('code', 'NG')->orderby('name', 'asc')->get();

        $data['locationType'] = \App\Model\Locationtype::where('status', '1')->where('deleted', '0')->get();

        // $data['countryList'] = \App\Model\Country::where('deleted', '0')->orderby('name', 'asc')->get();

        return view('Administrator.autoshipment.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        //print_r($request->all());exit;
        $data = array();
        $carImage = $titleName = "";
        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $autoshipment = new Autoshipment;

        /* Fetch User Details */
        //$userData = User::where('unit', $request->userUnit)->first();
        //$userId = $userData->id;       


        $validator = Validator::make($request->all(), [
                    'makeId' => 'required',
                    'modelId' => 'required',
                    'year' => 'required',
                    'itemPrice' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            //DB::beginTransaction();

            $userData = User::where("unit", $request->userUnit)->first();

            if ($request->hasFile('uploadCarImage')) {
                $uploadFile = $request->file('uploadCarImage');
                $carImage = time() . '_' . $uploadFile->getClientOriginalName();
                $destinationPath = public_path('/uploads/auto/carpickup/' . $userData->id);
                $uploadFile->move($destinationPath, $carImage);
            }

            if ($request->hasFile('uploadCarTitle')) {
                $uploadTitle = $request->file('uploadCarTitle');
                $titleName = time() . '_' . $uploadTitle->getClientOriginalName();
                $destinationPathTitle = public_path('/uploads/auto/carpickup/' . $userData->id);
                $uploadTitle->move($destinationPathTitle, $titleName);
            }



            $shippingData = array();
            $shippingData['userId'] = $userData->id;
            $shippingData['warehouseId'] = $request->warehouseId;
            $shippingData['itemPrice'] = $request->itemPrice;
            $shippingData['pickupCountry'] = $request->pickupCountry;
            $shippingData['pickupState'] = $request->pickupState;
            $shippingData['pickupCity'] = $request->pickupCity;
            $shippingData['destinationCountry'] = $request->destinationCountry;
            $shippingData['destinationState'] = $request->destinationState;
            $shippingData['destinationCity'] = $request->destinationCity;
            $shippingData['makeId'] = $request->makeId;
            $shippingData['modelId'] = $request->modelId;

            $shippingChargesDetails = Autoshipment::getshipmycarallcharges($shippingData);

            //print_r($shippingChargesDetails);exit;

            $autoShipment = new Autoshipment;
            $autoShipment->userId = $userData->id;
            $autoShipment->warehouseId = $request->warehouseId;
            $autoShipment->pickupAddress = $request->pickupAddress;
            $autoShipment->pickupCountry = $request->pickupCountry;
            $autoShipment->pickupState = $request->pickupState;
            $autoShipment->pickupCity = $request->pickupCity;
            $autoShipment->pickupPhone = $request->pickupPhone;
            $autoShipment->pickupEmail = $request->pickupEmail;
            $autoShipment->pickupLocationType = $request->pickupLocationType;
            $autoShipment->destinationCountry = $request->destinationCountry;
            $autoShipment->destinationState = $request->destinationState;
            $autoShipment->destinationCity = $request->destinationCity;
            $autoShipment->receiverAddress = $request->receiverAddress;
            $autoShipment->recceiverName = $request->recceiverName;
            $autoShipment->receiverEmail = $request->receiverEmail;
            $autoShipment->receiverPhone = $request->receiverPhone;
            $autoShipment->itemPrice = $request->itemPrice;
            $autoShipment->shipmentStatus = 'submitted';
            $autoShipment->paymentStatus = 'unpaid';
            $autoShipment->websiteLink = $request->directUrl;
            $autoShipment->makeId = $request->makeId;
            $autoShipment->modelId = $request->modelId;
            $autoShipment->year = $request->year;
            $autoShipment->color = $request->color;
            $autoShipment->milage = $request->millege;
            $autoShipment->vinNumber = $request->vin;
            $autoShipment->carCondition = $request->carCondition;
            $autoShipment->pickupCost = $shippingChargesDetails['pickupCost'];
            $autoShipment->insuranceCost = $shippingChargesDetails['insuranceCost'];
            $autoShipment->shipmentCost = $shippingChargesDetails['shippingCost'];
            $autoShipment->isCurrencyChanged = 'N';
            $autoShipment->defaultCurrencyCode = $shippingChargesDetails['defaultCurrency'];
            $autoShipment->paidCurrencyCode = $shippingChargesDetails['currencyCode'];
            $autoShipment->exchangeRate = $shippingChargesDetails['exchangeRate'];
            $autoShipment->taxCost = '0.00';
            $autoShipment->totalCost = ($shippingChargesDetails['pickupCost'] + $shippingChargesDetails['insuranceCost'] + $shippingChargesDetails['shippingCost']);
            $autoShipment->createdBy = Auth::user()->id;
            $autoShipment->createdByType = 'admin';
            $autoShipment->createdOn = Config::get('constants.CURRENTDATE');
            $autoShipment->pickupDate = Config::get('constants.CURRENTDATE');
            $autoShipment->save();
            $autoShipmentId = $autoShipment->id;

            $autoshipmentStatusLog = new \App\Model\Autoshipmentstatuslog;
            $autoshipmentStatusLog->autoshipmentId = $autoShipmentId;
            $autoshipmentStatusLog->oldStatus = 'none';
            $autoshipmentStatusLog->status = 'submitted';
            $autoshipmentStatusLog->updatedOn = Config::get('constants.CURRENTDATE');
            $autoshipmentStatusLog->save();

            if ($carImage != '') {
                $autoshipmentImage = new Autoshipmentitemimage;
                $autoshipmentImage->autoShipmentId = $autoShipmentId;
                $autoshipmentImage->imageType = 'carpicture';
                $autoshipmentImage->filename = $carImage;
                $autoshipmentImage->status = '1';
                $autoshipmentImage->createdBy = Auth::user()->id;
                $autoshipmentImage->createdByType = 'admin';
                $autoshipmentImage->createdOn = Config::get('constants.CURRENTDATE');
                $autoshipmentImage->save();
            }

            if ($titleName != '') {
                $autoshipmentImage = new Autoshipmentitemimage;
                $autoshipmentImage->autoShipmentId = $autoShipmentId;
                $autoshipmentImage->imageType = 'cartitle';
                $autoshipmentImage->filename = $titleName;
                $autoshipmentImage->status = '1';
                $autoshipmentImage->createdBy = Auth::user()->id;
                $autoshipmentImage->createdByType = 'admin';
                $autoshipmentImage->createdOn = Config::get('constants.CURRENTDATE');
                $autoshipmentImage->save();
            }
            $to = $userData->email;
            $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'create_auto_shipment')->first();
            $replace['[NAME]'] = $userData->firstName . ' ' . $userData->lastName;
            $replace['[MAKE]'] = \App\Model\Automake::find($request->makeId)->name;
            $replace['[MODEL]'] = \App\Model\Automodel::find($request->modelId)->name;
            $replace['[YEAR]'] = $request->year;

            \App\Helpers\customhelper::SendMail($emailTemplate, $replace, $to);


            if (!empty($autoShipmentId)) {
                return redirect('/administrator/autoshipment')->with('successMessage', 'Autoshipment saved successfuly.');
            } else {

                return redirect('/administrator/autoshipment')->with('errorMessage', 'Autoshipment not saved.');
            }
        }
    }

    /**
     * Method used to change status
     * @param integer $id
     * @param integer $page
     * @param integer $status
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Autoshipment::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/autoshipment/?page=' . $page)->with('successMessage', 'Auto Shipment changed successfully.');
            } else {
                return \Redirect::to('administrator/autoshipment/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/autoshipment/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Autoshipment::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/autoshipment/?page=' . $page)->with('successMessage', 'Auto Shipment deleted successfully.');
            } else {
                return \Redirect::to('administrator/autoshipment/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/autoshipment/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    public function getstates($country) {
        $countryData = Country::find($country);
        $stateList = array();
        if (isset($countryData->code) && !empty($countryData->code)) {
            $stateList = State::where('status', '1')
                    ->where('deleted', '0')
                    ->where('countryCode', $countryData->code)
                    ->orderby('name', 'asc')
                    ->get();
        }
        echo json_encode($stateList);
        exit;
    }

    /**
     * Method for fetching city list
     * @param $stateId
     * @return string
     */
    public function getcities($state) {
        $state = State::find($state);
        $cityList = array();
        if (isset($state->code) && !empty($state->code)) {
            $cityList = City::where('status', '1')
                    ->where('deleted', '0')
                    ->where('stateCode', $state->code)
                    ->where('countryCode', $state->countryCode)
                    ->orderby('name', 'asc')
                    ->get();
        }
        echo json_encode($cityList);
        exit;
    }

    /**
     * Method for fetching Car Make list
     * @param $makeId
     * @return string
     */
    public function getCarMake($make) {
        $modelList = array();

        $modelList = \App\Model\Automodel::where('deleted', '0')->where('makeType', '0')
                ->where('makeId', $make)
                ->orderby('name', 'asc')
                ->get();

        echo json_encode($modelList);
        exit;
    }

    /**
     * Method for user unit verification
     * @return string
     */
    public function verifyuser($unit = '') {

        if (!empty($unit)) {
            $userExist = \App\Model\User::where('uniqueId', $unit)->get();


            if (!empty($userExist) && count($userExist) > 0) {
                $data['user'] = \App\Model\User::where('uniqueId', $unit)->first();

                return view('Administrator.autoshipment.userverification', $data);
            } else {
                return 0;
            }
        }
    }

    /**
     * Method used to clear search history
     * @return type
     */
    public function showall() {
        \Session::forget('AUTOSHIPMENTDATA');
        return \Redirect::to('administrator/autoshipment');
    }

    public function deleteSelected(Request $request) {

        if (isset($request->checkedval) && $request->checkedval != '') {
            $idsToDelete = explode('^', $request->checkedval);
            foreach ($idsToDelete as $eachId) {
                $autoShipment = Autoshipment::find($eachId);
                $autoShipment->deleted = '1';
                $autoShipment->deletedOn = Config::get('constants.CURRENTDATE');
                $autoShipment->deletedBy = Auth::user()->id;
                $autoShipment->save();
            }

            return \Redirect::to('administrator/autoshipment')->with('successMessage', 'Auto Shipment deleted successfully.');
        }

        return \Redirect::to('administrator/autoshipment')->with('errorMessage', 'Something went wrong.');
    }

    public function view($id = '0', $page = '') {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Autoshipment'), Auth::user()->id); 

        // call the helper function
        if ($findRole['canAdd'] == 0 || $findRole['canAdd'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Auto Shipments";
        $data['contentTop'] = array('breadcrumbText' => 'Auto Shipments', 'contentTitle' => 'Auto Shipments', 'pageInfo' => 'This section allows you to manage Auto Shipments details');
        $data['pageTitle'] = "Auto Shipment Details";
        $data['procurementId'] = $id;
        $data['page'] = $page;
        $data['autoShipmentData'] = \App\Model\Autoshipment::find($id)->toArray();
        $data['userUnit'] = User::find($data['autoShipmentData']['userId'])->unit;
        $data['autoShipmentImage'] = Autoshipmentitemimage::where('autoShipmentId', $id)->where('imageType', 'carpicture')->first();
        $data['autoShipmentTitle'] = Autoshipmentitemimage::where('autoShipmentId', $id)->where('imageType', 'cartitle')->first();
        /* FETCH Country LIST  */
        $countryData = collect(Country::where('status', '1')->orderby('name', 'asc')->get());
        $data['countryList'] = $countryData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        /* FETCH State LIST  */
        $stateData = collect(State::where('status', '1')->where('deleted', '0')->orderby('name', 'asc')->get());
        $data['stateList'] = $stateData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        /* FETCH City LIST  */
        $cityData = collect(City::where('status', '1')->where('deleted', '0')->orderby('name', 'asc')->get());
        $data['cityList'] = $cityData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        /* Fetch Make List */
        $makeData = collect(\App\Model\Automake::where('deleted', '0')->get());
        $data['makeList'] = $makeData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        /* Fetch Model List */
        $modelData = collect(\App\Model\Automodel::where('deleted', '0')->where('makeType', '0')->get());
        $data['modelList'] = $modelData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        /* Fetch Location Type list */
        $locationTypeData = collect(\App\Model\Locationtype::where('status', '1')->where('deleted', '0')->get());
        $data['locationTypeList'] = $locationTypeData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        /* Fetch Invoice Data */
        $data['invoices'] = \App\Model\Invoice::where('shipmentId', $id)->whereRaw("(type='autoshipment' or type='buycarforme')")->get();
        if ($data['autoShipmentData']['paymentMethodId'] != '')
            $data['paymentMethod'] = \App\Model\Paymentmethod::find($data['autoShipmentData']['paymentMethodId'])->paymentMethod;
        /* Fetch Website List */
        $data['websiteList'] = \App\Model\Autowebsite::where('deleted', '0')->get()->toArray();

        $data['allItemStatus'] = \App\Model\Autoshipment::allStatus();
        $data['allItemStatusIndexes'] = array_keys($data['allItemStatus']);
        /* Fetch Order Data */
        $data['orderData'] = \App\Model\Order::where('type', 'autoshipment')->where('shipmentId', $id)->first();
        $data['statusLog'] = \App\Model\Autoshipmentstatuslog::getOrderStatusProgress($id);
        
        $data['notifiedMessages'] = \App\Model\Autoshipmentcomment::where('shipmentId', $id)->where('type', 'S')->orderBy('id', 'desc')->get()->toArray();
        //dd($data['statusLog']);

        /*  FETCH PROCUREMENT ITEM DETAILS */
        $data['autopickupData']['items'] = \App\Model\Procurementitem::getCarDetails($id)->toArray();

        $data['uploadedImages'] = \App\Model\Autowarehouseimage::getAllImages($id);

        //echo "<pre>"; print_r($data['autopickupData']); die;



        return view('Administrator.autoshipment.view', $data);
    }

    /**
     * Method used to display notification message
     * @param integer $messageId
     * @param integer $shipmentId
     * @return string
     */
    public function notifycustomer($shipmentId, Request $request) {
        if (\Request::isMethod('post')) {

            $autopickupissues = new \App\Model\Autopickupissues;
            $autopickupissues->procurementId = $shipmentId;
            $autopickupissues->shipmentType = 'autoshipment';
            $autopickupissues->userId = $request->userId;
            $autopickupissues->message = $request->delayMessage;
            $autopickupissues->sentBy = Auth::user()->id; //
            $autopickupissues->sentOn = Config::get('constants.CURRENTDATE');

            $user = \App\Model\User::find($request->userId);
            $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'notification_admin')->first();
            //echo $emailTemplate->templateBody;exit;
            $replace['[NAME]'] = $user['firstName'];
            $replace['[NOTIFICATION]'] = $request->delayMessage;
            $replace['[SHIPMENTID]'] = $shipmentId;

            $to = $user['email'];
            $sendMail = customhelper::SendMail($emailTemplate, $replace, $to);

            if ($sendMail) {
                if ($autopickupissues->save()) {
                    return redirect()->back()->with('successMessage', 'Notification sent successfully');
                } else {
                    return redirect()->back()->with('errorMessage', 'Record not added');
                }
            } else {
                return redirect()->back()->with('errorMessage', 'Notification not sent');
            }
        }
    }

    public function updatestatus(Request $request) {

        $shipmentId = $request->shipmentId;
        $status = $request->status;
        $allStatus = Autoshipment::allStatus();
        $autoshipment = Autoshipment::find($shipmentId);
        $autoshipment->shipmentStatus = $status;
        $autoshipment->save();

        $autoshipmentLogData = \App\Model\Autoshipmentstatuslog::where('autoshipmentid', $shipmentId)->orderBy('id', 'desc')->first();

        $autoshipmentLog = new \App\Model\Autoshipmentstatuslog;
        $autoshipmentLog->autoshipmentId = $shipmentId;
        $autoshipmentLog->oldStatus = $autoshipmentLogData->status;
        $autoshipmentLog->status = $status;
        $autoshipmentLog->updatedOn = Config::get('constants.CURRENTDATE');
        $autoshipmentLog->save();

        if ($status != 'completed') {
            /* Change status notification */
            if ($status == "orderplaced") {
                $notificationTemplateKey = "ship_my_car_order_placed";
            } else if ($status == "pickedup") {
                $notificationTemplateKey = "ship_my_car_order_pickedup";
            } else if ($status == "received") {
                $notificationTemplateKey = "ship_my_car_order_received";
            } else if ($status == "customverification") {
                $notificationTemplateKey = "ship_my_car_order_custom_verification";
            } else if ($status == "sailed") {
                $notificationTemplateKey = "ship_my_car_order_sailed";
            } else if ($status == "customclearing") {
                $notificationTemplateKey = "ship_my_car_order_customs_clearing";
            } else if ($status == "availpickup") {
                $notificationTemplateKey = "ship_my_car_order_avilable_pickup";
            }

            $makeInfo = \App\Model\Automake::find($autoshipment->makeId);
            $modelInfo = \App\Model\Automodel::find($autoshipment->modelId);
            $userInfo = \App\Model\User::find($autoshipment->userId);
            $emailTemplate = \App\Model\Emailtemplate::where('templateKey', $notificationTemplateKey)->first();
            $replace['[NAME]'] = $userInfo->firstName . ' ' . $userInfo->lastName;
            $replace['[MODEL]'] = (!empty($modelInfo->name) ? $modelInfo->name : "N/A");
            $replace['[MAKE]'] = (!empty($makeInfo->name) ? $makeInfo->name : "N/A");
            $replace['[YEAR]'] = $autoshipment->year;
            $to = $userInfo->email;

            $isSend = \App\Helpers\customhelper::SendMail($emailTemplate, $replace, $to);

            $toMobile = trim($userInfo->isdCode . $userInfo->contactNumber);
            $smsTemplate = \App\Model\Smstemplate::where('templateKey', $notificationTemplateKey)->first();
//        $link = Config::get('constants.frontendUrl');
//        $replace['[WEBSITE_LINK]'] = $link;
            $isSendMsg = customhelper::sendMSG($toMobile, $replace, $smsTemplate);
            $replace['[SHIPMENT_STATUS]'] = ucfirst($status);
            \App\Model\User::sendPushNotification($autoshipment->userId, 'autoshipment_status_update', Auth::user()->id, $replace);
            /* Notification end */
        }

        if ($status == 'completed') {
            $orderData = \App\Model\Order::where('type', 'autoshipment')->where('shipmentId', $shipmentId)->first();
            if (!empty($orderData)) {
                \App\Model\Order::where('id', $orderData->id)->update(['status' => '5']);
            }
        }

        return 1;
    }

    public function downloadtitle($titleId, $userId) {

        $imageData = Autoshipmentitemimage::find($titleId);
        //$filepath = public_path('uploads/image/')."abc.jpg";
        $filepath = public_path('uploads/auto/carpickup/' . $userId . '/' . $imageData->filename);
        return Response::download($filepath);
    }

    /**
     * Method used to print invoice
     * @param integer $id
     * @param integer $invoiceId 
     * @return string
     */
    public function printinvoice($id, $invoiceId) {
        /* Fetch  Invoice Data */
        $data['invoice'] = \App\Model\Invoice::find($invoiceId);
        return view('Administrator.autoshipment.printinvoice', $data);
    }

    public function updatepaymentstatus($shipmentId, $userId, Request $request) {

        Autoshipment::where('id', $shipmentId)->update(['paymentStatus' => $request->paymentStatus, 'paymentReceivedOn' => Config::get('constants.CURRENTDATE'), 'paidByUserType'=> 'admin', 'paidByUserId' => Auth::user()->id]);

        if ($request->paymentStatus == 'paid') {

            $invoiceData = \App\Model\Invoice::where('shipmentId', $shipmentId)->where('type', 'autoshipment')->where('invoiceType', 'invoice')->first();

            if(!empty($invoiceData))
            {
               \App\Model\Invoice::where('shipmentId', $shipmentId)->update(['deleted' => '1', 'deletedBy' => Auth::user()->id, 'deletedOn' => Config::get('constants.CURRENTDATE')]);


                /*  INSERT DATA INTO INVOICE TABLE */
                $invoiceUniqueId = 'RECP' . $invoiceData->userUnit . '-' . $shipmentId . '-' . date('Ymd');
                $invoice = new \App\Model\Invoice;
                $invoice->invoiceUniqueId = $invoiceUniqueId;
                $invoice->shipmentId = $shipmentId;
                $invoice->type = 'autoshipment';
                $invoice->invoiceType = 'receipt';
                $invoice->userUnit = $invoiceData->userUnit;
                $invoice->userFullName = $invoiceData->userFullName;
                $invoice->userEmail = $invoiceData->userEmail;
                $invoice->userContactNumber = $invoiceData->userContactNumber;
                $invoice->billingName = $invoiceData->billingName;
                $invoice->billingEmail = $invoiceData->billingEmail;
                $invoice->billingAddress = $invoiceData->billingAddress;
                $invoice->billingAlternateAddress = $invoiceData->billingAlternateAddress;
                $invoice->billingCity = isset($invoiceData->billingCity) ? $invoiceData->billingCity : '';
                $invoice->billingState = isset($invoiceData->billingState) ? $invoiceData->billingState : '';
                $invoice->billingCountry = $invoiceData->billingCountry;
                $invoice->billingZipcode = $invoiceData->billingZipcode;
                $invoice->billingPhone = $invoiceData->billingPhone;
                $invoice->billingAlternatePhone = $invoiceData->billingAlternatePhone;
                $invoice->totalBillingAmount = $invoiceData->totalBillingAmount;
                $invoice->invoiceParticulars = $invoiceData->invoiceParticulars;
                $invoice->paymentMethodId = $invoiceData->paymentMethodId;
                $invoice->paymentStatus = 'paid';
                $invoice->createdOn = Config::get('constants.CURRENTDATE');
                $invoice->save();
                $invoiceId = $invoice->id;

                if (isset($invoiceId)) {
                    $data['invoice'] = \App\Model\Invoice::find($invoiceId);
                    $fileName = "Receipt_" . $invoiceUniqueId . ".pdf";
                    PDF::loadView('Administrator.autoshipment.buyacarreceipt', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                    $to = $invoiceData->userEmail;
                    Mail::send(['html' => 'mail'], ['content' => 'Payment Receipt for Auto Shipment #' . $shipmentId], function ($message) use($invoiceUniqueId, $to, $fileName) {
                        $message->from('contact@shoptomydoor.com', 'Shoptomydoor');
                        $message->subject("$invoiceUniqueId - Receipt Details");
                        $message->to($to);
                        $message->attach(public_path('exports/invoice/' . $fileName));
                    });
                }


                $paymentTransaction = \App\Model\Paymenttransaction::where('paidForId', $shipmentId)->update(['status' => 'paid']);

                }


        }

        return redirect('/administrator/autoshipment/view/' . $shipmentId . '/1')->with('successMessage', 'Payment status updated successfuly.');
    }

    public function createOrder($shipmentId, $page = 1) {

        $shipmentData = Autoshipment::find($shipmentId);

        $orderObj = new \App\Model\Order;
        $orderObj->shipmentId = $shipmentId;
        $orderObj->userId = $shipmentData->userId;
        $orderObj->totalCost = $shipmentData->totalCost;
        $orderObj->status = '2';
        $orderObj->type = 'autoshipment';
        $orderObj->createdBy = Auth::user()->id;
        $orderObj->save();
        $orderNumber = \App\Model\Order::generateOrderNumber($orderObj->id, $orderObj->userId);
        $orderObj->orderNumber = $orderNumber;
        $orderObj->save();

        return \Redirect::to('/administrator/autoshipment/view/' . $shipmentId . '/' . $page)->with('successMessage', 'Order for shipment created successfully.');
    }

    public function uploadimages($shipmentId, Request $request) {

        if ($request->hasFile('carImage')) {
            $image = $request->file('carImage');
            $imageNameTitle = time() . '_' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/auto/carimages/' . $shipmentId);
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0777);
                chmod($destinationPath, 0777);
            }
            $image->move($destinationPath, $imageNameTitle);

            $res100 = Autoshipment::createThumbnail($imageNameTitle, 100, 100, $destinationPath, public_path('/uploads/auto/carimages/thumb/'));

            $autoImage = new \App\Model\Autowarehouseimage;
            $autoImage->shipmentId = $shipmentId;
            $autoImage->imagePath = $imageNameTitle;
            $autoImage->imageSection = $request->carImageType;
            $autoImage->createdBy = Auth::user()->id;
            if ($autoImage->save()) {
                return json_encode(array('imagePath' => asset('public/uploads/auto/carimages/thumb/' . $imageNameTitle), 'imageId' => $autoImage->id));
            } else
                return json_encode(array('imageId' => '-1'));
        }
    }

    public function showwarehouseimage($imageId, $page) {

        $data['autoImage'] = \App\Model\Autowarehouseimage::find($imageId);

        return view('Administrator.autoshipment.showwarehouseimage', $data);
    }

    public function deletewarehouseimage($imageId) {

        $imagedata = \App\Model\Autowarehouseimage::find($imageId);

        if (file_exists(public_path('/uploads/auto/carimages/' . $imagedata->shipmentId . '/' . $imagedata->imagePath))) {
            unlink(public_path('/uploads/auto/carimages/' . $imagedata->shipmentId . '/' . $imagedata->imagePath));
        }

        if (file_exists(public_path('/uploads/auto/carimages/thumb/' . $imagedata->imagePath))) {
            unlink(public_path('/uploads/auto/carimages/thumb/' . $imagedata->imagePath));
        }

        if ($imagedata->delete()) {
            return json_encode(array('return' => 'success'));
        } else {
            return json_encode(array('return' => 'error'));
        }
    }


    //Added on Aug-2019

    public function createextrainvoice($shipmentId, Request $request)
    {

        Autoshipment::where('id', $shipmentId)->increment('totalCost', $request->extraCost);

        $shipmentData = Autoshipment::find($shipmentId);
        $userInfo = User::find($shipmentData->userId);
        

        $invoiceDetails = \App\Model\Invoice::where('shipmentId', $shipmentId)->where('type','autoshipment')->where('deleted', '0')->where('extraCostCharged', 'N')->first();

        if (!empty($invoiceDetails)) {

            $invoicePreviousData = json_decode($invoiceDetails->invoiceParticulars, true);
            //print_r($invoicePreviousData);exit;
            $invoiceParticulars = $invoicePreviousData;
            $invoiceParticulars['shipment']['totalItemCost'] = $shipmentData->totalItemCost;
            $invoiceParticulars['shipment']['totalTax'] = '0.00';
            $invoiceParticulars['shipment']['isInsuranceCharged'] = 'N';
            $invoiceParticulars['shipment']['totalInsurance'] = '0.00';
            $invoiceParticulars['shipment']['inventoryCharge'] = '0.00';
            $invoiceParticulars['shipment']['otherChargeCost'] = '0.00';
            $invoiceParticulars['shipment']['storageCharge'] = '0.00';
            $invoiceParticulars['shipment']['maxStorageDate'] = 'N/A';
            $invoiceParticulars['shipment']['discount'] = '0.00';
            $invoiceParticulars['shipment']['shippingCost'] = '0.00';
            $invoiceParticulars['shipment']['clearingDutyCost'] = '0.00';
            $invoiceParticulars['shipment']['isDutyCharged'] = '0';
            $invoiceParticulars['shipment']['totalCost'] = $shipmentData->totalCost;
            //unset($invoiceParticulars['shipment']['shippingCost']);
            $invoiceParticulars['extracharge'] = array(
                'extraCost' => $request->extraCost,
                'notes' => $request->notes,
            );

            $totalCost = (!empty($shipmentData->totalCost) ? 0.00 : $shipmentData->totalCost);

            /*  INSERT DATA INTO INVOICE TABLE */
            $invoiceUniqueId = 'AUTO-INV-EXC-' . $userInfo->unit . '-' . $shipmentData->id . '-' . date('Ymd');
            $newInvoice = $invoiceDetails->replicate();
            $newInvoice->invoiceUniqueId = $invoiceUniqueId;
            $newInvoice->extraCostCharged = 'Y';
            $newInvoice->extraCostAmount = $request->extraCost;
            $newInvoice->invoiceParticulars = json_encode($invoiceParticulars);
            $newInvoice->totalBillingAmount = (!empty($totalCost)?$totalCost:0);
            $newInvoice->invoiceType = 'invoice';
            $newInvoice->paymentMethodId = null;
            $newInvoice->paymentStatus = 'unpaid';
            $newInvoice->save();
            $invoiceId = $newInvoice->id;
            
            
            if (!empty($invoiceId)) {
                $data['invoice'] = \App\Model\Invoice::find($invoiceId);    
                $fileName = "Invoice_" . $invoiceUniqueId . ".pdf";
                $data['pageTitle'] = "Print Invoice";
                PDF::loadView('Administrator.autoshipment.extrainvoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                $to = $userInfo->email;
                 
                $frontendUrl = Config::get('constants.frontendUrl') . "payment/invoice/" . $invoiceId;
                $content = "Invoice for Extra Cost Charged for Shipment #" . $shipmentId . " is attached. Please find the cost page at <a href=" . $frontendUrl . ">" . $frontendUrl . "</a>";
                Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($invoiceUniqueId, $to, $fileName) {
                    $message->from('contact@shoptomydoor.com', 'Shoptomydoor');
                    $message->subject("$invoiceUniqueId - Invoice Details");
                    $message->to($to);
                    $message->attach(public_path('exports/invoice/' . $fileName));
                });

                //return 1;
                return \Redirect::to('administrator/autoshipment/view/' . $shipmentId . '/1')->with('successMessage', 'Extra invoice created successfully.');
            }
        }

    }
    
    public function addcomment($shipmentId, Request $request) {
        $data = array();

        $message = $request->comment;

        $shipmentnotes = new \App\Model\Autoshipmentcomment;

        $shipmentnotes->shipmentId = $shipmentId;
        $shipmentnotes->message = $message;
        $shipmentnotes->type = "S";
        $shipmentnotes->sentBy = Auth::user()->id;
        $shipmentnotes->sentOn = Config::get('constants.CURRENTDATE');

        $userdetail = \App\Model\UserAdmin::select(array('firstname', 'lastname', 'email'))->where('id', Auth::user()->id)->first()->toArray();


        if ($shipmentnotes->save()) {
            $userdetail = array_merge($userdetail, array('sentOn' => date('Y-m-d', strtotime($shipmentnotes->sentOn)), 'noteId' => $shipmentnotes->id, 'shipmentId' => $shipmentId));
            return json_encode($userdetail);
        } else {
            return 0;
        }
    }
    
    public function shownotes($notesId, $shipmentId) {
        $data = array();

        $data['shipmentId'] = $shipmentId;

        $data['notes'] = \App\Model\Autoshipmentcomment::select('message')->where('id', $notesId)->where('type', 'S')->first();

        return view('Administrator.autoshipment.shownotes', $data);
    }
    
    public function sendcomment($id, $shipmentId) {
        $data = array();

        $data['shipmentId'] = $shipmentId;

        $data['customerEmailList'] = \App\Model\UserAdmin::select(array('id', 'email'))->where(array('deleted' => '0', 'status' => '1'))->get()->toArray();

        return view('Administrator.autoshipment.sendcomment', $data);
    }
    
    public function saveandnotify($shipmentId, Request $request) {

        $shipmentnotes = new \App\Model\Autoshipmentcomment;

        $emailto[] = $request->admin;



        $shipmentnotes->shipmentId = $shipmentId;
        $shipmentnotes->message = $request->warehousemsg;
        $shipmentnotes->type = "S";
        $shipmentnotes->sentBy = Auth::user()->id;
        $shipmentnotes->sentOn = Config::get('constants.CURRENTDATE');

        $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'notification_admin')->first();
        $replace['[NAME]'] = "Admin";
        $replace['[NOTIFICATION]'] = $request->warehousemsg;

        foreach ($emailto[0] as $email) {
            $sendMail = customhelper::SendMail($emailTemplate, $replace, $email);
        }
        if ($sendMail) {
            if ($shipmentnotes->save()) {

                return redirect()->back()->with('successMessage', 'Notes saved and sent successfully');
            } else {

                return redirect()->back()->with('errorMessage', 'Record not added');
            }
        } else {
            return redirect()->back()->with('errorMessage', 'Notes not sent');
        }
    }

}
