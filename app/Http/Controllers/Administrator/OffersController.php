<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Offer;
use App\Model\Offercondition;
use App\Model\Sitecategory;
use App\Model\Siteproduct;
use App\Model\Stores;
use App\Model\Shippingmethods;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\Sentlogcouponcode;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;

class OffersController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    /**
     * Method for showing the offers data
     * @return object
     */
    public function index() {

        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Offers'), Auth::user()->id);
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $data = array();
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('OFFERSDATA');
            \Session::push('OFFERSDATA.searchDisplay', $searchDisplay);
            \Session::push('OFFERSDATA.field', $field);
            \Session::push('OFFERSDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('OFFERSDATA.field');
            $sortType = \Session::get('OFFERSDATA.type');
            $searchDisplay = \Session::get('OFFERSDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'shortName' => array('current' => 'sorting'),
            'couponCode' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH OFFERS LIST  */
        $offersData = Offer::getList($param);

        if (count($offersData) > 0) {
            $data['page'] = $offersData->currentPage();
        } else {
            $data['page'] = 1;
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Offers";
        $data['contentTop'] = array('breadcrumbText' => 'Offers', 'contentTitle' => 'Offers', 'pageInfo' => 'This page allows you to view special offers & coupons
');
        $data['pageTitle'] = "Offers";
        $data['offersData'] = $offersData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavSettings', 'menuSub' => '', 'menuSubSub' => 'leftNavOffersSettings3');

        return view('Administrator.offers.index', $data);
    }

    /**
     * Method for change the status of offer
     * @param integer $id
     * @param integer $page
     * @param integer $status
     * @return object
     */
    public function changeStatus($id = 0, $page, $status = 1) {
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Offers'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            /*  CALLED FUNCTION TO CHANGE THE STATUS OF THE OFFER  */
            if (Offer::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/offers/?page=' . $page)->with('successMessage', 'Offer status changed successfully.');
            } else {
                return \Redirect::to('administrator/offers/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/offers/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method to generate category and subcategory
     * @param integer $id
     * @param integer $page
     * @param integer $status
     * @return object
     */
    public function generateSiteCategorySubcategory($startAt) {
        if ($children = Sitecategory::getCategorySubcategory($startAt)) {
            $thisLevel = array();
            foreach ($children as $child) {
                $thisLevel[$child->id] = $child;
                /*  CALLED THE CHILD LEVEL  */
                $thisLevel[$child->id]->children = $this->generateSiteCategorySubcategory($child->id);
            }
            return $thisLevel;
        }
    }

    /**
     * Method for add edit page
     * @param integer $id
     * @param integer $page
     * @return string
     */
    public function addedit($id = '0', $page = 1) {

        /* SET DATA FOR VIEW  */
        $data = array();
        $data['title'] = "Administrative Panel :: Offers";
        $data['contentTop'] = array('breadcrumbText' => 'Offers', 'contentTitle' => 'Offers', 'pageInfo' => 'Manage offers and coupons');
        $data['page'] = !empty($page) ? $page : '1';
        $data['sitecategories'] = $this->generateSiteCategorySubcategory(-1);
        $data['sitecategoriesOnlyParents'] = Sitecategory::getOnlyParents();
        $data['storeData'] = Stores::where('deleted', '0')->get();
        $data['shippingmethodsData'] = Shippingmethods::where('deleted', '0')->where('active', 'Y')->get();
        $data['countryData'] = Country::where('status', '1')->orderBy('name')->get();

        /*  EDIT OFFER DATA  */
        if ($id != 0) {
            /*  FIND USER ROLE  */
            $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Offers'), Auth::user()->id);
            if ($findRole['canEdit'] == 0) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }

            /* SET DATA FOR EDIT VIEW  */
            $data['pageTitle'] = "Offers & Coupons";
            $data['id'] = $id;
            $data['page'] = $page;
            $data['action'] = 'Edit';

            /* GET THE OFFER RECORD  */
            $offerData = Offer::where('id', $id)->get();

            /* GET THE RELATED DATA FOR OFFER CONDITION  */
            $offerConditionsData = Offercondition::where('offerId', $id)->where('deleted', '0')->get();

            /* UNSERIALIZE THE VALUES FROM BONUS SECTION */
            $unserializeBonus = unserialize($offerData[0]->bonusValuesSerialized);

            /* RANGE DATE OR VALID FOR REG BY APP */
            $checkRegbyApp = Offercondition::where('offerId', $id)->where('keyword', 'new_customer_reg_from_app')->count();
            if ($checkRegbyApp > 0) {
                $offerRangeValid = $offerData[0]->offerRangeDateFrom;
                $data['rangeDateDays'] = "1";
            } else {
                $offerRangeDateTo = $offerData[0]->offerRangeDateTo;
                if ($offerRangeDateTo == '') {
                    $offerRangeValid = date('m/d/Y') . " - " . date('m/d/Y');
                    $data['rangeDateDays'] = "2";
                    $offer = Offer::find($id);
                    $offer->offerRangeDateFrom = date('m/d/Y');
                    $offer->offerRangeDateTo = date('m/d/Y');
                    $offer->save();
                } else {
                    $offerRangeValid = $offerData[0]->offerRangeDateFrom . " - " . $offerData[0]->offerRangeDateTo;
                    $data['rangeDateDays'] = "2";
                }
            }

            $data['offerRangeValid'] = $offerRangeValid;

            /* IF THE CONDITION 1 VALUE IS NULL THEN DELETE THE CONDITION 1 FROM CONDITION TABLE */
            $offerConditionsOne = Offercondition::where('offerId', $id)->where('deleted', '0')->where('keyword', Config::get('constants.Offers.' . 1))->get();
            if (count($offerConditionsOne) > 0) {
                $extractOne = unserialize($offerConditionsOne[0]->valuesSerialized);

                if (count($extractOne) == 0) {
                    Offercondition::where('offerId', $id)->where('keyword', Config::get('constants.Offers.' . 1))->delete();
                }
            }

            /* IF NO CONDITION FOUND, SET THE OFFER AS PENDING */
            if (count($offerConditionsData) == 0) {
                $getOffer = Offer::find($id);
                $getOffer->status = '2';
                $getOffer->isSetCondition = '0';
                $getOffer->save();
            }


            /* MULTIPLE CONDITIONS */
            foreach ($offerConditionsData as $key => $offerCondition) {

                /* CONDITION 1 */
                if ($offerCondition->keyword == 'customer_get_discounts_and_special_offers_on_certain_product_or_categories') {
                    /* UNSERIALIZE THE VALUES FROM CONDITION TABLE  */
                    $unserialize1[] = unserialize($offerCondition->valuesSerialized);

                    /* GET THE RELATED VALUES */
                    $data['noOfSet'] = $offerData[0]->noOfSet;
                    $data['totalSet'] = $unserialize1[0];
                    $data['condition_tab_no'] = 1;
                }

                /* CONDITION 5 */
                if ($offerCondition->keyword == 'customer_of_a_specific_shipping_location') {
                    /* UNSERIALIZE THE VALUES FROM CONDITION TABLE  */
                    $unserialize5[] = unserialize($offerCondition->valuesSerialized);

                    foreach ($unserialize5[0] as $key => $val) {

                        /* GET THE COUNTRY LIST */
                        $getCountry[] = \App\Model\Country::where('id', $val['countrySelected'])->get();
                        $data['getCountry'] = $getCountry;

                        /* GET THE STATE LIST */
                        if ($val['stateSelected'] != 'N/A') {
                            $getState[] = \App\Model\State::where('id', $val['stateSelected'])->get();
                            $data['getState'] = $getState;
                        } else {
                            $getState[] = "N/A";
                        }

                        /* GET THE CITY LIST */
                        if ($val['citySelected'] != 'N/A') {
                            $getCity[] = \App\Model\City::where('id', $val['citySelected'])->get();
                            $data['getCity'] = $getCity;
                        } else {
                            $getCity[] = "N/A";
                        }
                    }

                    $data['getCountry'] = $getCountry;
                    $data['getState'] = $getState;
                    $data['getCity'] = $getCity;
                    $data['condition_tab_no'] = 5;
                }

                /* CONDITION 2 */
                if ($offerCondition->keyword == 'discount_on_total_amount_of_shipping') {
                    /* UNSERIALIZE THE VALUES FROM CONDITION TABLE  */
                    $unserialize2[] = unserialize($offerCondition->valuesSerialized);

                    /* GET THE RELATED VALUES */
                    $data['Minimum_2'] = $unserialize2[0]['Minimum'];
                    $data['Maximum_2'] = $unserialize2[0]['Maximum'];
                    $data['condition_tab_no'] = 2;
                }

                /* CONDITION 4 */
                if ($offerCondition->keyword == 'customer_has_not_purchased_any_goods_for_a_certain_timeframe') {
                    /* UNSERIALIZE THE VALUES FROM CONDITION TABLE  */
                    $unserialize4[] = unserialize($offerCondition->valuesSerialized);

                    /* GET THE RELATED VALUES */
                    $data['dateRange_4'] = $unserialize4[0]['dateRange'];
                    $data['condition_tab_no'] = 4;
                }

                /* CONDITION 6 */
                if ($offerCondition->keyword == 'customer_purchased_an_item_from_particular_store') {
                    /* UNSERIALIZE THE VALUES FROM CONDITION TABLE  */
                    $unserialize6[] = unserialize($offerCondition->valuesSerialized);

                    /* GET THE RELATED VALUES */
                    $data['store_6'] = $unserialize6[0]['store'];
                    $data['condition_tab_no'] = 6;
                }

                /* CONDITION 7 */
                if ($offerCondition->keyword == 'discount_on_total_weight_of_shipping') {
                    /* UNSERIALIZE THE VALUES FROM CONDITION TABLE  */
                    $unserialize7[] = unserialize($offerCondition->valuesSerialized);

                    /* GET THE RELATED VALUES */
                    $data['WeightFrom_7'] = $unserialize7[0]['WeightFrom'];
                    $data['WeightTo_7'] = $unserialize7[0]['WeightTo'];
                    $data['condition_tab_no'] = 7;
                }

                /* CONDITION 9 */
                if ($offerCondition->keyword == 'discount_on_total_weight_of_shipping_for_any_specific_customer') {
                    /* UNSERIALIZE THE VALUES FROM CONDITION TABLE  */
                    $unserialize9[] = unserialize($offerCondition->valuesSerialized);

                    /* GET THE RELATED VALUES */
                    $data['duration_9'] = $unserialize9[0]['duration'];
                    $data['WeightFrom_9'] = $unserialize9[0]['WeightFrom'];
                    $data['WeightTo_9'] = $unserialize9[0]['WeightTo'];
                    $data['condition_tab_no'] = 9;
                }

                /* CONDITION 8 */
                if ($offerCondition->keyword == 'customer_is_using_a_particular_shipping_method') {
                    /* UNSERIALIZE THE VALUES FROM CONDITION TABLE  */
                    $unserialize8[] = unserialize($offerCondition->valuesSerialized);

                    /* GET THE RELATED VALUES */
                    $data['shippingMethod_8'] = $unserialize8[0]['shippingMethod'];
                    $data['condition_tab_no'] = 8;
                }

                /* CONDITION 10 */
                if ($offerCondition->keyword == 'new_customer_reg_from_app') {
                    /* GET THE RELATED VALUES */
                    $data['condition_tab_no'] = 10;
                }
            }
            /* BONUS 21 */
            if ($offerData[0]->bonusKeyword == 'give_discount') {
                /* GET THE RELATED VALUES */
                $data['discount_21'] = $unserializeBonus['discount'];
                $data['type_21'] = $unserializeBonus['type'];
                $data['exceedType_21'] = $unserializeBonus['exceedType'];
                $data['discountNotExceed_21'] = $unserializeBonus['discountNotExceed'];
                $data['couponCode'] = $offerData[0]->couponCode;
                $data['bonus_tab_no'] = 1;
            }

            /* BONUS 22 */
            if ($offerData[0]->bonusKeyword == 'Give_bonus_points') {
                /* GET THE RELATED VALUES */
                $data['bonusPoint_22'] = $unserializeBonus['bonusPoint'];
                $data['fixedAmount_22'] = $unserializeBonus['fixedAmount'];
                $data['amountPer_22'] = $unserializeBonus['amountPer'];
                $data['bonusPer_22'] = $unserializeBonus['bonusPer'];
                $data['bonusOf_22'] = $unserializeBonus['bonusOf'];
                $data['pointsOnly_22'] = $unserializeBonus['pointsOnly'];
                $data['applyBonusToProductSets_22'] = $unserializeBonus['applyBonusToProductSets'];
                $data['couponCode'] = $offerData[0]->couponCode;
                $data['bonus_tab_no'] = 2;
            }

            /* IF NO DATA FOUND */
            if (count($offerData) == 0) {
                return redirect('/administrator/offers')->with('errorMessage', 'No data found!');
            }
            $data['isSent'] = $offerData[0]->isSent;
            $data['offerData'] = $offerData;
            $data['offerConditionsData'] = $offerConditionsData;
        } else {

            /* ADD */
            $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Offers'), Auth::user()->id); // call the helper function
            if ($findRole['canAdd'] == 0) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }

            /* SET DATA FOR ADD VIEW  */
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Offers & Coupons";
            $data['page'] = $page;
        }

        /* SET THE RELATED MENU ACTIVATED  */
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavSettings', 'menuSub' => '', 'menuSubSub' => 'leftNavOffersSettings3');

        return view('Administrator.offers.add', $data);
    }

    /** Method used to delete location data
     * @param integer $id
     * @param integer $page
     * @param Request $request
     * @return object
     */
    public function deletedataLocation($id, $page, Request $request) {
        $data = array();
        $oldRecord = Offercondition::where('offerId', $id)->where('keyword', 'customer_of_a_specific_shipping_location')->get();
        $extract = (unserialize($oldRecord[0]->valuesSerialized));
        #$extractJson = json_decode($oldRecord[0]->jsonValues);

        /* CHECK FOR LAST RECORD AND OTHER CONDITIONS  */
        $checkAllRecordsOfsameOfferId = Offercondition::where('offerId', $id)->get();

        if (count($extract) < 2 && count($checkAllRecordsOfsameOfferId) < 2) {
            return redirect('/administrator/offers/addedit/' . $id . '/' . $page)->with('errorMessage', 'Unable to remove the last record.');
        }

        /* UNSET RECORD IN ARRAY  */
        if (count($request->locationSelect) > 0) {
            foreach ($request->locationSelect as $key => $val) {
                unset($extract[$val]);
            }
        }


        if (count($request->locationSelect) > 0) {
            foreach ($request->locationSelect as $key => $val) {
                #unset($extractJson->shipping_location[$val]);
            }
        }

        /* IF LAST RECORD IS DELETED */
        if (count($extract) == 0) {
            Offercondition::where('offerId', $id)->where('id', $oldRecord[0]->id)->delete();
            return redirect('/administrator/offers/addedit/' . $id . '/' . $page)->with('successMessage', 'Information deleted.');
        }

        /* SET THE ARRAY FOR CONDITION  */
        $extract = array_values($extract);
        #$extractJson = array_values($extractJson->shipping_location);
        //Offercondition::where('offerId', $id)->delete();

        $offerCondition = Offercondition::find($oldRecord[0]->id);
        $offerCondition->offerId = $id;
        $offerCondition->valuesSerialized = serialize($extract);
        #$offerCondition->jsonValues = json_encode(array("shipping_location"=>$extractJson));
        $offerCondition->jsonValues = json_encode($extract);
        $offerCondition->keyword = Config::get('constants.Offers.' . 5);
        $offerCondition->setNo = 1;
        $offerCondition->modifiedBy = Auth::user()->id;
        $offerCondition->modifiedOn = Config::get('constants.CURRENTDATE');
        $offerCondition->save();
        return redirect('/administrator/offers/addedit/' . $id . '/' . $page)->with('successMessage', 'Information deleted.');
    }

    /** Method used to save offer information
     * @param integer $id
     * @param integer $page
     * @param Request $request
     * @return object
     */
    public function savedata($id, $page, Request $request) {

        $data = array();

        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Offers'), Auth::user()->id);

        /* SET DATA FOR VIEW  */
        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $offer = new Offer;

        if (($id != 0)) {
            /* EDIT  */
            if ($findRole['canEdit'] == 0) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }

            /* GET THE OFFER DATA BY ID  */
            $offer = Offer::find($id);
            $offer->modifiedBy = Auth::user()->id;
            $offer->modifiedOn = Config::get('constants.CURRENTDATE');
            $offer->isSetCondition = '1';
            $offer->save();


            /* CONDITION 1  */
            if ($request->condition_tab_no == 1) {

                $oldRecord = Offercondition::where('offerId', $id)->where('keyword', 'customer_get_discounts_and_special_offers_on_certain_product_or_categories')->get();

                /* IF ROW IN CONDITION TABLE FOUND */
                if (count($oldRecord) > 0) {

                    $extract = (unserialize($oldRecord[0]->valuesSerialized));
                    $extractJson = (json_decode($oldRecord[0]->jsonValues));
                    #$extractJsonMore = $extractJson->product_category;


                    $create_array = array($request->setno);
                    if ($request->Setcategoryid == 1) {
                        $create_values = $request->categoryid;
                        $setname = "Categories";
                        $setnameJson = "category";
                    } else {
                        $create_values = $request->Selectedproducts1;
                        $setname = "Products";
                        $setnameJson = "product";
                    }

                    /* CREATE THE ARRAYS  */
                    $newArr = array(array('setname' => $setname, 'values' => $create_values, 'setno' => $request->setno));
                    $newArrPush = array('setname' => $setname, 'values' => $create_values, 'setno' => $request->setno);



                    /* MAKE THE JSON ARRAY */

                    $flag = 0;

                    /* if($setnameJson == 'product'){

                      if (array_key_exists($setnameJson, $extractJsonMore[$request->setno][1])){
                      foreach($create_values as $key => $val){
                      array_push($extractJsonMore[$request->setno][1]->$setnameJson, $val);
                      }
                      }


                      } else {

                      if (array_key_exists($setnameJson, $extractJsonMore[$request->setno][0])){
                      array_push($extractJsonMore[$request->setno][0]->$setnameJson, $create_values);
                      }

                      }

                      $jsonArray = $extractJsonMore; */
                    #dd($jsonArray);


                    /* PUSHING THE ARRAY  */
                    if (count($extract) > 0 && $oldRecord[0]->keyword == 'customer_get_discounts_and_special_offers_on_certain_product_or_categories') {
                        $arr3 = array_push($extract[$request->setno], $newArrPush);
                        $pushed = serialize($extract);
                        $pushedJson = json_encode($extract);
                    } else {
                        $pushed = serialize(array(array(array('setname' => $setname, 'values' => $create_values, 'setno' => 0))));
                        $pushedJson = json_encode(array(array(array('setname' => $setname, 'values' => $create_values, 'setno' => 0))));
                    }

                    $blankArray = array(array(array()));

                    $pushedNew = array_merge_recursive($extract, $blankArray);

                    /* REMOVE DATA FROM OFFER CONDITION  */
                    $offerCondition = Offercondition::firstOrNew(array('keyword' => Config::get('constants.Offers.' . $request->condition_tab_no),
                                'offerId' => $id));

                    /* INSERT RECORD INTO OFFER CONDITION  */
                    $offerCondition->offerId = $id;
                    $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);
                    $offerCondition->valuesSerialized = $pushed;
                    #$offerCondition->jsonValues = json_encode(array("product_category"=>$jsonArray));
                    $offerCondition->jsonValues = $pushedJson;
                    $offerCondition->setNo = 1;
                    $offerCondition->createdBy = Auth::user()->id;
                    $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
                    $offerCondition->save();
                } else {

                    /* IF NO ROW UNDER CONDITION TABLE FOUND */

                    $offerCondition = new Offercondition;
                    $offerCondition->offerId = $id;

                    $create_array = array($request->setno);
                    if ($request->Setcategoryid == 1) {
                        $create_values = $request->categoryid;
                        #$jsonArray = array("product_category"=>array(array(array("category" => array($create_values)), array("product"=>array()))));
                        $setname = "Categories";
                    } else {
                        $create_values = $request->Selectedproducts1;
                        #$jsonArray = array("product_category"=>array(array(array("category" => array(), "product" => $create_values))));
                        $setname = "Products";
                    }

                    /* ADD OFFER CONDITION DATA  */
                    $setno = 1;
                    $offerCondition->valuesSerialized = serialize(array(array(array('setname' => $setname, 'values' => $create_values, 'setno' => $setno))));
                    #$offerCondition->jsonValues = json_encode($jsonArray);
                    $offerCondition->jsonValues = json_encode(array(array(array('setname' => $setname, 'values' => $create_values, 'setno' => $setno))));
                    $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);
                    $offerCondition->setNo = 1;
                    $offerCondition->createdBy = Auth::user()->id;
                    $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
                    $offerCondition->save();
                }
            }

            /* CONDITION 5  */
            if ($request->condition_tab_no == 5) {
                $theKeyword = Config::get('constants.Offers.' . $request->condition_tab_no);

                //$offerCondition = new Offercondition;
                //$offerCondition->offerId = $id;
                //$offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);

                $country = ($request->countrySelected != 'Please select one' ? $request->countrySelected : 'N/A');
                $state = ($request->stateSelected != 'Please select one' ? $request->stateSelected : 'N/A');
                $city = ($request->citySelected != 'Please select one' ? $request->citySelected : 'N/A');


                /* CREATING ARRAY  */
                $newArr = array('countrySelected' => $country, 'stateSelected' => $state, 'citySelected' => $city);

                /* FETCHING EXISTING OFFER CONDITION RECORD  */
                $oldRecord = Offercondition::where('offerId', $id)->where('keyword', $theKeyword)->get();

                /* IF RECORD EXISTS IN THE CONDITION TABLE */
                if (count($oldRecord) > 0) {

                    /* THIS BLOCK IS FOR CREATE ARRAY TO USE IN JSON ENCODE */
                    /* $getJosnArray = json_decode($oldRecord[0]->jsonValues);
                      $lastKey = count($getJosnArray->shipping_location);

                      if($state != 'N/A'){
                      $jsonArray = array($lastKey=>array("country" => $country, "state" => $state));
                      }
                      if($city != 'N/A'){
                      $jsonArray = array($lastKey=>array("country" => $country, "state" => $state, "city" => $city));
                      }
                      if($state == 'N/A' && $city == 'N/A'){
                      $jsonArray = array($lastKey=>array("country" => $country));
                      }

                      $getJosnArrayNew = $getJosnArray->shipping_location + $jsonArray; */

                    $rowId = $oldRecord[0]->id;


                    $extract = (unserialize($oldRecord[0]->valuesSerialized));
                    $flag = 0;


                    foreach ($extract as $key => $old) {
                        if ($extract[$key]['countrySelected'] == $newArr['countrySelected'] && $extract[$key]['stateSelected'] == $newArr['stateSelected'] && $extract[$key]['citySelected'] == $newArr['citySelected']) {

                            /* MATCHED FOUND, REDIRECT TO OFFER PAGE WITH ERROR MESSAGE  */
                            $flag = 1;
                            return redirect('/administrator/offers/addedit/' . $id . '/' . $page)->with('errorMessage', 'Data already exists.');
                        } else {
                            $flag = 0;

                            /* DO NOTHING  */
                        }
                    }
                    if ($flag == 0) {

                        /* DELETE OFFER CONDITION RECORD FIRST  */
                        //Offercondition::where('offerId', $id)->delete();

                        /* SAVING THE OFFER CONDITION RECORD  */

                        $offerCondition = Offercondition::find($rowId);
                        $arr3 = array_merge_recursive($extract, array($newArr));
                        $offerCondition->valuesSerialized = serialize($arr3);
                        #$offerCondition->jsonValues = json_encode(array("shipping_location"=>$getJosnArrayNew));
                        $offerCondition->jsonValues = json_encode($arr3);
                        $offerCondition->setNo = 1;
                        $offerCondition->modifiedBy = Auth::user()->id;
                        $offerCondition->modifiedOn = Config::get('constants.CURRENTDATE');
                        $offerCondition->save();
                        return redirect('/administrator/offers/addedit/' . $id . '/' . $page)->with('successMessage', 'Information saved successfuly.');
                    }
                } else {
                    $country = ($request->countrySelected != 'Please select one' ? $request->countrySelected : 'N/A');
                    $state = ($request->stateSelected != 'Please select one' ? $request->stateSelected : 'N/A');
                    $city = ($request->citySelected != 'Please select one' ? $request->citySelected : 'N/A');

                    if ($country == 'N/A') {
                        return redirect('/administrator/offers/addedit/' . $id . '/' . $page)->with('errorMessage', 'Select a country');
                    }

                    /* THIS BLOCK IS FOR CREATE ARRAY TO USE IN JSON ENCODE */
                    /* if($state != 'N/A'){
                      $jsonArray = array("country" => $request->countrySelected, "state" => $request->stateSelected);
                      }
                      if($city != 'N/A'){
                      $jsonArray = array("country" => $request->countrySelected, "state" => $request->stateSelected, "city" => $request->citySelected);
                      }
                      if($state == 'N/A' && $city == 'N/A'){
                      $jsonArray = array("country" => $request->countrySelected);
                      } */


                    /* ADD OFFER CONDITION DATA  */
                    $offerCondition = new Offercondition;
                    $offerCondition->offerId = $id;
                    $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);

                    $offerCondition->valuesSerialized = serialize(array(array('countrySelected' => $country, 'stateSelected' => $state, 'citySelected' => $city)));
                    #$offerCondition->jsonValues = json_encode(array("shipping_location"=>array($jsonArray) ));
                    $offerCondition->jsonValues = json_encode(array(array('countrySelected' => $country, 'stateSelected' => $state, 'citySelected' => $city)));
                    $offerCondition->setNo = 1;
                    $offerCondition->createdBy = Auth::user()->id;
                    $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
                    $offerCondition->save();
                    return redirect('/administrator/offers/addedit/' . $id . '/' . $page)->with('successMessage', 'Information saved successfuly.');
                }
            }

            /* CONDITION 2  */
            if ($request->condition_tab_no == 2) {
                $offerCondition = Offercondition::firstOrNew(array('keyword' => Config::get('constants.Offers.' . $request->condition_tab_no),
                            'offerId' => $id));

                /* SAVING THE RECORDS  */
                $offerCondition->offerId = $id;
                $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);
                $offerCondition->valuesSerialized = serialize(array('Minimum' => sprintf('%0.2f', $request->discountTotalAmountMinimum), 'Maximum' => sprintf('%0.2f', $request->discountTotalAmountMaximum)));
                $offerCondition->jsonValues = json_encode(array("shipping_cost" => array("from" => sprintf('%0.2f', $request->discountTotalAmountMinimum), "to" => sprintf('%0.2f', $request->discountTotalAmountMaximum))));
                $offerCondition->setNo = 1;
                $offerCondition->createdBy = Auth::user()->id;
                $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
                $offerCondition->save();
            }

            /* CONDITION 4  */
            if ($request->condition_tab_no == 4) {
                $offerCondition = Offercondition::firstOrNew(array('keyword' => Config::get('constants.Offers.' . $request->condition_tab_no),
                            'offerId' => $id));

                /* SAVING THE RECORDS  */
                $offerCondition->offerId = $id;
                $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);
                $offerCondition->valuesSerialized = serialize(array('dateRange' => $request->notPurchasedWithindate));
                $explodedate = explode("-", $request->notPurchasedWithindate);
                $offerCondition->jsonValues = json_encode(array("user_not_purchased" => array("from" => trim($explodedate[0]), "to" => trim($explodedate[1]))));
                $offerCondition->setNo = 1;
                $offerCondition->createdBy = Auth::user()->id;
                $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
                $offerCondition->save();
            }

            /* CONDITION 6  */
            if ($request->condition_tab_no == 6) {
                $offerCondition = Offercondition::firstOrNew(array('keyword' => Config::get('constants.Offers.' . $request->condition_tab_no),
                            'offerId' => $id));

                /* SAVING THE RECORDS  */
                $offerCondition->offerId = $id;
                $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);
                $offerCondition->valuesSerialized = serialize(array('store' => $request->store));
                $offerCondition->jsonValues = json_encode(array("store_id" => array($request->store)));
                $offerCondition->setNo = 1;
                $offerCondition->createdBy = Auth::user()->id;
                $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
                $offerCondition->save();
            }

            /* CONDITION 7 */
            if ($request->condition_tab_no == 7) {
                $offerCondition = Offercondition::firstOrNew(array('keyword' => Config::get('constants.Offers.' . $request->condition_tab_no),
                            'offerId' => $id));

                /* SAVING THE RECORDS  */
                $offerCondition->offerId = $id;
                $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);
                $offerCondition->valuesSerialized = serialize(array('WeightFrom' => sprintf('%0.2f', $request->weightFrom), 'WeightTo' => sprintf('%0.2f', $request->weightTo)));
                $offerCondition->jsonValues = json_encode(array("shipping_weight" => array("from" => sprintf('%0.2f', $request->weightFrom), "to" => sprintf('%0.2f', $request->weightTo))));
                $offerCondition->setNo = 1;
                $offerCondition->createdBy = Auth::user()->id;
                $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
                $offerCondition->save();
            }

            /* CONDITION 9  */
            if ($request->condition_tab_no == 9) {
                $offerCondition = Offercondition::firstOrNew(array('keyword' => Config::get('constants.Offers.' . $request->condition_tab_no),
                            'offerId' => $id));

                /* SAVING THE RECORDS  */
                $offerCondition->offerId = $id;
                $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);
                $offerCondition->valuesSerialized = serialize(array('duration' => $request->durationspecificcustomer, 'WeightFrom' => sprintf('%0.2f', $request->weightFromspecificcustomer), 'WeightTo' => sprintf('%0.2f', $request->weightTospecificcustomer)));
                $offerCondition->jsonValues = json_encode(array("shipping_weight_for_specific_user" => array("from" => sprintf('%0.2f', $request->weightFromspecificcustomer), "to" => sprintf('%0.2f', $request->weightTospecificcustomer), "duration" => $request->durationspecificcustomer)));
                $offerCondition->setNo = 1;
                $offerCondition->createdBy = Auth::user()->id;
                $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
                $offerCondition->save();
            }

            /* CONDITION 8 */
            if ($request->condition_tab_no == 8) {
                $offerCondition = Offercondition::firstOrNew(array('keyword' => Config::get('constants.Offers.' . $request->condition_tab_no),
                            'offerId' => $id));

                /* SAVING THE RECORDS  */
                $offerCondition->offerId = $id;
                $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);
                $offerCondition->valuesSerialized = serialize(array('shippingMethod' => $request->shippingMethod));
                $offerCondition->jsonValues = json_encode(array("shipping_method" => array($request->shippingMethod)));
                $offerCondition->setNo = 1;
                $offerCondition->createdBy = Auth::user()->id;
                $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
                $offerCondition->save();
            }

            /* CONDITION 10  */
            if ($request->condition_tab_no == 10) {
                $offerCondition = Offercondition::firstOrNew(array('keyword' => Config::get('constants.Offers.' . $request->condition_tab_no),
                            'offerId' => $id));

                /* SAVING THE RECORDS  */
                $offerCondition->offerId = $id;
                $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);
                $offerCondition->valuesSerialized = serialize(array('value' => 'New Customer Registration from App'));
                $offerCondition->jsonValues = json_encode(array("app_user_registration" => array("Y")));
                $offerCondition->setNo = 1;
                $offerCondition->createdBy = Auth::user()->id;
                $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
                $offerCondition->save();

                // UPDATE OFFER DATERANGE WITH DEFAULT VALUE 7
                $offer = Offer::find($id);
                $offer->offerRangeDateFrom = 7;
                $offer->offerRangeDateTo = '';
                $offer->save();
            }


            /* BONUS 1  */
            if ($request->bonus_tab_no == 1) {
                $offer = Offer::find($id);
                if ($request->couponCode != '') {

                    /* CHECKING DUPLICATE COUPON CODE, IF ANY  */
                    $couponCheckedDuplicate = Offer::where('couponCode', $request->couponCode)->where('id', '!=', $id)->first();
                    if (!empty($couponCheckedDuplicate)) {
                        return \Redirect::back()->withInput(\Input::all())->with('errorMessage', 'Coupon Code already exists');
                    }
                }

                /* SAVING BONUS RECORD  */
                $offer->couponCode = (\Input::has('couponCode') ? $request->couponCode : NULL);
                $offer->modifiedBy = Auth::user()->id;
                $offer->modifiedOn = Config::get('constants.CURRENTDATE');
                $offer->bonusKeyword = Config::get('constants.Offers.' . 21);

                /*  IF DISCOUNT TYPE IS PERCENTAGE, MAKE THE DISCOUNT SHOULD NOT BE EXCEEDED FIELD AS 0  */
                if ($request->discountTypePerProductDiscount == 'Percent') {
                    $discountNotExceeded = $request->discountNotExceed;
                } else {
                    $discountNotExceeded = 0.00;
                }

                $offer->bonusValuesSerialized = serialize(array('discount' => sprintf('%0.2f', $request->perProductDiscount), 'type' => $request->discountTypePerProductDiscount, 'discountNotExceed' => sprintf('%0.2f', $discountNotExceeded), 'exceedType' => $request->discountTypeDiscountNotExceed));
                $offer->isSetBonus = '1';
                $offer->save();
            }

            /* BONUS 2  */
            if ($request->bonus_tab_no == 2) {
                $offer = Offer::find($id);
                if ($request->couponCode != '') {

                    /* CHECKING DUPLICATE COUPON CODE, IF ANY  */
                    $couponCheckedDuplicate = Offer::where('couponCode', $request->couponCode)->where('id', '!=', $id)->first();
                    if (!empty($couponCheckedDuplicate)) {
                        return \Redirect::back()->withInput(\Input::all())->with('errorMessage', 'Coupon Code already exists');
                    }
                }

                /* SAVING BONUS RECORD  */
                $offer->couponCode = (\Input::has('couponCode') ? $request->couponCode : NULL);
                $offer->modifiedBy = Auth::user()->id;
                $offer->modifiedOn = Config::get('constants.CURRENTDATE');
                $offer->bonusKeyword = Config::get('constants.Offers.' . 22);
                $offer->bonusValuesSerialized = serialize(array('bonusPoint' => $request->bonus_point, 'fixedAmount' => $request->bonusPointsFixedAmount,
                    'amountPer' => $request->bonusPointsAmountPer, 'bonusPer' => $request->bonusPer, 'bonusOf' => $request->bonusOf,
                    'applyBonusToProductSets' => $request->applyBonusToProductSets, 'pointsOnly' => $request->pointsOnly));
                $offer->isSetBonus = '1';
                $offer->save();
            }

            /* SAVING RECORD FOR PROMO  */
            if ($request->promo_tab == 1) {
                $offer->modifiedBy = Auth::user()->id;
                $offer->modifiedOn = Config::get('constants.CURRENTDATE');
                $offer->promoText = $request->promoText;
                $offer->promoDetailed = $request->promoDetailed;
                $offer->promoCar = $request->promoCar;
                $offer->promoTotal = $request->promoTotal;
                $offer->isSetPromo = '1';

                /* SET THE IMAGE UPLOADED  */
                if ($request->hasFile('logo')) {
                    $image = $request->file('logo');
                    if (substr($image->getMimeType(), 0, 5) == 'image') {
                        $name = time() . '_' . $image->getClientOriginalName();
                        $destinationPath = public_path('/uploads/promo');
                        $image->move($destinationPath, $name);
                        $offer->promoImage = $name;
                    }
                }
                $offer->save();
            }


            /* SAVING RECORD FOR OFFER  */
            if ($request->offer_tab == 1) {
                $redirectMode = '?mode=offer';
                if (trim($request->shortName) == '') {
                    return redirect('/administrator/offers/addedit/' . $id . '/' . $page . $redirectMode)->with('errorMessage', 'Short name cannot be left blanked');
                } else {
                    $findOfferName = Offer::where('shortName', $request->shortName)->where('id', '!=', $id)->first();

                    if (!empty($findOfferName)) {
                        return redirect('/administrator/offers/addedit/' . $id . '/' . $page . $redirectMode)->with('errorMessage', 'Short name should be unique');
                    } else if ($request->OfferRangeDate == '') {
                        return redirect('/administrator/offers/addedit/' . $id . '/' . $page . $redirectMode)->with('errorMessage', 'Days cannot be left blanked');
                    } else {

                        /* EXPLODE DATE */

                        if ($request->has('validKey')) {
                            $offer->offerRangeDateFrom = $request->OfferRangeDate;
                            $offer->offerRangeDateTo = "";
                        } else {
                            $dateRange = $request->OfferRangeDate;
                            $explodeDate = explode("-", $dateRange);
                            $offer->offerRangeDateFrom = trim($explodeDate[0]);
                            $offer->offerRangeDateTo = trim($explodeDate[1]);
                        }

                        $offer->modifiedBy = Auth::user()->id;
                        $offer->modifiedOn = Config::get('constants.CURRENTDATE');
                        $offer->shortName = $request->shortName;
                        $offer->status = $request->status;
                        $offer->save();
                    }
                }
            }

            /* REDIRECTION CODE  */
            if ($request->condition_tab_no != '') {
                $redirectMode = '';
            } else if ($request->bonus_tab_no != '') {
                $redirectMode = '?mode=promo';
            } else if ($request->promo_tab_no != '') {
                $redirectMode = '?mode=offer';
            } else {
                $redirectMode = '?mode=offer';
            }

            return redirect('/administrator/offers/addedit/' . $id . '/' . $page . $redirectMode)->with('successMessage', 'Information saved successfuly.');
        }
        if ($findRole['canAdd'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        /* ADD  */
        $LastOfferData = Offer::orderBy('id', 'desc')->first();
        if (!empty($LastOfferData)) {

            /* GET LAST INSERTED ROW ID  */
            $nextOfferId = $LastOfferData->id + 1;
        } else {
            $nextOfferId = 1;
        }

        /* ADD RECORD TO OFFER MAIN TABLE  */
        $offer->shortName = (\Input::has('shortName') ? $request->shortName : 'Untitled ' . $nextOfferId);
        $offer->status = '2';
        $offer->createdBy = Auth::user()->id;
        $offer->createdOn = Config::get('constants.CURRENTDATE');
        $offer->isSetCondition = '1';
        $offer->save();

        /* CONDITION 1  */
        if ($request->condition_tab_no == 1) {
            $offerCondition = new Offercondition;
            $offerCondition->offerId = $nextOfferId;

            $create_array = array($request->setno);
            if ($request->Setcategoryid == 1) {
                $create_values = $request->categoryid;
                $setname = "Categories";
                $jsonArray = array("product_category" => array(array(array("category" => array($create_values)), array("product" => array()))));
            } else {
                $create_values = $request->Selectedproducts1;
                $setname = "Products";
                $jsonArray = array("product_category" => array(array(array("category" => array(), "product" => $create_values))));
            }

            /* ADD OFFER CONDITION DATA  */
            $setno = 1;
            $offerCondition->valuesSerialized = serialize(array(array(array('setname' => $setname, 'values' => $create_values, 'setno' => $setno))));
            #$offerCondition->jsonValues = json_encode($jsonArray);
            $offerCondition->jsonValues = json_encode(array(array(array('setname' => $setname, 'values' => $create_values, 'setno' => $setno))));
            $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);
            $offerCondition->setNo = 1;
            $offerCondition->createdBy = Auth::user()->id;
            $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
            $offerCondition->save();
            return redirect('/administrator/offers/addedit/' . $nextOfferId . '/' . $page)->with('successMessage', 'Information saved successfuly.');
        }

        /* CONDITION 5  */
        if ($request->condition_tab_no == 5) {

            /* ADD OFFER CONDITION DATA  */
            $offerCondition = new Offercondition;
            $offerCondition->offerId = $nextOfferId;
            $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);

            $country = ($request->countrySelected != 'Please select one' ? $request->countrySelected : 'N/A');
            $state = ($request->stateSelected != 'Please select one' ? $request->stateSelected : 'N/A');
            $city = ($request->citySelected != 'Please select one' ? $request->citySelected : 'N/A');

            /* THIS BLOCK IS FOR CREATE ARRAY TO USE IN JSON ENCODE */
            if ($state != 'N/A') {
                $jsonArray = array("country" => $request->countrySelected, "state" => $request->stateSelected);
            }
            if ($city != 'N/A') {
                $jsonArray = array("country" => $request->countrySelected, "state" => $request->stateSelected, "city" => $request->citySelected);
            }
            if ($state == 'N/A' && $city == 'N/A') {
                $jsonArray = array("country" => $request->countrySelected);
            }

            $offerCondition->valuesSerialized = serialize(array(array('countrySelected' => $country, 'stateSelected' => $state, 'citySelected' => $city)));
            #$offerCondition->jsonValues = json_encode(array("shipping_location"=>array($jsonArray) ));
            $offerCondition->jsonValues = json_encode(array(array('countrySelected' => $country, 'stateSelected' => $state, 'citySelected' => $city)));
            $offerCondition->setNo = 1;
            $offerCondition->createdBy = Auth::user()->id;
            $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
            $offerCondition->save();
            return redirect('/administrator/offers/addedit/' . $nextOfferId . '/' . $page)->with('successMessage', 'Information saved successfuly.');
        }

        /* CONDITION 2  */
        if ($request->condition_tab_no == 2) {

            /* ADD OFFER CONDITION DATA  */
            $offerCondition = new Offercondition;
            $offerCondition->offerId = $nextOfferId;
            $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);
            $offerCondition->valuesSerialized = serialize(array('Minimum' => sprintf('%0.2f', $request->discountTotalAmountMinimum), 'Maximum' => sprintf('%0.2f', $request->discountTotalAmountMaximum)));
            $offerCondition->jsonValues = json_encode(array("shipping_cost" => array("from" => sprintf('%0.2f', $request->discountTotalAmountMinimum), "to" => sprintf('%0.2f', $request->discountTotalAmountMaximum))));
            $offerCondition->setNo = 1;
            $offerCondition->createdBy = Auth::user()->id;
            $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
            $offerCondition->save();
        }

        /* CONDITION 4  */
        if ($request->condition_tab_no == 4) {
            $explodedate = explode("-", $request->notPurchasedWithindate);
            /* ADD OFFER CONDITION DATA  */
            $offerCondition = new Offercondition;
            $offerCondition->offerId = $nextOfferId;
            $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);
            $offerCondition->valuesSerialized = serialize(array('dateRange' => $request->notPurchasedWithindate));
            $offerCondition->jsonValues = json_encode(array("user_not_purchased" => array("from" => trim($explodedate[0]), "to" => trim($explodedate[1]))));
            $offerCondition->setNo = 1;
            $offerCondition->createdBy = Auth::user()->id;
            $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
            $offerCondition->save();
        }

        /* CONDITION 6  */
        if ($request->condition_tab_no == 6) {

            /* ADD OFFER CONDITION DATA  */
            $offerCondition = new Offercondition;
            $offerCondition->offerId = $nextOfferId;
            $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);
            $offerCondition->valuesSerialized = serialize(array('store' => $request->store));
            $offerCondition->jsonValues = json_encode(array("store_id" => array($request->store)));
            $offerCondition->setNo = 1;
            $offerCondition->createdBy = Auth::user()->id;
            $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
            $offerCondition->save();
        }

        /* CONDITION 7  */
        if ($request->condition_tab_no == 7) {

            /* ADD OFFER CONDITION DATA  */
            $offerCondition = new Offercondition;
            $offerCondition->offerId = $nextOfferId;
            $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);
            $offerCondition->valuesSerialized = serialize(array('WeightFrom' => sprintf('%0.2f', $request->weightFrom), 'WeightTo' => sprintf('%0.2f', $request->weightTo)));
            $offerCondition->jsonValues = json_encode(array("shipping_weight" => array("from" => sprintf('%0.2f', $request->weightFrom), "to" => sprintf('%0.2f', $request->weightTo))));
            $offerCondition->setNo = 1;
            $offerCondition->createdBy = Auth::user()->id;
            $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
            $offerCondition->save();
        }

        /* CONDITION 9 */
        if ($request->condition_tab_no == 9) {

            /* ADD OFFER CONDITION DATA  */
            $offerCondition = new Offercondition;
            $offerCondition->offerId = $nextOfferId;
            $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);
            $offerCondition->valuesSerialized = serialize(array('duration' => $request->durationspecificcustomer, 'WeightFrom' => sprintf('%0.2f', $request->weightFromspecificcustomer), 'WeightTo' => sprintf('%0.2f', $request->weightTospecificcustomer)));
            $offerCondition->jsonValues = json_encode(array("shipping_weight_for_specific_user" => array("from" => sprintf('%0.2f', $request->weightFromspecificcustomer), "to" => sprintf('%0.2f', $request->weightTospecificcustomer), "duration" => $request->durationspecificcustomer)));
            $offerCondition->setNo = 1;
            $offerCondition->createdBy = Auth::user()->id;
            $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
            $offerCondition->save();
        }

        /* CONDITION 8  */
        if ($request->condition_tab_no == 8) {

            /* ADD OFFER CONDITION DATA  */
            $offerCondition = new Offercondition;
            $offerCondition->offerId = $nextOfferId;
            $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);
            $offerCondition->valuesSerialized = serialize(array('shippingMethod' => $request->shippingMethod));
            $offerCondition->jsonValues = json_encode(array("shipping_method" => array($request->shippingMethod)));
            $offerCondition->setNo = 1;
            $offerCondition->createdBy = Auth::user()->id;
            $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
            $offerCondition->save();
        }

        /* CONDITION 10  */
        if ($request->condition_tab_no == 10) {

            /* ADD OFFER CONDITION DATA  */
            $offerCondition = new Offercondition;
            $offerCondition->offerId = $nextOfferId;
            $offerCondition->keyword = Config::get('constants.Offers.' . $request->condition_tab_no);
            $offerCondition->valuesSerialized = serialize(array('value' => 'New Customer Registration from App'));
            $offerCondition->jsonValues = json_encode(array("app_user_registration" => array("Y")));
            $offerCondition->setNo = 1;
            $offerCondition->createdBy = Auth::user()->id;
            $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
            $offerCondition->save();
        }

        return redirect('/administrator/offers/addedit/' . $nextOfferId . '/' . $page)->with('successMessage', 'Information saved successfuly.');
    }

    /** Method used to get site products by category
     * @param Request $request
     * @return object
     */
    public function getProductsByCategory(Request $request) {
        $catId = $request->catId;
        $data['productdata'] = Siteproduct::getListByCatid($catId);

        return view('Administrator.offers.products', $data);

        //return \Response::json($html);
    }

    /** Method used to get site states
     * @param Request $request
     * @return object
     */
    public function getStates(Request $request) {
        $getCode = Country::where('id', $request->countryId)->first();
        $states = State::where('countryCode', "$getCode->code")->where('deleted', '0')->orderBy('name')->get();
        return \Response::json($states);
    }

    /** Method used to get site cities
     * @param Request $request
     * @return object
     */
    public function getCities(Request $request) {
        $getCode = State::where('id', $request->stateId)->first();
        $cities = City::where('stateCode', "$getCode->code")->where('countryCode', "$getCode->countryCode")->where('deleted', '0')->orderBy('name')->get();
        return \Response::json($cities);
    }

    /** Method use to add product set
     * @param integer $id
     * @param integer $page
     * @param Request $request
     * @return type
     */
    public function addeditproductset($id, $page, Request $request) {

        /* GET EXISTING RECORD  */
        $oldRecord = Offercondition::where('offerId', $id)->where('keyword', Config::get('constants.Offers.' . 1))->get();

        /* UNSERIALIZE CONDITION VALUE  */
        $extract = (unserialize($oldRecord[0]->valuesSerialized));
        #$extractJson = json_decode($oldRecord[0]->jsonValues);


        /* MAKE A NEW SET FOR JSON VALUES */
        $newarr = array(array("category" => array()), array("product" => array()));
        #array_push($extractJson->product_category, $newarr);
        #$jsonValues = $extractJson;
        #dd($jsonValues);

        /* MAKE NEW SET  */
        $blankArray = array(array());
        $pushedNew = array_merge_recursive($extract, $blankArray);

        /* DELETE OFFER CONDITION DATA  */
        //Offercondition::where('offerId', $id)->delete();


        /* GET THE ID OF ROW IN THE OFFER CONDITION TABLE */
        $rowIdArray = Offercondition::where('offerId', $id)->where('keyword', Config::get('constants.Offers.' . 1))->get(['id']);
        $rowId = $rowIdArray[0]->id;

        /* ADD OFFER CONDITION DATA  */
        $offerCondition = Offercondition::find($rowId);
        //$offerCondition->offerId = $id;
        //$offerCondition->keyword = Config::get('constants.Offers.' . 1);
        $offerCondition->valuesSerialized = serialize($pushedNew);
        #$offerCondition->jsonValues = json_encode($jsonValues);
        $offerCondition->jsonValues = json_encode($pushedNew);
        //$offerCondition->setNo = 1;
        $offerCondition->modifiedBy = Auth::user()->id;
        $offerCondition->modifiedOn = Config::get('constants.CURRENTDATE');
        $offerCondition->save();

        /* MODIFY OFFER DATA  */
        $offer = Offer::find($id);
        $offer->modifiedBy = Auth::user()->id;
        $offer->modifiedOn = Config::get('constants.CURRENTDATE');
        $offer->save();

        return redirect('/administrator/offers/addedit/' . $id . '/' . $page)->with('successMessage', 'Product set added');
    }

    /** Method use to delete product set
     * @param integer $setId
     * @param integer $id
     * @param integer $page
     * @param Request $request
     * @return type
     */
    public function deleteproductset($setId, $id, $page, Request $request) {

        /* GET EXISTING RECORD  */
        $oldRecord = Offercondition::where('offerId', $id)->where('keyword', Config::get('constants.Offers.' . 1))->get();

        /* UNSERIALIZE CONDITION VALUE  */
        $extract = (unserialize($oldRecord[0]->valuesSerialized));
        #$extractJson = json_decode($oldRecord[0]->jsonValues);
        #unset($extractJson->product_category[$setId]);
        #$exJsonRaw = array_values($extractJson->product_category);
        #$exJson = array("product_category"=>$exJsonRaw);


        /* UNSET ARRAY VALUES  */
        unset($extract[$setId]);
        $extract = array_values($extract);



        /* DELETE OFFER CONDITION DATA  */
        //Offercondition::where('offerId', $id)->delete();

        /* ADD DATA TO OFFER CONDITION  */
        $offerCondition = Offercondition::find($oldRecord[0]->id);
        $offerCondition->offerId = $id;
        $offerCondition->keyword = Config::get('constants.Offers.' . 1);
        $offerCondition->valuesSerialized = serialize($extract);
        #$offerCondition->jsonValues = json_encode($exJson);
        $offerCondition->jsonValues = json_encode($extract);
        $offerCondition->setNo = 1;
        $offerCondition->modifiedBy = Auth::user()->id;
        $offerCondition->modifiedOn = Config::get('constants.CURRENTDATE');
        $offerCondition->save();
        return redirect('/administrator/offers/addedit/' . $id . '/' . $page)->with('successMessage', 'Product set deleted');
    }

    /** Method use to delete product item for first condition
     * @param integer $setId
     * @param integer $valueId
     * @param integer $mainId
     * @param integer $id
     * @param integer $page
     * @param Request $request
     * @return object
     */
    public function deleteitemproduct($setId, $valueId, $mainId, $id, $page, Request $request) {

        /* GET EXISTING RECORD  */
        $oldRecord = Offercondition::where('offerId', $id)->where('keyword', Config::get('constants.Offers.' . 1))->get();

        /* UNSERIALIZE CONDITION VALUE  */
        $extract = (unserialize($oldRecord[0]->valuesSerialized));
        #$extractJson = json_decode($oldRecord[0]->jsonValues);
        #unset($extractJson->product_category[$setId][$mainId]->product[$valueId]);
        #$exJsonRaw = array_values($extractJson->product_category[$setId][$mainId]->product);
        #$exJson = $extractJson;    
        #dd($exJson);

        /* UNSET ARRAY VALUES  */
        unset($extract[$setId][$valueId]['values'][$mainId]);
        $extract = array_values(array_filter($extract));



        if (count($extract[$setId][$valueId]['values']) < 1) {
            unset($extract[$setId][$valueId]['values']);
            unset($extract[$setId][$valueId]);
        }

        if (count($extract) == 0) {
            Offercondition::where('offerId', $id)->where('keyword', Config::get('constants.Offers.' . 1))->delete();
            return redirect('/administrator/offers/addedit/' . $id . '/' . $page)->with('successMessage', 'Item deleted');
        }


        /* DELETE OFFER CONDITION DATA  */
        //Offercondition::where('offerId', $id)->delete();

        /* ADD DATA TO OFFER CONDITION  */
        $offerCondition = Offercondition::find($oldRecord[0]->id);
        $offerCondition->offerId = $id;
        $offerCondition->keyword = Config::get('constants.Offers.' . 1);
        $offerCondition->valuesSerialized = serialize($extract);
        #$offerCondition->jsonValues = json_encode($exJson);
        $offerCondition->jsonValues = json_encode($extract);
        $offerCondition->setNo = 1;
        $offerCondition->modifiedBy = Auth::user()->id;
        $offerCondition->modifiedOn = Config::get('constants.CURRENTDATE');
        $offerCondition->save();
        return redirect('/administrator/offers/addedit/' . $id . '/' . $page)->with('successMessage', 'Item deleted');
    }

    /** Method use to delete invidual item for first condition
     * @param integer $setId
     * @param integer $valueId
     * @param integer $id
     * @param integer $page
     * @param Request $request
     * @return type
     */
    public function deleteitemcategory($setId, $valueId, $id, $page, Request $request) {

        /* GET EXISTING RECORD  */
        $oldRecord = Offercondition::where('offerId', $id)->where('keyword', Config::get('constants.Offers.' . 1))->get();

        /* UNSERIALIZE CONDITION VALUE  */
        $extract = (unserialize($oldRecord[0]->valuesSerialized));
        #$extractJson = json_decode($oldRecord[0]->jsonValues);
        #unset($extractJson->product_category[$setId][0]->category[$valueId]);
        #$exJsonRaw = array_values($extractJson->product_category[$setId][0]->category);
        #$exJson = $extractJson; 


        /* UNSET ARRAY VALUES  */
        unset($extract[$setId][$valueId]);
        $extract = array_values(array_filter($extract));

        if (count($extract) == 0) {
            Offercondition::where('offerId', $id)->where('keyword', Config::get('constants.Offers.' . 1))->delete();
            return redirect('/administrator/offers/addedit/' . $id . '/' . $page)->with('successMessage', 'Item deleted');
        }


        /* DELETE OFFER CONDITION DATA  */
        //Offercondition::where('offerId', $id)->delete();

        /* ADD DATA TO OFFER CONDITION  */
        $offerCondition = Offercondition::find($oldRecord[0]->id);
        $offerCondition->offerId = $id;
        $offerCondition->keyword = Config::get('constants.Offers.' . 1);
        $offerCondition->valuesSerialized = serialize($extract);
        #$offerCondition->jsonValues = json_encode($exJson);
        $offerCondition->jsonValues = json_encode($extract);
        $offerCondition->setNo = 1;
        $offerCondition->modifiedBy = Auth::user()->id;
        $offerCondition->modifiedOn = Config::get('constants.CURRENTDATE');
        $offerCondition->save();
        return redirect('/administrator/offers/addedit/' . $id . '/' . $page)->with('successMessage', 'Item deleted');
    }

    /** Method use to send coupon code view
     * @param integer $id
     * @return object
     */
    public function sendcoupon($id, Request $request) {

        /* CHECK WHETHER the STATUS OF THE OFFER IS ACTIVE */
        $offerActive = Offer::where('deleted', '0')->where('status', '1')->where('id', $id)->get();
        if (count($offerActive) < 1) {
            return redirect('/administrator/offers/')->with('errorMessage', 'The offer ID ' . $id . ' is not active');
        }


        $data = array();
        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchByfirstName = \Input::get('searchByfirstName', '');
            $searchBylastName = \Input::get('searchBylastName', '');
            $searchByEmail = \Input::get('searchByEmail', '');
            $searchByStatus = \Input::get('searchByStatus', '');
            $searchByCompany = \Input::get('searchByCompany', '');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('SENDCOUPON');
            \Session::push('SENDCOUPON.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('SENDCOUPON.searchDisplay', $searchDisplay);
            \Session::push('SENDCOUPON.searchByDate', $searchByDate);
            \Session::push('SENDCOUPON.searchByfirstName', $searchByfirstName);
            \Session::push('SENDCOUPON.searchBylastName', $searchBylastName);
            \Session::push('SENDCOUPON.searchByEmail', $searchByEmail);
            \Session::push('SENDCOUPON.searchByCompany', $searchByCompany);
            \Session::push('SENDCOUPON.searchByStatus', $searchByStatus);

            \Session::push('SENDCOUPON.field', $field);
            \Session::push('SENDCOUPON.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByfirstName'] = $searchByfirstName;
            $param['searchBylastName'] = $searchBylastName;
            $param['searchByEmail'] = $searchByEmail;
            $param['searchByStatus'] = $searchByStatus;
            $param['searchByCompany'] = $searchByCompany;
            $param['searchByDate'] = $searchByDate;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('SENDCOUPON.field');
            $sortType = \Session::get('SENDCOUPON.type');
            $searchByCreatedOn = \Session::get('SENDCOUPON.searchByCreatedOn');
            $searchByDate = \Session::get('SENDCOUPON.searchByDate');
            $searchByfirstName = \Session::get('SENDCOUPON.searchByfirstName');
            $searchBylastName = \Session::get('SENDCOUPON.searchBylastName');
            $searchByEmail = \Session::get('SENDCOUPON.searchByEmail');
            $searchByStatus = \Session::get('SENDCOUPON.searchByStatus');
            $searchByCompany = \Session::get('SENDCOUPON.searchByCompany');
            $searchDisplay = \Session::get('SENDCOUPON.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchByfirstName'] = !empty($searchByfirstName) ? $searchByfirstName[0] : '';
            $param['searchBylastName'] = !empty($searchBylastName) ? $searchBylastName[0] : '';
            $param['searchByEmail'] = !empty($searchByEmail) ? $searchByEmail[0] : '';
            $param['searchByStatus'] = !empty($searchByStatus) ? $searchByStatus[0] : '';
            $param['searchByCompany'] = !empty($searchByCompany) ? $searchByCompany[0] : '';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'unit' => array('current' => 'sorting'),
            'firstName' => array('current' => 'sorting'),
            'email' => array('current' => 'sorting'),
            'company' => array('current' => 'sorting'),
            'orderCount' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH USER LIST  */
        $userData = \App\Model\User::getUserList($param);
        $noOfUsersFound = count($userData);
        \Session::push('SENDCOUPON.noOfUsersFound', $noOfUsersFound);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Send Coupon";
        $data['contentTop'] = array('breadcrumbText' => 'Send Coupon', 'contentTitle' => 'Send Coupon', 'pageInfo' => 'This section allows you to send coupon to users');
        $data['pageTitle'] = "Send Coupon";
        $data['page'] = $userData->currentPage();
        $data['userData'] = $userData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['couponId'] = $id;

        return view('Administrator.offers.sendcoupon', $data);
    }

    /** Method use to store the info to table to cron later
     * @param integer $id
     * @return object
     */
    public function sendcouponcodeselected($id, Request $request) {

        if (!empty($id)) {

            /* GET THE COUPON CODE */
            $code = Offer::where('id', $id)->get();
            $couponCode = $code[0]->couponCode;

            /* FOR SELECTED USER */
            if ($request->selectedType == 'S') {
                /* SET THE INFO TO STORE INTO THE DB TABLE */

                foreach ($request->selected as $key => $val) {
                    $userDetails = \App\Model\User::where('id', $val)->get();

                    $scheduler = new Sentlogcouponcode;
                    $scheduler->userId = $userDetails[0]->id;
                    $scheduler->firstName = $userDetails[0]->firstName;
                    $scheduler->lastName = $userDetails[0]->lastName;
                    $scheduler->email = $userDetails[0]->email;
                    $scheduler->couponCode = $couponCode;
                    $scheduler->sent = "N";
                    $scheduler->save();
                }
            } else {

                /* FOR ALL USERS */

                $searchByCreatedOn = \Session::get('SENDCOUPON.searchByCreatedOn')[0];
                $searchByDate = \Session::get('SENDCOUPON.searchByDate')[0];
                $searchByfirstName = \Session::get('SENDCOUPON.searchByfirstName')[0];
                $searchBylastName = \Session::get('SENDCOUPON.searchBylastName')[0];
                $searchByEmail = \Session::get('SENDCOUPON.searchByEmail')[0];
                $searchByStatus = \Session::get('SENDCOUPON.searchByStatus')[0];
                $searchByCompany = \Session::get('SENDCOUPON.searchByCompany')[0];
                $searchDisplay = \Session::get('SENDCOUPON.searchDisplay')[0];

                $param['searchByCreatedOn'] = $searchByCreatedOn;
                $param['searchByfirstName'] = $searchByfirstName;
                $param['searchBylastName'] = $searchBylastName;
                $param['searchByEmail'] = $searchByEmail;
                $param['searchByStatus'] = $searchByStatus;
                $param['searchByCompany'] = $searchByCompany;
                $param['searchByDate'] = $searchByDate;
                $param['searchDisplay'] = $searchDisplay;
                $param['field'] = 'id';
                $param['type'] = 'asc';

                $userData = \App\Model\User::getUserList($param, 'coupon');

                if (count($userData) == 0) {
                    return response()->json(['success' => 'X']);
                }

                foreach ($userData as $key => $val) {


                    $scheduler = new Sentlogcouponcode;
                    $scheduler->userId = $val->id;
                    $scheduler->firstName = $val->firstName;
                    $scheduler->lastName = $val->lastName;
                    $scheduler->email = $val->email;
                    $scheduler->couponCode = $couponCode;
                    $scheduler->sent = "N";
                    $scheduler->save();
                }
            }

            /* SET THE FLAG SO THAT COUPON CODE IS NOT EDITABLE */
            $Offer = Offer::find($id);
            $Offer->isSent = 'Y';
            $Offer->save();

            return response()->json(['success' => 'Data saved successfully. Sending coupon codes to users will started shortly. This will take time based on number of users.']);
        }
    }

    /** Method use to delete a single row under condition table
     * @param Request $request
     */
    public function deletesinglerowcondition(Request $request) {
        /* DETERMINE KEYWORD */
        switch ($request->rowId) {
            case "1":
                $keyword = "customer_get_discounts_and_special_offers_on_certain_product_or_categories";
                break;
            case "5":
                $keyword = "customer_of_a_specific_shipping_location";
                break;
            case "2":
                $keyword = "discount_on_total_amount_of_shipping";
                break;
            case "4":
                $keyword = "customer_has_not_purchased_any_goods_for_a_certain_timeframe";
                break;
            case "6":
                $keyword = "customer_purchased_an_item_from_particular_store";
                break;
            case "7":
                $keyword = "discount_on_total_weight_of_shipping";
                break;
            case "9":
                $keyword = "discount_on_total_weight_of_shipping_for_any_specific_customer";
                break;
            case "8":
                $keyword = "customer_is_using_a_particular_shipping_method";
                break;
            default:
                $keyword = "new_customer_reg_from_app";
        }

        /* DELETE OFFER CONDITION DATA  */
        Offercondition::where('offerId', $request->offerId)->where('keyword', $keyword)->delete();
    }

    /** Method use to check whether any condition is there
     * @param Request $request
     * @return number
     */
    public function checknumberofcondition(Request $request) {
        /* GET TOTAL NUMBER OF CONDITION FOR A OFFER */

        $count = Offercondition::where('offerId', $request->offerId)->count();
        return response()->json(['success' => $count]);
    }

}
