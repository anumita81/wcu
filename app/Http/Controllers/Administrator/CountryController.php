<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Country;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use Excel;
use Illuminate\Support\Facades\DB;
use customhelper;

class CountryController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Country'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchByRegion = \Input::get('searchByRegion', '');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('COUNTRYDATA');
            \Session::push('COUNTRYDATA.searchByRegion', $searchByRegion);
            \Session::push('COUNTRYDATA.searchDisplay', $searchDisplay);
            \Session::push('COUNTRYDATA.field', $field);
            \Session::push('COUNTRYDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByRegion'] = $searchByRegion;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('COUNTRYDATA.field');
            $sortType = \Session::get('COUNTRYDATA.type');
            $searchByRegion = \Session::get('COUNTRYDATA.searchByRegion');
            $searchDisplay = \Session::get('COUNTRYDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'code';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchByRegion'] = !empty($searchByRegion) ? $searchByRegion[0] : '';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
            'code' => array('current' => 'sorting'),
            'isdCode' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTRY LIST  */
        $countryData = Country::getCountryList($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Countries";
        $data['contentTop'] = array('breadcrumbText' => 'Countries', 'contentTitle' => 'Countries', 'pageInfo' => 'This section allows you to manage countries');
        $data['pageTitle'] = "Countries";
        $data['page'] = $countryData->currentPage();
        $data['countryData'] = $countryData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.country.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Countries";
        $data['contentTop'] = array('breadcrumbText' => 'Countries', 'contentTitle' => 'Countries', 'pageInfo' => '');
        $data['page'] = !empty($page) ? $page : '1';
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $country = Country::find($id);
            $data['country'] = $country;
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add';
        }
        return view('Administrator.country.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $country = new Country;

        $validator = Validator::make($request->all(), [
                    'code' => 'required|unique:' . $country->table . ',code,' . $id . '|max:2|alpha',
                    'name' => 'required|regex:/^[\pL\s\-]+$/u|unique:' . $country->table . ',name,' . $id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $country = Country::find($id);
                $country->modifiedBy = Auth::user()->id;
                $country->modifiedOn = Config::get('constants.CURRENTDATE');
            }
            $country->code = $request->code;
            $country->name = $request->name;
            $country->isdCode = $request->isdCode;
            $country->save();
            $countryId = $country->id;

            return redirect('/administrator/country?page='.$page)->with('successMessage', 'Country information saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Country::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/country/?page=' . $page)->with('successMessage', 'Country status changed successfully.');
            } else {
                return \Redirect::to('administrator/country/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/country/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    
    public function getcountryisocode($countryId)
    {
        $countryData = Country::find($countryId);
        
        if(!empty($countryData)) {
            return json_encode(
                array(
                    'countryCode'=>$countryData->code,
                    'isdCode'=>$countryData->isdCode
                )
            );
        }
        else
        {
            return json_encode(array(
                'countryCode' => '',
                'isdCode' => '',
                )
            );
        }
    }

    

}
