<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Autowebsite;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;

class AutowebsiteController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Autowebsite'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('AUTOWEBSITEDATA');
            \Session::push('AUTOWEBSITEDATA.searchDisplay', $searchDisplay);
            \Session::push('AUTOWEBSITEDATA.field', $field);
            \Session::push('AUTOWEBSITEDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('AUTOWEBSITEDATA.field');
            $sortType = \Session::get('AUTOWEBSITEDATA.type');
            $searchDisplay = \Session::get('AUTOWEBSITEDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
            'createdOn'=> array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTRY LIST  */
        $autowebsiteData = Autowebsite::getAutowebsiteList($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Autowebsite";
        $data['contentTop'] = array('breadcrumbText' => 'Autowebsite', 'contentTitle' => 'Autowebsite', 'pageInfo' => 'This section allows you to manage auto websites');
        $data['pageTitle'] = "Autowebsite";
        $data['page'] = $autowebsiteData->currentPage();
        $data['autowebsiteData'] = $autowebsiteData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.autowebsite.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['pageTitle'] = 'Autowebsite';
        $data['title'] = "Administrative Panel :: Autowebsite";
        $data['contentTop'] = array('breadcrumbText' => 'Autowebsite', 'contentTitle' => 'Autowebsite', 'pageInfo' => 'This section allows you to manage auto websites');
        $data['page'] = !empty($page) ? $page : '1';
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $autowebsite = Autowebsite::find($id);
            $data['websiteData'] = $autowebsite;


        
        return view('Administrator.autowebsite.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $autowebsite = new Autowebsite;
        
        if(empty($id)){
            $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'description' => 'required',
                    'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        }else {
            $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'description' => 'required',
            ]);
        }
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            
            if ($request->hasFile('logo')) {
                $image = $request->file('logo');
                //$name = time().'.'.$image->getClientOriginalExtension();
                $name = time() . '_' . $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/auto');
                $image->move($destinationPath, $name);
            }
            
            if (!empty($id)) {
                $autowebsite = Autowebsite::find($id);
                $autowebsite->modifiedBy = Auth::user()->id;
                $autowebsite->modifiedOn = Config::get('constants.CURRENTDATE');
            } else {
              
                $autowebsite->createdBy = Auth::user()->id;
                $autowebsite->createdOn = Config::get('constants.CURRENTDATE');
            }
       
            $autowebsite->name = $request->name;
            $autowebsite->description = $request->description;
            if(!empty($name))
                $autowebsite->logo = $name;
            $autowebsite->save();
            $autowebsiteId = $autowebsite->id;

            return redirect('/administrator/autowebsite')->with('successMessage', 'Website saved successfuly.');
        }
    }
    
     /**
     * Method used to change status
     * @param integer $id
     * @param integer $page
     * @param integer $status
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Autowebsite::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/autowebsite/?page=' . $page)->with('successMessage', 'Status changed successfully.');
            } else {
                return \Redirect::to('administrator/autowebsite/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/autowebsite/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            $carWebsiteIdUsed = \App\Model\Procurementitem::fetchUsedFieldIds('carWebsiteId');
            if(!in_array($id,$carWebsiteIdUsed))
            {
                if (Autowebsite::deleteRecord($id, $createrModifierId)) {
                    return \Redirect::to('administrator/autowebsite/?page=' . $page)->with('successMessage', 'Auto Website deleted successfully.');
                } else {
                    return \Redirect::to('administrator/autowebsite/?page=' . $page)->with('errorMessage', 'Error in operation!');
                }
            } else
            {
                return \Redirect::to('administrator/autowebsite?page=' . $page)->with('errorMessage', 'Auto website cannot be deleted. Dependency exists.');
            }
        } else {
            return \Redirect::to('administrator/autowebsite/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    
    /**
     * Method to show available Make Model
     * @param integer $id
     * @param integer $page
     */

    public function checkavailability($autowebsiteId, $page)
    {
        
        $data = array();

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Autowebsite'), Auth::user()->id); // call the helper function
        if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        
        /* Website Details */
        $data['autoWebsite'] = Autowebsite::find($autowebsiteId)->toArray(); 
        
        $data['autowebsiteId'] = $autowebsiteId;
        
        $data['page'] = !empty($page) ? $page : '1';
       
        /* FETCH AUTO MODEl LIST  */
        $data['autoModelList'] = \App\Model\Automodel::getMakeModelList()->toArray();
        
        $data['autoMapping']= \App\Model\Autowebsitemap::getMakeModelMap($autowebsiteId)->toArray();

        $data['mapData'] = array();
        
        if(!empty($data['autoMapping'])){
            foreach($data['autoMapping'] as $mapping)
            {
                $data['mapData'][] = $mapping['modelMakeMap'];
            }
        }

    
        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Autowebsite";
        $data['contentTop'] = array('breadcrumbText' => 'Autowebsite', 'contentTitle' => 'Autowebsite', 'pageInfo' => 'This section allows you to manage auto websites');
        $data['pageTitle'] = "Autowebsite";

        return view('Administrator.autowebsite.availability', $data);
    }
    
    
    public function saveavailability($id, $page, Request $request)
    {
        $data = array();
        
             
        
        $findWebsite = \App\Model\Autowebsitemap::where('websiteId', $id)->get();
        
        if(count($findWebsite) == 0)
        {
           
            
           for($i=0; $i<count($request->model); $i++)
            {
               $modelMakeId = explode(':', $request->model[$i]);
               $autowebMapping = array('websiteId'=> $id, 'makeId'=>$modelMakeId[0], 'modelId'=>$modelMakeId[1]);
               
               
               \App\Model\Autowebsitemap::insert($autowebMapping);
               
               
              
            } 
            
             return redirect('/administrator/autowebsite')->with('successMessage', 'Make and Model saved successfuly.');
          
       }else{
           
           $delete =  \App\Model\Autowebsitemap::where('websiteId', $id)->delete();
           
           if($delete)
           {
               for($i=0; $i<count($request->model); $i++)
                {
                   $modelMakeId = explode(':', $request->model[$i]);
                   echo $request->model[$i].",";
                   $autowebMapping = array('websiteId'=> $id, 'makeId'=>$modelMakeId[0], 'modelId'=>$modelMakeId[1]);


                   \App\Model\Autowebsitemap::insert($autowebMapping);

                }
               
           }else{
                return redirect('/administrator/autowebsite')->with('errorMessage', 'Make and Model not saved.');
           }
           
            return redirect('/administrator/autowebsite')->with('successMessage', 'Make and Model saved successfuly.'); 
           
       }
        
        
       
        
    }

}
