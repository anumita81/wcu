<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Shippingcost;
use App\Model\Zone;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;

class ShippingcostController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Shippingcost'), Auth::user()->id); // call the helper function
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            $searchByMake = \Input::get('searchByMake', '');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('SHIPPINGCOSTDATA');
            \Session::push('SHIPPINGCOSTDATA.searchByMake', $searchByMake);

            $param['searchByMake'] = $searchByMake;

        } else {
            $searchByMake = \Session::get('SHIPPINGCOSTDATA.searchByMake');
            $param['searchByMake'] = !empty($searchByMake) ? $searchByMake[0] : '';
        }

        /* FETCH model LIST  */
        $shippingCharges = Shippingcost::getChargesData($param);
        //dd($shippingCharges);
        $data['makeData'] = \App\Model\Automake::where('deleted', '0')->orderBy('name', 'ASC')->get();
        $data['shippingCharges'] = $shippingCharges;
        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Shipping Cost";
        $data['contentTop'] = array('breadcrumbText' => 'Shipping Cost', 'contentTitle' => 'Shipping Cost', 'pageInfo' => 'This section allows you to manage different shipping costs');
        $data['pageTitle'] = "Shipping Cost";
        //$data['page'] = $shippingCostData->currentPage();
        $data['searchData'] = $param;
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavAuto', 'menuSub' => '', 'menuSubSub' => 'leftNavShippingCost9');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.shippingcost.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Shippingcost'), Auth::user()->id); // call the helper function
        $data = array();
        $data['title'] = "Administrative Panel :: Shipping Cost";
        $data['contentTop'] = array('breadcrumbText' => 'Shipping Cost', 'contentTitle' => 'Shipping Cost', 'pageInfo' => 'This section allows you to manage different shipping costs');
        $data['page'] = !empty($page) ? $page : '1';

        $data['makeList'] = \App\Model\Automake::where('deleted', '0')->orderBy('name', 'ASC')->get();

        $data['allZones'] = Zone::where('status','1')->get();

        if (!empty($id)) {
            if ($findRole['canEdit'] == 0) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['pageTitle'] = "Edit Cost";
            $data['id'] = $id;
            $data['page'] = $page;
            $data['action'] = 'Edit';
            $shippingCost = $data['shippingCost'] = Shippingcost::find($id);

            $data['modelList'] = \App\Model\Automodel::where('deleted', '0')->where('makeId', $shippingCost->makeId)->orderBy('name', 'ASC')->get();
        } else {
            if ($findRole['canAdd'] == 0) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add New Cost";
            $data['page'] = $page;
        }
        return view('Administrator.shippingcost.addedit', $data);
    }

    /**
     * Method to fetch model list
     * @return array
     */
    public function getmodellist($makeId = '') {
        $modelList = array();
        if (isset($makeId) && !empty($makeId)) {
            $modelList = \App\Model\Automodel::where('deleted', '0')
                    ->where('makeId', $makeId)
                    ->orderby('name', 'asc')
                    ->get();
        }
        echo json_encode($modelList);
        exit;
    }

    /**
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Shippingcost'), Auth::user()->id); // call the helper function
        if ($findRole['canAdd'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $data['page'] = !empty($page) ? $page : '1';
        $shippingCost = new Shippingcost;

        $validator = Validator::make($request->all(), [
                    'zoneId' => 'required',
                    'pzoneId' => 'required',
                    'makeId' => 'required',
                    'modelId' => 'required',
                    'shippingCharge' => 'required',
                    'clearingPortHandlingCharge' => 'required',
                    'insurancePercent' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if($request->zoneId == $request->pzoneId)
            {
                return redirect('/administrator/shippingcost')->with('errorMessage', 'Source and destination zone can not same');
            }
            // Add
            if(Shippingcost::checkIfDataExist($request))
                return redirect('/administrator/shippingcost')->with('errorMessage', 'Shipping charge exist for same zone and make model');
            if ($findRole['canAdd'] == 0) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }

            $shippingCost->createdBy = Auth::user()->id;
            $shippingCost->createdOn = Config::get('constants.CURRENTDATE');
        
            $shippingCost->zoneId = $request->zoneId;
            $shippingCost->pzoneId = $request->pzoneId;
            $shippingCost->makeId = $request->makeId;
            $shippingCost->modelId = $request->modelId;
            $shippingCost->shippingCharge = $request->shippingCharge;
            $shippingCost->clearingPortHandlingCharge = $request->clearingPortHandlingCharge;
            $shippingCost->insurancePercent = $request->insurancePercent;
            $shippingCost->save();

            return redirect('/administrator/shippingcost')->with('successMessage', 'Shipping cost information saved successfuly.');
        }
    }

    public function editdata(Request $request) {
        $postData = $request->postData;
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Shippingcost'), Auth::user()->id);
        if(!empty($postData))
        {
            foreach($postData as $editId => $editdata)
            {
                if(isset($editdata['is_update']) && $editdata['is_update']=='on')
                {
                    $shippingCost = Shippingcost::find($editId);
                    if($request->action == 'update') {

                        if ($findRole['canEdit'] == 0) {
                            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                        }
                        $shippingCost->shippingCharge = $editdata['shippingCharge'];
                        $shippingCost->clearingPortHandlingCharge = $editdata['clearingPortHandlingCharge'];
                        $shippingCost->insurancePercent = $editdata['insurancePercent'];

                        
                    }
                    else if($request->action == 'delete') {

                        if ($findRole['canDelete'] == 0) {
                            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                        }
                        $shippingCost->deleted = '1';
                        $shippingCost->deletedBy = Auth::user()->id;
                        $shippingCost->deletedOn = Config::get('constants.CURRENTDATE');
                    }

                    $shippingCost->save();
                }
            }
        }

        return redirect('/administrator/shippingcost')->with('successMessage', 'Shipping cost information '.$request->action.'d successfuly.');
    }

}
