<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\libraries\dbHelpers;
use Auth;
use App\Model\Partner;
use Hash;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Routing\Route;
use Config;
use customhelper;
use Carbon\Carbon;

class PartnerController extends Controller {
    public $_perPage;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Request $request) {
        $data = array();
        
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Partner'), Auth::user()->id); 
        
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            \Session::forget('PARTNER');
            \Session::push('PARTNER.searchDisplay', $searchDisplay);
            \Session::push('PARTNER.field', $field);
            \Session::push('PARTNER.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('PARTNER.field');
            $sortType = \Session::get('PARTNER.type');
            $searchDisplay = \Session::get('PARTNER.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
            'status' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
            'type'      => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* GET THE PARTNER RECORD  */
        $partnerData = Partner::where('deleted', '0')->where('contentType','image')->orderBy($param['field'], $param['type'])->paginate($param['searchDisplay']);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Partner :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Partner";
        $data['contentTop'] = array('breadcrumbText' => 'Partner', 'contentTitle' => 'Partner', 'pageInfo' => 'This section allows you to manage partner information');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavContent', 'menuSub' => 'leftNavSiteContent2', 'menuSubSub' => 'leftNavPartner16');
        $data['partnerData'] = $partnerData;
        $data['page'] = $partnerData->currentPage();
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        return view('Administrator.partner.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $id
     * @param string $page
     * @return array
     */
    public function addedit($id = '0', $page = '') {
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Partner'), Auth::user()->id);
        $data = array();
        $data['title'] = "Administrative Panel :: Partner";
        $data['contentTop'] = array('breadcrumbText' => 'Partner', 'contentTitle' => 'Partner', 'pageInfo' => 'This section allows you to manage partner information');
        $data['page'] = !empty($page) ? $page : '1';

        if (!empty($id)) {
            if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            /* SET DATA FOR VIEW  */
            $data['pageTitle'] = "Edit Partner";
            $data['partnerId'] = $id;
            $data['page'] = $page;
            $data['action'] = 'Edit';
            $data['partnerData'] = Partner::find($id);
        } else {
            if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            /* SET DATA FOR VIEW  */
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add Partner";
            $data['page'] = $page;
        }
        return view('Administrator.partner.addedit', $data);
    }


    /**
     * Method used to save information
     * @param integer $id
     * @param string $page
     * @param Request $request
     * @return array
     */
    public function savedata($id, $page, Request $request) {

        $data = array();
        
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Partner'), Auth::user()->id);
        $data['page'] = !empty($page) ? $page : '1';
        $partner = new Partner;

        /*  SET CUSTOM MESSAGE FOR VALIDATION  */
        $messages = [
            'fileName.max' => 'Image maximum size exceed. ',
        ];
        
        $validator = Validator::make($request->all(), [
            'fileName' => 'max:4096',
        ], $messages);


        /*  IF VALIDATION FAIL */
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->customMessages);
        }

        /*  EDIT */
        if ($id != 0) {
            if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }



            if ($request->hasFile('fileName')) {
                $image = $request->file('fileName');

                if (substr($image->getMimeType(), 0, 5) == 'image') {

                    $info = pathinfo($image->getClientOriginalName());
                    if($info['extension'] == 'bmp' || $info['extension'] == 'svg'){
                        return \Redirect::to('administrator/partner?page=' . $page)->with('errorMessage', 'Please upload an image with jpeg, jpg, gif, png extension');
                    }

                    $name = time() . '_' . $image->getClientOriginalName();
                    $destinationPath = public_path('/uploads/partner/original');
                    $image->move($destinationPath, $name);
                    
                    /* RESIZE THE IMAGE AND SAVED IN DIFFERENT PATHS */
                    $res100 = $this->createThumbnail($name, 100, 100, public_path('/uploads/partner/original'), public_path('/uploads/partner/thumb/'));
                    $res300 = $this->createThumbnail($name, 300, 300, public_path('/uploads/partner/original'), public_path('/uploads/partner/medium/'));

                    /* SAVING PARTNER RECORD  */
                    $partner = Partner::find($id);
                    $partner->name = $request->name;
                    $partner->type = $request->type;
                    $partner->fileName = $name;
                    $partner->createdBy = Auth::user()->id;
                    $partner->createdOn = Config::get('constants.CURRENTDATE');
                    $partner->save();
                } else {
                    return \Redirect::to('administrator/partner?page=' . $page)->with('errorMessage', 'Please upload an image.');
                }
            } else {
                return \Redirect::to('administrator/partner?page=' . $page)->with('errorMessage', 'Please upload an image.');
            }

            
        } else {
            /*  ADD */
            if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            
            /* SET THE IMAGE UPLOADED  */
            if ($request->hasFile('fileName')) {
                $image = $request->file('fileName');

                if (substr($image->getMimeType(), 0, 5) == 'image') {

                    $info = pathinfo($image->getClientOriginalName());
                    if($info['extension'] == 'bmp' || $info['extension'] == 'svg'){
                        return \Redirect::to('administrator/partner?page=' . $page)->with('errorMessage', 'Please upload an image with jpeg, jpg, gif, png extension');
                    }

                    $name = time() . '_' . $image->getClientOriginalName();
                    $destinationPath = public_path('/uploads/partner/original');
                    $image->move($destinationPath, $name);
                    
                    /* RESIZE THE IMAGE AND SAVED IN DIFFERENT PATHS */
                    $res100 = $this->createThumbnail($name, 100, 100, public_path('/uploads/partner/original'), public_path('/uploads/partner/thumb/'));
                    $res300 = $this->createThumbnail($name, 300, 300, public_path('/uploads/partner/original'), public_path('/uploads/partner/medium/'));

                    /* SAVING PARTNER RECORD  */
                    $partner->name = $request->name;
                    $partner->type = $request->type;
                    $partner->fileName = $name;
                    $partner->createdBy = Auth::user()->id;
                    $partner->createdOn = Config::get('constants.CURRENTDATE');
                    $partner->save();
                } else {
                    return \Redirect::to('administrator/partner?page=' . $page)->with('errorMessage', 'Please upload an image.');
                }
            } else {
                return \Redirect::to('administrator/partner?page=' . $page)->with('errorMessage', 'Please upload an image.');
            }
            
        }
        

        return redirect('/administrator/partner?page=' . $page)->with('successMessage', 'Record saved successfuly.');
    }



    /**
     * Method used to delete
     * @param integer $id
     * @param string $page
     * @return array
     */
    public function delete($id, $page) {
        
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.partner'), Auth::user()->id);
        if($findRole['canDelete'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {

            /* GET THE RECORD */
            $getValue = Partner::where('id', $id)->get();
            $fileName = $getValue[0]->fileName;
            $destinationPath = public_path('/uploads/partner/original/');
            $path = $destinationPath. $fileName;


            /*  DELETE A PARTNER RECORD  */
            if (Partner::deleteRecord($id, $createrModifierId)) {
                unlink($path);
                
                return \Redirect::to('administrator/partner?page=' . $page)->with('successMessage', 'Partner record deleted successfully.');
            } else {
                return \Redirect::to('administrator/partner?page=' . $page)->with('errorMessage', 'File in operation!');
            }
        } else {
            return \Redirect::to('administrator/partner?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }


    /**
     * Method used to resize image and save the same
     * @param string $image_name
     * @param integer $new_width
     * @param integer $new_height
     * @param string $uploadDir
     * @param string $moveToDir
     * @return array
     */
    public function createThumbnail($image_name,$new_width,$new_height,$uploadDir,$moveToDir)
    { 

        /* CREATING THE PATH OF THE IMAGE */
        $path = $uploadDir . '/' . $image_name;

        $mime = getimagesize($path);


        if($mime['mime']=='image/png') { 
            $src_img = imagecreatefrompng($path);
        }
        if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
            $src_img = imagecreatefromjpeg($path);
        } 
        if($mime['mime']=='image/gif') { 
            $src_img = imagecreatefromgif($path);
        }
        if($mime['mime']=='image/x-ms-bmp') { 
            $src_img = imagecreatefrombmp($path);
        }  

        $old_x          =   imageSX($src_img);
        $old_y          =   imageSY($src_img);

        if($old_x > $old_y) 
        {
            $thumb_w    =   $new_width;
            $thumb_h    =   $old_y*($new_height/$old_x);
        }

        if($old_x < $old_y) 
        {
            $thumb_w    =   $old_x*($new_width/$old_y);
            $thumb_h    =   $new_height;
        }

        if($old_x == $old_y) 
        {
            $thumb_w    =   $new_width;
            $thumb_h    =   $new_height;
        }

        $dst_img        =   ImageCreateTrueColor($thumb_w,$thumb_h);

        imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 


        /* NEW SAVE LOCATION */
        $new_thumb_loc = $moveToDir . $image_name;

        if($mime['mime']=='image/png') {
            $result = imagepng($dst_img,$new_thumb_loc,8);
        }
        if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg' || $mime['mime']=='image/gif' || $mime['mime']=='image/x-ms-bmp') {
            $result = imagejpeg($dst_img,$new_thumb_loc,80);
        }

        imagedestroy($dst_img); 
        imagedestroy($src_img);

        return $result;
    }



    /**
     * Method used to change status
     * @param integer $id
     * @param string $page
     * @param string $status
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $getData = Partner::find($id);
        $status = $getData->status;
        if($status == '1'){
            $newStatus = '0';
        } else {
            $newStatus = '1';
        }

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Partner::changeStatus($id, $createrModifierId, $newStatus)) {
                return \Redirect::to('administrator/partner/?page=' . $page)->with('successMessage', 'Partner status changed successfully.');
            } else {
                return \Redirect::to('administrator/partner/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/partner/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    public function getcontent($id=0,$page=1,$type = '0') {
        
        $partnerContent = Partner::where('contentType','content')->where('type',$type)->where('status','1')->where('deleted','0')->first();
        
        $data['content'] = $partnerContent;
        $data['type'] = $type;
        
        return view('Administrator.partner.getcontent', $data);
    }
    
    public function updatecontent($id,Request $request) {
        
        $validator = Validator::make($request->all(), [
                    'title' => 'required',
                    'content' => 'required',

        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
        
        $partnerContent = Partner::find($id);
        $partnerContent->title = $request->title;
        $partnerContent->content = $request->content;
        $partnerContent->save();
        
        return \Redirect::to('administrator/partner')->with('successMessage', 'Content Update successfully');
        }
    }



}
