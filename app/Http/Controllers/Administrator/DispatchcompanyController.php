<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Dispatchcompany;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class DispatchcompanyController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Dispatchcompany'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('DISPATCHCOMPANYDATA');
            \Session::push('DISPATCHCOMPANYDATA.searchDisplay', $searchDisplay);
            \Session::push('DISPATCHCOMPANYDATA.field', $field);
            \Session::push('DISPATCHCOMPANYDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('DISPATCHCOMPANYDATA.field');
            $sortType = \Session::get('DISPATCHCOMPANYDATA.type');
            $searchDisplay = \Session::get('DISPATCHCOMPANYDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTRY LIST  */
        $dispatchCompanyData = Dispatchcompany::getCompanyList($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Dispatch Companies";
        $data['contentTop'] = array('breadcrumbText' => 'Dispatch Companies', 'contentTitle' => 'Dispatch Companies', 'pageInfo' => 'This section allows you to manage dispatch companies');
        $data['pageTitle'] = "Dispatch Companies";
        $data['page'] = $dispatchCompanyData->currentPage();
        $data['dispatchCompanyData'] = $dispatchCompanyData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.dispatchcompany.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['page'] = !empty($page) ? $page : '1';
        if (!empty($id)) {
            $data['id'] = $id;
            $data['pageTitle'] = 'Edit Dispatch Company';
            $dispatchcompany = Dispatchcompany::find($id);
            $data['dispatchcompany'] = $dispatchcompany;
        } else {
            $data['id'] = 0;
            $data['pageTitle'] = 'Add Dispatch Company';
        }
        return view('Administrator.dispatchcompany.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $dispatchcompany = new Dispatchcompany;

        $validator = Validator::make($request->all(), [
                    'name' => 'required|unique:' . $dispatchcompany->table . ',name,' . $id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $dispatchcompany = Dispatchcompany::find($id);
                $dispatchcompany->modifiedBy = Auth::user()->id;
                $dispatchcompany->modifiedOn = Config::get('constants.CURRENTDATE');
            } else {
                $dispatchcompany->createdBy = Auth::user()->id;
                $dispatchcompany->createdOn = Config::get('constants.CURRENTDATE');
            }
            $dispatchcompany->name = $request->name;
            $dispatchcompany->save();
            $dispatchcompanyId = $dispatchcompany->id;

            return redirect('/administrator/dispatchcompany?page=' . $page)->with('successMessage', 'Dispatch company information saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Dispatchcompany::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/dispatchcompany/?page=' . $page)->with('successMessage', 'Dispatch company status changed successfully.');
            } else {
                return \Redirect::to('administrator/dispatchcompany/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/dispatchcompany/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
