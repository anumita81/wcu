<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Fillshipbox;
use App\Model\Fillship;
use App\Model\Fillshipitem;
use App\Model\Fillshipitemstatus;
use App\libraries\dbHelpers;
use App\Model\Deliverycompany;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\Warehousemessage;
use App\Model\Fillshipwarehousemessage;
use App\Model\Fillshipwarehouselocation;
use App\Model\Fillshipwarehousenotes;
use App\Model\User;
use Auth;
use Config;
use customhelper;
use Excel;
use PDF;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use Mail;
use Validator;

class FillnshipController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {
        $data = array();

        \Session::forget('FILLSHIPDATA');
        $searchShipmentArr = array(
            'status' => '',
            'unit' => '',
            'shipmentId' => '',
            'deliveryCompanyId' => '',
            'totalCost' => '',
            'receiver' => '',
            'paymentStatus' => '',
            'toCountry' => '',
            'toState' => '',
            'toCity' => ''
        );

        if (\Request::isMethod('post')) {

            /* GET POST VALUE  */
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchShipment = \Input::get('searchShipment', $searchShipmentArr);
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'createdOn');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('FILLSHIPDATA');
            \Session::push('FILLSHIPDATA.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('FILLSHIPDATA.searchDisplay', $searchDisplay);
            \Session::push('FILLSHIPDATA.searchByDate', $searchByDate);
            \Session::push('FILLSHIPDATA.searchShipment', $searchShipment);
            \Session::push('FILLSHIPDATA.field', $field);
            \Session::push('FILLSHIPDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchShipment'] = $searchShipment;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('FILLSHIPDATA.field');
            $sortType = \Session::get('FILLSHIPDATA.type');
            $searchByCreatedOn = \Session::get('FILLSHIPDATA.searchByCreatedOn');
            $searchByDate = \Session::get('FILLSHIPDATA.searchByDate');
            $searchShipment = \Session::get('FILLSHIPDATA.searchShipment');
            $searchDisplay = \Session::get('FILLSHIPDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchShipment'] = !empty($searchShipment) ? $searchShipment[0] : $searchShipmentArr;
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'shipmentId' => array('current' => 'sorting'),
            'deliveryCompany' => array('current' => 'sorting'),
            'totalCost' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';


        /* FETCH USER LIST  */
        $shipmentData = Fillship::getShipmentList($param);

        /* FETCH DELIVERY COMPANY LIST  */
        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();

        /* FETCH COUNTRY LIST  */
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

        $data['shipmentData'] = $shipmentData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['title'] = "Fill & Ship";
        $data['contentTop'] = array('breadcrumbText' => 'Fill And Ship', 'contentTitle' => 'Fill And Ship', 'pageInfo' => 'This section allows you to manage fill and ship shipments');
        $data['pageTitle'] = "Fill & Ship";
        $data['page'] = $shipmentData->currentPage();


        return view('Administrator.fillnship.index', $data);
    }

    /**
     * Method to fetch state list
     * @return array
     */
    public function getstatelist($countryId = '') {
        $country = Country::find($countryId);
        $stateList = array();
        if (isset($country->code) && !empty($country->code)) {

            $stateList = State::where('status', '1')->where('deleted', '0')->where('countryCode', $country->code)->orderby('name', 'asc')->get();
        }
        echo json_encode($stateList);
        exit;
    }

    /**
     * Method to fetch city list
     * @return array
     */
    public function getcitylist($stateid = '') {
        $state = State::find($stateid);
        $cityList = array();
        if (isset($state->code) && !empty($state->code)) {
            $cityList = City::where('status', '1')
                    ->where('deleted', '0')
                    ->where('stateCode', $state->code)
                    ->orderby('name', 'asc')
                    ->get();
        }
        echo json_encode($cityList);
        exit;
    }

    /**
     * Method used to print invoice
     * @param integer $id
     * @param integer $invoiceId 
     * @return string
     */
    public function printinvoice($id, $invoiceId) {
        /* Fetch  Invoice Data */
        $invoice = \App\Model\Invoice::find($invoiceId);
        $data['invoice'] = $invoice;
        $orderData = \App\Model\Order::where('shipmentId', $id)->where('type', 'fillship')->first();

        if (!empty($orderData))
            $data['orderNumber'] = $orderData->orderNumber;

        $data['pageTitle'] = "Print Invoice";
        if ($invoice->extraCostCharged == "Y")
            return view('Administrator.fillnship.extrainvoice', $data);
        else
            return view('Administrator.fillnship.printinvoice', $data);
    }

    /**
     * Method for add edit shipment
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();

        $fillnshipItem = new Fillshipitem;
        $fillnship = new Fillship;


        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Fill & Ship Shipments";
        $data['contentTop'] = array('breadcrumbText' => 'Fill & Ship', 'contentTitle' => 'Fill & Ship', 'pageInfo' => 'This section allows you to manage fill & ship shipments');
        $data['page'] = !empty($page) ? $page : '1';


        $data['id'] = $id;
        $data['adminRole'] = Auth::user()->userType;

        /*  FETCH FILLNSHIP DETAILS */
        $fillshipShipment = Fillship::getFillnShipDetails($id);
        $data['shipment'] = $fillshipShipment;

        /*  FETCH FILLNSHIP ITEM DETAILS */
        $data['shipment']['items'] = Fillshipitem::getItems($id);
        $data['shipment']['boxFill'] = Fillship::calculateBoxFill($fillshipShipment);
        //$data['shipment']['boxFill'] = $actualBoxFill = round(($fillshipShipment->totalChargeableWeight / $fillshipShipment->boxChargeableWeight) * 100, 2);

        $data['shipment']['warehouselocation'] = Fillship::getWareHouseLocationList($id);

        $data['shipment']['invoices'] = collect(DB::select('select * from stmd_invoices where shipmentId = ' . $id . ' and (type = "fillship" or  type ="itemReturn") and deleted="0"'));

        /* Fetch Warehouse Location History */
        $id = (int) $id;
        $data['shipment']['warehouseLocationLog'] = Fillship::getShipmentHistory($id);

        /* FETCH SHIPMENT STAGES */

        $data['statusLog'] = \App\Model\Fillshipshipmentstatuslog::getShipmentwiseStatusLog($id);


        /* Fetch Warehouse Message */
        $data['warehousemessages'] = Warehousemessage::where('status', '1')->get();

        /* FETCH DELIVERY COMPANY LIST  */
        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();


        $data['warehouseNotes'] = \App\Model\Fillshipwarehousenotes::where('fillshipId', $id)->orderBy('id', 'desc')->limit(5)->get()->toArray();

        /* FETCH CATEGORY LIST  */
        $data['categoryList'] = \App\Model\Sitecategory::getCategoryList('parent');

        /* FETCH SHIPPING METHOD LIST  */
        $data['shipment']['shippingMethod'] = \App\Model\Shippingmethods::where('shippingid', $data['shipment']['shippingMethodId'])->pluck('shipping')->toArray();

        /* FETCH PAYMENT METHOD LIST  */
        $data['shipment']['paymentMethod'] = \App\Model\Paymentmethod::where('id', $data['shipment']['paymentMethodId'])->where('deleted', '0')->where('status', '1')->pluck('paymentMethod')->toArray();
        $paymentMethodData = collect(\App\Model\Paymentmethod::orderby('orderby', 'asc')->get());
        $data['paymentMethodList'] = $paymentMethodData->mapWithKeys(function($item) {
            return [$item['id'] => $item['paymentMethod']];
        });
        
        $data['orderData'] = \App\Model\Order::where('shipmentId', $id)->where('type', 'fillship')->first();

        $data['packageDetailsData'] = \App\Model\Fillshipshipmentspackaging::where("shipmentId", $id)->with('packageaddedby')->get();

        /* Fetch package details comments data */
        $data['packageDetailsCommentData'] = \App\Model\Fillshippackagenotes::where('shipmentId', $id)->get();

        /* Calculate total chargebale weight of all packaging records */
        $packageTotalChargebleWeight = \App\Model\Fillshipshipmentspackaging::where('shipmentId', $id)->sum('weight');
        $itemTotalWeight = Fillshipitem::where('fillshipId',$id)->sum('weight');

        /* Get difference between total weight of deliveries and total chargeable weight of packaging records */
        $data['packageAcceptableLimit'] = \App\Model\Fillshipshipmentspackaging::getpackageLimit($packageTotalChargebleWeight, $itemTotalWeight);

        $data['shipment']['invoices'] = \App\Model\Invoice::where('shipmentId', $id)->where('type', 'fillShip')->where("deleted", "0")->get();
        $data['extraChargeExist'] = \App\Model\Invoice::checkIfExtraInvoiceExist($data['shipment']['invoices']);

        $settings = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();
        $data['chWeightFactor'] = $settings->settingsValue;

        //echo"<pre>"; print_r($data['shipment']); die;

        $data['pageTitle'] = "Fill & Ship Shipment Details";

        return view('Administrator.fillnship.edit', $data);
    }

    public function addproduct($shipmentId, $page = "1", Request $request) {

        if (\Request::isMethod('post')) {
            //print_r($request->all());
            $item = $request->shipment;
            $firstReceived = 1;
            $numItemsInArr = Fillshipitem::where("fillshipId", $shipmentId)->where("deleted","0")->get();
            $deliveryDetailsContent = '<table border="1px"><tr><th>Store</th><th>Product Description</th><th>Quantity</th><th>Total</th></tr>';
            $itemInWarehouse = 0;
            $itemStore = array();
            $replace = array();
            $totalVolumeWeight = $totalWeight = 0;
            $settings = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();
            $storeData = collect(\App\Model\Stores::get());
            $storeList = $storeData->mapWithKeys(function($item) {
                return [$item['id'] => $item['storeName']];
            });
            if ($numItemsInArr->count() > 0) {
                $firstReceived = 0;
            }
            if (!empty($numItemsInArr)) {
                foreach ($numItemsInArr as $eachItem) {
                    $storeName = "N/A";
                    if (!empty($storeList[$eachItem->storeId]))
                        $storeName = $storeList[$eachItem->storeId];
                    $deliveryDetailsContent.= '<tr>';
                    $deliveryDetailsContent.= '<td>' . $storeName . '</td>';
                    $deliveryDetailsContent.= '<td>' . $eachItem->itemName . '</td>';
                    $deliveryDetailsContent.= '<td>' . $eachItem->itemQuantity . '</td>';
                    $deliveryDetailsContent.= '<td>' . $eachItem->itemTotalCost . '</td>';
                    $deliveryDetailsContent.= '</tr>';
                    $itemInWarehouse++;

                    if ($storeName != "N/A" && !in_array($storeName, $itemStore)) {
                        $itemStore[] = $storeName;
                    }
                    $totalVolumeWeight += (($eachItem->length * $eachItem->width * $eachItem->height) / $settings->settingsValue);
                    $totalWeight += $eachItem->weight;
                }
            }
            if (!empty($item)) {
                for ($i = 0; $i < count($item['storeId']); $i++) {

                    $fillshipitem = new Fillshipitem;
                    $fillshipitem->fillshipId = $shipmentId;
                    $fillshipitem->storeId = $item['storeId'][$i];
                    $fillshipitem->siteCategoryId = $item['siteCategoryId'][$i];
                    $fillshipitem->siteSubCategoryId = $item['siteSubCategoryId'][$i];
                    $fillshipitem->siteProductId = $item['siteProductId'][$i];
                    $fillshipitem->itemName = $item['item'][$i];
                    $fillshipitem->itemPrice = $item['value'][$i];
                    $fillshipitem->itemQuantity = $item['quantity'][$i];
                    $fillshipitem->length = $item['length'][$i];
                    $fillshipitem->width = $item['width'][$i];
                    $fillshipitem->height = $item['height'][$i];
                    $fillshipitem->weight = $item['weight'][$i];
                    $itemVolumeWeight = 0;
                    $itemVolumeWeight = (($item['length'][$i] * $item['width'][$i] * $item['height'][$i]) / $settings->settingsValue);
                    $chargeableWeight = max($item['weight'][$i], $itemVolumeWeight);
                    $fillshipitem->itemWeight = round($chargeableWeight, 2);
                    $fillshipitem->itemTotalCost = $item['total'][$i];
                    $fillshipitem->deliveryCompanyId = $item['deliveryCompanyId'][$i];
                    $fillshipitem->receivedDate = $item['delivered'][$i];
                    $fillshipitem->trackingNumber = $item['trackingNumber'][$i];
                    $fillshipitem->tracking2 = $item['trackingNumber2'][$i];
                    $fillshipitem->deliveryNotes = $item['deliveryNote'][$i];
                    $fillshipitem->status = "received";

                    $totalVolumeWeight += $itemVolumeWeight;
                    $totalWeight += $item['weight'][$i];

                    $storeName = "N/A";
                    if (!empty($storeList[$item['storeId'][$i]]))
                        $storeName = $storeList[$item['storeId'][$i]];

                    $deliveryDetailsContent.= '<tr>';
                    $deliveryDetailsContent.= '<td>' . $storeName . '</td>';
                    $deliveryDetailsContent.= '<td>' . $item['item'][$i] . '</td>';
                    $deliveryDetailsContent.= '<td>' . $item['quantity'][$i] . '</td>';
                    $deliveryDetailsContent.= '<td>' . $item['total'][$i] . '</td>';
                    $deliveryDetailsContent.= '</tr>';
                    if ($storeName != "N/A" && !in_array($storeName, $itemStore)) {
                        $itemStore[] = $storeName;
                    }
                    $itemInWarehouse++;

                    $fillshipitem->save();
                }
                $deliveryDetailsContent .='</table>';
                //echo $totalVolumeWeight.' '.$totalWeight;
                //$totalItemWeight = Fillshipitem::where('fillshipId',$shipmentId)->where("deleted","0")->sum('itemWeight');
                $shipmentChargeableWeight = round(max($totalVolumeWeight, $totalWeight), 2);
                Fillship::where('id', $shipmentId)->update(["totalChargeableWeight" => $shipmentChargeableWeight]);
                if ($firstReceived == 1) {
                    $firstReceivedDate = Config::get('constants.CURRENTDATE');
                    $maxStorageDateSettings = \App\Model\Generalsettings::where('settingsKey', 'fill_and_ship_storage_days')->first();
                    $maxStorageDate = date('Y-m-d', strtotime('+' . $maxStorageDateSettings->settingsValue . ' days', time()));
                    Fillship::where('id', $shipmentId)->update(["maxStorageDate" => $maxStorageDate, "firstReceivedDate" => $firstReceivedDate]);
                }

                $shipmentInfo = Fillship::find($shipmentId);

                $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'fillnship_item_received')->first();
                $to = $shipmentInfo->fromEmail;

                $replace['[NAME]'] = $shipmentInfo->fromName;
                $link = Config::get('constants.frontendUrl') . 'login/';
                $replace['[WEBSITE_LINK]'] = '<a href="' . $link . '">Click here</a>';
                $replace['[SHIPMENTID]'] = "#" . $shipmentId;
                $replace['[BOXFILL]'] = '<strong>' . round(($shipmentInfo->totalChargeableWeight / $shipmentInfo->boxChargeableWeight) * 100, 2) . ' %</strong>';
                $replace['[DELIVERY_DETAILS]'] = $deliveryDetailsContent;
                customhelper::SendMail($emailTemplate, $replace, $to);

                $user = User::find($shipmentInfo->userId);
                $toMobile = trim($user->isdCode . $user->contactNumber);
                $smsTemplate = \App\Model\Smstemplate::where('templateKey', 'fillnship_item_received')->first();

                $replace['[ITEM]'] = $itemInWarehouse . " Item(s)";
                $replace['[STORE_LINK]'] = implode(',', $itemStore);
                $replace['[WEBSITE_LINK]'] = $link;
                $replace['[BOXFILL]'] = round(($shipmentInfo->totalChargeableWeight / $shipmentInfo->boxChargeableWeight) * 100, 2) . ' %';

                $isSendMsg = customhelper::sendMSG($toMobile, $replace, $smsTemplate);

                /* Save & Send Notification */
                User::sendPushNotification($user->id, 'fillnship_item_received', Auth::user()->id, $replace);
            }

            return \Redirect::to('administrator/fillnship/addedit/' . $shipmentId)->with('successMessage', 'Item added successfully');
        }

        $data = array();

        /* FETCH DELIVERY COMPANY LIST  */
        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();

        /* FETCH CATEGORY LIST  */
        $data['categoryList'] = \App\Model\Sitecategory::getCategoryList('parent');

        /* FETCH STORE LIST  */
        $data['storeList'] = \App\Model\Stores::where('deleted', '0')->where('status', '1')->orderby('storeName', 'asc')->get();

        $data['shipmentId'] = $shipmentId;
        $data['action'] = 'Add';
        $data['pageTitle'] = "Add New Product";
        return view('Administrator.fillnship.addproduct', $data);
    }

    public function editproduct($productId, $page = 1, Request $request) {

        $data = array();
        $productDetails = Fillshipitem::find($productId);

        if (\Request::isMethod('post')) {
            //print_r($request->all());
            $item = $request->shipment;

            if (!empty($item)) {
                for ($i = 0; $i < count($item['storeId']); $i++) {

                    $productDetails->storeId = $item['storeId'][$i];
                    $productDetails->siteCategoryId = $item['siteCategoryId'][$i];
                    $productDetails->siteSubCategoryId = $item['siteSubCategoryId'][$i];
                    $productDetails->siteProductId = $item['siteProductId'][$i];
                    $productDetails->itemName = $item['item'][$i];
                    $productDetails->itemPrice = $item['value'][$i];
                    $productDetails->itemQuantity = $item['quantity'][$i];
                    $productDetails->length = $item['length'][$i];
                    $productDetails->width = $item['width'][$i];
                    $productDetails->height = $item['height'][$i];
                    $productDetails->weight = $item['weight'][$i];
                    $settings = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();
                    $chargeableWeight = max($item['weight'][$i], (($item['length'][$i] * $item['width'][$i] * $item['height'][$i]) / $settings->settingsValue));
                    $productDetails->itemWeight = round($chargeableWeight, 2);
                    $productDetails->itemTotalCost = $item['total'][$i];

                    $productDetails->save();
                }
                $shipmentId = $productDetails->fillshipId;
                $totalItemWeight = Fillshipitem::where('fillshipId', $shipmentId)->where("deleted", "0")->sum('itemWeight');
                Fillship::where('id', $shipmentId)->update(["totalChargeableWeight" => $totalItemWeight]);
            }

            return \Redirect::to('administrator/fillnship/addedit/' . $shipmentId)->with('successMessage', 'Item Edited successfully');
        }


        /* FETCH DELIVERY COMPANY LIST  */
        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();

        /* FETCH CATEGORY LIST  */
        $data['categoryList'] = \App\Model\Sitecategory::getCategoryList('parent');

        /* FETCH SUBCATEGORY LIST */
        $data['subcategoryList'] = \App\Model\Sitecategory::getSubCategory($productDetails->siteCategoryId);

        /* FETCH STORE LIST  */
        $data['storeList'] = \App\Model\Stores::where('deleted', '0')->where('status', '1')->orderby('storeName', 'asc')->get();

        /* FETCH PRODUCT LIST */
        $data['productList'] = \App\Model\Siteproduct::getListBySubCatid($productDetails->siteSubCategoryId);

        $data['productId'] = $productId;
        $data['page'] = $page;
        $data['productDetails'] = $productDetails;

        return view('Administrator.fillnship.editproduct', $data);
    }

    public function notifycustomer($messageId, $shipmnentId) {
        $data = array();

        $data['messageId'] = $messageId;

        $data['shipmentId'] = $shipmnentId;

        $data['warehousemsg'] = Warehousemessage::select('message')->where('id', $messageId)->get()->toArray();



        return view('Administrator.fillnship.notifycustomer', $data);
    }

    public function savenotification($messageId, $shipmnentId) {
        $data = array();

        $warehouseMessage = new Fillshipwarehousemessage;
        $warehouseMsg = Warehousemessage::find($messageId);
        $warehouseMessage->fillshipId = $shipmnentId;
        $warehouseMessage->messageId = $messageId;
        $warehouseMessage->sentBy = Auth::user()->id; //
        $warehouseMessage->sentOn = Config::get('constants.CURRENTDATE');

        $user = Fillship::find($shipmnentId);
        $customer = User::find($user->userId);

        /* Save and Send Push Notification */
        $replacearr = array('[TOPIC]' => $warehouseMsg->name, '[NOTIFICATION]' => nl2br($warehouseMsg->message));
        User::sendPushNotification($user->userId, 'warehouse_message_notification', Auth::user()->id, $replacearr);

        /* ++++++++++ email functionality ++++++++ */
        $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'notification_admin')->first();
        $replace['[NAME]'] = $customer->firstName . " " . $customer->lastName;
        $replace['[NOTIFICATION]'] = $warehouseMsg->message;
        //   $replace['[NOTIFICATION]'] = nl2br($request->message);

        $to = $customer->email;
        $sendMail = customhelper::SendMail($emailTemplate, $replace, $to);
        /* ++++++++++ end of email functionality ++++ */

        if ($sendMail) {
            if ($warehouseMessage->save()) {
                return redirect()->back()->with('successMessage', 'Notification sent successfully');
            } else {
                return redirect()->back()->with('errorMessage', 'Record not added');
            }
        } else {
            return redirect()->back()->with('errorMessage', 'Notification not sent');
        }
    }

    public function addcomment($shipmentId, Request $request) {
        $data = array();

        $message = $request->comment;

        $shipmentnotes = new \App\Model\Fillshipwarehousenotes;

        $shipmentnotes->fillshipId = $shipmentId;
        $shipmentnotes->message = $message;
        $shipmentnotes->sentBy = Auth::user()->id;
        $shipmentnotes->sentOn = Config::get('constants.CURRENTDATE');

        $userdetail = \App\Model\UserAdmin::select(array('firstname', 'lastname', 'email'))->where('id', Auth::user()->id)->first()->toArray();


        if ($shipmentnotes->save()) {
            $userdetail = array_merge($userdetail, array('sentOn' => date('Y-m-d', strtotime($shipmentnotes->sentOn)), 'noteId' => $shipmentnotes->id, 'fillshipId' => $shipmentId));
            return json_encode($userdetail);
        } else {
            return 0;
        }
    }

    public function sendcomment($id, $shipmentId) {
        $data = array();

        $data['fillshipId'] = $shipmentId;

        $data['customerEmailList'] = \App\Model\UserAdmin::select(array('id', 'email'))->where(array('deleted' => '0', 'status' => '1'))->get()->toArray();

        return view('Administrator.fillnship.sendcomment', $data);
    }

    /**
     * Method used to fetch & display shipment notes
     * @param integer $notesId
     * @param integer $shipmentId
     * @return string
     */
    public function shownotes($notesId, $shipmentId) {
        $data = array();

        $data['fillshipId'] = $shipmentId;

        $data['notes'] = \App\Model\Fillshipwarehousenotes::select('message')->where('id', $notesId)->get()->toArray();

        return view('Administrator.fillnship.shownotes', $data);
    }

    public function saveandnotify($shipmentId, Request $request) {

        $shipmentnotes = new \App\Model\Fillshipwarehousenotes;

        $emailto[] = $request->admin;

        $shipmentnotes->fillshipId = $shipmentId;
        $shipmentnotes->message = $request->warehousemsg;
        $shipmentnotes->sentBy = Auth::user()->id;
        $shipmentnotes->sentOn = Config::get('constants.CURRENTDATE');

        $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'notification_admin')->first();
        $replace['[NAME]'] = "Admin";
        $replace['[NOTIFICATION]'] = $request->warehousemsg;

        foreach ($emailto[0] as $email) {
            $sendMail = customhelper::SendMail($emailTemplate, $replace, $email);
        }
        if ($sendMail) {
            if ($shipmentnotes->save()) {

                return redirect()->back()->with('successMessage', 'Notes saved and sent successfully');
            } else {

                return redirect()->back()->with('errorMessage', 'Record not added');
            }
        } else {
            return redirect()->back()->with('errorMessage', 'Notes not sent');
        }
    }

    /**
     * Method to fetch sub category list
     * @param integer $categoryId
     * @return json
     */
    public function getsubcategorylist($categoryId) {
        $subCategoryList = \App\Model\Sitecategory::getSubCategory($categoryId);

        echo json_encode($subCategoryList);
    }

    /**
     * Method to fetch sub category list
     * @param integer $categoryId
     * @return json
     */
    public function getproductlist($categoryId) {
        $productList = \App\Model\Siteproduct::getListBySubCatid($categoryId);

        echo json_encode($productList);
    }

    /**
     * Method to add new fillnship package details
     * @param object $request
     * @return boolean
     */
    public function addfillnshipitem(Request $request) {
        // $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.shipment'), Auth::user()->id);
        // if ($findRole['canEdit'] == 0) {
        //     return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        // }

        if (\Request::isMethod('post')) {
            $shipmentItemData = array();

            //print_r($request->all()); die;

            for ($i = 0; $i < count($request->data); $i++) {
                $name = $request->data[$i]['name'];

                if ($name == 'shipmentId')
                    $shipmentId = $request->data[$i]['value'];

                $shipmentItemData[$name] = $request->data[$i]['value'];
            }

            /* SAVE FILLSHIP DETAILS */
            Fillshipitem::insert($shipmentItemData);

            /* CALCULATE TOTAL ITEM COST */
            //Fillship::calculateCost($shipmentId);

            return 1;
        }
    }

    /**
     * Method used to delete fillnship item
     * @param integer $id
     * @return boolean
     */
    public function deletefillnshipitem($id) {
        // $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.shipment'), Auth::user()->id);
        // if ($findRole['canEdit'] == 0) {
        //     return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        // }

        if (!empty($id)) {
            $createrModifier = Auth::user()->email;
            if (Fillshipitem::deleteRecord($id, $createrModifier)) {
                /* CALCULATE TOTAL ITEM COST */
                $shipmentId = Fillshipitem::find($id)->fillshipId;
                $settings = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();
                $numItemsInArr = Fillshipitem::where("fillshipId", $shipmentId)->where("deleted","0")->get();
                $totalVolumeWeight = $totalWeight = 0;
                if (!empty($numItemsInArr)) {
                    foreach ($numItemsInArr as $eachItem) {
                        $totalVolumeWeight += (($eachItem->length * $eachItem->width * $eachItem->height) / $settings->settingsValue);
                        $totalWeight += $eachItem->weight;
                    }
                }
                
                $shipmentChargeableWeight = round(max($totalVolumeWeight, $totalWeight), 2);               
                Fillship::where('id', $shipmentId)->update(["totalChargeableWeight" => $shipmentChargeableWeight]);
                // shipment::calculateCost($shipmentItem->shipmentId);

                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Method used to fetch & display package details
     * @param integer $id
     * @return string
     */
    public function getpackagedetails($id) {
        // $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.shipment'), Auth::user()->id);
        // if ($findRole['canEdit'] == 0) {
        //     return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        // }
        /* FETCH CATEGORY LIST  */
        $data['categoryList'] = \App\Model\Sitecategory::getCategoryList('parent');

        /* FETCH STORE LIST  */
        $data['storeList'] = \App\Model\Stores::where('deleted', '0')->where('status', '1')->orderby('storeName', 'asc')->get();

        /*  FETCH shipment ITEM DETAILS */
        $data['shipment'] = Fillship::select('shippingMethod', 'shippingMethodId', 'userId')->where('id', $id)->first();


        /*  FETCH shipment ITEM DETAILS */
        $data['shipment']['items'] = Fillshipitem::getItems($id);

        return view('Administrator.fillnship.packagedetails', $data);
    }

    /**
     * Method used to delete fillnship item
     * @param integer $id
     * @return boolean
     */
    public function deleteshipmentitem($id) {

        // $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.shipment'), Auth::user()->id);
        // if ($findRole['canEdit'] == 0) {
        //     return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        // }

        if (!empty($id)) {
            $createrModifier = Auth::user()->email;
            if (Fillshipitem::deleteRecord($id, $createrModifier)) {
                /* CALCULATE TOTAL ITEM COST */
                $shipmentItem = new Fillshipitem;
                $shipmentItem = Fillshipitem::find($id);
                //shipment::calculateCost($shipmentItem->shipmentId);

                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Method used to update fillnship item status
     * @param integer $id
     * @return boolean
     */
    public function updateitemstatus(Request $request) {

        // $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.shipment'), Auth::user()->id);
        // if ($findRole['canEdit'] == 0) {
        //     return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        // }


        $shipmentItemIds = $request->shipmentItemIds;
        $status = $request->status;



        if (!empty($shipmentItemIds)) {
            $shipmentItemArr = explode('^', $shipmentItemIds);

            foreach ($shipmentItemArr as $id) {
                $shipmentItem = new Fillshipitem;
                $shipmentItem = Fillshipitem::find($id);

                /* INSERT DATA INTO STATUS LOG TABLE */
                $shipmentitemstatus = new Fillshipitemstatus;
                $shipmentitemstatus->fillshipId = $shipmentItem->id;
                $shipmentitemstatus->fillshipItemId = $id;
                $shipmentitemstatus->oldStatus = $shipmentItem->status;
                $shipmentitemstatus->status = $status;
                $shipmentitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
                $shipmentitemstatus->save();

                /* UPDATE shipment ITEM TABLE */
                $shipmentItem->status = $status;
                $shipmentItem->save();
            }



            return 1;
        }
        return 0;
    }

    /**
     * Method used to display and update receive package info
     * @param integer $id
     * @return string
     */
    public function receivepackage($id, $status, Request $request) {


        if (\Request::isMethod('post')) {
            //print_r($request->all()); die;

            if (!empty($id)) {

                $shipmentItemData = array();
                $shipmentData = array();

                $storageDays = \App\Model\Generalsettings::where('settingsKey', 'fill_and_ship_storage_days')->first()->settingsValue;

                $shipmentItem = Fillshipitem::find($id);

                //Fetch FillShip Status Log and Update

                $statusLog = \App\Model\Fillshipshipmentstatuslog::where('status', 'in_warehouse')->where('fillshipShipmentId', $request->fillshipId)->get()->toArray();
                $shipmentStatus = new \App\Model\Fillshipshipmentstatuslog;

                if (count($statusLog) == 0) {
                    $shipmentStatus->fillshipShipmentId = $request->fillshipId;
                    $shipmentStatus->oldStatus = 'none';
                    $shipmentStatus->status = 'in_warehouse';
                    $shipmentStatus->updatedOn = Config::get('constants.CURRENTDATE');
                    $shipmentStatus->save();
                }

                /* GET FILLSHIP DETAILS OF FIRST RECEIVED ITEM */

                $isFirstItem = Fillshipitem::select(DB::raw("count(id) as count"))->where('fillshipId', $shipmentItem->fillshipId)->whereNotNull('receivedDate')->get()->toArray();

                //print_r($isFirstItem[0]['count'])  ;

                /* GET FILLSHIP DETAILS OF FIRST RECEIVED ITEM */

                //print_r($request->all()); die;

                if ($isFirstItem[0]['count'] == 0) {


                    $shipmentItemData['status'] = $status;
                    $shipmentItemData['trackingNumber'] = $request->trackingNumber;
                    $shipmentItemData['tracking2'] = $request->tracking2;
                    $shipmentItemData['deliveryCompanyId'] = $request->deliveryCompanyId;
                    $shipmentItemData['receivedDate'] = \Carbon\Carbon::parse($request->receivedDate)->format('Y-m-d');
                    $shipmentItemData['deliveryNotes'] = $request->deliveryNotes;
                    $shipmentItemData['itemWeight'] = $request->itemWeight;

                    $update = Fillshipitem::where('id', $id)->update($shipmentItemData);


                    /* CALCULATION OF MAX STORAGE DATE AND STORAGE CHARGE */
                    $shipmentData['maxStorageDate'] = \Carbon\Carbon::parse($request->receivedDate)->addDays($storageDays);
                    $shipmentData['firstReceivedDate'] = \Carbon\Carbon::parse($request->receivedDate)->addDays($storageDays);


                    Fillship::where('id', $request->fillshipId)->update($shipmentData);
                } else {

                    $shipmentItemData['status'] = $status;
                    $shipmentItemData['trackingNumber'] = $request->trackingNumber;
                    $shipmentItemData['deliveryCompanyId'] = $request->deliveryCompanyId;
                    $shipmentItemData['receivedDate'] = \Carbon\Carbon::parse($request->receivedDate)->format('Y-m-d');
                    $shipmentItemData['deliveryNotes'] = $request->deliveryNotes;
                    $shipmentItemData['itemWeight'] = $request->itemWeight;

                    $update = Fillshipitem::where('id', $id)->update($shipmentItemData);
                }

                /* INSERT DATA INTO STATUS LOG TABLE */

                $shipmentitemstatus = new Fillshipitemstatus;
                $shipmentitemstatus->fillshipId = $request->fillshipId;
                $shipmentitemstatus->fillshipItemId = $id;
                $shipmentitemstatus->oldStatus = $shipmentItem->status;
                $shipmentitemstatus->status = $status;
                $shipmentitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
                $shipmentitemstatus->save();

                return redirect()->back();
            }
        }

        $data['id'] = $id;
        $data['status'] = $status;
        $data['pageTitle'] = "Receive Package in Warehouse";


        /*  FETCH Fillship ITEM DETAILS */
        //$data['shipment'] = Fillship::where('id', $id)->first();

        /*  FETCH shipment ITEM DETAILS */
        $data['shipmentItems'] = Fillshipitem::find($id);



        /* FETCH DELIVERY COMPANY LIST  */
        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();

        /* FETCH PRODUCT WEIGHT  */
        if ($data['shipmentItems']['itemWeight'] == 0.00) {
            $data['shipmentItems']['itemWeight'] = \App\Model\Siteproduct::where('status', '1')->where('id', $data['shipmentItems']['siteProductId'])->pluck('weight')->first();
        }


        return view('Administrator.fillnship.receivepackage', $data);
    }

    /**
     * Method used to fetch & display the shipment details
     * @param integer $id
     * @return string
     */
    public function viewtrackingdetails($id) {
        $data['pageTitle'] = "View Tracking Details";
        /*  FETCH PROCUREMENT DETAILS */
        $data['shipment'] = Fillshipitem::find($id);
        $data['deliverycompany'] = Fillshipitem::find($id)->deliverycompany()->first();

        return view('Administrator.fillnship.viewtrackingdetails', $data);
    }

    /**
     * Method used to print location label
     * @param integer $id
     * @param integer $page 
     * @return html
     */
    public function printlocationlabel($id, $page = 0) {
        $data['pageTitle'] = "Print Location Label";

        $data['warehouselocation'] = Fillship::getWareHouseLocationList($id);
        $data['shipment'] = Fillship::where('id', $id)->first(['id', 'userId']);
        $data['unit'] = User::find($data['shipment']->userId)->unit;

        return view('Administrator.fillnship.locationlabel', $data);
    }

    /**
     * Method used to add/edit warehouse location
     * @param integer $id
     * @param integer $shipmentid
     * @param object $request
     * @return html
     */
    public function addeditlocation($id, $shipmentid, Request $request) {
        if (\Request::isMethod('post')) {

            $warehouseLocation = new Fillshipwarehouselocation;

            if (!empty($id)) {
                $warehouseLocation = Fillshipwarehouselocation::find($id);
            } else {
                $warehouseLocation->fillshipId = $shipmentid;
                $warehouseLocation->createdBy = Auth::user()->id;
                $warehouseLocation->createdOn = Config::get('constants.CURRENTDATE');
            }

            $warehouseLocation->warehouseRowId = $request->warehouseRowId;
            $warehouseLocation->warehouseZoneId = $request->warehouseZoneId;

            if ($warehouseLocation->save()) {
                return 1;
            } else {
                return 0;
            }
        }

        /* SET DATA FOR VIEW  */
        $data['pageTitle'] = "Edit Warehouse Location";
        $data['id'] = $id;
        $data['shipmentId'] = $shipmentid;
        /* FETCH WAREHOUSE ROW LIST  */
        $data['warehouseRowList'] = \App\Model\Warehouselocation::where('type', 'R')->where('status', '1')->orderby('name', 'asc')->get();

        if (!empty($id)) {
            $warehouseData = Fillshipwarehouselocation::find($id);
            if (!empty($warehouseData->warehouseZoneId))
                $data['warehouseZoneList'] = \App\Model\Warehouselocation::where('parentId', $warehouseData->warehouseRowId)->where('status', '1')->get();

            $data['warehouseData'] = $warehouseData;
        }

        return view('Administrator.fillnship.addeditlocation', $data);
    }

    /**
     * Method used to delete warehouse location
     * @param integer $id
     * @param integer $shipmentid
     * @return boolean
     */
    public function deletelocation($id, $shipmentid) {
        if (!empty($id)) {
            $warehouseData = Fillshipwarehouselocation::find($id);
            if ($warehouseData->delete()) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Method for fetcing warehouse zone list
     * @param $rowId
     * @return string
     */
    public function getwarehousezonelist($rowId = '') {
        $zoneList = array();
        if (!empty($rowId)) {
            $zoneList = \App\Model\Warehouselocation::where('parentId', $rowId)->where('status', '1')->orderby(DB::raw("ABS('name')"), 'asc')->get();
        }
        echo json_encode($zoneList);
    }

    /**
     * Method used to fetch warehouse locations
     * @param integer $id
     * @param integer $page
     * @return html
     */
    public function getwarehouselocations($id, $page = 0) {
        $data['warehouselocation'] = Fillship::getWareHouseLocationList($id);
        print_r($data['warehouselocation']);
        /* Fetch Warehouse Location History */
        $id = (int) $id;
        $data['warehouseLocationLog'] = Fillship::getShipmentHistory($id);
        $data['id'] = $id;

        return view('Administrator.fillnship.warehouselocations', $data);
    }

    /**
     * Method used to print delivery labels
     * @param integer $id
     * @param integer $deliveryId 
     * @return html
     */
    public function printdeliverylabel($id) {
        /* FETCH SHIPMENT PACKAGES DETAILS */
        $shipment = Fillship::where('id', $id)->first(['id', 'userId']);

        $data['shipment']['id'] = $shipment->id;
        $data['shipment']['userId'] = $shipment->userId;

        $data['shipment']['pacakges'] = Fillshipitem::where('fillshipId', $id)->where('deleted', '0')->sum('itemQuantity');

        /* FETCH SHIPMENT PACKAGES DETAILS */
        $data['items'] = Fillshipitem::where('fillshipId', $id)->where('deleted', '0')->get();

        /* SET DATA FOR VIEW  */
        $data['pageTitle'] = "Print Delivery Label";
        $data['warehouselocation'] = Fillship::getWareHouseLocationList($id);
        $data['shipment']['userUnit'] = User::find($shipment->userId)->unit;

        return view('Administrator.fillnship.deliverylabel', $data);
    }

    public function updatestage($shipmentId, Request $request) {
        if (!empty($request->status)) {
            $statusPost = $request->status;
            foreach ($statusPost as $eachStatus => $dates) {
                if ($dates != '') {
                    $shipmentStatusData = \App\Model\Fillshipshipmentstatuslog::where("fillshipShipmentId", $shipmentId)->orderBy("id", "desc")->first();
                    $shipmentStatus = new \App\Model\Fillshipshipmentstatuslog;
                    $shipmentStatus->fillshipShipmentId = $shipmentId;
                    $shipmentStatus->oldStatus = (!empty($shipmentStatusData->status) ? $shipmentStatusData->status : 'none');
                    $shipmentStatus->status = $eachStatus;
                    $shipmentStatus->updatedOn = date('Y-m-d h:i:s', strtotime($dates));
                    $shipmentStatus->save();

                    if ($eachStatus == 'delivered') {
                        Fillship::where('id', $shipmentId)->update(['status' => 'delivered', 'deliveredOn' => Config::get('constants.CURRENTDATE')]);
                    }
                }
            }

            return redirect()->back()->with('successMessage', 'Stage updated successfully.');
        } else {
            return redirect()->back()->withErrors('Stage not updated');
        }
    }

    /**
     * This function is used to open popup when startpackaging is clicked
     * @param type $shipmentId
     * @param type $page
     * @return type
     */
    public function validatepackage($shipmentId, $page) {

        $data['shipmentId'] = $shipmentId;
        $data['deliveryCount'] = '1';

        return view('Administrator.fillnship.validatepackage', $data);
    }

    /**
     * This function is used to enable packaging if all condition matches
     * @param <int> $shipmentId
     * @param <int> $page
     * @param Request $request
     * @return type
     */
    public function startpackaging($shipmentId, $page, Request $request) {

        $packageShipment = $request->input('packageShipment');
        if ($packageShipment == $shipmentId) {

            $dataObj = Fillship::where('id', $shipmentId)->update(array('allowedPackaging' => 'Y', 'packed' => 'started'));
            return json_encode(array('message' => 'success'));
        } else
            return json_encode(array('message' => 'error'));
    }

    /**
     * This is used to Add new packaging records
     * @param <int> $shipmentId
     * @param <int> $page
     * @param Request $request
     * @return type
     */
    public function addpackage($shipmentId, $page, Request $request) {

        $validator = Validator::make($request->all(), [
                    'length' => 'required|numeric',
                    'width' => 'required|numeric',
                    'height' => 'required|numeric',
                    'weight' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            $shipmentPackageObj = New \App\Model\Fillshipshipmentspackaging;
            $chargeableWeightFactor = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();

            $shipmentPackageObj->length = $request->input('length');
            $shipmentPackageObj->width = $request->input('width');
            $shipmentPackageObj->height = $request->input('height');
            $shipmentPackageObj->weight = $request->input('weight');
            $shipmentPackageObj->shipmentId = $shipmentId;
            $shipmentPackageObj->chargeableWeight = max($shipmentPackageObj->weight, (($shipmentPackageObj->length * $shipmentPackageObj->width * $shipmentPackageObj->height) / $chargeableWeightFactor->settingsValue));
            $shipmentPackageObj->createdBy = Auth::user()->id;
            if ($shipmentPackageObj->save())
                return json_encode(array('message' => 'success'));
            else
                return json_encode(array('message' => 'error'));
            /* if ($shipmentPackageObj->save())
              return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/' . $page)->with('successMessage', 'Packaging data saved.');
              else
              return \Redirect::to('administrator/shipments/addedit/' . $shipmentId . '/' . $page)->with('errorMessage', 'Something went wrong.'); */
        }
    }

    /**
     * This function is used to update or delete packaging records
     * @param <int> $shipmentId
     * @param <int> $page
     * @param Request $request
     * @return type
     */
    public function editpackage($shipmentId, $page, Request $request) {
        //print_r($request->all());exit;
        $updateArr = $request->input('update');
        /* Chargeable weight factor from settings */
        $chargeableWeightFactor = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();
        $recordUpdate = 0;
        foreach ($updateArr as $eachId => $eachArr) {

            if (!empty($eachArr['cb'])) {
                $shipmentPackageObj = New \App\Model\Fillshipshipmentspackaging;
                $shipmentPackageObj = $shipmentPackageObj->find($eachId);

                if ($request->input('action') == 'update') {
                    $shipmentPackageObj->length = $eachArr['length'];
                    $shipmentPackageObj->width = $eachArr['width'];
                    $shipmentPackageObj->height = $eachArr['height'];
                    $shipmentPackageObj->weight = $eachArr['weight'];
                    $shipmentPackageObj->chargeableWeight = max($shipmentPackageObj->weight, (($shipmentPackageObj->length * $shipmentPackageObj->width * $shipmentPackageObj->height) / $chargeableWeightFactor->settingsValue));

                    if ($shipmentPackageObj->save())
                        $recordUpdate ++;
                }
                else if ($request->input('action') == 'delete') {
                    if ($shipmentPackageObj->delete())
                        $recordUpdate ++;
                }
            }
        }
        if ($request->input('fromSource') == 'ajax') {
            if ($recordUpdate > 0)
                return json_encode(array('message' => 'success'));
            else
                return json_encode(array('message' => 'error'));
        }
        else {
            if ($recordUpdate > 0)
                return redirect()->back()->with('successMessage', 'Record updated/deleted successfully');

            return redirect()->back()->with('errorMessage', 'Please select any record to update/delete');
        }
    }

    /**
     * This function is used to add packaging comments
     * @param <int> $shipmentId
     * @param <int> $page
     * @param Request $request
     * @return type
     */
    public function addfillshippackagenotes($shipmentId, $page, Request $request) {

        if ($request->input('packageDetails') != '') {
            $dataObj = new \App\Model\Fillshippackagenotes;
            $dataObj->shipmentId = $shipmentId;
            $dataObj->message = $request->input('packageDetails');
            $dataObj->user = Auth::user()->email;
            //$dataObj->user = Auth::user()->email;

            $dataObj->save();
            return redirect()->back()->with('successMessage', 'Record added successfully');
        }
        return redirect()->back()->with('errorMessage', 'Please enter package details');
    }

    public function updatepackedstatus($shipmentid) {

        Fillship::where('id', $shipmentid)->update(['packed' => 'packing_complete', "status" => "shipped"]);
        return 1;
    }

    /**
     * This function is used to print manifest
     * @param <int> $shipmentId
     * @param <int> $page
     * @return type
     */
    public function printmanifest($shipmentId, $page) {

        $data = array();
        /* Fetch shipemnt record */
        $shipmentData = Fillship::find($shipmentId)->toArray();

        $data['shipmentData'] = $shipmentData;
        /* FETCH Country LIST  */
        $countryData = collect(Country::where('status', '1')->orderby('name', 'asc')->get());
        $data['countryList'] = $countryData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        /* FETCH State LIST  */
        $stateData = collect(State::where('status', '1')->where('deleted', '0')->orderby('name', 'asc')->get());
        $data['stateList'] = $stateData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        /* FETCH City LIST  */
        $cityData = collect(City::where('status', '1')->where('deleted', '0')->orderby('name', 'asc')->get());
        $data['cityList'] = $cityData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });

        /* Fetch packaging records */
        $packageDetailsData = Fillshipitem::where("fillshipId", $shipmentId)->where('deleted', '0')->get();
        $data['packageDetailsData'] = $packageDetailsData;
        $packagingDetailsData = \App\Model\Fillshipshipmentspackaging::where("shipmentId", $shipmentId)->get();
        $data['packageCount'] = $packagingDetailsData->count();
        $data['orderData'] = \App\Model\Order::where('shipmentId', $shipmentId)->where('type', 'fillship')->first();
        $data['userInfo'] = \App\Model\User::find($shipmentData['userId']);


        return view('Administrator.fillnship.manifest', $data);
    }

    public function getshippinglabelfields($shipmentId, $page) {

        $data['shipmentId'] = $shipmentId;
        $data['page'] = $page;
        return view('Administrator.fillnship.shippinglabelfields', $data);
    }

    /**
     * This function is used to print shipping labels
     * @param <int>  $shipmentId
     * @param <int> $page
     * @param Request $request
     * @return type
     */
    public function printshippinglabel($shipmentId, $page, Request $request) {

        $data = array();
        /* Fetch shipment record */
        $shipmentData = Fillship::find($shipmentId)->toArray();

        $data['shipmentData'] = $shipmentData;


        $shippingMethodDetails = \App\Model\Shippingmethods::select('shipping', 'companyLogo')->where('shippingid', $data['shipmentData']['shippingMethodId'])->first();
        $deliveryShippingMethod['name'] = $shippingMethodDetails->shipping;
        $deliveryShippingMethod['logo'] = $shippingMethodDetails->companyLogo;


        /* FETCH Country LIST  */
        $countryData = collect(Country::where('status', '1')->orderby('name', 'asc')->get());
        $data['countryList'] = $countryData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        /* FETCH State LIST  */
        $stateData = collect(State::where('status', '1')->where('deleted', '0')->orderby('name', 'asc')->get());
        $data['stateList'] = $stateData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        /* FETCH City LIST  */
        $cityData = collect(City::where('status', '1')->where('deleted', '0')->orderby('name', 'asc')->get());
        $data['cityList'] = $cityData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });

        /* Fetch packaging records */
        //$packageDetailsData = Fillshipitem::where("fillshipId", $shipmentId)->where('deleted','0')->get();
        $data['orderData'] = \App\Model\Order::where('shipmentId', $shipmentId)->where('type', 'fillship')->first();
        $data['userInfo'] = \App\Model\User::find($shipmentData['userId']);
        //$data['packageDetailsData'] = $packageDetailsData;
        $packagingDetailsData = \App\Model\Fillshipshipmentspackaging::where("shipmentId", $shipmentId)->get();
        $data['packageDetailsData'] = $packagingDetailsData;
        $data['packageCount'] = $packagingDetailsData->count();
        $data['deliveryShippingMethod'] = $deliveryShippingMethod;
        #dd($data['delivery']['deliveries'][10]['id']);

        $data['contents'] = $request->input('contents');
        $data['type'] = $request->input('type');
        //\App\Model\Shipmentlabelprintlog::logEntry($shipmentId, 'Shipping Label');
        return view('Administrator.fillnship.shippinglabel', $data);
    }

    /**
     * Method used to print Red Star label Pop up
     * @param integer $shipmentId
     * @param integer $page 
     * @return html
     */
    public function redstarlabel($shipmentId, $page) {

        $data = array();
        $shipmentData = Fillship::find($shipmentId)->toArray();

        $data['shipmentData'] = $shipmentData;

        /* FETCH Country LIST  */
        $countryData = collect(Country::where('status', '1')->orderby('name', 'asc')->get());
        $data['countryList'] = $countryData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        /* FETCH State LIST  */
        $stateData = collect(State::where('status', '1')->where('deleted', '0')->orderby('name', 'asc')->get());
        $data['stateList'] = $stateData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        /* FETCH City LIST  */
        $cityData = collect(City::where('status', '1')->where('deleted', '0')->orderby('name', 'asc')->get());
        $data['cityList'] = $cityData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });

        $packageDetailsData = Fillshipitem::where("fillshipId", $shipmentId)->where('deleted', '0')->get();
        $data['itemCount'] = $packageDetailsData->count();
        $packagingDetailsData = \App\Model\Fillshipshipmentspackaging::where("shipmentId", $shipmentId)->get();
        $data['packageCount'] = $packagingDetailsData->count();
        $data['packageDetailsData'] = $packagingDetailsData;

        $data['userInfo'] = \App\Model\User::find($shipmentData['userId']);
        /* GET ORDER ID FROM SHIPMENT ID */
        $orderId = \App\Model\Order::where('shipmentId', $shipmentId)->where('type', 'fillship')->first();
        $data['orderId'] = $orderId->orderNumber;
        //\App\Model\Shipmentlabelprintlog::logEntry($shipmentId, 'Red Star Label');
        return view('Administrator.fillnship.redstarlabel', $data);
    }

    /**
     * Method used to print DHL Label Pop up
     * @param integer $shipmentId
     * @param integer $page 
     * @return html
     */
    public function dhl($shipmentId, $page) {

        $data = array();
        $shipmentData = Fillship::find($shipmentId)->toArray();
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

        $data['shipmentData'] = $shipmentData;

        $packageDetailsData = \App\Model\Fillshipshipmentspackaging::where("shipmentId", $shipmentId)->get();
        $data['packageDetailsData'] = $packageDetailsData;
        $packagingDetailsData = \App\Model\Fillshipshipmentspackaging::where("shipmentId", $shipmentId)->get();
        $data['packageCount'] = $packagingDetailsData->count();
        $data['userInfo'] = \App\Model\User::find($shipmentData['userId']);
        //dd($packageDetailsData);
        //\App\Model\Shipmentlabelprintlog::logEntry($shipmentId, 'DHL Label');
        return view('Administrator.fillnship.dhl', $data);
    }

    function func_dhl_filter_output($ab_response) {
        $ab_response = explode("
        ", $ab_response);

        $_ab_response = '';
        foreach ($ab_response as $k => $v) {
            $elem = trim($v);
            if (strlen($elem) > 4 && $elem != '0fe8')
                $_ab_response .= $elem;
        }
        $ab_response = $_ab_response;
        return $ab_response;
    }

    /**
     * Method used to print DHL Label functionality
     * @param integer $page 
     * @return html
     */
    public function dhlpost($page = 1, Request $request) {
        if (\Request::isMethod('post')) {
            $getDefaultCurrency = \App\Model\Currency::getDefaultCurrency();
            $currenctSymbol = $getDefaultCurrency[0]->code;

            $str = $request->data;
            $dataExtract = explode("&", $str);
            $datafromAddress = urldecode(str_replace('fromAddress=', '', $dataExtract[2]));
            $datatoAddress = urldecode(str_replace('toAddress=', '', $dataExtract[4]));
            $datafromAddress2 = urldecode(str_replace('', '', $dataExtract[4]));
            $datatoAddress2 = str_replace('toAddress2=', '', $dataExtract[5]);
            $datafromCountry = $request->input('fromCountry');
            $datatoCountry = $request->input('toCountry');
            $datafromState = $request->input('fromState');
            $datatoState = $request->input('toState');
            $datafromCity = $request->input('fromCity');
            $datatoCity = $request->input('toCity');
            $datafromZip = urldecode(str_replace('fromZip=', '', $dataExtract[12]));
            $datatoZip = urldecode(str_replace('toZip=', '', $dataExtract[13]));
            $datafromPhone = urldecode(str_replace('fromPhone=', '', $dataExtract[14]));
            $datatoPhone = urldecode(str_replace('toPhone=', '', $dataExtract[15]));
            $datafromEmail = urldecode(str_replace('fromEmail=', '', $dataExtract[16]));
            $datatoEmail = urldecode(str_replace('toEmail=', '', $dataExtract[17]));
            $datafromName = urldecode(str_replace('fromName=', '', $dataExtract[18]));
            $datatoName = urldecode(str_replace('toName=', '', $dataExtract[19]));
            $datafromCompanyName = urldecode(str_replace('fromCompanyName=', '', $dataExtract[20]));
            $datatoCompanyName = urldecode(str_replace('toCompanyName=', '', $dataExtract[21]));
            $datashipmentId = urldecode(str_replace('invoiceId=', '', $dataExtract[22]));
            $piecesCount = urldecode(str_replace('piecesCount=', '', $dataExtract[23]));
            $total_weight = urlencode(str_replace('grossWeight=', '', $dataExtract[24]));
            $total_Chweight = urlencode(str_replace('grossChWeight=', '', $dataExtract[25]));
            $totalPrice = urlencode(str_replace('totalPrice=', '', $dataExtract[26]));

            $data_width = $request->width;
            $data_height = $request->height;
            $data_length = $request->length;
            $data_weight = $request->weight;
            $data_cweight = $request->cweight;
            $data_contents = $request->contents;


            $datatoCountryQuery = \App\Model\Country::where('name', $datatoCountry)->get();
            if (count($datatoCountryQuery)) {
                $datatoCountryCode = $datatoCountryQuery[0]->code;
            } else {
                $datatoCountryCode = '';
            }

            $datatoStateQuery = \App\Model\State::where('name', $datatoState)->get();
            if (count($datatoStateQuery)) {
                $datatoStateDivisionCode = $datatoStateQuery[0]->code;
            } else {
                $datatoStateDivisionCode = '';
            }

            $datafromCountryQuery = \App\Model\Country::where('name', $datafromCountry)->get();
            if (count($datafromCountryQuery)) {
                $datafromCountryCode = $datafromCountryQuery[0]->code;
            } else {
                $datafromCountryCode = '';
            }

            $datafromStateQuery = \App\Model\State::where('name', $datafromState)->get();
            if (count($datafromStateQuery)) {
                $datafromStateDivisionCode = $datafromStateQuery[0]->code;
            } else {
                $datafromStateDivisionCode = '';
            }

            if ($datatoCompanyName == '') {
                $datatoCompanyName = '-';
            }
            $datafromAddress = substr($datafromAddress, 0, 35);
            $datatoAddress = substr($datatoAddress, 0, 35);
            $message_ref = '';
            for ($i = 0; $i < 30; $i++) {
                $message_ref .= rand(0, 9);
            }
            $message_time = date("Y-m-d") . "T" . date("H:i:sP");
            $ab_date = date("Y-m-d", strtotime("+1 day"));

            $logo_image = base64_encode(file_get_contents(asset('public/administrator/img/logo.png')));
            if ($total_weight == '0.00') {
                $total_weight = round($data_weight[0], 1);
            } else {
                $total_weight = round($total_weight, 1);
            }

            $pieces = '';

            for ($i = 0; $i < $piecesCount; $i++) {
                $no = $i + 1;



                $data_width1 = round($data_width[$i], 1);
                $data_height1 = round($data_height[$i], 1);
                $data_length1 = round($data_length[$i], 1);
                $data_weight1 = round($data_weight[$i], 1);
                $data_cweight1 = round($data_cweight[$i], 1);

                $pieces .= <<<EOT
<Piece>
<PieceID>$no</PieceID>
<PackageType>EE</PackageType>
<Weight>$data_weight1</Weight>
<DimWeight>$data_cweight1</DimWeight>
<Width>$data_width1</Width>
<Height>$data_height1</Height>
<Depth>$data_length1</Depth>
<PieceContents>$data_contents[$i]</PieceContents>
</Piece>
EOT;
            }


            $query = <<<EOT
<?xml version="1.0" encoding="ISO-8859-1"?>
<req:ShipmentValidateRequest xmlns:req="http://www.dhl.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dhl.com ship-val-req.xsd">
<Request>
<ServiceHeader>
<MessageTime>$message_time</MessageTime>
<MessageReference>$message_ref</MessageReference>
<SiteID>AmericanAirS</SiteID>
<Password>7UcSzTXnsR</Password>
</ServiceHeader>
</Request>
<RequestedPickupTime>N</RequestedPickupTime>
<NewShipper>N</NewShipper>
<LanguageCode>en</LanguageCode>
<PiecesEnabled>Y</PiecesEnabled>
<Billing>
<ShipperAccountNumber>848177056</ShipperAccountNumber>
<ShippingPaymentType>S</ShippingPaymentType>
<BillingAccountNumber>848177056</BillingAccountNumber>
<DutyPaymentType>R</DutyPaymentType>
</Billing>
<Consignee>
<CompanyName>$datatoCompanyName</CompanyName>
<AddressLine>$datatoAddress</AddressLine>
<City>$datatoCity</City>
<Division>$datatoState</Division>
<DivisionCode>$datatoStateDivisionCode</DivisionCode>
<PostalCode>$datatoZip</PostalCode>
<CountryCode>$datatoCountryCode</CountryCode>
<CountryName>$datatoCountry</CountryName>
<Contact>
<PersonName>$datatoName</PersonName>
<PhoneNumber>$datatoPhone</PhoneNumber>
<PhoneExtension></PhoneExtension>
</Contact>
</Consignee>
<Dutiable>
<DeclaredValue>$totalPrice</DeclaredValue>
<DeclaredCurrency>$currenctSymbol</DeclaredCurrency>
<TermsOfTrade>DAP</TermsOfTrade>
</Dutiable>
<Reference>
<ReferenceID>$datashipmentId</ReferenceID>
<ReferenceType>St</ReferenceType>
</Reference>
<ShipmentDetails>
<NumberOfPieces>$piecesCount</NumberOfPieces>
<Pieces>$pieces</Pieces>
<Weight>$total_weight</Weight>
<WeightUnit>L</WeightUnit>
<GlobalProductCode>P</GlobalProductCode>
<Date>$ab_date</Date>
<Contents>SHIPMENT #$datashipmentId</Contents>
<DoorTo>DD</DoorTo>
<DimensionUnit>I</DimensionUnit>
<PackageType>EE</PackageType>
<IsDutiable>N</IsDutiable>
<CurrencyCode>$currenctSymbol</CurrencyCode>
</ShipmentDetails>
<Shipper>
<ShipperID>848177056</ShipperID>
<CompanyName>$datafromCompanyName</CompanyName>
<RegisteredAccount>848177056</RegisteredAccount>
<AddressLine>$datafromAddress</AddressLine>
<City>$datafromCity</City>
<Division>$datafromState</Division>
<DivisionCode>$datafromStateDivisionCode</DivisionCode>
<PostalCode>$datafromZip</PostalCode>
<CountryCode>$datafromCountryCode</CountryCode>
<CountryName>$datafromCountry</CountryName>
<Contact>
<PersonName>$datafromName</PersonName>
<PhoneNumber>$datafromPhone</PhoneNumber>
<PhoneExtension></PhoneExtension>
</Contact>
</Shipper>
<EProcShip>N</EProcShip>
<LabelImageFormat>PDF</LabelImageFormat>
<RequestArchiveDoc>Y</RequestArchiveDoc>
<Label>
<LabelTemplate>8X4_thermal</LabelTemplate>
<Logo>Y</Logo>
<CustomerLogo>
<LogoImage>$logo_image</LogoImage>
<LogoImageFormat>PNG</LogoImageFormat>
</CustomerLogo>
<Resolution>200</Resolution>
</Label>
</req:ShipmentValidateRequest>
EOT;
//dd($query);
            $url = "https://xmlpi-ea.dhl.com/XMLShippingServlet?isUTF8Support=true";

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, "$query");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);

            $output = $this->func_dhl_filter_output($output);

            preg_match("/<ConditionData>(.*?)<\/ConditionData>/", $output, $matches);
            if (count($matches) > 0) {
                $message = $matches[1];
            }

            preg_match("/<OutputImage>(.*?)<\/OutputImage>/", $output, $matches);
            if (count($matches) > 0) {


                $image = base64_decode($matches[1]);

                $filename = $datashipmentId . '.pdf';

                if (file_exists(public_path("/uploads/fillnship_shipments/" . $request->shipmentId . "/" . $request->shipmentId . '.pdf'))) {
                    // do nothing
                } else {
                    mkdir(public_path("/uploads/fillnship_shipments/" . $request->shipmentId));
                    copy((public_path("/uploads/demo.pdf")), public_path("/uploads/fillnship_shipments/" . $request->shipmentId . "/" . $filename));
                }

                $destinationPath = public_path("/uploads/fillnship_shipments/" . $request->shipmentId);
                \PDF::loadView('Administrator.fillnship.dhl-post')->save($destinationPath . "/" . $filename)->stream('download.pdf');

                $file = $destinationPath . "/" . $filename;
                file_put_contents($file, $image);

                /* if (file_exists($file)) {
                  header('Content-Description: File Transfer');
                  header('Content-Type: application/octet-stream');
                  header('Content-Disposition: attachment; filename="' . basename($file) . '"');
                  header('Expires: 0');
                  header('Cache-Control: must-revalidate');
                  header('Pragma: public');
                  header('Content-Length: ' . filesize($file));
                  } */

                $data['file'] = $filename;
                $data['shipId'] = $datashipmentId;

                return view('Administrator.fillnship.dhl-post', $data);
            } else {
                $data['message'] = urldecode($message);
                $data['shipId'] = $datashipmentId;
                return view('Administrator.fillnship.dhl-post', $data);
            }
        }
    }

    /**
     * Method used to print DHL Commercial Invoice Pop up
     * @param integer $shipmentId
     * @param integer $page 
     * @return string
     */
    public function dhlci($shipmentId, $page) {

        $data = array();
        $shipmentData = Fillship::find($shipmentId)->toArray();
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

        $data['shipmentData'] = $shipmentData;

        /* FETCH Product LIST  */
        $productData = collect(\App\Model\Siteproduct::orderby('productName', 'asc')->get());
        $data['productList'] = $productData->mapWithKeys(function($item) {
            return [$item['id'] => $item['productName']];
        });

        $packageDetailsData = \App\Model\Fillshipshipmentspackaging::where("shipmentId", $shipmentId)->get();
        $data['packageDetailsData'] = $packageDetailsData;
        $packagingDetailsData = \App\Model\Fillshipshipmentspackaging::where("shipmentId", $shipmentId)->get();
        $data['packageCount'] = $packagingDetailsData->count();
        $fillnshipItem = Fillshipitem::where("fillshipId", $shipmentId)->where('deleted', '0')->get()->toArray();
        $data['fillnshipItem'] = $fillnshipItem;
        $data['userInfo'] = \App\Model\User::find($shipmentData['userId']);
        //\App\Model\Shipmentlabelprintlog::logEntry($shipmentId, 'DHL Comercial Invoice Label');
        return view('Administrator.fillnship.dhlci', $data);
    }

    /**
     * Method used to print DHL Commercial Invoice
     * @return string
     */
    public function dhlcipost($shipmentId = 1, $page = 1, Request $request) {

        if (\Request::isMethod('post')) {
            $str = $request->data;

            $dataExtract = explode("&", $str); #dd($dataExtract);

            $data['fromAddress'] = urldecode(str_replace('fromAddress=', '', $dataExtract[2]));
            $data['toAddress'] = urldecode(str_replace('toAddress=', '', $dataExtract[4]));
            $data['fromAddress2'] = urldecode(str_replace('', '', $dataExtract[4]));
            $data['toAddress2'] = str_replace('toAddress2=', '', $dataExtract[5]);
            $data['fromCountry'] = $request->input('fromCountry');
            $data['toCountry'] = $request->input('toCountry');
            $data['fromState'] = $request->input('fromState');
            $data['toState'] = $request->input('toState');
            $data['fromCity'] = $request->input('fromCity');
            $data['toCity'] = $request->input('toCity');
            $data['fromZip'] = urldecode(str_replace('fromZip=', '', $dataExtract[12]));
            $data['toZip'] = urldecode(str_replace('toZip=', '', $dataExtract[13]));
            $data['fromPhone'] = urldecode(str_replace('fromPhone=', '', $dataExtract[14]));
            $data['toPhone'] = urldecode(str_replace('toPhone=', '', $dataExtract[15]));
            $data['fromEmail'] = urldecode(str_replace('fromEmail=', '', $dataExtract[16]));
            $data['toEmail'] = urldecode(str_replace('toEmail=', '', $dataExtract[17]));
            $data['fromName'] = urldecode(str_replace('fromName=', '', $dataExtract[18]));
            $data['toName'] = urldecode(str_replace('toName=', '', $dataExtract[19]));
            $data['fromCompanyName'] = urldecode(str_replace('fromCompanyName=', '', $dataExtract[20]));
            $data['toCompanyName'] = urldecode(str_replace('toCompanyName=', '', $dataExtract[21]));

            $data['airWaybillNo'] = urldecode(str_replace('airWaybillNo=', '', $dataExtract[22]));
            $data['incoterms'] = urldecode(str_replace('incoterms=', '', $dataExtract[23]));
            #$data['Pieces'] = str_replace('Pieces=', '', $dataExtract[39]);
            $data['Pieces'] = str_replace('Pieces=', '', $dataExtract[29]);
            #$data['invoiceDate'] = urldecode(str_replace('invoiceDate=', '', $dataExtract[42]));
            $data['invoiceDate'] = urldecode(str_replace('invoiceDate=', '', $dataExtract[32]));
            #$data['invoiceNo'] = str_replace('invoiceNo=', '', $dataExtract[43]);
            $data['invoiceNo'] = str_replace('invoiceNo=', '', $dataExtract[33]);
            #$data['grossWeight'] = str_replace('grossWeight=', '', $dataExtract[40]);
            $data['grossWeight'] = str_replace('grossWeight=', '', $dataExtract[30]);
            #$data['netWeight'] = str_replace('netWeight=', '', $dataExtract[41]);
            $data['netWeight'] = str_replace('netWeight=', '', $dataExtract[31]);
            #$data['totalPrice'] = urldecode(str_replace('totalPrice=', '', $dataExtract[44]));
            $data['totalPrice'] = urldecode(str_replace('totalPrice=', '', $dataExtract[34]));

            $data['qty'] = $request->input('qty');
            $data['price'] = $request->input('price');
            $data['manufCountry'] = $request->input('manufCountry');

            $data['descr'] = $request->descr;
            $data['ccc'] = $request->ccc;
            $data['manufCountry'] = $request->manufCountry;

            $data['fromState'] = ($data['fromState'] == 'Select any one' ? '-' : $data['fromState']);
            $data['fromCity'] = ($data['fromCity'] == 'Select any one' ? '-' : $data['fromCity']);
            $data['toState'] = ($data['toState'] == 'Select any one' ? '-' : $data['toState']);
            $data['toCity'] = ($data['toCity'] == 'Select any one' ? '-' : $data['toCity']);

            $dataExtract = explode("&", $str);
            $data['fromAddress'] = urldecode(str_replace('fromAddress=', '', $dataExtract[2]));
            $data['toAddress'] = urldecode(str_replace('toAddress=', '', $dataExtract[4]));
            $data['fromAddress2'] = urldecode(str_replace('', '', $dataExtract[4]));
            $data['toAddress2'] = str_replace('toAddress2=', '', $dataExtract[5]);
            $data['fromCountry'] = $request->input('fromCountry');
            $data['toCountry'] = $request->input('toCountry');
            $data['fromState'] = $request->input('fromState');
            $data['toState'] = $request->input('toState');
            $data['fromCity'] = $request->input('fromCity');
            $data['toCity'] = $request->input('toCity');
            $data['fromZip'] = urldecode(str_replace('fromZip=', '', $dataExtract[12]));
            $data['toZip'] = urldecode(str_replace('toZip=', '', $dataExtract[13]));
            $data['fromPhone'] = urldecode(str_replace('fromPhone=', '', $dataExtract[14]));
            $data['toPhone'] = urldecode(str_replace('toPhone=', '', $dataExtract[15]));
            $data['fromEmail'] = urldecode(str_replace('fromEmail=', '', $dataExtract[16]));
            $data['toEmail'] = urldecode(str_replace('toEmail=', '', $dataExtract[17]));
            $data['fromName'] = urldecode(str_replace('fromName=', '', $dataExtract[18]));
            $data['toName'] = urldecode(str_replace('toName=', '', $dataExtract[19]));
            $data['fromCompanyName'] = urldecode(str_replace('fromCompanyName=', '', $dataExtract[20]));
            $data['toCompanyName'] = urldecode(str_replace('toCompanyName=', '', $dataExtract[21]));

            /* $data['airWaybillNo'] = urldecode(str_replace('airWaybillNo=', '', $dataExtract[22]));
              $data['incoterms'] = urldecode(str_replace('incoterms=', '', $dataExtract[23]));
              $data['Pieces'] = str_replace('Pieces=', '', $dataExtract[39]);
              $data['invoiceDate'] = urldecode(str_replace('invoiceDate=', '', $dataExtract[42]));
              $data['invoiceNo'] = str_replace('invoiceNo=', '', $dataExtract[43]);
              $data['grossWeight'] = str_replace('grossWeight=', '', $dataExtract[40]);
              $data['netWeight'] = str_replace('netWeight=', '', $dataExtract[41]);
              $data['totalPrice'] = urldecode(str_replace('totalPrice=', '', $dataExtract[44])); */

            /* $data['qty'] = $request->input('qty');
              $data['price'] = $request->input('price');
              $data['manufCountry'] = $request->input('manufCountry');

              $data['descr'] = $request->descr;
              $data['ccc'] = $request->ccc;
              $data['manufCountry'] = $request->manufCountry; */

            $data['fromState'] = ($data['fromState'] == 'Select any one' ? '-' : $data['fromState']);
            $data['fromCity'] = ($data['fromCity'] == 'Select any one' ? '-' : $data['fromCity']);
            $data['toState'] = ($data['toState'] == 'Select any one' ? '-' : $data['toState']);
            $data['toCity'] = ($data['toCity'] == 'Select any one' ? '-' : $data['toCity']);

            return view('Administrator.fillnship.dhlci-post', $data);
        }
        return view('Administrator.fillnship.dhlci-post');
    }

    /**
     * Method used to print Nations Delivery pop up Pop up
     * @param integer $shipmentId
     * @param integer $page 
     * @return html
     */
    public function nationsdelivery($shipmentId, $page) {

        $data = array();
        $shipmentData = Fillship::find($shipmentId)->toArray();

        $data['shipmentData'] = $shipmentData;

        $packageDetailsData = Fillshipitem::where("fillshipId", $shipmentId)->where('deleted', '0')->get();
        $data['itemCount'] = $packageDetailsData->count();
        $packagingDetailsData = \App\Model\Fillshipshipmentspackaging::where("shipmentId", $shipmentId)->get();
        $data['packageDetailsData'] = $packagingDetailsData;
        $data['packageCount'] = $packagingDetailsData->count();

        /* FETCH Country LIST  */
        $countryData = collect(Country::where('status', '1')->orderby('name', 'asc')->get());
        $data['countryList'] = $countryData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        /* FETCH State LIST  */
        $stateData = collect(State::where('status', '1')->where('deleted', '0')->orderby('name', 'asc')->get());
        $data['stateList'] = $stateData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        /* FETCH City LIST  */
        $cityData = collect(City::where('status', '1')->where('deleted', '0')->orderby('name', 'asc')->get());
        $data['cityList'] = $cityData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });

        #dd($data['delivery']['deliveries'][10]['id']);
        $data['userInfo'] = \App\Model\User::find($shipmentData['userId']);
        /* GET ORDER ID FROM SHIPMENT ID */
        $orderId = \App\Model\Order::where('shipmentId', $shipmentId)->where('type', 'fillship')->first();
        $data['orderId'] = $orderId->id;
        //\App\Model\Shipmentlabelprintlog::logEntry($shipmentId, 'Nations Delivery Label');
        return view('Administrator.fillnship.nationsdelivery', $data);
    }

    /**
     * Method to manage shipment status
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function changestatus($id = '0', $page = '', Request $request) {

        if (\Request::isMethod('post')) {

            $shipment = new Fillship;
            $validator = Validator::make($request->all(), [
                        'shipmentStatus' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            } else {
                if (!empty($id)) {
                    $shipment = Fillship::find($id);
                    $shipment->status = $request->shipmentStatus;
                    $shipment->updatedBy = Auth::user()->id;
                    $shipment->updatedOn = Config::get('constants.CURRENTDATE');
                    $shipment->save();

                    return \Redirect::to('administrator/fillnship/?page=' . $page)->with('successMessage', 'Status updated successfully.');
                }
            }
        }

        /* SET DATA FOR VIEW  */

        $data['pageTitle'] = "Change Status";
        $data['id'] = $id;
        $data['page'] = $page;

        $data['shipmentData'] = Fillship::find($id);

        return view('Administrator.fillnship.changestatus', $data);
    }

    /**
     * This function is user to update tracking number of shipment and its lock status
     * @param type $shipmentPackage
     * @param Request $request
     * @return type
     */
    public function updatetracking($shipmentPackage, Request $request) {

        if ($request->trackingNumber != '') {
            $updateArray = array();
            $updateArray['trackingNumber'] = $request->trackingNumber;
            $updateArray['tracking2'] = $request->tracking2;
            if ($request->action == 'lock')
                $updateArray['trackingLock'] = '1';
            Fillshipitem::where('id', $shipmentPackage)->update($updateArray);
            $shipmentId = Fillshipitem::find($shipmentPackage)->fillshipId;
            return \Redirect::to('administrator/fillnship/addedit/' . $shipmentId)->with('successMessage', 'Shipment updated successfully.');
        } else
            return \Redirect::to('administrator/fillnship/addedit/' . $shipmentId)->with('errorMessage', 'Please enter tracking number');
    }

    /**
     * Method used to Export All with selected fields
     * @param integer $page
     * @return type
     */
    public function exportall($page, Request $request) {
        // dd(session()->all());
        if (count($request->selectall) == 0) {
            return \Redirect::to('administrator/autoparts/')->with('errorMessage', 'Select atleast one field during the time of export');
        }
        if (\Request::isMethod('post')) {

            $searchShipmentArr = array(
                'status' => '',
                'unit' => '',
                'shipmentId' => '',
                'deliveryCompanyId' => '',
                'totalCost' => '',
                'receiver' => '',
                'paymentStatus' => '',
                'toCountry' => '',
                'toState' => '',
                'toCity' => ''
            );


            $sortField = \Session::get('FILLSHIPDATA.field');
            $sortType = \Session::get('FILLSHIPDATA.type');
            $searchDisplay = \Session::get('FILLSHIPDATA.searchDisplay');
            $searchByCreatedOn = \Session::get('FILLSHIPDATA.searchByCreatedOn');
            $searchByDate = \Session::get('FILLSHIPDATA.searchByDate');
            $searchShipment = \Session::get('FILLSHIPDATA.searchShipmentArr');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'shipmentid';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchShipment'] = !empty($searchShipment) ? $searchShipment[0] : $searchShipmentArr;



            $recordList = Fillship::getShipmentList($param, 'export')->toArray();
            // print_r($recordList); die;
            $shipmentData = array();
            $headers = array();
            if (!empty($recordList)) {
                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {

                            if ($count == 0) {
                                if ($key == 'shipmentid')
                                    $cellValue = 'Shipment ID';
                                elseif ($key == 'customer')
                                    $cellValue = 'Customer';
                                elseif ($key == 'destinationCityName')
                                    $cellValue = 'Destination City Name';
                                elseif ($key == 'destinationStateName')
                                    $cellValue = 'Destination State Name';
                                elseif ($key == 'destinationCountryName')
                                    $cellValue = 'Destination Country Name';
                                elseif ($key == 'createdOn')
                                    $cellValue = 'Date';
                                elseif ($key == 'totalcost')
                                    $cellValue = 'Cost';
                                elseif ($key == 'status')
                                    $cellValue = 'Shipment Status';
                                elseif ($key == 'paymentstatus')
                                    $cellValue = 'Payment Status';

                                $headers[$key] = $cellValue;
                            }
                        }
                    }
                }

                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {
                            if ($key == 'createdOn')
                                $value = \Carbon\Carbon::parse($value)->format('Y-m-d');

                            $shipmentData[$count + 1][$headers[$key]] = $value;
                        }
                    }
                    //print_r($shipmentData);
                }
            }
            // print_r($shipmentData);die;
            ob_end_clean();
            ob_start();
            Excel::create("FillnShip-" . \Carbon\Carbon::now(), function($excel) use($shipmentData) {
                $excel->sheet('Sheet 1', function($sheet) use($shipmentData) {
                    $sheet->fromArray($shipmentData);
                });
            })->export('xlsx');
            ob_flush();


            return \Redirect::to('administrator/fillnship/')->with('successMessage', 'Excel file created and downloaded');
        }
    }

    /**
     * Method used to Export selected with selected fields
     * @param integer $page
     * @return type
     */
    public function exportselected($page, Request $request) {
        /* if (count($request->selectall) == 0) {
          return \Redirect::to('administrator/users/')->with('errorMessage', 'Select atleast one field during the time of export');
          } */
        if (\Request::isMethod('post')) {

            $searchShipmentArr = array(
                'status' => '',
                'unit' => '',
                'shipmentId' => '',
                'deliveryCompanyId' => '',
                'totalCost' => '',
                'receiver' => '',
                'paymentStatus' => '',
                'toCountry' => '',
                'toState' => '',
                'toCity' => ''
            );

            $sortField = \Session::get('FILLSHIPDATA.field');
            $sortType = \Session::get('FILLSHIPDATA.type');
            $searchDisplay = \Session::get('FILLSHIPDATA.searchDisplay');
            $searchByCreatedOn = \Session::get('FILLSHIPDATA.searchByCreatedOn');
            $searchByDate = \Session::get('FILLSHIPDATA.searchByDate');
            $searchShipment = \Session::get('FILLSHIPDATA.searchShipmentArr');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'shipmentid';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchShipment'] = !empty($searchShipment) ? $searchShipment[0] : $searchShipmentArr;
            $param['searchByshipment'] = $request->selected;



            $recordList = Fillship::getShipmentList($param, 'export')->toArray();



            $shipmentData = array();
            $headers = array();
            if (!empty($recordList)) {
                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        //print_r($request->selectall);die;
                        if (in_array($key, $request->selectall)) {
                            if ($count == 0) {
                                if ($key == 'shipmentid')
                                    $cellValue = 'Shipment ID';
                                elseif ($key == 'customer')
                                    $cellValue = 'Customer';
                                elseif ($key == 'destinationCityName')
                                    $cellValue = 'Destination City Name';
                                elseif ($key == 'destinationStateName')
                                    $cellValue = 'Destination State Name';
                                elseif ($key == 'destinationCountryName')
                                    $cellValue = 'Destination Country Name';
                                elseif ($key == 'createdOn')
                                    $cellValue = 'Date';
                                elseif ($key == 'totalcost')
                                    $cellValue = 'Cost';
                                elseif ($key == 'status')
                                    $cellValue = 'Shipment Status';
                                elseif ($key == 'paymentstatus')
                                    $cellValue = 'Payment Status';

                                $headers[$key] = $cellValue;
                            }
                        }
                    }
                }

                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {

                            if ($key == 'createdOn')
                                $value = \Carbon\Carbon::parse($value)->format('Y-m-d');

                            $shipmentData[$count + 1][$headers[$key]] = $value;
                        }
                    }
                }
            }

            $excelName = "FillnShip-" . \Carbon\Carbon::now();
            $path = public_path('export/' . $excelName);

            ob_end_clean();
            ob_start();
            Excel::create($excelName, function($excel) use($shipmentData) {
                $excel->sheet('Sheet 1', function($sheet) use($shipmentData) {
                    $sheet->fromArray($shipmentData);
                });
            })->store('xlsx', public_path('exports'));
            ob_flush();
            $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
            return response()->json(['path' => $creatingPath]);
        }
    }

    public function updatepaymentstatus($shipmentId, Request $request) {

        if ($request->paymentStatus == 'paid') {
            $invoiceData = \App\Model\Invoice::where('shipmentId', $shipmentId)->where('type', 'fillship')->where('invoiceType', 'invoice')->where('deleted', '0')->where("extraCostCharged", "N")->first();
            $newInvoice = $invoiceData->replicate();
            $newInvoice->invoiceType = "receipt";
            $newInvoice->paymentStatus = "paid";
            $newInvoice->save();

            $invoiceData->deleted = "1";
            $invoiceData->save();

            Fillship::where("id", $shipmentId)->update(["paymentStatus" => "paid", "status" => "processed"]);

            return 1;
        } else {
            return 1;
        }
    }

    public function updateextracostinvoice(Request $request) {

        $invoiceId = $request->invoiceId;
        \App\Model\Invoice::where("id", $invoiceId)->update(["paymentStatus" => "paid", "invoiceType" => "receipt"]);
        return 1;
    }
    ///////////////////////////////////Confirm Payment/////////////////////////////////////////////////////////////////

    public function receivedPayment($shipmentId, $page = 0, Request $request) {
        $data = array();


        $data['pageTitle'] = "Payment Details";
        $data['id'] = $shipmentId;
        $data['page'] = $page;
        $data['paymentMethod'] = \App\Model\Paymentmethod::where("status", "1")->where("deleted", "0")->orderBy('status', 'desc')->get();
        /* FETCH SHIPPING METHOD LIST  */
        $data['shippingMethod'] = \App\Model\Shippingmethods::where('active', 'Y')->where('deleted', '0')->orderby('shipping', 'asc')->get();

        return view('Administrator.fillnship.receivedPayment', $data);
    }

    public function savePaymentDetails($shipmentId, $page = 0, Request $request)
    {
       
        //fetch shipment details

        $shipment = Fillship::find($shipmentId);

        $userInfo = User::find($shipment->userId);


        $shipmentItems = Fillshipitem::where('fillshipId', $shipmentId)->where('deleted', '0')->get()->toArray();


        $shipmentMethod = \App\Model\Shippingmethods::where('shippingid', $request->shippingMethodId)->pluck('shipping');

        

       // $data['shipment']['items'] = Fillshipitem::getItems($id);

        $packageDetails = array();
        if (!empty($shipmentItems)) {
            foreach ($shipmentItems as $key => $item) {
                /* SET DATA FOR INVOICE PARTICULARS */
                $packageDetails[$key] = array(
                    'id' => $item['id'],
                    'itemName' => $item['itemName'],
                    'storeId' => $item['storeId'],
                    'siteCategoryId' => $item['siteCategoryId'],
                    'siteSubCategoryId' => $item['siteSubCategoryId'],
                    'siteProductId' => $item['siteProductId'],
                    'length' => $item['length'],
                    'width' => $item['width'],
                    'height' => $item['height'],
                    'weight' => $item['weight'],
                    'chargeableWeight' => $item['itemWeight'],
                    'itemPrice' => $item['itemPrice'],
                    'itemQuantity' => $item['itemQuantity'],
                    'itemTotalCost' => $item['itemTotalCost']
                );
            }
        }

        /*  PREPARE DATA FOR INVOICE PARTICULARS */
        $invoiceData = array(
            'shipment' => array(
                'totalItemCost' => $shipment->totalItemCost,
                'shippingCost' => $shipment->totalShippingCost,
                'clearingDutyCost' => $shipment->totalClearingDuty,
                'isInsuranceCharged' => ($shipment->totalInsurance!='0') ? 'Y':'N', 
                'totalInsurance' => $shipment->totalInsurance,
                'totalQuantity' => 1,
                'totalTax' => $shipment->totalTax,
                'discount' => $shipment->totalDiscount,
                'totalCost' => (!empty($request->paidAmount) ? $request->paidAmount : $shipment->totalCost),
                'totalWeight' => $shipment->totalChargeableWeight,
                'otherChargeCost' => $shipment->totalOtherCharges,
                'storageCharge' => '',
                'totalDiscount' => $shipment->totalDiscount,
                'boxChargeableWeight' => $shipment->boxChargeableWeight,
                'boxLength'=> $shipment->boxLength,
                'boxHeight'=> $shipment->boxHeight,
                'boxLength'=> $shipment->boxLength,
                'boxWidth'=> $shipment->boxWidth,
                'boxName'=> \App\Model\Fillshipbox::find($shipment->boxId)->name,
                'shippingMethodName'=> $shipmentMethod[0]
            ),
            'warehouse' => array(
                'fromAddress' => $shipment->fromAddress,
                'fromZipCode' => $shipment->fromZipCode,
                'fromCountry' => $shipment->fromCountry,
                'fromState' => $shipment->fromState,
                'fromCity' => $shipment->fromCity,
            ),
            'shippingaddress' => array(
                'toCountry' => $shipment->toCountry,
                'toState' => $shipment->toState,
                'toCity' => $shipment->toCity,
                'toAddress' => $shipment->toAddress,
                'toZipCode' => $shipment->toZipCode,
                'toName' => $shipment->toName,
                'toEmail' => $shipment->toEmail,
                'toPhone' => $shipment->toPhone,
            ),
            'items' => $packageDetails,
            'payment' => array(
                'paymentMethodId' => $request->paymentMethodId,
                'paymentMethodName' => \App\Model\Paymentmethod::find($request->paymentMethodId)->paymentMethod,
            ),
        );

        //Insert row in Invoice//

        $invoice = new \App\Model\Invoice;

        $invoice->invoiceUniqueId = "REC" . $userInfo->unit . "-" . $shipment->id . '-' . date('Ymd');
        $invoice->shipmentId = $shipmentId;
        $invoice->type = "fillship";
        $invoice->invoiceType = "receipt";
        $invoice->userUnit = $userInfo->unit;
        $invoice->userFullName = $shipment->fromName;
        $invoice->userEmail = $shipment->fromEmail;
        $invoice->userContactNumber = $shipment->fromPhone;

        /*  FETCH ADDRESS BOOK DETAILS */
        $addressBookData = \App\Model\Addressbook::where('userId', $shipment->userId)->where('isDefaultBilling', '1')->where('deleted', '0')->with('country', 'state', 'city')->first();
        $invoice->billingName = $addressBookData->title . ' ' . $addressBookData->firstName . ' ' . $addressBookData->lastName;
        $invoice->billingEmail = $addressBookData->email;
        $invoice->billingAddress = $addressBookData->address;
        $invoice->billingAlternateAddress = $addressBookData->alternateAddress;
        $invoice->billingCity = isset($addressBookData->city) ? $addressBookData->city->name : '';
        $invoice->billingState = isset($addressBookData->state) ? $addressBookData->state->name : '';
        $invoice->billingCountry = $addressBookData->country->name;
        $invoice->billingZipcode = $addressBookData->zipcode;
        $invoice->billingPhone = $addressBookData->phone;
        $invoice->billingAlternatePhone = $addressBookData->alternatePhone;
        $invoice->totalBillingAmount = !empty($request->paidAmount) ? $request->paidAmount : $shipment->totalCost;
        $invoice->paymentMethodId = $request->paymentMethodId;
        $invoice->paymentStatus = 'paid';
        $invoice->invoiceParticulars = json_encode($invoiceData);
        $invoice->createdOn = Config::get('constants.CURRENTDATE');
        $invoice->save();

        $invoiceId = $invoice->id;


        if (!empty($invoiceId)) {
            $data['invoice'] = \App\Model\Invoice::find($invoiceId);
            $fileName = "Receipt_" . $invoice->invoiceUniqueId . ".pdf";

            PDF::loadView('Administrator.fillnship.invoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');

            $frontendUrl = Config::get('constants.frontendUrl') . "payment/invoice/" . $invoiceId;
            $content = "Receipt for Offline Payment for Shipment #" . $shipmentId . " is attached. Please find the cost page at <a href=" . $frontendUrl . ">" . $frontendUrl . "</a>";
            $to = $userInfo->email;

            Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($shipment, $to, $fileName) {
                $message->from('contact@shoptomydoor.com', 'Shoptomydoor');
                $message->subject('Fill and Ship Receipt');
                $message->to($to);
                $message->attach(public_path('exports/invoice/' . $fileName));
            });
        }
        /*  UPDATE PAYMENT STATUS IN SHIPMENT TABLE */

        $shipment->paymentStatus = 'paid';
        $shipment->totalCost = $shipment->totalCost;
        $shipment->paidByUserType = 'admin';
        $shipment->paidByUserId = Auth::user()->id;
        $shipment->save();


        $orderObj = new \App\Model\Order;
        $orderObj->shipmentId = $shipmentId;
        $orderObj->userId = $shipment->userId;
        $orderObj->totalCost = $shipment->totalCost;
        $orderObj->status = '2';
        $orderObj->type = 'fillship';
        $orderObj->createdBy = Auth::user()->id;
        $orderNumber = \App\Model\Order::generateOrderNumber($orderObj->id, $orderObj->userId);
        $orderObj->orderNumber = $orderNumber;
        //Newly added 09/09/2019
        $orderObj->createdDate = Config::get('constants.CURRENTDATE');
        $orderObj->save();

       // $save = $orderObj->save();

        //Insert record in Payment Transaction////

        $shipmentStatusData = new \App\Model\Paymenttransaction;

        $shipmentStatusData->paymentMethodId = $request->paymentMethodId;        
        $shipmentStatusData->transactionOn = $request->paidOn;
        $shipmentStatusData->paidForId = $shipmentId;
        $shipmentStatusData->amountPaid = $request->paidAmount;        
        $shipmentStatusData->paidFor = 'fillship';
        $shipmentStatusData->status = 'paid';

   
        $save = $shipmentStatusData->save();

        if ($save) {
            // \App\Model\Shipment::notificationEmail($shipmentId);

                $replace = array();
                $replace['[SHIPMENTID]'] = $shipmentId;
                $replace['[ORDERID]'] = $orderNumber;
             User::sendPushNotification($shipment->userId, 'order_created', Auth::user()->id, $replace);

            return \Redirect::to('administrator/fillnship/addedit/' . $shipmentId)->with('successMessage', 'Shipment updated successfully.');
        } else {

            return \Redirect::to('administrator/fillnship/addedit/' . $shipmentId)->with('errorMessage', 'Please enter comments');
        }
    }


    public function generateInvoice($shipmentId, $shipmentType) {
        
        $shipment = Fillship::find($shipmentId);

        $userInfo = User::find($shipment->userId);


        $shipmentItems = Fillshipitem::where('fillshipId', $shipmentId)->where('deleted', '0')->get()->toArray();
        $paymentMethod = \App\Model\Paymentmethod::select('paymentMethod')->find($shipment->paymentMethodId);
        $shipmentMethod = \App\Model\Shippingmethods::where('shippingid', $request->shippingMethodId)->pluck('shipping');

        $paymentTransactionData = \App\Model\Paymenttransaction::where('paidFor', $shipmentType)->where('paidForId', $shipmentId)->get();

        $packageDetails = array();

        if (!empty($shipmentItems)) {
            foreach ($shipmentItems as $key => $item) {
                /* SET DATA FOR INVOICE PARTICULARS */
                $packageDetails[$key] = array(
                    'id' => $item['id'],
                    'itemName' => $item['itemName'],
                    'storeId' => $item['storeId'],
                    'siteCategoryId' => $item['siteCategoryId'],
                    'siteSubCategoryId' => $item['siteSubCategoryId'],
                    'siteProductId' => $item['siteProductId'],
                    'length' => $item['length'],
                    'width' => $item['width'],
                    'height' => $item['height'],
                    'weight' => $item['weight'],
                    'chargeableWeight' => $item['itemWeight'],
                    'itemPrice' => $item['itemPrice'],
                    'itemQuantity' => $item['itemQuantity'],
                    'itemTotalCost' => $item['itemTotalCost']
                );
            }
        }


        /*  PREPARE DATA FOR INVOICE PARTICULARS */
         $invoiceData = array(
            'shipment' => array(
                'totalItemCost' => $shipment->totalItemCost,
                'totalShippingCost' => $shipment->totalShippingCost,
                'totalClearingDuty' => $shipment->totalClearingDuty,
                'totalInsurance' => $shipment->totalInsurance,
                'totalQuantity' => 1,
                'totalTax' => $shipment->totalTax,
                'totalCost' => (!empty($request->paidAmount) ? $request->paidAmount : $shipment->totalCost),
                'totalWeight' => $shipment->totalChargeableWeight,
                'totalOtherCharges' => $shipment->totalOtherCharges,
                'totalDiscount' => $shipment->totalDiscount,
                'boxChargeableWeight' => $shipment->boxChargeableWeight,
                'boxLength'=> $shipment->boxLength,
                'boxHeight'=> $shipment->boxHeight,
                'boxLength'=> $shipment->boxLength,
                'boxWidth'=> $shipment->boxWidth,
                'boxName'=> \App\Model\Fillshipbox::find($shipment->boxId)->name,
                'shippingMethodName'=> $shipmentMethod[0]
            ),
            'warehouse' => array(
                'fromAddress' => $shipment->fromAddress,
                'fromZipCode' => $shipment->fromZipCode,
                'fromCountry' => $shipment->fromCountry,
                'fromState' => $shipment->fromState,
                'fromCity' => $shipment->fromCity,
            ),
            'shippingaddress' => array(
                'toCountry' => $shipment->toCountry,
                'toState' => $shipment->toState,
                'toCity' => $shipment->toCity,
                'toAddress' => $shipment->toAddress,
                'toZipCode' => $shipment->toZipCode,
                'toName' => $shipment->toName,
                'toEmail' => $shipment->toEmail,
                'toPhone' => $shipment->toPhone,
            ),
            'items' => $packageDetails,
            'payment' => array(
                'paymentMethodId' => isset($shipment->paymentMethodId)?$shipment->paymentMethodId:0,
                'paymentMethodName' => isset($shipment->paymentMethodId)?\App\Model\Paymentmethod::find($shipment->paymentMethodId)->paymentMethod:"",
            ),
        );

        //Insert row in Invoice//

        $invoice = new \App\Model\Invoice;

        $invoice->invoiceUniqueId = "REC" . $userInfo->unit . "-" . $shipment->id . '-' . date('Ymd');
        $invoice->shipmentId = $shipmentId;
        $invoice->type = 'fillship';
        $invoice->invoiceType = "receipt";
        $invoice->userUnit = $userInfo->unit;
        $invoice->userFullName = $shipment->fromName;
        $invoice->userEmail = $shipment->fromEmail;
        $invoice->userContactNumber = $shipment->fromPhone;

        /*  FETCH ADDRESS BOOK DETAILS */
        $addressBookData = \App\Model\Addressbook::where('userId', $shipment->userId)->where('isDefaultBilling', '1')->where('deleted', '0')->with('country', 'state', 'city')->first();
        $invoice->billingName = $addressBookData->title . ' ' . $addressBookData->firstName . ' ' . $addressBookData->lastName;
        $invoice->billingEmail = $addressBookData->email;
        $invoice->billingAddress = $addressBookData->address;
        $invoice->billingAlternateAddress = $addressBookData->alternateAddress;
        $invoice->billingCity = isset($addressBookData->city) ? $addressBookData->city->name : '';
        $invoice->billingState = isset($addressBookData->state) ? $addressBookData->state->name : '';
        $invoice->billingCountry = $addressBookData->country->name;
        $invoice->billingZipcode = $addressBookData->zipcode;
        $invoice->billingPhone = $addressBookData->phone;
        $invoice->billingAlternatePhone = $addressBookData->alternatePhone;
        $invoice->totalBillingAmount = (!empty($shipment->totalCost) ? $shipment->totalCost : 0);
        $invoice->paymentMethodId = $shipment->paymentMethodId;
        $invoice->paymentStatus = 'paid';
        $invoice->invoiceParticulars = json_encode($invoiceData);
        $invoice->createdOn = Config::get('constants.CURRENTDATE');

        $save = $invoice->save();

        if (empty($paymentTransactionData)) {
            $shipmentStatusData = new \App\Model\Paymenttransaction;
            $shipmentStatusData->paymentMethodId = $request->paymentMethodId;
            $shipmentStatusData->paidFor = 'fillship';
            $shipmentStatusData->paidForId = $shipmentId;
            $shipmentStatusData->amountPaid = $shipment->totalCost;
            $shipmentStatusData->status = 'paid';
            $shipmentStatusData->paidFor = $shipmentType;
            $shipmentStatusData->save();
        }
        $invoiceId = $invoice->id;


        if (!empty($invoiceId)) {
            $data['invoice'] = \App\Model\Invoice::find($invoiceId);
            $fileName = "Receipt_" . $invoice->invoiceUniqueId . ".pdf";

            PDF::loadView('Administrator.fillnship.invoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
        }

        if ($save) {
            return 1;
        } else {
            return 0;
        }
    }


    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Fillship::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/fillnship/?page=' . $page)->with('successMessage', 'Shipments deleted successfully.');
            } else {
                return \Redirect::to('administrator/fillnship/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/fillnship/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to delete multiple records
     * @param object $request
     * @return html
     */
    public function deleteall(Request $request) {
        $createrModifierId = Auth::user()->id;
        $checkedval = $request->checkedval;

        if (!empty($checkedval)) {
            $idArray = explode('^', $checkedval);

            foreach ($idArray as $id) {
                Fillship::deleteRecord($id, $createrModifierId);
            }
            return \Redirect::to('administrator/fillnship')->with('successMessage', 'Shipments deleted successfully.');
        } else {
            return \Redirect::to('administrator/fillnship/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
