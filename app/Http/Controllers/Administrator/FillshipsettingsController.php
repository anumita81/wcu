<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Shippingsettings;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class FillshipsettingsController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index() {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.FillShipOptions'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $data['title'] = "Administrative Panel :: Fill And Ship Options";
        $data['pageTitle'] = "Fill And Ship Options";
        $data['contentTop'] = array('breadcrumbText' => 'Fill & Ship Options', 'contentTitle' => 'Fill And Ship Options', 'pageInfo' => 'This section allows you to manage different fill and ship options');

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        $data['settings'] = Shippingsettings::getFillShipSettingsData()->toArray();
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavFillShip', 'menuSub' => '', 'menuSubSub' => 'leftNavFillShipOptions8');


        return view('Administrator.fillshipsettings.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string

     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata(Request $request) {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.FillShipOptions'), Auth::user()->id); // call the helper function
        
        if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $shippingsettings = new Shippingsettings;


        $request->except('_token');

        //print_r($request->post()); die;

        foreach ($request->post() as $key => $val) {


            //If post new value and db settings value are not same then db old value will updated with settings value 
            if ($key != "_token") {
                $validator = Validator::make($request->all(), [
                            $key => 'required'
                ]);


                $settingsId = Shippingsettings::where('settingsKey', $key)->get(['id']);
                $settingsId = $settingsId[0]['id'];

                $shippingsettings = Shippingsettings::find($settingsId);
                $shippingsettings->settingsValue = $val[1];
                $shippingsettings->oldValue = $val[0];
                $shippingsettings->save();
            }
        }

        return redirect('/administrator/fillshipsettings')->with('successMessage', 'Options saved successfuly.');
    }

    

}
