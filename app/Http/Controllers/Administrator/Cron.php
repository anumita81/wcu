<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Shipment;
use App\Model\Order;
use App\Model\Schedulenotification;
use App\Model\Emailtemplate;
use App\Model\Smstemplate;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Excel;
use Carbon\Carbon;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Hash;
use Mail;
use customhelper;
use Spatie\Activitylog\Models\Activity;
use Twilio\Rest\Client;


class Cron extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function sendnotification()
    {
        $notification = Schedulenotification::where('emailSentStatus', '0')->where('smsSentStatus', '0')->get()->toArray();
        
        $currentDate = Config::get('constants.CURRENTDATE');
        
        
        
        foreach($notification as $notify)
        {
           
           if((Carbon::parse($notify['scheduleDate'])->format('d/m/Y')) == (Carbon::parse($currentDate)->format('d/m/Y')))
           {
                $userEmail = User::select('*')->where('id', $notify['customerId'])->first()->toArray();
                $emailTemplate = Emailtemplate::select('*')->where('id', $notify['emailTemplateId'])->first();
                $smsTemplate = Smstemplate::select('*')->where('id', $notify['smsTemplateId'])->first()->toArray();

               
                /* ++++++++++ email functionality ++++++++ */
                
                $replace['[NAME]'] = $userEmail['firstName'] . " " . $userEmail['lastName'];
                $to = $userEmail['email'];
          
                
                $sendMail = customhelper::SendMail($emailTemplate, $replace, $to);
                
                /* ++++++++++ end of email functionality ++++ */
               
               
               
                //////SMS Sending Functionality////
                
                $credentials = (Config::get('constants.SMSCredentials'));                
                
                $sid = $credentials['SID'];
                $token = $credentials['TOKEN'];
                $fromNum = $credentials['FROMNUMBER'];
               
                $twilio = new Client($sid, $token);
                
              // print_r($twilio); die;
                
               $message = $twilio->messages->create($userEmail['contactNumber'], array("body" => $smsTemplate['templateBody'],
                               "from" => $fromNum));                
      
                
                //////SMS Sending Functionality////
                
               
                if($sendMail && !empty($message))
                {
                   Schedulenotification::where('id', $notify['id'])->update(array('emailSentStatus'=>'1', 'smsSentStatus'=>'1'));
                   
                   echo "Mail and SMS sent";
                }
                
                
                
                
           }
            
        }
        
        
        
    }
}
