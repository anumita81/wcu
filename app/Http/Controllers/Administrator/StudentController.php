<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Student;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use Excel;
use Illuminate\Support\Facades\DB;
use customhelper;

class StudentController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Student'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchData = \Input::get('searchData', '');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('STUDENTDATA.searchData', $searchData);
            \Session::push('STUDENTDATA.searchDisplay', $searchDisplay);
            \Session::push('STUDENTDATA.field', $field);
            \Session::push('STUDENTDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchData'] = $searchData;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('STUDENTDATA.field');
            $sortType = \Session::get('STUDENTDATA.type');
            $searchData = \Session::get('STUDENTDATA.searchData');
            $searchDisplay = \Session::get('STUDENTDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'code';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchData'] = !empty($searchData) ? $searchData[0] : '';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'studentFirstName' => array('current' => 'sorting'),
            'subjectCode' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';
        //print_r($param); die;

        /* FETCH Subject LIST  */
        $studentData = Student::getStudentList($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Student";
        $data['contentTop'] = array('breadcrumbText' => 'Student', 'contentTitle' => 'Student', 'pageInfo' => 'This section allows you to manage student');
        $data['pageTitle'] = "Student";
        $data['page'] = $studentData->currentPage();
        $data['studentData'] = $studentData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.student.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Student";
        $data['contentTop'] = array('breadcrumbText' => 'Student', 'contentTitle' => 'Student', 'pageInfo' => '');
        $data['page'] = !empty($page) ? $page : '1';
        $data['centerList'] = \App\Model\Center::where('deleted', '0')->where('status', 'active')->get();
        $data['subjectGroupList'] = \App\Model\Subjectgroup::where('deleted', '0')->where('status', '1')->whereRaw("parentgroupId", "0")->get();
        $data['subjectList'] = \App\Model\Subject::where('deleted', '0')->where('status', '1')->get();
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $studentData = Student::find($id);
            $data['studentData'] = $studentData;
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add';
        }
        return view('Administrator.student.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $student = new Student;

        $validator = Validator::make($request->all(), [
                    'studentFirstName' => 'required',
                    'studentLastName' => 'required',
                    'studentClass' => 'required',
                    'studentAge' => 'required',
                    'studentAddress' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $student = Student::find($id);
                $student->modifiedBy = Auth::user()->id;
                $student->modifiedOn = Config::get('constants.CURRENTDATE');
            }else{                
                $student->createdBy = Auth::user()->id;
                $student->createdOn = Config::get('constants.CURRENTDATE');
            }
            $student->studentFirstName = $request->studentFirstName;
            $student->studentLastName = $request->studentLastName;
            $student->studentClass = $request->studentClass;
            $student->studentAge = $request->studentAge;
            $student->studentContactPerson = $request->studentContactPerson;
            $student->studentAddress = $request->studentAddress;
            $student->studentContactEmail = $request->studentContactEmail;
            $student->studentContactPhone = $request->studentContactPhone;
            $student->centerCode = $request->centerCode;
            $student->subjectGroupId = $request->subjectGroupId;
            $student->subjectId = implode(",", $request->subjectId);
            $student->status = '1';
            $student->save();
            $studentId = $student->id;


            $registrationId = "REG".$request->centerCode.$studentId;

            return redirect('/administrator/student?page='.$page)->with('successMessage', 'Student information saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Student::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/student/?page=' . $page)->with('successMessage', 'Student status changed successfully.');
            } else {
                return \Redirect::to('administrator/student/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/student/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    
    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Student::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/student/?page=' . $page)->with('successMessage', 'Student deleted successfully.');
            } else {
                return \Redirect::to('administrator/student/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/student/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    

}
