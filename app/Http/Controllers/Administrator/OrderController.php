<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Model\Order;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\Shipment;
use App\Model\Addressbook;
use App\Model\Viewshipment;
use App\Model\ViewShipmentDetails;
use App\Model\Stores;
use App\Model\Deliverycompany;
use App\Model\Shippingmethods;
use App\Model\Paymentmethod;
use Illuminate\Contracts\Auth\Authenticatable;
use customhelper;
use Config;
use Excel;
use Carbon\Carbon;
use Geocoder;

/* use Excel;
  use Carbon\Carbon;
  use Illuminate\Support\Facades\Storage; */

class OrderController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 30;
    }

    public function index() {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Orders'), Auth::user()->id); // call the helper function
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $searchOrderArr = array(
            'idFrom' => '',
            'idTo' => '',
            'orderIdFrom' => '',
            'unitTo' => '',
            'unitFrom' => '',
            'orderStatus' => '',
            'prepaid' => '',
            'deliveryCompanyId' => '',
            'dispatchCompanyId' => '',
            'shipmentType' => '',
            'totalFrom' => '',
            'totalTo' => '',
            'user' => '',
            'receiver' => '',
            'paymentStatus' => '',
            'toCountry' => '',
            'warehouseId' => '',
            'labelType' => '',
            'itemType' => '',
            'packed' => '',
            'shipmentType' => '',
            'shippingMethod' => '',
            'rows' => '',
            'zones' => '',
            'warehouseId' => '',
        );


        if (\Request::isMethod('post')) {

            /* GET POST VALUE  */
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchOrder = \Input::get('searchOrder', $searchOrderArr);
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('ORDERDATA');
            \Session::push('ORDERDATA.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('ORDERDATA.searchDisplay', $searchDisplay);
            \Session::push('ORDERDATA.searchByDate', $searchByDate);
            \Session::push('ORDERDATA.searchOrder', $searchOrder);
            \Session::push('ORDERDATA.field', $field);
            \Session::push('ORDERDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchOrder'] = $searchOrder;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('ORDERDATA.field');
            $sortType = \Session::get('ORDERDATA.type');
            $searchByCreatedOn = \Session::get('ORDERDATA.searchByCreatedOn');
            $searchByDate = \Session::get('ORDERDATA.searchByDate');
            $searchOrder = \Session::get('ORDERDATA.searchOrder');
            $searchDisplay = \Session::get('ORDERDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdDate';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : 'createdDate';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchOrder'] = !empty($searchOrder) ? $searchOrder[0] : $searchOrderArr;
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }


        /* BUILD SORTING ARRAY */
        $sort = array(
            'id' => array('current' => 'sorting'),
            'firstName' => array('current' => 'sorting'),
            'totalCost' => array('current' => 'sorting'),
            'createdDate' => array('current' => 'sorting'),
            'type' => array('current' => 'sorting'),
            'shipmentId' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH ORDER LIST  */
        $orderData = Order::orderList($param);


        /* FETCH COUNTRY LIST  */
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

        $data['orderStatusNames'] = Order::allOrderStatus();
        $data['orderStatusClasses'] = Order::allOrderStatusClasses();
        $data['orderwiseCharbeableWeight'] = Order::orderwiseChargeableWeight($orderData);
        $data['shipmentTypeList'] = Shipment::allParticularShipmenttype();
        /* FETCH SHIPPING METHOD LIST  */
        $shippingMethodData = collect(Shippingmethods::where('active', 'Y')->where('deleted', '0')->orderby('shipping', 'asc')->get());
        $data['shippingMethodList'] = $shippingMethodData->mapWithKeys(function($item) {
            return [$item['shippingid'] => $item['shipping']];
        });
        $data['warehouseRowList'] = \App\Model\Warehouselocation::where('type', 'R')->where('status', '1')->orderby('name', 'asc')->get();
        if (!empty($param['searchOrder']['rows']))
            $data['warehouseZoneList'] = \App\Model\Warehouselocation::where('type', 'Z')->where('status', '1')->where('parentId', $param['searchOrder']['rows'])->get();
        /* FETCH Warehouse LIST  */
        $data['warehouseList'] = Shipment::getWarehouseList();
        $data['title'] = "Administrative Panel :: Orders";
        $data['contentTop'] = array('breadcrumbText' => 'Orders', 'contentTitle' => 'Orders', 'pageInfo' => 'This section allows you to manage orders');
        $data['pageTitle'] = "Orders";
        $data['page'] = $orderData->currentPage();
        $data['orderData'] = $orderData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['leftMenuSelection'] = array('menuMain' => 'leftNavOrders', 'menuSub' => '', 'menuSubSub' => 'leftNavManageOrders6');

        //\Session::forget('ORDERDATA.searchOrder');

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.orders.index', $data);
    }

    public function showall() {

        /* Delete session search fields */

        return \Redirect::to('administrator/orders');
    }

    public function changestatus($orderId, $page, Request $request) {

        if (\Request::isMethod('post')) {

            $orderObj = new Order;
            /* Fetch order details */
            $orderObj = $orderObj->find($orderId);
            /* Set post status to order object */
            $orderObj->status = $request->input('orderStatus');
            if ($orderObj->save())
                return \Redirect::to('administrator/orders/?page=' . $page)->with('successMessage', 'Order status updated successfully.');
            else
                return \Redirect::to('administrator/orders/?page=' . $page)->with('errorMessage', 'Something went wrong.');
        }
        else {
            /* This section used to show status modal */
            $orderObj = new Order;
            /* Fetch order details */
            $orderObj = $orderObj->find($orderId);
            /* Set current order status */
            $data['orderStatus'] = $orderObj->status;
            $data['page'] = $page;
            /* Fetch all available statuses or order */
            $data['orderStatusNames'] = Order::allOrderStatus();
            $data['id'] = $orderId;
            $data['route'] = 'updateorderstatus';
            return view('Administrator.orders.changestatus', $data);
        }
    }

    public function bulkchangestatus($orderId, $page, Request $request) {

        /* Convert string to order id array to update status */

        $orderIdList = explode('^', $orderId);

        if (\Request::isMethod('post')) {
            if (!empty($orderIdList)) {
                foreach ($orderIdList as $eachOrderId) {

                    $orderObj = new Order;
                    $orderObj = $orderObj->find($eachOrderId);
                    $orderObj->status = $request->input('orderStatus');
                    $orderObj->save();
                }
            }
            return \Redirect::to('administrator/orders/?page=' . $page)->with('successMessage', 'Order status updated successfully.');
        } else {

            $orderObj = new Order;
            $orderObj = $orderObj->find($orderIdList[0]);
            $data['orderStatus'] = $orderObj->status;
            $data['page'] = $page;
            $data['orderStatusNames'] = Order::allOrderStatus();
            $data['id'] = $orderId;
            $data['route'] = 'updatebulkorderstatus';
            return view('Administrator.orders.changestatus', $data);
        }
    }

    public function delete($orderId, $page) {

        if (strstr($orderId, '^')) {
            /* Convert string to order id array to update status */
            $orderIdList = explode('^', $orderId);
            foreach ($orderIdList as $eachOrderId) {

                $orderObj = new Order;
                $orderObj = $orderObj->find($eachOrderId);
                $orderObj->deleted = '1';
                $orderObj->deletedBy = Auth::user()->email;
                $orderObj->deletedOn = date('Y-m-d h:i:s');
                $orderObj->save();
            }

            return \Redirect::to('administrator/orders/?page=' . $page)->with('successMessage', 'Order record deleted successfully.');
        } else {

            $orderObj = new Order;
            $orderObj = $orderObj->find($orderId);
            $orderObj->deleted = '1';
            $orderObj->deletedBy = Auth::user()->email;
            $orderObj->deletedOn = date('Y-m-d h:i:s');
            if ($orderObj->save())
                return \Redirect::to('administrator/orders/?page=' . $page)->with('successMessage', 'Order record deleted successfully.');
        }
    }

    /**
     * This function used to generate and export Selected shipment records
     * @param type $page
     * @param Request $request
     * @return type
     */
    public function exportselected($page, Request $request) {

        $orderObj = new Order;
        /* Get data for export */
        $exportData = $orderObj->exportData(array(), $request->selected, $request->selectedField);
        //print_r($exportData);exit;

        $excelName = "Order-Data" . Carbon::now();
        ob_end_clean();
        ob_start();
        /* Generate Excel */
        Excel::create($excelName, function($excel) use($exportData) {
            $excel->sheet('Sheet 1', function($sheet) use($exportData) {
                $sheet->fromArray($exportData);
            });
        })->store('xlsx', public_path('exports'));
        ob_flush();
        $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
        return response()->json(['path' => $creatingPath]);
    }

    /**
     * This function used to generate and export All shipment records
     * @param type $page
     * @param Request $request
     */
    public function exportall($page, Request $request) {

        $orderObj = new Order;
        $param = array();
        /* Initialize search field data */
        $searchOrderArr = array(
            'idFrom' => '',
            'idTo' => '',
            'orderIdFrom' => '',
            'unitTo' => '',
            'unitFrom' => '',
            'orderStatus' => '',
            'prepaid' => '',
            'deliveryCompanyId' => '',
            'dispatchCompanyId' => '',
            'shipmentType' => '',
            'totalFrom' => '',
            'totalTo' => '',
            'user' => '',
            'receiver' => '',
            'paymentStatus' => '',
            'toCountry' => '',
            'warehouseId' => '',
            'labelType' => '',
            'itemType' => '',
            'packed' => '',
            'shipmentType' => '',
            'shippingMethod' => '',
            'rows' => '',
            'zones' => '',
            'warehouseId' => '',
        );
        $searchByCreatedOn = \Session::get('ORDERDATA.searchByCreatedDate');
        $searchByDate = \Session::get('ORDERDATA.searchByDate');
        $searchOrder = \Session::get('ORDERDATA.searchOrder');
        //print_r($searchOrder);
        $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
        $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
        $param['searchOrder'] = !empty($searchOrder) ? $searchOrder[0] : $searchOrderArr;
//        print_r($param);
        //exit;
        /* Get data to export */
        $exportData = $orderObj->exportData($param, array(), $request->selectall);
        
        $excelName = "Order-Data" . Carbon::now();
        ob_end_clean();
        ob_start();
        /* Generate excell */
        Excel::create($excelName, function($excel) use($exportData) {
            $excel->sheet('Sheet 1', function($sheet) use($exportData) {
                $sheet->fromArray($exportData);
            });
        })->export('xlsx');
        ob_flush();
    }

    public function showaddressonmap($userId, $page) {

        $fullAddress = array();
        $data = array();
        /* Fetch shipping address of customer */
        $shippingAddress = Addressbook::where('userId', $userId)->where('isDefaultShipping', '1')->where('deleted', '0')->first();
        if (!empty($shippingAddress)) {
            /* Fetch country,state,city and prepare address on map */
            if (isset($shippingAddress->countryId))
                $country = Country::find($shippingAddress->countryId);
            if (isset($shippingAddress->stateId))
                $state = State::find($shippingAddress->stateId)->name;
            if (isset($shippingAddress->cityId))
                $city = City::find($shippingAddress->cityId)->name;
            $fullAddress[] = (isset($shippingAddress->address) ? $shippingAddress->address : '');
            //$fullAddress[] = (isset($shippingAddress->zipcode)?$shippingAddress->zipcode:'');
            $fullAddress[] = (isset($state) ? $state : '');

            $fullAddress[] = (isset($country->code) ? $country->code : '');

            if (!empty($fullAddress)) {

                $curl = curl_init();

                curl_setopt($curl, CURLOPT_URL, 'https://maps.googleapis.com/maps/api/geocode/json?address=' . str_replace(" ", "+", implode(',', $fullAddress)) . '&key=AIzaSyAnc0yAYlPhMjaBPrOOuS5ss1BWrfhF6_Q');

                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                /* Get latitute and longitude and other details of customer address to show on map */
                $addressDetails = curl_exec($curl);

                curl_close($curl);

                $data['adressDetails'] = $addressDetails;
            }
        }

        return view('Administrator.orders.addressonmap', $data);
    }

    public function addedit($orderId, $page) {

        $data = array();

        /* fetch Shipment details with order data */
        $orderShipmentInfo = Viewshipment::where('orderId', $orderId)->first();
        /* Fetch Order details data */
        $orderDetails = Order::find($orderId);
        /* Fetch  Shipment Delivery Data */
        $deliveryData = ViewShipmentDetails::where("shipmentId", $orderShipmentInfo->id)->where('deleted', '0')->where('packageDeleted', '0')->where('packageType', 'I')->get()->toarray();
        $data['shipment']['delivery'] = Shipment::getDeliveryDetails($deliveryData);
        /* FETCH STORE LIST  */
        $storeData = collect(Stores::where('deleted', '0')->where('status', '1')->orderby('storeName', 'asc')->get());
        $data['storeList'] = $storeData->mapWithKeys(function($item) {
            return [$item['id'] => $item['storeName']];
        });

        /* FETCH DELIVERY COMPANY LIST  */
        $deliveryCompanyData = collect(Deliverycompany::where('status', '1')->orderby('name', 'asc')->get());
        $data['deliveryCompanyList'] = $deliveryCompanyData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        /* Fetch shipping address of customer */
        $data['shippingAddress'] = Addressbook::where('userId', $orderShipmentInfo->userId)->where('isDefaultShipping', '1')->where('deleted', '0')->first();
        /* Fetch billing address of customer */
        $data['billingAddress'] = Addressbook::where('userId', $orderShipmentInfo->userId)->where('isDefaultBilling', '1')->where('deleted', '0')->first();
        $data['orderShipmentInfo'] = $orderShipmentInfo;
        $data['orderDetails'] = $orderDetails;
        /* Fetch payment method of order */
        $paymentMethod = 'N/A';
        if ($orderDetails->paymentId != '' || $orderDetails->paymentId != 0)
            $paymentMethod = PaymentMethod::find($orderDetails->paymentId)->paymentMethod;
        $data['paymentMethod'] = $paymentMethod;
        /* Fetch delivery method of order */
        $data['deliveryMethod'] = Shippingmethods::where('shippingid', $orderDetails->shippingId)->first()->shipping;
        $data['title'] = "Administrative Panel :: Orders";
        $data['contentTop'] = array('breadcrumbText' => 'Orders', 'contentTitle' => 'Orders', 'pageInfo' => 'This section allows you to manage orders');
        $data['pageTitle'] = "Orders";
        $data['page'] = $page;

        return view('Administrator.orders.edit', $data);
    }

    public function savecustomernote($orderId = '-1', $page, Request $request) {

        if ($orderId != '-1' && $request->input('customerNote') != '') {
            $dataObj = Order::find($orderId);
            $dataObj->customerNotes = $request->input('customerNote');
            if ($dataObj->save())
                return \Redirect::to(route('orderdetails', ['orderId' => $orderId, 'page' => $page]))->with('successMessage', 'Customer notes updated successfully.');
            else
                return \Redirect::to(route('orderdetails', ['orderId' => $orderId, 'page' => $page]))->with('errorMessage', 'Something Went wrong.');
        } else
            return \Redirect::to(route('orderdetails', ['orderId' => $orderId, 'page' => $page]))->with('errorMessage', 'Customer notes can not be blank.');
    }

    /**
     * This function used to generate and export All shipment records
     * @param type $page
     * @param Request $request
     */
    public function vieworder($id, $pageId, Request $request) {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Orders'), Auth::user()->id); // call the helper function
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $data['title'] = "Administrative Panel :: Order Details";
        $data['contentTop'] = array('breadcrumbText' => 'Order Details', 'contentTitle' => 'Order Details', 'pageInfo' => 'This section allows you to view orders');
        $data['pageTitle'] = "Order Details";
        $snapshots = array('snapshots' => array());
        //$orderDetails = Order::where('id', $id)->where('status', '5')->first();

        $orderDetails = Order::where('id', $id)->first();  //Updated on 28-11-2018

        if (!empty($orderDetails)) {

            $data['orderDetails'] = $orderDetails;
            

            $userDetails = \App\Model\User::where('id', $orderDetails->userId)->first();
            $data['userDetails'] = $userDetails;

            $addressbookShip = Order::getaddressbookShip($userDetails->id);
            $data['addressbookShip'] = $addressbookShip[0];

            $addressbookBill = Order::getaddressbookBill($userDetails->id);
            $data['addressbookBill'] = $addressbookBill[0];


            if ($orderDetails->type == 'shipment') {

                $shipmentDetails = Viewshipment::where('id', $orderDetails->shipmentId)->where('deleted', '0')->first();
                $data['shipmentDetails'] = $shipmentDetails;

                $deliveryData = ViewShipmentDetails::where("shipmentId", $orderDetails->shipmentId)->where('deleted', '0')->where('packageDeleted', '0')->where('packageType', 'I')->get()->toarray();
                $data['shipment']['delivery'] = Shipment::getDeliveryDetails($deliveryData);

                $data['invoiceDetails'] = \App\Model\Invoice::where("shipmentId", $orderDetails->shipmentId)->where("invoiceType", 'receipt')->whereRaw("(type='shopforme' or type='autopart' or type='othershipment')")
                                ->where('deleted', '0')->get();

                return view('Administrator.orders.view', $data);
            } else if ($orderDetails->type == 'autoshipment') {

                $shipmentDetails = \App\Model\Autoshipment::getautoshipmentdetails($orderDetails->shipmentId);
                $data['shipmentDetails'] = $shipmentDetails;

                $data['autoShipmentImage'] = \App\Model\Autoshipmentitemimage::where('autoShipmentId', $orderDetails->shipmentId)->where('imageType', 'carpicture')->first();

                $data['invoiceDetails'] = \App\Model\Invoice::where('shipmentId', $orderDetails->shipmentId)->where("invoiceType", 'receipt')->whereRaw("(type='autoshipment' or type='buycarforme')")->where('deleted', '0')->get();
                $data['statusLog'] = \App\Model\Autoshipmentstatuslog::getOrderStatusProgress($orderDetails->shipmentId);

                return view('Administrator.orders.viewautoshipment', $data);
            } else {
                $shipmentDetails = \App\Model\Fillship::where('id', $orderDetails->shipmentId)->where('deleted', '0')->first();
                $data['shipmentDetails'] = $shipmentDetails;

                $data['shipmentDetailsFromAddon'] = \App\Model\Fillship::getFromCountryetc($orderDetails->shipmentId);
                $data['shipmentDetailsToAddon'] = \App\Model\Fillship::getToCountryetc($orderDetails->shipmentId);
                $data['shipAndPaymentDetails'] = \App\Model\Fillship::getShipmentAndPayment($orderDetails->shipmentId);

                //$data['deliveryData'] = \App\Model\Fillshipitem::where("fillshipId", $orderDetails->shipmentId)->where('deleted', '0')->get()->toarray();
                $data['deliveryData'] = \App\Model\Fillshipitem::getItems($orderDetails->shipmentId);
                //dd($res);
                //$data['shipment']['delivery'] = Shipment::getDeliveryDetails($deliveryData);

                $data['invoiceDetails'] = \App\Model\Invoice::where("shipmentId", $orderDetails->shipmentId)->where("invoiceType", 'receipt')->where('type', 'fillship')
                                ->where('deleted', '0')->get();

                return view('Administrator.orders.fillshipview', $data);
            }
        } else {
            dd("No Data Found!");
        }
    }

    /**
     * Method used to get the snapshot
     * @param integer $shipmentId
     * @param integer $deliveryId
     * @param integer $packageId
     * @return array
     */
    public function getsnapshots($shipmentId, $deliveryId, $packageId) {
        $data = \App\Model\Shipmentsnapshot::where('shipmentId', $shipmentId)->where('packageId', $packageId)->get();
        if (!empty($data)) {
            return view('Administrator.orders.snapshotimages', array('data' => $data));
        } else {
            return view('Administrator.orders.snapshotimages', array('data' => $data));
        }
    }
    
    public function getwerehouselocations(Request $request) {
        //print_r($request->listIds);exit;
        $postData = json_decode($request->listIds,TRUE);
        $counter = 0;
        if(!empty($postData['shipment']))
        {
            $listIds = implode(',',$postData['shipment']);
            $locationRecords = \App\Model\Shipment::getShipmentLocations($listIds);
            $returnLocationArr = array();
            
            if(!empty($locationRecords))
            {
                foreach($locationRecords as $shipment)
                {
                    $locations = explode(",",$shipment->shipmentLocation);
                    $printedLocations = array();
                    $displayLocation = '';
                    foreach($locations as $eachLocations)
                    {
                        if(!in_array($eachLocations,$printedLocations))
                        {
                            $displayLocation .= $eachLocations.',';
                            $printedLocations[] = $eachLocations;
                        }
                    }
                    $displayLocation = substr($displayLocation,0,'-1');
                    $returnLocationArr['shipment'][$counter]['id'] = $shipment->id;
                    $returnLocationArr['shipment'][$counter]['location'] = $displayLocation;
                    $counter++;
                }
            }
        }
        
        if(!empty($postData['fillship']))
        {
            $fillshipListIds = implode(',',$postData['fillship']);
            $fillshipLocations = \App\Model\Fillshipwarehouselocation::getWarehouLocation($fillshipListIds);
            if(!empty($fillshipLocations))
            {
                foreach($fillshipLocations as $shipment)
                {
                    $locations = explode(",",$shipment->shipmentLocation);
                    $printedLocations = array();
                    $displayLocation = '';
                    foreach($locations as $eachLocations)
                    {
                        if(!in_array($eachLocations,$printedLocations))
                        {
                            $displayLocation .= $eachLocations.',';
                            $printedLocations[] = $eachLocations;
                        }
                    }
                    $displayLocation = substr($displayLocation,0,'-1');
                    $returnLocationArr['fillship'][$counter]['id'] = $shipment->id;
                    $returnLocationArr['fillship'][$counter]['location'] = $displayLocation;
                    $counter++;
                }
            }
        }

        if(!empty($returnLocationArr))
        {
            return json_encode($returnLocationArr);
        }
        else
        {
            return json_encode(array());
        }
    }

}
