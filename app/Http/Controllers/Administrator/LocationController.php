<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Location;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class LocationController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Location'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchByCountry = \Input::get('searchByCountry', '');
            $searchByState = \Input::get('searchByState', '');
            $searchByCity = \Input::get('searchByCity', '');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('LOCATIONDATA');
            \Session::push('LOCATIONDATA.searchByCountry', $searchByCountry);
            \Session::push('LOCATIONDATA.searchByState', $searchByState);
            \Session::push('LOCATIONDATA.searchByCity', $searchByCity);
            \Session::push('LOCATIONDATA.searchDisplay', $searchDisplay);
            \Session::push('LOCATIONDATA.field', $field);
            \Session::push('LOCATIONDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCountry'] = $searchByCountry;
            $param['searchByState'] = $searchByState;
            $param['searchByCity'] = $searchByCity;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('LOCATIONDATA.field');
            $sortType = \Session::get('LOCATIONDATA.type');
            $searchByCountry = \Session::get('LOCATIONDATA.searchByCountry');
            $searchByState = \Session::get('LOCATIONDATA.searchByState');
            $searchByCity = \Session::get('LOCATIONDATA.searchByCity');
            $searchDisplay = \Session::get('LOCATIONDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'displayOrder';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchByCountry'] = !empty($searchByCountry) ? $searchByCountry[0] : '';
            $param['searchByState'] = !empty($searchByState) ? $searchByState[0] : '';
            $param['searchByCity'] = !empty($searchByCity) ? $searchByCity[0] : '';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'displayOrder' => array('current' => 'sorting'),
            'countryName' => array('current' => 'sorting'),
            'stateName' => array('current' => 'sorting'),
            'cityName' => array('current' => 'sorting'),
            'businessName' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTRY LIST  */
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

        $data['stateList'] = $data['cityList'] = array();
        if (isset($param['searchByCountry']) && !empty($param['searchByCountry'])) {
            /* FETCH COUNTRY CODE */
            $location = Country::where('id', $param['searchByCountry'])->get(['code']);
            $countryCode = $location[0]['code'];

            /* FETCH STATE LIST  */
            $data['stateList'] = State::where('status', '1')
                    ->where('deleted', '0')
                    ->where('countryCode', $countryCode)
                    ->orderby('name', 'asc')
                    ->get();

            /* FETCH CITY LIST  */
            if (isset($param['searchByState']) && !empty($param['searchByState'])) { // Check if State Id Exists
                $state = State::where('id', $param['searchByState'])->get(['code']);
                $stateCode = $state[0]['code'];
                if (isset($stateCode) && !empty($stateCode)) {
                    $data['cityList'] = City::where('status', '1')
                            ->where('deleted', '0')
                            ->where('stateCode', $stateCode)
                            ->where('countryCode', $countryCode)
                            ->orderby('name', 'asc')
                            ->get();
                            
                    if (empty($data['cityList'])) { // If No Staes Under Country Show All Cities Based on Country Id
                        $data['cityList'] = City::where('status', '1')
                                ->where('deleted', '0')
                                ->where('countryCode', $countryCode)
                                ->orderby('name', 'asc')
                                ->get();
                    }
                }
            } else { // Show All Cities Based on Country Id
                $data['cityList'] = City::where('status', '1')
                        ->where('deleted', '0')
                        ->where('countryCode', $countryCode)
                        ->orderby('name', 'asc')
                        ->get();
            }
            /* FETCH CITY LIST  */
        }



        /* FETCH LOCATION LIST  */
        $locationData = Location::getLocationList($param);

        /* Fetch Data For Procurement Iems & Shipment Packages */
        $data['locationIdExists'] = array();
        $data['locationIdExists'] = \App\Model\Addressbook::fetchUsedFieldIds('locationId');

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Locations";
        $data['contentTop'] = array('breadcrumbText' => 'Locations', 'contentTitle' => 'Locations', 'pageInfo' => 'This section allows you to manage different locations');
        $data['pageTitle'] = "Locations";
        $data['page'] = $locationData->currentPage();
        $data['locationData'] = $locationData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        
        return view('Administrator.location.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();
        $data['stateList'] = array();
        $data['cityList'] = array();
        $data['page'] = !empty($page) ? $page : '1';
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $data['pageTitle'] = "Edit Location";
            $location = Location::find($id);
            $countryCode = Country::where('id', $location->countryId)->orderby('name', 'asc')->get(['code']);
            $data['location'] = $location;
            $data['stateList'] = State::where('status', '1')
                    ->where('deleted', '0')
                    ->where('countryCode', $countryCode[0]['code'])
                    ->orderby('name', 'asc')
                    ->get();
            
            $state = State::where('id', $location->stateId)->get(['code', 'countryCode']);
            $stateCode = $state[0]['code'];
            $countryCode = $state[0]['countryCode'];
            $data['cityList'] = City::where('status', '1')
                    ->where('deleted', '0')
                    ->where('stateCode', $stateCode)
                    ->where('countryCode', $countryCode)
                    ->orderby('name', 'asc')
                    ->groupby('id')
                    ->get();
            
            return view('Administrator.location.add', $data);
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add Location";
            return view('Administrator.location.add', $data);
        }
    }

    /**
     * Method to fetch state list
     * @return array
     */
    public function getstatelist($countryid = '') {
        $data = array();
        $location = Country::where('id', $countryid)->orderby('name', 'asc')->get(['code']);
        $phCode = Country::select('isdCode')->where('id', $countryid)->orderby('name', 'asc')->get()->toArray();

        $countryCode = $location[0]['code'];
        $data['phCode'] = $phCode[0]['isdCode'];
        // $stateList = array();
        if (isset($countryCode) && !empty($countryCode)) {
            $data['stateList'] = State::where('status', '1')
                    ->where('deleted', '0')
                    ->where('countryCode', $countryCode)
                    ->orderby('name', 'asc')
                    ->get();
        }
        echo json_encode($data);
        exit;
    }

    /**
     * Method to fetch city list
     * @return array
     */
    public function getcitylist($stateid = '') {
        $state = State::where('id', $stateid)->get(['code', 'countryCode']);
        $stateCode = $state[0]['code'];
        $countryCode = $state[0]['countryCode'];

        $cityList = array();
        if (isset($stateCode) && !empty($stateCode)) {
            $cityList = City::where('status', '1')
                    ->where('deleted', '0')
                    ->where('stateCode', $stateCode)
                    ->where('countryCode', $countryCode)
                    ->orderby('name', 'asc')
                    ->groupby('id')
                    ->get();
        }
        echo json_encode($cityList);
        exit;
    }

    /**
     * Method to fetch city list by location
     * @return array
     */
    public function getcitylistbycountry($countryid = '') {
        $location = Country::where('id', $countryid)->get(['code']);
        $countryCode = $location[0]['code'];
        $cityList = array();
        if (isset($countryCode) && !empty($countryCode)) {
            $cityList = City::where('status', '1')
                    ->where('deleted', '0')
                    ->where('countryCode', $countryCode)
                    ->orderby('name', 'asc')
                    ->groupby('id')
                    ->get();
        }
        echo json_encode($cityList);
        exit;
    }

    /*     * Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {
        //print_r($request->all());exit;
        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $location = new Location;

        if (empty($id)) {
            $validator = Validator::make($request->all(), [
                        'countryId' => 'required',
                        'cityId' => 'required',
                        'businessName' => 'required',
                        'address' => 'required',
                        'phone' => 'required|min:10',
                        'email' => 'required|email',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                        'phone' => 'required|min:10',
                        'email' => 'required|email',
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $location = Location::find($id);
                $location->alternatePhone = $request->alternatePhone;
                if ($request->alternatePhone != '') {
                    $location->altIsdCode = substr($request->altIsdCode, 1);
                }
                $location->zipcode = $request->zipcode;
                $location->phone = $request->phone;
                $location->isdCode = substr($request->isdCode, 1);
                $location->fax = $request->fax;
                $location->email = $request->email;
                $location->modifiedBy = Auth::user()->id;
                $location->modifiedOn = Config::get('constants.CURRENTDATE');
                
                \App\Model\Addressbook::where('locationId',$id)->update(['phone'=>$request->phone,'email'=>$request->email]);
                
            } else {
                $location->countryId = $request->countryId;
                $location->stateId = $request->stateId;
                $location->cityId = $request->cityId;
                $location->businessName = $request->businessName;
                $location->address = $request->address;
                $location->alternateAddress = $request->alternateAddress;
                $location->alternatePhone = $request->alternatePhone;
                if ($request->alternatePhone != '') {
                    $location->altIsdCode = substr($request->altIsdCode, 1);
                }
                $location->zipcode = $request->zipcode;
                $location->phone = $request->phone;
                $location->isdCode = substr($request->isdCode, 1);
                $location->fax = $request->fax;
                $location->email = $request->email;
                //$location->comment = $request->comment;
                $location->createdBy = Auth::user()->id;
                $location->createdOn = Config::get('constants.CURRENTDATE');
            }

            $location->save();
            $locationId = $location->id;

            if (empty($id)) {
                $location = Location::find($locationId);
                $location->displayOrder = $locationId;
                $location->save();
            }

            return redirect('/administrator/location?page=' . $page)->with('successMessage', 'Location information saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Location::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/location/?page=' . $page)->with('successMessage', 'Location status changed successfully.');
            } else {
                return \Redirect::to('administrator/location/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/location/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $locationIdExists = array();
        $locationIdExists = \App\Model\Addressbook::fetchUsedFieldIds('locationId');

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (!in_array($id, $locationIdExists)) {
                if (Location::deleteRecord($id, $createrModifierId)) {
                    return \Redirect::to('administrator/location/?page=' . $page)->with('successMessage', 'Location deleted successfully.');
                } else {
                    return \Redirect::to('administrator/location/?page=' . $page)->with('errorMessage', 'Error in operation!');
                }
            } else {
                return \Redirect::to('administrator/location/?page=' . $page)->with('errorMessage', 'Location cannot be deleted. Dependency exists.');
            }
        } else {
            return \Redirect::to('administrator/location/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
