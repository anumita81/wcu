<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Customerhelpcategories;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;

class DhltradetermsController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {

        $data = array();

        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.DhlTradetermsManagement'), Auth::user()->id);
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('CH');
            \Session::push('CH.searchDisplay', $searchDisplay);
            \Session::push('CH.field', $field);
            \Session::push('CH.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('CH.field');
            $sortType = \Session::get('CH.type');
            $searchDisplay = \Session::get('CH.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
            'orders' => array('current' => 'sorting'),
            'status' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH make LIST  */
        $chCatData = \App\Model\Dhltradetermssettings::where('deleted', '0')->orderBy($param['field'], $param['type'])->paginate($param['searchDisplay']);
        if (count($chCatData) > 0) {
            $data['page'] = $chCatData->currentPage();
        } else {
            $data['page'] = 1;
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: DHL Terms of Trades Management";
        $data['contentTop'] = array('breadcrumbText' => 'DHL Terms of Trades Management', 'contentTitle' => 'DHL Terms of Trades Management', 'pageInfo' => 'This section allows you to manage DHL terms of trades');
        $data['pageTitle'] = "DHL Terms of Trades Management";
        $data['chCatData'] = $chCatData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavContent', 'menuSub' => 'leftNavCustomerHelp2', 'menuSubSub' => 'leftNavManageCategories111');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.dhltradeterms.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $id
     * @param string $page
     * @return array
     */
    public function addedit($id = '0', $page = '') {
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.CustomerHelpCategories'), Auth::user()->id); 
        $data = array();
        $data['title'] = "Administrative Panel :: DHL Account Management";
        $data['contentTop'] = array('breadcrumbText' => 'DHL Account Management', 'contentTitle' => 'DHL Account Management', 'pageInfo' => 'This section allows you to manage DHL accounts');
        $data['page'] = !empty($page) ? $page : '1';

        if (!empty($id)) {
            if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['pageTitle'] = "Edit DHL Terms of Trade";
            $data['chCatId'] = $id;
            $data['page'] = $page;
            $data['action'] = 'Edit';
            $data['chCatData'] = \App\Model\Dhltradetermssettings::find($id);
        } else {
            if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add New DHL Terms of Trade";
            $data['page'] = $page;
        }
        return view('Administrator.dhltradeterms.add', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $id
     * @param string $page
     * @param Request $request
     * @return array
     */
    public function savedata($id, $page, Request $request) {

        $data = array();
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.CustomerHelpCategories'), Auth::user()->id);
        $data['page'] = !empty($page) ? $page : '1';
        $chCat = new \App\Model\Dhltradetermssettings;

        /* VALIDATATION */
        $validator = Validator::make(\Input::all(), [
            'tradeTermsTitle' => 'required',
            'tardeTermsDescription' => 'required',
        ]);

        /* IF VALIDATATIONS FAILS */
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

            /*  EDIT  */
            if ($id != 0) { 
                if($findRole['canEdit'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
                $chCat = \App\Model\Dhltradetermssettings::find($id);
                $chCat->updatedBy = Auth::user()->email;
                $chCat->updatedOn = Config::get('constants.CURRENTDATE');
            } 
            /*  ADD  */
            else {
                if($findRole['canAdd'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
                
                $chCat->createdBy = Auth::user()->email;
                $chCat->createdOn = Config::get('constants.CURRENTDATE');
            }
            $chCat->tradeTermsTitle = $request->tradeTermsTitle;
            $chCat->tardeTermsDescription = $request->tardeTermsDescription;

            /* SAVING THE DATA */
            $chCat->save();
        }


        return redirect('/administrator/managedhltradeterms?page=' . $page)->with('successMessage', 'Category saved successfuly.');
    }




    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.CustomerHelpCategories'), Auth::user()->id); 
        if($findRole['canDelete'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $page = !empty($page) ? $page : '1';

        /* CHECK AT LEAST ONE CATEGORY LIVE */
        $checkLive = \App\Model\Dhltradetermssettings::where("deleted", '0')->count();
        if($checkLive == 1){
            return \Redirect::to('administrator/managedhltradeterms/?page=' . $page)->with('errorMessage', 'At least one account should activated');
        }

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (\App\Model\Dhltradetermssettings::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/managedhltradeterms?page=' . $page)->with('successMessage', 'DHL account deleted successfully.');
            } else {
                return \Redirect::to('administrator/managedhltradeterms?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/managedhltradeterms?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    
}
