<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Country;
use App\Model\State;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class StateController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.State'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchByCountry = \Input::get('searchByCountry', '');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('STATEDATA');
            \Session::push('STATEDATA.searchByCountry', $searchByCountry);
            \Session::push('STATEDATA.searchDisplay', $searchDisplay);
            \Session::push('STATEDATA.field', $field);
            \Session::push('STATEDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCountry'] = $searchByCountry;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('STATEDATA.field');
            $sortType = \Session::get('STATEDATA.type');
            $searchByCountry = \Session::get('STATEDATA.searchByCountry');
            $searchDisplay = \Session::get('STATEDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'code';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchByCountry'] = !empty($searchByCountry) ? $searchByCountry[0] : '';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
            'code' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH STATE LIST  */
        $stateData = State::getStateList($param);

        /* FETCH COUNTRY LIST  */
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: States";
        $data['contentTop'] = array('breadcrumbText' => 'States', 'contentTitle' => 'States', 'pageInfo' => '');
        $data['pageTitle'] = "States";
        $data['page'] = $stateData->currentPage();
        $data['stateData'] = $stateData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.state.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();
        $data['pageTitle'] = "States";
        $data['page'] = !empty($page) ? $page : '1';
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $state = State::find($id);
            $data['state'] = $state;
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['state'] = array();
        }
        return view('Administrator.state.addedit', $data);
    }

    /*     * Edit
     * Method used to save content information
     * @param integer $id
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $state = new State;

        $validator = Validator::make($request->all(), [
                    //   'code' => 'required|unique:' . $state->table . ',code,' . $id . '|alpha',
                    'code' => 'required|alpha',
                    'name' => 'required|regex:/^[\pL\s\-]+$/u|unique:' . $state->table . ',name,NULL,' . $id . ',deleted,1',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $state = State::find($id);
                $state->modifiedBy = Auth::user()->id;
                $state->modifiedOn = Config::get('constants.CURRENTDATE');
            } else {
                $state->countryCode = $request->countryCode;
                $state->createdBy = Auth::user()->id;
                $state->createdOn = Config::get('constants.CURRENTDATE');
            }
            $state->code = $request->code;
            $state->name = $request->name;
            $state->save();
            $stateId = $state->id;

            return redirect('/administrator/state?page=' . $page)->with('successMessage', 'State information saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (State::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/state/?page=' . $page)->with('successMessage', 'State status changed successfully.');
            } else {
                return \Redirect::to('administrator/state/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/state/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (State::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/state/?page=' . $page)->with('successMessage', 'State deleted successfully.');
            } else {
                return \Redirect::to('administrator/state/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/state/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
