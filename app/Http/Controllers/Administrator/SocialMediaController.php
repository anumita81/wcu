<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Socialmedia;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class SocialMediaController extends Controller {

	public $_perPage;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
		$this->_perPage=10;
    }

    public function index() {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Socialmedia'), Auth::user()->id); // call the helper function
        if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

       $data['title'] = "Administrative Panel :: Settings";
       $data['pageTitle'] = "Social Media Settings";
       $data['contentTop'] = array('breadcrumbText'=>'Social Media', 'contentTitle'=>'Social Media Settings', 'pageInfo'=>'This section allows you to update social media settings');

       $userId = Auth::id();

       $data['settings'] = Socialmedia::getSettingsData()->toArray();


      $data['canView'] = $findRole['canView'];
      $data['canAdd'] = $findRole['canAdd'];
      $data['canEdit'] = $findRole['canEdit'];
      $data['canDelete'] = $findRole['canDelete'];
       return view('Administrator.socialmedia.index', $data);
    }
	
    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata(Request $request){
        
        $data = array();
		
        $socialmedia = new Socialmedia;

        //print_r($request->post()); die;

        foreach($request->post() as $key=> $val)
        {
          if($key != "_token"){ 



                $socialId = Socialmedia::where('media', $key)->get(['id']);
                $socialId = $socialId[0]['id'];
                
               $socialmedia = Socialmedia::find($socialId);
               $socialmedia->mediaLink = $val[1];
               $socialmedia->save();


          }


        }

		return redirect('/administrator/socialmedia/index')->with('successMessage', 'Social Media saved successfuly.');
		
    }
	
	
	 /**
     * Method used to unset search session data
     *
     */
    public function cleardata() {
        \Session::forget('SETTINGSDATA');
        return \Redirect::to('administrator/generalsettings');
    }

}
