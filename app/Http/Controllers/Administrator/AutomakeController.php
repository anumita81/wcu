<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Automake;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;

class AutomakeController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoMake'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('AUTOMAKEDATA');
            \Session::push('AUTOMAKEDATA.searchDisplay', $searchDisplay);
            \Session::push('AUTOMAKEDATA.field', $field);
            \Session::push('AUTOMAKEDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('AUTOMAKEDATA.field');
            $sortType = \Session::get('AUTOMAKEDATA.type');
            $searchDisplay = \Session::get('AUTOMAKEDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
            'type' => array('current' => 'sorting'),
            'orders' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH make LIST  */
        $makeData = Automake::where('deleted', '0')->orderBy($param['field'], $param['type'])->paginate($param['searchDisplay']);
        $makeIdUsedAuto = \App\Model\Autoshipment::fetchUsedFieldIds('makeId');
        $makeIdUsedProcurement = \App\Model\Procurementitem::fetchUsedFieldIds('makeId');
        $makeIdUsed = array_unique(array_merge($makeIdUsedAuto,$makeIdUsedProcurement));
        if (count($makeData) > 0) {
            $data['page'] = $makeData->currentPage();
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Make";
        $data['contentTop'] = array('breadcrumbText' => 'Make', 'contentTitle' => 'Make', 'pageInfo' => 'This section allows you to manage the make details');
        $data['pageTitle'] = "Make";
        $data['makeData'] = $makeData;
        $data['makeIdUsed'] = $makeIdUsed;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavAuto', 'menuSub' => '', 'menuSubSub' => 'leftNavMakeSettings9');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.automake.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoMake'), Auth::user()->id); // call the helper function
        $data = array();
        $data['title'] = "Administrative Panel :: Make";
        $data['contentTop'] = array('breadcrumbText' => 'Make', 'contentTitle' => 'Make', 'pageInfo' => 'This section allows you to manage the make details');
        $data['page'] = !empty($page) ? $page : '1';

        if (!empty($id)) {
            if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['pageTitle'] = "Edit Make";
            $data['makeId'] = $id;
            $data['page'] = $page;
            $data['action'] = 'Edit';
            $data['makeData'] = Automake::find($id);
        } else {
            if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add New Make";
            $data['page'] = $page;
        }
        return view('Administrator.automake.add', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoMake'), Auth::user()->id); // call the helper function
        $data['page'] = !empty($page) ? $page : '1';
        $make = new Automake;

        if ($id != 0) { // Edit
            if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $make = Automake::find($id);
            $make->modifiedBy = Auth::user()->id;
            $make->modifiedOn = Config::get('constants.CURRENTDATE');
            $make->orders = $request->orders;
        } else { // Add
            if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $checkDuplicate = Automake::where('name', $request->name)->where('deleted', '0')->get();
            if (count($checkDuplicate) > 0) {
                return redirect('/administrator/automake/?page=' . $page)->with('errorMessage', 'Make already exists');
            }

            $getLastOrders = Automake::orderBy('orders', 'desc')->where('deleted', '0')->first();
            $newOrder = $getLastOrders->orders+1;
            $make->orders = $newOrder;
            
        }
        $make->name = $request->name;
        $make->type = $request->type;

        $make->save();


        return redirect('/administrator/automake?page=' . $page)->with('successMessage', 'Make information saved successfuly.');
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoMake'), Auth::user()->id); // call the helper function
        if($findRole['canDelete'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            $makeIdUsedAuto = \App\Model\Autoshipment::fetchUsedFieldIds('makeId');
            $makeIdUsedProcurement = \App\Model\Procurementitem::fetchUsedFieldIds('makeId');
            $makeIdUsed = array_unique(array_merge($makeIdUsedAuto,$makeIdUsedProcurement));
            if(!in_array($id,$makeIdUsed))
            {
            if (Automake::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/automake?page=' . $page)->with('successMessage', 'Make deleted successfully.');
            } else {
                return \Redirect::to('administrator/automake?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
            }
            else
            {
                return \Redirect::to('administrator/automake?page=' . $page)->with('errorMessage', 'Auto make cannot be deleted. Dependency exists.');
            }
        } else {
            return \Redirect::to('administrator/automake?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    
    /**
     * Method used to get makelist with selected type
     * @param $type
     * @return json
     */
    public function getmakelist($type)
    {
      $makeData = Automake::where('deleted', '0')->where('type', $type)->get();
      
      echo json_encode($makeData);
    }
}
