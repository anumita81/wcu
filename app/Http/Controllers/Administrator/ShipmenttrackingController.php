<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Shipmenttracking;
use App\Model\Shipmenttrackingnumber;
use App\Model\Shipmenttrackingnotes;
use App\Model\Shipmenttrackingfile;
use App\Model\Updatetype;
use App\Model\Deliverycompany;
use Auth;
use Excel;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;

class ShipmenttrackingController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {
        $data = array();

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Shipmenttracking'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $searchShipmentTrackingArr = array(
            'updateType' => '',
            'deliveryCompanyId' => '',
            'name' => '',
            'trackingNumber' => '',
        );

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchShipmentTracking = \Input::get('searchShipmentTracking', $searchShipmentTrackingArr);
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('TRACKINGDATA');
            \Session::push('TRACKINGDATA.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('TRACKINGDATA.searchDisplay', $searchDisplay);
            \Session::push('TRACKINGDATA.searchByDate', $searchByDate);
            \Session::push('TRACKINGDATA.searchShipmentTracking', $searchShipmentTracking);
            \Session::push('TRACKINGDATA.field', $field);
            \Session::push('TRACKINGDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchShipmentTracking'] = $searchShipmentTracking;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('TRACKINGDATA.field');
            $sortType = \Session::get('TRACKINGDATA.type');
            $searchByCreatedOn = \Session::get('TRACKINGDATA.searchByCreatedOn');
            $searchByDate = \Session::get('TRACKINGDATA.searchByDate');
            $searchShipmentTracking = \Session::get('TRACKINGDATA.searchShipmentTracking');
            $searchDisplay = \Session::get('TRACKINGDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchShipmentTracking'] = !empty($searchShipmentTracking) ? $searchShipmentTracking[0] : $searchShipmentTrackingArr;
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'cityName' => array('current' => 'sorting'),
            'countryName' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH TRACKING LIST  */
        $trackingData = Shipmenttracking::getTrackingList($param);
        
        $trackingFileExist = \App\Model\Shipmenttrackingfile::trackingFileExist(); 

        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();
        $data['updateTypeList'] = Updatetype::where('deleted', '0')->where('status', '1')->orderby('name', 'asc')->get();

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Shipment Tracking";
        $data['contentTop'] = array('breadcrumbText' => 'Shipment Tracking', 'contentTitle' => 'Shipment Tracking', 'pageInfo' => 'This section allows you to manage shipment tracking numbers');
        $data['pageTitle'] = "Shipment Tracking";
        $data['page'] = $trackingData->currentPage();
        $data['trackingData'] = $trackingData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.shipmenttracking.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();
        $data['updateTypeList'] = Updatetype::where('deleted', '0')->where('status', '1')->orderby('name', 'asc')->get();

        $data['pageTitle'] = "Shipment Tracking";
        $data['page'] = !empty($page) ? $page : '1';

        $data['id'] = 0;
        $data['action'] = 'Add';
        $data['shipmentTracking'] = array();
        return view('Administrator.shipmenttracking.add', $data);
    }

    /**
     * Method used to save tarcking information
     * @param integer $id
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $shipmentTracking = new Shipmenttracking;

        if (!empty($id)) {
            $validator = Validator::make($request->all(), [
                        'name' => 'required',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                        'fileName.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                        'updateTypeId' => 'required',
                        'deliveryCompanyId' => 'required',
                        'deliveredOn' => 'required',
                        'name' => 'required',
                        'trackingNumber' => 'required',
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $shipmentTracking = Shipmenttracking::find($id);
                $shipmentTracking->modifiedBy = Auth::user()->id;
                $shipmentTracking->modifiedOn = Config::get('constants.CURRENTDATE');
            } else {
                $shipmentTracking->createdBy = Auth::user()->id;
                $shipmentTracking->createdOn = Config::get('constants.CURRENTDATE');
            }
            $shipmentTracking->updateTypeId = $request->updateTypeId;
            $shipmentTracking->deliveryCompanyId = $request->deliveryCompanyId;
            $shipmentTracking->deliveredOn = $request->deliveredOn;
            $shipmentTracking->name = $request->name;
            $shipmentTracking->message = $request->message;

            $shipmentTracking->save();
            $shipmentTrackingId = $shipmentTracking->id;

            if (!empty($shipmentTrackingId)) {
                /* Shipment Tracking Files */
                if ($request->hasFile('fileName')) {
                    foreach ($request->file('fileName') as $image) {
                        $fileName = time() . '_' . $image->getClientOriginalName();
                        $image->move(public_path() . '/uploads/shipmenttracking', $fileName);

                        $shipmentTrackingFile = new Shipmenttrackingfile;
                        $shipmentTrackingFile->trackingId = $shipmentTrackingId;
                        $shipmentTrackingFile->fileName = $fileName;
                        $shipmentTrackingFile->createdBy = Auth::user()->id;
                        $shipmentTrackingFile->createdOn = Config::get('constants.CURRENTDATE');
                        $shipmentTrackingFile->save();
                    }
                }

                /* Shipment Tracking Notes */
                if (!empty($request->notes)) {
                    $trackingNotes = new Shipmenttrackingnotes;

                    $trackingNotes->trackingId = $shipmentTrackingId;
                    $trackingNotes->notes = $request->notes;
                    $trackingNotes->createdBy = Auth::user()->id;
                    $trackingNotes->createdOn = Config::get('constants.CURRENTDATE');
                    $trackingNotes->save();
                }

                /* Shipment Tracking Numbers */
                $trackingNumbers = preg_split('/\r\n|[\r\n]/', $request->trackingNumber);
                if (!empty($trackingNumbers)) {
                    foreach ($trackingNumbers as $tracking) {
                        $trackingNumber = new Shipmenttrackingnumber;
                        $trackingNumber->trackingId = $shipmentTrackingId;
                        $trackingNumber->trackingNumber = $tracking;
                        $trackingNumber->createdBy = Auth::user()->id;
                        $trackingNumber->createdOn = Config::get('constants.CURRENTDATE');
                        $trackingNumber->save();
                    }
                }
            }

            return redirect('/administrator/shipmenttracking?page=' . $page)->with('successMessage', 'Shipment tracking information saved successfuly.');
        }
    }

    /**
     * Method used to view tracking files
     * @param integer $id
     * @param integer $page
     * @return html
     */
    public function viewfiles($id, $page) {
        $data['pageTitle'] = "View Tracking Files";
        $shipmentTrackingFile = new Shipmenttrackingfile;
        $data['trackingFileData'] = Shipmenttrackingfile::where('trackingId', $id)->get();

        return view('Administrator.shipmenttracking.viewfiles', $data);
    }

    /**
     * Method used to view tracking numbers
     * @param integer $id
     * @param integer $page
     * @return html
     */
    public function viewtracking($id, $page) {
        $data['pageTitle'] = "View Tracking Numbers";
        $shipmentTrackingNum = new Shipmenttrackingnumber;
        $data['trackingNumData'] = Shipmenttrackingnumber::where('trackingId', $id)->get();
        $data['trackingNumShipments'] = Shipmenttrackingnumber::getShipmentRecords($data['trackingNumData']);
        $data['trackingLock'] = Shipmenttracking::find($id)->trackingLock;
        $data['id'] = $id;
        $data['page'] = $page;

        return view('Administrator.shipmenttracking.viewtracking', $data);
    }
    
    
    /**
     * This function is user to update tracking number of shipment and its lock status
     * @param type $shipmentTrackingId
     * @param type $page
     * @param Request $request
     * @return type
     */
    public function updatetracking($shipmentTrackingId, $page, Request $request) {

        Shipmenttrackingnumber::where('trackingId',$shipmentTrackingId)->delete();
        /* Shipment Tracking Numbers */
        $trackingNumbers = preg_split('/\r\n|[\r\n]/', $request->trackingNumber);
        if (!empty($trackingNumbers)) {
            foreach ($trackingNumbers as $tracking) {
                $trackingNumber = new Shipmenttrackingnumber;
                $trackingNumber->trackingId = $shipmentTrackingId;
                $trackingNumber->trackingNumber = $tracking;
                $trackingNumber->createdBy = Auth::user()->id;
                $trackingNumber->createdOn = Config::get('constants.CURRENTDATE');
                $trackingNumber->save();
            }
        }
        
        if($request->action == 'lock')
        {
            Shipmenttracking::where('id',$shipmentTrackingId)->update(['trackingLock'=>'1']);
        }
        
        return redirect('/administrator/shipmenttracking?page=' . $page)->with('successMessage', 'Shipment tracking information updated successfuly.');
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return html
     */
    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Shipmenttracking::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/shipmenttracking/?page=' . $page)->with('successMessage', 'Shipment tracking deleted successfully.');
            } else {
                return \Redirect::to('administrator/shipmenttracking/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/shipmenttracking/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to delete multiple records
     * @param object $request
     * @return html
     */
    public function deleteall(Request $request) {
        $createrModifierId = Auth::user()->id;
        $checkedval = $request->checkedval;

        if (!empty($checkedval)) {
            $idArray = explode('^', $checkedval);

            foreach ($idArray as $id) {
                Shipmenttracking::deleteRecord($id, $createrModifierId);
            }
            return \Redirect::to('administrator/shipmenttracking')->with('successMessage', 'Shipment tarcking information deleted successfully.');
        } else {
            return \Redirect::to('administrator/shipmenttracking/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to clear search history
     * @return html
     */
    public function showall() {
        \Session::forget('TRACKINGDATA');
        return \Redirect::to('administrator/shipmenttracking');
    }

    /**
     * Method used to generate and export selected shipment tracking records
     * @param integer $page
     * @param Request $request
     * @return string
     */
    public function exportselected($page, Request $request) {

        $shipmentTracking = new Shipmenttracking;

        $searchShipmentTrackingArr = array(
            'updateType' => '',
            'deliveryCompanyId' => '',
            'name' => '',
            'trackingNumber' => '',
        );

        $searchByCreatedOn = \Session::get('SHIPMENTDATA.searchByCreatedOn');
        $searchByDate = \Session::get('SHIPMENTDATA.searchByDate');
        $searchShipment = \Session::get('SHIPMENTDATA.searchShipmentTracking');

        $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
        $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
        $param['searchShipmentTracking'] = !empty($searchShipment) ? $searchShipment[0] : $searchShipmentTrackingArr;

        $exportData = $shipmentTracking->exportData($param, $request->selected, $request->selectedField);

        $excelName = "Shipment-Tracking-Data" . Carbon::now();
        ob_end_clean();
        ob_start();
        Excel::create($excelName, function($excel) use($exportData) {
            $excel->sheet('Sheet 1', function($sheet) use($exportData) {
                $sheet->fromArray($exportData);
            });
        })->store('xlsx', public_path('exports'));
        ob_flush();
        $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
        return response()->json(['path' => $creatingPath]);
    }

    /**
     * Method used to generate and export all shipment tracking records
     * @param integer $page
     * @param Request $request
     * @return string
     */
    public function exportall($page, Request $request) {

        $shipmentTracking = new Shipmenttracking;
        $param = array();

        $searchShipmentTrackingArr = array(
            'updateType' => '',
            'deliveryCompanyId' => '',
            'name' => '',
            'trackingNumber' => '',
        );

        $searchByCreatedOn = \Session::get('SHIPMENTDATA.searchByCreatedOn');
        $searchByDate = \Session::get('SHIPMENTDATA.searchByDate');
        $searchShipment = \Session::get('SHIPMENTDATA.searchShipmentTracking');

        $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
        $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
        $param['searchShipmentTracking'] = !empty($searchShipment) ? $searchShipment[0] : $searchShipmentTrackingArr;

        $exportData = $shipmentTracking->exportData($param, array(), $request->selectall);

        $excelName = "Shipment-Tracking-Data" . Carbon::now();
        ob_end_clean();
        ob_start();
        Excel::create($excelName, function($excel) use($exportData) {
            $excel->sheet('Sheet 1', function($sheet) use($exportData) {
                $sheet->fromArray($exportData);
            });
        })->export('xlsx');
        ob_flush();
    }
    
    public function printtracking($trackingId,$page = 0) {
        
        $data = array();
        
        $trackingInfo = Shipmenttracking::find($trackingId);
        $trackingNumbers = Shipmenttrackingnumber::where('trackingId', $trackingId)->get();
        $deliveryCompanyData = collect(\App\Model\Deliverycompany::where('status', '1')->get());
        $deliveryCompanyList = $deliveryCompanyData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        $updateTypeData = collect(\App\Model\Updatetype::where('status', '1')->get());
        $updateTypeList = $updateTypeData->mapWithKeys(function($item) {
            return [$item['id'] => $item['name']];
        });
        $userAdminEmail = \App\Model\UserAdmin::find($trackingInfo->createdBy)->email;    
        $data['trackingNumData'] = $trackingNumbers;
        $data['trackingData'] = $trackingInfo;
        $data['trackingCount'] = $trackingNumbers->count();
        $data['deliveryCompanyList'] = $deliveryCompanyList;
        $data['updateTypeList'] = $updateTypeList;
        $data['userAdminEmail'] = $userAdminEmail;
        
        return view('Administrator.shipmenttracking.printtracking', $data);
    }

}
