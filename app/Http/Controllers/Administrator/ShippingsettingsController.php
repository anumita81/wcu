<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Shippingsettings;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class ShippingsettingsController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index() {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Shippingsettings'), Auth::user()->id); // call the helper function
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $data['title'] = "Administrative Panel :: Shipping Settings Management :: Shipping General Settings";
        $data['pageTitle'] = "General Shipping Configuration";
        $data['contentTop'] = array('breadcrumbText' => 'Shipping Settings', 'contentTitle' => 'General Shipping Configuration', 'pageInfo' => '');



        $data['settings'] = Shippingsettings::getSettingsData()->toArray();

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.shippingsettings.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string

     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata(Request $request) {

        $data = array();

        $shippingsettings = new Shippingsettings;


        $request->except('_token');

        //print_r($request->post()); die;

        foreach ($request->post() as $key => $val) {


            //If post new value and db settings value are not same then db old value will updated with settings value 
            if ($key != "_token") {
                $validator = Validator::make($request->all(), [
                            $key => 'required'
                ]);


                $settingsId = Shippingsettings::where('settingsKey', $key)->get(['id']);
                $settingsId = $settingsId[0]['id'];

                $shippingsettings = Shippingsettings::find($settingsId);
                $shippingsettings->settingsValue = $val[1];
                $shippingsettings->oldValue = $val[0];
                $shippingsettings->save();
            }
        }

        return redirect('/administrator/shippingsettings')->with('successMessage', 'Settings saved successfuly.');
    }

    /**
     * Method used to unset search session data
     *
     */
    public function cleardata() {
        \Session::forget('SETTINGSDATA');
        return \Redirect::to('administrator/shippingsettings');
    }

    public function shippingmethods(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Shippingmethods'), Auth::user()->id); // call the helper function
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'orderby');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('SHIPPINGMETHODDATA');
            \Session::push('SHIPPINGMETHODDATA.searchDisplay', $searchDisplay);
            \Session::push('SHIPPINGMETHODDATA.field', $field);
            \Session::push('SHIPPINGMETHODDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('SHIPPINGMETHODDATA.field');
            $sortType = \Session::get('SHIPPINGMETHODDATA.type');
            $searchDisplay = \Session::get('SHIPPINGMETHODDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'orderby';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'shipping' => array('current' => 'sorting'),
            'orderby' => array('current' => 'sorting'),
            'shipping_time' => array('current' => 'sorting'),
            'destination' => array('current' => 'sorting')
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';


        $methodsData = Shippingsettings::getShippingMethodData($param);
        $methodIdUsedShipment = \App\Model\Shipmentdelivery::fetchUsedFieldIds('shippingMethodId');
        $methodIdUsedProcurement = \App\Model\Procurement::fetchUsedFieldIds('shippingMethodId');
        $shippingMethodIdUsed = array_unique(array_merge($methodIdUsedProcurement, $methodIdUsedShipment));

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Shipping Settings Management :: Shipping Methods";
        $data['pageTitle'] = "Shipping Methods";
        $data['contentTop'] = array('breadcrumbText' => 'Shipping Methods', 'contentTitle' => 'Shipping Methods', 'pageInfo' => 'This section allows you to change shipping methods');
        $data['page'] = $methodsData->currentPage();
        $data['methodsData'] = $methodsData;
        $data['shippingMethodIdUsed'] = $shippingMethodIdUsed;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.shippingsettings.shippingmethod', $data);
    }

    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Shippingsettings::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/shippingmethods/?page=' . $page)->with('successMessage', 'Shipping metbod status changed successfully.');
            } else {
                return \Redirect::to('administrator/shippingmethods/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/shippingmethods/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    public function methodDelete($id = '', $page = '') {

        $method = new Shippingsettings();

        $createrModifierId = Auth::user()->id;


        if (!empty($id)) {
            $method = $method->findData($id);
            $methodIdUsedShipment = \App\Model\Shipmentdelivery::fetchUsedFieldIds('shippingMethodId');
            $methodIdUsedProcurement = \App\Model\Procurement::fetchUsedFieldIds('shippingMethodId');
            $shippingMethodIdUsed = array_unique(array_merge($methodIdUsedProcurement, $methodIdUsedShipment));
            if (!in_array($id, $shippingMethodIdUsed)) {
                if (Shippingsettings::deleteRecord($id, $createrModifierId)) {
                    return \Redirect::to('administrator/shippingmethods/?page=' . $page)->with('successMessage', 'Information deleted successfuly.');
                } else {
                    return \Redirect::to('administrator/shippingmethods/?page=' . $page)->with('errorMessage', 'Error in operation!');
                }
            } else {
                return \Redirect::to('administrator/shippingmethods/?page=' . $page)->with('errorMessage', 'Shipping method cannot be deleted. Dependency exists.');
            }
        } else {
            return \Redirect::to('administrator/shippingmethods/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    public function addmethod($page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Shipping Settings Management :: Shipping Methods";
        $data['contentTop'] = array('breadcrumbText' => 'Shipping Methods', 'contentTitle' => 'Shipping Methods', 'pageInfo' => 'This section allows you to change shipping methods');
        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;
        $data['action'] = 'Add New';

        //print_r($data['method']); die;
        return view('Administrator.shippingsettings.addmethod', $data);
    }

    public function editmethod($id = '', $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Shipping Settings Management :: Shipping Methods";
        $data['contentTop'] = array('breadcrumbText' => 'Shipping Methods', 'contentTitle' => 'Shipping Methods', 'pageInfo' => 'This section allows you to change shipping methods');
        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = $id;
        $data['action'] = 'Edit';
        $method = Shippingsettings::findData($id);
        $deliveryTime = explode(' - ', str_replace(' Business Days', '', $method->shipping_time));
        $data['deliveryTimeFrom'] = $deliveryTime[0];
        $data['deliveryTimeTo'] = $deliveryTime[1];
        $data['method'] = $method;


        return view('Administrator.shippingsettings.editmethod', $data);
    }

    public function savemethod($id, $page, Request $request) {
        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $shippingsettings = new Shippingsettings;

        $validator = Validator::make($request->all(), [
                    'name' => 'required|unique:' . $shippingsettings->methodTable . ',shipping,NULL,' . $id . ',deleted,1',
                    'deliveryTimeFrom' => 'required',
                    'deliveryTimeTo' => 'required',
                    'destination' => 'required|not_in:0',
                    'weightFrom' => 'required|numeric',
                    'weightTo' => 'required|numeric',
                    'seaOnly' => 'required',
                    'duty' => 'required',
                    'cod' => 'required',
                    'companyLogo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if ($request->deliveryTimeFrom[0] > $request->deliveryTimeTo[0]) {
                return redirect('/administrator/shippingmethods')->with('errorMessage', 'From delivery time can not be greater than To delivery time.');
            } else {
                if ($request->hasFile('companyLogo')) {
                    $image = $request->file('companyLogo');
                    $name = $image->getClientOriginalName();
                    $destinationPath = public_path('/uploads/shipping');
                    $image->move($destinationPath, $name);
                } else {
                    $name = $request->companyoldLogo;
                }
                $shippingRecord = array();
                if (!empty($id)) {
                    $shippingsettings = Shippingsettings::findData($id);
                    $shippingRecord['updatedBy'] = Auth::user()->id;
                    $shippingRecord['updatedOn'] = Config::get('constants.CURRENTDATE');
                } else {
                    $shippingRecord['createdBy'] = Auth::user()->id;
                    $shippingRecord['createdOn'] = Config::get('constants.CURRENTDATE');
                }
                if($request->orderby)
                {
                    $shippingRecord['orderby'] = $request->orderby;
                }
                else
                {
                    $shippingRecord['orderby'] = \App\Model\Shippingmethods::max('orderby')+1;
                }
                
                $shippingRecord['shipping'] = $request->name;
                $shippingRecord['shipping_time'] = $request->deliveryTimeFrom[0] . ' - ' . $request->deliveryTimeTo[0] . ' Business Days';
                $shippingRecord['destination'] = $request->destination[0];
                $shippingRecord['weight_min'] = $request->weightFrom;
                $shippingRecord['weight_limit'] = $request->weightTo;
                $shippingRecord['is_cod'] = $request->cod[0];
                $shippingRecord['sea'] = $request->seaOnly[0];
                $shippingRecord['is_duty'] = $request->duty[0];
                $shippingRecord['companyLogo'] = $name;
                $shippingRecord['days'] = $request->deliveryTimeTo[0];

                if (empty($id)) {
                    $saveData = Shippingsettings::saveRecord($shippingRecord);
                } else {
                    $saveData = Shippingsettings::saveRecord($shippingRecord, $id);
                }
                return redirect('/administrator/shippingmethods')->with('successMessage', 'Shipping method saved successfuly.');
            }
        }
    }

    public function editcomment($id, $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Shipping Settings Management :: Shipping Methods";
        $data['contentTop'] = array('breadcrumbText' => 'Shipping Methods', 'contentTitle' => 'Shipping Methods', 'pageInfo' => 'This section allows you to change shipping methods');
        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = $id;
        $data['action'] = 'Edit';
        $method = Shippingsettings::findData($id);
        $data['method'] = $method;
        // print_r($data['method']); die;
        return view('Administrator.shippingsettings.editcomment', $data);
    }

    public function savecomment($id, $page, Request $request) {
        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;
        $shippingRecord = array();

        // print_r($request->post()); die;
        if (!empty($request)) {
            $shippingRecord['learn_more'] = $request->learnMore;
        }



        $saveData = Shippingsettings::saveRecord($shippingRecord, $id);

        return redirect('/administrator/shippingmethods')->with('successMessage', 'Shipping method saved successfuly.');
    }

}
