<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Driver;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class DriverController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Driver'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        
        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('DRIVERDATA');
            \Session::push('DRIVERDATA.searchDisplay', $searchDisplay);
            \Session::push('DRIVERDATA.field', $field);
            \Session::push('DRIVERDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('DRIVERDATA.field');
            $sortType = \Session::get('DRIVERDATA.type');
            $searchDisplay = \Session::get('DRIVERDATA.searchDisplay');


            $param['field'] = !empty($sortField) ? $sortField[0] : 'driverName';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'driverName' => array('current' => 'sorting'), 'driverPhone'  => array('current' => 'sorting'), 'companyName' => array('current' => 'sorting')
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTRY LIST  */
        $driverData = Driver::getDriverList($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Driver";
        $data['contentTop'] = array('breadcrumbText' => 'Driver', 'contentTitle' => 'Driver', 'pageInfo' => 'This section allows you to manage driver informations');
        $data['pageTitle'] = "Manage Drivers";
        $data['page'] = $driverData->currentPage();
        $data['driverData'] = $driverData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        
        return view('Administrator.driver.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Driver";
        $data['contentTop'] = array('breadcrumbText' => 'Driver', 'contentTitle' => 'Driver', 'pageInfo' => 'This section allows you to manage driver informations');
        $data['page'] = !empty($page) ? $page : '1';
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $driver = Driver::find($id);
            $data['driver'] = $driver;
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add';
        }
        return view('Administrator.driver.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $driver = new Driver;

        $validator = Validator::make($request->all(), [
                    'driverName' => 'required',
                    'contactNumber' => 'required',
                    'companyName' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $driver = Driver::find($id);
                $driver->updatedBy = Auth::user()->id;
                $driver->updatedOn = Config::get('constants.CURRENTDATE');
            }
            else{
                $driver->createdBy = Auth::user()->id;
                $driver->createdOn = Config::get('constants.CURRENTDATE');
            }
            $driver->driverName = $request->driverName;
            $driver->driverPhone = $request->contactNumber;
            $driver->companyName = $request->companyName;
            $driver->contactEmail = $request->contactEmail;
            $driver->save();
            $driverId = $driver->id;

            return redirect('administrator/driver')->with('successMessage', 'Driver saved successfuly.');
        }
    }

    public function deletedata($id = '',$page='') {

        $driver= new Driver();

        $createrModifierId = Auth::user()->id;
        if(!empty($id)){
        $driver = $driver->find($id);        

        if (Driver::deleteRecord($id, $createrModifierId)) {
               return \Redirect::to('administrator/driver/?page='.$page)->with('successMessage', 'Driver deleted successfuly.');
            } else {
                return \Redirect::to('administrator/driver/?page=' .$page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/driver/?page=' .$page)->with('errorMessage', 'Error in operation!');
        }
    }


}
