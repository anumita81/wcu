<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Menu;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class MenuController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {
        $data = array();

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Menu'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }


        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('CURRENCYDATA');
            \Session::push('CURRENCYDATA.searchDisplay', $searchDisplay);
            \Session::push('CURRENCYDATA.field', $field);
            \Session::push('CURRENCYDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('CURRENCYDATA.field');
            $sortType = \Session::get('CURRENCYDATA.type');
            $searchDisplay = \Session::get('CURRENCYDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'sort';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'label' => array('current' => 'sorting'),
            'position' => array('current' => 'sorting'),
            'sort' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH CURRENCY LIST  */
        $menuData = Menu::getMenuList($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Menu Management";
        $data['contentTop'] = array('breadcrumbText' => 'Menu Management', 'contentTitle' => 'Menu Management', 'pageInfo' => 'This section allows you to manage menu');
        $data['pageTitle'] = "Menu Management";
        $data['page'] = $menuData->currentPage();
        $data['menuData'] = $menuData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.menu.index', $data);
    }

    /**
     * Method for add edit menu
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['page'] = !empty($page) ? $page : '1';
        $data['cmsPageList'] = \App\Model\Sitepage::where('status', '1')->orderBy('displayOrder', 'ASC')->get();
        $data['blockList'] = \App\Model\Blockcontent::where('status', '1')->groupby('slug')->get();
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $data['pageTitle'] = "Edit Menu";
            $menu = Menu::find($id);
            $data['menu'] = $menu;
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add Menu";
            $data['menu'] = array();
        }
        return view('Administrator.menu.addedit', $data);
    }

    /*
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {
        $menu = new Menu;
        $validator = Validator::make($request->all(), [
                    'label' => 'required|regex:/^[a-z0-9 .\-]+$/i',
                    'linkType' => 'required',
                    'sort' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $menu = Menu::find($id);
                $menu->sort = $request->sort;
            }

            /* IF HEADER ITEMS ARE MORE THAN 8 (AS PER THE CURRENT FRONT END DESIGN) */
            if ($request->position == 0) {
                if (!empty($id)) {

                    $existingItem = Menu::find($id);
                    if ($existingItem->position != $request->position) {
                        $headerItems = Menu::where("deleted", '0')->where('position', '0')->count();
                        if ($headerItems >= 8) {
                            return redirect('/administrator/menu?page=' . $page)->with('errorMessage', 'You cannot add more than 8 header items');
                        }
                    }
                } else {
                    $headerItems = Menu::where("deleted", '0')->where('position', '0')->count();
                    if ($headerItems >= 8) {
                        return redirect('/administrator/menu?page=' . $page)->with('errorMessage', 'You cannot add more than 8 header items');
                    }
                }
            }


            $menu->position = $request->position;
            $menu->pannel = !empty($request->pannel) ? $request->pannel : '0';
            $menu->label = $request->label;
            $menu->linkType = $request->linkType;
            if ($menu->linkType == 1) {
                $menu->link = $request->link;
                $menu->cmsPageLink = '';
            } else if ($menu->linkType == 2) {
                $menu->link = '';
                $menu->cmsPageLink = $request->cmsPageLink;
            } else {
                $menu->link = '';
                $menu->cmsPageLink = $request->blockPageLink;
            }

            $menu->modifiedBy = Auth::user()->id;
            $menu->modifiedOn = Config::get('constants.CURRENTDATE');

            if (empty($request->sort)) {
                /* ASSIGN DEFAULT MENU ORDER */
                $orderId = Menu::where("deleted", '0')->orderby('sort', 'desc')->get();

                if (count($orderId) > 0) {
                    $menu->sort = $orderId[0]->sort + 1;
                } else {
                    $menu->sort = 1;
                }
            } else {
                $menu->sort = $request->sort;
            }

            $menu->save();
            $menuId = $menu->id;

            return redirect('/administrator/menu?page=' . $page)->with('successMessage', 'Menu information saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $id
     * @param integer $page
     *  @param integer $status
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Menu::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/menu/?page=' . $page)->with('successMessage', 'Menu status changed successfully.');
            } else {
                return \Redirect::to('administrator/menu/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/menu/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Menu::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/menu/?page=' . $page)->with('successMessage', 'Menu deleted successfully.');
            } else {
                return \Redirect::to('administrator/menu/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/menu/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
