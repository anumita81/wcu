<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Zone;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\County;
use App\Model\Zonecountry;
use App\Model\Zonecounty;
use App\Model\Zonestate;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class ZoneController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Zone'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('ZONEDATA');
            \Session::push('ZONEDATA.searchDisplay', $searchDisplay);
            \Session::push('ZONEDATA.field', $field);
            \Session::push('ZONEDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('ZONEDATA.field');
            $sortType = \Session::get('ZONEDATA.type');
            $searchDisplay = \Session::get('ZONEDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'name';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
            'countryCount' => array('current' => 'sorting'),
            'stateCount' => array('current' => 'sorting'),
            'countyCount' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTRY LIST  */
        $zoneData = Zone::getZoneList($param);
        $zoneCountries = Zonecountry::getData();
        $zoneStates = Zonestate::getData();
        $zoneCities = \App\Model\Zonecity::getData();
        //print_r($zoneCountries);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Zones";
        $data['contentTop'] = array('breadcrumbText' => 'Zones', 'contentTitle' => 'Zones', 'pageInfo' => 'This section allows you to manage destination zones');
        $data['pageTitle'] = "Zones";
        $data['page'] = $zoneData->currentPage();
        $data['zoneData'] = $zoneData;
        $data['zoneCountries'] = $zoneCountries;
        $data['zoneStates'] = $zoneStates;
        $data['zoneCities'] = $zoneCities;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.zone.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['contentTop'] = array('breadcrumbText' => 'Zones', 'contentTitle' => 'Zones', 'pageInfo' => 'This section allows you to manage destination zones');
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();
        $data['page'] = !empty($page) ? $page : '1';
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $data['pageTitle'] = "Edit Zone";
            $location = Zone::find($id);
            $data['location'] = $location;
            return view('Administrator.location.edit', $data);
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add Zone";
            return view('Administrator.location.add', $data);
        }
    }

    public function add($id = '0', $page = '') {

        $data = array();
        $data['action'] = 'Add';
        $data['countryList'] = Country::getCountryData('all');
        $data['stateList'] = State::getStateData('all');
        $data['countyList'] = County::getCountyData('all');
        $data['cityList'] = City::getAllCityList();
        $data['regionWiseCountry'] = json_encode(Country::getRegionWiseCountry(), JSON_FORCE_OBJECT);
        $data['page'] = $page;
        $data['title'] = "Administrative Panel :: Zones";
        $data['contentTop'] = array('breadcrumbText' => 'Zones', 'contentTitle' => 'Zones', 'pageInfo' => 'This section allows you to manage destination zones');
        $data['pageTitle'] = "Add Zone";

        return view('Administrator.zone.add', $data);
    }

    public function edit($id = '0', $page = '') {

        $zoneObj = new Zone;
        if ($id != '0')
            $zoneObj = $zoneObj->find($id);

        $data = array();
        $data['action'] = 'Edit';
        $data['countryList'] = Country::getCountryData('all');
        $data['stateList'] = State::getStateData('all');
        $data['countyList'] = County::getCountyData('all');
        $data['cityList'] = City::getAllCityList();
        $data['zoneWiseCountry'] = Zonecountry::zoneWiseData($id);
        $data['zoneWiseState'] = Zonestate::zoneWiseData($id);
        $data['zoneWiseCounty'] = Zonecounty::zoneWiseData($id);
        $data['zoneWiseCity'] = \App\Model\Zonecity::zoneWiseData($id);
        //dd($data['zoneWiseCity']);
        $data['regionWiseCountry'] = json_encode(Country::getRegionWiseCountry(), JSON_FORCE_OBJECT);
        $data['title'] = "Administrative Panel :: Zones";
        $data['contentTop'] = array('breadcrumbText' => 'Zones', 'contentTitle' => 'Zones', 'pageInfo' => 'This section allows you to manage destination zones');
        $data['zoneObj'] = $zoneObj;
        $data['pageTitle'] = "Edit Zone";
        $data['page'] = $page;

        return view('Administrator.zone.edit', $data);
    }

    public function update($id = '0', $page = '1', Request $request) {

        $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'country' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

            $zoneObj = new Zone;
            if ($id != '0')
                $zoneObj = $zoneObj->find($id);

            $zoneCountryObj = new Zonecountry;
            $zoneStateObj = new Zonestate;
            $zoneCountyObj = new ZoneCounty;


            $zoneObj->name = $request->input('name');
            $zoneObj->modifiedBy = Auth::user()->id;
            $zoneObj->cities = addslashes($request->input('cities'));
            $zoneObj->zipcodes = addslashes($request->input('zipcodes'));
            $zoneObj->addresses = addslashes($request->input('addresses'));
            $zoneObj->save();

            Zonecountry::where('zoneId', $id)->delete();
            foreach ($request->input('country') as $eachCountry) {
                $zoneCountryObj->zoneId = $id;
                $zoneCountryObj->countryId = $eachCountry;
                if ($zoneCountryObj->save())
                    $zoneCountryObj = new Zonecountry;
            }

            Zonestate::where('zoneId', $id)->delete();
            if(!empty($request->input('state')))
            {
                
                foreach ($request->input('state') as $eachstate) {
                    $zoneStateObj->zoneId = $id;
                    $zoneStateObj->stateId = $eachstate;
                    if ($zoneStateObj->save())
                        $zoneStateObj = new Zonestate;
                }
            }

            ZoneCounty::where('zoneId', $id)->delete();
            if(!empty($request->input('county')))
            {
                foreach ($request->input('county') as $eachcounty) {
                    $zoneCountyObj->zoneId = $id;
                    $zoneCountyObj->countyId = $eachcounty;
                    if ($zoneCountyObj->save())
                        $zoneCountyObj = new ZoneCounty;
                }
            }
            \App\Model\Zonecity::where('zoneId',$id)->delete();
            if(!empty($request->input('city')))
            {
                foreach ($request->input('city') as $eachcity)
                {
                    $zoneCityObj = new \App\Model\Zonecity();
                    $zoneCityObj->zoneId = $id;
                    $zoneCityObj->cityId = $eachcity;
                    $zoneCityObj->save();
                }
            }


            return redirect('/administrator/zone?page=' . $page)->with('successMessage', 'Zone information saved successfuly.');
        }
    }

    public function addeditzonerecord($id = -1,$page = 1, Request $request) {

        $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'country' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

            $zoneObj = new Zone;
            $zoneCountryObj = new Zonecountry;
            $zoneStateObj = new Zonestate;
            $zoneCountyObj = new ZoneCounty;


            $zoneObj->name = $request->input('name');
            $zoneObj->provider = 2;
            $zoneObj->modifiedBy = Auth::user()->id;
            $zoneObj->cities = addslashes($request->input('cities'));
            $zoneObj->zipcodes = addslashes($request->input('zipcodes'));
            $zoneObj->addresses = addslashes($request->input('addresses'));
            $zoneObj->save();

            $insertedZoneId = $zoneObj->id;
            foreach ($request->input('country') as $eachCountry) {
                $zoneCountryObj->zoneId = $insertedZoneId;
                $zoneCountryObj->countryId = $eachCountry;
                if ($zoneCountryObj->save())
                    $zoneCountryObj = new Zonecountry;
            }
            if(!empty($request->input('state')))
            {
                foreach ($request->input('state') as $eachstate) {
                    $zoneStateObj->zoneId = $insertedZoneId;
                    $zoneStateObj->stateId = $eachstate;
                    if ($zoneStateObj->save())
                        $zoneStateObj = new Zonestate;
                }
            }
            if(!empty($request->input('county')))
            {
                foreach ($request->input('county') as $eachcounty) {
                    $zoneCountyObj->zoneId = $insertedZoneId;
                    $zoneCountyObj->countyId = $eachcounty;
                    if ($zoneCountyObj->save())
                        $zoneCountyObj = new ZoneCounty;
                }
            }
            if(!empty($request->input('city')))
            {
                foreach ($request->input('city') as $eachcity)
                {
                    $zoneCityObj = new \App\Model\Zonecity();
                    $zoneCityObj->zoneId = $insertedZoneId;
                    $zoneCityObj->cityId = $eachcity;
                    $zoneCityObj->save();
                }
            }

            return redirect('/administrator/zone?page=' . $page)->with('successMessage', 'Zone information saved successfuly.');
        }
    }

    /**
     * Method to fetch state list
     * @return array
     */
    public function getstatelist($countryid = '') {
        $country = Country::where('id', $countryid)->orderby('name', 'asc')->get(['code']);
        $countryCode = $country[0]['code'];
        $stateList = array();
        if (isset($countryCode) && !empty($countryCode)) {
            $stateList = State::where('status', '1')
                    ->where('deleted', '0')
                    ->where('countryCode', $countryCode)
                    ->orderby('name', 'asc')
                    ->get();
        }
        echo json_encode($stateList);
        exit;
    }

    /**
     * Method to fetch city list
     * @return array
     */
    public function getcitylist($stateid = '') {
        $state = State::where('id', $stateid)->get(['code']);
        $stateCode = $state[0]['code'];

        $cityList = array();
        if (isset($stateCode) && !empty($stateCode)) {
            $cityList = City::where('status', '1')
                    ->where('deleted', '0')
                    ->where('stateCode', $stateCode)
                    ->orderby('name', 'asc')
                    ->get();
        }
        echo json_encode($cityList);
        exit;
    }

    /**
     * Method to fetch city list by country
     * @return array
     */
    public function getcitylistbycountry($countryid = '') {
        $country = Country::where('id', $countryid)->get(['code']);
        $countryCode = $country[0]['code'];
        $cityList = array();
        if (isset($countryCode) && !empty($countryCode)) {
            $cityList = City::where('status', '1')
                    ->where('deleted', '0')
                    ->where('countryCode', $countryCode)
                    ->orderby('name', 'asc')
                    ->get();
        }
        echo json_encode($cityList);
        exit;
    }

    /**
     * Method used to change status
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Zone::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/zone/?page=' . $page)->with('successMessage', 'Zone status changed successfully.');
            } else {
                return \Redirect::to('administrator/zone/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/zone/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
