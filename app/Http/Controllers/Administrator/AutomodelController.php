<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Automake;
use App\Model\Automodel;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;

class AutomodelController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoModel'), Auth::user()->id); // call the helper function
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            $searchByMake = \Input::get('searchByMake', '');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('AUTOMODELDATA');
            \Session::push('AUTOMODELDATA.searchByMake', $searchByMake);
            \Session::push('AUTOMODELDATA.searchDisplay', $searchDisplay);
            \Session::push('AUTOMODELDATA.field', $field);
            \Session::push('AUTOMODELDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByMake'] = $searchByMake;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('AUTOMODELDATA.field');
            $sortType = \Session::get('AUTOMODELDATA.type');
            $searchByMake = \Session::get('AUTOMODELDATA.searchByMake');
            $searchDisplay = \Session::get('AUTOMODELDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByMake'] = !empty($searchByMake) ? $searchByMake[0] : '';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'makeType' => array('current' => 'sorting'),
            'makeId' => array('current' => 'sorting'),
            'name' => array('current' => 'sorting'),
            'insurance' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH model LIST  */
        $modelData = Automodel::getList($param);
        if (count($modelData) > 0) {
            $data['page'] = $modelData->currentPage();
        }
        $modelIdUsedAuto = \App\Model\Autoshipment::fetchUsedFieldIds('modelId');
        $modelIdUsedProcurement = \App\Model\Procurementitem::fetchUsedFieldIds('modelId');
        $modelIdUsed = array_unique(array_merge($modelIdUsedAuto,$modelIdUsedProcurement));

        $data['makeData'] = Automake::where('deleted', '0')->orderBy('name', 'ASC')->get();
        $data['modelData'] = $modelData;
        $data['modelIdUsed'] = $modelIdUsed;
        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Model";
        $data['contentTop'] = array('breadcrumbText' => 'Model', 'contentTitle' => 'Model', 'pageInfo' => 'This section allows you to manage the model details');
        $data['pageTitle'] = "Model";
        $data['page'] = $data['modelData']->currentPage();
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavAuto', 'menuSub' => '', 'menuSubSub' => 'leftNavModelSettings9');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        
        

        return view('Administrator.automodel.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoModel'), Auth::user()->id); // call the helper function
        $data = array();
        $data['title'] = "Administrative Panel :: Model";
        $data['contentTop'] = array('breadcrumbText' => 'Model', 'contentTitle' => 'Model', 'pageInfo' => 'This section allows you to manage the model details');
        $data['page'] = !empty($page) ? $page : '1';

        if (!empty($id)) {
            if ($findRole['canEdit'] == 0) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['makeData'] = Automake::where('deleted', '0')->orderBy('name', 'ASC')->get();
            $data['pageTitle'] = "Edit Model";
            $data['id'] = $id;
            $data['page'] = $page;
            $data['action'] = 'Edit';
            $data['modelData'] = Automodel::find($id);
        } else {
            if ($findRole['canAdd'] == 0) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['makeData'] = Automake::where('deleted', '0')->orderBy('name', 'ASC')->get();
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add New Model";
            $data['page'] = $page;
        }
        return view('Administrator.automodel.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoModel'), Auth::user()->id); // call the helper function
        $data['page'] = !empty($page) ? $page : '1';
        $model = new Automodel;

        if ($id != 0) { // Edit
            if ($findRole['canEdit'] == 0) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $model = Automodel::find($id);
            $model->modifiedBy = Auth::user()->id;
            $model->modifiedOn = Config::get('constants.CURRENTDATE');
        } else { // Add
            if ($findRole['canAdd'] == 0) {
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $checkDuplicate = Automodel::where('name', $request->name)->where('makeId', $request->makeId)->where('deleted', '0')->get();
            if (count($checkDuplicate) > 0) {
                return redirect('/administrator/automodel/?page=' . $page)->with('errorMessage', 'Model already exists');
            }
        }
        $model->name = $request->name;
        $model->makeType = $request->type;
        $model->makeId = $request->makeId;
        $model->insurance = $request->insurance;

        $model->save();


        return redirect('/administrator/automodel?page=' . $page)->with('successMessage', 'Model information saved successfuly.');
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoModel'), Auth::user()->id); // call the helper function
        if ($findRole['canDelete'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            $modelIdUsedAuto = \App\Model\Autoshipment::fetchUsedFieldIds('modelId');
            $modelIdUsedProcurement = \App\Model\Procurementitem::fetchUsedFieldIds('modelId');
            $modelIdUsed = array_unique(array_merge($modelIdUsedAuto, $modelIdUsedProcurement));
            if (!in_array($id, $modelIdUsed)) {
                if (Automodel::deleteRecord($id, $createrModifierId)) {
                    return \Redirect::to('administrator/automodel/?page=' . $page)->with('successMessage', 'Model deleted successfully.');
                } else {
                    return \Redirect::to('administrator/automodel/?page=' . $page)->with('errorMessage', 'Error in operation!');
                }
            } else {
                return \Redirect::to('administrator/automodel/?page=' . $page)->with('errorMessage', 'Auto make cannot be deleted. Dependency exists.');
            }
        } else {
            return \Redirect::to('administrator/automodel/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
