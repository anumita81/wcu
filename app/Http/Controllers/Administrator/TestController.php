<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Mail\DemoEmail;
use Illuminate\Support\Facades\Mail;
use Mail;

class TestController extends Controller {

    public function index() {
        $objDemo = new \stdClass();
        $objDemo->demo_one = 'Demo One Value';
        $objDemo->demo_two = 'Demo Two Value';
        $objDemo->sender = 'SenderUserName';
        $objDemo->receiver = 'ReceiverUserName';

        $emailTemplate = Emailtemplate::find('templateKey', 'FORGOT_PASSWORD')->get();

        $content = str_replace(array('[NAME]', '[SHIPMENTTITLE]'), array($objDemo->demo_one, $objDemo->demo_two), $emailTemplate->template_body);

        Mail::send(['text' => 'mail'], $content, function($message) {
            $message->to($to, 'Tutorials Point')->subject
                    ($emailTemplate->template_subject);
            $message->from($emailTemplate->fromEmail, $emailTemplate->fromName);
        });

    }
    
    public function migrate() {
        
        $customers = \App\Model\Customers::where('usertype','c')->orderBy('id','asc')->get();
        foreach($customers as $eachCustomers)
        {
            $encpassword = Hash::make($eachCustomers->decryptpassword);
            \App\Model\Customers::where('id',$eachCustomers->id)->update(['encpassword'=>$encpassword]);
            echo $eachCustomers->id.'<br>';
        }
        //echo $customers->count();
        
        $allUsers = \App\Model\User::all();
        foreach($allUsers as $eachUsers) {
            $userAddressbook = \App\Model\Addressbook::where('userId',$eachUsers->id)->where('isDefaultShipping','1')->first();
            if(!empty($userAddressbook)) {
                $countryCode = \App\Model\Country::find($userAddressbook->countryId)->code;
                $unitNumber = $countryCode.$eachUsers->id;
                \App\Model\User::where("id",$eachUsers->id)->update(['unit'=>$unitNumber]);
                echo $eachUsers->id.'<br/>';
            }
        }
    }

}
