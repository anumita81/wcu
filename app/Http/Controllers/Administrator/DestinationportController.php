<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Destinationport;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class DestinationportController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Destinationport'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        
        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('DESTINATIONPORTDATA');
            \Session::push('DESTINATIONPORTDATA.searchDisplay', $searchDisplay);
            \Session::push('DESTINATIONPORTDATA.field', $field);
            \Session::push('DESTINATIONPORTDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('DESTINATIONPORTDATA.field');
            $sortType = \Session::get('DESTINATIONPORTDATA.type');
            $searchDisplay = \Session::get('DESTINATIONPORTDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'port';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'port' => array('current' => 'sorting')
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTRY LIST  */
        $destinationportData = Destinationport::getUpdatetypeList($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Destinationport";
        $data['contentTop'] = array('breadcrumbText' => 'Destinationport', 'contentTitle' => 'Destination Ports', 'pageInfo' => 'This section allows you to add/edit destination port.');
        $data['pageTitle'] = "Destination Ports";
        $data['page'] = $destinationportData->currentPage();
        $data['destinationportData'] = $destinationportData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.destinationport.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Destinationport";
        $data['contentTop'] = array('breadcrumbText' => 'Destinationport', 'contentTitle' => 'Destination Ports', 'pageInfo' => 'This section allows you to add/edit destination port.');
        $data['page'] = !empty($page) ? $page : '1';
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $destinationport = Destinationport::find($id);
            $data['destinationport'] = $destinationport;
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add';
        }
        return view('Administrator.destinationport.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $destinationport = new Destinationport;

        $validator = Validator::make($request->all(), [
                    'name' => 'required|unique:' . $destinationport->table . ',port,' . $id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $destinationport = Destinationport::find($id);
                $destinationport->updatedBy = Auth::user()->id;
                $destinationport->updatedOn = Config::get('constants.CURRENTDATE');
            } else {
                $destinationport->createdBy = Auth::user()->id;
                $destinationport->createdOn = Config::get('constants.CURRENTDATE');
            }
            $destinationport->port = $request->name;
            $destinationport->save();
            $destinationport = $destinationport->id;

            return redirect('/administrator/destinationport?page=' . $page)->with('successMessage', 'Destination port saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Destinationport::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/destinationport/?page=' . $page)->with('successMessage', 'Destination port status changed successfully.');
            } else {
                return \Redirect::to('administrator/destinationport/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/destinationport/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    public function deletedata($id = '', $page = '') {

        $destinationport = new Destinationport;

        $createrModifierId = Auth::user()->id;


        if (!empty($id)) {
            $destinationport = $destinationport->find($id);

            if (Destinationport::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/destinationport/?page=' . $page)->with('successMessage', 'Destination port deleted successfuly.');
            } else {
                return \Redirect::to('administrator/destinationport/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/destinationport/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
