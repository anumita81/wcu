<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\libraries\dbHelpers;
use Auth;
use App\Model\Contactreason;
use Hash;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Routing\Route;
use Config;
use customhelper;
use Carbon\Carbon;

class ContactreasonController extends Controller {
    public $_perPage;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Request $request) {
        $data = array();
        
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.ContactReason'), Auth::user()->id); 
        
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            \Session::forget('CREASON');
            \Session::push('CREASON.searchDisplay', $searchDisplay);
            \Session::push('CREASON.field', $field);
            \Session::push('CREASON.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('CREASON.field');
            $sortType = \Session::get('CREASON.type');
            $searchDisplay = \Session::get('CREASON.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
            'keyname' => array('current' => 'sorting'),
            'status' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* GET THE REASON RECORD  */
        $reasonData = Contactreason::orderBy($param['field'], $param['type'])->paginate($param['searchDisplay']);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Contact Us Reasons :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Contact Us Reasons";
        $data['contentTop'] = array('breadcrumbText' => 'Contact Us Reasons', 'contentTitle' => 'Contact Us Reasons', 'pageInfo' => 'This section allows you to manage contact us reasons');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavMessageBoard', 'menuSub' => '', 'menuSubSub' => 'leftNavContactUsReasons11');
        $data['reasonData'] = $reasonData;
        $data['page'] = $reasonData->currentPage();
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        return view('Administrator.contactreason.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $id
     * @param string $page
     * @return array
     */
    public function addedit($id = '0', $page = '') {
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.ContactReason'), Auth::user()->id);
        $data = array();
        $data['title'] = "Administrative Panel :: Contact Us Reasons";
        $data['contentTop'] = array('breadcrumbText' => 'Contact Us Reasons', 'contentTitle' => 'Contact Us Reasons', 'pageInfo' => 'This section allows you to manage contact us reasons');
        $data['page'] = !empty($page) ? $page : '1';

        if (!empty($id)) {
            if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            /* SET DATA FOR VIEW  */
            $data['pageTitle'] = "Edit Contact Us Reasons";
            $data['reasonId'] = $id;
            $data['page'] = $page;
            $data['action'] = 'Edit';
            $data['reasonData'] = Contactreason::find($id);
        } else {
            if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            /* SET DATA FOR VIEW  */
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add Contact Us Reasons";
            $data['page'] = $page;
        }
        return view('Administrator.contactreason.addedit', $data);
    }


    /**
     * Method used to save information
     * @param integer $id
     * @param string $page
     * @param Request $request
     * @return array
     */
    public function savedata($id, $page, Request $request) {

        $data = array();
        
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.ContactReason'), Auth::user()->id);
        $data['page'] = !empty($page) ? $page : '1';
        $reason = new Contactreason;

        /*  EDIT */
        if ($id != 0) {
            if($findRole['canEdit'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }

            $checkValidation = Contactreason::where("id", "!=", $id)->where("name", $request->name)->first();
            if(!empty($checkValidation))
            {
                return \Redirect::to('administrator/contactusreasons?page=' . $page)->with('errorMessage', 'Record already exists');
            }

            /* SAVING RECORD  */
            $reason = Contactreason::find($id);
            $reason->name = $request->name;
            $reason->createdBy = Auth::user()->id;
            $reason->createdOn = Config::get('constants.CURRENTDATE');
            $reason->save();
                

            
        } else {
            /*  ADD */
            if($findRole['canAdd'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            /* SAVING RECORD  */

            $checkValidation = Contactreason::where("name", $request->name)->first();

            if(!empty($checkValidation))
            {
                return \Redirect::to('administrator/contactusreasons?page=' . $page)->with('errorMessage', 'Record already exists');
            }

            /* EXTRACT ALL SPECIAL CHARS AND SPACE FOR KEYNAME */
            $keyname = preg_replace('/[^A-Za-z0-9\- ]/', '', $request->name);
            $keyname = strtolower(preg_replace('/\s+/', '_', $keyname));
            $reason->name = $request->name;
            $reason->keyname = $keyname;
            $reason->createdBy = Auth::user()->id;
            $reason->createdOn = Config::get('constants.CURRENTDATE');
            $reason->save();
        }
        

        return redirect('/administrator/contactusreasons?page=' . $page)->with('successMessage', 'Record saved successfuly.');
    }

    
    /**
     * Method used to change status
     * @param integer $id
     * @param string $page
     * @param string $status
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $getData = Contactreason::find($id);
        $status = $getData->status;
        if($status == '1'){
            $newStatus = '0';
        } else {
            $newStatus = '1';
        }

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Contactreason::changeStatus($id, $createrModifierId, $newStatus)) {
                return \Redirect::to('administrator/contactusreasons/?page=' . $page)->with('successMessage', 'Status changed successfully.');
            } else {
                return \Redirect::to('administrator/contactusreasons/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/contactusreasons/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
