<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Studentclass;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use Excel;
use Illuminate\Support\Facades\DB;
use customhelper;

class ClassController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Class'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchData = \Input::get('searchData', '');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('CLASSDATA.searchData', $searchData);
            \Session::push('CLASSDATA.searchDisplay', $searchDisplay);
            \Session::push('CLASSDATA.field', $field);
            \Session::push('CLASSDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchData'] = $searchData;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('CLASSDATA.field');
            $sortType = \Session::get('CLASSDATA.type');
            $searchData = \Session::get('CLASSDATA.searchData');
            $searchDisplay = \Session::get('CLASSDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'code';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchData'] = !empty($searchData) ? $searchData[0] : '';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'className' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';
        //print_r($param); die;

        /* FETCH Subject LIST  */
        $classData = Studentclass::getClassList($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Class";
        $data['contentTop'] = array('breadcrumbText' => 'Class', 'contentTitle' => 'Class', 'pageInfo' => 'This section allows you to manage classes');
        $data['pageTitle'] = "Class";
        $data['page'] = $classData->currentPage();
        $data['classData'] = $classData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.studentclass.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Class";
        $data['contentTop'] = array('breadcrumbText' => 'Class', 'contentTitle' => 'Class', 'pageInfo' => '');
        $data['page'] = !empty($page) ? $page : '1';
     
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $class = Studentclass::find($id);
            $data['classData'] = $class;
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add';
        }
        return view('Administrator.studentclass.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $studentclass = new Studentclass;

        $validator = Validator::make($request->all(), [
                    'className' => 'required',
                    'classFees' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $studentclass = Studentclass::find($id);
                $studentclass->modifiedBy = Auth::user()->id;
                $studentclass->modifiedOn = Config::get('constants.CURRENTDATE');
            }else{                
                $studentclass->createdBy = Auth::user()->id;
                $studentclass->createdOn = Config::get('constants.CURRENTDATE');
            }
            $studentclass->className = $request->className;
            $studentclass->classFees = $request->classFees;
            $studentclass->status = '1';
            $studentclass->save();
            $classId = $studentclass->id;

            return redirect('/administrator/studentclass?page='.$page)->with('successMessage', 'Class information saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Studentclass::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/studentclass/?page=' . $page)->with('successMessage', 'Class status changed successfully.');
            } else {
                return \Redirect::to('administrator/studentclass/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/studentclass/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    

 /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Studentclass::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/studentclass/?page=' . $page)->with('successMessage', 'Class deleted successfully.');
            } else {
                return \Redirect::to('administrator/studentclass/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/studentclass/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    

}
