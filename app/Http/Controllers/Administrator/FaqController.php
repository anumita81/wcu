<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Faq;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;

class FaqController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {

        $data = array();

        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Faq'), Auth::user()->id);
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('FAQ');
            \Session::push('FAQ.searchDisplay', $searchDisplay);
            \Session::push('FAQ.field', $field);
            \Session::push('FAQ.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('FAQ.field');
            $sortType = \Session::get('FAQ.type');
            $searchDisplay = \Session::get('FAQ.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'question' => array('current' => 'sorting'),
            'answer' => array('current' => 'sorting'),
            'status' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH make LIST  */
        $faqData = Faq::where('deleted', '0')->orderBy($param['field'], $param['type'])->paginate($param['searchDisplay']);
        if (count($faqData) > 0) {
            $data['page'] = $faqData->currentPage();
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: FAQ";
        $data['contentTop'] = array('breadcrumbText' => 'FAQ', 'contentTitle' => 'FAQ', 'pageInfo' => 'This section allows you to manage FAQs');
        $data['pageTitle'] = "FAQ";
        $data['faqData'] = $faqData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavContent', 'menuSub' => 'leftNavSiteContent2', 'menuSubSub' => 'leftNavFAQ16');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.faq.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $id
     * @param string $page
     * @return array
     */
    public function addedit($id = '0', $page = '') {
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Faq'), Auth::user()->id); 
        $data = array();
        $data['title'] = "Administrative Panel :: FAQ";
        $data['contentTop'] = array('breadcrumbText' => 'FAQ', 'contentTitle' => 'FAQ', 'pageInfo' => 'This section allows you to manage FAQs');
        $data['page'] = !empty($page) ? $page : '1';

        if (!empty($id)) {
            if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['pageTitle'] = "Edit FAQ";
            $data['faqId'] = $id;
            $data['page'] = $page;
            $data['action'] = 'Edit';
            $data['faqData'] = Faq::find($id);
        } else {
            if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add New FAQ";
            $data['page'] = $page;
        }
        return view('Administrator.faq.add', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $id
     * @param string $page
     * @param Request $request
     * @return array
     */
    public function savedata($id, $page, Request $request) {

        $data = array();
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Fa'), Auth::user()->id);
        $data['page'] = !empty($page) ? $page : '1';
        $faq = new Faq;

        /* VALIDATATION */
        $validator = Validator::make(\Input::all(), [
            'question' => 'required',
            'answer' => 'required',
        ]);

        /* IF VALIDATATIONS FAILS */
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

            /*  EDIT  */
            if ($id != 0) { 
                if($findRole['canEdit'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
                $faq = Faq::find($id);
                $faq->modifiedBy = Auth::user()->id;
                $faq->modifiedOn = Config::get('constants.CURRENTDATE');
                $faq->displayOrder = $request->displayOrder;
            } 
            /*  ADD  */
            else {
                if($findRole['canAdd'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
                
                $getLastOrders = Faq::orderBy('id', 'desc')->first();
                $newOrder = $getLastOrders->displayOrder+1;
                $faq->displayOrder = $newOrder;
                $faq->createdBy = Auth::user()->id;
                $faq->createdOn = Config::get('constants.CURRENTDATE');
                
            }
            $faq->question = $request->question;
            $faq->answer = $request->answer;

            /* SAVING THE DATA */
            $faq->save();
        }


        return redirect('/administrator/faq?page=' . $page)->with('successMessage', 'FAQ information saved successfuly.');
    }


    /**
     * Method used to change status
     * @param integer $id
     * @param string $page
     * @param string $status
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Faq::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/faq/?page=' . $page)->with('successMessage', 'Faq status changed successfully.');
            } else {
                return \Redirect::to('administrator/faq/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/faq/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }



    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Faq'), Auth::user()->id); 
        if($findRole['canDelete'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Faq::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/faq?page=' . $page)->with('successMessage', 'Faq deleted successfully.');
            } else {
                return \Redirect::to('administrator/faq?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/faq?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    
}
