<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Subject;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use Excel;
use Illuminate\Support\Facades\DB;
use customhelper;

class SubjectController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Subject'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchData = \Input::get('searchData', '');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('SUBJECTDATA.searchData', $searchData);
            \Session::push('SUBJECTDATA.searchDisplay', $searchDisplay);
            \Session::push('SUBJECTDATA.field', $field);
            \Session::push('SUBJECTDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchData'] = $searchData;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('SUBJECTDATA.field');
            $sortType = \Session::get('SUBJECTDATA.type');
            $searchData = \Session::get('SUBJECTDATA.searchData');
            $searchDisplay = \Session::get('SUBJECTDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'code';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchData'] = !empty($searchData) ? $searchData[0] : '';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'subjectName' => array('current' => 'sorting'),
            'subjectCode' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';
        //print_r($param); die;

        /* FETCH Subject LIST  */
        $subjectData = Subject::getSubjectList($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Subjects";
        $data['contentTop'] = array('breadcrumbText' => 'Subjects', 'contentTitle' => 'Subjects', 'pageInfo' => 'This section allows you to manage subjects');
        $data['pageTitle'] = "Subjects";
        $data['page'] = $subjectData->currentPage();
        $data['subjectData'] = $subjectData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.subject.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Subjects";
        $data['contentTop'] = array('breadcrumbText' => 'Subjects', 'contentTitle' => 'Subjects', 'pageInfo' => '');
        $data['page'] = !empty($page) ? $page : '1';
        $data['subjectgroup'] = \App\Model\Subjectgroup::where('deleted', '0')->where('status', '1')->where('parentgroupId', '0')->get();
        $data['subjectsubgroup'] = \App\Model\Subjectgroup::where('deleted', '0')->where('status', '1')->whereRaw("parentgroupId > 0")->get();
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $subject = Subject::find($id);
            $data['subjectData'] = $subject;
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add';
        }
        return view('Administrator.subject.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $subject = new Subject;

        $validator = Validator::make($request->all(), [
                    'subjectCode' => 'required|unique:' . $subject->table . ',subjectCode,' . $id,
                    'subjectName' => 'required',
                    'noofPapers' => 'required',
                    'fullMarks' => 'required',
                    'passMarks' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $subject = Subject::find($id);
                $subject->modifiedBy = Auth::user()->id;
                $subject->modifiedOn = Config::get('constants.CURRENTDATE');
            }else{                
                $subject->createdBy = Auth::user()->id;
                $subject->createdOn = Config::get('constants.CURRENTDATE');
            }
            $subject->subjectCode = $request->subjectCode;
            $subject->subjectName = $request->subjectName;
            $subject->subjectGroupId = $request->subjectGroupId;
            $subject->sujectSubgroupId = $request->sujectSubgroupId;
            $subject->subjectforClass = $request->subjectforClass;
            $subject->noofPapers = $request->noofPapers;
            $subject->fullMarks = $request->fullMarks;
            $subject->writtenTotal = $request->writtenTotal;
            $subject->othersTotal = $request->othersTotal;
            $subject->passMarks = $request->passMarks;
            $subject->status = '1';
            $subject->save();
            $subjectId = $subject->id;

            return redirect('/administrator/subject?page='.$page)->with('successMessage', 'Subject information saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Subject::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/subject/?page=' . $page)->with('successMessage', 'Subject status changed successfully.');
            } else {
                return \Redirect::to('administrator/subject/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/subject/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    
    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Subject::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/subject/?page=' . $page)->with('successMessage', 'Subject deleted successfully.');
            } else {
                return \Redirect::to('administrator/subject/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/subject/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    

}
