<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Procurement;
use App\Model\Procurementitem;
use App\Model\Procurementitemstatus;
use App\Model\Sitecategory;
use App\Model\Storecountrymapping;
use App\Model\Stores;
use App\Model\Siteproduct;
use App\Model\Deliverycompany;
use App\Model\Dispatchcompany;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\Shipment;
use App\Model\Shipmentdelivery;
use App\Model\Shipmentpackage;
use App\Model\Shippingmethods;
use App\Model\Viewshipment;
use App\Model\ViewShipmentDetails;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Hash;
use Log;
use Config;
use Excel;
use Mail;
use customhelper;
use PDF;
use Carbon\Carbon;

class ReportController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function procurementindex(Route $route, Request $request) {
        $data = array();

        /* BUILD DEFAULT SEARCH ARRAY */
        $searchProcurementArr = array(
            'idFrom' => '',
            'idTo' => '',
            'totalCostFrom' => '',
            'totalCostTo' => '',
            'toCountry' => '',
            'toState' => '',
            'toCity' => '',
            'deliveryCompanyId' => '',
            'status' => '',
            'paymentStatus' => '',
            'user' => '',
            'procurementType' => ''
        );

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchProcurement = \Input::get('searchProcurement', $searchProcurementArr);
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $searchWarehouse = \Input::get('searchWarehouse', '');

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('PROCUREMENTREPORT');
            \Session::push('PROCUREMENTREPORT.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('PROCUREMENTREPORT.searchDisplay', $searchDisplay);
            \Session::push('PROCUREMENTREPORT.searchByDate', $searchByDate);
            \Session::push('PROCUREMENTREPORT.searchWarehouse', $searchWarehouse);
            \Session::push('PROCUREMENTREPORT.searchProcurement', $searchProcurement);
            \Session::push('PROCUREMENTREPORT.field', $field);
            \Session::push('PROCUREMENTREPORT.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchWarehouse'] = $searchWarehouse;
            $param['searchProcurement'] = $searchProcurement;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('PROCUREMENTREPORT.field');
            $sortType = \Session::get('PROCUREMENTREPORT.type');
            $searchByCreatedOn = \Session::get('PROCUREMENTREPORT.searchByCreatedOn');
            $searchByDate = \Session::get('PROCUREMENTREPORT.searchByDate');
            $searchWarehouse = \Session::get('PROCUREMENTREPORT.searchWarehouse');

            $searchProcurement = \Session::get('PROCUREMENTREPORT.searchProcurement');
            $searchDisplay = \Session::get('PROCUREMENTREPORT.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchWarehouse'] = !empty($searchWarehouse) ? $searchWarehouse[0] : '';
            $param['searchProcurement'] = !empty($searchProcurement) ? $searchProcurement[0] : $searchProcurementArr;
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'id' => array('current' => 'sorting'),
            'firstName' => array('current' => 'sorting'),
            //'totalCost' => array('current' => 'sorting'),
            'status' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
            'warehouseId' => array('current' => 'sorting'),
            'paymentStatus' => array('current' => 'sorting'),
            'procurementType' => array('current' => 'sorting'),
            'totalProcurementCost' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
        );


        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* GET THE COUNT FOR STATUES */
        $arrayStatus = array('draft','requestforcost','submitted','itemreceived','completed','rejected');
        foreach($arrayStatus as $value){
            $data[$value] = Procurement::where('status', $value)->count();
        }

        /* GET THE TYPE */
        $data['arrayProcurementTypes'] = array('buycarforme'=>'Buy Car For Me', 'shopforme' => 'Shop For Me', 'autopart' => 'Auto Part', 'othervehicle' => 'Other Vehicle');

        /* GET THE WAREHOUSES */
        $data['warehouses'] = \App\Model\Warehouse::warehouseCountryMap();

        /* FETCH PPROCUREMENT LIST  */
        $procurementData = Procurement::getShopForMeList($param, '', $procurementType = 'ALL');


        /* FETCH DELIVERY COMPANY LIST  */
        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();
        /* FETCH DISPATCH COMPANY LIST  */
        $data['dispatchCompanyList'] = Dispatchcompany::where('status', '1')->orderby('name', 'asc')->get();
        /* FETCH COUNTRY LIST  */
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Warehouse Tracking";
        $data['contentTop'] = array('breadcrumbText' => 'Warehouse Tracking', 'contentTitle' => 'Warehouse Tracking', 'pageInfo' => 'This section allows you to view procurement statistics and resports');
        $data['pageTitle'] = "Warehouse Tracking";
        $data['page'] = $procurementData->currentPage();
        $data['procurementData'] = $procurementData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        return view('Administrator.report.index-procurement', $data);
    }
    
    public function shipmenttracking(Request $request) {
        
        $data = array();

        \Session::forget('SHIPMENTREPORTDATA');
        $searchShipmentArr = array(
            'idFrom' => '',
            'idTo' => "",
            'unitTo' => '',
            'unitFrom' => '',
            'shipmentStatus' => '',
            'prepaid' => '',
            'deliveryCompanyId' => '',
            'dispatchCompanyId' => '',
            'shippingMethod' => '',
            'shipmentType' => '',
            'totalFrom' => '',
            'totalTo' => '',
            'user' => '',
            'receiver' => '',
            'paymentStatus' => '',
            'toCountry' => '',
            'warehouseId' => '',
            'labelType' => '',
            'itemType' => '',
            'store' => '',
            'category' => '',
            'subcategory' => '',
            'product' => '',
        );
        
        //  \Session::forget('SHIPMENTDATA');

        if (\Request::isMethod('post')) {

            /* GET POST VALUE  */
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchShipment = \Input::get('searchShipment', $searchShipmentArr);
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $searchWarehouse = \Input::get('searchWarehouse', '');

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('SHIPMENTREPORTDATA');
            \Session::push('SHIPMENTREPORTDATA.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('SHIPMENTREPORTDATA.searchDisplay', $searchDisplay);
            \Session::push('SHIPMENTREPORTDATA.searchWarehouse', $searchWarehouse);
            \Session::push('SHIPMENTREPORTDATA.searchByDate', $searchByDate);
            \Session::push('SHIPMENTREPORTDATA.searchShipment', $searchShipment);
            \Session::push('SHIPMENTREPORTDATA.field', $field);
            \Session::push('SHIPMENTREPORTDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchShipment'] = $searchShipment;
            $param['searchDisplay'] = $searchDisplay;
            $param['searchWarehouse'] = $searchWarehouse;
        } else {
            $sortField = \Session::get('SHIPMENTREPORTDATA.field');
            $sortType = \Session::get('SHIPMENTREPORTDATA.type');
            $searchByCreatedOn = \Session::get('SHIPMENTREPORTDATA.searchByCreatedOn');
            $searchByDate = \Session::get('SHIPMENTREPORTDATA.searchByDate');
            $searchShipment = \Session::get('SHIPMENTREPORTDATA.searchShipment');
            $searchDisplay = \Session::get('SHIPMENTREPORTDATA.searchDisplay');
            $searchWarehouse = \Session::get('SHIPMENTREPORTDATA.searchWarehouse');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchShipment'] = !empty($searchShipment) ? $searchShipment[0] : $searchShipmentArr;
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
            $param['searchWarehouse'] = $searchWarehouse;
        }


        /* BUILD SORTING ARRAY */
        $sort = array(
            'id' => array('current' => 'sorting'),
            'fromName' => array('current' => 'sorting'),
            'deliveryCompany' => array('current' => 'sorting'),
            'dispatchCompany' => array('current' => 'sorting'),
            'total' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH USER LIST  */
        $shipmentData = Shipment::getShipmentList($param);
        
        /* Fetch shipment status wise statistics data */
        $shipmentStats = Shipment::statusWiseStatistics();

        /* FETCH DELIVERY COMPANY LIST  */
        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();
        /* FETCH DISPATCH COMPANY LIST  */
        $data['dispatchCompanyList'] = Dispatchcompany::where('status', '1')->orderby('name', 'asc')->get();
        /* FETCH COUNTRY LIST  */
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();
        /* FETCH Warehouse LIST  */
        $data['warehouseList'] = Shipment::getWarehouseMappedList();
        /* FETCH SHIPPING METHOD LIST  */
        $shippingMethodData = collect(Shippingmethods::where('active', 'Y')->where('deleted','0')->orderby('shipping', 'asc')->get());
        $data['shippingMethodList'] = $shippingMethodData->mapWithKeys(function($item){ return [$item['shippingid']=>$item['shipping']]; });
        /* FETCH STORE LIST  */
        $data['storeList'] = \App\Model\Stores::where('deleted', '0')->where('status', '1')->orderby('storeName', 'asc')->get();

        /* FETCH CATEGORY LIST  */
        $data['categoryList'] = \App\Model\Sitecategory::getCategoryList('parent');
        
        /* SET DATA FOR VIEW  */
        $data['subcategoryList'] = array();
        if(!empty($param['searchShipment']['category']))
        {
            $data['subcategoryList'] = \App\Model\Sitecategory::getSubCategory($param['searchShipment']['category']);
        }
        $data['productList'] = array();
        if(!empty($param['searchShipment']['subcategory']))
        {
            $data['productList'] = \App\Model\Siteproduct::getListBySubCatid($param['searchShipment']['subcategory']);
        }
        
        $data['page'] = $shipmentData->currentPage();
        $data['shipmentData'] = $shipmentData;
        $data['searchData'] = $param;
        $data['shipmentStats'] = $shipmentStats;
        $data['sort'] = $sort;
        $data['title'] = "Administrative Panel :: Warehouse Tracking";
        $data['contentTop'] = array('breadcrumbText' => 'Warehouse Tracking', 'contentTitle' => 'Warehouse Tracking', 'pageInfo' => 'This section allows you to view shipment statistics and reports');
        $data['pageTitle'] = "Warehouse Tracking";
        
        return view('Administrator.report.shipmenttracking', $data);
    }


    /**
     * Method used to clear search history
     * @return type
     */
    public function showall($type = 'procurement') {
        if($type == 'procurement')
        {
            \Session::forget('PROCUREMENTREPORT');
            return \Redirect::to('administrator/report/procurement-tracking/procurementindex');
        }
        elseif($type == 'customer')
        {
            \Session::forget('CUSTOMERREPORTDATA');
            return \Redirect::to('administrator/report/customer');
        }
        else
        {
            \Session::forget('SHIPMENTREPORTDATA');
            return \Redirect::to('administrator/report/shipment-tracking');
        }
    }

    /**
     * Method used to Export All with selected fields
     * @param integer $page
     * @return type
     */
    public function exportall_procurement($page, Request $request) {
        // dd(session()->all());
        if (count($request->selectall) == 0) {
            return \Redirect::to('administrator/procurement/')->with('errorMessage', 'Select atleast one field during the time of export');
        }

        $searchProcurementArr = array(
            'idFrom' => '',
            'idTo' => '',
            'totalCostFrom' => '',
            'totalCostTo' => '',
            'toCountry' => '',
            'toState' => '',
            'toCity' => '',
            'deliveryCompanyId' => '',
            'status' => '',
            'paymentStatus' => '',
            'user' => '',
            'procurementType' => ''
        );


        if (\Request::isMethod('post')) {

            $sortField = \Session::get('PROCUREMENTREPORT.field');
            $sortType = \Session::get('PROCUREMENTREPORT.type');
            $searchByDate = \Session::get('PROCUREMENTREPORT.searchByDate');
            $searchByCreatedOn = \Session::get('PROCUREMENTREPORT.searchByCreatedOn');
            $searchByShipmentId = \Session::get('PROCUREMENTREPORT.searchByShipmentId');
            $searchByCustomer = \Session::get('PROCUREMENTREPORT.searchByCustomer');
            $searchByDestination = \Session::get('PROCUREMENTREPORT.searchByDestination');
            $searchByCost = \Session::get('PROCUREMENTREPORT.searchByCost');
            $searchByPaymentStatus = \Session::get('PROCUREMENTREPORT.searchByPaymentStatus');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByShipmentId'] = !empty($searchByShipmentId) ? $searchByShipmentId[0] : '';
            $param['searchByCustomer'] = !empty($searchByCustomer) ? $searchByCustomer[0] : '';
            $param['searchByDestination'] = !empty($searchByDestination) ? $searchByDestination[0] : '';
            $param['searchByCost'] = !empty($searchByCost) ? $searchByCost[0] : '';
            $param['searchByPaymentStatus'] = !empty($searchByPaymentStatus) ? $searchByPaymentStatus[0] : '';

            $searchProcurement = \Session::get('PROCUREMENTREPORT.searchProcurement');
            $param['searchProcurement'] = !empty($searchProcurement) ? $searchProcurement[0] : $searchProcurementArr;


            $recordList = Procurement::getShopForMeList($param, 'export', 'ALL')->toArray();
            //print_r($request->selectall); die;
            $shipmentData = array();
            $headers = array();
            if (!empty($recordList)) {
                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {

                            if ($count == 0) {
                                if ($key == 'procurementid')
                                    $cellValue = 'Shipment ID';
                                elseif ($key == 'customer')
                                    $cellValue = 'Customer';
                                elseif ($key == 'destinationCityName')
                                    $cellValue = 'Destination City Name';
                                elseif ($key == 'destinationStateName')
                                    $cellValue = 'Destination State Name';
                                elseif ($key == 'destinationCountryName')
                                    $cellValue = 'Destination Country Name';
                                elseif ($key == 'createdOn')
                                    $cellValue = 'Date';
                                elseif ($key == 'totalcost')
                                    $cellValue = 'Cost';
                                elseif ($key == 'paymentstatus')
                                    $cellValue = 'Payment Status';
                                elseif ($key == 'status')
                                    $cellValue = 'Status';

                                $headers[$key] = $cellValue;
                            }
                        }
                    }
                }

                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
            if (in_array($key, $request->selectall)) {
                        if ($key == 'createdOn')
                            $value = \Carbon\Carbon::parse($value)->format('Y-m-d');

                        $shipmentData[$count + 1][$headers[$key]] = $value;
            }
                    }
                    //print_r($shipmentData);
                }
            }
            // print_r($shipmentData);die;
            ob_end_clean();
            ob_start();
            Excel::create("Procurement-" . \Carbon\Carbon::now(), function($excel) use($shipmentData) {
                $excel->sheet('Sheet 1', function($sheet) use($shipmentData) {
                    $sheet->fromArray($shipmentData);
                });
            })->export('xls');
            ob_flush();


            return \Redirect::to('administrator/procurement/')->with('successMessage', 'Excel file created and downloaded');
        }
    }



    /**
     * Method used to Export selected with selected fields
     * @param integer $page
     * @return type
     */
    public function exportselected_procurement($page, Request $request) {
        /* if (count($request->selectall) == 0) {
          return \Redirect::to('administrator/users/')->with('errorMessage', 'Select atleast one field during the time of export');
          } */
        if (\Request::isMethod('post')) {

            $sortField = \Session::get('PROCUREMENTDATA.field');
            $sortType = \Session::get('PROCUREMENTDATA.type');
            $searchByDate = \Session::get('PROCUREMENTDATA.searchByDate');
            $searchByCreatedOn = \Session::get('PROCUREMENTDATA.searchByCreatedOn');
            $searchByShipmentId = \Session::get('PROCUREMENTDATA.searchByShipmentId');
            $searchByCustomer = \Session::get('PROCUREMENTDATA.searchByCustomer');
            $searchByDestination = \Session::get('PROCUREMENTDATA.searchByDestination');
            $searchByCost = \Session::get('PROCUREMENTDATA.searchByCost');
            $searchByPaymentStatus = \Session::get('PROCUREMENTDATA.searchByPaymentStatus');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByShipmentId'] = !empty($searchByShipmentId) ? $searchByShipmentId[0] : '';
            $param['searchByCustomer'] = !empty($searchByCustomer) ? $searchByCustomer[0] : '';
            $param['searchByDestination'] = !empty($searchByDestination) ? $searchByDestination[0] : '';
            $param['searchByCost'] = !empty($searchByCost) ? $searchByCost[0] : '';
            $param['searchByPaymentStatus'] = !empty($searchByPaymentStatus) ? $searchByPaymentStatus[0] : '';
            $param['searchByshipment'] = $request->selected;

            $searchProcurement = \Session::get('PROCUREMENTREPORT.searchProcurement');
            $param['searchProcurement'] = !empty($searchProcurement) ? $searchProcurement[0] : $searchProcurementArr;

            $recordList = Procurement::getShopForMeList($param, 'export', 'ALL')->toArray();

            $shipmentData = array();
            $headers = array();
            if (!empty($recordList)) {
                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {
                            if ($count == 0) {
                                if ($key == 'procurementid')
                                    $cellValue = 'Shipment ID';
                                elseif ($key == 'customer')
                                    $cellValue = 'Customer';
                                elseif ($key == 'destinationCityName')
                                    $cellValue = 'Destination City Name';
                                elseif ($key == 'destinationStateName')
                                    $cellValue = 'Destination State Name';
                                elseif ($key == 'destinationCountryName')
                                    $cellValue = 'Destination Country Name';
                                elseif ($key == 'createdOn')
                                    $cellValue = 'Date';
                                elseif ($key == 'totalcost')
                                    $cellValue = 'Cost';
                                elseif ($key == 'paymentstatus')
                                    $cellValue = 'Payment Status';
                                elseif ($key == 'status')
                                    $cellValue = 'Status';

                                $headers[$key] = $cellValue;
                            }
                        }
                    }
                }

                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
            if (in_array($key, $request->selectall)) {

                        if ($key == 'createdOn')
                            $value = \Carbon\Carbon::parse($value)->format('Y-m-d');

                        $shipmentData[$count + 1][$headers[$key]] = $value;
            }
                    }
                }
            }

            $excelName = "Procurement-" . \Carbon\Carbon::now();
            $path = public_path('export/' . $excelName);

            ob_end_clean();
            ob_start();
            Excel::create($excelName, function($excel) use($shipmentData) {
                $excel->sheet('Sheet 1', function($sheet) use($shipmentData) {
                    $sheet->fromArray($shipmentData);
                });
            })->store('xls', public_path('exports'));
            ob_flush();
            $creatingPath = url('/') . "/public/exports/" . $excelName . ".xls";
            return response()->json(['path' => $creatingPath]);
        }
    }
    
    /**
     * This function used to generate and export All shipment records
     * @param type $page
     * @param Request $request
     */
    public function exportallshipment($page, Request $request) {

        $shipmentObj = new Shipment;
        $param = array();
        /* Initialize search field data */
        $searchShipmentArr = array(
            'idFrom' => '',
            'idTo' => '',
            'unitTo' => '',
            'unitFrom' => '',
            'shipmentStatus' => '',
            'prepaid' => '',
            'deliveryCompanyId' => '',
            'dispatchCompanyId' => '',
            'shipmentType' => '',
            'totalFrom' => '',
            'totalTo' => '',
            'user' => '',
            'receiver' => '',
            'paymentStatus' => '',
            'toCountry' => '',
            'warehouseId' => '',
            'labelType' => '',
            'itemType' => '',
        );
        $searchByCreatedOn = \Session::get('SHIPMENTREPORTDATA.searchByCreatedOn');
        $searchByDate = \Session::get('SHIPMENTREPORTDATA.searchByDate');
        $searchShipment = \Session::get('SHIPMENTREPORTDATA.searchShipment');

        $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
        $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
        $param['searchShipment'] = !empty($searchShipment) ? $searchShipment[0] : $searchShipmentArr;
        /* Get data to export */
        $exportData = $shipmentObj->exportData($param, array(), $request->selectall);
        $checkedBoxes = $request->selectall;
        $del = array();
        $arr = array();

        if(!empty($request->selectall)){
            foreach ($request->selectall as $selectAll) {
                if($selectAll == "isDelivery"){
                    $del = $shipmentObj->exportDataDeliveryReport($exportData['shipments'], $need = 'all');
                    foreach($del['delivery'] as $key => $val){
                        foreach($val as $key1 => $val1){

                            $arr['deli'][$val1['Shipment ID']][$key1] = $val1;
                        }
                    }
                }
            }
            
        }

        $excelName = "Shipment-Data" . Carbon::now();
        ob_end_clean();
        ob_start();
        /* Generate excell */
        Excel::create($excelName, function($excel) use($exportData, $del, $checkedBoxes, $arr) {
            $excel->sheet('Shipments', function($sheet) use($exportData) {
                $sheet->fromArray($exportData['shipments']);
                $sheet->row(1, function($row) { $row->setBackground('#CCCCCC'); $row->setFontWeight('bold'); });
            });

            if(!empty($checkedBoxes)){
                foreach ($checkedBoxes as $selectAll) {
                    if($selectAll == "isDelivery"){
                        $excel->sheet('Deliveries', function($sheet) use($arr) {
                            $i = 1;
                            foreach (array_values($arr['deli']) as $k => $record) {
                                
                                if($k > 0){
                                    $sheet->fromArray($record, null, 'A1', false, false);
                                } else {
                                    $sheet->fromArray($record);
                                }
                            }
                            $sheet->row(1, function($row) { $row->setBackground('#CCCCCC'); $row->setFontWeight('bold'); });

                        });
                    }
                }
            }

        })->export('xlsx');
        ob_flush();
    }
    
    /**
     * This function used to generate and export Selected shipment records
     * @param type $page
     * @param Request $request
     * @return type
     */
    public function exportselected($page, Request $request) {

        $shipmentObj = new Shipment;
        /* Get data for export */
        $exportData = $shipmentObj->exportData(array(), $request->selected, $request->selectedField);
        //print_r($exportData);exit;
        $checkedBoxes = $request->selectedField;
        $arr = array();
        if(!empty($request->selectedField)){
            foreach ($request->selectedField as $selectAll) { 
                if($selectAll == "isDelivery"){
                    $del = $shipmentObj->exportDataDeliveryReport($request->selected, $need = 'selected');
                    foreach($del['delivery'] as $key => $val){
                        foreach($val as $key1 => $val1){

                            $arr['deli'][$val1['Shipment ID']][$key1] = $val1;
                        }
                    }
                }
            }
        }

        $excelName = "Shipment-Data" . Carbon::now();
        ob_end_clean();
        ob_start();
        /* Generate Excel */
        Excel::create($excelName, function($excel) use($exportData, $checkedBoxes, $arr) {
            $excel->sheet('Shipments', function($sheet) use($exportData) {
                $sheet->fromArray($exportData['shipments']);
                $sheet->row(1, function($row) { $row->setBackground('#CCCCCC'); $row->setFontWeight('bold'); });
            });
            if(!empty($checkedBoxes)){
                foreach ($checkedBoxes as $selectAll) {
                    if($selectAll == "isDelivery"){
                        $excel->sheet('Deliveries', function($sheet) use($arr) {
                            $i = 1;
                            foreach (array_values($arr['deli']) as $k => $record) {
                                
                                if($k > 0){
                                    $sheet->fromArray($record, null, 'A1', false, false);
                                } else {
                                    $sheet->fromArray($record);
                                }
                            }
                            $sheet->row(1, function($row) { $row->setBackground('#CCCCCC'); $row->setFontWeight('bold'); });

                        });
                    }
                }
            }
        })->store('xlsx', public_path('exports'));
        ob_flush();
        $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
        return response()->json(['path' => $creatingPath]);
    }

    public function storehistory(Route $route, Request $request) {
        $data = array();

        /* BUILD DEFAULT SEARCH ARRAY */
        $searchStoreArr = array(
            'storeId' => '',
            'category' => '',
            'subCategory' => '',
            'product' => '',
                        
        );

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchStore = \Input::post('searchStore', $searchStoreArr);
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

           
            $field = \Input::get('field', 'stores.id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('STOREREPORT');
            \Session::push('STOREREPORT.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('STOREREPORT.searchDisplay', $searchDisplay);
            \Session::push('STOREREPORT.searchByDate', $searchByDate);
            \Session::push('STOREREPORT.searchStore', $searchStore);
            \Session::push('STOREREPORT.field', $field);
            \Session::push('STOREREPORT.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchStore'] = $searchStore;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('STOREREPORT.field');
            $sortType = \Session::get('STOREREPORT.type');
            $searchByCreatedOn = \Session::get('STOREREPORT.searchByCreatedOn');
            $searchByDate = \Session::get('STOREREPORT.searchByDate');

            $searchStore = \Session::get('STOREREPORT.searchStore');
            $searchDisplay = \Session::get('STOREREPORT.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'stores.createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchStore'] = !empty($searchStore) ? $searchStore[0] : $searchStoreArr;
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'storeName' => array('current' => 'sorting'),
            'categoryName' => array('current' => 'sorting'),
            'subCategoryName' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting')
        );


        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        


        /* GET THE STORES */
        $data['stores'] = \App\Model\Stores::where('status', '1')->where('deleted', '0')->get();

        
        $storeData = \App\Model\Stores::getcustomerDetailsByStore($param);

        $data['categoryList'] = array();
        if(!empty($param['searchStore']['storeId']))
        {
            $data['categoryList'] = Sitecategory::where('status', '1')->where('parentCategoryId', '-1')->get();
        }
        
       // print_r($storeData); die;
        
        /* SET DATA FOR VIEW  */

       // $data['page'] = $storeData->currentPage();
        $data['title'] = "Administrative Panel :: Store History";
        $data['contentTop'] = array('breadcrumbText' => 'Store History', 'contentTitle' => 'Store History', 'pageInfo' => 'This section allows you to view store history and reports');
        $data['pageTitle'] = "Store History";
        $data['storeData'] = $storeData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        return view('Administrator.report.storehistory', $data);
    }


    public function exportallstores($page)
    {


        $shipmentObj = new Shipment;
        $param = array();
        /* BUILD DEFAULT SEARCH ARRAY */
        $searchStoreArr = array(
            'storeId' => '',
            'category' => '',
            'subCategory' => '',
            'product' => '',
                        
        );
        $searchByCreatedOn = \Session::get('STOREREPORT.searchByCreatedOn');
        $searchByDate = \Session::get('STOREREPORT.searchByDate');
        $searchStore = \Session::get('STOREREPORT.searchStore');

        $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
        $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
        $param['searchStore'] = !empty($searchStore) ? $searchStore[0] : $searchStoreArr;
        /* Get data to export */
        $exportData = \App\Model\Stores::exportStore($param);
        

        $excelName = "Store-Data" . Carbon::now();

        ob_end_clean();
        ob_start();

        /* Generate excell */

        Excel::create($excelName, function($excel) use($exportData) {
            $excel->sheet('Store History', function($sheet) use($exportData) {
                $sheet->fromArray($exportData);
                $sheet->row(1, function($row) { $row->setBackground('#CCCCCC'); $row->setFontWeight('bold'); });
            });
        })->store('xlsx', public_path('exports'));

        ob_flush();
         $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
        return response()->json(['path' => $creatingPath]);

    }


    /**
     * Method for special offers report
     * @return array
     */
    public function offerreport(Request $request) {
        
        $data = array();

        //\Session::forget('OFFERREPORT');
        $searchShipmentArr = array(
            'idFrom' => '',
            'idTo' => "",
            'unitTo' => '',
            'unitFrom' => '',
            'shipmentStatus' => '',
            'prepaid' => '',
            'deliveryCompanyId' => '',
            'dispatchCompanyId' => '',
            'shippingMethod' => '',
            'shipmentType' => '',
            'totalFrom' => '',
            'totalTo' => '',
            'user' => '',
            'receiver' => '',
            'paymentStatus' => '',
            'toCountry' => '',
            'warehouseId' => '',
            'labelType' => '',
            'itemType' => '',
            'store' => '',
            'category' => '',
            'subcategory' => '',
            'product' => '',
        );
        
        //  \Session::forget('SHIPMENTDATA');

        if (\Request::isMethod('post')) {

            /* GET POST VALUE  */
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchShipment = \Input::get('searchShipment', $searchShipmentArr);
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $searchWarehouse = \Input::get('searchWarehouse', '');
            $searchOffer = \Input::get('searchOffer', '');

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('OFFERREPORT');
            \Session::push('OFFERREPORT.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('OFFERREPORT.searchDisplay', $searchDisplay);
            \Session::push('OFFERREPORT.searchWarehouse', $searchWarehouse);
            \Session::push('OFFERREPORT.searchOffer', $searchOffer);
            \Session::push('OFFERREPORT.searchByDate', $searchByDate);
            \Session::push('OFFERREPORT.searchShipment', $searchShipment);
            \Session::push('OFFERREPORT.field', $field);
            \Session::push('OFFERREPORT.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchShipment'] = $searchShipment;
            $param['searchDisplay'] = $searchDisplay;
            $param['searchWarehouse'] = $searchWarehouse;
            $param['searchOffer'] = $searchOffer;
        } else {
            $sortField = \Session::get('OFFERREPORT.field');
            $sortType = \Session::get('OFFERREPORT.type');
            $searchByCreatedOn = \Session::get('OFFERREPORT.searchByCreatedOn');
            $searchByDate = \Session::get('OFFERREPORT.searchByDate');
            $searchShipment = \Session::get('OFFERREPORT.searchShipment');
            $searchDisplay = \Session::get('OFFERREPORT.searchDisplay');
            $searchWarehouse = \Session::get('OFFERREPORT.searchWarehouse');
            $searchOffer = \Session::get('OFFERREPORT.searchOffer.0');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchShipment'] = !empty($searchShipment) ? $searchShipment[0] : $searchShipmentArr;
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
            $param['searchWarehouse'] = '';
            $param['searchOffer'] = $searchOffer;
        }


        /* BUILD SORTING ARRAY */
        $sort = array(
            'id' => array('current' => 'sorting'),
            'fromName' => array('current' => 'sorting'),
            'deliveryCompany' => array('current' => 'sorting'),
            'dispatchCompany' => array('current' => 'sorting'),
            'total' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

    
        /* GET THE COUPONS/OFFERS */
        $paramOffer = array();
        $paramOffer['field'] = 'id';
        $paramOffer['type'] = 'desc';
        $paramOffer['searchDisplay'] = 10000;
        $data['offersList'] = \App\Model\Offer::getList($paramOffer);


        if(!empty(\Session::get('OFFERREPORT.searchOffer.0'))){
            $countShipment = \App\Model\Offer::countCouponUsedShipment(\Session::get('OFFERREPORT.searchOffer.0'), 'count');
            $countAuto = \App\Model\Offer::countCouponUsedAuto(\Session::get('OFFERREPORT.searchOffer.0'), 'count');
            $countFillShip = \App\Model\Offer::countCouponUsedFillShip(\Session::get('OFFERREPORT.searchOffer.0'), 'count');
            $countProc = \App\Model\Offer::countCouponUsedProc(\Session::get('OFFERREPORT.searchOffer.0'), 'count');

            $totalcostShipment = \App\Model\Offer::countCouponUsedShipment(\Session::get('OFFERREPORT.searchOffer.0'), 'total');
            $totalcostAuto = \App\Model\Offer::countCouponUsedAuto(\Session::get('OFFERREPORT.searchOffer.0'), 'total');
            $totalcostFillShip = \App\Model\Offer::countCouponUsedFillShip(\Session::get('OFFERREPORT.searchOffer.0'), 'total');
            $totalcostProc = \App\Model\Offer::countCouponUsedProc(\Session::get('OFFERREPORT.searchOffer.0'), 'total');

            $discountShipment = \App\Model\Offer::countCouponUsedShipment(\Session::get('OFFERREPORT.searchOffer.0'), 'discount');
            $discountcostAuto = \App\Model\Offer::countCouponUsedAuto(\Session::get('OFFERREPORT.searchOffer.0'), 'discount');
            $discountcostFillShip = \App\Model\Offer::countCouponUsedFillShip(\Session::get('OFFERREPORT.searchOffer.0'), 'discount');
            $discountcostProc = \App\Model\Offer::countCouponUsedProc(\Session::get('OFFERREPORT.searchOffer.0'), 'discount');

            $data['totalUsed'] = $countShipment + $countAuto + $countFillShip + $countProc;
            $data['totalCost'] = $totalcostShipment + $totalcostAuto + $totalcostFillShip + $totalcostProc;
            $data['totalDiscount'] = $discountShipment + $discountcostAuto + $discountcostFillShip + $discountcostProc;

            $data['shipment'] = $countShipment;
            $data['auto'] = $countAuto;
            $data['fillship'] = $countFillShip;
            $data['proc'] = $countProc;

            $data['totalcostShipment']      = $totalcostShipment;
            $data['totalcostAuto']          = $totalcostAuto;
            $data['totalcostFillShip']      = $totalcostFillShip;
            $data['totalcostProc']          = $totalcostProc;

            if(empty($request->id) || $request->id == 1){
                $shipmentData = Shipment::getShipmentList($param);
                $data['page'] = $shipmentData->currentPage();
                $data['shipmentData'] = $shipmentData;
                $data['typeId'] = 1;
            } else if($request->id == 2){
                $shipmentData = \App\Model\Autoshipment::getAutoshipmentListForReport($param, '');
                $data['page'] = $shipmentData->currentPage();
                $data['shipmentData'] = $shipmentData;
                $data['typeId'] = 2;
            } else if($request->id == 3){
                $shipmentData = \App\Model\Fillship::getShipmentList($param);
                $data['page'] = $shipmentData->currentPage();
                $data['shipmentData'] = $shipmentData;
                $data['typeId'] = 3;
            } else {
                $shipmentData = \App\Model\Procurement::getProcurementList($param);
                $data['page'] = $shipmentData->currentPage();
                $data['shipmentData'] = $shipmentData;
                $data['typeId'] = 4;
            }

        } else {
            $data['totalUsed'] = 0;
            $data['totalCost'] = 0;
            $data['totalDiscount'] = 0;

            $data['shipment'] = 0;
            $data['auto'] = 0;
            $data['fillship'] = 0;
            $data['proc'] = 0;

            $shipmentData = array();
            $data['page'] = 1;
            $data['shipmentData'] = $shipmentData;

            $data['typeId'] = 0;
        }
        
        
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['title'] = "Administrative Panel :: Special Offer Report";
        $data['contentTop'] = array('breadcrumbText' => 'Special Offer Report', 'contentTitle' => 'Special Offer Report', 'pageInfo' => 'This section allows you to view special offer report');
        $data['pageTitle'] = "Special Offer Report";
        
        return view('Administrator.report.offerreport', $data);
    }





    /**
     * Method used to Export All Procurement data with coupon code
     * @param integer $page
     * @return type
     */
    public function exportallProcurementReportForSpecialOffer($page, Request $request) {
        // dd(session()->all());
        if (count($request->selectall) == 0) {
            return \Redirect::to('administrator/procurement/')->with('errorMessage', 'Select atleast one field during the time of export');
        }
        if (\Request::isMethod('post')) {

            $searchOffer = \Session::get('OFFERREPORT.searchOffer.0');
            $param['searchOffer'] = $searchOffer;

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByShipmentId'] = !empty($searchByShipmentId) ? $searchByShipmentId[0] : '';
            $param['searchByCustomer'] = !empty($searchByCustomer) ? $searchByCustomer[0] : '';
            $param['searchByDestination'] = !empty($searchByDestination) ? $searchByDestination[0] : '';
            $param['searchByCost'] = !empty($searchByCost) ? $searchByCost[0] : '';
            $param['searchByPaymentStatus'] = !empty($searchByPaymentStatus) ? $searchByPaymentStatus[0] : '';


            $recordList = Procurement::getProcurementList($param, 'export')->toArray();
            //print_r($request->selectall); die;
            $shipmentData = array();
            $headers = array();
            if (!empty($recordList)) {
                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {

                            if ($count == 0) {
                                if ($key == 'id')
                                    $cellValue = 'ID';
                                elseif ($key == 'customer')
                                    $cellValue = 'Customer';
                                elseif ($key == 'destinationCityName')
                                    $cellValue = 'Destination City Name';
                                elseif ($key == 'destinationStateName')
                                    $cellValue = 'Destination State Name';
                                elseif ($key == 'destinationCountryName')
                                    $cellValue = 'Destination Country Name';
                                elseif ($key == 'createdOn')
                                    $cellValue = 'Date';
                                elseif ($key == 'totalcost')
                                    $cellValue = 'Cost';
                                elseif ($key == 'paymentstatus')
                                    $cellValue = 'Payment Status';
                                elseif ($key == 'totalDiscount')
                                    $cellValue = 'Discount Received';
                                elseif ($key == 'totalWeight')
                                    $cellValue = 'Total Weight';

                                $headers[$key] = $cellValue;
                            }
                        }
                    }
                }

                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {
                            if ($key == 'createdOn')
                                $value = \Carbon\Carbon::parse($value)->format('Y-m-d');

                            $shipmentData[$count + 1][$headers[$key]] = $value;
                        }
                    }
                    //print_r($shipmentData);
                }
            }
            // print_r($shipmentData);die;
            ob_end_clean();
            ob_start();
            Excel::create("Procurement-" . \Carbon\Carbon::now(), function($excel) use($shipmentData) {
                $excel->sheet('Sheet 1', function($sheet) use($shipmentData) {
                    $sheet->fromArray($shipmentData);
                });
            })->export('xls');
            ob_flush();


            return \Redirect::to('administrator/procurement/')->with('successMessage', 'Excel file created and downloaded');
        }
    }

    /**
     * Method used to Export Selected Procurement data with coupon code
     * @param integer $page
     * @return type
     */
    public function exportselectedProcurementReportForSpecialOffer($page, Request $request) {
        if (count($request->selectall) == 0) {
            return \Redirect::to('administrator/procurement/')->with('errorMessage', 'Select atleast one field during the time of export');
        }
        if (\Request::isMethod('post')) {

            $searchOffer = \Session::get('OFFERREPORT.searchOffer.0');
            $param['searchOffer'] = $searchOffer;

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByShipmentId'] = !empty($searchByShipmentId) ? $searchByShipmentId[0] : '';
            $param['searchByCustomer'] = !empty($searchByCustomer) ? $searchByCustomer[0] : '';
            $param['searchByDestination'] = !empty($searchByDestination) ? $searchByDestination[0] : '';
            $param['searchByCost'] = !empty($searchByCost) ? $searchByCost[0] : '';
            $param['searchByPaymentStatus'] = !empty($searchByPaymentStatus) ? $searchByPaymentStatus[0] : '';

            $recordList = Procurement::getProcurementList($param, 'export', $request->selected)->toArray();
            $shipmentData = array();
            $headers = array();
            if (!empty($recordList)) {
                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {

                            if ($count == 0) {
                                if ($key == 'id')
                                    $cellValue = 'ID';
                                elseif ($key == 'customer')
                                    $cellValue = 'Customer';
                                elseif ($key == 'destinationCityName')
                                    $cellValue = 'Destination City Name';
                                elseif ($key == 'destinationStateName')
                                    $cellValue = 'Destination State Name';
                                elseif ($key == 'destinationCountryName')
                                    $cellValue = 'Destination Country Name';
                                elseif ($key == 'createdOn')
                                    $cellValue = 'Date';
                                elseif ($key == 'totalcost')
                                    $cellValue = 'Cost';
                                elseif ($key == 'paymentstatus')
                                    $cellValue = 'Payment Status';
                                elseif ($key == 'totalDiscount')
                                    $cellValue = 'Discount Received';
                                elseif ($key == 'totalWeight')
                                    $cellValue = 'Total Weight';

                                $headers[$key] = $cellValue;
                            }
                        }
                    }
                }

                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {
                            if ($key == 'createdOn')
                                $value = \Carbon\Carbon::parse($value)->format('Y-m-d');

                            $shipmentData[$count + 1][$headers[$key]] = $value;
                        }
                    }
                }
            }

            ob_end_clean();
            ob_start();
            $excelName = "Procurement-" . Carbon::now();
            Excel::create($excelName, function($excel) use($shipmentData) {
                $excel->sheet('Sheet 1', function($sheet) use($shipmentData) {
                    $sheet->fromArray($shipmentData);
                });
            })->store('xlsx', public_path('exports'));
            ob_flush();
            $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
            return response()->json(['path' => $creatingPath]);
        }
    }




    /**
     * Method used to Export All auto data with selected fields with coupon code
     * @param integer $page
     * @return type
     */
    public function exportallAutoReportForSpecialOffer($page, Request $request) {
        // dd(session()->all());
        if (count($request->selectall) == 0) {
            return \Redirect::to('administrator/procurement/')->with('errorMessage', 'Select atleast one field during the time of export');
        }
        if (\Request::isMethod('post')) {

            $searchOffer = \Session::get('OFFERREPORT.searchOffer.0');
            $param['searchOffer'] = $searchOffer;

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByShipmentId'] = !empty($searchByShipmentId) ? $searchByShipmentId[0] : '';
            $param['searchByCustomer'] = !empty($searchByCustomer) ? $searchByCustomer[0] : '';
            $param['searchByDestination'] = !empty($searchByDestination) ? $searchByDestination[0] : '';
            $param['searchByCost'] = !empty($searchByCost) ? $searchByCost[0] : '';
            $param['searchByPaymentStatus'] = !empty($searchByPaymentStatus) ? $searchByPaymentStatus[0] : '';


            $recordList = \App\Model\Autoshipment::getAutoshipmentListForReport($param, 'export')->toArray();
            $shipmentData = array();
            $headers = array();
            if (!empty($recordList)) {
                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {

                            if ($count == 0) {
                                if ($key == 'id')
                                    $cellValue = 'ID';
                                elseif ($key == 'customer')
                                    $cellValue = 'Customer';
                                elseif ($key == 'destinationCityName')
                                    $cellValue = 'Destination City Name';
                                elseif ($key == 'destinationStateName')
                                    $cellValue = 'Destination State Name';
                                elseif ($key == 'destinationCountryName')
                                    $cellValue = 'Destination Country Name';
                                elseif ($key == 'createdOn')
                                    $cellValue = 'Date';
                                elseif ($key == 'totalcost')
                                    $cellValue = 'Cost';
                                elseif ($key == 'paymentstatus')
                                    $cellValue = 'Payment Status';
                                elseif ($key == 'totalDiscount')
                                    $cellValue = 'Discount Received';
                                elseif ($key == 'totalWeight')
                                    $cellValue = 'Total Weight';

                                $headers[$key] = $cellValue;
                            }
                        }
                    }
                }

                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {
                            if ($key == 'createdOn')
                                $value = \Carbon\Carbon::parse($value)->format('Y-m-d');

                            $shipmentData[$count + 1][$headers[$key]] = $value;
                        }
                    }
                    //print_r($shipmentData);
                }
            }
            // print_r($shipmentData);die;
            ob_end_clean();
            ob_start();
            Excel::create("AutoShipment-" . \Carbon\Carbon::now(), function($excel) use($shipmentData) {
                $excel->sheet('Sheet 1', function($sheet) use($shipmentData) {
                    $sheet->fromArray($shipmentData);
                });
            })->export('xls');
            ob_flush();


            return \Redirect::to('administrator/procurement/')->with('successMessage', 'Excel file created and downloaded');
        }
    }

    /**
     * Method used to Export Selected auto data with coupon code
     * @param integer $page
     * @return type
     */
    public function exportselectedAutoReportForSpecialOffer($page, Request $request) {
        if (count($request->selectall) == 0) {
            return \Redirect::to('administrator/procurement/')->with('errorMessage', 'Select atleast one field during the time of export');
        }
        if (\Request::isMethod('post')) {

            $searchOffer = \Session::get('OFFERREPORT.searchOffer.0');
            $param['searchOffer'] = $searchOffer;

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByShipmentId'] = !empty($searchByShipmentId) ? $searchByShipmentId[0] : '';
            $param['searchByCustomer'] = !empty($searchByCustomer) ? $searchByCustomer[0] : '';
            $param['searchByDestination'] = !empty($searchByDestination) ? $searchByDestination[0] : '';
            $param['searchByCost'] = !empty($searchByCost) ? $searchByCost[0] : '';
            $param['searchByPaymentStatus'] = !empty($searchByPaymentStatus) ? $searchByPaymentStatus[0] : '';

            $recordList = \App\Model\Autoshipment::getAutoshipmentListForReport($param, 'export', $request->selected)->toArray();
            $shipmentData = array();
            $headers = array();
            if (!empty($recordList)) {
                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {

                            if ($count == 0) {
                                if ($key == 'id')
                                    $cellValue = 'ID';
                                elseif ($key == 'customer')
                                    $cellValue = 'Customer';
                                elseif ($key == 'destinationCityName')
                                    $cellValue = 'Destination City Name';
                                elseif ($key == 'destinationStateName')
                                    $cellValue = 'Destination State Name';
                                elseif ($key == 'destinationCountryName')
                                    $cellValue = 'Destination Country Name';
                                elseif ($key == 'createdOn')
                                    $cellValue = 'Date';
                                elseif ($key == 'totalcost')
                                    $cellValue = 'Cost';
                                elseif ($key == 'paymentstatus')
                                    $cellValue = 'Payment Status';
                                elseif ($key == 'totalDiscount')
                                    $cellValue = 'Discount Received';
                                elseif ($key == 'totalWeight')
                                    $cellValue = 'Total Weight';

                                $headers[$key] = $cellValue;
                            }
                        }
                    }
                }

                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {
                            if ($key == 'createdOn')
                                $value = \Carbon\Carbon::parse($value)->format('Y-m-d');

                            $shipmentData[$count + 1][$headers[$key]] = $value;
                        }
                    }
                }
            }

            ob_end_clean();
            ob_start();
            $excelName = "AutoShipment-" . Carbon::now();
            Excel::create($excelName, function($excel) use($shipmentData) {
                $excel->sheet('Sheet 1', function($sheet) use($shipmentData) {
                    $sheet->fromArray($shipmentData);
                });
            })->store('xlsx', public_path('exports'));
            ob_flush();
            $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
            return response()->json(['path' => $creatingPath]);
        }
    }






    /**
     * Method used to Export All fillship data with coupon code
     * @param integer $page
     * @return type
     */
    public function exportallFillshipReportForSpecialOffer($page, Request $request) {
        // dd(session()->all());
        if (count($request->selectall) == 0) {
            return \Redirect::to('administrator/autoparts/')->with('errorMessage', 'Select atleast one field during the time of export');
        }
        if (\Request::isMethod('post')) {

            $searchShipmentArr = array(
            'status' => '',
            'unit' => '',
            'shipmentId' => '',
            'deliveryCompanyId' => '',
            'totalCost'=>'',
            'receiver' => '',
            'paymentStatus' => '',
            'toCountry' => '',
            'toState' => '',
            'toCity' => ''
        );

            $searchOffer = \Session::get('OFFERREPORT.searchOffer.0');
            $param['searchOffer'] = $searchOffer;

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchShipment'] = !empty($searchShipment) ? $searchShipment[0] : $searchShipmentArr;


           
            $recordList = \App\Model\Fillship::exportallFillshipReportForSpecialOffer($param, 'export')->toArray();
            // print_r($recordList); die;
            $shipmentData = array();
            $headers = array();
            if (!empty($recordList)) {
                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {

                            if ($count == 0) {
                                if ($key == 'id')
                                    $cellValue = 'ID';
                                elseif ($key == 'customer')
                                    $cellValue = 'Customer';
                                elseif ($key == 'destinationCityName')
                                    $cellValue = 'Destination City Name';
                                elseif ($key == 'destinationStateName')
                                    $cellValue = 'Destination State Name';
                                elseif ($key == 'destinationCountryName')
                                    $cellValue = 'Destination Country Name';
                                elseif ($key == 'createdOn')
                                    $cellValue = 'Date';
                                elseif ($key == 'totalcost')
                                    $cellValue = 'Cost';
                                elseif ($key == 'status')
                                    $cellValue = 'Shipment Status';
                                elseif ($key == 'paymentstatus')
                                    $cellValue = 'Payment Status';
                                elseif ($key == 'totalDiscount')
                                    $cellValue = 'Discount Received';
                                elseif ($key == 'totalWeight')
                                    $cellValue = 'Total Weight';

                                $headers[$key] = $cellValue;
                            }
                        }
                    }
                }

                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {
                            if ($key == 'createdOn')
                                $value = \Carbon\Carbon::parse($value)->format('Y-m-d');

                            $shipmentData[$count + 1][$headers[$key]] = $value;
                        }
                    }
                    //print_r($shipmentData);
                }
            }
            // print_r($shipmentData);die;
            ob_end_clean();
            ob_start();
            Excel::create("FillnShip-" . \Carbon\Carbon::now(), function($excel) use($shipmentData) {
                $excel->sheet('Sheet 1', function($sheet) use($shipmentData) {
                    $sheet->fromArray($shipmentData);
                });
            })->export('xlsx');
            ob_flush();


            return \Redirect::to('administrator/fillnship/')->with('successMessage', 'Excel file created and downloaded');
        }
    }


    /**
     * Method used to Export Selected fillship data with coupon code
     * @param integer $page
     * @return type
     */
    public function exportselectedFillshipReportForSpecialOffer($page, Request $request) {
        if (count($request->selectall) == 0) {
            return \Redirect::to('administrator/procurement/')->with('errorMessage', 'Select atleast one field during the time of export');
        }
        if (\Request::isMethod('post')) {

            $searchOffer = \Session::get('OFFERREPORT.searchOffer.0');
            $param['searchOffer'] = $searchOffer;

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByShipmentId'] = !empty($searchByShipmentId) ? $searchByShipmentId[0] : '';
            $param['searchByCustomer'] = !empty($searchByCustomer) ? $searchByCustomer[0] : '';
            $param['searchByDestination'] = !empty($searchByDestination) ? $searchByDestination[0] : '';
            $param['searchByCost'] = !empty($searchByCost) ? $searchByCost[0] : '';
            $param['searchByPaymentStatus'] = !empty($searchByPaymentStatus) ? $searchByPaymentStatus[0] : '';

            $recordList = \App\Model\Fillship::exportallFillshipReportForSpecialOffer($param, 'export', $request->selected)->toArray();
            $shipmentData = array();
            $headers = array();
            if (!empty($recordList)) {
                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {

                            if ($count == 0) {
                                if ($key == 'id')
                                    $cellValue = 'ID';
                                elseif ($key == 'customer')
                                    $cellValue = 'Customer';
                                elseif ($key == 'destinationCityName')
                                    $cellValue = 'Destination City Name';
                                elseif ($key == 'destinationStateName')
                                    $cellValue = 'Destination State Name';
                                elseif ($key == 'destinationCountryName')
                                    $cellValue = 'Destination Country Name';
                                elseif ($key == 'createdOn')
                                    $cellValue = 'Date';
                                elseif ($key == 'totalcost')
                                    $cellValue = 'Cost';
                                elseif ($key == 'paymentstatus')
                                    $cellValue = 'Payment Status';
                                elseif ($key == 'totalDiscount')
                                    $cellValue = 'Discount Received';
                                elseif ($key == 'totalWeight')
                                    $cellValue = 'Total Weight';

                                $headers[$key] = $cellValue;
                            }
                        }
                    }
                }

                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {
                            if ($key == 'createdOn')
                                $value = \Carbon\Carbon::parse($value)->format('Y-m-d');

                            $shipmentData[$count + 1][$headers[$key]] = $value;
                        }
                    }
                }
            }

            ob_end_clean();
            ob_start();
            $excelName = "FillnShip-" . Carbon::now();
            Excel::create($excelName, function($excel) use($shipmentData) {
                $excel->sheet('Sheet 1', function($sheet) use($shipmentData) {
                    $sheet->fromArray($shipmentData);
                });
            })->store('xlsx', public_path('exports'));
            ob_flush();
            $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
            return response()->json(['path' => $creatingPath]);
        }
    }







    /**
     * This function used to generate and export All shipment records with coupon code
     * @param type $page
     * @param Request $request
     */
    public function exportallShipmentReportForSpecialOffer($page, Request $request) {

        $shipmentObj = new Shipment;
        $param = array();
        /* Initialize search field data */
        $searchShipmentArr = array(
            'idFrom' => '',
            'idTo' => '',
            'unitTo' => '',
            'unitFrom' => '',
            'shipmentStatus' => '',
            'prepaid' => '',
            'deliveryCompanyId' => '',
            'dispatchCompanyId' => '',
            'shipmentType' => '',
            'totalFrom' => '',
            'totalTo' => '',
            'user' => '',
            'receiver' => '',
            'paymentStatus' => '',
            'toCountry' => '',
            'warehouseId' => '',
            'labelType' => '',
            'itemType' => '',
        );
        $searchByCreatedOn = \Session::get('SHIPMENTDATA.searchByCreatedOn');
        $searchByDate = \Session::get('SHIPMENTDATA.searchByDate');
        $searchShipment = \Session::get('SHIPMENTDATA.searchShipment');

        $searchOffer = \Session::get('OFFERREPORT.searchOffer.0');
        $param['searchOffer'] = $searchOffer;

        $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
        $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
        $param['searchShipment'] = !empty($searchShipment) ? $searchShipment[0] : $searchShipmentArr;
        /* Get data to export */
        $exportData = $shipmentObj->exportDataSpecialOfferReport($param, array(), $request->selectall);
        #dd($exportData);

        $excelName = "Shipment-Data" . Carbon::now();
        ob_end_clean();
        ob_start();
        /* Generate excell */
        Excel::create($excelName, function($excel) use($exportData) {
            $excel->sheet('Sheet 1', function($sheet) use($exportData) {
                $sheet->fromArray($exportData['shipments']);
            });
        })->export('xlsx');
        ob_flush();
    }

    /**
     * This function used to generate and export Selected shipment records with coupon code
     * @param type $page
     * @param Request $request
     */
    public function exportselectedShipmentReportForSpecialOffer(Request $request) {

        $shipmentObj = new Shipment;
        /* Get data for export */
        $exportData = $shipmentObj->exportDataSpecialOfferReport(array(), $request->selected, $request->selectedField);
        //dd($exportData);
        //print_r($exportData);exit;

        $excelName = "Shipment-Data" . Carbon::now();
        ob_end_clean();
        ob_start();
        /* Generate Excel */
        Excel::create($excelName, function($excel) use($exportData) {
            $excel->sheet('Sheet 1', function($sheet) use($exportData) {
                $sheet->fromArray($exportData['shipments']);
            });
        })->store('xlsx', public_path('exports'));
        ob_flush();
        $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
        return response()->json(['path' => $creatingPath]);
    }




    
    public function referral(Request $request) {
        
        $param = array();
        $data = array();
        $dataList = array();
        $searchByDate = $searchByDate = $searchByCreatedOn = "";

        if (\Request::isMethod('post')) {
            $searchByCreatedOnOld = \Session::get('REFERRALREPORT.searchByCreatedOn');
            $searchByCreatedOnOld = !empty($searchByCreatedOnOld) ? $searchByCreatedOnOld[0]:"";
            $searchByCreatedOn = \Input::get('searchByCreatedOn', $searchByCreatedOnOld);
            
            $searchByDateOld = \Session::get('REFERRALREPORT.searchByDate');
            $searchByDateOld = !empty($searchByDateOld) ? $searchByDateOld[0]:"";
            $searchByDate = \Input::get('searchByDate', $searchByDateOld);
            
            $searchByUnitOld = \Session::get('REFERRALREPORT.searchByUserUnit');
            $searchByUnitOld = !empty($searchByUnitOld) ? $searchByUnitOld[0]:"";
            $searchByUnit = \Input::get('searchByUserUnit', $searchByUnitOld);
            
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = (!empty(\Input::get('field'))) ? \Input::get('field') : 'paidAmount';
            $type = (!empty(\Input::get('type'))) ? \Input::get('type') : 'desc';;
            \Session::forget('REFERRALREPORT');
            \Session::push('REFERRALREPORT.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('REFERRALREPORT.searchByDate', $searchByDate);
            \Session::push('REFERRALREPORT.searchByUnit', $searchByUnit);
            \Session::push('REFERRALREPORT.searchDisplay', $searchDisplay);
            \Session::push('REFERRALREPORT.field', $field);
            \Session::push('REFERRALREPORT.type', $type);
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchByUnit'] = $searchByUnit;
            $param['searchDisplay'] = $searchDisplay;
            $param['field'] = $field;
            $param['type'] = $type;
        }
        else
        {
            $sortField = \Session::get('REFERRALREPORT.field');
            $sortType = \Session::get('REFERRALREPORT.type');
            $searchByCreatedOn = \Session::get('REFERRALREPORT.searchByCreatedOn');
            $searchByDate = \Session::get('REFERRALREPORT.searchByDate');
            $searchByUnit = \Session::get('REFERRALREPORT.searchByUnit');
            $searchDisplay = \Session::get('REFERRALREPORT.searchDisplay');
            $param['field'] = !empty($sortField) ? $sortField[0] : 'paidAmount';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0]:"";
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0]:"";
            $param['searchByUnit'] = !empty($searchByUnit) ? $searchByUnit[0]:"";
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }
        
        /* BUILD SORTING ARRAY */
        $sort = array(
            'paidAmount' => array('current' => 'sorting'),
            'numShipments' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';
        
        if($param['searchByDate']!="" || $param['searchByUnit']!="" || $param['searchByCreatedOn']!="")
        {
            $dataList = User::getReferralReportData($param);
            $data['page'] = $dataList->currentPage();
        }
        
        $data['param'] = $param;
        $data['sort'] = $sort;
        $data['dataList'] = $dataList;
        
        $data['title'] = "Administrative Panel :: Referral Report";
        $data['contentTop'] = array('breadcrumbText' => 'Referral Report', 'contentTitle' => 'Referral Report', 'pageInfo' => 'This section allows you to view referral report');
        $data['pageTitle'] = "Referral Report";
        
        return view('Administrator.report.referralreport', $data);
    }
    
    public function exportallreferral() {
        
        $dataList = array();
        $sortField = \Session::get('REFERRALREPORT.field');
        $sortType = \Session::get('REFERRALREPORT.type');
        $searchByCreatedOn = \Session::get('REFERRALREPORT.searchByCreatedOn');
        $searchByDate = \Session::get('REFERRALREPORT.searchByDate');
        $searchByUnit = \Session::get('REFERRALREPORT.searchByUnit');
        $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0]:"";
        $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0]:"";
        $param['searchByUnit'] = !empty($searchByUnit) ? $searchByUnit[0]:"";
        $param['field'] = !empty($sortField) ? $sortField[0] : 'paidAmount';
        $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
        
        $exportData = User::referralExport($param);

        //print_r($dataList);exit;
        //$dataList = $dataList->toArray();
        $excelName = "Regerral-Data" . Carbon::now();

        ob_end_clean();
        ob_start();

        /* Generate excell */

        Excel::create($excelName, function($excel) use($exportData) {
            $excel->sheet('Sheet 1', function($sheet) use($exportData) {
                $sheet->fromArray($exportData);
            });
        })->store('xlsx', public_path('exports'));

        ob_flush();
         $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
        return response()->json(['path' => $creatingPath]);
    }

    public function customerreport(Request $request) {
        $data =  $param = $headers =  $records = array();
        $dataList = array();
        $searchByDate = $searchByDate = $searchByCreatedOn =  $searchByUnit =  "";

        $searchCustomerArr = array(
            'createdOn' => 'all',
            'searchByDate' => '',
            'toCountry' => '',
            'toState' => '',
            'toCity' => '',
        );

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchByCondition = \Input::get('searchByCondition', '');
            $searchCustomer = \Input::get('searchCustomer', $searchCustomerArr);
            $searchDisplay = \Input::get('searchDisplay', 1000);

            $sessionSearchByCondition = \Session::get('CUSTOMERREPORTDATA.searchByCondition');

            if($searchByCondition == $sessionSearchByCondition[0]){
                $field = \Input::get('field', 'users.createdOn');
                $type = \Input::get('type', 'desc');
            } else {
                $field = 'users.createdOn';
                $type = 'desc';
            }            

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('CUSTOMERREPORTDATA');
            \Session::push('CUSTOMERREPORTDATA.searchByCondition', $searchByCondition);
            \Session::push('CUSTOMERREPORTDATA.searchDisplay', $searchDisplay);
            \Session::push('CUSTOMERREPORTDATA.searchCustomer', $searchCustomer);
            \Session::push('CUSTOMERREPORTDATA.field', $field);
            \Session::push('CUSTOMERREPORTDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCondition'] = $searchByCondition;
            $param['searchCustomer'] = $searchCustomer;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('CUSTOMERREPORTDATA.field');
            $sortType = \Session::get('CUSTOMERREPORTDATA.type');
            $searchByCondition = \Session::get('CUSTOMERREPORTDATA.searchByCondition');
            $searchCustomer = \Session::get('CUSTOMERREPORTDATA.searchCustomer');
            $searchDisplay = \Session::get('CUSTOMERREPORTDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'users.createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCondition'] = !empty($searchByCondition) ? $searchByCondition[0] : '';
            $param['searchCustomer'] = !empty($searchCustomer) ? $searchCustomer[0] : $searchCustomerArr;
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : 1000;
        }

        if(isset($param['searchByCondition']) && !empty($param['searchByCondition'])) {

             if($param['searchByCondition'] == 'numberofshipment'){
                 $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company'  => 'Company',
                    'shippingMethod' => 'Shipping Method',
                    'createdOn' => 'Created On',
                    'numberofshipment'  => 'Number of Shipments',
                 );

                 /* BUILD SORTING ARRAY */
                $sort = array(
                    'unit' => array('current' => 'sorting'),
                    'name' => array('current' => 'sorting'),
                    'email' => array('current' => 'sorting'),
                    'contactNumber' => array('current' => 'sorting'),
                    'company' => array('current' => 'sorting'),
                    'shippingMethod' => array('current' => 'sorting'),
                    'createdOn' => array('current' => 'sorting'),
                    'numberofshipment' => array('current' => 'sorting'),
                );

                $records = User::getCustomerShippingMethodData($param);
            }
            elseif($param['searchByCondition'] == 'shipmentweight'){
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company'  => 'Company',
                    'totalWeight' => 'Shipment Weight',
                    'createdOn' => 'Created On',
                 );

                 /* BUILD SORTING ARRAY */
                $sort = array(
                    'unit' => array('current' => 'sorting'),
                    'name' => array('current' => 'sorting'),
                    'email' => array('current' => 'sorting'),
                    'contactNumber' => array('current' => 'sorting'),
                    'company' => array('current' => 'sorting'),
                    'totalWeight' => array('current' => 'sorting'),
                    'createdOn' => array('current' => 'sorting'),
                );
              
                 $records = User::getCustomerShipmentWeightData($param);
            }
            elseif($param['searchByCondition'] == 'destinationcity'){
                 $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company'  => 'Company',
                    'toCountry' => 'Destination Country',
                    'toState' => 'Destination State',
                    'toCity' => 'Destination City',
                    'numberofshipment'  => 'Number of Shipments',
                 );

                 /* BUILD SORTING ARRAY */
                $sort = array(
                    'unit' => array('current' => 'sorting'),
                    'name' => array('current' => 'sorting'),
                    'email' => array('current' => 'sorting'),
                    'contactNumber' => array('current' => 'sorting'),
                    'company' => array('current' => 'sorting'),
                    'toCountry' => array('current' => 'sorting'),
                    'toState' => array('current' => 'sorting'),
                    'toCity' => array('current' => 'sorting'),
                    'numberofshipment' => array('current' => 'sorting'),
                );

                $records = User::getCustomerDestinationData($param);
            }
           elseif($param['searchByCondition'] == 'registrationdate'){
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company'  => 'Company',
                    'createdOn' => 'Registered On',
                    'registeredBy' => 'Registered From',
                 );


                 /* BUILD SORTING ARRAY */
                $sort = array(
                    'unit' => array('current' => 'sorting'),
                    'name' => array('current' => 'sorting'),
                    'email' => array('current' => 'sorting'),
                    'contactNumber' => array('current' => 'sorting'),
                    'company' => array('current' => 'sorting'),
                    'createdOn' => array('current' => 'sorting'),
                    'registeredBy' => array('current' => 'sorting'),
                );

                $records = User::getCustomerRegistrationData($param);
            } elseif($param['searchByCondition'] == 'numberoforder'){
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company'  => 'Company',
                    'numberoforder' => 'Number Of Orders'
                 );


                 /* BUILD SORTING ARRAY */
                $sort = array(
                    'unit' => array('current' => 'sorting'),
                    'name' => array('current' => 'sorting'),
                    'email' => array('current' => 'sorting'),
                    'contactNumber' => array('current' => 'sorting'),
                    'company' => array('current' => 'sorting'),
                    'numberoforder' => array('current' => 'sorting'),
                );

                $records = User::getCustomerOrderNumData($param);
            } elseif($param['searchByCondition'] == 'orderdate'){
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company'  => 'Company',
                    'shipmentId' => 'Shipment ID',
                    'orderNumber' => 'Order Number',
                    'createdDate' => 'Last Order Date'
                 );


                 /* BUILD SORTING ARRAY */
                $sort = array(
                    'unit' => array('current' => 'sorting'),
                    'name' => array('current' => 'sorting'),
                    'email' => array('current' => 'sorting'),
                    'contactNumber' => array('current' => 'sorting'),
                    'company' => array('current' => 'sorting'),
                    'shipmentId' =>  array('current' => 'sorting'),
                    'orderNumber' => array('current' => 'sorting'),
                    'createdDate' => array('current' => 'sorting'),
                );

                $records = User::getCustomerOrderDateData($param);
            } elseif($param['searchByCondition'] == 'spentamount'){
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company'  => 'Company',
                    'paidAmount' => 'Spent Amount',
                 );


                 /* BUILD SORTING ARRAY */
                $sort = array(
                    'unit' => array('current' => 'sorting'),
                    'name' => array('current' => 'sorting'),
                    'email' => array('current' => 'sorting'),
                    'contactNumber' => array('current' => 'sorting'),
                    'company' => array('current' => 'sorting'),
                    'paidAmount' =>  array('current' => 'sorting'),
                );

                $records = User::getCustomerSpendingData($param);
                
            } elseif($param['searchByCondition'] == 'procurementshipmenttype'){
                
                if($param['searchCustomer']['servicetype'] == 'shipment')
                {
                    $headers = array(
                        'unit' => 'Unit Number',
                        'name' => 'Name',
                        'email' => 'Email',
                        'contactNumber' => 'Contact',
                        'company'  => 'Company',
                        'shipmentId' => 'Shipment ID',
                        'orderNumber' => 'Order Number',
                        'createdDate' => 'Created On'
                    );
                    
                    /* BUILD SORTING ARRAY */
                    $sort = array(
                        'unit' => array('current' => 'sorting'),
                        'name' => array('current' => 'sorting'),
                        'email' => array('current' => 'sorting'),
                        'contactNumber' => array('current' => 'sorting'),
                        'company' => array('current' => 'sorting'),
                        'shipmentId' =>  array('current' => 'sorting'),
                        'orderNumber' => array('current' => 'sorting'),
                        'createdDate' => array('current' => 'sorting'),
                    );
                    
                }
                else if($param['searchCustomer']['servicetype'] == 'shop_for_me')
                {
                    $headers = array(
                        'unit' => 'Unit Number',
                        'name' => 'Name',
                        'email' => 'Email',
                        'contactNumber' => 'Contact',
                        'company'  => 'Company',
                        'shipmentId' => 'Procurement ID',
                        'procurementType' => 'Type',
                        'createdDate' => 'Created On'
                    );
                    
                    /* BUILD SORTING ARRAY */
                    $sort = array(
                        'unit' => array('current' => 'sorting'),
                        'name' => array('current' => 'sorting'),
                        'email' => array('current' => 'sorting'),
                        'contactNumber' => array('current' => 'sorting'),
                        'company' => array('current' => 'sorting'),
                        'shipmentId' =>  array('current' => 'sorting'),
                        'procurementType' =>  array('current' => 'sorting'),
                        'createdDate' => array('current' => 'sorting'),
                    );
                }
                else if($param['searchCustomer']['servicetype'] == 'auto')
                {
                    $headers = array(
                        'unit' => 'Unit Number',
                        'name' => 'Name',
                        'email' => 'Email',
                        'contactNumber' => 'Contact',
                        'company'  => 'Company',
                        'shipmentId' => 'Shipment ID',
                        'orderNumber' => 'Order Number',
                        'createdDate' => 'Created On'
                    );
                    
                    /* BUILD SORTING ARRAY */
                    $sort = array(
                        'unit' => array('current' => 'sorting'),
                        'name' => array('current' => 'sorting'),
                        'email' => array('current' => 'sorting'),
                        'contactNumber' => array('current' => 'sorting'),
                        'company' => array('current' => 'sorting'),
                        'shipmentId' =>  array('current' => 'sorting'),
                        'orderNumber' => array('current' => 'sorting'),
                        'createdDate' => array('current' => 'sorting'),
                    );
                }

                $records = User::getCustomerServiceType($param);
            } elseif($param['searchByCondition'] == 'customertotalspent') {
                
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company'  => 'Company',
                    'shipmentId' => 'Shipment ID',
                    'type' => 'Type',
                    'amountPaid' => 'Amount Spent',
                    'createdOn' => 'Created On'
                );
                
                /* BUILD SORTING ARRAY */
                $sort = array();
                
                $records = User::getCustomerLifetimeData($param);
            }
            
        }

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        $dataList['headers'] = $headers;
        $dataList['records'] = $records;

        $data['sort'] = $sort;
        $data['searchData'] = $param;
        $data['dataList'] = $dataList;
        $data['title'] = "Administrative Panel :: Customer Reports";
        $data['contentTop'] = array('breadcrumbText' => 'Customer Reports', 'contentTitle' => 'Customer Reports', 'pageInfo' => 'This section allows you to view customer reports');
        $data['pageTitle'] = "Customer Reports";
        
        return view('Administrator.report.customerreport', $data);

    }

    public function getcustomersearchcondition($type = ''){
        $data = array();

        $data['type'] = $type;

        $searchCustomerArr = array(
            'createdOn' => 'all',
            'searchByDate' => '',
            'toCountry' => '',
            'toState' => '',
            'toCity' => '',
            'shippingMethodId' => '',
        );

        $searchCustomer = \Session::get('CUSTOMERREPORTDATA.searchCustomer');
        $param['searchCustomer'] = !empty($searchCustomer) ? $searchCustomer[0] : $searchCustomerArr;


       /* FETCH COUNTRY LIST  */
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

        /* FETCH SHIPPING METHOD LIST  */
        $data['shippingMethodList'] = Shippingmethods::select('shippingid','shipping')->where('deleted', '0')->orderby('shipping', 'asc')->get();


        $data['searchData'] = $param;

       return view('Administrator.report.customersearchcondition', $data);
    }


    public function accountsreport(Request $request)
    {

        $data =  $param = $headers =  $records = array();
        $dataList = array();
        $searchByDate = $searchByEmail =  $searchByCondition = "";

        $searchAccountArr = array(
            'createdOn' => 'all',
            'searchByDate' => '',
            'tabType'=> 'shipment',
            'accountType'=>'paid'
        );

         if (\Request::isMethod('post')) {
            /* GET POST VALUE  */         

            $searchByCondition = \Input::get('searchByCondition', '');
            $searchAccount = \Input::get('searchAccount', $searchAccountArr);
            $searchDisplay = \Input::get('searchDisplay', 1000);

            $sessionSearchByCondition = \Session::get('ACCOUNTREPORTDATA.searchByCondition');

            if($searchByCondition == $sessionSearchByCondition[0]){
                $field = \Input::get('field', 'shipments.createdOn');
                $type = \Input::get('type', 'desc');
            } else {
                $field = 'shipments.createdOn';
                $type = 'desc';
            }            

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('ACCOUNTREPORTDATA');
            \Session::push('ACCOUNTREPORTDATA.searchByCondition', $searchByCondition);
            \Session::push('ACCOUNTREPORTDATA.searchDisplay', $searchDisplay);
            \Session::push('ACCOUNTREPORTDATA.searchAccount', $searchAccount);
            \Session::push('ACCOUNTREPORTDATA.field', $field);
            \Session::push('ACCOUNTREPORTDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCondition'] = $searchByCondition;
            $param['searchAccount'] = $searchAccount;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('ACCOUNTREPORTDATA.field');
            $sortType = \Session::get('ACCOUNTREPORTDATA.type');
            $searchByCondition = \Session::get('ACCOUNTREPORTDATA.searchByCondition');
            $searchAccount = \Session::get('ACCOUNTREPORTDATA.searchAccount');
            $searchDisplay = \Session::get('ACCOUNTREPORTDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'users.createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCondition'] = !empty($searchByCondition) ? $searchByCondition[0] : 'allshipment';
            $param['searchAccount'] = !empty($searchAccount) ? $searchAccount[0] : $searchAccountArr;
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : 1000;
        }

       

       //print_r($param['searchAccount']); die;

        /* FETCH USER LIST  */
         if(isset($param['searchByCondition']) && !empty($param['searchByCondition'])) {

            
             if($param['searchByCondition'] == 'allshipment'){

                if(isset($param['searchAccount']['accountType']) && !empty($param['searchAccount']['accountType']))
                {
                    if($param['searchAccount']['accountType'] == "paid"){
                         $headers = array(
                            'id' => 'Shipment#',
                            'unit' => 'Unit Number',
                            'name' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city'=> 'City',
                            'state'=> 'State',
                            'country'=> 'Country',
                            'createdOn' => 'Created On',
                            'paymentStatus' => 'Payment Status',                    
                            'totalCost'  => 'Cost of Shipments',
                            'processor' => 'Processor',
                            'paymentMethod'  => 'Payment Method',
                            'paymentDate'  => 'Payment Date',
                         );

                         /* BUILD SORTING ARRAY */
                        $sort = array(
                            'id' => array('current' => 'sorting'),
                            'unit' => array('current' => 'sorting'),
                            'name' => array('current' => 'sorting'),
                            'email' => array('current' => 'sorting'),
                            'contactNumber' => array('current' => 'sorting'),
                            'city' => array('current' => 'sorting'),
                            'state' => array('current' => 'sorting'),
                            'country' => array('current' => 'sorting'),
                            'createdOn' => array('current' => 'sorting'),                    
                            'paymentStatus' => array('current' => 'sorting'),
                            'totalCost' => array('current' => 'sorting'),
                            'processor' => array('current' => 'sorting'),
                            'paymentMethod' => array('current' => 'sorting'),
                            'paymentDate' => array('current' => 'sorting'),
                        );

                        $records = Shipment::getallShipmentData($param);
                    }else{

                        $headers = array(
                            'id' => 'Shipment#',
                            'unit' => 'Unit Number',
                            'name' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city'=> 'City',
                            'state'=> 'State',
                            'country'=> 'Country',
                            'createdOn' => 'Created On',
                            'paymentStatus' => 'Payment Status',                    
                            'totalCost'  => 'Cost of Shipments',
                         );

                         /* BUILD SORTING ARRAY */
                        $sort = array(
                            'id' => array('current' => 'sorting'),
                            'unit' => array('current' => 'sorting'),
                            'name' => array('current' => 'sorting'),
                            'email' => array('current' => 'sorting'),
                            'contactNumber' => array('current' => 'sorting'),
                            'city' => array('current' => 'sorting'),
                            'state' => array('current' => 'sorting'),
                            'country' => array('current' => 'sorting'),
                            'createdOn' => array('current' => 'sorting'),                    
                            'paymentStatus' => array('current' => 'sorting'),
                            'totalCost' => array('current' => 'sorting'),
                        );

                        $records = Shipment::getallShipmentData($param);

                    }
                }else{
                     $headers = array(
                            'id' => 'Shipment#',
                            'unit' => 'Unit Number',
                            'name' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city'=> 'City',
                            'state'=> 'State',
                            'country'=> 'Country',
                            'createdOn' => 'Created On',
                            'paymentStatus' => 'Payment Status',                    
                            'totalCost'  => 'Cost of Shipments',
                            'processor' => 'Processor',
                            'paymentMethod'  => 'Payment Method',
                            'paymentDate'  => 'Payment Date',
                         );

                         /* BUILD SORTING ARRAY */
                        $sort = array(
                            'id' => array('current' => 'sorting'),
                            'unit' => array('current' => 'sorting'),
                            'name' => array('current' => 'sorting'),
                            'email' => array('current' => 'sorting'),
                            'contactNumber' => array('current' => 'sorting'),
                            'city' => array('current' => 'sorting'),
                            'state' => array('current' => 'sorting'),
                            'country' => array('current' => 'sorting'),
                            'createdOn' => array('current' => 'sorting'),                    
                            'paymentStatus' => array('current' => 'sorting'),
                            'totalCost' => array('current' => 'sorting'),
                            'processor' => array('current' => 'sorting'),
                            'paymentMethod' => array('current' => 'sorting'),
                            'paymentDate' => array('current' => 'sorting'),
                        );

                        $records = Shipment::getallShipmentData($param);

                }

            }elseif($param['searchByCondition'] == 'withstoragecharge'){

                if(isset($param['searchAccount']['accountType']) && !empty($param['searchAccount']['accountType']))
                {
                    if($param['searchAccount']['accountType'] == "paid"){
                        $headers = array(
                            'id' => 'Shipment#',
                            'unit' => 'Unit Number',
                            'name' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city'=> 'City',
                            'state'=> 'State',
                            'country'=> 'Country',
                            'createdOn' => 'Created On',
                            'maxStorageDate' => 'Storage Expiry Date',
                            'storageCharge' => 'Storage Charge',
                            'totalCost'  => 'Cost of Shipments',
                            'processor' => 'Processor',
                            'paymentMethod'  => 'Payment Method',
                            'paymentDate'  => 'Payment Date',
                            
                         );

                         /* BUILD SORTING ARRAY */
                        $sort = array(
                            'id' => array('current' => 'sorting'),
                            'unit' => array('current' => 'sorting'),
                            'name' => array('current' => 'sorting'),
                            'email' => array('current' => 'sorting'),
                            'contactNumber' => array('current' => 'sorting'),
                            'city' => array('current' => 'sorting'),
                            'state' => array('current' => 'sorting'),
                            'country' => array('current' => 'sorting'),
                            'createdOn' => array('current' => 'sorting'),
                            'maxStorageDate' => array('current' => 'sorting'),
                            'storageCharge' => array('current' => 'sorting'),
                            'totalCost' => array('current' => 'sorting'),
                            'processor' => array('current' => 'sorting'),
                            'paymentMethod' => array('current' => 'sorting'),
                            'paymentDate' => array('current' => 'sorting'),
                        );
                      
                         $records = Shipment::getallShipmentDatawithStorage($param);
                    }else{
                        $headers = array(
                            'id' => 'Shipment#',
                            'unit' => 'Unit Number',
                            'name' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city'=> 'City',
                            'state'=> 'State',
                            'country'=> 'Country',
                            'createdOn' => 'Created On',
                            'maxStorageDate' => 'Storage Expiry Date',
                            'storageCharge' => 'Storage Charge',
                            'totalCost'  => 'Cost of Shipments',
                            
                         );

                         /* BUILD SORTING ARRAY */
                        $sort = array(
                            'id' => array('current' => 'sorting'),
                            'unit' => array('current' => 'sorting'),
                            'name' => array('current' => 'sorting'),
                            'email' => array('current' => 'sorting'),
                            'contactNumber' => array('current' => 'sorting'),
                            'city' => array('current' => 'sorting'),
                            'state' => array('current' => 'sorting'),
                            'country' => array('current' => 'sorting'),
                            'createdOn' => array('current' => 'sorting'),
                            'maxStorageDate' => array('current' => 'sorting'),
                            'storageCharge' => array('current' => 'sorting'),
                            'totalCost' => array('current' => 'sorting'),
                            
                        );
                      
                         $records = Shipment::getallShipmentDatawithStorage($param);

                    }
                }else{
                    $headers = array(
                        'id' => 'Shipment#',
                        'unit' => 'Unit Number',
                        'name' => 'Name',
                        'email' => 'Email',
                        'contactNumber' => 'Contact',
                        'city'=> 'City',
                        'state'=> 'State',
                        'country'=> 'Country',
                        'createdOn' => 'Created On',
                        'maxStorageDate' => 'Storage Expiry Date',
                        'storageCharge' => 'Storage Charge',
                        'totalCost'  => 'Cost of Shipments',
                        'processor' => 'Processor',
                        'paymentMethod'  => 'Payment Method',
                        'paymentDate'  => 'Payment Date',
                     );

                     /* BUILD SORTING ARRAY */
                    $sort = array(
                        'id' => array('current' => 'sorting'),
                        'unit' => array('current' => 'sorting'),
                        'name' => array('current' => 'sorting'),
                        'email' => array('current' => 'sorting'),
                        'contactNumber' => array('current' => 'sorting'),
                        'city' => array('current' => 'sorting'),
                        'state' => array('current' => 'sorting'),
                        'country' => array('current' => 'sorting'),
                        'createdOn' => array('current' => 'sorting'),
                        'maxStorageDate' => array('current' => 'sorting'),
                        'storageCharge' => array('current' => 'sorting'),
                        'totalCost' => array('current' => 'sorting'),
                        'processor' => array('current' => 'sorting'),
                        'paymentMethod' => array('current' => 'sorting'),
                        'paymentDate' => array('current' => 'sorting'),
                    );
                  
                     $records = Shipment::getallShipmentDatawithStorage($param);

                }

                
            }elseif($param['searchByCondition'] == 'shopforme')
            {
               
                if(isset($param['searchAccount']['accountType']) && !empty($param['searchAccount']['accountType']))
                {
                    if($param['searchAccount']['accountType'] == "paid"){

                        $headers = array(
                            'id' => 'Shipment#',
                            'unit' => 'Unit Number',
                            'name' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city'=> 'City',
                            'state'=> 'State',
                            'country'=> 'Country',
                            'createdOn' => 'Created On',
                            'totalItemCost'  => 'Cost of Items',
                            'totalProcessingFee'  => 'Processing Fees',
                            'totalShippingCost'  => 'Total Shipping Cost',
                            'totalProcurementCost'  => 'Shop for me Cost',
                            'totalCost'  => 'Total Cost',
                            'processor'  => 'Processor',
                            'paymentMethod'  => 'Payment Method',
                            'paymentDate'  => 'Payment Date',
                         );

                         /* BUILD SORTING ARRAY */
                        $sort = array(
                            'id' => array('current' => 'sorting'),
                            'unit' => array('current' => 'sorting'),
                            'name' => array('current' => 'sorting'),
                            'email' => array('current' => 'sorting'),
                            'contactNumber' => array('current' => 'sorting'),
                            'city' => array('current' => 'sorting'),
                            'state' => array('current' => 'sorting'),
                            'country' => array('current' => 'sorting'),
                            'createdOn' => array('current' => 'sorting'),
                            'totalItemCost' => array('current' => 'sorting'),
                            'totalProcessingFee' => array('current' => 'sorting'),
                            'totalShippingCost' => array('current' => 'sorting'),
                            'totalProcurementCost' => array('current' => 'sorting'),
                            'totalCost' => array('current' => 'sorting'),
                            'processor' => array('current' => 'sorting'),
                            'paymentMethod' => array('current' => 'sorting'),
                            'paymentDate' => array('current' => 'sorting'),
                        );
                      
                         $records = Shipment::getallShipmentDataShopforme($param);


                    }else{
                        $headers = array(
                            'id' => 'Shipment#',
                            'unit' => 'Unit Number',
                            'name' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city'=> 'City',
                            'state'=> 'State',
                            'country'=> 'Country',
                            'createdOn' => 'Created On',
                            'totalItemCost'  => 'Cost of Items',
                            'totalProcessingFee'  => 'Processing Fees',
                            'totalShippingCost'  => 'Total Shipping Cost',
                            'totalProcurementCost'  => 'Shop for me Cost',
                            'totalCost'  => 'Total Cost'
                         );

                         /* BUILD SORTING ARRAY */
                        $sort = array(
                            'id' => array('current' => 'sorting'),
                            'unit' => array('current' => 'sorting'),
                            'name' => array('current' => 'sorting'),
                            'email' => array('current' => 'sorting'),
                            'contactNumber' => array('current' => 'sorting'),
                            'city' => array('current' => 'sorting'),
                            'state' => array('current' => 'sorting'),
                            'country' => array('current' => 'sorting'),
                            'createdOn' => array('current' => 'sorting'),
                            'totalItemCost' => array('current' => 'sorting'),
                            'totalProcessingFee' => array('current' => 'sorting'),
                            'totalShippingCost' => array('current' => 'sorting'),
                            'totalProcurementCost' => array('current' => 'sorting'),
                            'totalCost' => array('current' => 'sorting')
                        );
                      
                         $records = Shipment::getallShipmentDataShopforme($param);
                    }


                }else{
                        $headers = array(
                        'id' => 'Shipment#',
                        'unit' => 'Unit Number',
                        'name' => 'Name',
                        'email' => 'Email',
                        'contactNumber' => 'Contact',
                        'city'=> 'City',
                        'state'=> 'State',
                        'country'=> 'Country',
                        'createdOn' => 'Created On',
                        'totalItemCost'  => 'Cost of Items',
                        'totalProcessingFee'  => 'Processing Fees',
                        'totalShippingCost'  => 'Total Shipping Cost',
                        'totalProcurementCost'  => 'Shop for me Cost',
                        'totalCost'  => 'Total Cost',
                        'processor'  => 'Processor',
                        'paymentMethod'  => 'Payment Method',
                        'paymentDate'  => 'Payment Date',
                     );

                     /* BUILD SORTING ARRAY */
                    $sort = array(
                        'id' => array('current' => 'sorting'),
                        'unit' => array('current' => 'sorting'),
                        'name' => array('current' => 'sorting'),
                        'email' => array('current' => 'sorting'),
                        'contactNumber' => array('current' => 'sorting'),
                        'city' => array('current' => 'sorting'),
                        'state' => array('current' => 'sorting'),
                        'country' => array('current' => 'sorting'),
                        'createdOn' => array('current' => 'sorting'),
                        'totalItemCost' => array('current' => 'sorting'),
                        'totalProcessingFee' => array('current' => 'sorting'),
                        'totalShippingCost' => array('current' => 'sorting'),
                        'totalProcurementCost' => array('current' => 'sorting'),
                        'totalCost' => array('current' => 'sorting'),
                        'processor' => array('current' => 'sorting'),
                        'paymentMethod' => array('current' => 'sorting'),
                        'paymentDate' => array('current' => 'sorting'),
                    );
                  
                     $records = Shipment::getallShipmentDataShopforme($param);
                }


            }elseif($param['searchByCondition'] == 'autopart')
            {

                if(isset($param['searchAccount']['accountType']) && !empty($param['searchAccount']['accountType']))
                {
                    if($param['searchAccount']['accountType'] == "paid"){

                          $headers = array(
                            'id' => 'Shipment#',
                            'unit' => 'Unit Number',
                            'name' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city'=> 'City',
                            'state'=> 'State',
                            'country'=> 'Country',
                            'createdOn' => 'Created On',
                            'totalItemCost'  => 'Cost of Items',
                            'totalProcessingFee'  => 'Processing Fees',
                            'totalShippingCost'  => 'Total Shipping Cost',
                            'totalProcurementCost'  => 'Auto Parts Cost',
                            'totalCost'  => 'Total Cost',
                            'processor'  => 'Processor',
                            'paymentMethod'  => 'Payment Method',
                            'paymentDate'  => 'Payment Date',
                         );

                         /* BUILD SORTING ARRAY */
                        $sort = array(
                            'id' => array('current' => 'sorting'),
                            'unit' => array('current' => 'sorting'),
                            'name' => array('current' => 'sorting'),
                            'email' => array('current' => 'sorting'),
                            'contactNumber' => array('current' => 'sorting'),
                            'city' => array('current' => 'sorting'),
                            'state' => array('current' => 'sorting'),
                            'country' => array('current' => 'sorting'),
                            'createdOn' => array('current' => 'sorting'),
                            'totalItemCost' => array('current' => 'sorting'),
                            'totalProcessingFee' => array('current' => 'sorting'),
                            'totalShippingCost' => array('current' => 'sorting'),
                            'totalProcurementCost' => array('current' => 'sorting'),
                            'totalCost' => array('current' => 'sorting'),
                            'processor' => array('current' => 'sorting'),
                            'paymentMethod' => array('current' => 'sorting'),
                            'paymentDate' => array('current' => 'sorting'),
                        );
                      
                        $records = Shipment::getallShipmentDataAutoparts($param);

                    }else{

                          $headers = array(
                            'id' => 'Shipment#',
                            'unit' => 'Unit Number',
                            'name' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city'=> 'City',
                            'state'=> 'State',
                            'country'=> 'Country',
                            'createdOn' => 'Created On',
                            'totalItemCost'  => 'Cost of Items',
                            'totalProcessingFee'  => 'Processing Fees',
                            'totalShippingCost'  => 'Total Shipping Cost',
                            'totalProcurementCost'  => 'Auto Parts Cost',
                            'totalCost'  => 'Total Cost'
                         );

                         /* BUILD SORTING ARRAY */
                        $sort = array(
                            'id' => array('current' => 'sorting'),
                            'unit' => array('current' => 'sorting'),
                            'name' => array('current' => 'sorting'),
                            'email' => array('current' => 'sorting'),
                            'contactNumber' => array('current' => 'sorting'),
                            'city' => array('current' => 'sorting'),
                            'state' => array('current' => 'sorting'),
                            'country' => array('current' => 'sorting'),
                            'createdOn' => array('current' => 'sorting'),
                            'totalItemCost' => array('current' => 'sorting'),
                            'totalProcessingFee' => array('current' => 'sorting'),
                            'totalShippingCost' => array('current' => 'sorting'),
                            'totalProcurementCost' => array('current' => 'sorting'),
                            'totalCost' => array('current' => 'sorting')
                        );
                      
                        $records = Shipment::getallShipmentDataAutoparts($param);

                    }
                }else{
                      $headers = array(
                            'id' => 'Shipment#',
                            'unit' => 'Unit Number',
                            'name' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city'=> 'City',
                            'state'=> 'State',
                            'country'=> 'Country',
                            'createdOn' => 'Created On',
                            'totalItemCost'  => 'Cost of Items',
                            'totalProcessingFee'  => 'Processing Fees',
                            'totalShippingCost'  => 'Total Shipping Cost',
                            'totalProcurementCost'  => 'Auto Parts Cost',
                            'totalCost'  => 'Total Cost',
                            'processor'  => 'Processor',
                            'paymentMethod'  => 'Payment Method',
                            'paymentDate'  => 'Payment Date',
                         );

                         /* BUILD SORTING ARRAY */
                     $sort = array(
                            'id' => array('current' => 'sorting'),
                            'unit' => array('current' => 'sorting'),
                            'name' => array('current' => 'sorting'),
                            'email' => array('current' => 'sorting'),
                            'contactNumber' => array('current' => 'sorting'),
                            'city' => array('current' => 'sorting'),
                            'state' => array('current' => 'sorting'),
                            'country' => array('current' => 'sorting'),
                            'createdOn' => array('current' => 'sorting'),
                            'totalItemCost' => array('current' => 'sorting'),
                            'totalProcessingFee' => array('current' => 'sorting'),
                            'totalShippingCost' => array('current' => 'sorting'),
                            'totalProcurementCost' => array('current' => 'sorting'),
                            'totalCost' => array('current' => 'sorting'),
                            'processor' => array('current' => 'sorting'),
                            'paymentMethod' => array('current' => 'sorting'),
                            'paymentDate' => array('current' => 'sorting'),
                        );
                      
                    $records = Shipment::getallShipmentDataAutoparts($param);

                }
              

            }elseif($param['searchByCondition'] == 'buyacar')
            {
               if(isset($param['searchAccount']['accountType']) && !empty($param['searchAccount']['accountType']))
                {
                    if($param['searchAccount']['accountType'] == "paid"){
                        $headers = array(
                            'id' => 'Shipment#',
                            'unit' => 'Unit Number',
                            'name' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city'=> 'City',
                            'state'=> 'State',
                            'country'=> 'Country',
                            'createdOn' => 'Created On',
                            'totalItemCost'  => 'Cost of Items',
                            'totalProcessingFee'  => 'Processing Fees',
                            'totalShippingCost'  => 'Total Shipping Cost',
                            'totalProcurementCost'  => 'Buy a Car Cost',
                            'totalCost'  => 'Total Cost',
                            'processor'  => 'Processor',
                            'paymentMethod'  => 'Payment Method',
                            'paymentDate'  => 'Payment Date',
                         );

                         /* BUILD SORTING ARRAY */
                        $sort = array(
                            'id' => array('current' => 'sorting'),
                            'unit' => array('current' => 'sorting'),
                            'name' => array('current' => 'sorting'),
                            'email' => array('current' => 'sorting'),
                            'contactNumber' => array('current' => 'sorting'),
                            'city' => array('current' => 'sorting'),
                            'state' => array('current' => 'sorting'),
                            'country' => array('current' => 'sorting'),
                            'createdOn' => array('current' => 'sorting'),
                            'totalItemCost' => array('current' => 'sorting'),
                            'totalProcessingFee' => array('current' => 'sorting'),
                            'totalShippingCost' => array('current' => 'sorting'),
                            'totalProcurementCost' => array('current' => 'sorting'),
                            'totalCost' => array('current' => 'sorting'),
                            'processor' => array('current' => 'sorting'),
                            'paymentMethod' => array('current' => 'sorting'),
                            'paymentDate' => array('current' => 'sorting'),
                        );
                      
                         $records = Shipment::getallShipmentDataBuyacar($param);

                    }else{

                        $headers = array(
                            'id' => 'Shipment#',
                            'unit' => 'Unit Number',
                            'name' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city'=> 'City',
                            'state'=> 'State',
                            'country'=> 'Country',
                            'createdOn' => 'Created On',
                            'totalItemCost'  => 'Cost of Items',
                            'totalProcessingFee'  => 'Processing Fees',
                            'totalShippingCost'  => 'Total Shipping Cost',
                            'totalProcurementCost'  => 'Buy a Car Cost',
                            'totalCost'  => 'Total Cost'
                         );

                         /* BUILD SORTING ARRAY */
                        $sort = array(
                            'id' => array('current' => 'sorting'),
                            'unit' => array('current' => 'sorting'),
                            'name' => array('current' => 'sorting'),
                            'email' => array('current' => 'sorting'),
                            'contactNumber' => array('current' => 'sorting'),
                            'city' => array('current' => 'sorting'),
                            'state' => array('current' => 'sorting'),
                            'country' => array('current' => 'sorting'),
                            'createdOn' => array('current' => 'sorting'),
                            'totalItemCost' => array('current' => 'sorting'),
                            'totalProcessingFee' => array('current' => 'sorting'),
                            'totalShippingCost' => array('current' => 'sorting'),
                            'totalProcurementCost' => array('current' => 'sorting'),
                            'totalCost' => array('current' => 'sorting')
                        );
                      
                         $records = Shipment::getallShipmentDataBuyacar($param);

                    }
                }else{

                        $headers = array(
                            'id' => 'Shipment#',
                            'unit' => 'Unit Number',
                            'name' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city'=> 'City',
                            'state'=> 'State',
                            'country'=> 'Country',
                            'createdOn' => 'Created On',
                            'totalItemCost'  => 'Cost of Items',
                            'totalProcessingFee'  => 'Processing Fees',
                            'totalShippingCost'  => 'Total Shipping Cost',
                            'totalProcurementCost'  => 'Buy a Car Cost',
                            'totalCost'  => 'Total Cost',
                            'processor'  => 'Processor',
                            'paymentMethod'  => 'Payment Method',
                            'paymentDate'  => 'Payment Date',
                         );

                         /* BUILD SORTING ARRAY */
                        $sort = array(
                            'id' => array('current' => 'sorting'),
                            'unit' => array('current' => 'sorting'),
                            'name' => array('current' => 'sorting'),
                            'email' => array('current' => 'sorting'),
                            'contactNumber' => array('current' => 'sorting'),
                            'city' => array('current' => 'sorting'),
                            'state' => array('current' => 'sorting'),
                            'country' => array('current' => 'sorting'),
                            'createdOn' => array('current' => 'sorting'),
                            'totalItemCost' => array('current' => 'sorting'),
                            'totalProcessingFee' => array('current' => 'sorting'),
                            'totalShippingCost' => array('current' => 'sorting'),
                            'totalProcurementCost' => array('current' => 'sorting'),
                            'totalCost' => array('current' => 'sorting'),
                            'processor' => array('current' => 'sorting'),
                            'paymentMethod' => array('current' => 'sorting'),
                            'paymentDate' => array('current' => 'sorting'),
                        );
                      
                         $records = Shipment::getallShipmentDataBuyacar($param);

                }

                

            }elseif($param['searchByCondition'] == 'shipacar')
            {
                if(isset($param['searchAccount']['accountType']) && !empty($param['searchAccount']['accountType']))
                {
                    if($param['searchAccount']['accountType'] == "paid"){
                        $headers = array(
                            'id' => 'Shipment#',
                            'unit' => 'Unit Number',
                            'name' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city'=> 'City',
                            'state'=> 'State',
                            'country'=> 'Country',
                            'createdOn' => 'Created On',
                            'totalItemCost'  => 'Cost of Items',
                            'totalProcessingFee'  => 'Processing Fees',
                            'totalShippingCost'  => 'Total Shipping Cost',
                            'totalProcurementCost'  => 'Buy a Car Cost',
                            'totalCost'  => 'Total Cost',
                            'processor'  => 'Processor',
                            'paymentMethod'  => 'Payment Method',
                            'paymentDate'  => 'Payment Date',
                         );

                         /* BUILD SORTING ARRAY */
                        $sort = array(
                            'id' => array('current' => 'sorting'),
                            'unit' => array('current' => 'sorting'),
                            'name' => array('current' => 'sorting'),
                            'email' => array('current' => 'sorting'),
                            'contactNumber' => array('current' => 'sorting'),
                            'city' => array('current' => 'sorting'),
                            'state' => array('current' => 'sorting'),
                            'country' => array('current' => 'sorting'),
                            'createdOn' => array('current' => 'sorting'),
                            'totalItemCost' => array('current' => 'sorting'),
                            'totalProcessingFee' => array('current' => 'sorting'),
                            'totalShippingCost' => array('current' => 'sorting'),
                            'totalProcurementCost' => array('current' => 'sorting'),
                            'totalCost' => array('current' => 'sorting'),
                            'processor' => array('current' => 'sorting'),
                            'paymentMethod' => array('current' => 'sorting'),
                            'paymentDate' => array('current' => 'sorting'),
                        );
                      
                         $records = Shipment::getallShipmentDataShipacar($param);
                    }else{
                            $headers = array(
                                'id' => 'Shipment#',
                                'unit' => 'Unit Number',
                                'name' => 'Name',
                                'email' => 'Email',
                                'contactNumber' => 'Contact',
                                'city'=> 'City',
                                'state'=> 'State',
                                'country'=> 'Country',
                                'createdOn' => 'Created On',
                                'totalItemCost'  => 'Cost of Items',
                                'totalProcessingFee'  => 'Processing Fees',
                                'totalShippingCost'  => 'Total Shipping Cost',
                                'totalProcurementCost'  => 'Buy a Car Cost',
                                'totalCost'  => 'Total Cost'
                             );

                             /* BUILD SORTING ARRAY */
                            $sort = array(
                                'id' => array('current' => 'sorting'),
                                'unit' => array('current' => 'sorting'),
                                'name' => array('current' => 'sorting'),
                                'email' => array('current' => 'sorting'),
                                'contactNumber' => array('current' => 'sorting'),
                                'city' => array('current' => 'sorting'),
                                'state' => array('current' => 'sorting'),
                                'country' => array('current' => 'sorting'),
                                'createdOn' => array('current' => 'sorting'),
                                'totalItemCost' => array('current' => 'sorting'),
                                'totalProcessingFee' => array('current' => 'sorting'),
                                'totalShippingCost' => array('current' => 'sorting'),
                                'totalProcurementCost' => array('current' => 'sorting'),
                                'totalCost' => array('current' => 'sorting')
                            );
                          
                             $records = Shipment::getallShipmentDataShipacar($param);

                        }
                    }else{
                            $headers = array(
                                'id' => 'Shipment#',
                                'unit' => 'Unit Number',
                                'name' => 'Name',
                                'email' => 'Email',
                                'contactNumber' => 'Contact',
                                'city'=> 'City',
                                'state'=> 'State',
                                'country'=> 'Country',
                                'createdOn' => 'Created On',
                                'totalItemCost'  => 'Cost of Items',
                                'totalProcessingFee'  => 'Processing Fees',
                                'totalShippingCost'  => 'Total Shipping Cost',
                                'totalProcurementCost'  => 'Buy a Car Cost',
                                'totalCost'  => 'Total Cost',
                                'processor'  => 'Processor',
                                'paymentMethod'  => 'Payment Method',
                                'paymentDate'  => 'Payment Date',
                             );

                             /* BUILD SORTING ARRAY */
                            $sort = array(
                                'id' => array('current' => 'sorting'),
                                'unit' => array('current' => 'sorting'),
                                'name' => array('current' => 'sorting'),
                                'email' => array('current' => 'sorting'),
                                'contactNumber' => array('current' => 'sorting'),
                                'city' => array('current' => 'sorting'),
                                'state' => array('current' => 'sorting'),
                                'country' => array('current' => 'sorting'),
                                'createdOn' => array('current' => 'sorting'),
                                'totalItemCost' => array('current' => 'sorting'),
                                'totalProcessingFee' => array('current' => 'sorting'),
                                'totalShippingCost' => array('current' => 'sorting'),
                                'totalProcurementCost' => array('current' => 'sorting'),
                                'totalCost' => array('current' => 'sorting'),
                                'processor' => array('current' => 'sorting'),
                                'paymentMethod' => array('current' => 'sorting'),
                                'paymentDate' => array('current' => 'sorting'),
                            );
                      
                         $records = Shipment::getallShipmentDataShipacar($param);

                }
                

            }
            elseif($param['searchByCondition'] == 'valueofitem')
            {
                if(isset($param['searchAccount']['accountType']) && !empty($param['searchAccount']['accountType']))
                {
                    if($param['searchAccount']['accountType'] == "paid"){
                        $headers = array(
                            'id' => 'Shipment#',
                            'unit' => 'Unit Number',
                            'customer' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city'=> 'City',
                            'state'=> 'State',
                            'country'=> 'Country',
                            'createdOn' => 'Created On',
                            'numItems' => 'Number of Items',
                            'totalShippingCost'=> 'Total Shipping Cost',
                            'itemTotalCost' => 'Item Total Cost',
                            'processor'  => 'Processor',
                            'paymentMethod'  => 'Payment Method',
                            'paymentDate'  => 'Payment Date',
                        );
                        
                        $sort = array(
                            'id' => array('current' => 'sorting'),
                            'unit' => array('current' => 'sorting'),
                            'customer' => array('current' => 'sorting'),
                            'email' => array('current' => 'sorting'),
                            'contactNumber' => array('current' => 'sorting'),
                            'city'=> array('current' => 'sorting'),
                            'state'=> array('current' => 'sorting'),
                            'country'=> array('current' => 'sorting'),
                            'createdOn' => array('current' => 'sorting'),
                            'numItems' => array('current' => 'sorting'),
                            'totalShippingCost' => array('current' => 'sorting'),
                            'itemTotalCost' => array('current' => 'sorting'),
                            'processor' => array('current' => 'sorting'),
                            'paymentMethod' => array('current' => 'sorting'),
                            'paymentDate' => array('current' => 'sorting'),
                        );
                        
                        $records = Shipment::getValueofitemsReport($param);
                    }else{

                        $headers = array(
                            'id' => 'Shipment#',
                            'unit' => 'Unit Number',
                            'customer' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city'=> 'City',
                            'state'=> 'State',
                            'country'=> 'Country',
                            'createdOn' => 'Created On',
                            'numItems' => 'Number of Items',
                            'totalShippingCost'=> 'Total Shipping Cost',
                            'itemTotalCost' => 'Item Total Cost'
                        );
                        
                        $sort = array(
                            'id' => array('current' => 'sorting'),
                            'unit' => array('current' => 'sorting'),
                            'customer' => array('current' => 'sorting'),
                            'email' => array('current' => 'sorting'),
                            'contactNumber' => array('current' => 'sorting'),
                            'city'=> array('current' => 'sorting'),
                            'state'=> array('current' => 'sorting'),
                            'country'=> array('current' => 'sorting'),
                            'createdOn' => array('current' => 'sorting'),
                            'numItems' => array('current' => 'sorting'),
                            'totalShippingCost' => array('current' => 'sorting'),
                            'itemTotalCost' => array('current' => 'sorting')
                        );
                        
                        $records = Shipment::getValueofitemsReport($param);

                    }
                }else{
                    $headers = array(
                        'id' => 'Shipment#',
                        'unit' => 'Unit Number',
                        'customer' => 'Name',
                        'email' => 'Email',
                        'contactNumber' => 'Contact',
                        'city'=> 'City',
                        'state'=> 'State',
                        'country'=> 'Country',
                        'createdOn' => 'Created On',
                        'numItems' => 'Number of Items',
                        'totalShippingCost'=> 'Total Shipping Cost',
                        'itemTotalCost' => 'Item Total Cost',
                        'processor'  => 'Processor',
                        'paymentMethod'  => 'Payment Method',
                        'paymentDate'  => 'Payment Date',
                    );
                    
                    $sort = array(
                        'id' => array('current' => 'sorting'),
                        'unit' => array('current' => 'sorting'),
                        'customer' => array('current' => 'sorting'),
                        'email' => array('current' => 'sorting'),
                        'contactNumber' => array('current' => 'sorting'),
                        'city'=> array('current' => 'sorting'),
                        'state'=> array('current' => 'sorting'),
                        'country'=> array('current' => 'sorting'),
                        'createdOn' => array('current' => 'sorting'),
                        'numItems' => array('current' => 'sorting'),
                        'totalShippingCost' => array('current' => 'sorting'),
                        'itemTotalCost' => array('current' => 'sorting'),
                        'processor' => array('current' => 'sorting'),
                        'paymentMethod' => array('current' => 'sorting'),
                        'paymentDate' => array('current' => 'sorting'),
                    );
                    
                    $records = Shipment::getValueofitemsReport($param);

                }

                
            }
            elseif($param['searchByCondition'] == 'revenuewithshippingmethod')
            {
                if(isset($param['searchAccount']['tabType']) && !empty($param['searchAccount']['tabType']))
                {
                     if($param['searchAccount']['tabType'] == "shipment")
                      {
                          $headers = array(
                            'id' => 'Shipment#',
                            'totalShippingCost'  => 'Total Shipping Cost',
                            'totalClearingcost'  => 'Total Clearing Charge',
                            'totalDispatchCharge'  => 'Total Dispatch Cost',
                            'totalInsurance'  => 'Total Insurance',
                            'totalCustomDuty'  => 'Custom Duty',
                            'totalInventoryCharge' => 'Total Inventory Charge'
                         );

                         /* BUILD SORTING ARRAY */
                        $sort = array(
                            'id' => array('current' => 'sorting'),
                            'totalShippingCost' => array('current' => 'sorting'),
                            'totalClearingcost' => array('current' => 'sorting'),
                            'totalDispatchCharge' => array('current' => 'sorting'),
                            'totalInsurance' => array('current' => 'sorting'),
                            'totalCustomDuty' => array('current' => 'sorting'),
                            'totalInventoryCharge' => array('current' => 'sorting')
                        );
                        $records = Shipment::getallShipmentShippingMethodData($param);
                      }
                     else if($param['searchAccount']['tabType'] == "fillship")
                      {
                          $headers = array(
                            'id' => 'Shipment#',
                            'totalShippingCost'  => 'Total Shipping Cost',
                            'totalClearingcost'  => 'Total Clearing Charge',
                            'totalDispatchCharge'  => 'Total Dispatch Cost',
                            'totalInsurance'  => 'Total Insurance',
                            'totalCustomDuty'  => 'Custom Duty'
                         );

                         /* BUILD SORTING ARRAY */
                        $sort = array(
                            'id' => array('current' => 'sorting'),
                            'totalShippingCost' => array('current' => 'sorting'),
                            'totalClearingcost' => array('current' => 'sorting'),
                            'totalDispatchCharge' => array('current' => 'sorting'),
                            'totalInsurance' => array('current' => 'sorting'),
                            'totalCustomDuty' => array('current' => 'sorting')
                        );
                        $records = Shipment::getallFillshipShippingMethodData($param);
                        
                      }

                }
                else{
                          $headers = array(
                            'totalShipment' => 'Total No of Shipment',
                            'totalShippingCost'  => 'Total Shipping Cost',
                            'totalClearingcost'  => 'Total Clearing Charge',
                            'totalDispatchCharge'  => 'Total Dispatch Cost',
                            'totalInsurance'  => 'Total Insurance',
                            'totalCustomDuty'  => 'Custom Duty',
                            'totalInventoryCharge' => 'Total Inventory Charge'
                         );

                         /* BUILD SORTING ARRAY */
                        $sort = array(
                            'totalShipment' => array('current' => 'sorting'),
                            'totalShippingCost' => array('current' => 'sorting'),
                            'totalClearingcost' => array('current' => 'sorting'),
                            'totalDispatchCharge' => array('current' => 'sorting'),
                            'totalInsurance' => array('current' => 'sorting'),
                            'totalCustomDuty' => array('current' => 'sorting'),
                            'totalInventoryCharge' => array('current' => 'sorting')
                        );

                        $records = Shipment::getallShipmentShippingMethodData($param);
                    }

                    for($i=0; $i<count($records); $i++)
                    {
                        $records[$i]['totalDispatchCharge'] = "0.00";
                        $records[$i]['totalCustomDuty'] = "0.00";

                        

                    }

            }
            elseif($param['searchByCondition'] == 'withoutstoragecharge')
            {

                if(isset($param['searchAccount']['accountType']) && !empty($param['searchAccount']['accountType']))
                {
                    if($param['searchAccount']['accountType'] == "paid"){
                            $headers = array(
                                'id' => 'Shipment#',
                                'unit' => 'Unit Number',
                                'name' => 'Name',
                                'email' => 'Email',
                                'contactNumber' => 'Contact',
                                'city'=> 'City',
                                'state'=> 'State',
                                'country'=> 'Country',
                                'createdOn' => 'Created On',                    
                                'maxStorageDate' => 'Storage Expiry Date',
                                'totalCost'  => 'Cost of Shipments',
                                'processor'  => 'Processor',
                                'paymentMethod'  => 'Payment Method',
                                'paymentDate'  => 'Payment Date',
                             );

                             /* BUILD SORTING ARRAY */
                            $sort = array(
                                'id' => array('current' => 'sorting'),
                                'unit' => array('current' => 'sorting'),
                                'name' => array('current' => 'sorting'),
                                'email' => array('current' => 'sorting'),
                                'contactNumber' => array('current' => 'sorting'),
                                'city' => array('current' => 'sorting'),
                                'state' => array('current' => 'sorting'),
                                'country' => array('current' => 'sorting'),
                                'createdOn' => array('current' => 'sorting'),
                                'maxStorageDate' => array('current' => 'sorting'),
                                'totalCost' => array('current' => 'sorting'),
                                'processor' => array('current' => 'sorting'),
                                'paymentMethod' => array('current' => 'sorting'),
                                'paymentDate' => array('current' => 'sorting'),
                            );
                          
                         $records = Shipment::getallShipmentDatawithoutStorage($param);
                    }else{

                            $headers = array(
                                'id' => 'Shipment#',
                                'unit' => 'Unit Number',
                                'name' => 'Name',
                                'email' => 'Email',
                                'contactNumber' => 'Contact',
                                'city'=> 'City',
                                'state'=> 'State',
                                'country'=> 'Country',
                                'createdOn' => 'Created On',                    
                                'maxStorageDate' => 'Storage Expiry Date',
                                'totalCost'  => 'Cost of Shipments'
                             );

                             /* BUILD SORTING ARRAY */
                            $sort = array(
                                'id' => array('current' => 'sorting'),
                                'unit' => array('current' => 'sorting'),
                                'name' => array('current' => 'sorting'),
                                'email' => array('current' => 'sorting'),
                                'contactNumber' => array('current' => 'sorting'),
                                'city' => array('current' => 'sorting'),
                                'state' => array('current' => 'sorting'),
                                'country' => array('current' => 'sorting'),
                                'createdOn' => array('current' => 'sorting'),
                                'maxStorageDate' => array('current' => 'sorting'),
                                'totalCost' => array('current' => 'sorting')
                            );
                      
                           $records = Shipment::getallShipmentDatawithoutStorage($param);

                    }
                }else{
                            $headers = array(
                                'id' => 'Shipment#',
                                'unit' => 'Unit Number',
                                'name' => 'Name',
                                'email' => 'Email',
                                'contactNumber' => 'Contact',
                                'city'=> 'City',
                                'state'=> 'State',
                                'country'=> 'Country',
                                'createdOn' => 'Created On',                    
                                'maxStorageDate' => 'Storage Expiry Date',
                                'totalCost'  => 'Cost of Shipments',
                                'processor'  => 'Processor',
                                'paymentMethod'  => 'Payment Method',
                                'paymentDate'  => 'Payment Date',
                             );

                             /* BUILD SORTING ARRAY */
                            $sort = array(
                                'id' => array('current' => 'sorting'),
                                'unit' => array('current' => 'sorting'),
                                'name' => array('current' => 'sorting'),
                                'email' => array('current' => 'sorting'),
                                'contactNumber' => array('current' => 'sorting'),
                                'city' => array('current' => 'sorting'),
                                'state' => array('current' => 'sorting'),
                                'country' => array('current' => 'sorting'),
                                'createdOn' => array('current' => 'sorting'),
                                'maxStorageDate' => array('current' => 'sorting'),
                                'totalCost' => array('current' => 'sorting'),
                                'processor' => array('current' => 'sorting'),
                                'paymentMethod' => array('current' => 'sorting'),
                                'paymentDate' => array('current' => 'sorting'),
                            );
                          
                             $records = Shipment::getallShipmentDatawithoutStorage($param);

                }
                

            }
            
            
        }
        
        
      //print_r($records); die;
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        $data['sort'] = $sort;
        $data['searchData'] = $param;
        $data['type'] = $param['searchByCondition'];
        $data['tabtype'] = (!empty($param['searchAccount']['tabType']))?$param['searchAccount']['tabType']:'';
        $data['accountType'] = (!empty($param['searchAccount']['accountType']))?$param['searchAccount']['accountType']:'';
        $dataList['headers'] = $headers;
        $dataList['records'] = $records;

        $data['dataList'] = $dataList;
        $data['title'] = "Administrative Panel :: Account Reports";
        $data['contentTop'] = array('breadcrumbText' => 'Account Reports', 'contentTitle' => 'Account Reports', 'pageInfo' => 'This section allows you to view account reports');
        $data['pageTitle'] = "Account Reports";

        //print_r($data['searchData']); die;

        return view('Administrator.report.accountreport', $data);
    }


    
    public function exportaccountreport() {

        $searchAccountArr = array(
            'createdOn' => 'all',
            'searchByDate' => '',
        );
        
        $sortField = \Session::get('ACCOUNTREPORTDATA.field');
        $sortType = \Session::get('ACCOUNTREPORTDATA.type');
        $searchByCondition = \Session::get('ACCOUNTREPORTDATA.searchByCondition');
        $searchAccount = \Session::get('ACCOUNTREPORTDATA.searchAccount');
        $searchDisplay = \Session::get('ACCOUNTREPORTDATA.searchDisplay');

        $param['field'] = !empty($sortField) ? $sortField[0] : 'users.createdOn';
        $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
        $param['searchByCondition'] = !empty($searchByCondition) ? $searchByCondition[0] : '';
        $param['searchAccount'] = !empty($searchAccount) ? $searchAccount[0] : $searchAccountArr;
        
        $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : 1000;

        $exportData = Shipment::exportAccountData($param);
        //print_r($exportData);
        $excelName = "Accounting-Data" . Carbon::now();

        ob_end_clean();
        ob_start();

        /* Generate excell */

        Excel::create($excelName, function($excel) use($exportData) {
            $excel->sheet('Sheet 1', function($sheet) use($exportData) {
                $sheet->fromArray($exportData);
            });
        })->store('xlsx', public_path('exports'));

        ob_flush();
        $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
        return response()->json(['path' => $creatingPath]);
    }

    public function getaccountsearchcondition($type = ''){
        $data = array();

        $data['type'] = $type;

        $searchAccountArr = array(
            'createdOn' => 'all',
            'searchByDate' => '',
            'shippingMethodId' => '',
        );

        $searchAccount = \Session::get('ACCOUNTREPORTDATA.searchAccount');
        $param['searchAccount'] = !empty($searchAccount) ? $searchAccount[0] : $searchAccountArr;

        /* FETCH SHIPPING METHOD LIST  */
        $data['shippingMethodList'] = Shippingmethods::select('shippingid','shipping')->where('deleted', '0')->orderby('shipping', 'asc')->get();


        $data['searchData'] = $param;

       return view('Administrator.report.accountsearchcondition', $data);
    }
    
    public function exportcustomerreportdata() {
        
        $sortField = \Session::get('CUSTOMERREPORTDATA.field');
        $sortType = \Session::get('CUSTOMERREPORTDATA.type');
        $searchByCondition = \Session::get('CUSTOMERREPORTDATA.searchByCondition');
        $searchCustomer = \Session::get('CUSTOMERREPORTDATA.searchCustomer');
        $searchDisplay = \Session::get('CUSTOMERREPORTDATA.searchDisplay');

        $param['field'] = !empty($sortField) ? $sortField[0] : 'users.createdOn';
        $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
        $param['searchByCondition'] = !empty($searchByCondition) ? $searchByCondition[0] : '';
        $param['searchCustomer'] = !empty($searchCustomer) ? $searchCustomer[0] : $searchCustomerArr;
        $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : 1000;
        
        $exportData = User::exportCustomerData($param);
        
        $excelName = "Customer-Data" . Carbon::now();

        ob_end_clean();
        ob_start();

        /* Generate excell */

        Excel::create($excelName, function($excel) use($exportData) {
            $excel->sheet('Sheet 1', function($sheet) use($exportData) {
                $sheet->fromArray($exportData);
            });
        })->store('xlsx', public_path('exports'));

        ob_flush();
         $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
        return response()->json(['path' => $creatingPath]);
    }


    public function warehousetrackingreport(Request $request)
    {
       $data =  $param = array();
        $dataList = array();
        $searchByDate = $searchByEmail =  $searchByCondition = "";

        $searchWarehouseArr = array(
            'searchByEmail' => ''
        );

         if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchByConditionOld = \Session::get('WAREHOUSEREPORT.searchByCondition');
            $searchByConditionOld = !empty($searchByConditionOld) ? $searchByConditionOld[0] : 'ondelivery';

            $searchByDateOld = \Session::get('WAREHOUSEREPORT.searchByDate');
            $searchByDateOld = !empty($searchByDateOld) ? $searchByDateOld[0] : '';

            $searchByCreatedOnOld = \Session::get('WAREHOUSEREPORT.searchByCreatedOn');
            $searchByCreatedOnOld = !empty($searchByCreatedOnOld) ? $searchByCreatedOnOld[0] : '';

            $searchWarehouseOld = \Session::get('WAREHOUSEREPORT.searchWarehouse');
            $searchWarehouseOld = !empty($searchWarehouseOld) ? $searchWarehouseOld[0] : $searchWarehouseArr;

            $searchByDate = \Input::get('searchByDate', $searchByDateOld);
            $searchByCreatedOn = \Input::get('searchByCreatedOn', $searchByCreatedOnOld);
            $searchByCondition = \Input::get('searchByCondition', $searchByConditionOld);
            $searchWarehouse = \Input::get('searchWarehouse', $searchWarehouseArr);
            $searchDisplay = \Input::get('searchDisplay', 10);


            $field = \Input::get('field', 'shipments.createdOn');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('WAREHOUSEREPORT');
            \Session::push('WAREHOUSEREPORT.searchDisplay', $searchDisplay);
            \Session::push('WAREHOUSEREPORT.searchWarehouse', $searchWarehouse);
            \Session::push('WAREHOUSEREPORT.searchByCondition', $searchByCondition);
            \Session::push('WAREHOUSEREPORT.searchByDate', $searchByDate);
            \Session::push('WAREHOUSEREPORT.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('WAREHOUSEREPORT.field', $field);
            \Session::push('WAREHOUSEREPORT.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByDate'] = $searchByDate;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchDisplay'] = $searchDisplay;
            $param['searchWarehouse'] = $searchWarehouse; 
            $param['searchByCondition'] = $searchByCondition; 

           

         }else{
            $sortField = \Session::get('WAREHOUSEREPORT.field');
            $sortType = \Session::get('WAREHOUSEREPORT.type');
            $searchWarehouse = \Session::get('WAREHOUSEREPORT.searchWarehouse');
            $searchByDate = \Session::get('WAREHOUSEREPORT.searchByDate');
            $searchByCreatedOn = \Session::get('WAREHOUSEREPORT.searchByCreatedOn');
            $searchDisplay = \Session::get('WAREHOUSEREPORT.searchDisplay');
            $searchByCondition = \Session::get('WAREHOUSEREPORT.searchByCondition');

            $param['field'] = !empty($sortField) ? 'shipments.createdOn' : 'shipments.createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchWarehouse'] = !empty($searchWarehouse) ? $searchWarehouse[0]: $searchWarehouseArr;
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : 1000;
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByCondition'] = !empty($searchByCondition) ? $searchByCondition[0] : 'ondelivery';
            
         }

        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* BUILD SORTING ARRAY */
        $sort = array(
            'storeName' => array('current' => 'sorting'),
            'categoryName' => array('current' => 'sorting'),
            'subCategoryName' => array('current' => 'sorting'),
            'itemName' => array('current' => 'sorting'),
            'shipmentId' => array('current' => 'sorting'),
        );

       //print_r($param['searchByCondition']); die;

        /* FETCH USER LIST  */
         if(isset($param['searchByCondition']) && !empty($param['searchByCondition'])) {

            if($param['searchByCondition'] == 'ondelivery'){

                $shipmentData = Shipment::getShipmentListByUser($param);

            }else if($param['searchByCondition'] == 'onpackage'){

               $shipmentData = Shipment::getShipmentListByUser($param); 
            }else if($param['searchByCondition'] == 'onpackaging'){
                
               $shipmentData = Shipment::getPackagingReport($param); 
            }

            
        }

       // echo "<pre>";print_r($shipmentData); die;
        
        $data['sort'] = $sort;
        $data['searchData'] = $param;
        $data['dataList'] = $shipmentData;
        $data['page'] = $shipmentData->currentPage();
        $data['title'] = "Administrative Panel :: Warehouse Reports";
        $data['contentTop'] = array('breadcrumbText' => 'Warehouse Reports', 'contentTitle' => 'Warehouse Reports', 'pageInfo' => 'This section allows you to view warehouse reports');
        $data['pageTitle'] = "Warehouse Reports";

        //print_r($data['dataList']); die;

        return view('Administrator.report.warehousereport', $data);


    }

    public function exportwarehousereportdata()
    {

        $sortField = \Session::get('WAREHOUSEREPORT.field');
        $sortType = \Session::get('WAREHOUSEREPORT.type');
        $searchWarehouse = \Session::get('WAREHOUSEREPORT.searchWarehouse');
        $searchByDate = \Session::get('WAREHOUSEREPORT.searchByDate');
        $searchByCreatedOn = \Session::get('WAREHOUSEREPORT.searchByCreatedOn');
        $searchDisplay = \Session::get('WAREHOUSEREPORT.searchDisplay');
        $searchByCondition = \Session::get('WAREHOUSEREPORT.searchByCondition');

        $param['field'] = !empty($sortField) ? 'shipments.createdOn' : 'shipments.createdOn';
        $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
        $param['searchWarehouse'] = !empty($searchWarehouse) ? $searchWarehouse[0]: $searchWarehouseArr;
        $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : 1000;
        $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
        $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
        $param['searchByCondition'] = !empty($searchByCondition) ? $searchByCondition[0] : 'ondelivery';
        
        $exportData = Shipment::exportWarehouseData($param);
        
        $excelName = "Warehouse-Report_" . Carbon::now();

        ob_end_clean();
        ob_start();

        /* Generate excell */

        Excel::create($excelName, function($excel) use($exportData) {
            $excel->sheet('Sheet 1', function($sheet) use($exportData) {
                $sheet->fromArray($exportData);
            });
        })->store('xlsx', public_path('exports'));

        ob_flush();
         $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
        return response()->json(['path' => $creatingPath]);
    }
    
    public function valueofitems() {
        
        $param = array();
        $data = array();
        $dataList = array();
        $searchByDate = $searchByDate = $searchByCreatedOn = "";
        \Session::forget("ITEMVALUEREPORT");
        if (\Request::isMethod('post')) {
            
            $searchByCreatedOnOld = \Session::get('ITEMVALUEREPORT.searchByCreatedOn');
            $searchByCreatedOnOld = !empty($searchByCreatedOnOld) ? $searchByCreatedOnOld[0]:"";
            $searchByCreatedOn = \Input::get('searchByCreatedOn', $searchByCreatedOnOld);
            
            $searchByDateOld = \Session::get('ITEMVALUEREPORT.searchByDate');
            $searchByDateOld = !empty($searchByDateOld) ? $searchByDateOld[0]:"";
            $searchByDate = \Input::get('searchByDate', $searchByDateOld);
            
            $searchByUnitOld = \Session::get('ITEMVALUEREPORT.searchByUserUnit');
            $searchByUnitOld = !empty($searchByUnitOld) ? $searchByUnitOld[0]:"";
            $searchByUnit = \Input::get('searchByUserUnit', $searchByUnitOld);
            
            $searchDisplay = \Input::get('searchDisplay', 100);

            $field = (!empty(\Input::get('field'))) ? \Input::get('field') : 'id';
            $type = (!empty(\Input::get('type'))) ? \Input::get('type') : 'desc';;
            \Session::forget('ITEMVALUEREPORT');
            \Session::push('ITEMVALUEREPORT.searchByDate', $searchByDate);
            \Session::push('ITEMVALUEREPORT.searchByUnit', $searchByUnit);
            \Session::push('ITEMVALUEREPORT.searchDisplay', $searchDisplay);
            \Session::push('ITEMVALUEREPORT.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('ITEMVALUEREPORT.field', $field);
            \Session::push('ITEMVALUEREPORT.type', $type);
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchByUnit'] = $searchByUnit;
            $param['searchDisplay'] = $searchDisplay;
            $param['field'] = $field;
            $param['type'] = $type;
        }
        else
        {
            $sortField = \Session::get('ITEMVALUEREPORT.field');
            $sortType = \Session::get('ITEMVALUEREPORT.type');
            $searchByCreatedOn = \Session::get('ITEMVALUEREPORT.searchByCreatedOn');
            $searchByDate = \Session::get('ITEMVALUEREPORT.searchByDate');
            $searchByUnit = \Session::get('ITEMVALUEREPORT.searchByUnit');
            $searchDisplay = \Session::get('ITEMVALUEREPORT.searchDisplay');
            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0]:"";
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0]:"";
            $param['searchByUnit'] = !empty($searchByUnit) ? $searchByUnit[0]:"";
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }
        
        /* BUILD SORTING ARRAY */
        $sort = array(
            'id' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';
        
        if($param['searchByDate']!="" || $param['searchByUnit']!="" || $param['searchByCreatedOn']!="")
        {
            $dataList = Shipment::getValueofitemsReport($param);
            $data['page'] = $dataList->currentPage();
        }
        
        $data['param'] = $param;
        $data['sort'] = $sort;
        $data['dataList'] = $dataList;
        
        $data['title'] = "Administrative Panel :: Value of Item Report";
        $data['contentTop'] = array('breadcrumbText' => 'Value of Item Report', 'contentTitle' => 'Value of Item Report', 'pageInfo' => 'This section allows you to view value of items of shipments');
        $data['pageTitle'] = "Value of Item Report";
        
        return view('Administrator.report.valueofitemsreport', $data);
    }

    ///////////////////Subscription History/////////////////////

    public function subscriptionhistory(Route $route, Request $request) {
        $data = array();

        

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchByCreatedOn = \Input::get('searchByCreatedOn', 'all');
            $searchByDate = \Input::get('searchByDate', '');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

           
            $field = \Input::get('field', 'user_subscription.id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('SUBSCRIPTIONREPORT');
            \Session::push('SUBSCRIPTIONREPORT.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('SUBSCRIPTIONREPORT.searchDisplay', $searchDisplay);
            \Session::push('SUBSCRIPTIONREPORT.searchByDate', $searchByDate);
            \Session::push('SUBSCRIPTIONREPORT.field', $field);
            \Session::push('SUBSCRIPTIONREPORT.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('SUBSCRIPTIONREPORT.field');
            $sortType = \Session::get('SUBSCRIPTIONREPORT.type');
            $searchByCreatedOn = \Session::get('SUBSCRIPTIONREPORT.searchByCreatedOn');
            $searchByDate = \Session::get('SUBSCRIPTIONREPORT.searchByDate');
            $searchDisplay = \Session::get('SUBSCRIPTIONREPORT.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'user_subscription.subscribedOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'subscribedFor' => array('current' => 'sorting'),
            'status' => array('current' => 'sorting'),
            'subscribedOn' => array('current' => 'sorting')
        );


        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';
        
        $subscriptionData = \App\Model\Usersubscription::getcustomerDetailsBySubscription($param);

        /* SET DATA FOR VIEW  */

       // $data['page'] = $storeData->currentPage();
        $data['title'] = "Administrative Panel :: Subscription History";
        $data['contentTop'] = array('breadcrumbText' => 'Subscription History', 'contentTitle' => 'Subscription History', 'pageInfo' => 'This section allows you to view store history and reports');
        $data['pageTitle'] = "Subscription History";
        $data['subscriptionData'] = $subscriptionData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        return view('Administrator.report.subscriptionhistory', $data);
    }

    public function renewemail(Request $request)
    {
        $userdetails = \App\Model\User::find($request->userId);
        $to = $userdetails->email;
        $firstName = $userdetails->firstName;
        $lastName = $userdetails->lastName;

        $expiryDate = \App\Model\Usersubscription::where('userId', $request->userId)->pluck('expiryDate');
        $emailTemplate = array();
        $replace['[NAME]'] = $firstName . ' ' . $lastName;
        $replace['[DATE]'] = $expiryDate;

        $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'subscription_renewal_reminder')->first();
        
        customhelper::SendMail($emailTemplate, $replace, $to);

        return response()->json([
            'status' => '1',
            'results' => 'success',
        ]);

    }


    public function exportallsubscription($page)
    {
        $shipmentObj = new Shipment;
        $param = array();
        /* BUILD DEFAULT SEARCH ARRAY */
        
        $searchByCreatedOn = \Session::get('SUBSCRIPTIONREPORT.searchByCreatedOn');
        $searchByDate = \Session::get('SUBSCRIPTIONREPORT.searchByDate');

        $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
        $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
        /* Get data to export */
        $exportData = \App\Model\Stores::exportStore($param);
        

        $excelName = "Subscription-Data" . Carbon::now();

        ob_end_clean();
        ob_start();

        /* Generate excell */

        Excel::create($excelName, function($excel) use($exportData) {
            $excel->sheet('Store History', function($sheet) use($exportData) {
                $sheet->fromArray($exportData);
                $sheet->row(1, function($row) { $row->setBackground('#CCCCCC'); $row->setFontWeight('bold'); });
            });
        })->store('xlsx', public_path('exports'));

        ob_flush();
        $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
        return response()->json(['path' => $creatingPath]);

    }


}
