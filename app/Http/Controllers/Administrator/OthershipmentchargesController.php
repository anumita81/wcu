<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Country;
use App\Model\Othershipmentcharges;
use App\Model\Warehouse;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class OthershipmentchargesController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Othershipmentcharges'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */

            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('OTHERCHARGESDATA');
            \Session::push('OTHERCHARGESDATA.searchDisplay', $searchDisplay);
            \Session::push('OTHERCHARGESDATA.field', $field);
            \Session::push('OTHERCHARGESDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('OTHERCHARGESDATA.field');
            $sortType = \Session::get('OTHERCHARGESDATA.type');
            $searchDisplay = \Session::get('OTHERCHARGESDATA.searchDisplay');


            $param['field'] = !empty($sortField) ? $sortField[0] : 'orderby';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name'=>array('current' => 'sorting'), 'amount'  => array('current' => 'sorting')
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTRY LIST  */
        $feesData = Othershipmentcharges::getShipmentChargesList($param);
        
        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Other Shipment Charges";
        $data['contentTop'] = array('breadcrumbText' => 'Other Shipment Charges', 'contentTitle' => 'Other Shipment Charges', 'pageInfo' => 'This section allows you to add/edit other shipment charges');
        $data['pageTitle'] = "Other Shipment Charges";
        $data['page'] = $feesData->currentPage();
        $data['feesData'] = $feesData;    
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.othershipmentcharges.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Other Shipment Charges";
        $data['contentTop'] = array('breadcrumbText' => 'Other Shipment Charges', 'contentTitle' => 'Other Shipment Charges', 'pageInfo' => 'This section allows you to add/edit other shipment charges');
        $data['page'] = !empty($page) ? $page : '1';
        
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $shipmentcharges = Othershipmentcharges::find($id);
            $data['shipmentcharges'] = $shipmentcharges;
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add New';

        }
        return view('Administrator.othershipmentcharges.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $othershipmentcharges = new Othershipmentcharges;

        //print_r($request->all()); die;

        $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'amount' => 'required',
                    'orderby'=> 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $othershipmentcharges = Othershipmentcharges::find($id);
                $othershipmentcharges->updatedBy = Auth::user()->id;
                $othershipmentcharges->updatedOn = Config::get('constants.CURRENTDATE');
            }
            else{
                $othershipmentcharges->createdBy = Auth::user()->id;
                $othershipmentcharges->createdOn = Config::get('constants.CURRENTDATE');
            }
            $othershipmentcharges->name = $request->name;
            $othershipmentcharges->amount = $request->amount;
            $othershipmentcharges->orderby = $request->orderby;
            $othershipmentcharges->save();
            $othershipmentchargesId = $othershipmentcharges->id;

            return redirect('administrator/othershipmentcharges')->with('successMessage', 'Other Shipment Charges saved successfuly.');
        }
    }

    public function deletedata($id = '',$page='') {

        $othershipmentcharges= new Othershipmentcharges();

        $createrModifierId = Auth::user()->id;
        if(!empty($id)){
        $othershipmentcharges = $othershipmentcharges->find($id);        

        if (Othershipmentcharges::deleteRecord($id, $createrModifierId)) {
               return \Redirect::to('administrator/othershipmentcharges/?page='.$page)->with('successMessage', 'Other Shipment Charges deleted successfuly.');
            } else {
                return \Redirect::to('administrator/othershipmentcharges/?page=' .$page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/othershipmentcharges/?page=' .$page)->with('errorMessage', 'Error in operation!');
        }
    }

    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Othershipmentcharges::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/othershipmentcharges/?page=' . $page)->with('successMessage', 'Other Shipment Charges status changed successfully.');
            } else {
                return \Redirect::to('administrator/othershipmentcharges/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/othershipmentcharges/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

 



}
