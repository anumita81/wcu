<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Schedulepickup;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\Driver;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Model\Usernotification;
use App\Model\Scheduleimage;
use Carbon\Carbon;
use customhelper;

class SchedulepickupController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Schedulepickup'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $searchScheduleArr = array(
            'pickuptype' => '',
            'pickupCountry' => '',
            'pickupState' => '',
            'pickupCity' => '',
            'pickupName' => '',
            'pickupPhone' => '',
            'pickupEmail' => '',
            'scheduleTocountry' => '',
            'scheduleToState' => '',
            'scheduleToCity' => '',
            'scheduleToName' => '',
            'scheduleToPhone' => '',
            'scheduleToEmail' => '',
            'minWeight' => '',
            'maxWeight' => '',
            'minPackage' => '',
            'maxPackage' => '');

        if (\Request::isMethod('post')) {

            /* GET POST VALUE  */
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchSchedule = \Input::get('searchSchedule', $searchScheduleArr);
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('SCHEDULEPICKUPDATA');
            \Session::push('SCHEDULEPICKUPDATA.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('SCHEDULEPICKUPDATA.searchDisplay', $searchDisplay);
            \Session::push('SCHEDULEPICKUPDATA.searchByDate', $searchByDate);
            \Session::push('SCHEDULEPICKUPDATA.searchSchedule', $searchSchedule);
            \Session::push('SCHEDULEPICKUPDATA.field', $field);
            \Session::push('SCHEDULEPICKUPDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchSchedule'] = $searchSchedule;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('SCHEDULEPICKUPDATA.field');
            $sortType = \Session::get('SCHEDULEPICKUPDATA.type');
            $searchByCreatedOn = \Session::get('SCHEDULEPICKUPDATA.searchByCreatedOn');
            $searchByDate = \Session::get('SCHEDULEPICKUPDATA.searchByDate');
            $searchSchedule = \Session::get('SCHEDULEPICKUPDATA.searchSchedule');
            $searchDisplay = \Session::get('SCHEDULEPICKUPDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchSchedule'] = !empty($searchSchedule) ? $searchSchedule[0] : $searchScheduleArr;
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }


        /* BUILD SORTING ARRAY */
        $sort = array(
            'id' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

       
        $scheduleData = Schedulepickup::getScheduleList($param);

        \Session::forget('SCHEDULEPICKUPDATA');

        
        /* FETCH COUNTRY LIST  */
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Schedule Pickup";
        $data['contentTop'] = array('breadcrumbText' => 'Schedule Pickup', 'contentTitle' => 'Schedule Pickup', 'pageInfo' => 'This section allows you to manage pickup requests');
        $data['pageTitle'] = "Schedule Pickup";
        $data['page'] = $scheduleData->currentPage();
        $data['scheduleData'] = $scheduleData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.schedulepickup.index', $data);
    }

    /**
     * Method to fetch state list
     * @return array
     */
    public function getstatelist($countryId = '') {
        $country = Country::find($countryId);
        $stateList = array();
        if (isset($country->code) && !empty($country->code)) {

            $stateList = State::where('status', '1')->where('deleted', '0')->where('countryCode', $country->code)->orderby('name', 'asc')->get();

            
        }
        echo json_encode($stateList);
        exit;
    }

      /**
     * Method to fetch city list
     * @return array
     */
    public function getcitylist($stateid = '') {
        $state = State::find($stateid);
        $cityList = array();
        if (isset($state->code) && !empty($state->code)) {
            $cityList = City::where('status', '1')
                    ->where('deleted', '0')
                    ->where('stateCode', $state->code)
                    ->orderby('name', 'asc')
                    ->get();
        }
        echo json_encode($cityList);
        exit;
    }

    /**
     * Method used to change status
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, Request $request) {

        if (\Request::isMethod('post')) {
            $schedulepickup = new Schedulepickup;

            $validator = Validator::make($request->all(), [
                        'status' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            } else {
                if (!empty($id)) {
                    $schedulepickup = Schedulepickup::find($id);
                    $schedulepickup->scheduleStatus = $request->status;
                    if($request->status == 'S')
                    {
                       $schedulepickup->assignedDriverId = $request->assignedDriver;
                       $schedulepickup->assignedDate = Carbon::parse($request->assignedDate);
                    }
                    $schedulepickup->updatedBy = Auth::user()->id;
                    $schedulepickup->updatedOn = Config::get('constants.CURRENTDATE');

                    $schedulepickup->save();

                    return \Redirect::to('administrator/schedulepickup/?page=' . $page)->with('successMessage', 'Schedulepickup status updated successfully.');
                }
            }
        }


        /* SET DATA FOR VIEW  */
        $data['pageTitle'] = "Update Status";
        $data['id'] = $id;
        $data['page'] = $page;
        //$data['status'] = Schedulepickup::where('id', $id)->first()->status;
        $data['scheduleData'] = Schedulepickup::getScheduleDeatil($id);
        $data['driverList'] = Driver::where('deleted', '0')->orderby('driverName', 'asc')->get();
        if($data['scheduleData'][0]['scheduleStatus'] == 'S' || $scheduleData[0]['scheduleStatus'] == 'C')
        {
           $data['statusList'] = array(
            'S' => 'Scheduled',
            'C' => 'Completed'
            ); 
        }else{
            $data['statusList'] = array(
            'P' => 'Pending',
            'S' => 'Scheduled',
            'C' => 'Completed'
        );
        }
        


        return view('Administrator.schedulepickup.changestatus', $data);
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Schedulepickup::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/schedulepickup/?page=' . $page)->with('successMessage', 'Schedule pickup deleted successfully.');
            } else {
                return \Redirect::to('administrator/schedulepickup/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/schedulepickup/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to clear search history
     * @return type
     */
    public function showall() {
        \Session::forget('SCHEDULEPICKUPDATA');
        return \Redirect::to('administrator/schedulepickup');
    }

    /**
     * Method used to send notification
     * @return type
     */

    public function sendnotification($page, Request $request)
    {

        if (\Request::isMethod('post')) {
            $notification = new Usernotification;

            $validator = Validator::make($request->all(), [
                        'notificationEmail' => 'required|email',
                        'notificationMessage'=> 'required'
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            } else {
                
                    $requesteEmail = array();

                    $requestEmail = explode(',', $request->notificationEmail);

                    for($i=0; $i<count($requestEmail); $i++)
                    {
                        $userId = User::where('email', $requestEmail[$i])->get()->toArray();


                        $notification->userId = $userId[0]['id'];
                        $notification->message = $request->notificationMessage;
                        $notification->senderId = Auth::user()->id;
                        $notification->sentOn = Config::get('constants.CURRENTDATE');

                        $notification->save();


                    }
                 

                    return \Redirect::to('administrator/schedulepickup/?page=' . $page)->with('successMessage', 'Notification sent successfully.');
                
            }
        }

    }

    public function scheduledetail($id, $page )
    {
        $data = array();

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Pickup Request Details";
        $data['contentTop'] = array('breadcrumbText' => 'Pickup Request Details', 'contentTitle' => 'Pickup Request Details', 'pageInfo' => 'This section allows you to manage pickup requests');
        $data['pageTitle'] = "Pickup Request Details";
        $data['page'] = $page;
        /* FETCH PICKUP DETAILS  */

        $data['scheduleData'] = Schedulepickup::getScheduleDeatil($id);

        $data['scheduleimage'] = Schedulepickup::getImage($id);

        

        $data['driverList'] = Driver::where('deleted', '0')->orderby('driverName', 'asc')->get();
        
        if($data['scheduleData'][0]['scheduleStatus'] == 'S' || $scheduleData[0]['scheduleStatus'] == 'C')
        {
            $data['statusList'] = array(
             'S' => 'Scheduled',
             'C' => 'Completed'
             ); 
         }else{
             $data['statusList'] = array(
             'P' => 'Pending',
             'S' => 'Scheduled',
             'C' => 'Completed'
            );
        }
       
        /* FETCH DELIVERY COMPANY LIST  */
       // $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();

        /* FETCH SOTRE LIST  */
       // $data['storeList'] = \App\Model\Stores::where('deleted', '0')->where('status', '1')->orderby('storeName', 'asc')->get();

        /* FETCH SOTRE LIST  */
      //  $data['warehouseRowList'] = \App\Model\Warehouselocation::where('type', 'R')->where('status', '1')->orderby('name', 'asc')->get();

      
            $data['id'] = $id;

            return view('Administrator.schedulepickup.detail', $data);
       

    }

    


}
