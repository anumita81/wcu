<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SplitShipmentContent;
//use App\libraries\helpers;
//use App\libraries\dbHelpers;
use Auth;

use Illuminate\Contracts\Auth\Authenticatable;
use customhelper;
use Config;

class SplitShipmentController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index (Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Splitshipmentrequests'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $sortOrder = '';
        $splitShipmentObj = new SplitShipmentContent();
        $searchDisplay = $this->_perPage;
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $sortOrder = \Input::get('sortOrder', 'asc');
        }

        if($sortOrder == '' || $sortOrder == 'desc')
          $sortOrder = 'asc'; 
        else
           $sortOrder = 'desc';

        $data['sortOrder'] = $sortOrder;
        $data['page'] = 1;
        $data['splitShipmentTypes'] = $splitShipmentObj->splitShipmentTypes();
        $data['searchDisplay'] = $searchDisplay;
        $data['records'] = $splitShipmentObj->getData($sortOrder);
        $data['pageTitle'] = 'Split Shipment Requests';
        $data['title'] = "Split Shipment Requests :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText'=>array('Site Content','Split Shipment Requests'),'contentTitle'=>'Split Shipment Requests','pageInfo'=>'This section allows you to manage split shipment requests contents');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.splitshipment.index',$data);
    }

    public function addeditsplitshipment($id = -1, $page = 1,Request $request) {

        $data = array();

        $splitShipmentObj = new SplitShipmentContent();


        $data['pageTitle'] = 'Split Shipment Requests';
        $data['title'] = "Split Shipment Requests :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText'=>'Split Shipment Requests','contentTitle'=>'Split Shipment Requests','pageInfo'=>'This section allows you to manage split shipment requests contents');
        $data['page'] = $page;

        if($id != -1)
        {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $record = SplitShipmentContent::find($id);
            $data['record'] = $record;
        }
        else
        {
            $data['action'] = 'Add';
            $data['record'] = new SplitShipmentContent;
        }
        $data['splitShipmentTypes'] = $splitShipmentObj->splitShipmentTypes();
        return view('Administrator.splitshipment.addeditsplitshipment', $data);
    }

    public function addeditsplitshipmentrecord($id = -1,$page = 1,Request $request) {

        $splitShipmentObj = new SplitShipmentContent();
        
        $postData = $request->input('data');
        $validator = Validator::make($request->input('data'), [
                    'splitShipmentType' => 'required',
                    'shipmentTypeContents' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

        if($id != '-1')
            $splitShipmentObj = $splitShipmentObj->find($id);

        $splitShipmentObj->splitShipmentType = $postData['splitShipmentType'];
        $splitShipmentObj->shipmentTypeContents = $postData['shipmentTypeContents'];
        if($splitShipmentObj->save())
            return redirect(url('/administrator/splitshipmentrequests'))->with('successMessage', 'Information saved successfuly.');
        }
    }

    public function deletesplitshipment($id= -1, $page = 1, Request $request) {

        if($id!='-1')
        {
            $splitShipmentObj = new SplitShipmentContent();
            $splitShipmentObj = $splitShipmentObj->find($id);
            
            if($splitShipmentObj->delete())
            {
                return redirect(url('/administrator/splitshipmentrequests'))->with('successMessage', 'Information deleted successfuly.');
            }
            else
            {
                $splitShipmentObj->errors();
            }

        }
    }

    public function editstatus($id = '-1',$status) {

        $splitShipmentObj = new SplitShipmentContent();
        if($id!=-1)
            $splitShipmentObj = $splitShipmentObj->find($id);
        $splitShipmentObj->status = $status;

        if($splitShipmentObj->save())
            return json_encode(array('updated'=>true));
        else
            return json_encode(array('updated'=>false));
    }



}
