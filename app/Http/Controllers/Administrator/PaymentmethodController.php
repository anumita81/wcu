<?php
namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Paymentmethod;
use App\Model\Paymentgateway;
use App\Model\Country;
use App\Model\Paymenttaxsettings;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use App\Model\Defaultcurrency;
use App\Model\Paymentgatewaysettings;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Excel;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Hash;
use Mail;
use customhelper;

class PaymentmethodController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Paymentmethod'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {

            /* GET POST VALUE  */           
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('PAYMENTMETHODDATA');
            \Session::push('PAYMENTMETHODDATA.searchDisplay', $searchDisplay);
            \Session::push('PAYMENTMETHODDATA.field', $field);
            \Session::push('PAYMENTMETHODDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('PAYMENTMETHODDATA.field');
            $sortType = \Session::get('PAYMENTMETHODDATA.type');
            $searchDisplay = \Session::get('PAYMENTMETHODDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'orderby';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }


        /* BUILD SORTING ARRAY */
        $sort = array(
            'id' => array('current' => 'sorting'),
            'paymentMethod' => array('current' => 'sorting'),
            'orderby' => array('current' => 'sorting')
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH USER LIST  */
        $paymentData = Paymentmethod::getPaymentMethodList($param, '');
        

        \Session::forget('PAYMENTMETHODDATA');
        
        
        /* FETCH COUNTRY LIST  */
        $data['countryName'] = '';
        foreach($paymentData as $payment)
        {
            $countryId = explode(',', $payment['countries']);
            
            if(!empty($countryId))
            {
                $countryName = array();
                if(count($countryId)>1)
                {
                    for($i=0; $i<count($countryId); $i++)
                    {

                        $countryName[] = Country::select('name')->where('id', $countryId[$i])->where('status', '1')->orderby('name', 'asc')->get()->toArray();

                    } 
                }else{
                    $countryName = Country::select('name')->where('id', $countryId[0])->where('status', '1')->orderby('name', 'asc')->get()->toArray();
                }
                
            }
           
            if(!empty($countryName)) {
                $countrydata = array();
                if(count($countryName)>1)
                {
                   foreach($countryName as $country)
                        {
                         
                            //$countrydata[] = $country[0]['name']; 
                        
                           // echo "<pre>";print_r($countrydata);
                        } 
                }else{
                      
                    
                   // $countrydata[] = $countryName[0]['name'];
                }
                     
               
               
               
               
               //echo "<pre>"; print_r($data['paymentData']['countryName']);
            }  
            
           $data['countryName'] = implode(',', $countrydata);
            
        }
        
        //$data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

       

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Payment Method";
        $data['contentTop'] = array('breadcrumbText' => 'Payment Method', 'contentTitle' => 'Payment Method', 'pageInfo' => 'This section allows you to manage payment method');
        $data['pageTitle'] = "Payment Method";
        $data['page'] = $paymentData->currentPage();
        $data['paymentData'] = $paymentData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        //echo "<pre>"; print_r($data['paymentData']->toArray());die;
        return view('Administrator.paymentmethod.index', $data);
    }

         /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get()->toArray();
       
        array_unshift($data['countryList'] , array('id'=>"All", 'code'=>'', 'codeA3'=>'', 'codeN3'=>'', 'region'=>'', 'name'=>'Select All', 'status'=>'', 'modifiedOn'=>'', 'modifiedBy'=>''));
       
        
        $paymentMethodName = Config::get('constants.paymentMethod.PAYMENTMETHOD');
        $data['paymentMethodName'] = unserialize($paymentMethodName);
        $data['paymentGatewayList'] = Paymentgateway::orderby('id', 'asc')->where('applicablePaymentMethodId',$id)->get();
        
        $data['title'] = "Administrative Panel :: Payment Method";
        $data['contentTop'] = array('breadcrumbText' => 'Payment Method', 'contentTitle' => 'Payment Method', 'pageInfo' => 'This section allows you to add/edit payment method');
        $data['page'] = !empty($page) ? $page : '1';
        
      
        $data['id'] = $id;
        $data['action'] = 'Edit';
        $paymentmethod = Paymentmethod::find($id);
        $data['paymentmethod'] = $paymentmethod;
        $data['countries'] = explode(',', $paymentmethod['countries']);
            
        
        return view('Administrator.paymentmethod.addedit', $data);
    }


    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $paymentmethod = new Paymentmethod;
        
        if($request->paymentGatewayRequired == 'Y')
        {
             $validator = Validator::make($request->all(), [
                    'countries'=> 'required',
                    'position'=> 'required',
                    'minallowedvalue'=> 'required',
                    'maxallowedvalue'=> 'required'
            ]);
        }else{
            $validator = Validator::make($request->all(), [
                    'countries'=> 'required',
                    'position'=> 'required',
                    'minallowedvalue'=> 'required',
                    'maxallowedvalue'=> 'required'
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $paymentmethodAr = Paymentmethod::find($id);
                $paymentmethodAr->updatedBy = Auth::user()->id;
                $paymentmethodAr->updatedOn = Config::get('constants.CURRENTDATE');
            }

            if($request->paymentGatewayRequired == 'Y')
            {
                $paymentmethodAr->paymentDetails = $request->paymentDetails;
                $paymentmethodAr->paymentGatewayId = $request->paymentGatewayId;
            }            
           
            if(!empty($request->countries))
            {
                if(count($request->countries)>1)
                {
                    $paymentmethodAr->countries = implode(',', $request->countries);
                }else{
                    $paymentmethodAr->countries = implode('', $request->countries);
                }
                
            }

            if(!empty($request->position))
            {
                $paymentmethodAr->orderby = $request->position;
            }else{
                $paymentmethodAr->orderby = 0;
            }

            if($request->minallowedvalue < $request->maxallowedvalue)
            {
                $paymentmethodAr->minallowedvalue = $request->minallowedvalue;
                $paymentmethodAr->maxallowedvalue = $request->maxallowedvalue;
            }else{
                return redirect('administrator/paymentmethod')->with('errorMessage', 'Minimum value must not be greater than Maximum value.');
            }

            $paymentmethodAr->paymentDetails = (!empty($request->paymentDetails) ? $request->paymentDetails : "" );
            $paymentmethodAr->save();
            $paymentmethodId = $paymentmethodAr->id;

            return redirect('administrator/paymentmethod')->with('successMessage', 'Payment method saved successfuly.');
        }
    }
    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Paymentmethod::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/paymentmethod/?page=' . $page)->with('successMessage', 'Payment Method deleted successfully.');
            } else {
                return \Redirect::to('administrator/paymentmethod/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/paymentmethod/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    
    /**
     * Method used to configure
     * @param integer $id
     * @param integer $page
     * @return type
     */    
    public function configurationsettings($id , $page = '')
    {
       
        $data = array(); 
        
        $data['title'] = "Administrative Panel :: Payment Gateway Configuration";
        $data['contentTop'] = array('breadcrumbText' => 'Payment Gateway', 'contentTitle' => 'Payment Gateway', 'pageInfo' => 'This section allows you to edit payment gateway informations');
        $data['page'] = !empty($page) ? $page : '1';
        $data['pageTitle'] = "Payment Method";
        if($id != 9 && $id != 10 && $id !=11)
        $data['settings'] = Paymentmethod::configurationSettings($id)->toArray();

        if(empty($data['settings'])){
            /* LIKE PAYSTACK */
            $data['settings'] = Paymentmethod::where('paymentGatewayId', $id)->get();
            $data['Paymentgatewaysettings'] = Paymentgatewaysettings::where('paymentGatewayId', $id)->get();
        }
       
        $data['currencyList'] = Defaultcurrency::orderby('id', 'asc')->get();
        
        $data['id'] = $id;
        
        //echo "<pre>"; print_r($data['settings']);
        
        return view('Administrator.paymentmethod.configuration', $data);        
    }
    
    
    public function saveconfiguration($id, Request $request)
    {
        $data = array();
        
       // print_r($request->configuration); die;
       // $gatewayConfiguration = new Paymentgatewaysettings;  

        if($request->configuration['paymentGatewayId'] == 1){
            for($i=0; $i< count($request->configuration)-1; $i++)
            {
               $paymentmethodAr = Paymentgatewaysettings::where('paymentGatewayId', $request->configuration['paymentGatewayId'])->get()->toArray();
               $paymentmethodAr[$i]['configurationValues'] = $request->configuration[$i];
               $paymentmethodAr[$i]['updatedBy'] = Auth::user()->id;
               $paymentmethodAr[$i]['updatedOn'] = Config::get('constants.CURRENTDATE');
               
              // print_r($paymentmethodAr[$i]); die;
               
              Paymentgatewaysettings:: where('id', $paymentmethodAr[$i]['id'])->update($paymentmethodAr[$i]); 
            }
        }
        if($request->configuration['paymentGatewayId'] == 2){
            for($i=6; $i<= 9; $i++)
            {  #dd($request->configuration[$i]);
               $paymentmethodAr = Paymentgatewaysettings::where('paymentGatewayId', $request->configuration['paymentGatewayId'])->get()->toArray();
               $paymentmethodAr[$i]['configurationValues'] = $request->configuration[$i];
               $paymentmethodAr[$i]['updatedBy'] = Auth::user()->id;
               $paymentmethodAr[$i]['updatedOn'] = Config::get('constants.CURRENTDATE');
               
               #print_r($paymentmethodAr[$i]); die;
               
              Paymentgatewaysettings:: where('id', $i)->update($paymentmethodAr[$i]); 
            }
        }

        if($request->configuration['paymentGatewayId'] == 9){
            for($i=10; $i<= 13; $i++)
            { 
                $paymentmethodAr = Paymentgatewaysettings::where('paymentGatewayId', $request->configuration['paymentGatewayId'])->get()->toArray();
                $paymentmethodAr[$i]['configurationValues'] = $request->configuration[$i];
                $paymentmethodAr[$i]['updatedBy'] = Auth::user()->id;
                $paymentmethodAr[$i]['updatedOn'] = Config::get('constants.CURRENTDATE');
           
                Paymentgatewaysettings:: where('id', $i)->update($paymentmethodAr[$i]); 
            }
        }

        if($request->configuration['paymentGatewayId'] == 10){
            for($i=19; $i<= 20; $i++)
            { 
                $paymentmethodAr = Paymentgatewaysettings::where('paymentGatewayId', $request->configuration['paymentGatewayId'])->get()->toArray();
                $paymentmethodAr[$i]['configurationValues'] = $request->configuration[$i];
                $paymentmethodAr[$i]['updatedBy'] = Auth::user()->id;
                $paymentmethodAr[$i]['updatedOn'] = Config::get('constants.CURRENTDATE');
           
                Paymentgatewaysettings:: where('id', $i)->update($paymentmethodAr[$i]); 
            }
        }
        
        if($request->configuration['paymentGatewayId'] == 11){
            for($i=21; $i<= 25; $i++)
            { 
                $paymentmethodAr = Paymentgatewaysettings::where('paymentGatewayId', $request->configuration['paymentGatewayId'])->get()->toArray();
                $paymentmethodAr[$i]['configurationValues'] = $request->configuration[$i];
                $paymentmethodAr[$i]['updatedBy'] = Auth::user()->id;
                $paymentmethodAr[$i]['updatedOn'] = Config::get('constants.CURRENTDATE');
           
                Paymentgatewaysettings:: where('id', $i)->update($paymentmethodAr[$i]); 
            }
        }

        
        //$paymentmethodAr->save();
        
       return redirect('administrator/paymentmethod')->with('successMessage', 'Configuration saved successfuly.'); 
    }
    
    public function addtax($id = '0', $page = '')
    {
         $data = array();
          $data['id'] = $id;
        
         
         $data['title'] = "Administrative Panel :: Payment Method";
         $data['contentTop'] = array('breadcrumbText' => 'Payment Method', 'contentTitle' => 'Payment Method', 'pageInfo' => 'This section allows you to add/edit payment method');
         $data['page'] = !empty($page) ? $page : '1';
        
         $paymentmethod = Paymentmethod::find($id);
         $data['paymentmethod'] = $paymentmethod;
         $data['countries'] = explode(',', $paymentmethod['countries']);
         
         /* To Find Countrywise tax */
         for($i=0; $i< count($data['countries']); $i++)
         {
            $country[] = Country::select(array('name','id'))->where('id', $data['countries'][$i])->where('status', '1')->orderby('name', 'asc')->get()->toArray();  
            
         }
         
         $data['countryList'] =$country;
        
         return view('Administrator.paymentmethod.addtax', $data);
        
    }
    
    public function savetax($id, $page, Request $request) {


        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = $id;
        
        $paytax = [];
        
        $validator = Validator::make($request->all(), [
                    'taxvalue' => 'required',
                    'type' => 'required'
        ]);
        

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {


          if(empty($id))
          {
           
            for($i=0; $i<count($request->countryId); $i++)
             {
                
                  $countryExist = Paymenttaxsettings::where('paymentMethodId', $id)->where('countryId', $request->countryId[$i])->get()->toArray(); 
                 
                 if(count($countryExist)== 0){
                   $paytax[]= ['paymentMethodId' => $request->paymentMethodId,
                 'createdBy' => Auth::user()->id,
                 'createdOn' => Config::get('constants.CURRENTDATE'),
                 'countryId' => $request->countryId[$i],
                 'taxValue' => $request->taxvalue[$i],
                 'taxtype' => $request->type[$i]];  
                 }
                else{
                    return redirect('administrator/paymentmethod')->with('errorMessage', 'Tax already present for this country.');
                } 
                 
             }

             Paymenttaxsettings::insert($paytax);

          } else{

            $paymentTaxSettings = Paymenttaxsettings::where('paymentMethodId', $request->paymentMethodId)->delete();

            for($i=0; $i<count($request->countryId); $i++)
             {

               $paytax[]= ['paymentMethodId' => $request->paymentMethodId,
                 'createdBy' => Auth::user()->id,
                 'createdOn' => Config::get('constants.CURRENTDATE'),
                 'countryId' => $request->countryId[$i],
                 'taxValue' => $request->taxvalue[$i],
                 'taxtype' => $request->type[$i]]; 




             }

              Paymenttaxsettings::insert($paytax);

          }
                      

           return redirect('administrator/paymentmethod')->with('successMessage', 'Tax saved successfuly.');
        }
       
       // print_r($request->all()); die;

    }
    /**
     * Method used to change status
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Paymentmethod::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/paymentmethod/?page=' . $page)->with('successMessage', 'Payment method status changed successfully.');
            } else {
                return \Redirect::to('administrator/paymentmethod/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/warehouse/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    /**
    * Method used to save ussdconfiguration
     * @param integer $id, $page
    */

    public function ussdconfigurationsettings()
    {

        $data = array();
        $data['title'] = "Administrative Panel :: Payment Method";
        $data['contentTop'] = array('breadcrumbText' => 'Payment Method', 'contentTitle' => 'Payment Method', 'pageInfo' => 'This section allows you to add/edit payment method');
        $page = !empty($page) ? $page : '1';
        $data['pageTitle'] = "Payment Method";

        $data['paymentMethodId'] = 0;
        $data['page'] = 0;

        $data['ussdconfiguration'] = \App\Model\UssdPaymentSettings::where('deleted','0')->get();



        return view('Administrator.paymentmethod.ussdconfiguration', $data);

    }
    public function addussdconfiguration(Request $request)
    {
        //print_r($request->all()); die;
        if (\Request::isMethod('post')) {
            $data = array();

            $paymentSettings = new \App\Model\UssdPaymentSettings;

            for ($i = 0; $i < count($request->data); $i++) {
                $name = $request->data[$i]['name'];

                $paymentSettings[$name] = $request->data[$i]['value'];
            }
                 $paymentSettings->createdOn = Config::get('constants.CURRENTDATE');
                 $paymentSettings->createdBy = Auth::user()->id;
                $paymentSettings->save();
            

            return 1;
        }
    }

    public function deleteussdconfiguration(Request $request)
    {
        if (\Request::isMethod('post')) {

            

            \App\Model\UssdPaymentSettings::where('id', $request->id)->update(['deleted' => '1']);

            return 1;

        }
    }

    public function saveussdconfiguration($id, Request $request)
    {
        
        if (\Request::isMethod('post')) {
            $data = array();

            $paymentSettings = \App\Model\UssdPaymentSettings::find($id);

            for ($i = 0; $i < count($request->data); $i++) {
                $name = $request->data[$i]['name'];

                $paymentSettings[$name] = $request->data[$i]['value'];
            }
                 $paymentSettings->modifiedOn = Config::get('constants.CURRENTDATE');
                 $paymentSettings->modifiedBy = Auth::user()->id;
                 $paymentSettings->save();
            

            return 1;
        }

    }
    

}

 
