<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Procurement;
use App\Model\Procurementitem;
use App\Model\Procurementitemstatus;
use App\Model\Automake;
use App\Model\Automodel;
use App\Model\Autowebsite;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\Autoshipment;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;
use PDF;
use Mail;

class AutopickupController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {
        \Session::forget('AUTOPICKUPDATA');
        $searchAutoPickupArr = array(
            'unit' => '',
            'status' => '',
            'user' => '',
            'vinNumber' => '',
            'make' => '',
            'model' => '',
            'year' => '',
            'color' => '',
            'id' =>'',
        );

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Autopickup'), Auth::user()->id); // call the helper function
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchAutoPickup = \Input::get('searchAutoPickup', $searchAutoPickupArr);
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('AUTOPICKUPDATA');
            \Session::push('AUTOPICKUPDATA.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('AUTOPICKUPDATA.searchByDate', $searchByDate);
            \Session::push('AUTOPICKUPDATA.searchAutoPickup', $searchAutoPickup);
            \Session::push('AUTOPICKUPDATA.searchDisplay', $searchDisplay);
            \Session::push('AUTOPICKUPDATA.field', $field);
            \Session::push('AUTOPICKUPDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchAutoPickup'] = $searchAutoPickup;
        } else {
            $sortField = \Session::get('AUTOPICKUPDATA.field');
            $sortType = \Session::get('AUTOPICKUPDATA.type');
            $searchDisplay = \Session::get('AUTOPICKUPDATA.searchDisplay');
            $searchByCreatedOn = \Session::get('AUTOPICKUPDATA.searchByCreatedOn');
            $searchByDate = \Session::get('AUTOPICKUPDATA.searchByDate');
            $searchAutoPickup = \Session::get('AUTOPICKUPDATA.searchAutoPickup');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchAutoPickup'] = !empty($searchAutoPickup) ? $searchAutoPickup[0] : $searchAutoPickupArr;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'firstName' => array('current' => 'sorting'),
            'email' => array('current' => 'sorting'),
            'websiteName' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
            'status' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH AUTO PICKUP LIST  */
        $autopickupData = Procurement::getautoPickupList($param);

        $data['allStatus'] = Procurement::allStatus();

        /* Fetch Make List */
        $data['makeList'] = Automake::where('deleted', '0')->get();
        if (!empty($param['searchAutoPickup']['make'])) {
            $data['modelList'] = Automodel::where('makeId', $param['searchAutoPickup']['make'])->where('deleted', '0')->get();
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Book a Car For Me Requests";
        $data['contentTop'] = array('breadcrumbText' => 'Book a Car For Me Requests', 'contentTitle' => 'Book a Car For Me Requests', 'pageInfo' => 'This section allows you to manage the Book a Car For Me request details');
        $data['pageTitle'] = "Book a Car For Me Requests";
        $data['autopickupData'] = $autopickupData;
        $data['page'] = $autopickupData->currentPage();
        $data['searchData'] = $param;
        //dd($data['searchData']);
        $data['sort'] = $sort;
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavAuto', 'menuSub' => '', 'menuSubSub' => 'leftNavCarPickupQuote9');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.autopickup.index', $data);
    }

    /**
     * Method to view car details
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function viewcardetails($id = '0', $page = '') {
        $data['pageTitle'] = "View Car Details";
        $data['id'] = $id;
        $data['carData'] = Procurement::getSingleCarDetails($id);

        return view('Administrator.autopickup.cardetails', $data);
    }

    /**
     * Method to manage shipment status
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function changestatus($id = '0', $page = '', Request $request) {
        if (\Request::isMethod('post')) {
            $autopickup = new Procurement;
            $validator = Validator::make($request->all(), [
                        'status' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            } else {
                if (!empty($id)) {
                    $autopickup = Autopickuprequest::find($id);
                    $autopickup->status = $request->status;
                    $autopickup->modifiedBy = Auth::user()->id;
                    $autopickup->modifiedOn = Config::get('constants.CURRENTDATE');
                    $autopickup->save();

                    return \Redirect::to('administrator/autopickup/?page=' . $page)->with('successMessage', 'Auto pickup status updated successfully.');
                }
            }
        }

        /* SET DATA FOR VIEW  */

        $data['pageTitle'] = "Change Status";
        $data['id'] = $id;
        $data['page'] = $page;

        $data['carData'] = Procurement::find($id);

        return view('Administrator.autopickup.changestatus', $data);
    }

    /**
     * Method to view request details
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function view($id = '0', $page = '') {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Autopickup'), Auth::user()->id); // call the helper function
        if ($findRole['canEdit'] == 0 || $findRole['canAdd'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Book a Car For Me Requests";
        $data['contentTop'] = array('breadcrumbText' => 'Book a Car For Me Requests', 'contentTitle' => 'Book a Car For Me Requests', 'pageInfo' => 'This section allows you to manage the Book a Car For Me request details');
        $data['pageTitle'] = "View Request Details";
        $data['procurementId'] = $id;
        $data['page'] = $page;
        $data['autopickupData'] = Procurement::getPickupRequestDetails($id)->toArray();


        /* Fetch Make List */
        $data['makeList'] = Automake::where('deleted', '0')->get()->toArray();

        /* Fetch Website List */
        $data['websiteList'] = Autowebsite::where('deleted', '0')->get()->toArray();

        $data['allItemStatus'] = Procurementitem::allStatus();
        $data['procurementStatus'] = Procurement::allStatus();
        $data['allItemStatusIndexes'] = array_keys($data['allItemStatus']);

        $data['statusLog'] = Procurementitemstatus::getOrderStatusProgress($id);
        //dd($data['statusLog']);

        /*  FETCH PROCUREMENT ITEM DETAILS */
        $data['autopickupData']['items'] = Procurementitem::getCarDetails($id)->toArray();
        /* Fetch Invoices */
        $data['autopickupData']['invoices'] = \App\Model\Invoice::where('procurementId', $id)->where('type', 'buycarforme')->get();
        
        $data['notifiedMessages'] = \App\Model\Autoshipmentcomment::where('shipmentId', $id)->where('type', 'B')->orderBy('id', 'desc')->get()->toArray();

        //echo "<pre>"; print_r($data['autopickupData']); die;



        return view('Administrator.autopickup.view', $data);
    }

    /**
     * Method used to clear search history
     * @return html
     */
    public function showall() {
        \Session::forget('AUTOPICKUPDATA');
        return \Redirect::to('administrator/autopickup');
    }

    /**
     * Method used to get model list according to make Id
     * @param int $makeId
     * @return json
     */
    public function getmodellist($makeId) {
        $result = Automodel::where('makeId', $makeId)->where('deleted', '0')->get()->toArray();

        echo json_encode($result);
    }

    /**
     * Method to get location
     * @return Json
     */
    public function getLocation(Request $request) {
        $locationName = $request->q;

        $getCity = Procurement::getCityState($locationName);

        echo json_encode($getCity);
    }

    /**
     * Method to add new car details
     * @param object $request
     * @return boolean
     */
    public function adddeliveryitem(Request $request) {
        if (\Request::isMethod('post')) {
            $autoShipmentItem = new Procurementitem;

            // print_r($request->all()); die;

            for ($i = 0; $i < count($request->data); $i++) {
                $name = $request->data[$i]['name'];

                if ($name == 'procurementId')
                    $procurementId = $request->data[$i]['value'];

                if ($name != 'addLocationId') {
                    $autoShipmentItem->$name = $request->data[$i]['value'];
                } else if ($name == 'year') {
                    $autoShipmentItem->$name = \Carbon\Carbon::parse($request->data[$i]['value'])->format('Y');
                } else {
                    $location = explode('-', $request->data[$i]['value']);
                    $autoShipmentItem->pickupStateId = $location[1];
                    $autoShipmentItem->pickupCityId = $location[0];
                }
            }
            /* SAVE PROCUREMENT DETAILS */

            $autoShipmentItem->save();

            return $autoShipmentItem->id;
        }
    }

    /**
     * Method to update new car details
     * @param object $request
     * @return boolean
     */
    public function editdeliveryitem(Request $request) {
        if (\Request::isMethod('post')) {
            $autoShipmentItem = array();

            // print_r($request->all()); 

            for ($i = 0; $i < count($request->data); $i++) {
                $name = $request->data[$i]['name'];

                if ($name == 'procurementItemId') {
                    $id = $request->data[$i]['value'];
                } else if ($name != 'editLocationId' && $name != 'automodelId') {
                    $autoShipmentItem[$name] = $request->data[$i]['value'];
                } else if ($name == 'year') {
                    $autoShipmentItem[$name] = \Carbon\Carbon::parse($request->data[$i]['value'])->format('Y');
                } else if ($name == 'editLocationId') {

                    $location = explode('-', $request->data[$i]['value']);
                    //print_r($location); 
                    $autoShipmentItem['pickupStateId'] = $location[1];
                    $autoShipmentItem['pickupCityId'] = $location[0];
                }
            }

            /* SAVE PROCUREMENT DETAILS */
            Procurementitem::where('id', $id)->update($autoShipmentItem);

            return 1;
        }
    }

    /**
     * Method used to fetch & display package details
     * @param integer $id
     * @return string
     */
    public function getitemdetails($id) {

        /* Fetch Make List */

        $data['makeList'] = Automake::where('deleted', '0')->get()->toArray();

        /* Fetch Website List */

        $data['websiteList'] = Autowebsite::where('deleted', '0')->get()->toArray();

        /*  FETCH PROCUREMENT ITEM DETAILS */
        $data['procurement']['items'] = Procurementitem::getCarDetails($id);

        return view('Administrator.autopickup.itemdetails', $data);
    }

    /**
     * Method used to delete procurement item
     * @param integer $id
     * @return boolean
     */
    public function deletedeliveryitem($id) {
        if (!empty($id)) {
            $createrModifier = Auth::user()->email;
            if (Procurementitem::deleteRecord($id, $createrModifier)) {
                /* CALCULATE TOTAL ITEM COST */
                $procurementItem = new Procurementitem;
                $procurementItem = Procurementitem::find($id);
                //Procurement::calculateCost($procurementItem->procurementId);

                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Method used to open pop up to upload image
     * @param int $procurementItemId
     */
    public function uploadcarimage($itemId, $page) {

        $data = array();

        $data['itemId'] = $itemId;
        $data['page'] = $page;

        $procurement = Procurementitem::find($itemId)->toArray();



        $data['procurementId'] = $procurement['procurementId'];

        $data['carImage'] = $procurement['itemImage'];


        return view('Administrator.autopickup.uploadimage', $data);
    }

    /**
     * Method used to upload and save car image
     * @param int $id, $procurementItemId
     * @param $request
     */
    public function saveimage($id, $procurementId, $page, Request $request) {
        $data = array();

        $data['procurementId'] = !empty($procurementId) ? $procurementId : '1';
        $data['id'] = 0;


        $autoShipmentItem = array();

        $validator = Validator::make($request->all(), [
                    'carimage' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

            if ($request->hasFile('carimage')) {
                $image = $request->file('carimage');
                $name = time() . '_' . $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/auto/carpickup/1');
                $image->move($destinationPath, $name);
            } else {
                $name = $request->carimage;
            }

            $autoShipmentItem['itemImage'] = $name;

            $saveData = Procurementitem::where('id', $id)->update($autoShipmentItem);

            return redirect('/administrator/autopickup/view/' . $procurementId . '/' . $page)->with('successMessage', 'Car image saved successfuly.');
        }
    }

    public function updateprocurementitemstatus(Request $request) {
        $procurementItemId = $request->procurementItemId;
        $status = $request->status;

        $procurementItem = Procurementitem::find($procurementItemId);
        $procurementDetails = Procurement::find($procurementItem->procurementId);

        /* INSERT DATA INTO STATUS LOG TABLE */
        $procurementitemstatus = new Procurementitemstatus;
        $procurementitemstatus->procurementId = $procurementItem->procurementId;
        $procurementitemstatus->procurementItemId = $procurementItemId;
        $procurementitemstatus->oldStatus = $procurementItem->status;
        $procurementitemstatus->status = $status;
        $procurementitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
        $procurementitemstatus->save();

        /* UPDATE PROCUREMENT ITEM TABLE */
        $procurementItem->status = $status;
        if ($status == 'received' && $request->deliveryNotes != '')
            $procurementItem->deliveryNotes = $request->deliveryNotes;
        $procurementItem->save();

        if ($status == 'received') {
            Procurement::where('id', $procurementItem->procurementId)->update(['status' => 'itemreceived']);
        } else if ($status == 'unavailable') {
            Procurement::where('id', $procurementItem->procurementId)->update(['status' => 'rejected']);
        }

        $notificationTemplateKey = "";
        if ($status == "orderplaced") {
            $notificationTemplateKey = "buy_car_order_placed";
        } else if ($status == "pickedup") {
            $notificationTemplateKey = "buy_car_pickedup";
        } else if ($status == "received") {
            $notificationTemplateKey = "buy_car_received";
        }

        $makeInfo = Automake::find($procurementItem->makeId);
        $modelInfo = Automodel::find($procurementItem->modelId);
        $userInfo = \App\Model\User::find($procurementDetails->userId);
        $emailTemplate = \App\Model\Emailtemplate::where('templateKey', $notificationTemplateKey)->first();
        $replace['[NAME]'] = $userInfo->firstName . ' ' . $userInfo->lastName;
        $replace['[MODEL]'] = (!empty($modelInfo->name) ? $modelInfo->name : "N/A");
        $replace['[MAKE]'] = (!empty($makeInfo->name) ? $makeInfo->name : "N/A");
        $replace['[YEAR]'] = $procurementItem->year;
        $replace['[CARURL]'] = $procurementItem->websiteUrl;
        $to = $userInfo->email;

        $isSend = \App\Helpers\customhelper::SendMail($emailTemplate, $replace, $to);

        $toMobile = trim($userInfo->isdCode . $userInfo->contactNumber);
        $smsTemplate = \App\Model\Smstemplate::where('templateKey', $notificationTemplateKey)->first();
        $link = Config::get('constants.frontendUrl');
        $replace['[WEBSITE_LINK]'] = $link;
        $isSendMsg = customhelper::sendMSG($toMobile, $replace, $smsTemplate);
        $replace['[SHIPMENT_STATUS]'] = ucfirst($status);

        \App\Model\User::sendPushNotification($procurementDetails->userId, 'buycar_status_update', Auth::user()->id, $replace);

        /* CHECK AND UPDATE PROCUREMENT STATUS */
        Procurement::updaterejectedstatus($procurementItem->id);

        return 1;
    }

    public function updatepaymentstatus($procurementId, $userId, Request $request) {

        Procurement::where('id', $procurementId)->update(['paymentStatus' => $request->paymentStatus, 'paymentReceivedOn' => Config::get('constants.CURRENTDATE'), 'paidByUserType'=> 'admin', 'paidByUserId' => Auth::user()->id]);

        if ($request->paymentStatus == 'paid') {

            $invoiceData = \App\Model\Invoice::where('procurementId', $procurementId)->where('type', 'buycarforme')->where('invoiceType', 'invoice')->first();

            \App\Model\Invoice::where('procurementId', $procurementId)->update(['deleted' => '1', 'deletedBy' => Auth::user()->id, 'deletedOn' => Config::get('constants.CURRENTDATE')]);


            /*  INSERT DATA INTO INVOICE TABLE */
            $invoiceUniqueId = 'RECP' . $invoiceData->userUnit . '-' . $procurementId . '-' . date('Ymd');
            $invoice = new \App\Model\Invoice;
            $invoice->invoiceUniqueId = $invoiceUniqueId;
            $invoice->procurementId = $procurementId;
            $invoice->type = 'buycarforme';
            $invoice->invoiceType = 'receipt';
            $invoice->userUnit = $invoiceData->userUnit;
            $invoice->userFullName = $invoiceData->userFullName;
            $invoice->userEmail = $invoiceData->userEmail;
            $invoice->userContactNumber = $invoiceData->userContactNumber;
            $invoice->billingName = $invoiceData->billingName;
            $invoice->billingEmail = $invoiceData->billingEmail;
            $invoice->billingAddress = $invoiceData->billingAddress;
            $invoice->billingAlternateAddress = $invoiceData->billingAlternateAddress;
            $invoice->billingCity = isset($invoiceData->billingCity) ? $invoiceData->billingCity : '';
            $invoice->billingState = isset($invoiceData->billingState) ? $invoiceData->billingState : '';
            $invoice->billingCountry = $invoiceData->billingCountry;
            $invoice->billingZipcode = $invoiceData->billingZipcode;
            $invoice->billingPhone = $invoiceData->billingPhone;
            $invoice->billingAlternatePhone = $invoiceData->billingAlternatePhone;
            $invoice->totalBillingAmount = $invoiceData->totalBillingAmount;
            $invoice->invoiceParticulars = $invoiceData->invoiceParticulars;
            $invoice->paymentMethodId = $invoiceData->paymentMethodId;
            $invoice->paymentStatus = 'paid';
            $invoice->createdOn = Config::get('constants.CURRENTDATE');
            $invoice->save();
            $invoiceId = $invoice->id;

            if (isset($invoiceId)) {
                $data['invoice'] = \App\Model\Invoice::find($invoiceId);
                $fileName = "Invoice_" . $invoiceUniqueId . ".pdf";
                PDF::loadView('Administrator.autoshipment.buyacarreceipt', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                $to = $invoiceData->userEmail;
                Mail::send(['html' => 'mail'], ['content' => 'Payment Receipt for Buy a car for Me #' . $procurementId], function ($message) use($invoiceUniqueId, $to, $fileName) {
                    $message->from('contact@shoptomydoor.com', 'Shoptomydoor');
                    $message->subject("$invoiceUniqueId - Receipt Details");
                    $message->to($to);
                    $message->attach(public_path('exports/invoice/' . $fileName));
                });
            }

            $paymentTransaction = \App\Model\Paymenttransaction::where('paidForId', $procurementId)->update(['status' => 'paid']);

        }

        return redirect('/administrator/autopickup/view/' . $procurementId . '/1')->with('successMessage', 'Payment status updated successfully.');
    }

    /**
     * Method used to update procurement item status
     * @param integer $id
     * @return boolean
     */
    public function updateprocurementitemstatusOld(Request $request) {
        $procurementItemIds = $request->procurementItemIds;
        $status = $request->status;

        if (!empty($procurementItemIds)) {
            $procurementItemArr = explode('^', $procurementItemIds);

            foreach ($procurementItemArr as $id) {
                $procurementItem = new Procurementitem;
                $procurementItem = Procurementitem::find($id);

                /* INSERT DATA INTO STATUS LOG TABLE */
                $procurementitemstatus = new Procurementitemstatus;
                $procurementitemstatus->procurementId = $procurementItem->id;
                $procurementitemstatus->procurementItemId = $id;
                $procurementitemstatus->oldStatus = $procurementItem->status;
                $procurementitemstatus->status = $status;
                $procurementitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
                $procurementitemstatus->save();

                /* UPDATE PROCUREMENT ITEM TABLE */
                $procurementItem->status = $status;
                $procurementItem->save();
            }

            /* CHECK AND UPDATE PROCUREMENT STATUS */
            Procurement::updaterejectedstatus($procurementItem->id);

            return 1;
        }
        return 0;
    }

    /**
     * Method used to check and update procurement status
     * @return booelan
     */
    public function checkprocurementstatus($id = '') {
        $procurementUnavailableStatus = Procurementitem::where('status', 'unavailable')->where('deleted', '0')->where('procurementId', $id)->count('id');
        $procurementReceivedStatus = Procurementitem::where('status', 'received')->where('deleted', '0')->where('procurementId', $id)->count('id');

        $totalItems = Procurementitem::where('deleted', '0')->where('procurementId', $id)->count('id');

        if (!empty($procurementReceivedStatus) && ($totalItems == $procurementReceivedStatus + $procurementUnavailableStatus)) {
            $procurement = Procurement::find($id);
            if ($procurement->status != 'itemreceived') {
                Procurement::where('id', $id)->update(array('status' => 'itemreceived'));
            }

            return 0;
        } else {
            //  Procurement::where('id', $id)->update(array('status' => 'submitted'));

            return 1;
        }
    }

    /**
     * Method used to display and update receive package info
     * @param integer $id
     * @return string
     */
    public function receiveitem($id, $status, Request $request) {
        if (\Request::isMethod('post')) {
            if (!empty($id)) {
                $procurementItemArr = explode('^', $id);
                $procurementItemData = array();

                foreach ($procurementItemArr as $itemId) {
                    $trackingNumber = 'trackingNumber' . $itemId;
                    $deliveryCompanyId = 'deliveryCompanyId' . $itemId;
                    $receivedDate = 'receivedDate' . $itemId;

                    /* SAVE PROCUREMENT DETAILS */
                    $procurementItemData['status'] = $status;
                    $procurementItemData['trackingNumber'] = $request->$trackingNumber;
                    $procurementItemData['deliveryCompanyId'] = $request->$deliveryCompanyId;
                    $procurementItemData['receivedDate'] = \Carbon\Carbon::parse($request->$receivedDate)->format('Y-m-d');
                    Procurementitem::where('id', $itemId)->update($procurementItemData);

                    /* INSERT DATA INTO STATUS LOG TABLE */
                    $procurementItem = Procurementitem::find($itemId);
                    $procurementitemstatus = new Procurementitemstatus;
                    $procurementitemstatus->procurementId = $procurementItem->id;
                    $procurementitemstatus->procurementItemId = $itemId;
                    $procurementitemstatus->oldStatus = $procurementItem->status;
                    $procurementitemstatus->status = $status;
                    $procurementitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
                    $procurementitemstatus->save();
                }
                return 1;
            } else {
                return 0;
            }
        }

        $data['id'] = $id;
        $data['status'] = $status;
        $data['pageTitle'] = "Receive Package in Warehouse";

        /* FETCH DELIVERY COMPANY LIST  */
        //$data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();

        return view('Administrator.autopickup.receiveitem', $data);
    }

    /**
     * Method used to fetch & display the shipment details
     * @param integer $id
     * @return string
     */
    public function viewtrackingdetails($id) {
        $data['pageTitle'] = "View Tracking Details";
        /*  FETCH PROCUREMENT DETAILS */
        $data['procurement'] = Procurementitem::find($id);



        $data['procurement']['receivedDate'] = \Carbon\Carbon::parse($data['procurement']['receivedDate'])->format('d-m-Y');

        return view('Administrator.autopickup.viewtrackingdetails', $data);
    }

    /**
     * Method used to display notification message
     * @param integer $messageId
     * @param integer $procurementId
     * @return string
     */
    public function notifycustomer($procurementId, Request $request) {
        if (\Request::isMethod('post')) {

            $autopickupissues = new \App\Model\Autopickupissues;
            $autopickupissues->procurementId = $procurementId;
            $autopickupissues->userId = $request->userId;
            $autopickupissues->message = $request->delayMessage;
            $autopickupissues->sentBy = Auth::user()->id; //
            $autopickupissues->sentOn = Config::get('constants.CURRENTDATE');

            $user = \App\Model\User::find($request->userId);
            $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'notification_admin')->first();
            //echo $emailTemplate->templateBody;exit;
            $replace['[NAME]'] = $user['firstName'];
            $replace['[NOTIFICATION]'] = $request->delayMessage;
            $replace['[SHIPMENTID]'] = $procurementId;

            $to = $user['email'];
            $sendMail = customhelper::SendMail($emailTemplate, $replace, $to);

            if ($sendMail) {
                if ($autopickupissues->save()) {
                    return redirect()->back()->with('successMessage', 'Notification sent successfully');
                } else {
                    return redirect()->back()->with('errorMessage', 'Record not added');
                }
            } else {
                return redirect()->back()->with('errorMessage', 'Notification not sent');
            }
        }
    }

    public function createshipment($procurementId) {

        $dataSave = Autoshipment::saveBuyaCar($procurementId);
        if ($dataSave)
            return redirect()->back()->with('successMessage', 'Shipment Created successfully');
        else
            return redirect()->back()->with('errorMessage', 'Something Went wrong');
    }

    /**
     * Method used to print invoice
     * @param integer $id
     * @param integer $invoiceId 
     * @return string
     */
    public function printinvoice($id, $invoiceId) {
        /* Fetch  Invoice Data */
        $data['invoice'] = \App\Model\Invoice::find($invoiceId);
        return view('Administrator.autopickup.printinvoice', $data);
    }
    
    public function addcomment($shipmentId, Request $request) {
        
        $data = array();

        $message = $request->comment;

        $shipmentnotes = new \App\Model\Autoshipmentcomment;

        $shipmentnotes->shipmentId = $shipmentId;
        $shipmentnotes->message = $message;
        $shipmentnotes->type = "B";
        $shipmentnotes->sentBy = Auth::user()->id;
        $shipmentnotes->sentOn = Config::get('constants.CURRENTDATE');

        $userdetail = \App\Model\UserAdmin::select(array('firstname', 'lastname', 'email'))->where('id', Auth::user()->id)->first()->toArray();


        if ($shipmentnotes->save()) {
            $userdetail = array_merge($userdetail, array('sentOn' => date('Y-m-d', strtotime($shipmentnotes->sentOn)), 'noteId' => $shipmentnotes->id, 'shipmentId' => $shipmentId));
            return json_encode($userdetail);
        } else {
            return 0;
        }
    }
    
    public function shownotes($notesId, $shipmentId) {
        $data = array();

        $data['shipmentId'] = $shipmentId;

        $data['notes'] = \App\Model\Autoshipmentcomment::select('message')->where('id', $notesId)->where('type', 'B')->first();

        return view('Administrator.autopickup.shownotes', $data);
    }
    
    public function sendcomment($id, $shipmentId) {
        $data = array();

        $data['shipmentId'] = $shipmentId;

        $data['customerEmailList'] = \App\Model\UserAdmin::select(array('id', 'email'))->where(array('deleted' => '0', 'status' => '1'))->get()->toArray();

        return view('Administrator.autopickup.sendcomment', $data);
    }
    
    public function saveandnotify($shipmentId, Request $request) {

        $shipmentnotes = new \App\Model\Autoshipmentcomment;

        $emailto[] = $request->admin;



        $shipmentnotes->shipmentId = $shipmentId;
        $shipmentnotes->message = $request->warehousemsg;
        $shipmentnotes->type = "B";
        $shipmentnotes->sentBy = Auth::user()->id;
        $shipmentnotes->sentOn = Config::get('constants.CURRENTDATE');

        $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'notification_admin')->first();
        $replace['[NAME]'] = "Admin";
        $replace['[NOTIFICATION]'] = $request->warehousemsg;

        foreach ($emailto[0] as $email) {
            $sendMail = customhelper::SendMail($emailTemplate, $replace, $email);
        }
        if ($sendMail) {
            if ($shipmentnotes->save()) {

                return redirect()->back()->with('successMessage', 'Notes saved and sent successfully');
            } else {

                return redirect()->back()->with('errorMessage', 'Record not added');
            }
        } else {
            return redirect()->back()->with('errorMessage', 'Notes not sent');
        }
    }

}
