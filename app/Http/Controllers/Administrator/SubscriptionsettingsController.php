<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Subscriptionsettings;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Excel;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Hash;
use Mail;
use customhelper;

class SubscriptionsettingsController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function index() {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Subscriptionsettings'), Auth::user()->id); // call the helper function
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $data['settings'] = Subscriptionsettings::all()->toArray();

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        $data['title'] = "Administrative Panel :: Subscription Settings";
        $data['contentTop'] = array('breadcrumbText' => 'Subscription Settings', 'contentTitle' => 'Subscription Settings', 'pageInfo' => 'This section allows you to manage subscription settings');
        $data['pageTitle'] = "Subscription Settings";

        return view('Administrator.subscriptionsettings.index', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata(Request $request) {

        $data = array();

        //Delete Records if exist
        $subscriptionSettings = Subscriptionsettings::count();
        if (!empty($subscriptionSettings)) {
            Subscriptionsettings::getQuery()->delete();
        }


        if (!empty($request->duration)) {
            $count = count($request->duration);
            for ($i = 0; $i < $count; $i++) {
                $subscriptionsettings = new Subscriptionsettings;
                $subscriptionsettings->duration = $request->duration[$i];
                $subscriptionsettings->amount = $request->amount[$i];
                $subscriptionsettings->save();
            }
        }

        return redirect('administrator/subscriptionsettings')->with('successMessage', 'Subscription feature saved successfuly.');
    }

    public function getsubscribtion(Request $request) {
        $data = array();
        $data['count'] = $request->count;

        return view('Administrator.subscriptionsettings.subscribtiondata', $data);
    }

    public function subscriptiondiscount($id, $page, Request $request) {
        $data = array();
        $id = 0;

        $couponDetails = \App\Model\Offer::where('type', 'subscription')->where('deleted', '0')->first();

        if (!empty($couponDetails)) {
            $id = $couponDetails['id'];

            $unserializeBonus = unserialize($couponDetails->bonusValuesSerialized);

            $data['couponDetails']['discount'] = $unserializeBonus['discount'];
            $data['couponDetails']['discountNotExceed'] = $unserializeBonus['discountNotExceed'];
            $data['couponDetails']['couponCode'] = $couponDetails->couponCode;
        } else {
            $data['couponDetails'] = array();
        }

        $data['id'] = $id;
        $data['pageTitle'] = 'Shipping Discount';

        return view('Administrator.subscriptionsettings.subscriptiondiscount', $data);
    }

    public function savediscount($id, $page, Request $request) {
        if (!empty($id)) {
            $offer = \App\Model\Offer::find($id);
        } else {
            $offer = new \App\Model\Offer;
        }

        if ($request->couponCode != '') {
            /* CHECKING DUPLICATE COUPON CODE, IF ANY  */
            $couponCheckedDuplicate = \App\Model\Offer::where('couponCode', $request->couponCode)->where('id', '!=', $id)->first();
            if (!empty($couponCheckedDuplicate)) {
                return \Redirect::back()->withInput(\Input::all())->with('errorMessage', 'Coupon Code already exists');
            }
        }

        $offer->type = 'subscription';
        $offer->status = '1';
        $offer->shortName = (\Input::has('couponCode') ? $request->couponCode : NULL);
        $offer->couponCode = (\Input::has('couponCode') ? $request->couponCode : NULL);
        $offer->modifiedBy = Auth::user()->id;
        $offer->modifiedOn = Config::get('constants.CURRENTDATE');
        $offer->bonusKeyword = Config::get('constants.Offers.' . 21);

        /*  IF DISCOUNT TYPE IS PERCENTAGE, MAKE THE DISCOUNT SHOULD NOT BE EXCEEDED FIELD AS 0  */
        if (!empty($request->discountNotExceed)) {
            $discountNotExceeded = $request->discountNotExceed;
        } else {
            $discountNotExceeded = 0.00;
        }

        $offer->bonusValuesSerialized = serialize(array('discount' => sprintf('%0.2f', $request->discount), 'type' => 'Percent', 'discountNotExceed' => sprintf('%0.2f', $discountNotExceeded), 'exceedType' => 'Yes'));
        $offer->isSetBonus = '1';
        $offer->save();

        if (!empty($id)) {
            $offerdelete = \App\Model\Offercondition::where('offerId', $id)->delete();
        } else
            $id = $offer->id;

        /* ADD OFFER CONDITION DATA  */
        $offerCondition = new \App\Model\Offercondition;
        $offerCondition->offerId = $id;
        $offerCondition->keyword = 'user_subscription';
        $offerCondition->valuesSerialized = serialize(array('value' => 'User Subscription Discount'));
        $offerCondition->jsonValues = json_encode(array("user_subscription" => array("Y")));
        $offerCondition->setNo = 1;
        $offerCondition->createdBy = Auth::user()->id;
        $offerCondition->createdOn = Config::get('constants.CURRENTDATE');
        $offerCondition->save();

        return redirect('administrator/subscriptionsettings')->with('successMessage', 'Subscription discount saved successfuly.');
    }

    public function removediscount($id) {
        if (!empty($id)) {
            $offer = \App\Model\Offer::where('id', $id)->delete();

            return redirect('administrator/subscriptionsettings')->with('successMessage', 'Subscription discount removed successfuly.');
        }
    }

}
