<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;

class UsergroupsController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }
    
    public function index(Request $request) {
        
        $data = array();
        
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.DhlTradetermsManagement'), Auth::user()->id);
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('USERGROUP');
            \Session::push('USERGROUP.searchDisplay', $searchDisplay);
            \Session::push('USERGROUP.field', $field);
            \Session::push('USERGROUP.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('USERGROUP.field');
            $sortType = \Session::get('USERGROUP.type');
            $searchDisplay = \Session::get('USERGROUP.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
            'orders' => array('current' => 'sorting'),
            'status' => array('current' => 'sorting'),
        );
        
        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: User Group Management";
        $data['contentTop'] = array('breadcrumbText' => 'User Group Management', 'contentTitle' => 'User Group Management', 'pageInfo' => 'This section allows you to manage Users Groups');
        $data['pageTitle'] = "User Group Management";
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        
        $userGroupData = \App\Model\Groupdetails::getUserGroupList($param);
        if (count($userGroupData) > 0) {
            $data['page'] = $userGroupData->currentPage();
        } else {
            $data['page'] = 1;
        }
        $data['userGroupData'] = $userGroupData;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.usergroups.index', $data);
    }
    
    public function changestatus($id,$page,Request $request) {
        
        $data = array();
        
        if(\Request::isMethod('post')) {
            
            $status = $request->groupStatus;
            $groupDetails = \App\Model\Groupdetails::find($id);
            if($status == '1')
            {
                $groupDetails->totalGroupMembers = '1';
            }
            $groupDetails->status = $status;
            $groupDetails->approvedBy = Auth::user()->email;
            $groupDetails->approvedOn = date('Y-m-d h:i:s');
            if($groupDetails->save()) {
                
                if($status == '1')
                {
                    $groupMembers = new \App\Model\Groupmembers;
                    $groupMembers->groupId = $groupDetails->id;
                    $groupMembers->userId = $groupDetails->coordinatorId;
                    $groupMembers->memberType = '2';
                    $groupMembers->status = '1';
                    $groupMembers->joinedOn = date('Y-m-d h:i:s');
                    $groupMembers->save();
                }
                
                return \Redirect::to('administrator/usergroups/?page=' . $page)->with('successMessage', 'Group status updated successfully.');
            }
            else
                return \Redirect::to('administrator/usergroups/?page=' . $page)->with('errorMessage', 'Something went wrong! Please try again later.');
            
        }
        
        $data['pageTitle'] = "Update Status";
        $data['id'] = $id;
        $data['page'] = $page;
        $data['groupStatus'] = \App\Model\Groupdetails::find($id)->status;
        
        return view('Administrator.usergroups.changestatus', $data);
    }
    
    public function assignrepresentative($id = '0', $page = '') {

        $data = array();

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Assign Account Manager";
        $data['contentTop'] = array('breadcrumbText' => 'Assign Account Manager', 'contentTitle' => 'Assign Account Manager', 'pageInfo' => 'This section allows you to assign account manager');
        $data['page'] = !empty($page) ? $page : '1';

        $data['groupId'] = $id;
        $userData = \App\Model\Groupdetails::find($id);
        $repuser = \App\Model\UserAdmin::where('userType', '11')->get();
        $data['repuser'] = $repuser;
        $data['representativeId'] = $userData->groupManagerId;

        $data['action'] = 'Edit';
        $data['pageTitle'] = "Edit User";

        return view('Administrator.usergroups.assignrepresentative', $data);
    }
    
    public function saverepresentative($groupId, $representativeId) {
        $data = array();

        $groupDetails = \App\Model\Groupdetails::find($groupId);

        $groupDetails->groupManagerId = $representativeId;
        
        if ($groupDetails->save()) {
            
            return response()->json(['status' => '1', 'message' => 'Account Manager added!']);
        } else {
            return response()->json(['status' => '0', 'message' => 'Error in operation!']);
        }
    }

    public function editgroupdetails($groupId)
    {
        $data = array();

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Edit Group Details";
        $data['contentTop'] = array('breadcrumbText' => 'Edit Group Details', 'contentTitle' => 'Edit Group Details', 'pageInfo' => 'This section allows you to edit group details');
        $data['page'] = !empty($page) ? $page : '1';
        $data['action'] = 'Edit';
        $data['pageTitle'] = "Edit Group Details";


        $data['id'] = $groupId;
        $data['groupDetails'] = \App\Model\Groupdetails::find($groupId);


        return view('Administrator.usergroups.edit', $data);
    }

    public function savegroupdetails($id, $page=0, Request $request)
    {
        $data = array();

        if(\Request::isMethod('post')) {

            $groupDetails = \App\Model\Groupdetails::find($id);
            
            $groupDetails->expectedMembers = $request->expectedMembers;
            $groupDetails->approvedBy = Auth::user()->email;
            $groupDetails->approvedOn = date('Y-m-d h:i:s');

            if($groupDetails->save()) {                
                
                return \Redirect::to('administrator/usergroups/?page=' . $page)->with('successMessage', 'Group details updated successfully.');
            }
            else{
                return \Redirect::to('administrator/usergroups/?page=' . $page)->with('errorMessage', 'Something went wrong! Please try again later.');
            }
            
        }

    }
    
    public function getgroupmembers($groupId, $page = 0) {
        
        $user = new \App\Model\User();     
        $groupMembersObj = new \App\Model\Groupmembers();
        
        $groupMembers = \App\Model\Groupmembers::select(array("$groupMembersObj->table.*","firstName","lastName","email","unit"))->join($user->table,"$user->table.id","=","$groupMembersObj->table.userId")->where('groupId',$groupId)->get();
        $data = array();
        $data['groupMembers'] = $groupMembers;
        
        return view('Administrator.usergroups.getgroupmembers', $data);
    }
}