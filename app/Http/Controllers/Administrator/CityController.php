<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use Excel;
use ZipArchive;
use Illuminate\Support\Facades\DB;
use customhelper;

class CityController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.City'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchByCountry = \Input::get('searchByCountry', '');
            $searchByState = \Input::get('searchByState', '');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('CITYDATA');
            \Session::push('CITYDATA.searchByCountry', $searchByCountry);
            \Session::push('CITYDATA.searchByState', $searchByState);
            \Session::push('CITYDATA.searchDisplay', $searchDisplay);
            \Session::push('CITYDATA.field', $field);
            \Session::push('CITYDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCountry'] = $searchByCountry;
            $param['searchByState'] = $searchByState;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('CITYDATA.field');
            $sortType = \Session::get('CITYDATA.type');
            $searchByCountry = \Session::get('CITYDATA.searchByCountry');
            $searchByState = \Session::get('CITYDATA.searchByState');
            $searchDisplay = \Session::get('CITYDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'cityName';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchByCountry'] = !empty($searchByCountry) ? $searchByCountry[0] : '';
            $param['searchByState'] = !empty($searchByState) ? $searchByState[0] : '';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'cityName' => array('current' => 'sorting'),
            'countryName' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH CITY LIST  */
        $cityData = City::getCityList($param);

        /* FETCH COUNTRY LIST  */
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

        /* FETCH STATE LIST  */
        $data['stateList'] = array();
        if (isset($param['searchByCountry']) && !empty($param['searchByCountry'])) {
            $data['stateList'] = State::where('status', '1')
                    ->where('deleted', '0')
                    ->where('countryCode', $param['searchByCountry'])
                    ->orderby('name', 'asc')
                    ->get();
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Cities";
        $data['contentTop'] = array('breadcrumbText' => 'Cities', 'contentTitle' => 'Cities', 'pageInfo' => 'This section allows you to manage cities');
        $data['pageTitle'] = "Cities";
        $data['page'] = $cityData->currentPage();
        $data['cityData'] = $cityData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.city.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();
        $data['pageTitle'] = "Cities";
        $data['page'] = !empty($page) ? $page : '1';
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $city = City::find($id);
            $data['city'] = $city;
            return view('Administrator.city.edit', $data);
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['city'] = array();
            return view('Administrator.city.add', $data);
        }
    }

    /**
     * Method to fetch state list
     * @return array
     */
    public function getstatelist($countrycode = '') {
        $stateList = array();
        if (isset($countrycode) && !empty($countrycode)) {
            $stateList = State::where('status', '1')
                    ->where('deleted', '0')
                    ->where('countryCode', $countrycode)
                    ->orderby('name', 'asc')
                    ->get();
        }
        echo json_encode($stateList);
        exit;
    }

    /*     * Edit
     * Method used to save content information
     * @param integer $id
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $city = new City;

        if (!empty($id)) {
            $validator = Validator::make($request->all(), [
                        'name' => 'required|regex:/^[\pL\s\-]+$/u',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                        'countryCode' => 'required',
                        'name' => 'required|regex:/^[\pL\s\-]+$/u',
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $city = City::find($id);
                $city->modifiedBy = Auth::user()->id;
                $city->modifiedOn = Config::get('constants.CURRENTDATE');
            } else {
                $city->countryCode = $request->countryCode;
                $city->stateCode = $request->stateCode;
                $city->createdBy = Auth::user()->id;
                $city->createdOn = Config::get('constants.CURRENTDATE');
            }
            $city->name = $request->name;
            $city->save();
            $cityId = $city->id;

            return redirect('/administrator/city?page=' . $page)->with('successMessage', 'City information saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (City::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/city/?page=' . $page)->with('successMessage', 'City status changed successfully.');
            } else {
                return \Redirect::to('administrator/city/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/city/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (City::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/city/?page=' . $page)->with('successMessage', 'City deleted successfully.');
            } else {
                return \Redirect::to('administrator/city/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/city/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    public function importdata() {
        $data = array();
        $data['title'] = "Administrative Panel :: Cities";
        $data['contentTop'] = array('breadcrumbText' => 'Cities', 'contentTitle' => 'Cities', 'pageInfo' => '');




        return view('Administrator.city.import', $data);
    }

    public function saveimport(Request $request) {
        $data = $insert = array();



        $country = new Country;

        $state = new State;

        $city = new City;

        $validator = Validator::make($request->all(), [
                    'uploadFile' => 'required|mimes:csv,xlsx,xls,ods',
        ]);

        if ($request->hasFile('uploadFile')) {

            $file = $request->file('uploadFile');

            //$path = $request->file->getRealPath();
            $path = $file->getRealPath();


            Excel::load($path, function($reader) {
                foreach ($reader->toArray() as $rows) {
                //print_r($rows);  die; Array ( [countryCode] => YY [codeA3] => AND [codeN3] => 356 [region] => AS [countryName] => India [stateCode] => WB [stateName] => WestBengal [cityName] => Kolkata )                
                    //foreach ($rows as $row) {
                       
                        $this->cities[] = ['countryCode' => $rows['countryCode'], 'stateCode' => $rows['stateCode'], 'name' => $rows['cityName']];

                        $this->states[] = ['countryCode' => $rows['countryCode'], 'code' => $rows['stateCode'], 'name' => $rows['stateName']];

                        $this->countries[] = ['code' => $rows['countryCode'], 'codeA3' => $rows['codeA3'], 'codeN3' => $rows['codeN3'], 'region' => $rows['region'], 'name' => $rows['countryName']];
                   // }
                }
            });
           // print_r($this->countries); die;

            /*if (!empty($this->countries[0])) {

                $countryCode = Country::select('id')->where('code', $this->countries[0]['code'])->get();


                if (count($countryCode) > 0) {

                    return redirect('/administrator/city')->with('errorMessage', 'The Country Code already exist');

                } else {

                    $insertCountry = DB::table('countries')->insert($this->countries[0]);

                    if ($insertCountry) {

                       $stateCode = State::select('id')->where('code', $this->states[0]['code'])->get();

                        if (count($stateCode) > 0) {

                           return redirect('/administrator/city')->with('errorMessage', 'The State Code already exist');

                        } else {

                            $insertState = DB::table('states')->insert($this->states[0]);

                            if ($insertState) {
                                $insertCity = DB::table('cities')->insert($this->cities);

                                if ($insertCity) {

                                    return redirect('/administrator/city')->with('successMessage', 'Data has successfully imported');
                                }
                            } else {

                                return redirect('/administrator/city')->with('successMessage', 'Error inserting the data');
                            }
                        }
                    }
                }
            }*/ 





            if (!empty($this->countries[0])) {

                #dd($this->cities[0]);

                // CHECK CITY
                $checkCity = DB::table('cities')->where("countryCode", $this->cities[0]['countryCode'])
                ->where('stateCode', $this->cities[0]['stateCode'])->where('name', $this->cities[0]['name'])->where('deleted', '0')->first();

                if(empty($checkCity)){
                    $insertCity = DB::table('cities')->insert($this->cities);
                }

                // CHECK STATE
                $stateCode = State::select('id')->where("countryCode", $this->cities[0]['countryCode'])
                ->where('code', $this->cities[0]['stateCode'])->where('deleted', '0')->first();

                if(empty($stateCode)){
                    $insertState = DB::table('states')->insert($this->states[0]);
                }

                // CHECK COUNTRY
                $countryCode = Country::select('id')->where('code', $this->cities[0]['countryCode'])->first();
                if(empty($countryCode)){
                    $insertCountry = DB::table('countries')->insert($this->countries[0]);
                }

                if(!empty($checkCity) && !empty($stateCode) && !empty($countryCode)){
                    return redirect('/administrator/city')->with('errorMessage', 'Duplicate Entry');
                }

                return redirect('/administrator/city')->with('successMessage', 'Data has successfully imported');

            } else {

                return redirect('/administrator/city')->with('errorMessage', 'Error inserting the data');
            }
        } else {
            
            return redirect('/administrator/city')->with('errorMessage', 'Please upload a valid xls/csv file..!!');
            
        }


        return view('Administrator.city.import', $data);
    }

}
