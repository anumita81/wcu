<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Subscriptionfeature;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Excel;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Hash;
use Mail;
use customhelper;

class SubscriptionfeatureController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function index() {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Subscriptionfeature'), Auth::user()->id); // call the helper function
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $maxstoragedays = \App\Model\Generalsettings::where('settingsKey', 'max_storage_days')->pluck('settingsValue');

        $data['maxstoragedays'] = $maxstoragedays[0];

       
        $data['featurelist'] = Subscriptionfeature::all();

        $featurelist = $data['featurelist']->toArray();

        

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        $data['title'] = "Administrative Panel :: Subscription Features";        
        $data['contentTop'] = array('breadcrumbText' => 'Subscription Features', 'contentTitle' => 'Subscription Features', 'pageInfo' => 'This section allows you to manage subscription features');
        $data['pageTitle'] = "Subscription Features";

        return view('Administrator.subscriptionfeature.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();

        $data['title'] = "Administrative Panel :: Subscription Features";
        $data['contentTop'] = array('breadcrumbText' => 'Subscription Features', 'contentTitle' => 'Subscription Features', 'pageInfo' => 'This section allows you to manage subscription features');
        $data['pageTitle'] = "Subscription Features";

        $data['id'] = $id;
        $data['action'] = 'Edit';
        $data['subscriptionfeature'] = Subscriptionfeature::find($id);

        return view('Administrator.subscriptionfeature.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $subscriptionfeature = new Subscriptionfeature;

        $validator = Validator::make($request->all(), [
                    'featureLabel' => 'required',
                    'helpText' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            $subscriptionfeature = Subscriptionfeature::find($id);
            $subscriptionfeature->featureLabel = $request->featureLabel;
            $subscriptionfeature->helpText = $request->helpText;
            $subscriptionfeature->modifiedBy = Auth::user()->id;
            $subscriptionfeature->modifiedOn = Config::get('constants.CURRENTDATE');
            $subscriptionfeature->save();

            return redirect('administrator/subscriptionfeature')->with('successMessage', 'Subscription feature saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $status) {
        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Subscriptionfeature::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/subscriptionfeature/')->with('successMessage', 'Subscription feature status changed successfully.');
            } else {
                return \Redirect::to('administrator/subscriptionfeature/')->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/subscriptionfeature/')->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to change status
     * @param integer $page
     * @return type
     */
    public function changefeaturestatus($id, $type, $status) {
        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Subscriptionfeature::changeFeatureStatus($id, $createrModifierId, $type, $status)) {
                return \Redirect::to('administrator/subscriptionfeature/')->with('successMessage', 'Subscription feature status changed successfully.');
            } else {
                return \Redirect::to('administrator/subscriptionfeature/')->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/subscriptionfeature/')->with('errorMessage', 'Error in operation!');
        }
    }

}
