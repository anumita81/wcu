<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Generalsettings;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use App\Model\Country;
use customhelper;

class SettingsController extends Controller {

	public $_perPage;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
		$this->_perPage=10;
    }

    public function index() {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Generalsettings'), Auth::user()->id); // call the helper function
        if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $data['checked'] = 0;
       $data['title'] = "Administrative Panel :: Settings Management :: General Settings";
       $data['pageTitle'] = "General Settings";
       $data['contentTop'] = array('breadcrumbText'=>'General Settings', 'contentTitle'=>'General Settings', 'pageInfo'=>'This sections allows you to manage site configuration');

       $userId = Auth::id();

       /* GET THE LIST OF PHONE FOR COUNTRIES */
       $data['getPhones'] = Country::orderByRaw('field (id, 163, 76, 230)')->get();
       
       

       $data['settings'] = Generalsettings::getSettingsData()->toArray();
       
       
       $data['referralSettings'] = Generalsettings::where('groupName', 'siteSettings')->where('subGroupName', 'Referral Settings')->get()->toArray();
       
       foreach($data['referralSettings'] as $value)
       {
           if($value['settingsKey'] == 'perdollar_value')
           {
               $perdollarVal = explode(":", $value['settingsValue']);
               
               $data['perdollarVal'] = $perdollarVal[0];
               $data['pointVal'] = !isset($perdollarVal[1])?0:$perdollarVal[1];
           }
           
           if($value['settingsKey'] == 'maxpoints_value')
           {
               $data['maxpoints_value'] = $value['settingsValue'];
           }
           
           if($value['settingsKey'] == 'absolute_value')
           {
               $data['absolute_value'] = $value['settingsValue'];
           }
           
          if($value['settingsKey'] == 'refferal_pont_type' && $value['settingsValue'] == 1)
          {
              $data['checked'] = 1;
          }
          
           
       }

       $getDefaultCurrency = \App\Model\Currency::getDefaultCurrency();
       
  
       
       $data['currencySymbol']= $getDefaultCurrency[0]['symbol']; 
       $data['defaultSymbol']= $getDefaultCurrency[0]['symbol']; 

      $data['canView'] = $findRole['canView'];
      $data['canAdd'] = $findRole['canAdd'];
      $data['canEdit'] = $findRole['canEdit'];
      $data['canDelete'] = $findRole['canDelete'];
       
       return view('Administrator.generalsettings.index', $data);
    }
	
    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata(Request $request){
        
        $data = array();
        
        $generalsettings = new Generalsettings;       
        
        $request->except('_token');

       //dd($request->post()); die;
        //dd($request->post());
        foreach($request->post() as $key=> $val)
        {
            //echo $key;
                        //continue;
        
           //If post new value and db settings value are not same then db old value will updated with settings value 
            if($key != "_token"){ 
               
                $referalSettings = array('refferal_pont_type', 'absolute_value', 'perdollarVal', 'pointVal', 'maxpoints_value');
                
                if(in_array($key, $referalSettings) || $request->subgroupName == "Referral Settings")
                {
                    if($key == 'refferal_pont_type')
                    {
                        $referId = Generalsettings::where('settingsKey', 'refferal_pont_type')->get(['id']);
                        $referId = $referId[0]['id'];
                        
                        $generalsettings = Generalsettings::find($referId);

                        //$val[1] = $request->refferal_pont_type[1];
                        //$val[0] = $request->refferal_pont_type[0];
                       
                        $generalsettings->settingsValue = $request->refferal_pont_type[1];
                        $generalsettings->oldValue = $request->refferal_pont_type[0];
                        
                        
                        $generalsettings->save();
                        
                    }
                    
                    if($key == 'maxpoints_value')
                    {
                       
                        $referId = Generalsettings::where('settingsKey', $key)->get(['id']);
                        //$val[1] = $request->maxpoints_value[1];
                        //$val[0] = $request->maxpoints_value[0];
                        $referId = $referId[0]['id'];
                  
                        $generalsettings = Generalsettings::find($referId);
                        $generalsettings->settingsValue = $request->maxpoints_value[1];
                        $generalsettings->oldValue = $request->maxpoints_value[0];
                        
                        
                        $generalsettings->save();
                    }
                    
                    if($key == 'absolute_value')
                    {
                      
                      //$val[1] = $request->absolute_value[1];
                      //$val[0] = $request->absolute_value[0];
                      $referId = Generalsettings::where('settingsKey', $key)->get(['id']);
                      $referId = $referId[0]['id'];
                  
                      $generalsettings = Generalsettings::find($referId);
                      $generalsettings->settingsValue = $request->absolute_value[1];
                        $generalsettings->oldValue = $request->absolute_value[0];
                        
                        
                        $generalsettings->save();
                    }
                    else{

                       $settingsPointVal = implode(":", array($request->perdollarVal[1],$request->pointVal[1]));
                       $oldSettingsPointVal = implode(":", array($request->perdollarVal[1],$request->pointVal[1]));
                       $referId = Generalsettings::where('settingsKey', 'perdollar_value')->get(['id']);
                       $referId = $referId[0]['id'];
                  
                       $generalsettings = Generalsettings::find($referId);
                       $generalsettings->settingsValue = $settingsPointVal;
                        $generalsettings->oldValue = $oldSettingsPointVal;
                        
                        
                        $generalsettings->save();
                    }
                   
                    
                    
                 
                }
                
              //if($request->subgroupName1 == "General Parameters"){
                 
                  //echo "here"; die;
                 
                    if($key == 'countryCode')
                    {
                        //
                    }

                if($key == 'contact_number')
                { 
                    
                    /* CREATE THE ARRAY TO GET THE PHONE NUNBERS */

                    $arrCC = array_values(array_filter($request->countryCode));

                    $arr = $request->contact_number;
                    unset($arr[0]);
                    
                    

                    $arrPhone = array_values(array_filter($arr));

                    foreach($arrPhone as $key1 => $val1){
                        
                        $create[] = $arrCC[$key1]."^".$val1;
                        
                    }
                        
                   

                    //$newArray = array_combine($arrCC, $arrPhone);

                    
                    /* MAKE THE ARRAY IN JSON FORMAT */

                    $jsonPhone = json_encode($create);
                    $val[1] = $jsonPhone;
                    
                    
                }

                if($key == 'cc_info')
                {
                    if(count($val)>1)
                    {
                        $val[1] = 1;
                    }else{
                        $val[1] = 0; 
                    }
                  
                }

                if($key == 'dispaly_cvv2')
                {
                    if(count($val)>1)
                    {
                        $val[1] = 1;
                    }else{
                        $val[1] = 0; 
                    }
                    
                    
                }



                if($key == 'point_expiry_period')
                {
                  if($val[1] == 0){
                    $val[0] = 1;
                    $val[1] = 1;
                  }
                  
                }
                if(!in_array($key,$referalSettings))
                {
                    //print_r($val);
                    //echo $key.'<br>';
                      $settingsId = Generalsettings::where('settingsKey', $key)->get(['id']);
                      if(count($settingsId)>0)
                      {
                        $settingsId = $settingsId[0]['id'];



                         $generalsettings = Generalsettings::find($settingsId);
                         $generalsettings->settingsValue = $val[1];
                         $generalsettings->oldValue = $val[0];
                         $generalsettings->save();
                      }
                }
                
                //echo ' '.$key.'<br/>';
                   
               //}

             

              
            }
            
        }
       //die;
        return redirect('/administrator/generalsettings')->with('successMessage', 'Settings saved successfuly.');
    

    }
	
	
	 /**
     * Method used to unset search session data
     *
     */
    public function cleardata() {
        \Session::forget('SETTINGSDATA');
        return \Redirect::to('administrator/generalsettings');
    }


    /**
     * Method used to get counrty phones
     * @return array
     */
    public function getphonedata(Request $request) {
        /* GET THE LIST OF PHONE FOR COUNTRIES */
       $getPhones = Country::where('status', '1')->orderby('priority', 'desc')->get();

       /* SET THE HTML */
       $html = '';
       $html .= '<div class="clearfix" style="clear:both; margin-bottom:15px;">
       <div class="form-inline">
       <div class="form-group">
       <select class="form-control customSelect2 fixedwidth120" name="countryCode[]" id="countryCode">';
       foreach($getPhones as $phone){
            $html .= "<option data-countryCode=\"$phone->code\" value=\"$phone->code\">$phone->name</option>";
       }
       $html .='</select></div>
       <div class="form-group">
       <input type="text" class="form-control fixedwidth110" name="contact_number[]" maxlength="12" onkeypress="return isNumberKey(event);" required />
       </div>
       <div class="form-group">
       <a class="actionIcons color-theme-2 no-mrg" href="javascript:void(0);">
       <i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash remove">
       </i>
       </a>
       </div>
       </div>
       </div>';


       echo $html;
    }

    /**
     * Method used to get counrty phones
     * @param integer $val
     * @return array
     */
    public function deletecontact($val = '0') {
        $Contacts = Generalsettings::where('settingsKey', 'contact_number')->get();
        /* MAKE ARRAY BY JSON DECODE */
        $decoded = json_decode($Contacts[0]->settingsValue);

        /* MAKE SURE THE LAST ARRAY ELEMENT CANNOT BE DELETED */
        if(count($decoded) > 1){
            if (($key = array_search($val, $decoded)) !== false) {
                unset($decoded[$key]);
            }

        /* MAKE THE ARRAY AS JSON ENCODED */

            $jsonEncoded = json_encode(array_values($decoded));
        

            $gsettings = Generalsettings::where('settingsKey', 'contact_number')->update(array('settingsValue' => $jsonEncoded));
            return redirect('/administrator/generalsettings')->with('successMessage', 'Contact deleted successfuly.');
        } else {
            return redirect('/administrator/generalsettings')->with('errorMessage', 'Last Contact can not be deleted');
        }


        
    }

}
