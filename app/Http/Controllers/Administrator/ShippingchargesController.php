<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Shippingmethods;
use App\Model\Shippingcharges;
use App\Model\Zone;
use Auth;

use Illuminate\Contracts\Auth\Authenticatable;

use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use customhelper;
use Config;

class ShippingchargesController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }


    public function index (Request $request) {
        
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Shippingcharges'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $param = array();

        \Session::forget('RECORD');
        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */

            $sortField = \Input::get('field', 'id');
            $sortOrder = \Input::get('type', 'desc');
            $perpage = \Input::get('searchDisplay', $this->_perPage);
            $searchData = \Input::get('searchData', "");
            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('RECORD');
            \Session::push('RECORD.searchData', $searchData);
            \Session::push('RECORD.searchDisplay', $perpage);
            \Session::push('RECORD.field', $sortField);
            \Session::push('RECORD.type', $sortOrder);

        }
        else
        {
            $sortField = \Session::get('RECORD.field');
            $sortType = \Session::get('RECORD.type');
            $perpage = \Session::get('RECORD.searchDisplay');
            $searchData = \Session::get('RECORD.searchData');

            $sortField = !empty($sortField) ? $sortField[0] : 'id';
            $sortOrder = !empty($sortType) ? $sortType[0] : 'desc';
            $perpage = !empty($perpage) ? $perpage[0] : $this->_perPage;
            $searchData = !empty($searchData) ? $searchData[0] : "";

        }
        
        $param['sortField'] = $sortField;
        $param['sortOrder'] = $sortOrder;
        $param['searchDisplay'] = $perpage;
        $param['searchData'] = $searchData;        
        $data['param'] = $param;
        $data['allMethods'] = Shippingmethods::where('active','Y')->where('deleted','0')->get();
       
        $data['allZones'] = Zone::where('status','1')->get();
        $data['shippingCharges'] = Shippingcharges::getData($param);
        
        $data['pageTitle'] = 'Shipping Charges';
        $data['title'] = "Shipping Charges :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText'=>array('Shipping Charges Settings','Shipping Charges'),'contentTitle'=>'Shipping Charges','pageInfo'=>'This section allows you to define the shipping rates for each shipping method defined for your store.');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.shipmentcharges.index',$data);
    }

    public function addData(Request $request) {

        $dataObj = new Shippingcharges;

        if($request->input('zoneId') == $request->input('pzoneid'))
            return redirect(route('shippingcharges'))->with('errorMessage', 'Source and destination zone can not be same');

        $dataObj->shippingId = $request->input('shippingId');
        $dataObj->zoneId = $request->input('zoneId');
        $dataObj->pzoneid = $request->input('pzoneid');
        $dataObj->minWeight = $request->input('minWeight');
        $dataObj->maxWeight = $request->input('maxWeight');
        $dataObj->minCost = $request->input('minCost');
        $dataObj->maxCost = $request->input('maxCost');
        $dataObj->flatRate = $request->input('flatRate');
        $dataObj->shippingChargePerpound = $request->input('shippingChargePerpound');
        $dataObj->clearingPortHandling = $request->input('clearingPortHandling');
        $dataObj->clearingPortHandlingPerpound = $request->input('clearingPortHandlingPerpound');
        $dataObj->dispatchCost = $request->input('dispatchCost');
        $dataObj->dispatchCostPerpound = $request->input('dispatchCostPerpound');
        $dataObj->insurancePercent = $request->input('insurancePercent');
        $dataObj->inventoryCharge = $request->input('inventoryCharge');
        $dataObj->customDutyPercent = $request->input('customDutyPercent');

        if($dataObj->save())
            return redirect(route('shippingcharges'))->with('successMessage', 'Information saved successfuly.');

    }

    public function updateData(Request $request) {

        //echo '<pre>';print_r($request->input('postData'));echo '</pre>';exit;
        $postArr = $request->input('postData');
        if($request->input('action') == 'update')
        {
            
            if(!empty($postArr))
            {
                foreach ($postArr as $chargeId => $chargeValues) {
                    if(isset($chargeValues['is_update']) && $chargeValues['is_update']=='on') {

                        $dataObj = Shippingcharges::find($chargeId);

                        $dataObj->minWeight = $chargeValues['minWeight'];
                        $dataObj->maxWeight = $chargeValues['maxWeight'];
                        $dataObj->minCost = $chargeValues['minCost'];
                        $dataObj->maxCost = $chargeValues['maxCost'];
                        $dataObj->flatRate = $chargeValues['flatRate'];
                        $dataObj->shippingChargePerpound = $chargeValues['shippingChargePerpound'];
                        $dataObj->clearingPortHandling = $chargeValues['clearingPortHandling'];
                        $dataObj->clearingPortHandlingPerpound = $chargeValues['clearingPortHandlingPerpound'];
                        $dataObj->dispatchCost = $chargeValues['dispatchCost'];
                        $dataObj->dispatchCostPerpound = $chargeValues['dispatchCostPerpound'];
                        $dataObj->insurancePercent = $chargeValues['insurancePercent'];
                        $dataObj->inventoryCharge = $chargeValues['inventoryCharge'];
                        $dataObj->customDutyPercent = $chargeValues['customDutyPercent'];

                        $dataObj->save();

                    }
                }
            }

            return redirect(route('shippingcharges'))->with('successMessage', 'Information updated successfuly.');
        }
        else if($request->input('action') == 'delete')
        {
            foreach ($postArr as $chargeId => $chargeValues) {

                if(isset($chargeValues['is_update']) && $chargeValues['is_update']=='on') {
                    $dataObj = Shippingcharges::find($chargeId);
                    $dataObj->delete();
                }
            }

            return redirect(route('shippingcharges'))->with('successMessage', 'Information deleted successfuly.');

        }
    }

}