<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Locationtype;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class LocationtypeController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Locationtype'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('LOCATIONTYPEDATA');
            \Session::push('LOCATIONTYPEDATA.searchDisplay', $searchDisplay);
            \Session::push('LOCATIONTYPEDATA.field', $field);
            \Session::push('LOCATIONTYPEDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('LOCATIONTYPEDATA.field');
            $sortType = \Session::get('LOCATIONTYPEDATA.type');
            $searchDisplay = \Session::get('LOCATIONTYPEDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTRY LIST  */
        $locationTypeData = Locationtype::getTypeList($param);
            $locationtypeUsed = \App\Model\Autoshipment::fetchUsedFieldIds('pickupLocationType');

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Location Types";
        $data['contentTop'] = array('breadcrumbText' => 'Location Types', 'contentTitle' => 'Location Types', 'pageInfo' => 'This section allows you to manage location types');
        $data['pageTitle'] = "Location Types";
        $data['page'] = $locationTypeData->currentPage();
        $data['locationTypeData'] = $locationTypeData;
        $data['locationtypeUsed'] = $locationtypeUsed;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.locationtype.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['page'] = !empty($page) ? $page : '1';
        if (!empty($id)) {
            $data['id'] = $id;
            $data['pageTitle'] = 'Edit Location Type';
            $locationtype = Locationtype::find($id);
            $data['locationtype'] = $locationtype;
        } else {
            $data['id'] = 0;
            $data['pageTitle'] = 'Add Location Type';
        }
        return view('Administrator.locationtype.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $id
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $locationtype = new Locationtype;

        $validator = Validator::make($request->all(), [
                    'name' => 'required|unique:' . $locationtype->table . ',name,' . $id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $locationtype = Locationtype::find($id);
            }

            $locationtype->name = $request->name;
            $locationtype->save();
            $locationtypeId = $locationtype->id;

            return redirect('/administrator/locationtype?page=' . $page)->with('successMessage', 'Location type information saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Locationtype::changeStatus($id, $status)) {
                return \Redirect::to('administrator/locationtype/?page=' . $page)->with('successMessage', 'Status changed successfully.');
            } else {
                return \Redirect::to('administrator/locationtype/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/locationtype/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    
    public function delete($id, $page) {
        if ($id != '') {
            $locationtypeUsed = \App\Model\Autoshipment::fetchUsedFieldIds('pickupLocationType');
            if (!in_array($id, $locationtypeUsed)) {
                if (Locationtype::deleteRecord($id)) {
                    return \Redirect::to('administrator/locationtype/?page=' . $page)->with('successMessage', 'Record deleted successfully.');
                } else {
                    return \Redirect::to('administrator/locationtype/?page=' . $page)->with('errorMessage', 'Error in operation!');
                }
            } else {
                return \Redirect::to('administrator/locationtype/?page=' . $page)->with('errorMessage', 'Location type cannot be deleted. Dependency exists.');
            }
        } else {
            return \Redirect::to('administrator/locationtype/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
