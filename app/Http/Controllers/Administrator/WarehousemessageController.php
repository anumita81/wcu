<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Warehousemessage;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;

class WarehousemessageController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('WAREHOUSEMESSAGEDATA');
            \Session::push('WAREHOUSEMESSAGEDATA.searchDisplay', $searchDisplay);
            \Session::push('WAREHOUSEMESSAGEDATA.field', $field);
            \Session::push('WAREHOUSEMESSAGEDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('WAREHOUSEMESSAGEDATA.field');
            $sortType = \Session::get('WAREHOUSEMESSAGEDATA.type');
            $searchDisplay = \Session::get('WAREHOUSEMESSAGEDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'text' => array('current' => 'sorting'),
            'author' => array('current' => 'sorting'),
            'email'=> array('current' => 'sorting'),
            'createdOn'=> array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTRY LIST  */
        $warehousemessageData = Warehousemessage::getWarehousemessageList($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Warehousemessage";
        $data['contentTop'] = array('breadcrumbText' => 'Warehousemessage', 'contentTitle' => 'Warehousemessage', 'pageInfo' => 'This section allows you to reply warehouse messages');
        $data['pageTitle'] = "Warehouse Message";
        $data['page'] = $warehousemessageData->currentPage();
        $data['warehousemessageData'] = $warehousemessageData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        return view('Administrator.warehousemessage.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function reply($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Warehousemessage";
        $data['contentTop'] = array('breadcrumbText' => 'Warehousemessage', 'contentTitle' => 'Warehousemessage', 'pageInfo' => '');
        $data['page'] = !empty($page) ? $page : '1';
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $warehousemessage = Warehousemessage::find($id);
            $data['warehousemessageData'] = $warehousemessage;
        
        return view('Administrator.warehousemessage.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $warehousemessage = new Warehousemessage;


        $validator = Validator::make($request->all(), [
                    'message' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            
            $warehousemessage = Warehousemessage::find($id);
            $warehousemessage->message = $request->message;
            $warehousemessage->save();


            return redirect('/administrator/warehousemessage')->with('successMessage', 'Message saved successfuly.');
        }
    }


    

}
