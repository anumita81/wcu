<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Smstemplate;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class SmstemplateController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index() {
        $data = array();

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Smstemplate'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchText = trim(\Input::get('search', ''));
            $searchSelect = \Input::get('searchSelect', '');
            $searchByStatus = \Input::get('searchByStatus', '');
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $searchByType = \Input::get('searchByType', 'G');
            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('SMSTEMPLATEDATA');
            \Session::push('SMSTEMPLATEDATA.field', $field);
            \Session::push('SMSTEMPLATEDATA.type', $type);
            \Session::push('SMSTEMPLATEDATA.searchDisplay', $searchDisplay);
            \Session::push('SMSTEMPLATEDATA.searchByType', $searchByType);
           
            $param['field'] = !empty($field) ? $field : 'id';
            $param['type'] = !empty($type) ? $type : 'desc';
            $param['searchDisplay'] = $searchDisplay;
            $param['searchByType'] = $searchByType;
        } else {
            $sortField = \Session::get('SMSTEMPLATEDATA.field');
            $sortType = \Session::get('SMSTEMPLATEDATA.type');
            $searchDisplay = \Session::get('SMSTEMPLATEDATA.searchDisplay');
            $searchByType = \Session::get('SMSTEMPLATEDATA.searchByType');

            $param['field'] = !empty($sortField[0]) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType[0]) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
            $param['searchByType'] = !empty($searchByType) ? $searchByType[0] : 'G';
        }


        /* BUILD SORTING ARRAY */
        $sort = array(
            'createdOn' => array('current' => 'sorting'),
            'templateKey' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        $smsData = Smstemplate::getSmsTemplateList($param);
        
        $emailTemplateKeys = collect(\App\Model\Emailtemplatekey::where('templateType', $param['searchByType'])->get());
        $data['templateKeysList'] = $emailTemplateKeys->mapWithKeys(function($item) {
            return [$item['keyname'] => $item['name']];
        });


        /* BUILD DATA FOR VIEW  */
        $data['title'] = "SMS Template Management :: ADMIN - Shoptomydoor";
        
        $data['pageTitle'] = "SMS Template Management";
        $data['contentTop'] = array('breadcrumbText' => 'SMS Template', 'contentTitle' => 'SMS Template', 'pageInfo' => 'This section allows you to manage templates for sms notifications');
        $data['page'] = $smsData->currentPage();
        $data['smsData'] = $smsData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.smstemplate.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Add SMS Template :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "SMS Template :: Add";
        $data['pageTitle'] = "SMS Template :: Add";
        $data['contentTop'] = array('breadcrumbText' => 'Dashboard', 'contentTitle' => 'SMS Template', 'pageInfo' => '');
        $data['page'] = !empty($page) ? $page : '1';
        $templateKey = Config::get('constants.templateKey.TEMPLATEKEY');
        $data['templateKey'] = unserialize($templateKey);

        if (!empty($id)) {
            $data['id'] = $id;
            $data['title'] = "ADMIN - Shoptomydoor :: SMS Template :: Update SMS";
            $data['pageTitle'] = "Update SMS Template";
            $data['contentTop'] = array('breadcrumbText' => 'Dashboard', 'contentTitle' => 'SMS Template', 'pageInfo' => '');
            $content = Smstemplate::find($id);
            $data['content'] = $content;
        } else {
            $data['id'] = 0;
            $data['pageTitle'] = "Add SMS Template";
            $data['content'] = array();
        }

        return view('Administrator.smstemplate.addedit', $data);
    }

    /**
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $smstemplate = new Smstemplate;

        if ($request->templateType != 'G') {
            $validator = Validator::make($request->all(), [
                        'templateBody' => 'required'
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                        'templateBody' => 'required'
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

            if (!empty($id)) {
                $smstemplate = Smstemplate::find($id);
                $smstemplate->updatedOn = Config::get('constants.CURRENTDATE');
                $smstemplate->updatedBy = Auth::user()->id;
            } else {
                $smstemplate->createdOn = Config::get('constants.CURRENTDATE');
                $smstemplate->createdBy = Auth::user()->id;
            }
            
            $smstemplate->templateBody = !empty($request->templateBody) ? strip_tags($request->templateBody) : '';
            $smstemplate->save();

            return redirect('/administrator/smstemplate?page=' . $page)->with('successMessage', 'SMS Template saved successfuly.');
        }
    }

    /**
     * Method used to unset search session data
     *
     */
    public function cleardata() {
        \Session::forget('SMSTEMPLATEDATA');
        return \Redirect::to('administrator/smstemplate');
    }

}
