<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Servicetype;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class ServicetypeController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Servicetype'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('SERVICETYPEDATA');
            \Session::push('SERVICETYPEDATA.searchDisplay', $searchDisplay);
            \Session::push('SERVICETYPEDATA.field', $field);
            \Session::push('SERVICETYPEDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('SERVICETYPEDATA.field');
            $sortType = \Session::get('SERVICETYPEDATA.type');
            $searchDisplay = \Session::get('SERVICETYPEDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'name';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting')
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTRY LIST  */
        $servicetypeData = Servicetype::getServicetypeList($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Servicetype";
        $data['contentTop'] = array('breadcrumbText' => 'Servicetype', 'contentTitle' => 'Service Types', 'pageInfo' => 'This section allows you to change service types');
        $data['pageTitle'] = "Service Types";
        $data['page'] = $servicetypeData->currentPage();
        $data['servicetypeData'] = $servicetypeData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.servicetype.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Servicetype";
        $data['contentTop'] = array('breadcrumbText' => 'Servicetype', 'contentTitle' => 'Service Types', 'pageInfo' => 'This section allows you to change service types');
        $data['page'] = !empty($page) ? $page : '1';
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $servicetype = Servicetype::find($id);
            $data['servicetype'] = $servicetype;
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add';
        }
        return view('Administrator.servicetype.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $servicetype = new Servicetype;

        $validator = Validator::make($request->all(), [
                    'name' => 'required|unique:' . $servicetype->table . ',name,' . $id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $servicetype = Servicetype::find($id);
                $servicetype->updatedBy = Auth::user()->id;
                $servicetype->updatedOn = Config::get('constants.CURRENTDATE');
            }
            else{
                $servicetype->createdBy = Auth::user()->id;
                $servicetype->createdOn = Config::get('constants.CURRENTDATE');
            }
            $servicetype->name = $request->name;
            $servicetype->save();
            $servicetypeId = $servicetype->id;

            return redirect('/administrator/servicetype')->with('successMessage', 'Service Type saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Servicetype::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/servicetype/?page=' . $page)->with('successMessage', 'Service type status changed successfully.');
            } else {
                return \Redirect::to('administrator/servicetype/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/servicetype/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    public function deletedata($id = '',$page='') {

        $servicetype= new Servicetype();

        $createrModifierId = Auth::user()->id;


        if(!empty($id)){
        $servicetype = $servicetype->find($id);        

        if (Servicetype::deleteRecord($id, $createrModifierId)) {
               return \Redirect::to('administrator/servicetype/?page='.$page)->with('successMessage', 'Service type deleted successfuly.');
            } else {
                return \Redirect::to('administrator/servicetype/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/servicetype/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
        
        


    }


}
