<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Warehouse;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class WarehouseController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Warehouse'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        
        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('WAREHOUSEDATA');
            \Session::push('WAREHOUSEDATA.searchDisplay', $searchDisplay);
            \Session::push('WAREHOUSEDATA.field', $field);
            \Session::push('WAREHOUSEDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('WAREHOUSEDATA.field');
            $sortType = \Session::get('WAREHOUSEDATA.type');
            $searchDisplay = \Session::get('WAREHOUSEDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'address' => array('current' => 'sorting'),
            'stateName' => array('current' => 'sorting'),
            'cityName' => array('current' => 'sorting'),
            'countryName' => array('current' => 'sorting'),
            'zipcode' => array('current' => 'sorting')
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';




        /* FETCH WAREHOUSE DATA */
        $warehouseData = Warehouse::getWarehouseList($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Warehouse";
        $data['contentTop'] = array('breadcrumbText' => 'Warehouse', 'contentTitle' => 'Warehouse', 'pageInfo' => 'This section allows you to change warehouse');
        $data['pageTitle'] = "Warehouse";
        $data['page'] = $warehouseData->currentPage();
        $data['warehouseData'] = $warehouseData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.warehouse.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addwarehouse($page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Warehouse";
        $data['contentTop'] = array('breadcrumbText' => 'Warehouse', 'contentTitle' => 'Warehouse', 'pageInfo' => 'This section allows you to change warehouse');
        $data['pageTitle'] = "Add New Warehouse";
        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;
        $data['action'] = 'Add';

        /* FETCH COUNTRY LIST  */
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

        return view('Administrator.warehouse.addwarehouse', $data);
    }

    /**
     * Method to fetch state list
     * @return array
     */
    public function getstatelist($countrycode = '') {
        $stateList = array();
        if (isset($countrycode) && !empty($countrycode)) {
            $countryId = Country::find($countrycode);
            $stateList = State::where('status', '1')
                    ->where('deleted', '0')
                    ->where('countryCode', $countryId->code)
                    ->orderby('name', 'asc')
                    ->get();
        }
        echo json_encode($stateList);
        exit;
    }

    public function getcitylist($stateCode = '') {
        $cityList = array();
        if (isset($stateCode) && !empty($stateCode)) {
            $stateId = State::find($stateCode);
            $cityList = City::where('status', '1')
                    ->where('deleted', '0')
                    ->where('stateCode', $stateId->code)
                    ->orderby('name', 'asc')
                    ->get();
        }
        echo json_encode($cityList);
        exit;
    }

    public function editwarehouse($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Warehouse";
        $data['contentTop'] = array('breadcrumbText' => 'Warehouse', 'contentTitle' => 'Warehouse', 'pageInfo' => 'This section allows you to change warehouse');
        $data['pageTitle'] = "Edit Warehouse";
        $data['page'] = !empty($page) ? $page : '1';
        /* FETCH COUNTRY LIST  */
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

        $data['id'] = $id;
        $data['action'] = 'Edit';
        $warehouse = Warehouse::find($id);
        $data['warehouse'] = $warehouse;

        $data['shipToCountries'] = explode(',', $warehouse['shipToCountries']);

        

        $countrycode = Country::find($data['warehouse']['countryId']);
        $data['stateList'] = State::where('status', '1')->where('countryCode', $countrycode->code)->orderby('name', 'asc')->get();

        $statecode = State::find($data['warehouse']['stateId']);
        $data['cityList'] = City::where('status', '1')->where('stateCode', $statecode->code)->orderby('name', 'asc')->get();

        return view('Administrator.warehouse.editwarehouse', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $warehouse = new Warehouse;
   
        $validator = Validator::make($request->all(), [
                    'address' => 'required',
                    'countryCode' => 'required',
                    'stateCode' => 'required',
                    'cityCode' => 'required',
                    'zipCode' => 'required',
                    'exchangeRate' => 'required|numeric',
                    'uploadContImg' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'urgentPurchaseCost'=>'required',
                    'urgentPurchase'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if ($request->hasFile('uploadContImg')) {
                $image = $request->file('uploadContImg');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/warehouse');
                $image->move($destinationPath, $name);
            } else {
                $name = $request->companyoldLogo;
            }

            if (!empty($id)) {
                $warehouse = Warehouse::find($id);
                $warehouse->modifiedBy = Auth::user()->id;
                $warehouse->modifiedOn = Config::get('constants.CURRENTDATE');
            } else {
                $warehouse->createdBy = Auth::user()->id;
                $warehouse->createdOn = Config::get('constants.CURRENTDATE');
            }
            $warehouse->address = $request->address;
            if (!empty($request->address2)) {
                $warehouse->address2 = $request->address2;
            }
            $warehouse->cityId = $request->cityCode;
            $warehouse->stateId = $request->stateCode;
            $warehouse->countryId = $request->countryCode;
            $warehouse->zipcode = $request->zipCode;
            $warehouse->image = $name;
            $warehouse->rate = $request->exchangeRate;
            $warehouse->urgentPurchaseCost = $request->urgentPurchaseCost;
            $warehouse->urgentPurchase = $request->urgentPurchase;
            $warehouse->shipToCountries = implode(',', $request->shipCountries);
            
            $warehouse->save();
            $warehouseId = $warehouse->id;

            return redirect('/administrator/warehouse?page=' . $page)->with('successMessage', 'Warehouse saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Warehouse::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/warehouse/?page=' . $page)->with('successMessage', 'Warehouse status changed successfully.');
            } else {
                return \Redirect::to('administrator/warehouse/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/warehouse/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    public function deletedata($id = '', $page = '') {

        $warehouse = new Warehouse();

        $createrModifierId = Auth::user()->id;


        if (!empty($id)) {
            $warehouse = $warehouse->find($id);

            if (Warehouse::deleteRecord($id, $createrModifierId)) {
                $warehouse = Warehouse::find($id);
                if (!empty($warehouse->image)) {
                    unlink(public_path('/uploads/warehouse/' . $warehouse->image));
                }

                return \Redirect::to('administrator/warehouse/?page=' . $page)->with('successMessage', 'Warehouse deleted successfuly.');
            } else {
                return \Redirect::to('administrator/warehouse/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/warehouse/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
