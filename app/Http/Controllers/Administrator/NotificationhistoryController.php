<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserAdmin;
use App\libraries\dbHelpers;
//use App\libraries\helpers;
//use App\libraries\dbHelpers;
use Auth;
use App\Model\Notification;
use Hash;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Routing\Route;   

class NotificationhistoryController extends Controller {

    public $_perPage;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    
    public function index(Route $route, Request $request, $id) {
        $data = array();

        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;

        }

        $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

        $field = \Input::get('field', 'id');
        $type = \Input::get('type', 'desc');

        $param['field'] = $field;
        $param['type'] = $type;
        $param['searchDisplay'] = $searchDisplay;

        $sort = array(
            'date' => array('current' => 'sorting'),
        );
        
        
        $notifyRecord = Notification::getNotificationList($param, $id);
        #print_r($userRecord);
        #dd($notifyRecord);
        $data['notifyRecord'] = $notifyRecord;
        $data['getId'] = $id;
        $data['title'] = "Notification History :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "View Notifications";
        $data['contentTop'] = array('breadcrumbText'=>'Notifications','contentTitle'=>'View Notifications','pageInfo'=>'This section allows you to view notification history');
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        return view('Administrator.notification.index',$data);

    }

    



}
