<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Country;
use App\Model\Procurementfees;
use App\Model\Warehouse;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class ProcurementfeesController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Procurementfees'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */

            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $searchWarehouse = \Input::get('searchWarehouse', '');

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('PROCUREMENTFEESDATA');
            \Session::push('PROCUREMENTFEESDATA.searchDisplay', $searchDisplay);
            \Session::push('PROCUREMENTFEESDATA.searchWarehouse', $searchWarehouse);
            \Session::push('PROCUREMENTFEESDATA.field', $field);
            \Session::push('PROCUREMENTFEESDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
            $param['searchWarehouse'] = $searchWarehouse;
        } else {
            $sortField = \Session::get('PROCUREMENTFEESDATA.field');
            $sortType = \Session::get('PROCUREMENTFEESDATA.type');
            $searchWarehouse = \Session::get('PROCUREMENTFEESDATA.searchWarehouse');
            $searchDisplay = \Session::get('PROCUREMENTFEESDATA.searchDisplay');

            $searchWarehouse = array();

            $param['field'] = !empty($sortField) ? $sortField[0] : 'warehouseId';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchWarehouse'] = !empty($searchWarehouse) ? $searchWarehouse[0] : '';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'warehouseId'=>array('current' => 'sorting'), 'feeLeft'  => array('current' => 'sorting'), 'feeRight' => array('current' => 'sorting'), 'feeType' => array('current' => 'sorting'), 'value' => array('current' => 'sorting')
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTRY LIST  */
        $feesData = Procurementfees::getFeesList($param);//countryName address
        

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Procurement Fees";
        $data['contentTop'] = array('breadcrumbText' => 'Procurement Fees', 'contentTitle' => 'Procurement Fees', 'pageInfo' => 'This section allows you to manage procurement fees');
        $data['pageTitle'] = "Procurement Fees";
        $data['page'] = $feesData->currentPage();
        $data['feesData'] = $feesData;

        $warehouse = Procurementfees::getWarehouseList()->toArray();
        
        $data['warehouseList'] = $warehouse;       
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        
        return view('Administrator.procurementfees.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Procurement Fees";
        $data['contentTop'] = array('breadcrumbText' => 'Procurement Fees', 'contentTitle' => 'Procurement Fees', 'pageInfo' => 'This section allows you to manage procurement fees');
        $data['page'] = !empty($page) ? $page : '1';
        
        $warehouse = Procurementfees::getWarehouseList()->toArray();

        $data['warehouseList'] = $warehouse;    

        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $procurementfees = Procurementfees::find($id);
            $data['procurementfees'] = $procurementfees;
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add New';

        }
        return view('Administrator.procurementfees.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */

    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $procurementfees = new Procurementfees;

        $validator = Validator::make($request->all(), [
                    'feeLeft' => 'required',
                    'feeRight' => 'required',
                    'feeType' => 'required',
                    'value' => 'required',
                    'warehouseId' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $procurementfees = Procurementfees::find($id);
                $procurementfees->updatedBy = Auth::user()->id;
                $procurementfees->updatedOn = Config::get('constants.CURRENTDATE');
            }
            else{
                $checkDuplicate = Procurementfees::checkduplicate($request->warehouseId, $request->feeLeft, $request->feeRight);
                
                if(count($checkDuplicate) > 0){
                    return redirect('administrator/procurementfees/')->with('errorMessage', 'Fees range already exists for the selected warehouse');
                }
                
                $procurementfees->createdBy = Auth::user()->id;
                $procurementfees->createdOn = Config::get('constants.CURRENTDATE');
            }
            $procurementfees->feeLeft = $request->feeLeft;
            $procurementfees->feeRight = $request->feeRight;
            $procurementfees->feeType = $request->feeType;
            $procurementfees->value = $request->value;
            $procurementfees->warehouseId = $request->warehouseId;
           
            $procurementfees->save();
            $procurementfeesId = $procurementfees->id;

            return redirect('administrator/procurementfees')->with('successMessage', 'Procurement fees saved successfuly.');
        }
    }

    public function deletedata($id = '',$page='') {

        $procurementfees= new Procurementfees();

        $createrModifierId = Auth::user()->id;
        if(!empty($id)){
        $procurementfees = $procurementfees->find($id);        

        if (Procurementfees::deleteRecord($id, $createrModifierId)) {
               return \Redirect::to('administrator/procurementfees/?page='.$page)->with('successMessage', 'Driver deleted successfuly.');
            } else {
                return \Redirect::to('administrator/procurementfees/?page=' .$page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/procurementfees/?page=' .$page)->with('errorMessage', 'Error in operation!');
        }
    }


    public function procurementDelete(Request $request) {
        $idsToDelete = $request->input('deleteData');
        $createrModifierId = Auth::user()->id;
          foreach ($idsToDelete as $eachId) {
            $procurementfees = new Procurementfees();

            $procurementfees = $procurementfees->find($eachId);
           
            
                if (Procurementfees::deleteRecord($eachId, $createrModifierId)) {
                       return \Redirect::to('administrator/procurementfees/index')->with('successMessage', 'Information deleted successfully.');
                    } else {
                        return \Redirect::to('administrator/procurementfees/index')->with('errorMessage', 'Error in operation!');
                    }
                
            }

        
      
    }
    
    /**
     * Method used to delete multiple records
     * @return type
     */
    public function deleteall(Request $request) {
        $createrModifierId = Auth::user()->id;
        $checkedval = $request->checkedval;

        if (!empty($checkedval)) {
            $idArray = explode('^', $checkedval);

            foreach ($idArray as $id) {
                Procurementfees::deleteRecord($id, $createrModifierId);
            }
            return \Redirect::to('administrator/procurementfees')->with('successMessage', 'Information deleted successfully.');
        } else {
            return \Redirect::to('aadministrator/procurementfees/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
   



}
