<?php
namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Paymentmethod;
use App\Model\Paymentgateway;
use App\Model\Country;
use App\Model\Paymenttaxsettings;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use App\Model\Rewardsetting;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use customhelper;
use Carbon\Carbon;
use Excel;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Hash;
use Mail;

class RewardsettingsController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    /**
     * Method for show configuration of reward
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function index(Route $route, Request $request) {
        $data = array();
        
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.RewardSettings'), Auth::user()->id); 
        
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            \Session::forget('REWARD');
            \Session::push('REWARD.searchDisplay', $searchDisplay);
            \Session::push('REWARD.field', $field);
            \Session::push('REWARD.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('REWARD.field');
            $sortType = \Session::get('REWARD.type');
            $searchDisplay = \Session::get('REWARD.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
            'points' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* GET THE REWARD RECORD  */
        $rewardData = Rewardsetting::where('deleted', '0')->orderBy($param['field'], $param['type'])->paginate($param['searchDisplay']);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Point Levels :: ADMIN - Shoptomydoor";
        $data['pageTitle'] = "Point Levels";
        $data['contentTop'] = array('breadcrumbText' => 'Point Levels', 'contentTitle' => 'Point Levels', 'pageInfo' => 'This section allows you to manage point levels settings');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavContent', 'menuSub' => 'leftNavSiteContent2', 'menuSubSub' => 'leftNavRewardManagement16');
        $data['rewardData'] = $rewardData;
        $data['page'] = $rewardData->currentPage();
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        return view('Administrator.rewardsettings.index', $data);
    }


    /**
     * Method for add edit page
     * @param integer $id
     * @param string $page
     * @return array
     */
    public function addedit($id = '0', $page = '') {
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.RewardSettings'), Auth::user()->id);
        $data = array();
        $data['title'] = "Administrative Panel :: Point Levels";
        $data['contentTop'] = array('breadcrumbText' => 'Point Levels', 'contentTitle' => 'Point Levels', 'pageInfo' => 'This section allows you to manage point levels settings');
        $data['page'] = !empty($page) ? $page : '1';

        if (!empty($id)) {
            if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            /* SET DATA FOR VIEW  */
            $data['pageTitle'] = "Edit Point Levels";
            $data['pointId'] = $id;
            $data['page'] = $page;
            $data['action'] = 'Edit';
            $data['rewardData'] = Rewardsetting::find($id);
        } else {
            if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            /* SET DATA FOR VIEW  */
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add Point Levels";
            $data['page'] = $page;
        }
        return view('Administrator.rewardsettings.addedit', $data);
    }


    /**
     * Method used to save information
     * @param integer $id
     * @param string $page
     * @param Request $request
     * @return array
     */
    public function savedata($id, $page, Request $request) {

        $data = array();
        
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.RewardSettings'), Auth::user()->id);
        $data['page'] = !empty($page) ? $page : '1';
        $reward = new Rewardsetting;

        /*  SET CUSTOM MESSAGE FOR VALIDATION  */
        $messages = [
            'image.max' => 'Image maximum size exceed. ',
        ];
        
        $validator = Validator::make($request->all(), [
            'image' => 'max:4096',
        ], $messages);


        /*  IF VALIDATION FAIL */
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->customMessages);
        }

        /*  EDIT */
        if ($id != 0) {
            if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }

            /* CHECK WHETHER THE POINTS IS BELOW OR GREATER THAN THE PREVIOUS RECORD, IF SO THEN REDIRECT WITH ERROR MESSAGE */
            $checkPrevRecord = Rewardsetting::where('deleted', '0')->where('id', '<', $id)->orderby('id', 'desc')->first();
            if(!empty($checkPrevRecord)){
                if($request->points <= $checkPrevRecord->points){
                    return \Redirect::to('administrator/rewardsettings?page=' . $page)->with('errorMessage', 'Point is below than the previous reward point');
                }
            } else {
                $checkNextRecord = Rewardsetting::where('deleted', '0')->where('id', '>', $id)->orderby('id', 'asc')->first();
                if($request->points >= $checkNextRecord->points){
                    return \Redirect::to('administrator/rewardsettings?page=' . $page)->with('errorMessage', 'Point is higher than the next reward point');
                }
            }

            if ($request->hasFile('image')) {
                $image = $request->file('image');

                if (substr($image->getMimeType(), 0, 5) == 'image') {

                    $info = pathinfo($image->getClientOriginalName());
                    if($info['extension'] == 'bmp' || $info['extension'] == 'svg'){
                        return \Redirect::to('administrator/rewardsettings?page=' . $page)->with('errorMessage', 'Please upload an image with jpeg, jpg, gif, png extension');
                    }

                    $name = time() . '_' . $image->getClientOriginalName();
                    $destinationPath = public_path('/uploads/rewardpoints');
                    $image->move($destinationPath, $name);
                    
                    /* RESIZE THE IMAGE AND SAVED IN DIFFERENT PATHS */
                    $res100 = $this->createThumbnail($name, 100, 100, public_path('/uploads/rewardpoints'), public_path('/uploads/rewardpoints/'));
                    $res300 = $this->createThumbnail($name, 300, 300, public_path('/uploads/rewardpoints'), public_path('/uploads/rewardpoints/'));

                    /* SAVING REWARD RECORD  */
                    $reward = Rewardsetting::find($id);
                    $reward->image = $name;
                    $reward->modifiedBy = Auth::user()->id;
                    $reward->modifiedOn = Config::get('constants.CURRENTDATE');
                    $reward->save();
                } 
            }

            /* SAVING REWARD RECORD  */
            $reward = Rewardsetting::find($id);
            $reward->name = $request->name;
            $reward->descr = $request->descr;
            $reward->points = $request->points;
            $reward->note_10 = $request->note_10;
            $reward->note_100 = $request->note_100;
            $reward->note_50 = $request->note_50;
            $reward->modifiedBy = Auth::user()->id;
            $reward->modifiedOn = Config::get('constants.CURRENTDATE');
            $reward->save(); 

            
        } else {
            /*  ADD */
            if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }

            /* CHECK WHETHER THE POINTS IS BELOW OR GREATER THAN THE PREVIOUS RECORD, IF SO THEN REDIRECT WITH ERROR MESSAGE */
            $checkPrevRecord = Rewardsetting::where('deleted', '0')->where('id', 'desc')->orderby('id', 'desc')->first();
            if(!empty($checkPrevRecord)){
                if($request->points <= $checkPrevRecord->points){
                    return \Redirect::to('administrator/rewardsettings?page=' . $page)->with('errorMessage', 'Point is below than the previous reward point');
                }
            }

            
            /* SET THE IMAGE UPLOADED  */
            if ($request->hasFile('image')) {
                $image = $request->file('image');

                if (substr($image->getMimeType(), 0, 5) == 'image') {

                    $info = pathinfo($image->getClientOriginalName());
                    if($info['extension'] == 'bmp' || $info['extension'] == 'svg'){
                        return \Redirect::to('administrator/rewardsettings?page=' . $page)->with('errorMessage', 'Please upload an image with jpeg, jpg, gif, png extension');
                    }

                    $name = time() . '_' . $image->getClientOriginalName();
                    $destinationPath = public_path('/uploads/rewardpoints');
                    $image->move($destinationPath, $name);
                    
                    /* RESIZE THE IMAGE AND SAVED IN DIFFERENT PATHS */
                    $res100 = $this->createThumbnail($name, 100, 100, public_path('/uploads/rewardpoints'), public_path('/uploads/rewardpoints/'));
                    $res300 = $this->createThumbnail($name, 300, 300, public_path('/uploads/rewardpoints'), public_path('/uploads/rewardpoints/'));

                    /* SAVING REWARD RECORD  */
                    $reward->name = $request->name;
                    $reward->descr = $request->descr;
                    $reward->points = $request->points;
                    $reward->image = $name;
                    $reward->note_10 = $request->note_10;
                    $reward->note_100 = $request->note_100;
                    $reward->note_50 = $request->note_50;
                    $reward->modifiedBy = Auth::user()->id;
                    $reward->modifiedOn = Config::get('constants.CURRENTDATE');
                    $reward->save();
                } else {
                    return \Redirect::to('administrator/rewardsettings?page=' . $page)->with('errorMessage', 'Please upload an image.');
                }
            } else {
                return \Redirect::to('administrator/rewardsettings?page=' . $page)->with('errorMessage', 'Please upload an image.');
            }
            
        }
        

        return redirect('/administrator/rewardsettings?page=' . $page)->with('successMessage', 'Record saved successfuly.');
    }

    


    /**
     * Method used to resize image and save the same
     * @param string $image_name
     * @param integer $new_width
     * @param integer $new_height
     * @param string $uploadDir
     * @param string $moveToDir
     * @return array
     */
    public function createThumbnail($image_name,$new_width,$new_height,$uploadDir,$moveToDir)
    { 

        /* CREATING THE PATH OF THE IMAGE */
        $path = $uploadDir . '/' . $image_name;

        $mime = getimagesize($path);


        if($mime['mime']=='image/png') { 
            $src_img = imagecreatefrompng($path);
        }
        if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
            $src_img = imagecreatefromjpeg($path);
        } 
        if($mime['mime']=='image/gif') { 
            $src_img = imagecreatefromgif($path);
        }
        if($mime['mime']=='image/x-ms-bmp') { 
            $src_img = imagecreatefrombmp($path);
        }  

        $old_x          =   imageSX($src_img);
        $old_y          =   imageSY($src_img);

        if($old_x > $old_y) 
        {
            $thumb_w    =   $new_width;
            $thumb_h    =   $old_y*($new_height/$old_x);
        }

        if($old_x < $old_y) 
        {
            $thumb_w    =   $old_x*($new_width/$old_y);
            $thumb_h    =   $new_height;
        }

        if($old_x == $old_y) 
        {
            $thumb_w    =   $new_width;
            $thumb_h    =   $new_height;
        }

        $dst_img        =   ImageCreateTrueColor($thumb_w,$thumb_h);

        imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 


        /* NEW SAVE LOCATION */
        $new_thumb_loc = $moveToDir . $image_name;

        if($mime['mime']=='image/png') {
            $result = imagepng($dst_img,$new_thumb_loc,8);
        }
        if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg' || $mime['mime']=='image/gif' || $mime['mime']=='image/x-ms-bmp') {
            $result = imagejpeg($dst_img,$new_thumb_loc,80);
        }

        imagedestroy($dst_img); 
        imagedestroy($src_img);

        return $result;
    }


    /**
     * Method used to delete
     * @param integer $id
     * @param string $page
     * @return array
     */
    public function delete($id, $page) {
        
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.RewardSettings'), Auth::user()->id);
        if($findRole['canDelete'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {

            /* GET THE RECORD */
            $getValue = Rewardsetting::where('id', $id)->get();
            $fileName = $getValue[0]->image;
            $destinationPath = public_path('/uploads/rewardpoints/');
            $path = $destinationPath. $fileName;
            chmod($path,0777);


            /*  DELETE RECORD  */
            if (Rewardsetting::deleteRecord($id, $createrModifierId)) {
                unlink($path);
                
                return \Redirect::to('administrator/rewardsettings?page=' . $page)->with('successMessage', 'Record deleted successfully.');
            } else {
                return \Redirect::to('administrator/rewardsettings?page=' . $page)->with('errorMessage', 'File in operation!');
            }
        } else {
            return \Redirect::to('administrator/rewardsettings?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }


}

 
