<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Procurement;
use App\Model\Procurementitem;
use App\Model\Procurementitemstatus;
use App\Model\Automake;
use App\Model\Automodel;
use App\Model\Autowebsite;
use App\Model\Deliverycompany;
use App\Model\Dispatchcompany;
use App\Model\Shippingmethods;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\Invoice;
use App\Model\Shipment;
use App\Model\Shipmentdelivery;
use App\Model\Shipmentpackage;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Excel;
use Mail;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;
use PDF;

class AutopartsController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {

        $searchAutoPartsArr = array(
            'idFrom' => '',
            'idTo' => '',
            'totalCostFrom' => '',
            'totalCostTo' => '',
            'toCountry' => '',
            'toState' => '',
            'toCity' => '',
            'deliveryCompanyId' => '',
            'status' => '',
            'paymentStatus' => '',
            'user' => '',
            'unitNumber' => '',
        );

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id); // call the helper function
        if ($findRole['canView'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchAutoParts = \Input::get('searchAutoParts', $searchAutoPartsArr);
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /* SET SESSION VALUE FOR SORTING  */
            \Session::forget('AUTOPARTSDATA');
            \Session::push('AUTOPARTSDATA.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('AUTOPARTSDATA.searchByDate', $searchByDate);
            \Session::push('AUTOPARTSDATA.searchAutoParts', $searchAutoParts);
            \Session::push('AUTOPARTSDATA.searchDisplay', $searchDisplay);
            \Session::push('AUTOPARTSDATA.field', $field);
            \Session::push('AUTOPARTSDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchAutoParts'] = $searchAutoParts;
        } else {
            $sortField = \Session::get('AUTOPARTSDATA.field');
            $sortType = \Session::get('AUTOPARTSDATA.type');
            $searchDisplay = \Session::get('AUTOPARTSDATA.searchDisplay');
            $searchByCreatedOn = \Session::get('AUTOPARTSDATA.searchByCreatedOn');
            $searchByDate = \Session::get('AUTOPARTSDATA.searchByDate');
            $searchAutoParts = \Session::get('AUTOPARTSDATA.searchAutoParts');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchAutoParts'] = !empty($searchAutoParts) ? $searchAutoParts[0] : $searchAutoPartsArr;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'firstName' => array('current' => 'sorting'),
            'email' => array('current' => 'sorting'),
            'totalcost' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
            'status' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH AUTO PICKUP LIST  */
        $autopartsData = Procurement::getautoPartsList($param);

        /* FETCH DELIVERY COMPANY LIST  */
        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();
        /* FETCH DISPATCH COMPANY LIST  */
        $data['dispatchCompanyList'] = Dispatchcompany::where('status', '1')->orderby('name', 'asc')->get();
        /* FETCH COUNTRY LIST  */
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();


        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Auto Parts";
        $data['contentTop'] = array('breadcrumbText' => 'Auto Parts', 'contentTitle' => 'Auto Parts', 'pageInfo' => 'This section allows you to manage the Auto Parts details');
        $data['pageTitle'] = "Auto Parts";
        $data['autopartsData'] = $autopartsData;
        $data['page'] = $autopartsData->currentPage();
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavProcurement', 'menuSub' => 'leftNavProcurementServices7', 'menuSubSub' => 'leftNavAutoParts73');
        $data['canView'] = $findRole['canView'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        // echo "<pre>"; print_r($data['autopartsData']); die;

        return view('Administrator.autoparts.index', $data);
    }

    /**
     * Method to fetch state list
     * @return array
     */
    public function getstatelist($countryId = '') {
        $country = Country::find($countryId);
        $stateList = array();
        if (isset($country->code) && !empty($country->code)) {

            $stateList = State::where('status', '1')->where('deleted', '0')->where('countryCode', $country->code)->orderby('name', 'asc')->get();
        }
        echo json_encode($stateList);
        exit;
    }

    /**
     * Method to fetch city list
     * @return array
     */
    public function getcitylist($stateid = '') {
        $state = State::find($stateid);
        $cityList = array();
        if (isset($state->code) && !empty($state->code)) {
            $cityList = City::where('status', '1')
                    ->where('deleted', '0')
                    ->where('stateCode', $state->code)
                    ->orderby('name', 'asc')
                    ->get();
        }
        echo json_encode($cityList);
        exit;
    }

    /**
     * Method to manage shipment status
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function changestatus($id = '0', $page = '', Request $request) {
        if (\Request::isMethod('post')) {
            $autoparts = new Procurement;
            $validator = Validator::make($request->all(), [
                        'status' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            } else {
                if (!empty($id)) {
                    $autoparts = Autopickuprequest::find($id);
                    $autoparts->status = $request->status;
                    $autoparts->modifiedBy = Auth::user()->id;
                    $autoparts->modifiedOn = Config::get('constants.CURRENTDATE');
                    $autoparts->save();

                    return \Redirect::to('administrator/autoparts/?page=' . $page)->with('successMessage', 'Auto pickup status updated successfully.');
                }
            }
        }

        /* SET DATA FOR VIEW  */

        $data['pageTitle'] = "Change Status";
        $data['id'] = $id;
        $data['page'] = $page;

        $data['autopartsData'] = Procurement::find($id);

        return view('Administrator.autoparts.changestatus', $data);
    }

    /**
     * Method to view request details
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function view($id = '0', $page = '') {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Auto Parts";
        $data['contentTop'] = array('breadcrumbText' => 'Auto Parts', 'contentTitle' => 'Auto Parts', 'pageInfo' => 'This section allows you to manage the Auto Parts details');
        $data['pageTitle'] = "View Request Details";
        $data['procurementId'] = $id;
        $data['page'] = $page;
        $data['autopartsData'] = Procurement::getAutopartsDetails($id)->toArray();


        /* FETCH DELIVERY COMPANY LIST  */
        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();

        /* FETCH CATEGORY LIST  */
        $data['categoryList'] = \App\Model\Sitecategory::where('parentCategoryId', '-1')->where('type', 'autoparts')->get()->toArray();
        ;

        /* FETCH STORE LIST  */
        $data['storeList'] = \App\Model\Stores::where('deleted', '0')->where('status', '1')->orderby('storeName', 'asc')->get();

        /* Fetch Make List */
        $data['makeList'] = Automake::where('deleted', '0')->where('type', '1')->get()->toArray();

        /*  FETCH PROCUREMENT ITEM DETAILS */
        $data['autopartsData']['items'] = Procurementitem::getAutoPartsItemDetails($id)->toArray();

        /* Procurement Status checking */

        $procurementItemReceived = (int) $this->checkprocurementstatus($id);


        if (empty($procurementItemReceived))
            $data['procurementItemReceived'] = true;
        else
            $data['procurementItemReceived'] = false;



        /* FETCH WAREHOUSE MESSAGES */
        $data['warehousemessages'] = \App\Model\Warehousemessage::where('status', '1')->get();

        /* FETCH WAREHOUSE NOTES */
        $data['warehouseNotes'] = \App\Model\Shipmentwarehousenotes::where('shipmentId', $id)->orderBy('id', 'desc')->limit(5)->get()->toArray();

        /* FETCH INVOICE DATA */
        $invoiceData = array();

        $invoiceData = Invoice::where('procurementId', $id)->where('type', 'autopart')->get()->toArray();

        $data['invoiceData'] = array();

        if (isset($invoiceData) && count($invoiceData) > 0) {
            $data['invoiceData'] = $invoiceData;
        }

        /* FETCH SHIPPING METHOD LIST  */
        $data['shippingMethodList'] = \App\Model\Shippingmethods::where('deleted', '0')->where('active', 'Y')->orderby('shipping', 'asc')->get();

        /* FETCH PAYMENT METHOD LIST  */
        $data['paymentMethodList'] = \App\Model\Paymentmethod::where('deleted', '0')->where('status', '1')->orderby('paymentMethod', 'asc')->get();

        $data['adminRole'] = Auth::user()->userType;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        $data['leftMenuSelection'] = array('menuMain' => 'leftNavProcurement', 'menuSub' => 'leftNavProcurementServices7', 'menuSubSub' => 'leftNavAutoParts73');

        return view('Administrator.autoparts.view', $data);
    }

    /**
     * Method used to clear search history
     * @return html
     */
    public function showall() {
        \Session::forget('AUTOPARTSDATA');
        return \Redirect::to('administrator/autoparts');
    }

    /**
     * Method used to get model list according to make Id
     * @param int $makeId
     * @return json
     */
    public function getmodellist($makeId) {
        $result = Automodel::where('makeId', $makeId)->where('deleted', '0')->where('makeType', '1')->get()->toArray();

        echo json_encode($result);
    }

    /**
     * Method to fetch sub category list
     * @param integer $categoryId
     * @return json
     */
    public function getsubcategorylist($categoryId) {
        $subCategoryList = \App\Model\Sitecategory::getSubCategory($categoryId);

        echo json_encode($subCategoryList);
    }

    /**
     * Method to fetch sub category list
     * @param integer $categoryId
     * @return json
     */
    public function getproductlist($categoryId) {
        $productList = \App\Model\Siteproduct::getListBySubCatid($categoryId);

        echo json_encode($productList);
    }

    /**
     * Method to get location
     * @return Json
     */
    public function getLocation(Request $request) {
        $locationName = $request->q;

        $getCity = Procurement::getCityState($locationName);

        echo json_encode($getCity);
    }

    /**
     * Method to add new car details
     * @param object $request
     * @return boolean
     */
    public function adddeliveryitem(Request $request) {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            $autoShipmentItem = new Procurementitem;

            // print_r($request->all()); die;

            for ($i = 0; $i < count($request->data); $i++) {
                $name = $request->data[$i]['name'];

                if ($name == 'procurementId')
                    $procurementId = $request->data[$i]['value'];

                if ($name != 'addLocationId') {
                    $autoShipmentItem->$name = $request->data[$i]['value'];
                } else if ($name == 'year') {
                    $autoShipmentItem->$name = \Carbon\Carbon::parse($request->data[$i]['value'])->format('Y');
                } else {
                    
                }
            }
            /* SAVE PROCUREMENT DETAILS */

            $autoShipmentItem->save();

            $produrementmainId = $request->shipmentId;

            Procurement::calculatecost($request->shipmentId);

            return $autoShipmentItem->id;
        }
    }

    /**
     * Method to update new car details
     * @param object $request
     * @return boolean
     */
    public function editdeliveryitem(Request $request) {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            $autoShipmentItem = array();

            for ($i = 0; $i < count($request->data); $i++) {
                $name = $request->data[$i]['name'];

                if ($name == 'procurementItemId') {
                    $id = $request->data[$i]['value'];
                } else if ($name != 'editLocationId' && $name != 'automodelId') {
                    $autoShipmentItem[$name] = $request->data[$i]['value'];
                } else if ($name == 'year') {
                    $autoShipmentItem[$name] = \Carbon\Carbon::parse($request->data[$i]['value'])->format('Y');
                } else if ($name == 'editLocationId') {

                    $location = explode('-', $request->data[$i]['value']);
                    //print_r($location); 
                    $autoShipmentItem['pickupStateId'] = $location[1];
                    $autoShipmentItem['pickupCityId'] = $location[0];
                }
            }

            //print_r($autoShipmentItem); die;

            /* SAVE PROCUREMENT DETAILS */
            Procurementitem::where('id', $id)->update($autoShipmentItem);

            $procurementmainId = $request->shipmentId;

            Procurement::calculatecost($procurementmainId);



            return 1;
        }
    }

    /**
     * Method used to fetch & display package details
     * @param integer $id
     * @return string
     */
    public function getitemdetails($id) {

        /* Fetch Make List */

        $data['makeList'] = Automake::where('deleted', '0')->where('type', '1')->get()->toArray();

        /* Fetch Website List */

        //$data['websiteList'] = Autowebsite::where('deleted', '0')->get()->toArray();

        /*  FETCH PROCUREMENT ITEM DETAILS */
        $data['procurementItems'] = Procurementitem::getAutoPartsItemDetails($id)->toArray();

        $data['procurement'] = Procurement::select('shippingMethod', 'shippingMethodId', 'userId')->where('id', $id)->first();

        /* FETCH CATEGORY LIST  */
        $data['categoryList'] = \App\Model\Sitecategory::where('parentCategoryId', '-1')->get()->toArray();
        ;

        /* FETCH STORE LIST  */
        $data['storeList'] = \App\Model\Stores::where('deleted', '0')->where('status', '1')->orderby('storeName', 'asc')->get();

        /* Fetch Make List */
        $data['makeList'] = Automake::where('deleted', '0')->where('type', '1')->get()->toArray();

        return view('Administrator.autoparts.itemdetails', $data);
    }

    /**
     * Method used to delete procurement item
     * @param integer $id
     * @return boolean
     */
    public function deletedeliveryitem($id) {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canDelete'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (!empty($id)) {
            $createrModifier = Auth::user()->email;
            if (Procurementitem::deleteRecord($id, $createrModifier)) {

                /* CALCULATE TOTAL ITEM COST */
                $procurementItem = new Procurementitem;
                $procurementItem = Procurementitem::find($id);
                Procurement::calculateCost($procurementItem->procurementId);

                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Method used to open pop up to upload image
     * @param int $procurementItemId
     */
    public function uploadimage($itemId, $page) {

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $data = array();

        $data['itemId'] = $itemId;
        $data['page'] = $page;

        $procurement = Procurementitem::find($itemId)->toArray();



        $data['procurementId'] = $procurement['procurementId'];

        $data['itemImage'] = $procurement['itemImage'];


        return view('Administrator.autoparts.uploadimage', $data);
    }

    /**
     * Method used to upload and save car image
     * @param int $id, $procurementItemId
     * @param $request
     */
    public function saveimage($id, $procurementId, $page, Request $request) {
        $data = array();

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $data['procurementId'] = !empty($procurementId) ? $procurementId : '1';
        $data['id'] = 0;


        $autoShipmentItem = array();

        $validator = Validator::make($request->all(), [
                    'itemimage' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

            if ($request->hasFile('itemimage')) {
                $image = $request->file('itemimage');
                $name = time() . '_' . $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/auto/parts');
                $image->move($destinationPath, $name);
            } else {
                $name = $request->carimage;
            }

            $autoShipmentItem['itemImage'] = $name;

            $saveData = Procurementitem::where('id', $id)->update($autoShipmentItem);

            return redirect('/administrator/autoparts/view/' . $procurementId . '/' . $page)->with('successMessage', 'Item image saved successfuly.');
        }
    }

    /**
     * Method used to update procurement item status
     * @param integer $id
     * @return boolean
     */
    public function updateprocurementitemstatus(Request $request) {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $procurementItemIds = $request->procurementItemIds;
        $status = $request->status;
        //Count Procurement Item and Count item for selected Status
        $noOfItem = count(Procurementitem::where('procurementId', $request->shipmentId)->get());


        if (!empty($procurementItemIds)) {
            $procurementItemArr = explode('^', $procurementItemIds);

            foreach ($procurementItemArr as $id) {
                $procurementItem = new Procurementitem;
                $procurementItem = Procurementitem::find($id);

                /* INSERT DATA INTO STATUS LOG TABLE */
                $procurementitemstatus = new Procurementitemstatus;
                $procurementitemstatus->procurementId = $procurementItem->id;
                $procurementitemstatus->procurementItemId = $id;
                $procurementitemstatus->oldStatus = $procurementItem->status;
                $procurementitemstatus->status = $status;
                $procurementitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
                $procurementitemstatus->save();

                /* UPDATE PROCUREMENT ITEM TABLE */
                $procurementItem->status = $status;
                $procurementItem->save();
            }


            $itemforSelectedStatus = Procurementitem::where('procurementId', $request->shipmentId)->where('status', $status)->get();

            $noofItemforSelectedStatus = count($itemforSelectedStatus);

            //echo $noOfItem; echo $noofItemforSelectedStatus; die;
            //Update Procurement Status if all items are in same status --- 12-04-2019
            if ($noOfItem == $noofItemforSelectedStatus) {

                $procurement = new Procurement;
                $procurement = Procurement::find($request->shipmentId);

                //'draft','requestforcost','submitted','orderplaced','itemreceived','completed','rejected'

                if ($status == 'submitted') {
                    $procurement->status = 'submitted';
                } else if ($status == 'orderplaced') {
                    $procurement->status = 'orderplaced';
                } else if ($status == 'received') {
                    $procurement->status = 'itemreceived';
                } else if ($status == 'unavailable') {
                    $procurement->status = 'rejected';
                } else {
                    $procurement->status = 'completed';
                }

                $procurement->save();
            }

            $procurementDetails = Procurement::find($procurementItem->procurementId);
            if ($status != "unavailable") {

                if ($status == "orderplaced") {
                    $templateKey = "autoparts_order_placed";
                } else if ($status == "received") {
                    $templateKey = "autoparts_received";
                }

                $replace['NAME'] = $procurementDetails->toName;
                $replace['[ORDERID]'] = $id;

                /* Save & Send Notification */
                \App\Model\User::sendPushNotification($procurementDetails->userId, $templateKey, Auth::user()->id, $replace);
            }

            /* CHECK AND UPDATE PROCUREMENT STATUS */
            Procurement::updaterejectedstatus($procurementItem->id);

            return 1;
        }
        return 0;
    }

    /**
     * Method used to check and update procurement status
     * @return booelan
     */
    public function checkprocurementstatus($id = '') {
        $procurementUnavailableStatus = Procurementitem::where('status', 'unavailable')->where('deleted', '0')->where('procurementId', $id)->count('id');
        $procurementReceivedStatus = Procurementitem::where('status', 'received')->where('deleted', '0')->where('procurementId', $id)->count('id');

        $totalItems = Procurementitem::where('deleted', '0')->where('procurementId', $id)->count('id');

        if (!empty($procurementReceivedStatus) && ($totalItems == $procurementReceivedStatus + $procurementUnavailableStatus)) {
            return 0;
        } else {
            //  Procurement::where('id', $id)->update(array('status' => 'submitted'));

            return 1;
        }
    }

    /**
     * Method used to display and update receive package info
     * @param integer $id
     * @return string
     */
    public function receiveitem($id, $status, Request $request) {

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            // print_r($request->all());die;
            if (!empty($id)) {
                $procurementItemArr = explode('^', $id);
                $procurementItemData = array();

                $procurementId = Procurementitem::where('id', $procurementItemArr[0])->pluck('procurementId')->first();
                $noOfItem = count(Procurementitem::where('procurementId', $procurementId)->get());

                foreach ($procurementItemArr as $itemId) {
                    $trackingNumber = 'trackingNumber' . $itemId;
                    $deliveryCompanyId = 'deliveryCompanyId' . $itemId;
                    $receivedDate = 'receivedDate' . $itemId;
                    $deliveryNotes = 'deliveryNotes' . $itemId;

                    /* SAVE PROCUREMENT DETAILS */
                    $procurementItemData['status'] = $status;
                    $procurementItemData['trackingNumber'] = $request->$trackingNumber;
                    $procurementItemData['deliveryCompanyId'] = $request->$deliveryCompanyId;
                    $procurementItemData['receivedDate'] = \Carbon\Carbon::parse($request->$receivedDate)->format('Y-m-d');
                    $procurementItemData['deliveryNotes'] = $request->$deliveryNotes;
                    Procurementitem::where('id', $itemId)->update($procurementItemData);

                    /* INSERT DATA INTO STATUS LOG TABLE */
                    $procurementItem = Procurementitem::find($itemId);
                    $procurementitemstatus = new Procurementitemstatus;
                    $procurementitemstatus->procurementId = $procurementItem->id;
                    $procurementitemstatus->procurementItemId = $itemId;
                    $procurementitemstatus->oldStatus = $procurementItem->status;
                    $procurementitemstatus->status = $status;
                    $procurementitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
                    $procurementitemstatus->save();
                }

                $itemforSelectedStatus = Procurementitem::where('procurementId', $procurementId)->where('status', 'received')->get();

                $noofItemforSelectedStatus = count($itemforSelectedStatus);

                // echo $noOfItem; echo $noofItemforSelectedStatus; die;
                //Update Procurement Status if all items are in same status --- 12-04-2019
                if ($noOfItem == $noofItemforSelectedStatus) {
                    $procurement = new Procurement;
                    $procurement = Procurement::find($procurementId);
                    $procurement->status = 'itemreceived';
                    $procurement->save();
                }

                $procurementDetails = Procurement::find($procurementItem->procurementId);
                $templateKey = "autoparts_received";
                $emailTemplate = \App\Model\Emailtemplate::where('templateKey', $templateKey)->first();
                $replace['NAME'] = $procurementDetails->toName;
                $replace['[ORDERID]'] = $id;

                /* Save & Send Notification */
                \App\Model\User::sendPushNotification($procurementDetails->userId, $templateKey, Auth::user()->id, $replace);

                $to = $procurementDetails->toEmail;
                $isSend = \App\Helpers\customhelper::SendMail($emailTemplate, $replace, $to);

                $toMobile = trim($procurementDetails->toPhone);
                $smsTemplate = \App\Model\Smstemplate::where('templateKey', $templateKey)->first();
//        $link = Config::get('constants.frontendUrl');
//        $replace['[WEBSITE_LINK]'] = $link;
                $isSendMsg = customhelper::sendMSG($toMobile, $replace, $smsTemplate);
                /* Notification end */

                return 1;
            } else {
                return 0;
            }
        }

        $data['id'] = $id;
        $data['status'] = $status;
        $data['pageTitle'] = "Receive Package in Warehouse";

        /* FETCH DELIVERY COMPANY LIST  */
        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();

        return view('Administrator.autoparts.receiveitem', $data);
    }

    /**
     * Method used to fetch & display the shipment details
     * @param integer $id
     * @return string
     */
    public function viewtrackingdetails($id) {
        $data['pageTitle'] = "View Tracking Details";
        /*  FETCH PROCUREMENT DETAILS */
        $data['procurement'] = Procurementitem::find($id);


        $data['deliverycompany'] = Procurementitem::find($id)->deliverycompany()->first();
        $data['procurement']['receivedDate'] = \Carbon\Carbon::parse($data['procurement']['receivedDate'])->format('d-m-Y');

        return view('Administrator.autoparts.viewtrackingdetails', $data);
    }

    /**
     * Method used to display notification message
     * @param integer $messageId
     * @param integer $procurementId
     * @return string
     */
    public function notifycustomer($messageId, $id, Request $request) {

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }


        if (\Request::isMethod('post')) {

            $warehouseMessage = new \App\Model\Procurementwarehousemessage;
            $warehouseMessage->procurementId = $id;
            $warehouseMessage->messageId = $messageId;
            $warehouseMessage->sentBy = Auth::user()->id; //
            $warehouseMessage->sentOn = Config::get('constants.CURRENTDATE');

            $procurement = Procurement::find($id);
            $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'notification_admin')->first();
            $replace['[NAME]'] = $procurement->fromName;
            $replace['[NOTIFICATION]'] = nl2br($request->message);

            $to = $procurement->fromEmail;
            $sendMail = customhelper::SendMail($emailTemplate, $replace, $to);

            $warehouseMsg = \App\Model\Warehousemessage::find($messageId);

            /* Save and Send Push Notification */
            $replacearr = array('[TOPIC]' => $warehouseMsg->name, '[NOTIFICATION]' => nl2br($request->message));
            User::sendPushNotification($procurement->userId, 'warehouse_message_notification', Auth::user()->id, $replacearr);

            if ($sendMail) {
                if ($warehouseMessage->save()) {
                    return redirect()->back()->with('successMessage', 'Notification sent successfully');
                } else {
                    return redirect()->back()->with('errorMessage', 'Record not added');
                }
            } else {
                return redirect()->back()->with('errorMessage', 'Notification not sent');
            }
        }

        $data = array();

        $data['messageId'] = $messageId;
        $data['id'] = $id;
        $data['warehousemsg'] = \App\Model\Warehousemessage::select('message')->where('id', $messageId)->first();

        return view('Administrator.autoparts.notifycustomer', $data);
    }

    /**
     * Method used to fetch & display the shipment details
     * @param integer $id
     * @return string
     */
    public function getprocurementcostdetails($id) {

        /*  CHECK IF NEED TO INPUT WEIGHT (CATEGORY BLANK) */
        $categoryExist = Procurementitem::where('procurementId', $id)->where('deleted', '0')->where('siteProductId', '0')->count('id');

        $procurementItem = Procurementitem::calculateItemWeight($id);
        $procurementItemCount = Procurementitem::select(DB::raw("sum(itemQuantity) AS totalItems"))->where('procurementId', $id)->where('deleted', '0')->first();

        if (!empty($categoryExist)) {
            echo json_encode(array(
                'weightCalculated' => 'N',
                'totalWeight' => $procurementItem->totalWeight,
                'totalQuantity' => $procurementItem->totalQuantity,
                'totalItems' => $procurementItemCount->totalItems)
            );
            exit;
        } else {
            echo json_encode(array(
                'weightCalculated' => 'Y',
                'totalWeight' => $procurementItem->totalWeight,
                'totalQuantity' => $procurementItem->totalQuantity,
                'totalItems' => $procurementItemCount->totalItems)
            );
            exit;
        }
    }

    /**
     * Method used to display procurement weight details
     * @param integer $id
     * @param object $request
     * @return string
     */
    public function procurementweightdetails($id, Request $request) {
        $data['id'] = $id;
        $data['weightCalculated'] = $request->weightCalculated;
        $data['totalWeight'] = $request->totalWeight;
        $data['totalQuantity'] = $request->totalQuantity;
        $data['totalItems'] = $request->totalItems;
        $data['pageTitle'] = "Calculate Item Weight";

        return view('Administrator.autoparts.procurementweightdetails', $data);
    }

    /**
     * Method used to save procurement weight details
     * @param integer $id
     * @param object $request
     * @return boolean
     */
    public function saveweightdetails($id, Request $request) {
        /*  SAVE PROCUREMENT WEIGHT DETAILS */
        $procurement = Procurement::find($id);
        $procurement->totalWeight = $request->totalWeight;
        $procurement->save();

        return 1;
    }

    /**
     * Method used to fetch procurement payment and charge details
     * @param integer $id
     * @param object $request
     * @return boolean
     */
    public function paymentdetails($id) {
        /*  FETCH PROCUREMENT DETAILS */
        $procurementData = Procurement::getAutopartsDetails($id);

        $param = array(
            'userId' => $procurementData->userId,
            'procurementId' => $id,
            'fromCountry' => $procurementData->fromCountry,
            'fromState' => $procurementData->fromState,
            'fromCity' => $procurementData->fromCity,
            'toCountry' => $procurementData->toCountry,
            'toState' => $procurementData->toState,
            'toCity' => $procurementData->toCity,
            'totalWeight' => $procurementData->totalWeight,
            'totalProcurementCost' => $procurementData->totalItemCost,
            'totalQuantity' => $procurementData->itemQuantity,
        );


        /*  FETCH PROCUREMENT SHIPPING METHOD CHARGES DETAILS */
        $data['shippingMethodCharges'] = \App\Model\Shippingmethods::calculateShippingMethodCharges($param);
        $data['procurement'] = $procurementData;

        // echo '<pre>'; print_r($data['shippingMethodCharges']); die;
        return view('Administrator.autoparts.paymentdetails', $data);
    }

    /**
     * Method used to send customer invoice
     * @param integer $id
     * @return boolean
     */
    public function sendcustomerinvoice($id) {

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }


        $packageDetails = array();

        /*  FETCH PROCUREMENT DETAILS */
        $procurementData = Procurement::getAutopartsDetails($id);

        //echo "<pre>"; print_r($procurementData); die;

        /*  FETCH PROCUREMENT ITEM DETAILS */
        $procurementItemList = Procurementitem::getAutoPartsItemDetails($id);

        if (!empty($procurementItemList)) {
            foreach ($procurementItemList as $procurementItemData) {
                $packageDetails[] = array(
                    'id' => $procurementItemData->id,
                    'makeName' => $procurementItemData->makeName,
                    'modelName' => $procurementItemData->modelName,
                    'itemName' => $procurementItemData->itemName,
                    'websiteUrl' => $procurementItemData->websiteUrl,
                    'options' => $procurementItemData->options,
                    'description' => $procurementItemData->itemDescription,
                    'itemMake' => $procurementItemData->itemMake,
                    'itemModel' => $procurementItemData->itemModel,
                    'itemPrice' => $procurementItemData->itemPrice,
                    'itemQuantity' => $procurementItemData->itemQuantity,
                    'itemShippingCost' => $procurementItemData->itemShippingCost,
                    'itemTotalCost' => $procurementItemData->itemTotalCost,
                    'trackingNumber' => $procurementItemData->trackingNumber,
                );
            }
        }

        // DB::enableQuerylog();
        /*  FETCH ADDRESS BOOK DETAILS */
        $addressBookData = \App\Model\Addressbook::where('userId', $procurementData->userId)->where('isDefaultBilling', '1')->where('deleted', '0')->with('country', 'state', 'city')->first();
        // dd(DB::getQuerylog());



        /*  FETCH PROCUREMENT SHIPPING METHOD CHARGES DETAILS */
        $param = array(
            'userId' => $procurementData->userId,
            'procurementId' => $id,
            'fromCountry' => $procurementData->fromCountry,
            'fromState' => $procurementData->fromState,
            'fromCity' => $procurementData->fromCity,
            'toCountry' => $procurementData->toCountry,
            'toState' => $procurementData->toState,
            'toCity' => $procurementData->toCity,
            'totalWeight' => $procurementData->totalWeight,
            'totalProcurementCost' => $procurementData->totalItemCost,
            'totalQuantity' => $procurementData->itemQuantity,
        );

        $shippingMethodCharges = \App\Model\Shippingmethods::calculateShippingMethodCharges($param);



        /*  PREPARE DATA FOR INVOICE PARTICULARS */
        $invoiceData = array(
            'shipment' => array(
                'totalItemCost' => $procurementData->totalItemCost,
                'totalProcessingFee' => $procurementData->totalProcessingFee,
                'urgentPurchaseCost' => $procurementData->urgentPurchaseCost,
                'totalProcurementCost' => $procurementData->totalProcurementCost,
                'totalWeight' => $procurementData->totalWeight,
                'totalQuantity' => $procurementData->itemQuantity,
            ),
            'warehouse' => array(
                'fromAddress' => $procurementData->fromAddress,
                'fromZipCode' => $procurementData->fromZipCode,
                'fromCountry' => $procurementData->fromCountryName,
                'fromState' => $procurementData->fromStateName,
                'fromCity' => $procurementData->fromCityName,
            ),
            'shippingaddress' => array(
                'toCountry' => $procurementData->toCountryName,
                'toState' => $procurementData->toStateName,
                'toCity' => $procurementData->toCityName,
                'toAddress' => $procurementData->toAddress,
                'toZipCode' => $procurementData->toZipCode,
                'toName' => $procurementData->toName,
                'toEmail' => $procurementData->toEmail,
                'toPhone' => $procurementData->toPhone,
            ),
            'shippingcharges' => $shippingMethodCharges,
            'packages' => $packageDetails,
        );

        /*  INSERT DATA INTO INVOICE TABLE */
        $invoiceUniqueId = 'INV' . $procurementData->userUnit . '-' . $id . '-' . date('Ymd');
        $invoice = new \App\Model\Invoice;
        $invoice->invoiceUniqueId = $invoiceUniqueId;
        $invoice->procurementId = $id;
        $invoice->type = 'autopart';
        $invoice->userUnit = $procurementData->userUnit;
        $invoice->userFullName = $addressBookData->firstName . ' ' . $addressBookData->lastName;
        $invoice->userEmail = $addressBookData->email;
        $invoice->userContactNumber = $procurementData->fromPhone;
        $invoice->billingName = $addressBookData->title . ' ' . $addressBookData->firstName . ' ' . $addressBookData->lastName;
        $invoice->billingEmail = $addressBookData->email;
        $invoice->billingAddress = $addressBookData->address;
        $invoice->billingAlternateAddress = $addressBookData->alternateAddress;
        $invoice->billingCity = isset($addressBookData->city) ? $addressBookData->city->name : '';
        $invoice->billingState = isset($addressBookData->state) ? $addressBookData->state->name : '';
        $invoice->billingCountry = $addressBookData->country->name;
        $invoice->billingZipcode = $addressBookData->zipcode;
        $invoice->billingPhone = $addressBookData->phone;
        $invoice->billingAlternatePhone = $addressBookData->alternatePhone;
        $invoice->invoiceParticulars = json_encode($invoiceData);
        $invoice->createdOn = Config::get('constants.CURRENTDATE');

        //echo "<pre>"; print_r($procurementData); die;
        $invoice->save();

        $invoiceId = $invoice->id;

        if (!empty($invoiceId)) {
            $data['invoice'] = \App\Model\Invoice::find($invoiceId);
            $fileName = "AutoPartsInvoice_" . $invoiceUniqueId . ".pdf";
            $frontendUrl = Config::get('constants.frontendUrl') . "auto-parts/order-history#orderhistory";
            //$content = 'Your Shop For Me invoice #' . $id . ' is attached. Please find the cost page at <a href="'. $frontendUrl.'">'.$frontendUrl.'</a>';
            $content = "Your Shop For Me invoice #" . $id . " is attached. Please find the cost page at <a href=" . $frontendUrl . ">" . $frontendUrl . "</a>";
            PDF::loadView('Administrator.autoparts.invoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
            $to = $procurementData->fromEmail;

            Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($invoiceUniqueId, $to, $fileName) {

                $message->from('contact@shoptomydoor.com', 'Shoptomydoor');
                $message->subject($invoiceUniqueId . " - Invoice Details");
                $message->to($to);
                $message->attach(public_path('exports/invoice/' . $fileName));
            });

            /*  UPDATE LOCK STATUS IN PROCUREMENT TABLE */
            $procurement = Procurement::find($id);
            $procurement->procurementLocked = 'Y';
            $procurement->save();

            return 1;
        } else
            return 0;
    }

    /**
     * Method used to change address
     * @param integer $id
     * @param integer $page
     * @param Request $request
     * @return string
     */
    public function changeaddress($id, $userid, Request $request) {

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            $addressBookId = $request->userAddressBookId;
            $procurement = Procurement::find($id);


            if (!empty($addressBookId)) {

                // IF EXISTING ADDRESS FROM ADDRESS BOOK
                /* FETCH USER ADDRESS BOOK DATA */
                $addressBookData = \App\Model\Addressbook::find($addressBookId);

                /* SAVE DATA TO PROCUREMENT TABLE */
                $procurement->toCountry = $addressBookData->countryId;
                $procurement->toState = $addressBookData->stateId;
                $procurement->toCity = $addressBookData->cityId;
                $procurement->toAddress = $addressBookData->address;
                $procurement->toPhone = $addressBookData->phone;
                $procurement->toName = $addressBookData->firstName . " " . $addressBookData->lastName;
                $procurement->toZipCode = $addressBookData->zipcode;
                $procurement->toEmail = $addressBookData->email;
                $procurement->save();
            } else { // IF NEW ADDRESS SELECTED

                /* SAVE DATA TO USER ADDRESS TABLE */
                $addressBook = new \App\Model\Addressbook;
                $addressBook->userId = $userid;
                $addressBook->title = $request->title;
                $addressBook->firstName = $request->firstName;
                $addressBook->lastName = $request->lastName;
                $addressBook->email = $request->email;
                $addressBook->address = $request->address;
                $addressBook->alternateAddress = $request->alternateAddress;
                $addressBook->cityId = $request->cityId;
                $addressBook->stateId = $request->stateId;
                $addressBook->countryId = $request->countryId;
                $addressBook->zipcode = $request->zipcode;
                $addressBook->phone = $request->phone;
                $addressBook->alternatePhone = $request->alternatePhone;
                $addressBook->modifiedBy = Auth::user()->id;
                $addressBook->modifiedOn = Config::get('constants.CURRENTDATE');
                $addressBook->save();

                /* SAVE DATA TO PROCUREMENT TABLE */
                $procurement->toCountry = $request->countryId;
                $procurement->toState = $request->stateId;
                $procurement->toCity = $request->cityId;
                $procurement->toAddress = $request->address;
                $procurement->toPhone = $request->phone;
                $procurement->toName = $request->firstName . " " . $request->lastName;
                $procurement->toEmail = $request->email;
                $procurement->toZipCode = $request->zipcode;
                $procurement->save();
            }

            return 1;
        }


        /* SET DATA FOR VIEW  */
        $data['pageTitle'] = "Change Delivery Address";
        $data['id'] = $id;
        $data['userId'] = $userid;
        $data['addressBookData'] = \App\Model\User::find($userid)->addressbook()->where('deleted', '0')->get(['id', 'firstName', 'lastName']);
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

        return view('Administrator.autoparts.changeaddress', $data);
    }

    /**
     * Method used to fetch address details
     * @param integer $id
     * @return type
     */
    public function getaddressbookdetails($id = '') {
        $addressBook = \App\Model\Addressbook::find($id)->toArray();
        echo json_encode($addressBook);
    }

    /**
     * Method used to fetch updated address address
     * @param integer $id
     * @return string
     */
    public function getaddressdetails($id = '') {
        $data['id'] = $id;

        $shipment = Procurement::getAutopartsDetails($id);
        $data['shipment'] = $shipment;

        return view('Administrator.autoparts.addressdetails', $data);
    }

    /**
     * Method used add comment
     * @param integer $id, Request $request
     * @return boolean
     */
    public function addcomment($shipmentId, Request $request) {
        $data = array();

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $message = $request->comment;

        $shipmentnotes = new \App\Model\Shipmentwarehousenotes;

        $shipmentnotes->shipmentId = $shipmentId;
        $shipmentnotes->message = $message;
        $shipmentnotes->sentBy = Auth::user()->id;
        $shipmentnotes->sentOn = Config::get('constants.CURRENTDATE');

        if ($shipmentnotes->save()) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Method used to send comment
     * @param integer $id, Request $request
     * @return string
     */
    public function sendcomment($id, $shipmentId) {
        $data = array();

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $data['shipmentId'] = $shipmentId;

        $data['customerEmailList'] = \App\Model\UserAdmin::select(array('id', 'email'))->where(array('deleted' => '0', 'status' => '1'))->get()->toArray();

        return view('Administrator.autoparts.sendcomment', $data);
    }

    /**
     * Method used to fetch save comments and notify user
     * @param integer $id
     * @return string
     */
    public function saveandnotify($shipmentId, Request $request) {

        $shipmentnotes = new \App\Model\Shipmentwarehousenotes;

        $emailto[] = $request->admin;

        $shipmentnotes->shipmentId = $shipmentId;
        $shipmentnotes->message = $request->warehousemsg;
        $shipmentnotes->sentBy = Auth::user()->id;
        $shipmentnotes->sentOn = Config::get('constants.CURRENTDATE');

        $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'notification_admin')->first();
        $replace['[NAME]'] = "Admin";
        $replace['[NOTIFICATION]'] = $request->warehousemsg;

        foreach ($emailto[0] as $email) {
            $sendMail = customhelper::SendMail($emailTemplate, $replace, $email);
        }
        if ($sendMail) {
            if ($shipmentnotes->save()) {

                return redirect()->back()->with('successMessage', 'Notes saved and sent successfully');
            } else {

                return redirect()->back()->with('errorMessage', 'Record not added');
            }
        } else {
            return redirect()->back()->with('errorMessage', 'Notes not sent');
        }
    }

    /**
     * Method used to Export All with selected fields
     * @param integer $page
     * @return type
     */
    public function exportall($page, Request $request) {
        // dd(session()->all());
        if (count($request->selectall) == 0) {
            return \Redirect::to('administrator/autoparts/')->with('errorMessage', 'Select atleast one field during the time of export');
        }
        if (\Request::isMethod('post')) {

            $searchAutoPartsArr = array(
                'idFrom' => '',
                'idTo' => '',
                'totalCostFrom' => '',
                'totalCostTo' => '',
                'toCountry' => '',
                'toState' => '',
                'toCity' => '',
                'deliveryCompanyId' => '',
                'status' => '',
                'paymentStatus' => '',
                'user' => '',
            );

            $sortField = \Session::get('AUTOPARTSDATA.field');
            $sortType = \Session::get('AUTOPARTSDATA.type');
            $searchDisplay = \Session::get('AUTOPARTSDATA.searchDisplay');
            $searchByCreatedOn = \Session::get('AUTOPARTSDATA.searchByCreatedOn');
            $searchByDate = \Session::get('AUTOPARTSDATA.searchByDate');
            $searchAutoParts = \Session::get('AUTOPARTSDATA.searchAutoParts');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'procurementid';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchAutoParts'] = !empty($searchAutoParts) ? $searchAutoParts[0] : $searchAutoPartsArr;


            //$recordList = Shipment::getShipmentList($param, 'export', 'P')->toArray();

            $recordList = Procurement::getautoPartsList($param, 'export')->toArray();
            // print_r($recordList); die;
            $shipmentData = array();
            $headers = array();
            if (!empty($recordList)) {
                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {

                            if ($count == 0) {
                                if ($key == 'procurementid')
                                    $cellValue = 'ID';
                                elseif ($key == 'customer')
                                    $cellValue = 'Customer';
                                elseif ($key == 'destinationCityName')
                                    $cellValue = 'Destination City Name';
                                elseif ($key == 'destinationStateName')
                                    $cellValue = 'Destination State Name';
                                elseif ($key == 'destinationCountryName')
                                    $cellValue = 'Destination Country Name';
                                elseif ($key == 'createdOn')
                                    $cellValue = 'Date';
                                elseif ($key == 'totalcost')
                                    $cellValue = 'Cost';
                                elseif ($key == 'status')
                                    $cellValue = 'Status';
                                elseif ($key == 'paymentstatus')
                                    $cellValue = 'Payment Status';

                                $headers[$key] = $cellValue;
                            }
                        }
                    }
                }

                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {
                            if ($key == 'createdOn')
                                $value = \Carbon\Carbon::parse($value)->format('Y-m-d');

                            $shipmentData[$count + 1][$headers[$key]] = $value;
                        }
                    }
                    //print_r($shipmentData);
                }
            }
            // print_r($shipmentData);die;
            ob_end_clean();
            ob_start();
            Excel::create("Autoparts-" . \Carbon\Carbon::now(), function($excel) use($shipmentData) {
                $excel->sheet('Sheet 1', function($sheet) use($shipmentData) {
                    $sheet->fromArray($shipmentData);
                });
            })->export('xlsx');
            ob_flush();


            return \Redirect::to('administrator/autoparts/')->with('successMessage', 'Excel file created and downloaded');
        }
    }

    /**
     * Method used to Export selected with selected fields
     * @param integer $page
     * @return type
     */
    public function exportselected($page, Request $request) {
        /* if (count($request->selectall) == 0) {
          return \Redirect::to('administrator/users/')->with('errorMessage', 'Select atleast one field during the time of export');
          } */
        if (\Request::isMethod('post')) {

            $searchAutoPartsArr = array(
                'idFrom' => '',
                'idTo' => '',
                'totalCostFrom' => '',
                'totalCostTo' => '',
                'toCountry' => '',
                'toState' => '',
                'toCity' => '',
                'deliveryCompanyId' => '',
                'status' => '',
                'paymentStatus' => '',
                'user' => '',
            );

            $sortField = \Session::get('AUTOPARTSDATA.field');
            $sortType = \Session::get('AUTOPARTSDATA.type');
            $searchDisplay = \Session::get('AUTOPARTSDATA.searchDisplay');
            $searchByCreatedOn = \Session::get('AUTOPARTSDATA.searchByCreatedOn');
            $searchByDate = \Session::get('AUTOPARTSDATA.searchByDate');
            $searchAutoParts = \Session::get('AUTOPARTSDATA.searchAutoParts');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'procurementid';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchAutoParts'] = !empty($searchAutoParts) ? $searchAutoParts[0] : $searchAutoPartsArr;
            $param['searchByshipment'] = $request->selected;



            $recordList = Procurement::getautoPartsList($param, 'export')->toArray();

            $shipmentData = array();
            $headers = array();
            if (!empty($recordList)) {
                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        //print_r($request->selectall);die;
                        if (in_array($key, $request->selectall)) {
                            if ($count == 0) {
                                if ($key == 'procurementid')
                                    $cellValue = 'ID';
                                elseif ($key == 'customer')
                                    $cellValue = 'Customer';
                                elseif ($key == 'destinationCityName')
                                    $cellValue = 'Destination City Name';
                                elseif ($key == 'destinationStateName')
                                    $cellValue = 'Destination State Name';
                                elseif ($key == 'destinationCountryName')
                                    $cellValue = 'Destination Country Name';
                                elseif ($key == 'createdOn')
                                    $cellValue = 'Date';
                                elseif ($key == 'totalcost')
                                    $cellValue = 'Cost';
                                elseif ($key == 'status')
                                    $cellValue = 'Status';
                                elseif ($key == 'paymentstatus')
                                    $cellValue = 'Payment Status';

                                $headers[$key] = $cellValue;
                            }
                        }
                    }
                }

                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {

                            if ($key == 'createdOn')
                                $value = \Carbon\Carbon::parse($value)->format('Y-m-d');

                            $shipmentData[$count + 1][$headers[$key]] = $value;
                        }
                    }
                }
            }

            $excelName = "Autoparts-" . \Carbon\Carbon::now();
            $path = public_path('export/' . $excelName);

            ob_end_clean();
            ob_start();
            Excel::create($excelName, function($excel) use($shipmentData) {
                $excel->sheet('Sheet 1', function($sheet) use($shipmentData) {
                    $sheet->fromArray($shipmentData);
                });
            })->store('xlsx', public_path('exports'));
            ob_flush();
            $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
            return response()->json(['path' => $creatingPath]);
        }
    }

    /**
     * Method used to Create Shipment
     * @param integer $page
     * @return type
     */
    public function createshipment($procurementItems, $id) {
        $data = array();
        $siteProduct = new \App\Model\Siteproduct;
        $procurementItem = new Procurementitem;

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $totalWeight = '0.00';
        $procurementItemDetails = \App\Model\Siteproduct::select("weight", "itemQuantity")
                        ->join($procurementItem->table, "$siteProduct->table.id", "=", "$procurementItem->table.siteProductId")
                        ->whereIn("$procurementItem->table.id", explode('^', $procurementItems))->get()->toArray();

        foreach ($procurementItemDetails as $eachItemDetails) {
            $totalWeight += $eachItemDetails['weight'] * $eachItemDetails['itemQuantity'];
        }



        $data['totalWeight'] = round($totalWeight, 2);

        $data['pageTitle'] = "Create Shipment";

        $data['procurementItemIds'] = $procurementItems;
        $data['procurementId'] = $id;
        $procurement = $data['procurement'] = Procurement::find($id);

        //Added to get default weight symbol set in general settings
        $data['weightSymbol'] = \App\Model\Generalsettings:: where('settingsKey', 'weight_symbol')->pluck('settingsValue')->toArray();




        $data['savedShipment'] = Shipment::where('shipmentType', 'autopart')->where('userId', $procurement->userId)->where('paymentStatus', 'unpaid')->where('warehouseId', $procurement->warehouseId)->where('deleted', '0')->get()->toArray();
        //dd( DB::getQueryLog());



        return view('Administrator.autoparts.chooseshipment', $data);
    }

    /**
     * Method used to get unpaid list of an user
     * @param integer $userId 
     * @return string
     */
    public function getunpaidshipmentlist($userId, Request $request) {
        $warehouseId = $request->warehouseId;

        $shipmentList = Shipment::where('shipmentType', 'autopart')->where('userId', $userId)->where('paymentStatus', 'unpaid')->where('warehouseId', $warehouseId)->where('deleted', '0')->get();

        echo json_encode($shipmentList);
        exit;
    }

    /**
     * Method used to Create New Shipment
     * @param integer $page
     * @return type
     */
    public function saveshipment($id, Request $request) {

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }


        $shipmentOption = $request->shipmentOption;
        $procurementItemIds = $request->procurementItemIds;

        $settings = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();
        // Total chargeable weight in case of no product has been selected(No productId present in procurement table)
        $totalChargeableWeight = max($request->itemWeight, (($request->itemHeight * $request->itemWidth * $request->itemLength) / $settings->settingsValue));

        $procurementRaw = Procurement::find($id);
        $procurementRaw->totalWeight = $totalChargeableWeight;
        $procurementRaw->save();



        $totalShipmentCost = $shipmentTotalItemCost = $shipmentTotalWeight = 0;
        $storageDates = array();
        $shipmentStorageCharge = '0.00';
        $trackingFlag = 0;

        if (!empty($id)) {

            /*  FETCH PROCUREMENT DETAILS */
            $procurementData = Procurement::getAutopartsDetails($id);


            /*  FETCH PROCUREMENT ITEM DETAILS */
            $procurementItemList = Procurementitem::getAutoPartsItemDetails($id);
            $procurementSelectedItemList = Procurementitem::getSelectedAutoPartsItemDetails($id, $procurementItemIds);

            if (count($procurementSelectedItemList) > 1) {

                for ($i = 0; $i < count($procurementSelectedItemList); $i++) {

                    if ($procurementSelectedItemList[$i]['trackingNumber'] == $procurementSelectedItemList[$i + 1]['trackingNumber']) {
                        $trackingFlag = 1;
                    }


                    break;
                }
            } else {
                $trackingFlag = 1;
            }

            if ($trackingFlag == 1) {


                if ($shipmentOption == 'addnew') {
                    //Fetch Inventory Charge If First Delivery
                    $inventoryCharge = \App\Model\Generalsettings::where('settingsKey', 'first_delivery_charge')->first();

                    /* INSERT DATA INTO MAIN SHIPMENT TABLE */
                    $shipment = new Shipment;
                    $shipment->userId = $procurementData->userId;
                    $shipment->warehouseId = $procurementData->warehouseId;
                    $shipment->shipmentType = 'autopart';
                    $shipment->fromCountry = $procurementData->fromCountry;
                    $shipment->fromState = $procurementData->fromState;
                    $shipment->fromCity = $procurementData->fromCity;
                    $shipment->fromName = $procurementData->fromName;
                    $shipment->fromAddress = $procurementData->fromAddress;
                    $shipment->fromZipCode = $procurementData->fromZipCode;
                    $shipment->fromPhone = $procurementData->fromPhone;
                    $shipment->fromEmail = $procurementData->fromEmail;
                    $shipment->toCountry = $procurementData->toCountry;
                    $shipment->toState = $procurementData->toState;
                    $shipment->toCity = $procurementData->toCity;
                    $shipment->toName = $procurementData->toName;
                    $shipment->toAddress = $procurementData->toAddress;
                    $shipment->toZipCode = $procurementData->toZipCode;
                    $shipment->toPhone = $procurementData->toPhone;
                    $shipment->toEmail = $procurementData->toEmail;
                    $shipment->shipmentStatus = '1';
                    $shipment->firstReceived = Config::get('constants.CURRENTDATE');
                    $shipment->totalShippingCost = $procurementData->totalShippingCost;
                    $shipment->totalClearingDuty = $procurementData->totalClearingDuty;
                    $shipment->isDutyCharged = $procurementData->isDutyCharged;
                    $shipment->totalInsurance = $procurementData->totalInsurance;
                    $shipment->totalTax = $procurementData->totalTax;
                    $shipment->totalDiscount = $procurementData->totalDiscount;
                    $shipment->totalCost = $procurementData->totalCost;
                    $shipment->prepaid = (!empty($procurementData->shippingMethodId) && $procurementData->paymentStatus == 'paid') ? 'Y' : 'N';
                    $shipment->paymentStatus = (!empty($procurementData->shippingMethodId) && $procurementData->paymentStatus == 'paid') ? 'paid' : 'unpaid';
                    $shipment->paymentMethodId = (!empty($procurementData->shippingMethodId) && $procurementData->paymentStatus == 'paid') ? $procurementData->paymentMethodId : '';
                    $shipment->paymentReceivedOn = $procurementData->paymentReceivedOn;
                    $shipment->createdBy = Auth::user()->id;
                    $shipment->createdByType = 'admin';
                    $shipment->createdOn = Config::get('constants.CURRENTDATE');
                    $shipment->save();
                    $totalShipmentCost = ($procurementData->totalShippingCost + $procurementData->totalClearingDuty + $shipment->totalInsurance + $shipment->totalTax) - $shipment->totalDiscount;
                    $shipmentId = $shipment->id;
                } else {

                    //Fetch Inventory Charge For Other Delivery
                    $inventoryCharge = \App\Model\Generalsettings::where('settingsKey', 'other_delivery_charge')->first();

                    $shipmentId = $request->shipmentId;
                    $shipment = Shipment::find($shipmentId);

                    $totalShipmentCost = $shipment->totalCost;
                    $shipmentTotalItemCost = $shipment->totalCost;
                    $shipmentTotalWeight = $shipment->totalWeight;
                }



                if (!empty($shipmentId)) {
                    /* INSERT DATA INTO DELIVERY MAPPING TABLE */
                    if (!empty($procurementItemIds)) {
                        $procurementItemArray = explode('^', $procurementItemIds);
                        $latestPackageReceivedDate = Procurementitem::whereRaw('id in (' . str_replace('^', ',', $procurementItemIds) . ')')->max('receivedDate');
                    }
                    $maxStorageDate = \App\Model\Generalsettings::where('settingsKey', 'max_storage_days')->first();
                    $chargePerUnit = \App\Model\Generalsettings::where('settingsKey', 'unit_weight_charge')->first();
                    $maxStorageDate = date('Y-m-d', strtotime('+' . $maxStorageDate->settingsValue . ' days', strtotime($latestPackageReceivedDate)));

                    $inventoryChargeCost = !empty($inventoryCharge->settingsValue) ? $inventoryCharge->settingsValue : '0.00';
                    /* $storageCharge = $chargePerUnit->settingsValue * $procurementData->totalWeight;

                      $totalShipmentCost += $inventoryChargeCost;
                      $shipmentStorageCharge += $storageCharge;
                     */
                    $totalCost = $inventoryChargeCost;
                    $storageDates[] = $maxStorageDate;


                    $shipmentDelivery = new Shipmentdelivery;
                    $shipmentDelivery->shipmentId = $shipmentId;
                    $shipmentDelivery->weight = !empty($procurementData->totalWeight) ? $procurementData->totalWeight : 0;
                    $shipmentDelivery->chargeableWeight = !empty($procurementData->totalWeight) ? $procurementData->totalWeight : 0;
                    $shipmentDelivery->shippingCost = !empty($procurementData->totalShippingCost) ? $procurementData->totalShippingCost : 0;
                    $shipmentDelivery->clearingDutyCost = !empty($procurementData->totalClearingDuty) ? $procurementData->totalClearingDuty : 0;
                    $shipmentDelivery->isDutyCharged = $procurementData->isDutyCharged;
                    // $shipmentDelivery->inventoryCharge = $inventoryChargeCost; //Updated On 29-11-2018
                    $shipmentDelivery->tracking = $procurementItemList[0]->trackingNumber;
                    $shipmentDelivery->tracking2 = (!empty($procurementItemList[0]->trackingNumber2) ? $procurementItemList[0]->trackingNumber2 : '');
                    $shipmentDelivery->inventoryCharge = (!empty($procurementData->shippingMethodId) && $procurementData->paymentStatus == 'paid') ? '0.00' : $inventoryChargeCost;
                    $shipmentDelivery->otherChargeCost = '0.00';
                    $shipmentDelivery->maxStorageDate = $maxStorageDate;
                    $shipmentDelivery->totalCost = $totalCost;
                    $shipmentDelivery->received = 'Y';
                    $shipmentDelivery->shippingMethodId = $procurementData->shippingMethodId;
                    $shipmentDelivery->createdBy = Auth::user()->id;
                    $shipmentDelivery->createdByType = 'admin';
                    $shipmentDelivery->createdOn = Config::get('constants.CURRENTDATE');
                    $shipmentDelivery->save();
                    $shipmentDeliveryId = $shipmentDelivery->id;

                    if (!empty($procurementItemIds)) {
                        $procurementItemArray = explode('^', $procurementItemIds);
                        $deliveryTotalItemCost = $deliveryWeight = $deliveryChargeableWeight = 0;
                        foreach ($procurementItemArray as $procurementItemId) {
                            /*  FETCH PROCUREMENT ITEM DETAILS */
                            $procurementItemData = Procurementitem::find($procurementItemId);
                            $procurementItemData->status = 'addedtodelivery';
                            $procurementItemData->save();
                            $received = $procurementItemData->status == 'received' ? 'Y' : 'N';

                            if (!empty($totalChargeableWeight)) {
                                $weight = $totalChargeableWeight;
                                $shipmentTotalWeight = $weight;
                                $deliveryWeight = $weight;
                                $deliveryChargeableWeight = $weight;
                            } else {
                                if (!empty($procurementItemData->siteProductId)) {
                                    $siteProduct = \App\Model\Siteproduct::find($procurementItemData->siteProductId);
                                    $weight = $siteProduct->weight;
                                    $shipmentTotalWeight += ($procurementItemData->itemQuantity * $weight);
                                    $deliveryWeight += ($procurementItemData->itemQuantity * $weight);
                                    $deliveryChargeableWeight += ($procurementItemData->itemQuantity * $weight);
                                }
                            }

                            $deliveryTotalItemCost += $procurementItemData->itemTotalCost;
                            $shipmentTotalItemCost += $procurementItemData->itemTotalCost;

                            $shipmentPackage = new Shipmentpackage;
                            $shipmentPackage->shipmentId = $shipmentId;
                            $shipmentPackage->deliveryId = $shipmentDeliveryId;
                            $shipmentPackage->storeId = $procurementItemData->storeId;
                            $shipmentPackage->siteCategoryId = $procurementItemData->siteCategoryId;
                            $shipmentPackage->siteSubCategoryId = $procurementItemData->siteSubCategoryId;
                            $shipmentPackage->siteProductId = $procurementItemData->siteProductId;
                            $shipmentPackage->itemName = $procurementItemData->itemName;
                            $shipmentPackage->websiteUrl = $procurementItemData->websiteUrl;
                            $shipmentPackage->weight = !empty($procurementItemData->siteProductId) ? $siteProduct->weight : 0;
                            $shipmentPackage->options = $procurementItemData->options;
                            $shipmentPackage->itemDescription = $procurementItemData->itemDescription;
                            $shipmentPackage->itemMake = $procurementItemData->itemMake;
                            $shipmentPackage->itemModel = $procurementItemData->itemModel;
                            $shipmentPackage->itemQuantity = $procurementItemData->itemQuantity;
                            $shipmentPackage->itemPrice = $procurementItemData->itemPrice;
                            $shipmentPackage->itemMinPrice = $procurementItemData->itemPrice;
                            $shipmentPackage->itemPriceEdited = '1';
                            $shipmentPackage->itemShippingCost = $procurementItemData->itemShippingCost;
                            $shipmentPackage->itemTotalCost = $procurementItemData->itemTotalCost;
                            $shipmentPackage->deliveryCompanyId = $procurementItemData->deliveryCompanyId;
                            $shipmentPackage->deliveredOn = $procurementItemData->receivedDate;
                            $shipmentPackage->tracking = $procurementItemData->trackingNumber;
                            $shipmentPackage->tracking2 = (!empty($procurementItemData->trackingNumber2) ? $procurementItemData->trackingNumber2 : '');
                            $shipmentPackage->type = "I";
                            $shipmentPackage->deliveryNotes = $procurementItemData->deliveryNotes;
                            $shipmentPackage->createdBy = Auth::user()->id;
                            $shipmentPackage->createdOn = Config::get('constants.CURRENTDATE');
                            $shipmentPackage->save();
                        }

                        $deliveryTotalCost = $deliveryTotalItemCost + $inventoryChargeCost;
                        $storageCharge = round(($chargePerUnit->settingsValue * $deliveryWeight), 2);
                        $shipmentStorageCharge += $storageCharge;
                        $totalShipmentCost += $inventoryChargeCost;

                        $shipmentDelivery = Shipmentdelivery::find($shipmentDeliveryId);
                        $shipmentDelivery->length = $request->itemLength;
                        $shipmentDelivery->width = $request->itemWidth;
                        $shipmentDelivery->height = $request->itemHeight;
                        $shipmentDelivery->weight = $request->itemWeight;
                        $shipmentDelivery->chargeableWeight = $deliveryChargeableWeight;
                        $shipmentDelivery->totalItemCost = $deliveryTotalItemCost;
                        $shipmentDelivery->storageCharge = $storageCharge;
                        $shipmentDelivery->tracking = $procurementItemList[0]->trackingNumber;
                        $shipmentDelivery->tracking2 = (!empty($procurementItemList[0]->trackingNumber2) ? $procurementItemList[0]->trackingNumber2 : '');
                        $shipmentDelivery->totalCost = $deliveryTotalCost;
                        $shipmentDelivery->save();

                        if (count($procurementItemList) == count($procurementItemArray)) {
                            // SAVE PROCUREMENT DETAILS //
                            $procurement = Procurement::find($id);
                            $procurement->procurementLocked = 'Y';
                            $procurement->status = 'completed';
                            $procurement->save();
                        } else {
                            $procurementUnavailableStatus = Procurementitem::where('status', 'unavailable')->where('deleted', '0')->where('procurementId', $id)->count('id');
                            $procurementDeliveredStatus = Procurementitem::where('status', 'addedtodelivery')->where('deleted', '0')->where('procurementId', $id)->count('id');

                            $totalItems = Procurementitem::where('deleted', '0')->where('procurementId', $id)->count('id');

                            if (!empty($procurementDeliveredStatus) && ($totalItems == $procurementDeliveredStatus + $procurementUnavailableStatus)) {
                                // SAVE PROCUREMENT DETAILS //
                                $procurement = Procurement::find($id);
                                $procurement->procurementLocked = 'Y';
                                $procurement->status = 'completed';
                                $procurement->save();
                            }
                        }

                        $shipmentstauslog = new \App\Model\Shipmentstatuslog;
                        $shipmentstauslog->shipmentId = $shipmentId;
                        $shipmentstauslog->deliveryId = $shipmentDeliveryId;
                        $shipmentstauslog->oldStatus = 'none';
                        $shipmentstauslog->status = 'in_warehouse';
                        $shipmentstauslog->updatedOn = Config::get('constants.CURRENTDATE');
                        $shipmentstauslog->save();
                    }

                    $totalShipmentCost = round($totalShipmentCost, 2);
                    $shipment->totalItemCost = round($shipmentTotalItemCost, 2);
                    $shipment->totalWeight = (!empty($totalChargeableWeight)) ? $shipment->totalWeight + $shipmentTotalWeight : $shipmentTotalWeight;
                    $shipment->totalCost = $totalShipmentCost;
                    $shipment->maxStorageDate = max($storageDates);
                    $shipment->storageCharge = $shipmentStorageCharge;
                    $shipment->save();

                    /* Shipment Notifications */
                    Shipment::notificationEmail($shipmentId);
                }


                return redirect()->back()->with('successMessage', 'Delivery Created Successfully.');
            } else {

                return redirect()->back()->withErrors('Delivery Cannot be created with different tracking number. Please create separate delivery for each item with different tracking number.');
            }
        }
        // return redirect()->back()->with('successMessage', 'Delivery Created Successfully.');
    }

    public function createprepaidshipment($id) {
        if (empty($id))
            return 0;

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        /*  FETCH PROCUREMENT ITEM DETAILS */
        $procurementItemList = Procurementitem::getAutoPartsItemDetails($id);

        if (count($procurementItemList) > 1) {

            for ($i = 0; $i < count($procurementItemList); $i++) {

                if ($procurementItemList[$i]['trackingNumber'] == $procurementItemList[$i + 1]['trackingNumber']) {
                    $trackingFlag = 1;
                }


                break;
            }
        } else {
            $trackingFlag = 1;
        }

        if ($trackingFlag == 1) {

            $totalShipmentCost = $shipmentTotalItemCost = $shipmentTotalWeight = '0.00';
            /*  FETCH PROCUREMENT DETAILS */
            $procurementData = Procurement::getAutopartsDetails($id);
            $userInfo = \App\Model\User::find($procurementData->userId);



            $inventoryCharge = \App\Model\Generalsettings::where('settingsKey', 'first_delivery_charge')->first();

            /* SAVE PROCUREMENT DETAILS */
            $procurement = Procurement::find($id);
            /* $procurement->procurementLocked = 'Y';
              $procurement->status = 'completed';
              $procurement->save(); */

            /* INSERT DATA INTO MAIN SHIPMENT TABLE */
            $shipment = new Shipment;
            $shipment->userId = $procurementData->userId;
            $shipment->warehouseId = $procurementData->warehouseId;
            $shipment->shipmentType = 'autopart';
            $shipment->fromCountry = $procurementData->fromCountry;
            $shipment->fromState = $procurementData->fromState;
            $shipment->fromCity = $procurementData->fromCity;
            $shipment->fromName = $procurementData->fromName;
            $shipment->fromAddress = $procurementData->fromAddress;
            $shipment->fromZipCode = $procurementData->fromAddress;
            $shipment->fromPhone = $procurementData->fromPhone;
            $shipment->fromEmail = $procurementData->fromEmail;
            $shipment->toCountry = $procurementData->toCountry;
            $shipment->toState = $procurementData->toState;
            $shipment->toCity = $procurementData->toCity;
            $shipment->toName = $procurementData->toName;
            $shipment->toAddress = $procurementData->toAddress;
            $shipment->toZipCode = $procurementData->toZipCode;
            $shipment->toPhone = $procurementData->toPhone;
            $shipment->toEmail = $procurementData->toEmail;
            $shipment->shipmentStatus = '1';
            $shipment->firstReceived = Config::get('constants.CURRENTDATE');
            $shipment->totalWeight = $procurementData->totalWeight;
            $shipment->totalShippingCost = $procurementData->totalShippingCost;
            $shipment->totalClearingDuty = $procurementData->totalClearingDuty;
            $shipment->isDutyCharged = $procurementData->isDutyCharged;
            $shipment->totalInsurance = (($procurementData->totalInsurance == '') ? '0.00' : $procurementData->totalInsurance);
            $shipment->totalTax = (($procurementData->totalTax == '') ? '0.00' : $procurementData->totalTax);
            $shipment->totalDiscount = (($procurementData->totalDiscount == '') ? '0.00' : $procurementData->totalDiscount);
            $shipment->totalCost = $procurementData->totalCost;
            $shipment->prepaid = (!empty($procurementData->shippingMethodId) && $procurementData->paymentStatus == 'paid') ? 'Y' : 'N';
            $shipment->paymentStatus = (!empty($procurementData->shippingMethodId) && $procurementData->paymentStatus == 'paid') ? 'paid' : 'unpaid';
            $shipment->paymentMethodId = (!empty($procurementData->shippingMethodId) && $procurementData->paymentStatus == 'paid') ? $procurementData->paymentMethodId : '';
            $shipment->paymentReceivedOn = $procurementData->paymentReceivedOn;
            $shipment->isCurrencyChanged = $procurementData->isCurrencyChanged;
            $shipment->defaultCurrencyCode = $procurementData->defaultCurrencyCode;
            $shipment->paidCurrencyCode = $procurementData->paidCurrencyCode;
            $shipment->exchangeRate = $procurementData->exchangeRate;
            $shipment->createdBy = Auth::user()->id;
            $shipment->createdByType = 'admin';
            $shipment->createdOn = Config::get('constants.CURRENTDATE');
            $shipment->save();
            /* Shipment Notifications */
            Shipment::notificationEmail($shipmentId);


            $totalShipmentCost = ($procurementData->totalShippingCost + $procurementData->totalClearingDuty + $shipment->totalInsurance + $shipment->totalTax) - $shipment->totalDiscount;
            $shipmentId = $shipment->id;

            if (!empty($shipmentId)) {

                $inventoryChargeCost = !empty($inventoryCharge->settingsValue) ? $inventoryCharge->settingsValue : '0.00';

                $totalShipmentCost += $inventoryChargeCost;

                /* INSERT DATA INTO DELIVERY MAPPING TABLE */
                $shipmentDelivery = new Shipmentdelivery;
                $shipmentDelivery->shipmentId = $shipmentId;
                $shipmentDelivery->weight = !empty($procurementData->totalWeight) ? $procurementData->totalWeight : 0;
                $shipmentDelivery->chargeableWeight = !empty($procurementData->totalWeight) ? $procurementData->totalWeight : 0;
                $shipmentDelivery->totalItemCost = $procurementData->totalItemCost;
                $shipmentDelivery->shippingCost = $procurementData->totalShippingCost;
                $shipmentDelivery->clearingDutyCost = $procurementData->totalClearingDuty;
                $shipmentDelivery->isDutyCharged = $procurementData->isDutyCharged;
                //$shipmentDelivery->inventoryCharge = $inventoryChargeCost; //Updated on 29-11-2018
                $shipmentDelivery->tracking = $procurementItemList[0]->trackingNumber;
                $shipmentDelivery->tracking2 = (!empty($procurementItemList[0]->trackingNumber2) ? $procurementItemList[0]->trackingNumber2 : '');
                $shipmentDelivery->inventoryCharge = '0.00';
                $shipmentDelivery->otherChargeCost = '0.00';
                $shipmentDelivery->maxStorageDate = '0000-00-00';
                $shipmentDelivery->storageCharge = '0.00';
                $shipmentDelivery->totalCost = $totalShipmentCost;
                $shipmentDelivery->received = 'Y';
                $shipmentDelivery->shippingMethodId = $procurementData->shippingMethodId;
                $shipmentDelivery->createdBy = Auth::user()->id;
                $shipmentDelivery->createdByType = 'admin';
                $shipmentDelivery->createdOn = Config::get('constants.CURRENTDATE');
                $shipmentDelivery->save();
                $shipmentDeliveryId = $shipmentDelivery->id;

                /* INSERT DATA INTO SHIPMENT PACKAGE TABLE */
                if (!empty($shipmentDeliveryId)) {
                    foreach ($procurementItemList as $procurementItemData) {
                        if ($procurementItemData->status != 'unavailable') {
                            /*  FETCH PROCUREMENT ITEM DETAILS */
                            $procurementItemId = $procurementItemData->id;
                            $procurementItemData = Procurementitem::find($procurementItemId);
                            $procurementItemData->status = 'addedtodelivery';
                            $procurementItemData->save();

                            if (!empty($procurementItemData->siteProductId))
                                $siteProduct = \App\Model\Siteproduct::find($procurementItemData->siteProductId);

                            $shipmentPackage = new Shipmentpackage;
                            $shipmentPackage->shipmentId = $shipmentId;
                            $shipmentPackage->deliveryId = $shipmentDeliveryId;
                            $shipmentPackage->storeId = $procurementItemData->storeId;
                            $shipmentPackage->siteCategoryId = $procurementItemData->siteCategoryId;
                            $shipmentPackage->siteSubCategoryId = $procurementItemData->siteSubCategoryId;
                            $shipmentPackage->siteProductId = $procurementItemData->siteProductId;
                            $shipmentPackage->itemName = $procurementItemData->itemName;
                            $shipmentPackage->websiteUrl = $procurementItemData->websiteUrl;
                            $shipmentPackage->weight = !empty($procurementItemData->siteProductId) ? $siteProduct->weight : '0.00';
                            $shipmentPackage->options = $procurementItemData->options;
                            $shipmentPackage->itemDescription = $procurementItemData->itemDescription;
                            $shipmentPackage->itemMake = $procurementItemData->itemMake;
                            $shipmentPackage->itemModel = $procurementItemData->itemModel;
                            $shipmentPackage->itemQuantity = $procurementItemData->itemQuantity;
                            $shipmentPackage->itemPrice = $procurementItemData->itemPrice;
                            $shipmentPackage->itemMinPrice = $procurementItemData->itemPrice;
                            $shipmentPackage->itemPriceEdited = '1';
                            $shipmentPackage->itemShippingCost = $procurementItemData->itemShippingCost;
                            $shipmentPackage->itemTotalCost = $procurementItemData->itemTotalCost;
                            $shipmentPackage->deliveryCompanyId = $procurementItemData->deliveryCompanyId;
                            $shipmentPackage->deliveredOn = $procurementItemData->receivedDate;
                            $shipmentPackage->tracking = $procurementItemData->trackingNumber;
                            $shipmentPackage->tracking2 = (!empty($procurementItemData->trackingNumber2) ? $procurementItemData->trackingNumber2 : '');
                            $shipmentPackage->type = "I";
                            $shipmentPackage->deliveryNotes = $procurementItemData->deliveryNotes;
                            $shipmentPackage->save();
                            $shipmentPackage->createdBy = Auth::user()->id;
                            $shipmentPackage->createdOn = Config::get('constants.CURRENTDATE');
                            $shipmentTotalItemCost += $procurementItemData->itemTotalCost;
                            $shipmentTotalWeight += ($shipmentPackage->weight * $procurementItemData->itemQuantity);
                        }
                    }

                    $totalShipmentCost = round($totalShipmentCost, 2);
                    $shipment->totalCost = $totalShipmentCost;
                    $shipment->totalItemCost = round($shipmentTotalItemCost, 2);
                    $shipment->totalWeight = round($shipmentTotalWeight, 2);
                    $shipment->save();

                    $shipmentstauslog = new \App\Model\Shipmentstatuslog;
                    $shipmentstauslog->shipmentId = $shipmentId;
                    $shipmentstauslog->deliveryId = $shipmentDeliveryId;
                    $shipmentstauslog->oldStatus = 'none';
                    $shipmentstauslog->status = 'in_warehouse';
                    $shipmentstauslog->updatedOn = Config::get('constants.CURRENTDATE');
                    $shipmentstauslog->save();

                    /* Create order on when creating shipment */
                    $orderObj = new \App\Model\Order;
                    $orderObj->shipmentId = $shipmentId;
                    $orderObj->userId = $shipment->userId;
                    $orderObj->totalCost = $totalShipmentCost;
                    $orderObj->status = '2';
                    $orderObj->type = 'shipment';
                    $orderObj->createdBy = Auth::user()->id;
                    $orderObj->save();
                    $orderNumber = \App\Model\Order::generateOrderNumber($orderObj->id, $orderObj->userId);
                    $orderObj->orderNumber = $orderNumber;
                    $orderObj->save();

                    $procurementInvoice = \App\Model\Invoice::where('procurementId', $id)->where('type', 'autopart')->where('invoiceType', 'receipt')->where('deleted', '0')->where('extraCostCharged', 'N')->first();
                    $invoiceDetails = json_decode($procurementInvoice->invoiceParticulars, TRUE);
                    $previousInvoiceShipment = $invoiceDetails['shipment'];
                    $invoiceDetails['shipment'] = array();
                    $invoiceDetails['shipment'] = array(
                        'totalItemCost' => $previousInvoiceShipment['totalItemCost'],
                        'totalShippingCost' => $shipment->totalShippingCost,
                        'totalClearingDuty' => $shipment->totalClearingDuty,
                        'isDutyCharged' => $shipment->isDutyCharged,
                        'totalTax' => $previousInvoiceShipment['totalTax'],
                        'isInsuranceCharged' => $previousInvoiceShipment['isInsuranceCharged'],
                        'totalInsurance' => $previousInvoiceShipment['totalInsurance'],
                        'totalCost' => $totalShipmentCost,
                        'totalWeight' => $previousInvoiceShipment['totalWeight'],
                        'totalQuantity' => $previousInvoiceShipment['totalQuantity'],
                    );
                    $invoiceUniqueId = 'REC' . $userInfo->unit . '-' . $shipmentId . '-' . date('Ymd');
                    $newInvoice = $procurementInvoice->replicate();
                    $newInvoice->invoiceUniqueId = $invoiceUniqueId;
                    $newInvoice->procurementId = null;
                    $newInvoice->shipmentId = $shipmentId;
                    $newInvoice->invoiceParticulars = json_encode($invoiceDetails);
                    $newInvoice->totalBillingAmount = $totalShipmentCost;
                    $newInvoice->save();
                }

                return 1;
            } else
                return 0;
        } else
            return 0;
    }

    /**
     * Method used to generate and send customer invoice
     * @param integer $id
     * @return boolean
     */
    public function createinvoice($id, Request $request) {
        $packageDetails = array();

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (!empty($request->extraCost)) {
            /* UPDATE TOTAL ITEM COST WITH EXTRA COST */
            Procurement::where('id', $id)->increment('totalItemCost', $request->extraCost);
            Procurement::where('id', $id)->increment('totalCost', $request->extraCost);

            /*  FETCH PROCUREMENT DETAILS */
            $procurementData = Procurement::getAutopartsDetails($id);

            /*  FETCH PROCUREMENT ITEM DETAILS */
            $procurementItemList = Procurementitem::getAutoPartsItemDetails($id);


            if (!empty($procurementItemList)) {
                foreach ($procurementItemList as $procurementItemData) {
                    $packageDetails[] = array(
                        'id' => $procurementItemData->id,
                        'storeName' => $procurementItemData->storeName,
                        'categoryName' => $procurementItemData->categoryName,
                        'subcategoryName' => $procurementItemData->subcategoryName,
                        'productName' => $procurementItemData->productName,
                        'itemName' => $procurementItemData->itemName,
                        'websiteUrl' => $procurementItemData->websiteUrl,
                        'options' => $procurementItemData->options,
                        'description' => $procurementItemData->itemDescription,
                        'itemMake' => $procurementItemData->itemMake,
                        'itemModel' => $procurementItemData->itemModel,
                        'itemPrice' => $procurementItemData->itemPrice,
                        'itemQuantity' => $procurementItemData->itemQuantity,
                        'itemShippingCost' => $procurementItemData->itemShippingCost,
                        'itemTotalCost' => $procurementItemData->itemTotalCost,
                        'trackingNumber' => $procurementItemData->trackingNumber,
                    );
                }
            }

            /*  FETCH ADDRESS BOOK DETAILS */
            $addressBookData = \App\Model\Addressbook::where('userId', $procurementData->userId)->where('isDefaultBilling', '1')->where('deleted', '0')->with('country', 'state', 'city')->first();

            /*  PREPARE DATA FOR INVOICE PARTICULARS */
            $invoiceData = array(
                'shipment' => array(
                    'totalItemCost' => $procurementData->totalItemCost,
                    'totalProcessingFee' => $procurementData->totalProcessingFee,
                    'urgentPurchaseCost' => $procurementData->urgentPurchaseCost,
                    'totalProcurementCost' => $procurementData->totalProcurementCost,
                    'totalShippingCost' => $procurementData->totalShippingCost,
                    'totalClearingDuty' => $procurementData->totalClearingDuty,
                    'isDutyCharged' => $procurementData->isDutyCharged,
                    'totalInsurance' => $procurementData->totalInsurance,
                    'totalTax' => $procurementData->totalTax,
                    'totalCost' => $procurementData->totalCost,
                    'totalWeight' => $procurementData->totalWeight,
                    'totalQuantity' => $procurementData->itemQuantity,
                ),
                'warehouse' => array(
                    'fromAddress' => $procurementData->fromAddress,
                    'fromZipCode' => $procurementData->fromZipCode,
                    'fromCountry' => $procurementData->fromCountryName,
                    'fromState' => $procurementData->fromStateName,
                    'fromCity' => $procurementData->fromCityName,
                ),
                'shippingaddress' => array(
                    'toCountry' => $procurementData->toCountryName,
                    'toState' => $procurementData->toStateName,
                    'toCity' => $procurementData->toCityName,
                    'toAddress' => $procurementData->toAddress,
                    'toZipCode' => $procurementData->toZipCode,
                    'toName' => $procurementData->toName,
                    'toEmail' => $procurementData->toEmail,
                    'toPhone' => $procurementData->toPhone,
                ),
                'packages' => $packageDetails,
                'extracharge' => array(
                    'extraCost' => $request->extraCost,
                    'notes' => $request->notes,
                ),
            );

            /*  INSERT DATA INTO INVOICE TABLE */
            $invoiceUniqueId = 'INV-EXC-' . $procurementData->userUnit . '-' . $id . '-' . date('Ymd');
            $invoice = new \App\Model\Invoice;
            $invoice->invoiceUniqueId = $invoiceUniqueId;
            $invoice->procurementId = $id;
            $invoice->type = 'autopart';
            $invoice->extraCostCharged = 'Y';
            $invoice->extraCostAmount = $request->extraCost;
            $invoice->userUnit = $procurementData->userUnit;
            $invoice->userFullName = $procurementData->fromName;
            $invoice->userEmail = $procurementData->fromEmail;
            $invoice->userContactNumber = $procurementData->fromPhone;
            $invoice->billingName = $addressBookData->title . ' ' . $addressBookData->firstName . ' ' . $addressBookData->lastName;
            $invoice->billingEmail = $addressBookData->email;
            $invoice->billingAddress = $addressBookData->address;
            $invoice->billingAlternateAddress = $addressBookData->alternateAddress;
            $invoice->billingCity = isset($addressBookData->city) ? $addressBookData->city->name : '';
            $invoice->billingState = isset($addressBookData->state) ? $addressBookData->state->name : '';
            $invoice->billingCountry = $addressBookData->country->name;
            $invoice->billingZipcode = $addressBookData->zipcode;
            $invoice->billingPhone = $addressBookData->phone;
            $invoice->billingAlternatePhone = $addressBookData->alternatePhone;
            $invoice->invoiceParticulars = json_encode($invoiceData);
            $invoice->createdOn = Config::get('constants.CURRENTDATE');
            $invoice->save();

            $invoiceId = $invoice->id;

            if (!empty($invoiceId)) {
                $data['invoice'] = \App\Model\Invoice::find($invoiceId);
                $fileName = "Invoice__" . $invoiceUniqueId . ".pdf";
                PDF::loadView('Administrator.autoparts.extrainvoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
                $to = $procurementData->fromEmail;
                $frontendUrl = Config::get('constants.frontendUrl') . "payment/invoice/" . $invoiceId;
                $content = "Invoice for Extra Cost Charged for Shop for Me #" . $id . " is attached. Please find the cost page at <a href=" . $frontendUrl . ">" . $frontendUrl . "</a>";
                Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($invoiceUniqueId, $to, $fileName) {

                    $message->from('contact@shoptomydoor.com', 'Shoptomydoor');

                    $message->subject($invoiceUniqueId . " - Invoice Details");
                    $message->to($to);
                    $message->attach(public_path('exports/invoice/' . $fileName));
                });

                /*  UPDATE LOCK STATUS IN PROCUREMENT TABLE */
                $procurement = Procurement::find($id);
                $procurement->procurementLocked = 'Y';
                $procurement->save();

                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Method used to print invoice
     * @param integer $id
     * @return boolean
     */
    public function printinvoice($id, $invoiceId) {
        $data = array();

        $data['invoice'] = Invoice::find($invoiceId);

        //print_r($data['invoice']);
        $data['pageTitle'] = "Print Invoice";
        return view('Administrator.autoparts.printinvoice', $data);
    }

    /**
     * Method used to update invoice payment status
     * @param integer $id
     * @param string $status
     * @return boolean
     */
    public function updatepaymentstatus($id, $status) {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $invoice = Invoice::find($id);
        $invoice->paymentStatus = $status;
        if ($status == 'paid')
            $invoice->invoiceType = 'receipt';
        $invoice->save();

        return 1;
    }

    /**
     * Method used to update procurement payment status
     * @param integer $id
     * @param string $status
     * @return boolean
     */
    public function updateprocurementpayment($id, $status) {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $procurement = \App\Model\Procurement::find($id);
        $procurement->paymentStatus = $status;
        $procurement->paidByUserId = Auth::user()->id;
        $procurement->paidByUserType = 'admin';
        $procurement->paymentReceivedOn = Config::get('constants.CURRENTDATE');
        $procurement->save();

        
        // Retrieve the first task
        $invoiceDeatils = \App\Model\Invoice::where('procurementId', $id)
                ->where('extraCostCharged', 'N')
                ->where('deleted', '0')
                ->where('paymentMethodId', '>', '0')
                ->orderby('id', 'desc')
                ->first();

        $invoice = \App\Model\Invoice::find($invoiceDeatils->id);
        $invoice->paymentStatus = $status;
        $invoice->save();

        $paymentTransaction = \App\Model\Paymenttransaction::where('paidForId', $id)->update(['status' => $status]);
    }

    /**
     * This function is user to update tracking number of shipment and its lock status
     * @param type $procurementItem
     * @param Request $request
     * @return type
     */
    public function updatetracking($procurementItem, Request $request) {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if ($request->trackingNumber != '') {
            $updateArray = array();
            $updateArray['trackingNumber'] = $request->trackingNumber;
            if ($request->action == 'lock')
                $updateArray['trackingLock'] = '1';
            Procurementitem::where('id', $procurementItem)->update($updateArray);
            $procurementId = Procurementitem::find($procurementItem)->procurementId;
            return \Redirect::to('administrator/autoparts/view/' . $procurementId . '/1')->with('successMessage', 'Shipment updated successfully.');
        } else
            return \Redirect::to('administrator/autoparts/view/' . $procurementId . '/1')->with('errorMessage', 'Please enter tracking number');
    }

    /**
     * Method used to delete multiple records
     * @return type
     */
    public function deleteall(Request $request) {

        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Autoparts'), Auth::user()->id);
        if ($findRole['canDelete'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }


        $createrModifierId = Auth::user()->id;
        $checkedval = $request->checkedval;

        if (!empty($checkedval)) {
            $idArray = explode('^', $checkedval);

            foreach ($idArray as $id) {
                Procurement::deleteRecord($id, $createrModifierId);
            }
            return \Redirect::to('administrator/autoparts')->with('successMessage', 'Shipments deleted successfully.');
        } else {
            return \Redirect::to('administrator/autoparts/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    public function generateInvoice($id) {

        /*  FETCH PROCUREMENT DETAILS */
        $procurement = Procurement::getAutopartsDetails($id);

        /*  FETCH PROCUREMENT ITEM DETAILS */
        $procurementItemList = Procurementitem::getAutoPartsItemDetails($id);
        
        $packageDetails = array();

        if (!empty($procurementItemList)) {
            foreach ($procurementItemList as $procurementItemData) {
                $packageDetails[] = array(
                    'id' => $procurementItemData->id,
                    'makeName' => $procurementItemData->makeName,
                    'modelName' => $procurementItemData->modelName,
                    'itemName' => $procurementItemData->itemName,
                    'websiteUrl' => $procurementItemData->websiteUrl,
                    'options' => $procurementItemData->options,
                    'description' => $procurementItemData->itemDescription,
                    'itemMake' => $procurementItemData->itemMake,
                    'itemModel' => $procurementItemData->itemModel,
                    'itemPrice' => $procurementItemData->itemPrice,
                    'itemQuantity' => $procurementItemData->itemQuantity,
                    'itemShippingCost' => $procurementItemData->itemShippingCost,
                    'itemTotalCost' => $procurementItemData->itemTotalCost,
                    'trackingNumber' => $procurementItemData->trackingNumber,
                );
            }
        }
        
        $paymentMethod = \App\Model\Paymentmethod::select('paymentMethod')->find($procurement->paymentMethodId);

        /*  PREPARE DATA FOR INVOICE PARTICULARS */
        $invoiceData = array(
            'shipment' => array(
                'urgent' => $procurement->urgent,
                'totalItemCost' => $procurement->totalItemCost,
                'totalProcessingFee' => $procurement->totalProcessingFee,
                'urgentPurchaseCost' => $procurement->urgentPurchaseCost,
                'totalProcurementCost' => $procurement->totalProcurementCost,
                'totalTax' => $procurement->totalTax,
                'isInsuranceCharged' => !empty($procurement->totalInsurance) ? 'Y' : 'N',
                'totalInsurance' => $procurement->totalInsurance,
                'totalCost' => $procurement->totalCost,
                'totalWeight' => $procurement->totalWeight,
                'totalQuantity' => $procurement->totalQuantity,
            ),
            'warehouse' => array(
                'fromAddress' => $procurement->fromAddress,
                'fromZipCode' => $procurement->fromZipCode,
                'fromCountry' => $procurement->fromCountry,
                'fromState' => $procurement->fromState,
                'fromCity' => $procurement->fromCity,
            ),
            'shippingaddress' => array(
                'toCountry' => $procurement->toCountry,
                'toState' => $procurement->toState,
                'toCity' => $procurement->toCity,
                'toAddress' => $procurement->toAddress,
                'toAlternateAddress' => '',
                'toZipCode' => $procurement->toZipCode,
                'toName' => $procurement->toName,
                'toEmail' => $procurement->toEmail,
                'toPhone' => $procurement->toPhone,
            ),
            'packages' => $packageDetails,
            'payment' => array(
                'paymentMethodId' => $procurement->paymentMethodId,
                'paymentMethodName' => $paymentMethod->paymentMethod,
            ),
        );

        if (!empty($procurement->couponcodeApplied)) {
            $invoiceData['shipment']['couponCode'] = $procurement->couponcodeApplied;
            $invoiceData['shipment']['discountAmount'] = $procurement->totalDiscount;
        }

        if (!empty($procurement->shippingMethodId)) {
            $shippingMethod = \App\Model\Shippingmethods::select('shipping')->where('shippingid', $procurement->shippingMethodId)->first();

            $invoiceData['shippingcharges']['shippingMethod'] = $procurement->shippingMethod;
            $invoiceData['shippingcharges']['shippingid'] = $procurement->shippingMethodId;
            $invoiceData['shippingcharges']['shipping'] = $shippingMethod->shipping;
            $invoiceData['shippingcharges']['estimateDeliveryDate'] = $procurement->estimateDeliveryDate;
            $invoiceData['shippingcharges']['isDutyCharged'] = $procurement->isDutyCharged;
            $invoiceData['shippingcharges']['shippingCost'] = $procurement->totalShippingCost;
            $invoiceData['shippingcharges']['totalClearingDuty'] = $procurement->totalClearingDuty;
            $invoiceData['shippingcharges']['totalShippingCost'] = $procurement->totalShippingCost + $procurement->totalClearingDuty;
        }

        $userData = \App\Model\User::find($procurement->userId);
        $addressBookData = \App\Model\Addressbook::where('userId', $procurement->userId)->where('isDefaultBilling', '1')->where('deleted', '0')->with('country', 'state', 'city')->first();


        /*  INSERT DATA INTO INVOICE TABLE */
        $invoiceUniqueId = 'REC' . $userData->unit . '-' . $id . '-' . date('Ymd');
        $invoice = new \App\Model\Invoice;
        $invoice->invoiceUniqueId = $invoiceUniqueId;
        $invoice->procurementId = $id;
        $invoice->invoiceType = 'receipt';
        $invoice->type = 'shopforme';
        $invoice->userUnit = $userData->unit;
        $invoice->userFullName = $procurement->fromName;
        $invoice->userEmail = $procurement->fromEmail;
        $invoice->userContactNumber = $procurement->fromPhone;
        $invoice->billingName = $addressBookData->title . ' ' . $addressBookData->firstName . ' ' . $addressBookData->lastName;
        $invoice->billingEmail = $addressBookData->email;
        $invoice->billingAddress = $addressBookData->address;
        $invoice->billingAlternateAddress = $addressBookData->alternateAddress;
        $invoice->billingCity = isset($addressBookData->city) ? $addressBookData->city->name : '';
        $invoice->billingState = isset($addressBookData->state) ? $addressBookData->state->name : '';
        $invoice->billingCountry = $addressBookData->country->name;
        $invoice->billingZipcode = $addressBookData->zipcode;
        $invoice->billingPhone = $addressBookData->phone;
        $invoice->billingAlternatePhone = $addressBookData->alternatePhone;
        $invoice->totalBillingAmount = (!empty($procurement->totalCost) ? $procurement->totalCost : 0);
        $invoice->paymentMethodId = $procurement->paymentMethodId;
        $invoice->paymentStatus = 'paid';
        $invoice->invoiceParticulars = json_encode($invoiceData);
        $invoice->createdOn = Config::get('constants.CURRENTDATE');

        $invoice->save();
        $invoiceId = $invoice->id;

        if (!empty($invoiceId)) {
            $data['invoice'] = \App\Model\Invoice::find($invoiceId);
            $fileName = "Receipt_" . $invoice->invoiceUniqueId . ".pdf";

            PDF::loadView('Administrator.procurement.invoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
        }

        if ($invoiceId) {
            return 1;
        } else {
            return 0;
        }
    }

        ////////////////////////////////////////Received Payment offline//////////////////////////////////////////

    public function receivedPayment($shipmentId, $page = 0, Request $request) {
        $data = array();


        $data['pageTitle'] = "Payment Details";
        $data['id'] = $shipmentId;
        $data['page'] = $page;
        $data['paymentMethod'] = \App\Model\Paymentmethod::where("status", "1")->where("deleted", "0")->orderBy('status', 'desc')->get();
        /* FETCH SHIPPING METHOD LIST  */
        $data['shippingMethod'] = \App\Model\Shippingmethods::where('active', 'Y')->where('deleted', '0')->orderby('shipping', 'asc')->get();

        return view('Administrator.autoparts.receivedPayment', $data);
    }

    public function savePaymentDetails($id, $page = 0, Request $request) {

        $procurement = Procurement::getautoPartsList($id);

        /*  FETCH PROCUREMENT ITEM DETAILS */
        $procurementItems = Procurementitem::where('procurementId', $id)->where('deleted', '0')->get()->toArray();
        $paymentMethod = \App\Model\Paymentmethod::select('paymentMethod')->find($request->paymentMethodId);

        $packageDetails = array();
        if (!empty($procurementItems)) {
            foreach ($procurementItems as $key => $item) {
                /* SET DATA FOR INVOICE PARTICULARS */
                $packageDetails[$key] = array(
                    'id' => $item['id'],
                    'itemName' => $item['itemName'],
                    'websiteUrl' => $item['websiteUrl'],
                    'storeId' => $item['storeId'],
                    'siteCategoryId' => $item['siteCategoryId'],
                    'siteSubCategoryId' => $item['siteSubCategoryId'],
                    'siteProductId' => $item['siteProductId'],
                    'itemDescription' => $item['itemDescription'],
                    'itemImage' => (empty($item['itemImage']) ? "" : $item['itemImage']),
                    'itemPrice' => $item['itemPrice'],
                    'itemQuantity' => $item['itemQuantity'],
                    'itemShippingCost' => $item['itemShippingCost'],
                    'itemTotalCost' => $item['itemTotalCost'],
                    'options' => $item['options'],
                );
            }
        }

        /*  PREPARE DATA FOR INVOICE PARTICULARS */
        $invoiceData = array(
            'shipment' => array(
                'totalItemCost' => $procurement->totalItemCost,
                'totalProcessingFee' => $procurement->totalProcessingFee,
                'urgentPurchaseCost' => $procurement->urgentPurchaseCost,
                'totalProcurementCost' => $procurement->totalProcurementCost,
                'totalTax' => $procurement->totalTax,
                'isInsuranceCharged' => !empty($procurement->totalInsurance) ? 'Y' : 'N',
                'totalInsurance' => $procurement->totalInsurance,
                'totalCost' => $procurement->totalCost,
                'totalWeight' => $procurement->totalWeight,
                'totalQuantity' => $procurement->totalQuantity,
            ),
            'warehouse' => array(
                'fromAddress' => $procurement->fromAddress,
                'fromZipCode' => $procurement->fromZipCode,
                'fromCountry' => $procurement->fromCountry,
                'fromState' => $procurement->fromState,
                'fromCity' => $procurement->fromCity,
            ),
            'shippingaddress' => array(
                'toCountry' => $procurement->toCountry,
                'toState' => $procurement->toState,
                'toCity' => $procurement->toCity,
                'toAddress' => $procurement->toAddress,
                'toAlternateAddress' => '',
                'toZipCode' => $procurement->toZipCode,
                'toName' => $procurement->toName,
                'toEmail' => $procurement->toEmail,
                'toPhone' => $procurement->toPhone,
            ),
            'packages' => $packageDetails,
            'payment' => array(
                'paymentMethodId' => $request->paymentMethodId,
                'paymentMethodName' => $paymentMethod->paymentMethod,
            ),
        );

        if (!empty($procurement->couponcodeApplied)) {
            $invoiceData['shipment']['couponCode'] = $procurement->couponcodeApplied;
            $invoiceData['shipment']['discountAmount'] = $procurement->totalDiscount;
        }

        if (!empty($procurement->shippingMethodId)) {
            $shippingMethod = \App\Model\Shippingmethods::select('shipping')->where('shippingid', $procurement->shippingMethodId)->first();

            $invoiceData['shippingcharges']['shippingMethod'] = $procurement->shippingMethod;
            $invoiceData['shippingcharges']['shippingid'] = $procurement->shippingMethodId;
            $invoiceData['shippingcharges']['shipping'] = $shippingMethod->shipping;
            $invoiceData['shippingcharges']['estimateDeliveryDate'] = $procurement->estimateDeliveryDate;
            $invoiceData['shippingcharges']['isDutyCharged'] = $procurement->isDutyCharged;
            $invoiceData['shippingcharges']['shippingCost'] = $procurement->totalShippingCost;
            $invoiceData['shippingcharges']['totalClearingDuty'] = $procurement->totalClearingDuty;
            $invoiceData['shippingcharges']['totalShippingCost'] = $procurement->totalShippingCost + $procurement->totalClearingDuty;
        }

        $userData = User::find($procurement->userId);
        $addressBookData = \App\Model\Addressbook::where('userId', $procurement->userId)->where('isDefaultBilling', '1')->where('deleted', '0')->with('country', 'state', 'city')->first();


        /*  INSERT DATA INTO INVOICE TABLE */
        $invoiceUniqueId = 'REC' . $userData->unit . '-' . $id . '-' . date('Ymd');
        $invoice = new \App\Model\Invoice;
        $invoice->invoiceUniqueId = $invoiceUniqueId;
        $invoice->procurementId = $id;
        $invoice->invoiceType = 'receipt';
        $invoice->type = 'shopforme';
        $invoice->userUnit = $userData->unit;
        $invoice->userFullName = $procurement->fromName;
        $invoice->userEmail = $procurement->fromEmail;
        $invoice->userContactNumber = $procurement->fromPhone;
        $invoice->billingName = $addressBookData->title . ' ' . $addressBookData->firstName . ' ' . $addressBookData->lastName;
        $invoice->billingEmail = $addressBookData->email;
        $invoice->billingAddress = $addressBookData->address;
        $invoice->billingAlternateAddress = $addressBookData->alternateAddress;
        $invoice->billingCity = isset($addressBookData->city) ? $addressBookData->city->name : '';
        $invoice->billingState = isset($addressBookData->state) ? $addressBookData->state->name : '';
        $invoice->billingCountry = $addressBookData->country->name;
        $invoice->billingZipcode = $addressBookData->zipcode;
        $invoice->billingPhone = $addressBookData->phone;
        $invoice->billingAlternatePhone = $addressBookData->alternatePhone;
        $invoice->totalBillingAmount = (!empty($procurement->totalCost) ? $procurement->totalCost : 0);
        $invoice->paymentMethodId = $procurement->paymentMethodId;
        $invoice->paymentStatus = 'paid';
        $invoice->invoiceParticulars = json_encode($invoiceData);
        $invoice->createdOn = Config::get('constants.CURRENTDATE');

        $invoice->save();
        $invoiceId = $invoice->id;

        if (!empty($invoiceId)) {
            $data['invoice'] = \App\Model\Invoice::find($invoiceId);
            $fileName = "Receipt_" . $invoice->invoiceUniqueId . ".pdf";

            PDF::loadView('Administrator.autoparts.invoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
        

            $frontendUrl = Config::get('constants.frontendUrl') . "payment/invoice/" . $invoiceId;
            $content = "Receipt for Offline Payment for Shipment #" . $id . " is attached. Please find the cost page at <a href=" . $frontendUrl . ">" . $frontendUrl . "</a>";
            $to = $userData->email;

            Mail::send(['html' => 'mail'], ['content' => $content], function ($message) use($procurement, $to, $fileName) {
                $message->from('contact@shoptomydoor.com', 'Shoptomydoor');
                $message->subject('Shipment Receipt');
                $message->to($to);
                $message->attach(public_path('exports/invoice/' . $fileName));
            });
        }
        /*  UPDATE PAYMENT STATUS IN SHIPMENT TABLE */

        $procurement->paymentStatus = 'paid';
        $procurement->paymentMethodId = $request->paymentMethodId;
        $procurement->paymentReceivedOn = $request->paidOn;
        $procurement->totalCost = $request->paidAmount;
        $procurement->paidByUserType = 'admin';
        $procurement->paidByUserId = Auth::user()->id;
         //Newly added 09/09/2019
        $procurement->orderDate = Config::get('constants.CURRENTDATE');
        $procurement->save();

 

        //Insert record in Payment Transaction////

        $shipmentStatusData = new \App\Model\Paymenttransaction;

        $shipmentStatusData->paymentMethodId = $request->paymentMethodId;
        $shipmentStatusData->paidFor = 'shopforme';
        $shipmentStatusData->paidForId = $id;
        $shipmentStatusData->amountPaid = $request->paidAmount;
        $shipmentStatusData->transactionOn = $request->paidOn;
        $shipmentStatusData->status = 'paid';


        $save = $shipmentStatusData->save();

        if ($save) {

            return \Redirect::to('administrator/autoparts/view/' . $id . '/1')->with('successMessage', 'Shipment updated successfully.');
        } else {

            return \Redirect::to('administrator/autoparts/view/' . $id . '/1')->with('errorMessage', 'Please enter comments');
        }
    }

    ////////////////////////////////////////Received Payment offline//////////////////////////////////////////
    
    public function additem($page = '1') {
        
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.AutoParts'), Auth::user()->id);
        if ($findRole['canEdit'] == 0) {
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        
        /* FETCH STORE LIST  */
        $data['storeList'] = \App\Model\Stores::where('deleted', '0')->where('storeType','autopart')->where('status', '1')->orderby('storeName', 'asc')->get();

        /* FETCH CATEGORY LIST  */
        $data['categoryList'] = \App\Model\Sitecategory::getCategoryList('parent');
        
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        $data['title'] = "Administrative Panel :: Auto Parts";
        $data['contentTop'] = array('breadcrumbText' => 'Auto Parts', 'contentTitle' => 'Auto Parts', 'pageInfo' => 'This section allows you to manage the Auto Parts details');
        $data['pageTitle'] = "Auto Parts - By Phone";
        $data['page'] = $page;
        
        
        return view('Administrator.autoparts.additem', $data);
    }
    
    public function saveprocurementbyphone($page, Request $request) {
        
//        print_r($request->all());
        $fileData = $request->file('productImage');
//        print_r($fileData[1]);exit;
        $totalWeight= $totalProcurementCost = $totalItemCost = $totalUrgentPurchaseCost = "0.00";
        $totalQuantity = 0;
        $userData = \App\Model\User::where("unit",$request->userUnit)->first();
        
        /* Fetch User Addressbook Details */
        $userId = $userData->id;
        $addressBookData = \App\Model\User::find($userId)->addressbook()->where('isDefaultShipping', '1')->first();
        /* Fetch Warehouse Details */
        $warehouseData = \App\Model\Warehouse::find($request->warehouseId);
        
        $procurement = new Procurement;
        $procurement->userId = $userId;
        $procurement->warehouseId = $request->warehouseId;
        $procurement->fromCountry = $warehouseData->countryId;
        $procurement->fromState = $warehouseData->stateId;
        $procurement->fromCity = $warehouseData->cityId;
        $procurement->fromAddress = $warehouseData->address;
        $procurement->fromZipCode = $warehouseData->zipcode;
        $procurement->fromPhone = $userData->contactNumber;
        $procurement->fromName = $userData->firstName . " " . $userData->lastName;
        $procurement->fromEmail = $userData->email;
        $procurement->toCountry = $addressBookData->countryId;
        $procurement->toState = $addressBookData->stateId;
        $procurement->toCity = $addressBookData->cityId;
        $procurement->toAddress = $addressBookData->address;
        $procurement->toZipCode = $addressBookData->zipcode;
        $procurement->toPhone = $addressBookData->phone;
        $procurement->toName = $addressBookData->firstName . " " . $addressBookData->lastName;
        $procurement->toEmail = $addressBookData->email;
        $procurement->procurementType = 'autopart';
        $procurement->urgent = $request->urgentPurchase;
        $procurement->status = 'byphone';
        $procurement->paymentStatus = 'unpaid';
        $procurement->paymentReceivedOn = Config::get('constants.CURRENTDATE');
        $procurement->createdBy = $userId;
        $procurement->createdByType = 'admin';
        $procurement->createdOn = Config::get('constants.CURRENTDATE');
        $procurement->orderDate = Config::get('constants.CURRENTDATE');
        $procurement->save();
        $procurementId = $procurement->id;
        /* Insert Data into Main Procurement Table */

        if (!empty($request->procurement)) {
            for($item = 0; $item < count($request->procurement['itemName']) ; $item++) {
                if (!empty($request->procurement['siteProductId'][$item])) {
                    $siteProduct = \App\Model\Siteproduct::find($request->procurement['siteProductId'][$item]);
                    $totalWeight += ($siteProduct->weight * $request->procurement['quantity'][$item]);
                }
                $name = "";
                if(!empty($fileData[$item])) {
                    $image = $fileData[$item];
                    $name = time() . '_' . $image->getClientOriginalName();
                    $destinationPath = public_path("/uploads/procurement/autoparts/$userId");
                    if (!file_exists($destinationPath)) {
                        mkdir($destinationPath);
                        chmod($destinationPath, 0777);
                    }
                    $image->move($destinationPath, $name);
                }

                $totalQuantity += $request->procurement['quantity'][$item];
                $totalItemCost += ($request->procurement['itemPrice'][$item] * $request->procurement['quantity'][$item]);

                /* INSERT DATA INTO PROCUREMENT ITEM TABLE */
                $pocurementitem = new Procurementitem;
                $pocurementitem->procurementId = $procurementId;
                $pocurementitem->itemName = $request->procurement['itemName'][$item];
                $pocurementitem->websiteUrl = $request->procurement['websiteLink'][$item];
                $pocurementitem->storeId = (!empty($request->procurement['store'][$item]) ? $request->procurement['store'][$item] : 0);
                $pocurementitem->siteCategoryId = $request->procurement['siteCategoryId'][$item];
                $pocurementitem->siteSubCategoryId = $request->procurement['siteSubCategoryId'][$item];
                $pocurementitem->siteProductId = (!empty($request->procurement['siteProductId'][$item]) ? $request->procurement['siteProductId'][$item] : 0);
                $pocurementitem->itemDescription = $request->procurement['description'][$item];
                $pocurementitem->year = $request->procurement['year'][$item];
                $pocurementitem->itemMake = $request->procurement['make'][$item];
                $pocurementitem->itemModel = $request->procurement['model'][$item];
                $pocurementitem->itemPrice = $request->procurement['itemPrice'][$item];
                $pocurementitem->itemQuantity = $request->procurement['quantity'][$item];
                $pocurementitem->itemImage = $name;
                $pocurementitem->itemShippingCost = $request->procurement['costToWarehouse'][$item];
                $pocurementitem->itemTotalCost = round(($request->procurement['itemPrice'][$item] * $request->procurement['quantity'][$item]),2);
                $pocurementitem->save();

                $procurementItemId = $pocurementitem->id;

                /* INSERT DATA INTO STATUS LOG TABLE */
                $procurementitemstatus = new Procurementitemstatus;
                $procurementitemstatus->procurementId = $procurementId;
                $procurementitemstatus->procurementItemId = $procurementItemId;
                $procurementitemstatus->oldStatus = '';
                $procurementitemstatus->status = 'submitted';
                $procurementitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
                $procurementitemstatus->save();

            }
        }

        /* Fetch Shipping Charge Details */
        $totalProcessingFee = $urgetPurchaseCost = "0.00";
        $processingFeeDetails = \App\Model\Procurementfees::where("feeLeft","<=",$totalItemCost)->where("feeRight",">=",$totalItemCost)->where("warehouseId",$request->warehouseId)->where("deleted","0")->first();
        if(!empty($processingFeeDetails)) {
            if($processingFeeDetails->feeType == 'A') {
                $totalProcessingFee = $processingFeeDetails->value;
            }
            else if($processingFeeDetails->feeType == 'P') {
                $totalProcessingFee = round(($totalItemCost*($processingFeeDetails->value/100)),2);
            }
        }
        if($request->urgentPurchase == 'Y') {
            $urgetPurchaseCost = round(($totalItemCost * ($warehouseData->urgentPurchase/100)),2);
            if($urgetPurchaseCost < $warehouseData->urgentPurchaseCost)
            {
                $urgetPurchaseCost = $warehouseData->urgentPurchaseCost;
            }
        }
        
        $totalProcurementCost = $totalItemCost+$totalProcessingFee+$urgetPurchaseCost;
        $procurement = Procurement::find($procurementId);
        $procurement->totalWeight = $totalWeight;
        $procurement->totalQuantity = $totalQuantity;
        $procurement->shippingMethod = 'N';
        $procurement->totalItemCost = $totalItemCost;
        $procurement->totalCost = $totalProcurementCost;
        $procurement->totalProcessingFee = $totalProcessingFee;
        $procurement->procurementLocked = 'N';
        $procurement->urgentPurchaseCost = $urgetPurchaseCost;
        $procurement->totalProcurementCost = $totalProcurementCost;
        $procurement->save();
        
        return \Redirect::to('administrator/autoparts/')->with('successMessage', 'Auto parts by phone request submitted successfully.');
        
    }


}
