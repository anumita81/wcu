<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Procurement;
use App\Model\Procurementitem;
use App\Model\Procurementitemstatus;
use App\Model\Deliverycompany;
use App\Model\Dispatchcompany;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\Shipment;
use App\Model\Shipmentdelivery;
use App\Model\Shipmentpackage;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Hash;
use Log;
use Config;
use Excel;
use Mail;
use customhelper;
use PDF;

class OthervehicleController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    /**
     * Method to display othervehicle service
     */
    public function index(Route $route, Request $request) {
        $data = array();

        /* BUILD DEFAULT SEARCH ARRAY */
        $searchOtherVehicleArr = array(
            'idFrom' => '',
            'idTo' => '',
            'totalCostFrom' => '',
            'totalCostTo' => '',
            'toCountry' => '',
            'toState' => '',
            'toCity' => '',
            'deliveryCompanyId' => '',
            'status' => '',
            'paymentStatus' => '',
            'user' => '',
        );

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchByCreatedOn = \Input::get('searchByCreatedOn', '');
            $searchByDate = \Input::get('searchByDate', '');
            $searchOtherVehicle = \Input::get('searchOtherVehicle', $searchOtherVehicleArr);
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('OTHERVEHICLEDATA');
            \Session::push('OTHERVEHICLEDATA.searchByCreatedOn', $searchByCreatedOn);
            \Session::push('OTHERVEHICLEDATA.searchDisplay', $searchDisplay);
            \Session::push('OTHERVEHICLEDATA.searchByDate', $searchByDate);
            \Session::push('OTHERVEHICLEDATA.searchOtherVehicle', $searchOtherVehicle);
            \Session::push('OTHERVEHICLEDATA.field', $field);
            \Session::push('OTHERVEHICLEDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchByCreatedOn'] = $searchByCreatedOn;
            $param['searchByDate'] = $searchByDate;
            $param['searchOtherVehicle'] = $searchOtherVehicle;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('OTHERVEHICLEDATA.field');
            $sortType = \Session::get('OTHERVEHICLEDATA.type');
            $searchByCreatedOn = \Session::get('OTHERVEHICLEDATA.searchByCreatedOn');
            $searchByDate = \Session::get('OTHERVEHICLEDATA.searchByDate');
            $searchOtherVehicle = \Session::get('OTHERVEHICLEDATA.searchOtherVehicle');
            $searchDisplay = \Session::get('OTHERVEHICLEDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchOtherVehicle'] = !empty($searchOtherVehicle) ? $searchOtherVehicle[0] : $searchOtherVehicleArr;
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'id' => array('current' => 'sorting'),
            'firstName' => array('current' => 'sorting'),
            'totalCost' => array('current' => 'sorting'),
            'status' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH PPROCUREMENT LIST  */
        $procurementData = Procurement::getOtherVehicleList($param, '', 'P');

        /* FETCH DELIVERY COMPANY LIST  */
        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();
        /* FETCH DISPATCH COMPANY LIST  */
        $data['dispatchCompanyList'] = Dispatchcompany::where('status', '1')->orderby('name', 'asc')->get();
        /* FETCH COUNTRY LIST  */
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Other Vehicles";
        $data['contentTop'] = array('breadcrumbText' => 'Other Vehicles', 'contentTitle' => 'Procurement Services', 'pageInfo' => 'This section allows you to manage procurement services');
        $data['pageTitle'] = "Other Vehicles";
        $data['page'] = $procurementData->currentPage();
        $data['procurementData'] = $procurementData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        return view('Administrator.othervehicle.index', $data);
    }

    /**
     * Method for add edit shipment
     * @param integer $id
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Other Vehicles";
        $data['contentTop'] = array('breadcrumbText' => 'Other Vehicles', 'contentTitle' => 'Other Vehicles', 'pageInfo' => 'This section allows you to view procurement details');
        $data['page'] = !empty($page) ? $page : '1';
        $data['pageTitle'] = "Other Vehicles";

        /* FETCH DELIVERY COMPANY LIST  */
        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();

        /* FETCH MAKE LIST */
        $data['makeList'] = \App\Model\Automake::where('deleted', '0')->where('type', '2')->get()->toArray();

        if (!empty($id)) {
            $data['id'] = $id;

            /*  FETCH PROCUREMENT DETAILS */
            $data['procurement'] = Procurement::getOtherVehicleDetails($id);

            /*  FETCH PROCUREMENT ITEM DETAILS */
            $data['procurement']['items'] = Procurementitem::getOtherVehicleItemDetails($id);

            /* FETCH WAREHOUSE MESSAGES */
            $data['warehousemessages'] = \App\Model\Warehousemessage::where('status', '1')->get();

            /* FETCH WAREHOUSE NOTES */
            $data['warehouseNotes'] = \App\Model\Shipmentwarehousenotes::where('shipmentId', $id)->orderBy('id', 'desc')->limit(5)->get()->toArray();

            return view('Administrator.othervehicle.edit', $data);
        }
    }

    /**
     * Method used to change address
     * @param integer $id
     * @param integer $page
     * @param Request $request
     * @return string
     */
    public function changeaddress($id, $userid, Request $request) {
        if (\Request::isMethod('post')) {
            $addressBookId = $request->userAddressBookId;
            $procurement = Procurement::find($id);

            if (!empty($addressBookId)) { // IF EXISTING ADDRESS FROM ADDRESS BOOK
                /* FETCH USER ADDRESS BOOK DATA */
                $addressBookData = \App\Model\Addressbook::find($addressBookId);

                /* SAVE DATA TO PROCUREMENT TABLE */
                $procurement->toCountry = $addressBookData->countryId;
                $procurement->toState = $addressBookData->stateId;
                $procurement->toCity = $addressBookData->cityId;
                $procurement->toAddress = $addressBookData->address;
                $procurement->toPhone = $addressBookData->phone;
                $procurement->toName = $addressBookData->firstName . " " . $addressBookData->lastName;
                $procurement->toZipCode = $addressBookData->zipcode;
                $procurement->toEmail = $addressBookData->email;
                $procurement->save();
            } else { // IF NEW ADDRESS SELECTED

                /* SAVE DATA TO USER ADDRESS TABLE */
                $addressBook = new \App\Model\Addressbook;
                $addressBook->userId = $userid;
                $addressBook->title = $request->title;
                $addressBook->firstName = $request->firstName;
                $addressBook->lastName = $request->lastName;
                $addressBook->email = $request->email;
                $addressBook->address = $request->address;
                $addressBook->alternateAddress = $request->alternateAddress;
                $addressBook->cityId = $request->cityId;
                $addressBook->stateId = $request->stateId;
                $addressBook->countryId = $request->countryId;
                $addressBook->zipcode = $request->zipcode;
                $addressBook->phone = $request->phone;
                $addressBook->alternatePhone = $request->alternatePhone;
                $addressBook->modifiedBy = Auth::user()->id;
                $addressBook->modifiedOn = Config::get('constants.CURRENTDATE');
                $addressBook->save();

                /* SAVE DATA TO PROCUREMENT TABLE */
                $procurement->toCountry = $request->countryId;
                $procurement->toState = $request->stateId;
                $procurement->toCity = $request->cityId;
                $procurement->toAddress = $request->address;
                $procurement->toPhone = $request->phone;
                $procurement->toName = $request->firstName . " " . $request->lastName;
                $procurement->toEmail = $request->email;
                $procurement->toZipCode = $request->zipcode;
                $procurement->save();
            }

            return 1;
        }


        /* SET DATA FOR VIEW  */
        $data['pageTitle'] = "Change Delivery Address";
        $data['id'] = $id;
        $data['userId'] = $userid;
        $data['addressBookData'] = User::find($userid)->addressbook()->where('deleted', '0')->get(['id', 'firstName', 'lastName']);
        $data['countryList'] = Country::where('status', '1')->orderby('name', 'asc')->get();

        return view('Administrator.othervehicle.changeaddress', $data);
    }

    /**
     * Method used to fetch address details
     * @param integer $id
     * @return type
     */
    public function getaddressbookdetails($id = '') {
        $addressBook = \App\Model\Addressbook::find($id)->toArray();
        echo json_encode($addressBook);
    }

    /**
     * Method used to fetch updated address address
     * @param integer $id
     * @return string
     */
    public function getaddressdetails($id = '') {
        $data['id'] = $id;

        $shipment = Procurement::getShopForMeDetails($id);
        $data['shipment'] = $shipment;

        return view('Administrator.othervehicle.addressdetails', $data);
    }

    /**
     * Method used to display notification message
     * @param integer $messageId
     * @param integer $shipmentId
     * @return string
     */
    public function notifycustomer($messageId, $id, Request $request) {
        if (\Request::isMethod('post')) {

            $warehouseMessage = new \App\Model\Procurementwarehousemessage;
            $warehouseMessage->procurementId = $id;
            $warehouseMessage->messageId = $messageId;
            $warehouseMessage->sentBy = Auth::user()->id; //
            $warehouseMessage->sentOn = Config::get('constants.CURRENTDATE');

            $procurement = Procurement::find($id);
            $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'notification_admin')->first();
            $replace['[NAME]'] = $procurement->fromName;
            $replace['[NOTIFICATION]'] = $request->message;

            $to = $procurement->fromEmail;
            $sendMail = customhelper::SendMail($emailTemplate, $replace, $to);

            if ($sendMail) {
                if ($warehouseMessage->save()) {
                    return redirect()->back()->with('successMessage', 'Notification sent successfully');
                } else {
                    return redirect()->back()->with('errorMessage', 'Record not added');
                }
            } else {
                return redirect()->back()->with('errorMessage', 'Notification not sent');
            }
        }

        $data = array();

        $data['messageId'] = $messageId;
        $data['id'] = $id;
        $data['warehousemsg'] = \App\Model\Warehousemessage::select('message')->where('id', $messageId)->first();

        return view('Administrator.othervehicle.notifycustomer', $data);
    }

    public function addcomment($shipmentId, Request $request) {
        $data = array();

        $message = $request->comment;

        $shipmentnotes = new \App\Model\Shipmentwarehousenotes;

        $shipmentnotes->shipmentId = $shipmentId;
        $shipmentnotes->message = $message;
        $shipmentnotes->sentBy = Auth::user()->id;
        $shipmentnotes->sentOn = Config::get('constants.CURRENTDATE');

        if ($shipmentnotes->save()) {
            return 1;
        } else {
            return 0;
        }
    }

    public function sendcomment($id, $shipmentId) {
        $data = array();

        $data['shipmentId'] = $shipmentId;

        $data['customerEmailList'] = \App\Model\UserAdmin::select(array('id', 'email'))->where(array('deleted' => '0', 'status' => '1'))->get()->toArray();

        return view('Administrator.shipments.sendcomment', $data);
    }

    public function saveandnotify($shipmentId, Request $request) {

        $shipmentnotes = new \App\Model\Shipmentwarehousenotes;

        $emailto[] = $request->admin;

        $shipmentnotes->shipmentId = $shipmentId;
        $shipmentnotes->message = $request->warehousemsg;
        $shipmentnotes->sentBy = Auth::user()->id;
        $shipmentnotes->sentOn = Config::get('constants.CURRENTDATE');

        $emailTemplate = \App\Model\Emailtemplate::where('templateKey', 'notification_admin')->first();
        $replace['[NAME]'] = "Admin";
        $replace['[NOTIFICATION]'] = $request->warehousemsg;

        foreach ($emailto[0] as $email) {
            $sendMail = customhelper::SendMail($emailTemplate, $replace, $email);
        }
        if ($sendMail) {
            if ($shipmentnotes->save()) {

                return redirect()->back()->with('successMessage', 'Notes saved and sent successfully');
            } else {

                return redirect()->back()->with('errorMessage', 'Record not added');
            }
        } else {
            return redirect()->back()->with('errorMessage', 'Notes not sent');
        }
    }

    /**
     * Method used to get model list according to make Id
     * @param int $makeId
     * @return json
     */
    public function getmodellist($makeId) {
        $result = \App\Model\Automodel::where('makeId', $makeId)->where('deleted', '0')->where('makeType', '2')->get()->toArray();

        echo json_encode($result);
    }

    /**
     * Method to add new shipment package details
     * @param object $request
     * @return boolean
     */
    public function addothervehicleitem(Request $request) {
        if (\Request::isMethod('post')) {
            $procurementItemData = array();

            for ($i = 0; $i < count($request->data); $i++) {
                $name = $request->data[$i]['name'];

                if ($name == 'procurementId')
                    $procurementId = $request->data[$i]['value'];

                $procurementItemData[$name] = $request->data[$i]['value'];
            }

            /* SAVE PROCUREMENT DETAILS */
            Procurementitem::insert($procurementItemData);

            /* CALCULATE TOTAL ITEM COST */
            Procurement::calculateCost($procurementId);

            return 1;
        }
    }

    /**
     * Method to edit procurement item details
     * @param object $request
     * @return boolean
     */
    public function editothervehicleitem(Request $request) {
        if (\Request::isMethod('post')) {
            $procurementItemData = array();

            for ($i = 0; $i < count($request->data); $i++) {
                $name = $request->data[$i]['name'];

                if ($name == 'procurementItemId') {
                    $id = $request->data[$i]['value'];
                } else if ($name != 'automodelId') {
                    $procurementItemData[$name] = $request->data[$i]['value'];
                }
            }

            /* SAVE PROCUREMENT DETAILS */
            Procurementitem::where('id', $id)->update($procurementItemData);

            /* CALCULATE TOTAL ITEM COST */
            $procurementItem = new Procurementitem;
            $procurementItem = Procurementitem::find($id);
            Procurement::calculateCost($procurementItem->procurementId);

            return 1;
        }
    }

    /**
     * Method used to delete procurement item
     * @param integer $id
     * @return boolean
     */
    public function deleteprocurementitem($id) {
        if (!empty($id)) {
            $createrModifier = Auth::user()->email;
            if (Procurementitem::deleteRecord($id, $createrModifier)) {
                /* CALCULATE TOTAL ITEM COST */
                $procurementItem = new Procurementitem;
                $procurementItem = Procurementitem::find($id);
                Procurement::calculateCost($procurementItem->procurementId);

                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Method used to update procurement item status
     * @param integer $id
     * @return boolean
     */
    public function updateprocurementitemstatus(Request $request) {
        $procurementItemIds = $request->procurementItemIds;
        $status = $request->status;

        if (!empty($procurementItemIds)) {
            $procurementItemArr = explode('^', $procurementItemIds);

            foreach ($procurementItemArr as $id) {
                $procurementItem = new Procurementitem;
                $procurementItem = Procurementitem::find($id);

                /* INSERT DATA INTO STATUS LOG TABLE */
                $procurementitemstatus = new Procurementitemstatus;
                $procurementitemstatus->procurementId = $procurementItem->id;
                $procurementitemstatus->procurementItemId = $id;
                $procurementitemstatus->oldStatus = $procurementItem->status;
                $procurementitemstatus->status = $status;
                $procurementitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
                $procurementitemstatus->save();

                /* UPDATE PROCUREMENT ITEM TABLE */
                $procurementItem->status = $status;
                $procurementItem->save();
            }

            /* CHECK AND UPDATE PROCUREMENT STATUS */
            Procurement::updaterejectedstatus($procurementItem->id);

            return 1;
        }
        return 0;
    }

    /**
     * Method used to fetch & display package details
     * @param integer $id
     * @return string
     */
    public function getpackagedetails($id) {
        /* FETCH MAKE LIST */
        $data['makeList'] = \App\Model\Automake::where('deleted', '0')->where('type', '2')->get()->toArray();

        /*  FETCH PROCUREMENT ITEM DETAILS */
        $data['procurement']['items'] = Procurementitem::getOtherVehicleItemDetails($id);

        return view('Administrator.othervehicle.packagedetails', $data);
    }

    /**
     * Method used to display and update receive package info
     * @param integer $id
     * @return string
     */
    public function receivepackage($id, $status, Request $request) {
        if (\Request::isMethod('post')) {
            if (!empty($id)) {
                $procurementItemArr = explode('^', $id);
                $procurementItemData = array();

                foreach ($procurementItemArr as $itemId) {
                    $trackingNumber = 'trackingNumber' . $itemId;
                    $deliveryCompanyId = 'deliveryCompanyId' . $itemId;
                    $receivedDate = 'receivedDate' . $itemId;

                    /* SAVE PROCUREMENT DETAILS */
                    $procurementItemData['status'] = $status;
                    $procurementItemData['trackingNumber'] = $request->$trackingNumber;
                    $procurementItemData['deliveryCompanyId'] = $request->$deliveryCompanyId;
                    $procurementItemData['receivedDate'] = \Carbon\Carbon::parse($request->$receivedDate)->format('Y-m-d');
                    Procurementitem::where('id', $itemId)->update($procurementItemData);

                    /* INSERT DATA INTO STATUS LOG TABLE */
                    $procurementItem = Procurementitem::find($itemId);
                    $procurementitemstatus = new Procurementitemstatus;
                    $procurementitemstatus->procurementId = $procurementItem->id;
                    $procurementitemstatus->procurementItemId = $itemId;
                    $procurementitemstatus->oldStatus = $procurementItem->status;
                    $procurementitemstatus->status = $status;
                    $procurementitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
                    $procurementitemstatus->save();
                }
                return 1;
            } else {
                return 0;
            }
        }

        $data['id'] = $id;
        $data['status'] = $status;
        $data['pageTitle'] = "Receive Package in Warehouse";

        /* FETCH DELIVERY COMPANY LIST  */
        $data['deliveryCompanyList'] = Deliverycompany::where('status', '1')->orderby('name', 'asc')->get();

        return view('Administrator.othervehicle.receivepackage', $data);
    }

    /**
     * Method used to check and update procurement status
     * @return booelan
     */
    public function checkprocurementstatus($id) {
        $procurementUnavailableStatus = Procurementitem::where('status', 'unavailable')->where('deleted', '0')->where('procurementId', $id)->count('id');
        $procurementReceivedStatus = Procurementitem::where('status', 'received')->where('deleted', '0')->where('procurementId', $id)->count('id');

        $totalItems = Procurementitem::where('deleted', '0')->where('procurementId', $id)->count('id');

        if (!empty($procurementReceivedStatus) && ($totalItems == $procurementReceivedStatus + $procurementUnavailableStatus)) {
            $procurement = Procurement::find($id);
            if ($procurement->status == 'requestforcost' || $procurement->status == 'submitted') {
                Procurement::where('id', $id)->update(array('status' => 'itemreceived'));
            }

            /* FETCH PROCUREMENT DETAILS  */
            $procurement = Procurement::find($id);
            if ($procurement->procurementLocked == 'Y' && $procurement->status == 'itemreceived' && $procurement->paymentStatus == 'paid')
                return 0;
            else if ($procurement->procurementLocked == 'Y' && $procurement->status == 'completed' && $procurement->paymentStatus == 'paid')
                return 2;
            else
                return 1;
        } else {
            return 1;
        }
    }

    /**
     * Method used to fetch & display the shipment details
     * @param integer $id
     * @return string
     */
    public function getprocurementdetails($id) {
        /*  FETCH PROCUREMENT DETAILS */
        $data['procurement'] = Procurement::getShopForMeDetails($id);
        return view('Administrator.othervehicle.procurementdetails', $data);
    }

    /**
     * Method used to fetch & display the shipment details
     * @param integer $id
     * @return string
     */
    public function viewtrackingdetails($id) {
        $data['pageTitle'] = "View Tracking Details";
        /*  FETCH PROCUREMENT DETAILS */
        $data['procurement'] = Procurementitem::find($id);
        $data['deliverycompany'] = Procurementitem::find($id)->deliverycompany()->first();

        return view('Administrator.othervehicle.viewtrackingdetails', $data);
    }

    /**
     * Method to fetch state list
     * @return array
     */
    public function getstatelist($countryId = '') {
        $country = Country::find($countryId);
        $stateList = array();
        if (isset($country->code) && !empty($country->code)) {

            $stateList = State::where('status', '1')->where('deleted', '0')->where('countryCode', $country->code)->orderby('name', 'asc')->get();
        }
        echo json_encode($stateList);
        exit;
    }

    /**
     * Method to fetch city list
     * @return array
     */
    public function getcitylist($stateid = '') {
        $state = State::find($stateid);
        $cityList = array();
        if (isset($state->code) && !empty($state->code)) {
            $cityList = City::where('status', '1')
                    ->where('deleted', '0')
                    ->where('stateCode', $state->code)
                    ->orderby('name', 'asc')
                    ->get();
        }
        echo json_encode($cityList);
        exit;
    }

    /**
     * Method to fetch city list by country
     * @return array
     */
    public function getcitylistbycountry($countryid = '', $json = TRUE) {
        $country = Country::where('id', $countryid)->get(['code']);
        $countryCode = $country[0]['code'];
        $cityList = array();
        if (isset($countryCode) && !empty($countryCode)) {
            $cityList = City::where('status', '1')
                    ->where('deleted', '0')
                    ->where('countryCode', $countryCode)
                    ->orderby('name', 'asc')
                    ->get();
        }
        if ($json == TRUE) {
            echo json_encode($cityList);
            exit;
        } else {
            return $cityList;
        }
    }

    /**
     * Method used to fetch & display the shipment details
     * @param integer $id
     * @return string
     */
    public function getprocurementcostdetails($id) {

        /*  CHECK IF NEED TO INPUT WEIGHT (CATEGORY BLANK) */
        $categoryExist = Procurementitem::where('procurementId', $id)->where('deleted', '0')->where('siteProductId', '0')->count('id');

        $procurementItem = Procurementitem::calculateItemWeight($id);
        $procurementItemCount = Procurementitem::select(DB::raw("sum(itemQuantity) AS totalItems"))->where('procurementId', $id)->where('deleted', '0')->first();

        if (!empty($categoryExist)) {
            echo json_encode(array(
                'weightCalculated' => 'N',
                'totalWeight' => $procurementItem->totalWeight,
                'totalQuantity' => $procurementItem->totalQuantity,
                'totalItems' => $procurementItemCount->totalItems)
            );
            exit;
        } else {
            echo json_encode(array(
                'weightCalculated' => 'Y',
                'totalWeight' => $procurementItem->totalWeight,
                'totalQuantity' => $procurementItem->totalQuantity,
                'totalItems' => $procurementItemCount->totalItems)
            );
            exit;
        }
    }

    /**
     * Method used to diplay procurement weight details
     * @param integer $id
     * @param object $request
     * @return string
     */
    public function procurementweightdetails($id, Request $request) {
        $data['id'] = $id;
        $data['weightCalculated'] = $request->weightCalculated;
        $data['totalWeight'] = $request->totalWeight;
        $data['totalQuantity'] = $request->totalQuantity;
        $data['totalItems'] = $request->totalItems;
        $data['pageTitle'] = "Calculate Item Weight";

        return view('Administrator.othervehicle.procurementweightdetails', $data);
    }

    /**
     * Method used to save procurement weight details
     * @param integer $id
     * @param object $request
     * @return boolean
     */
    public function saveweightdetails($id, Request $request) {
        /*  SAVE PROCUREMENT WEIGHT DETAILS */
        $procurement = Procurement::find($id);
        $procurement->totalWeight = $request->totalWeight;
        $procurement->save();

        return 1;
    }

    /**
     * Method used to fetch procurement payment and charge details
     * @param integer $id
     * @param object $request
     * @return boolean
     */
    public function paymentdetails($id) {
        /*  FETCH PROCUREMENT DETAILS */
        $procurementData = Procurement::getShopForMeDetails($id);

        $param = array(
            'userId' => $procurementData->userId,
            'procurementId' => $procurementData->id,
            'fromCountry' => $procurementData->fromCountry,
            'fromState' => $procurementData->fromState,
            'fromCity' => $procurementData->fromCity,
            'toCountry' => $procurementData->toCountry,
            'toState' => $procurementData->toState,
            'toCity' => $procurementData->toCity,
            'totalWeight' => $procurementData->totalWeight,
            'totalProcurementCost' => $procurementData->totalProcurementCost,
            'totalQuantity' => $procurementData->itemQuantity,
        );

        /*  FETCH PROCUREMENT SHIPPING METHOD CHARGES DETAILS */
        $data['shippingMethodCharges'] = \App\Model\Shippingmethods::calculateShippingMethodCharges($param);

        $data['procurement'] = $procurementData;

        return view('Administrator.othervehicle.paymentdetails', $data);
    }

    /**
     * Method used to send customer invoice
     * @param integer $id
     * @return boolean
     */
    public function sendcustomerinvoice($id) {
        $packageDetails = array();

        /*  FETCH PROCUREMENT DETAILS */
        $procurementData = Procurement::getShopForMeDetails($id);

        /*  FETCH PROCUREMENT ITEM DETAILS */
        $procurementItemList = Procurementitem::getItemDetails($id);
        if (!empty($procurementItemList)) {
            foreach ($procurementItemList as $procurementItemData) {
                $packageDetails[] = array(
                    'id' => $procurementItemData->id,
                    'storeName' => $procurementItemData->storeName,
                    'categoryName' => $procurementItemData->categoryName,
                    'subcategoryName' => $procurementItemData->subcategoryName,
                    'productName' => $procurementItemData->productName,
                    'itemName' => $procurementItemData->itemName,
                    'websiteUrl' => $procurementItemData->websiteUrl,
                    'options' => $procurementItemData->options,
                    'itemPrice' => $procurementItemData->itemPrice,
                    'itemQuantity' => $procurementItemData->itemQuantity,
                    'itemShippingCost' => $procurementItemData->itemShippingCost,
                    'itemTotalCost' => $procurementItemData->itemTotalCost,
                    'trackingNumber' => $procurementItemData->trackingNumber,
                );
            }
        }

        /*  FETCH ADDRESS BOOK DETAILS */
        $addressBookData = \App\Model\Addressbook::where('userId', $procurementData->userId)->where('isDefaultBilling', '1')->where('deleted', '0')->with('country', 'state', 'city')->first();

        /*  FETCH PROCUREMENT SHIPPING METHOD CHARGES DETAILS */
        $param = array(
            'userId' => $procurementData->userId,
            'procurementId' => $procurementData->id,
            'fromCountry' => $procurementData->fromCountry,
            'fromState' => $procurementData->fromState,
            'fromCity' => $procurementData->fromCity,
            'toCountry' => $procurementData->toCountry,
            'toState' => $procurementData->toState,
            'toCity' => $procurementData->toCity,
            'totalWeight' => $procurementData->totalWeight,
            'totalProcurementCost' => $procurementData->totalProcurementCost,
            'totalQuantity' => $procurementData->itemQuantity,
        );
        $shippingMethodCharges = \App\Model\Shippingmethods::calculateShippingMethodCharges($param);


        /*  PREPARE DATA FOR INVOICE PARTICULARS */
        $invoiceData = array(
            'shipment' => array(
                'totalItemCost' => $procurementData->totalItemCost,
                'totalProcessingFee' => $procurementData->totalProcessingFee,
                'urgentPurchaseCost' => $procurementData->urgentPurchaseCost,
                'totalProcurementCost' => $procurementData->totalProcurementCost,
                'totalWeight' => $procurementData->totalWeight,
                'totalQuantity' => $procurementData->itemQuantity,
            ),
            'warehouse' => array(
                'fromAddress' => $procurementData->fromAddress,
                'fromZipCode' => $procurementData->fromZipCode,
                'fromCountry' => $procurementData->fromCountryName,
                'fromState' => $procurementData->fromStateName,
                'fromCity' => $procurementData->fromCityName,
            ),
            'shippingaddress' => array(
                'toCountry' => $procurementData->toCountryName,
                'toState' => $procurementData->toStateName,
                'toCity' => $procurementData->toCityName,
                'toAddress' => $procurementData->toAddress,
                'toZipCode' => $procurementData->toZipCode,
                'toName' => $procurementData->toName,
                'toEmail' => $procurementData->toEmail,
                'toPhone' => $procurementData->toPhone,
            ),
            'shippingcharges' => $shippingMethodCharges,
            'packages' => $packageDetails,
        );

        /*  INSERT DATA INTO INVOICE TABLE */
        $invoiceUniqueId = 'INV' . $procurementData->userUnit . '-' . $procurementData->id . '-' . date('Ymd');
        $invoice = new \App\Model\Invoice;
        $invoice->invoiceUniqueId = $invoiceUniqueId;
        $invoice->procurementId = $procurementData->id;
        $invoice->type = 'shopforme';
        $invoice->userUnit = $procurementData->userUnit;
        $invoice->userFullName = $procurementData->fromName;
        $invoice->userEmail = $procurementData->fromEmail;
        $invoice->userContactNumber = $procurementData->fromPhone;
        $invoice->billingName = $addressBookData->title . ' ' . $addressBookData->firstName . ' ' . $addressBookData->lastName;
        $invoice->billingEmail = $addressBookData->email;
        $invoice->billingAddress = $addressBookData->address;
        $invoice->billingAlternateAddress = $addressBookData->alternateAddress;
        $invoice->billingCity = isset($addressBookData->city) ? $addressBookData->city->name : '';
        $invoice->billingState = isset($addressBookData->state) ? $addressBookData->state->name : '';
        $invoice->billingCountry = $addressBookData->country->name;
        $invoice->billingZipcode = $addressBookData->zipcode;
        $invoice->billingPhone = $addressBookData->phone;
        $invoice->billingAlternatePhone = $addressBookData->alternatePhone;
        $invoice->invoiceParticulars = json_encode($invoiceData);
        $invoice->createdOn = Config::get('constants.CURRENTDATE');
        $invoice->save();

        $invoiceId = $invoice->id;

        if (!empty($invoiceId)) {
            $data['invoice'] = \App\Model\Invoice::find($invoiceId);
            $fileName = "Invoice_" . $invoiceUniqueId . ".pdf";
            PDF::loadView('Administrator.othervehicle.invoice', $data)->save(public_path('exports/invoice/' . $fileName))->stream('download.pdf');
            $to = $procurementData->fromEmail;
            Mail::send(['html' => 'mail'], ['content' => 'Invoice for Shipping Cost Request of Shop for Me #' . $id], function ($message) use($invoiceUniqueId, $to, $fileName) {
                $message->from('contact@shoptomydoor.com', 'Shoptomydoor');
                $message->subject("$invoiceUniqueId - Invoice Details");
                $message->to($to);
                $message->attach(public_path('exports/invoice/' . $fileName));
            });

            /*  UPDATE LOCK STATUS IN PROCUREMENT TABLE */
            $procurement = Procurement::find($id);
            $procurement->procurementLocked = 'Y';
            $procurement->save();

            return 1;
        } else
            return 0;
    }

    /**
     * Method used to Export All with selected fields
     * @param integer $page
     * @return type
     */
    public function createshipment($id) {
        /*  FETCH PROCUREMENT DETAILS */
        $procurementData = Procurement::getShopForMeDetails($id);

        /*  FETCH PROCUREMENT ITEM DETAILS */
        $procurementItemList = Procurementitem::getItemDetails($id);

        /* SAVE PROCUREMENT DETAILS */
        $procurement = Procurement::find($id);
        $procurement->procurementLocked = 'Y';
        $procurement->status = 'completed';
        $procurement->save();


        /* Insert Data into Main Shipment Table */
        $shipment = new Shipment;
        $shipment->userId = $procurementData->userId;
        $shipment->fromCountry = $procurementData->fromCountry;
        $shipment->fromState = $procurementData->fromState;
        $shipment->fromCity = $procurementData->fromCity;
        $shipment->fromAddress = $procurementData->fromAddress;
        $shipment->fromName = $procurementData->fromName;
        $shipment->fromEmail = $procurementData->fromEmail;
        $shipment->fromPhone = $procurementData->fromPhone;
        $shipment->toCountry = $procurementData->toCountry;
        $shipment->toState = $procurementData->toState;
        $shipment->toCity = $procurementData->toCity;
        $shipment->toAddress = $procurementData->toAddress;
        $shipment->toName = $procurementData->toName;
        $shipment->toEmail = $procurementData->toEmail;
        $shipment->toPhone = $procurementData->toPhone;
        $shipment->total = $procurementData->totalCost;
        $shipment->orderDate = $procurementData->orderDate;
        $shipment->warehouseId = $procurementData->warehouseId;
        $shipment->actualDate = Config::get('constants.CURRENTDATE');
        $shipment->receiptDate = $procurementData->paymentReceivedOn;
        $shipment->urgent = $procurementData->urgent;
        $shipment->urgentCost = $procurementData->urgentPurchaseCost;
        $shipment->storeShippingCost = $procurementData->totalShippingCost;
        $shipment->procurementTotal = $procurementData->totalProcurementCost;
        $shipment->paymentStatus = !empty($procurementData->shippingMethodId) ? 5 : 0;
        $shipment->createdBy = Auth::user()->id;
        $shipment->createdByType = 'admin';
        $shipment->createdOn = Config::get('constants.CURRENTDATE');
        $shipment->save();

        $shipmentId = $shipment->id;

        if (!empty($shipmentId)) {

            /* INSERT DATA INTO DELIVERY MAPPING TABLE */
            $shipmentDelivery = new Shipmentdelivery;
            $chargeableWeightFactor = \App\Model\Generalsettings::where('settingsKey', 'weight_factor')->first();
            $maxDisplayDeliveryId = ShipmentDelivery::getMaxDisplayDeliveryId($procurementData->userId);

            $shipmentDelivery->shipmentId = $shipmentId;
            $shipmentDelivery->displayDeliveryId = $maxDisplayDeliveryId + 1;
            $shipmentDelivery->chargeableWeight = $procurementData->totalWeight;
            $shipmentDelivery->createdBy = Auth::user()->id;
            $shipmentDelivery->createdByType = 'admin';
            $shipmentDelivery->createdOn = Config::get('constants.CURRENTDATE');
            $shipmentDelivery->save();
            $shipmentDeliveryId = $shipmentDelivery->id;

            /* INSERT DATA INTO SHIPMENT PACKAGE TABLE */
            if (!empty($shipmentDeliveryId)) {
                foreach ($procurementItemList as $procurementItemData) {
                    $received = $procurementItemData->status == 'received' ? 'Y' : 'N';

                    $shipmentPackage = new Shipmentpackage;
                    $shipmentPackage->shipmentId = $shipmentId;
                    $shipmentPackage->actualContent = $procurementItemData->itemName;
                    $shipmentPackage->type = "I";
                    $shipmentPackage->note = $procurementItemData->options;
                    $shipmentPackage->value = $procurementItemData->itemPrice;
                    $shipmentPackage->quantity = $procurementItemData->itemQuantity;
                    $shipmentPackage->deliveryId = $shipmentDeliveryId;
                    $shipmentPackage->received = $received;
                    $shipmentPackage->save();
                }
            }
            /* Insert Data into Shipment Packages Mapping Table */


            /* Update Total Amount into Shipment Mapping Table */
            $shipment = Shipment::find($shipmentId);
            if ($received == 'Y') {
                $maxStorageDate = \App\Model\Generalsettings::where('settingsKey', 'max_storage_days')->first();
                $chargePerUnit = \App\Model\Generalsettings::where('settingsKey', 'unit_weight_charge')->first();
                $shipment->firstReceived = Config::get('constants.CURRENTDATE');
                $shipment->maxStorageDate = date('Y-m-d', strtotime('+' . $maxStorageDate->settingsValue . ' days', Config::get('constants.CURRENTDATE')));
                $shipment->storageCharge = $chargeableWeight * $chargePerUnit->settingsValue;
            }
            $shipment->save();
        }
    }

    /**
     * Method used to Export All with selected fields
     * @param integer $page
     * @return type
     */
    public function exportall($page, Request $request) {
        // dd(session()->all());
        if (count($request->selectall) == 0) {
            return \Redirect::to('administrator/procurement/')->with('errorMessage', 'Select atleast one field during the time of export');
        }
        if (\Request::isMethod('post')) {

            $sortField = \Session::get('OTHERVEHICLEDATA.field');
            $sortType = \Session::get('OTHERVEHICLEDATA.type');
            $searchByDate = \Session::get('OTHERVEHICLEDATA.searchByDate');
            $searchByCreatedOn = \Session::get('OTHERVEHICLEDATA.searchByCreatedOn');
            $searchByShipmentId = \Session::get('OTHERVEHICLEDATA.searchByShipmentId');
            $searchByCustomer = \Session::get('OTHERVEHICLEDATA.searchByCustomer');
            $searchByDestination = \Session::get('OTHERVEHICLEDATA.searchByDestination');
            $searchByCost = \Session::get('OTHERVEHICLEDATA.searchByCost');
            $searchByPaymentStatus = \Session::get('OTHERVEHICLEDATA.searchByPaymentStatus');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByShipmentId'] = !empty($searchByShipmentId) ? $searchByShipmentId[0] : '';
            $param['searchByCustomer'] = !empty($searchByCustomer) ? $searchByCustomer[0] : '';
            $param['searchByDestination'] = !empty($searchByDestination) ? $searchByDestination[0] : '';
            $param['searchByCost'] = !empty($searchByCost) ? $searchByCost[0] : '';
            $param['searchByPaymentStatus'] = !empty($searchByPaymentStatus) ? $searchByPaymentStatus[0] : '';


            $recordList = Shipment::getShipmentList($param, 'export', 'P')->toArray();
            //print_r($request->selectall); die;
            $shipmentData = array();
            $headers = array();
            if (!empty($recordList)) {
                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {

                            if ($count == 0) {
                                if ($key == 'shipmentId')
                                    $cellValue = 'Shipment ID';
                                elseif ($key == 'customer')
                                    $cellValue = 'Customer';
                                elseif ($key == 'destinationCityName')
                                    $cellValue = 'Destination City Name';
                                elseif ($key == 'destinationStateName')
                                    $cellValue = 'Destination State Name';
                                elseif ($key == 'destinationCountryName')
                                    $cellValue = 'Destination Country Name';
                                elseif ($key == 'createdOn')
                                    $cellValue = 'Date';
                                elseif ($key == 'cost')
                                    $cellValue = 'Cost';
                                elseif ($key == 'paymentStatus')
                                    $cellValue = 'Payment Status';

                                $headers[$key] = $cellValue;
                            }
                        }
                    }
                }

                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {

                        if ($key == 'createdOn')
                            $value = \Carbon\Carbon::parse($value)->format('Y-m-d');

                        $shipmentData[$count + 1][$headers[$key]] = $value;
                    }
                    //print_r($shipmentData);
                }
            }
            // print_r($shipmentData);die;
            ob_end_clean();
            ob_start();
            Excel::create("Procurement-" . \Carbon\Carbon::now(), function($excel) use($shipmentData) {
                $excel->sheet('Sheet 1', function($sheet) use($shipmentData) {
                    $sheet->fromArray($shipmentData);
                });
            })->export('xlsx');
            ob_flush();


            return \Redirect::to('administrator/othervehicle/')->with('successMessage', 'Excel file created and downloaded');
        }
    }

    /**
     * Method used to Export selected with selected fields
     * @param integer $page
     * @return type
     */
    public function exportselected($page, Request $request) {
        /* if (count($request->selectall) == 0) {
          return \Redirect::to('administrator/users/')->with('errorMessage', 'Select atleast one field during the time of export');
          } */
        if (\Request::isMethod('post')) {

            $sortField = \Session::get('OTHERVEHICLEDATA.field');
            $sortType = \Session::get('OTHERVEHICLEDATA.type');
            $searchByDate = \Session::get('OTHERVEHICLEDATA.searchByDate');
            $searchByCreatedOn = \Session::get('OTHERVEHICLEDATA.searchByCreatedOn');
            $searchByShipmentId = \Session::get('OTHERVEHICLEDATA.searchByShipmentId');
            $searchByCustomer = \Session::get('OTHERVEHICLEDATA.searchByCustomer');
            $searchByDestination = \Session::get('OTHERVEHICLEDATA.searchByDestination');
            $searchByCost = \Session::get('OTHERVEHICLEDATA.searchByCost');
            $searchByPaymentStatus = \Session::get('OTHERVEHICLEDATA.searchByPaymentStatus');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'createdOn';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchByDate'] = !empty($searchByDate) ? $searchByDate[0] : '';
            $param['searchByCreatedOn'] = !empty($searchByCreatedOn) ? $searchByCreatedOn[0] : '';
            $param['searchByShipmentId'] = !empty($searchByShipmentId) ? $searchByShipmentId[0] : '';
            $param['searchByCustomer'] = !empty($searchByCustomer) ? $searchByCustomer[0] : '';
            $param['searchByDestination'] = !empty($searchByDestination) ? $searchByDestination[0] : '';
            $param['searchByCost'] = !empty($searchByCost) ? $searchByCost[0] : '';
            $param['searchByPaymentStatus'] = !empty($searchByPaymentStatus) ? $searchByPaymentStatus[0] : '';
            $param['searchByshipment'] = $request->selected;



            $recordList = Shipment::getShipmentList($param, 'export', 'P')->toArray();

            $shipmentData = array();
            $headers = array();
            if (!empty($recordList)) {
                foreach ($recordList as $count => $row) {
                    foreach ($row as $key => $value) {
                        if (in_array($key, $request->selectall)) {
                            if ($count == 0) {
                                if ($key == 'shipmentId')
                                    $cellValue = 'Shipment ID';
                                elseif ($key == 'customer')
                                    $cellValue = 'Customer';
                                elseif ($key == 'destinationCityName')
                                    $cellValue = 'Destination City Name';
                                elseif ($key == 'destinationStateName')
                                    $cellValue = 'Destination State Name';
                                elseif ($key == 'destinationCountryName')
                                    $cellValue = 'Destination Country Name';
                                elseif ($key == 'createdOn')
                                    $cellValue = 'Date';
                                elseif ($key == 'cost')
                                    $cellValue = 'Cost';
                                elseif ($key == 'paymentStatus')
                                    $cellValue = 'Payment Status';

                                $headers[$key] = $cellValue;
                            }
                        }
                    }
                }

                foreach ($recordList as $count => $row) {
                    print_r($row);
                    foreach ($row as $key => $value) {


                        if ($key == 'createdOn')
                            $value = \Carbon\Carbon::parse($value)->format('Y-m-d');

                        $shipmentData[$count + 1][$headers[$key]] = $value;
                    }
                }
            }

            $excelName = "Shipment-" . \Carbon\Carbon::now();
            $path = public_path('export/' . $excelName);

            ob_end_clean();
            ob_start();
            Excel::create($excelName, function($excel) use($shipmentData) {
                $excel->sheet('Sheet 1', function($sheet) use($shipmentData) {
                    $sheet->fromArray($shipmentData);
                });
            })->store('xlsx', public_path('exports'));
            ob_flush();
            $creatingPath = url('/') . "/public/exports/" . $excelName . ".xlsx";
            return response()->json(['path' => $creatingPath]);
        }
    }

    /**
     * Method used to clear search history
     * @return type
     */
    public function showall() {
        \Session::forget('OTHERVEHICLEDATA');
        return \Redirect::to('administrator/othervehicle');
    }

    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Procurement::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/othervehicle/?page=' . $page)->with('successMessage', 'Procurement deleted successfully.');
            } else {
                return \Redirect::to('administrator/othervehicle/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/othervehicle/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    /**
     * Method used to delete multiple records
     * @return type
     */
    public function deleteall(Request $request) {
        $createrModifierId = Auth::user()->id;
        $checkedval = $request->checkedval;

        if (!empty($checkedval)) {
            $idArray = explode('^', $checkedval);

            foreach ($idArray as $id) {
                Procurement::deleteRecord($id, $createrModifierId);
            }
            return \Redirect::to('administrator/othervehicle')->with('successMessage', 'Shipments deleted successfully.');
        } else {
            return \Redirect::to('administrator/othervehicle/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
