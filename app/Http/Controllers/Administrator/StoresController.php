<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Stores;
use App\Model\Warehouse;
use App\Model\Sitecategory;
use App\Model\Storecountrymapping;
use App\Model\Country;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class StoresController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Managestores'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        
        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);
            $searchStore = \Input::get('searchStore', '');
            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');
            $searchCountry = \Input::get('searchCountry', '');
            $searchCategory = \Input::get('searchCategory', '');


            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('STOREDATA');
            \Session::push('STOREDATA.searchDisplay', $searchDisplay);
            \Session::push('STOREDATA.searchStore', $searchStore);
            \Session::push('STOREDATA.searchCountry', $searchCountry);
            \Session::push('STOREDATA.searchCategory', $searchCategory);
            \Session::push('STOREDATA.field', $field);
            \Session::push('STOREDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
            $param['searchStore'] = $searchStore;
            $param['searchCountry'] = $searchCountry;
            $param['searchCategory'] = $searchCategory;
        } else {
            $sortField = \Session::get('STOREDATA.field');
            $sortType = \Session::get('STOREDATA.type');
            $searchDisplay = \Session::get('STOREDATA.searchDisplay');
            $searchStore = \Session::get('STOREDATA.searchStore');
            $searchCountry = \Session::get('STOREDATA.searchCountry');
            $searchCategory = \Session::get('STOREDATA.searchCategory');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'storeName';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
            $param['searchCountry'] = !empty($searchCountry) ? $searchCountry[0] : '';
            $param['searchCategory'] = !empty($searchCategory) ? $searchCategory[0] : '';
            $param['searchStore'] = !empty($searchCategory) ? $searchStore[0] : '';
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'storeName' => array('current' => 'sorting'), 'storeURL' => array('current' => 'sorting'), 'storeType' => array('current' => 'sorting')
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        $storeData = Stores::getStoreList($param);

        /* Fetch Data For Procurement Iems & Shipment Packages */
        $data['storeIdExists'] = array();
        $procurementStoreId = \App\Model\Procurementitem::fetchUsedFieldIds('storeId');
        $shipmentStoreId = \App\Model\Shipmentpackage::fetchUsedFieldIds('storeId');
        $data['storeIdExists'] = array_unique(array_merge($procurementStoreId, $shipmentStoreId));

        /* SET DATA FOR VIEW  */
        $data['countryList'] = Warehouse::countryWarehoseData()->toArray();
        if($param['searchStore'] == 'shopforme')
        {
            $data['categoryList'] = Sitecategory::select('id', 'categoryName')->where('parentCategoryId', '-1')->where('type', 'shopforme')->get()->toArray();
        }else if($param['searchStore'] == 'autopart')
        {
            $data['categoryList'] = Sitecategory::select('id', 'categoryName')->where('parentCategoryId', '-1')->where('type', 'autoparts')->get()->toArray();
        }else{
            $data['categoryList'] = Sitecategory::select('id', 'categoryName')->where('parentCategoryId', '-1')->get()->toArray();
        }
        
        $data['title'] = "Administrative Panel :: Stores";
        $data['contentTop'] = array('breadcrumbText' => 'Stores', 'contentTitle' => 'Stores', 'pageInfo' => 'This section allows you to manage stores for product delivery');
        $data['pageTitle'] = "Manage Stores";
        $data['page'] = $storeData->currentPage();
        $data['storeData'] = $storeData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.stores.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Stores";
        $data['contentTop'] = array('breadcrumbText' => 'Stores', 'contentTitle' => 'Stores', 'pageInfo' => 'This section allows you to manage stores for product delivery');
        $data['page'] = !empty($page) ? $page : '1';

        /* FETCH COUNTRY LIST  OF PRESENT WAREHOUSES */

        $data['countryList'] = Warehouse::countryWarehoseData()->toArray();

        

        //print_r($data['categoryList']); die;

        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $stores = Stores::find($id);
            $data['stores'] = $stores;
            $data['countryId'] = Storecountrymapping::where('storeId', $id)->pluck('countryId')->toArray();

            $data['categoryId'] = Storecountrymapping::where('storeId', $id)->pluck('categoryId')->toArray();

            if($data['stores']['storeType'] == 'shopforme')
            {
                $data['categoryList'] = Sitecategory::select('id', 'categoryName')->where('parentCategoryId', '-1')->where('type', 'shopforme')->get()->toArray();
            }else if($data['stores']['storeType'] == 'autopart')
            {
                $data['categoryList'] = Sitecategory::select('id', 'categoryName')->where('parentCategoryId', '-1')->where('type', 'autoparts')->get()->toArray();
            }

        } else {
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['countryId'] = array();
            $data['categoryId'] = array();
            $data['categoryList'] = Sitecategory::select('id', 'categoryName')->where('parentCategoryId', '-1')->get()->toArray();
        }
        return view('Administrator.stores.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = $record = array();
        //print_r($request->all()); die;

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $stores = new Stores;

        $validator = Validator::make($request->all(), [
                    'storeName' => 'required',
                    'storeCategories' => 'required',
                    'storeCountries' => 'required',
                    'storeURL' => 'required',
                    'shopDirect' => 'required',
                    'storeIcon' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if ($request->hasFile('storeIcon')) {
                $image = $request->file('storeIcon');
                if (substr($image->getMimeType(), 0, 5) == 'image') {
                    $info = pathinfo($image->getClientOriginalName());
                    if ($info['extension'] == 'bmp' || $info['extension'] == 'svg') {
                        return redirect('administrator/managestores')->with('errorMessage', 'Only jpeg, jpg, gif and png formats supported');
                    } else {
                        //$name = time().'.'.$image->getClientOriginalExtension();
                        $name = time() . '_' . str_replace(' ','_',$image->getClientOriginalName());
                        $destinationPath = public_path('/uploads/stores');

                        

                        $image->move($destinationPath, $name);

                        //customhelper::bannerCrop('/uploads/stores/', '/uploads/stores/', $name, "120", "60");
                        $filename = $destinationPath . '/' . $name;
                        

                        $extension = pathinfo($image->getClientOriginalName(), PATHINFO_EXTENSION);


                        switch ($extension) {
                            case 'jpg':
                            case 'jpeg':
                                $image = imagecreatefromjpeg($filename);
                                break;
                            case 'gif':
                                $image = imagecreatefromgif($filename);
                                break;
                            case 'png':
                                $image = imagecreatefrompng($filename);
                                break;
                        }
                        imagesavealpha($image, true);

                        //Check if GD extension is loaded
                        if (!extension_loaded('gd') && !extension_loaded('gd2')) {
                            trigger_error("GD is not loaded", E_USER_WARNING);
                            return false;
                        }
                        //$image = imagecreatefrompng($filename);
                        list($width, $height) = getimagesize($filename);
                        $newImg = imagecreatetruecolor(120, 60);
                        imagealphablending($newImg, false);
                        imagesavealpha($newImg, true);
                        $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
                        imagefilledrectangle($newImg, 0, 0, 120, 60, $transparent);
                        imagecopyresampled($newImg, $image, 0, 0, 0, 0, 120, 60, $width, $height);
                        $newImageName = '1_' . time() . '.png';
                        imagepng($newImg, $destinationPath . '/' . $newImageName);
                        unlink($filename);

                        $stores->storeIcon = $newImageName;
                    }
                } else {
                    return redirect('administrator/managestores')->with('errorMessage', 'Only jpeg, jpg, gif and png formats supported');
                }
            }
            if (!empty($id)) {
                $stores = Stores::find($id);
                $stores->updatedBy = Auth::user()->id;
                $stores->updatedOn = Config::get('constants.CURRENTDATE');
                if ($request->hasFile('storeIcon')) {
                    $image = $request->file('storeIcon');
                    // $name = time() . '_' . $image->getClientOriginalName();
                    // customhelper::bannerCrop('/uploads/stores/', '/uploads/stores/', $name, "120", "60");
                    // $filename = $destinationPath . '/' . $name;

                    if ($image->getClientOriginalName() != '') {
                        $stores->storeIcon = $newImageName;
                    }
                }
            } else {
                $stores->createdBy = Auth::user()->id;
                $stores->createdOn = Config::get('constants.CURRENTDATE');
            }
            //echo $name; die;
            $stores->storeName = $request->storeName;
            $stores->storeType = $request->storeType;
            $stores->storeURL = $request->storeURL;
            $stores->shopDirect = $request->shopDirect;
            $stores->shortDescription = $request->shortDescription;
            $stores->description = $request->description;

            $stores->save();
            $storesId = $stores->id;

            if (!empty($id)) {
                $record = Storecountrymapping::where('storeId', $id)->delete();
            }

            for ($j = 0; $j < count($request->storeCountries); $j++) {

                $warehouseOfCountries = Warehouse::select('id')->where('countryId',$request->storeCountries[$j])->get();
                foreach($warehouseOfCountries as $eachWarehouse)
                {
                    for ($i = 0; $i < count($request->storeCategories); $i++) {

                        $mapping = new Storecountrymapping;
                        $mapping->originalCountryId = $request->storeCountries[$j];
                        $mapping->countryId = $eachWarehouse->id;
                        $mapping->storeId = $storesId;

                        $mapping->categoryId = $request->storeCategories[$i];

                        $mapping->save();
                    }
                }
            }




            return redirect('administrator/managestores')->with('successMessage', 'Stores saved successfuly.');
        }
    }

    public function deletedata($id = '', $page = '') {

        $stores = new Stores();

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {

            $procurementStoreId = \App\Model\Procurementitem::fetchUsedFieldIds('storeId');
            $shipmentStoreId = \App\Model\Shipmentpackage::fetchUsedFieldIds('storeId');
            $storeIdExists = array_unique(array_merge($procurementStoreId, $shipmentStoreId));

            if (!in_array($id, $storeIdExists)) {
                $stores = $stores->find($id);

                if (Stores::deleteRecord($id, $createrModifierId)) {
                    return \Redirect::to('administrator/managestores/?page=' . $page)->with('successMessage', 'Stores deleted successfuly.');
                } else {
                    return \Redirect::to('administrator/managestores/?page=' . $page)->with('errorMessage', 'Error in operation!');
                }
            } else {
                return \Redirect::to('administrator/managestores/?page=' . $page)->with('errorMessage', 'Store cannot be deleted. Dependency exists.');
            }
        } else {
            return \Redirect::to('administrator/managestores/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    public function getcategory(Request $request)
    {
        $storeType = $request->storeType;

        if($storeType == 'autopart')
        {
           $resultset = Sitecategory::select('id', 'categoryName')->where('parentCategoryId', '-1')->where('type', 'autoparts')->get()->toArray(); 
        }
        else{
        $resultset = Sitecategory::select('id', 'categoryName')->where('parentCategoryId', '-1')->where('type', $storeType)->get()->toArray();
        }

        return json_encode($resultset);
    }

}
