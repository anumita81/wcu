<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Testimonial;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class TestimonialController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Testimonial'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('TESTIMONIALDATA');
            \Session::push('TESTIMONIALDATA.searchDisplay', $searchDisplay);
            \Session::push('TESTIMONIALDATA.field', $field);
            \Session::push('TESTIMONIALDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('TESTIMONIALDATA.field');
            $sortType = \Session::get('TESTIMONIALDATA.type');
            $searchDisplay = \Session::get('TESTIMONIALDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'desc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'text' => array('current' => 'sorting'),
            'author' => array('current' => 'sorting'),
            'email' => array('current' => 'sorting'),
            'createdOn' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTRY LIST  */
        $testimonialData = Testimonial::getTestimonialList($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Testimonials";
        $data['contentTop'] = array('breadcrumbText' => 'Testimonials', 'contentTitle' => 'Testimonials', 'pageInfo' => 'This section allows you to manage testimonial');
        $data['pageTitle'] = "Testimonials";
        $data['page'] = $testimonialData->currentPage();
        $data['testimonialData'] = $testimonialData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        
        return view('Administrator.testimonial.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Testimonials";
        $data['contentTop'] = array('breadcrumbText' => 'Testimonials', 'contentTitle' => 'Testimonials', 'pageInfo' => 'This section allows you to manage testimonial');
        $data['page'] = !empty($page) ? $page : '1';
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $testimonial = Testimonial::find($id);
            $data['testimonial'] = $testimonial;
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add';
        }
        return view('Administrator.testimonial.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $testimonial = new Testimonial;

        $validator = Validator::make($request->all(), [
                    'testimonial' => 'required',
                    'author' => 'required',
                    'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $testimonial = Testimonial::find($id);
                $testimonial->updatedBy = Auth::user()->id;
                $testimonial->updatedOn = Config::get('constants.CURRENTDATE');
            } else {
                $testimonial->createdBy = Auth::user()->id;
                $testimonial->createdOn = Config::get('constants.CURRENTDATE');
            }
            $testimonial->text = $request->testimonial;
            $testimonial->author = $request->author;
            $testimonial->email = $request->email;
            $testimonial->save();
            $testimonial = $testimonial->id;

            return redirect('/administrator/testimonial?page=' . $page)->with('successMessage', 'Testimonial saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Testimonial::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/testimonial/?page=' . $page)->with('successMessage', 'Testimonial status changed successfully.');
            } else {
                return \Redirect::to('administrator/testimonial/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/testimonial/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    public function deletedata($id = '', $page = '') {

        $testimonial = new Testimonial();

        $createrModifierId = Auth::user()->id;


        if (!empty($id)) {
            $testimonial = $testimonial->find($id);

            if (Testimonial::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/testimonial/?page=' . $page)->with('successMessage', 'Testimonial deleted successfuly.');
            } else {
                return \Redirect::to('administrator/testimonial/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/testimonial/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
