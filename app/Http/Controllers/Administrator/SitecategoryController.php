<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Sitecategory;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use customhelper;
use Config;

class SitecategoryController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Request $request) {

        $data = $param = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Settingscategories'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $sitecategoryObj = new Sitecategory();

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */

            $sortField = \Input::get('field', 'id');
            $sortOrder = \Input::get('type', 'desc');
            $perpage = \Input::get('searchDisplay', $this->_perPage);
            $searchData = \Input::get('searchData', "");
            $searchCategory = \Input::get('searchCategory', $this->_perPage);
            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('RECORD');
            \Session::push('RECORD.searchData', $searchData);
            \Session::push('RECORD.searchDisplay', $perpage);
            \Session::push('RECORD.field', $sortField);
            \Session::push('RECORD.type', $sortOrder);
            \Session::push('RECORD.searchCategory', $searchCategory);

            $param['searchCategory'] = $searchCategory;
        } else {
            $sortField = \Session::get('RECORD.field');
            $sortType = \Session::get('RECORD.type');
            $perpage = \Session::get('RECORD.searchDisplay');
            $searchData = \Session::get('RECORD.searchData');
            $searchCategory = \Session::get('STOREDATA.searchCategory');


            $sortField = !empty($sortField) ? $sortField[0] : 'id';
            $sortOrder = !empty($sortType) ? $sortType[0] : 'desc';
            $perpage = !empty($perpage) ? $perpage[0] : $this->_perPage;
            $searchData = !empty($searchData) ? $searchData[0] : "";
            $param['searchCategory'] = !empty($searchCategory) ? $searchCategory[0] : '';
        }

        $param['sortField'] = $sortField;
        $param['sortOrder'] = $sortOrder;
        $param['searchDisplay'] = $perpage;
        $param['searchData'] = $searchData;

        /* Fetch Data For Procurement Iems */
        $data['categoryIdExists'] = array();
        $procurementCatgeoryId = \App\Model\Procurementitem::fetchUsedFieldIds('siteCategoryId');
        $shipmentCatgeoryId = \App\Model\Shipmentpackage::fetchUsedFieldIds('siteCategoryId');
        $categoryIdExists = array_unique(array_merge($procurementCatgeoryId, $shipmentCatgeoryId));

        $procurementSubCatgeoryId = \App\Model\Procurementitem::fetchUsedFieldIds('siteSubCategoryId');
        $shipmentSubCatgeoryId = \App\Model\Shipmentpackage::fetchUsedFieldIds('siteSubCategoryId');
        $subCategoryIdExists = array_unique(array_merge($procurementSubCatgeoryId, $shipmentSubCatgeoryId));

        $data['categoryIdExists'] = array_unique(array_merge($categoryIdExists, $subCategoryIdExists));
        /* Fetch Data For Procurement Iems */


        $data['param'] = $param;
        $records = $sitecategoryObj->getData($param);
        $data['records'] = $records;
        $data['page'] = $records->currentPage();
        $data['rootCategory'] = $sitecategoryObj->getCategoryList();
        $data['categoryList'] = Sitecategory::select('id', 'categoryName')->where('parentCategoryId', '-1')->get()->toArray(); 
        $data['pageTitle'] = 'Site Categories';
        $data['title'] = "Site Categories :: ADMIN - Shoptomydoor";
        $data['contentTop'] = array('breadcrumbText' => array('Shipping Settings', 'Site Categories'), 'contentTitle' => 'Site Categories', 'pageInfo' => 'This section allows you to change site categories and subcategories');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.sitecategory.index', $data);
    }

    public function addeditcategory($section = 'root-category', $id = -1, $page) {
        $sitecategoryObj = new Sitecategory();

        if ($id != -1) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $record = $sitecategoryObj->find($id);
            $data['record'] = $record;
            if ($record->parentCategoryId == '-1')
                $section = 'root-category';
            else
                $section = 'sub-category';
        }
        else {
            $data['action'] = 'Add';
            $data['record'] = $sitecategoryObj;
        }
        $data['rootCategory'] = $sitecategoryObj->getCategoryList();
        $data['page'] = $page;
        $data['section'] = $section;
        return view('Administrator.sitecategory.addeditcategory', $data);
    }

    public function addeditrecord($id = '-1', $page, Request $request) {

        $sitecategoryObj = new Sitecategory();


        if ($id != -1)
        {
            $sitecategoryObj = $sitecategoryObj->find($id);
        }

        if($request->input('parentCategoryId') == -1)
        {
            $sitecategoryObj->type = $request->input('categoryType');
        }
        else
        {
            $sitecategoryObj->type = ($sitecategoryObj->find($request->input('parentCategoryId'))->type);
            $sitecategoryObj->scheduleBNumber = !empty($request->input('scheduleNumber')) ? $request->input('scheduleNumber') : '';
        }

        $sitecategoryObj->categoryName = $request->input('categoryName');
        $sitecategoryObj->parentCategoryId = $request->input('parentCategoryId');

        if ($sitecategoryObj->save())
            return redirect(url('/administrator/settingscategories?page=' . $page))->with('successMessage', 'Information saved successfuly.');
    }

    public function deleterecord($id = -1, $parentId, $page) {

        /* Fetch Data For Procurement Iems & Shipment Packages */
        $procurementCatgeoryId = \App\Model\Procurementitem::fetchUsedFieldIds('siteCategoryId');
        $shipmentCatgeoryId = \App\Model\Shipmentpackage::fetchUsedFieldIds('siteCategoryId');
        $categoryIdExists = array_unique(array_merge($procurementCatgeoryId, $shipmentCatgeoryId));

        $procurementSubCatgeoryId = \App\Model\Procurementitem::fetchUsedFieldIds('siteSubCategoryId');
        $shipmentSubCatgeoryId = \App\Model\Shipmentpackage::fetchUsedFieldIds('siteSubCategoryId');
        $subCategoryIdExists = array_unique(array_merge($procurementSubCatgeoryId, $shipmentSubCatgeoryId));
        /* Fetch Data For Procurement Iems & Shipment Packages */

        /* Merge Sub Catgeories and Catgeories */
        $categoryIdExists = array_unique(array_merge($categoryIdExists, $subCategoryIdExists));

        $sitecategoryObj = new Sitecategory();
        if ($parentId == '-1')
            $sitecategoryObj->deleteChild($id);
        if ($id != '-1') {
            if (!in_array($id, $categoryIdExists)) {
                $sitecategoryObj = $sitecategoryObj->find($id);
                if ($sitecategoryObj->delete())
                    return redirect(url('/administrator/settingscategories?page=' . $page))->with('successMessage', 'Information deleted successfuly.');
            }else {
                return redirect(url('/administrator/settingscategories?page=' . $page))->with('errorMessage', 'Catgeory cannot be deleted. Dependency exists');
            }
        } else {
            return redirect(url('/administrator/settingscategories?page=' . $page))->with('errorMessage', 'Failed to delete Catgeory information.');
        }
    }

    public function editstatus($id = -1, $status) {

        $sitecategoryObj = new Sitecategory();
        if ($id != -1)
            $sitecategoryObj = $sitecategoryObj->find($id);
        $sitecategoryObj->status = $status;

        if ($sitecategoryObj->save())
            return json_encode(array('updated' => true));
        else
            return json_encode(array('updated' => false));
    }

    public function getsubcategory($rootCategory = '0') {

        $sitecategoryObj = new Sitecategory();
        $subcategoryList = $sitecategoryObj->getSubCategory($rootCategory);
        return json_encode(array('data' => $subcategoryList));
    }

}
