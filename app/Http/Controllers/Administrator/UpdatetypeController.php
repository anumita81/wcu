<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Updatetype;
use App\libraries\imageHelpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;
use Config;
use Illuminate\Routing\Route;
use customhelper;

class UpdatetypeController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 20;
    }

    public function index(Route $route, Request $request) {
        $data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Updatetype'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        if (\Request::isMethod('post')) {
            /* GET POST VALUE  */
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('UPDATETYPEDATA');
            \Session::push('UPDATETYPEDATA.searchDisplay', $searchDisplay);
            \Session::push('UPDATETYPEDATA.field', $field);
            \Session::push('UPDATETYPEDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('UPDATETYPEDATA.field');
            $sortType = \Session::get('UPDATETYPEDATA.type');
            $searchDisplay = \Session::get('UPDATETYPEDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'name';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting')
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH COUNTRY LIST  */
        $updatetypeData = Updatetype::getUpdatetypeList($param);

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Updatetype";
        $data['contentTop'] = array('breadcrumbText' => 'Updatetype', 'contentTitle' => 'Update Types', 'pageInfo' => 'This section allows you to change update types');
        $data['pageTitle'] = "Update Types";
        $data['page'] = $updatetypeData->currentPage();
        $data['updatetypeData'] = $updatetypeData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];

        return view('Administrator.updatetype.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $contentId
     * @param type $page
     * @return string
     */
    public function addedit($id = '0', $page = '') {
        $data = array();
        $data['title'] = "Administrative Panel :: Updatetype";
        $data['contentTop'] = array('breadcrumbText' => 'Updatetype', 'contentTitle' => 'Update Types', 'pageInfo' => 'This section allows you to change update types');
        $data['page'] = !empty($page) ? $page : '1';
        if (!empty($id)) {
            $data['id'] = $id;
            $data['action'] = 'Edit';
            $updatetype = Updatetype::find($id);
            $data['updatetype'] = $updatetype;
        } else {
            $data['id'] = 0;
            $data['action'] = 'Add';
        }
        return view('Administrator.updatetype.addedit', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $contentId
     * @param string $page
     * @param Request $request
     * @return type
     */
    public function savedata($id, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $data['id'] = 0;

        $updatetype = new Updatetype;

        $validator = Validator::make($request->all(), [
                    'name' => 'required|unique:' . $updatetype->table . ',name,' . $id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            if (!empty($id)) {
                $updatetype = Updatetype::find($id);
                $updatetype->updatedBy = Auth::user()->id;
                $updatetype->updatedOn = Config::get('constants.CURRENTDATE');
            } else {
                $updatetype->createdBy = Auth::user()->id;
                $updatetype->createdOn = Config::get('constants.CURRENTDATE');
            }
            $updatetype->name = $request->name;
            $updatetype->save();
            $updatetype = $updatetype->id;

            return redirect('/administrator/updatetype?page=' . $page)->with('successMessage', 'Update type saved successfuly.');
        }
    }

    /**
     * Method used to change status
     * @param integer $contentId
     * @param integer $page
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Updatetype::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/updatetype/?page=' . $page)->with('successMessage', 'Update type status changed successfully.');
            } else {
                return \Redirect::to('administrator/updatetype/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/updatetype/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

    public function deletedata($id = '', $page = '') {

        $updatetype = new Updatetype();

        $createrModifierId = Auth::user()->id;


        if (!empty($id)) {
            $updatetype = $updatetype->find($id);

            if (Updatetype::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/updatetype/?page=' . $page)->with('successMessage', 'Update type deleted successfuly.');
            } else {
                return \Redirect::to('administrator/updatetype/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/updatetype/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
