<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Country;
use App\Model\State;
use App\Model\Autopickupcost;
use App\Model\Warehouse;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use customhelper;
use Config;

class AutopickupcostController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    /**
    * This function is used to generate list of Auto pick cost depending on country and state
    * @return html
    */
    public function index(Request $request) {

    	$data = array();
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.Autopickupcost'), Auth::user()->id); // call the helper function
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

    	$param = array();
    	$country = 'US';
    	$state = '';
        $allWarehouse = Warehouse::where('status', '1')->where('deleted', '0')->with('country', 'state', 'city')->get();
        //dd($allWarehouse[0]->country->name);
        $selectedWarehouse = $allWarehouse[0]->id;
        /* This is to initialize filtered data */
    	if (\Request::isMethod('post')) {
    		$searchData = $request->input('searchData');
            if(!empty($searchData['searchCountry'])) 
                $country = $searchData['searchCountry'];
            if(!empty($searchData['searchWarehouse'])) 
                $warehouse = $searchData['searchWarehouse'];

            if(!empty($searchData['searchState']))
                $state = $searchData['searchState'];
            if($state == '')
            {
                $state = State::where('countryCode',$country)->first();
                if(!empty($state))
                    $state = $state->id;
            }

            \Session::forget('AUTOPICKUPCOST');
            \Session::push('AUTOPICKUPCOST.country', $country);
            \Session::push('AUTOPICKUPCOST.state', $state);
            \Session::push('AUTOPICKUPCOST.warehouse',$warehouse);
    		
    	}
        else
        {
            /* Check if previous data exist or set default state country */
            $countryData = \Session::get('AUTOPICKUPCOST.country');
            $stateData = \Session::get('AUTOPICKUPCOST.state');
            $country = !empty($countryData) ? $countryData[0] : 'US';
            $warehouseData = \Session::get('AUTOPICKUPCOST.warehouse');
            $warehouse = !empty($warehouseData) ? $warehouseData[0] : $selectedWarehouse;
            if(!empty($stateData))
            {
                $state = $stateData[0];
            }
            else
            {
                $state = State::where('countryCode',$country)->first();
                if(!empty($state))
                    $state = $state->id;
            }
        }

    	$param['searchData']['searchCountry'] = $country;
    	$param['searchData']['searchState'] = $state;
        $param['searchData']['searchWarehouse'] = $warehouse;
        /* Fetch cities data mapped with state and country */
    	$countryWiseMappedData = Country::countryMappedStateCity($country,$state);
        /* Fetch all active country for filtering */
    	$allCountries = Country::where('status','1')->orderBy('id')->get();
    	$allState = array();
    	if($country != '')
            /* Fetch all states of a country */
    		$allState = State::where('countryCode',$country)->where('status','1')->where('deleted','0')->get();

    	$data['countryWiseMappedData'] = $countryWiseMappedData;
    	$data['allCountries'] = $allCountries;
    	$data['allState'] = $allState;
        /* Fetch all pickup cost mapped with cities */
        $data['costData'] = Autopickupcost::cityWiseMappedData();
        //dd($data['costData']);
        $data['allWarehouse'] = $allWarehouse;
    	$data['param'] = $param;
    	$data['title'] = "Administrative Panel :: Auto Pick-up Cost";
        $data['contentTop'] = array('breadcrumbText' => array('Auto','Pick-up Cost'), 'contentTitle' => 'Auto Pick-up Cost', 'pageInfo' => 'This section allows you to manage pick-up cost for auto');
        $data['pageTitle'] = "Auto Pick-up Cost";

        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
    	return view('Administrator.autopickupcost.index',$data);

    }

    /**
    * This function used to Add or update pickup cost of selected cities 
    * @return html 
    */
    public function update(Request $request) {
        /* Get all post data */
    	$postData = $request->input('postData');
        /* Get warehouse id */
        $warehouseId = $request->warehouseId;
        /* Loop through all post data */
        if(!empty($postData))
        {
            foreach($postData as $cityId => $eachPostData)
            {
                /* Check if checkbor for any corresponding city is checked */
                    if(isset($eachPostData['is_update']))
                    {
                    $autoPickupCostObj = new Autopickupcost;
                    /* Check if cost for any particular city exist. If exist Update or Add */
                    $ifRecordExist = $autoPickupCostObj->where('cityId',$cityId)->where('warehouseId',$warehouseId)->first();
                            if(!empty($ifRecordExist))
                        $autoPickupCostObj = $autoPickupCostObj->find($ifRecordExist->id); // Initialize object to existing value
                    $autoPickupCostObj->cityId = $cityId; // Initialize city
                    $autoPickupCostObj->warehouseId = $warehouseId; // Initialize warehouseid
                    $autoPickupCostObj->pickupCost = $eachPostData['cost']; // Initilize cost
                    $autoPickupCostObj->save(); // Add or update depending on condition
                    }
            }

            return redirect()->back()->with('successMessage', 'Cost saved successfully.');
        }
        else
        {
            return redirect()->back()->with('errorMessage', 'No city selected or exists.');
        }
    }
    
     /**
     * Method used to update procurement item status
     * @param integer $id
     * @return boolean
     */
    public function updateprocurementitemstatus(Request $request) {
        print_r($request->all());
        $procurementItemIds = $request->procurementItemIds;
        $status = $request->status;

        if (!empty($procurementItemIds)) {
            $procurementItemArr = explode('^', $procurementItemIds);

            foreach ($procurementItemArr as $id) {
                $procurementItem = new Procurementitem;
                $procurementItem = Procurementitem::find($id);

                /* INSERT DATA INTO STATUS LOG TABLE */
                $procurementitemstatus = new Procurementitemstatus;
                $procurementitemstatus->procurementId = $procurementItem->id;
                $procurementitemstatus->procurementItemId = $id;
                $procurementitemstatus->oldStatus = $procurementItem->status;
                $procurementitemstatus->status = $status;
                $procurementitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
                $procurementitemstatus->save();

                /* UPDATE PROCUREMENT ITEM TABLE */
                $procurementItem->status = $status;
                $procurementItem->save();
            }

            /* CHECK AND UPDATE PROCUREMENT STATUS */
            Procurement::updaterejectedstatus($procurementItem->id);

            return 1;
        }
        return 0;
    }

}