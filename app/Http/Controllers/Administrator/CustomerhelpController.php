<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Customerhelpcategories;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;

class CustomerhelpController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, Request $request) {

        $data = array();

        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.CustomerHelpCategories'), Auth::user()->id);
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('CH');
            \Session::push('CH.searchDisplay', $searchDisplay);
            \Session::push('CH.field', $field);
            \Session::push('CH.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('CH.field');
            $sortType = \Session::get('CH.type');
            $searchDisplay = \Session::get('CH.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'name' => array('current' => 'sorting'),
            'orders' => array('current' => 'sorting'),
            'status' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH make LIST  */
        $chCatData = Customerhelpcategories::where('deleted', '0')->orderBy($param['field'], $param['type'])->paginate($param['searchDisplay']);
        if (count($chCatData) > 0) {
            $data['page'] = $chCatData->currentPage();
        } else {
            $data['page'] = 1;
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Customer Help Categories";
        $data['contentTop'] = array('breadcrumbText' => 'Customer Help Categories', 'contentTitle' => 'Customer Help Categories', 'pageInfo' => 'This section allows you to manage Customer Help Categories');
        $data['pageTitle'] = "Customer Help Categories";
        $data['chCatData'] = $chCatData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavContent', 'menuSub' => 'leftNavCustomerHelp2', 'menuSubSub' => 'leftNavManageCategories111');
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        return view('Administrator.customerhelp.categories.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $id
     * @param string $page
     * @return array
     */
    public function addedit($id = '0', $page = '') {
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.CustomerHelpCategories'), Auth::user()->id); 
        $data = array();
        $data['title'] = "Administrative Panel :: Customer Help Categories";
        $data['contentTop'] = array('breadcrumbText' => 'Customer Help Categories', 'contentTitle' => 'Customer Help Categories', 'pageInfo' => 'This section allows you to manage Customer Help Categories');
        $data['page'] = !empty($page) ? $page : '1';

        if (!empty($id)) {
            if($findRole['canEdit'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['pageTitle'] = "Edit Customer Help Category";
            $data['chCatId'] = $id;
            $data['page'] = $page;
            $data['action'] = 'Edit';
            $data['chCatData'] = Customerhelpcategories::find($id);
        } else {
            if($findRole['canAdd'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add New Category for Customer Help";
            $data['page'] = $page;
        }
        return view('Administrator.customerhelp.categories.add', $data);
    }

    /** Edit
     * Method used to save content information
     * @param integer $id
     * @param string $page
     * @param Request $request
     * @return array
     */
    public function savedata($id, $page, Request $request) {

        $data = array();
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.CustomerHelpCategories'), Auth::user()->id);
        $data['page'] = !empty($page) ? $page : '1';
        $chCat = new Customerhelpcategories;

        /* VALIDATATION */
        $validator = Validator::make(\Input::all(), [
            'name' => 'required',
        ]);

        /* IF VALIDATATIONS FAILS */
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

            /*  EDIT  */
            if ($id != 0) { 
                if($findRole['canEdit'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }


                /* Check Whether any leading zero */
                $orders = ltrim($request->displayOrder, 0);

                if($orders == ''){
                    return redirect('/administrator/customerhelp?page=' . $page)->with('errorMessage', 'Order cannot be 0');
                }

                $chCat = Customerhelpcategories::find($id);
                $chCat->updatedBy = Auth::user()->id;
                $chCat->updatedOn = Config::get('constants.CURRENTDATE');
                $chCat->orders = $orders;
            } 
            /*  ADD  */
            else {
                if($findRole['canAdd'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
                }
                
                $getLastOrders = Customerhelpcategories::orderBy('id', 'desc')->first();
                $lastOrder = (count($getLastOrders) > 0 ? $getLastOrders->orders+1 : 1);
                $newOrder = $lastOrder;
                $chCat->orders = $newOrder;
                $chCat->createdBy = Auth::user()->id;
                $chCat->createdOn = Config::get('constants.CURRENTDATE');
                $chCat->slug = strtolower(str_replace(' ','-',preg_replace('/[^\w\s]/', '', $request->input('name'))));
                
            }
            $chCat->name = $request->name;

            /* SAVING THE DATA */
            $chCat->save();
        }


        return redirect('/administrator/customerhelp?page=' . $page)->with('successMessage', 'Category saved successfuly.');
    }


    /**
     * Method used to change status
     * @param integer $id
     * @param string $page
     * @param string $status
     * @return type
     */
    public function changestatus($id, $page, $status) {
        $page = !empty($page) ? $page : '1';

        /* CHECK AT LEAST ONE CATEGORY LIVE */
        $checkLive = Customerhelpcategories::where("deleted", '0')->where("status", "1")->count();
        if($checkLive == 1 && $status == 0){
            return \Redirect::to('administrator/customerhelp/?page=' . $page)->with('errorMessage', 'At least one help category should activated');
        }

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Customerhelpcategories::changeStatus($id, $createrModifierId, $status)) {
                return \Redirect::to('administrator/customerhelp/?page=' . $page)->with('successMessage', 'Status changed successfully.');
            } else {
                return \Redirect::to('administrator/customerhelp/?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/customerhelp/?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }



    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $page) {
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.CustomerHelpCategories'), Auth::user()->id); 
        if($findRole['canDelete'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $page = !empty($page) ? $page : '1';

        /* CHECK AT LEAST ONE CATEGORY LIVE */
        $checkLive = Customerhelpcategories::where("deleted", '0')->where("status", "1")->count();
        if($checkLive == 1){
            return \Redirect::to('administrator/customerhelp/?page=' . $page)->with('errorMessage', 'At least one help category should activated');
        }

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            if (Customerhelpcategories::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/customerhelp?page=' . $page)->with('successMessage', 'Category and its related sections are deleted successfully.');
            } else {
                return \Redirect::to('administrator/customerhelp?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/customerhelp?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }
    
}
