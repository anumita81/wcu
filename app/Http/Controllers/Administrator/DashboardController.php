<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\libraries\helpers;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Log;
use Excel;
use Mail;
use Illuminate\Routing\Route;
use customhelper;
use App\Model\User;
use App\Model\Order;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
    }

    public function getIndex(Request $request) {
        $data = array();
        $data['title'] = "ADMIN - Shoptomydoor";

        $data['contentTop'] = array('breadcrumbText' => 'Dashboard', 'contentTitle' => 'Dashboard', 'pageInfo' => '');
        $data['pageTitle'] = "Dashboard";

        //$data['warehouseList'] = \App\Model\Warehouse::join('countries', 'warehousemaster.countryId', '=', 'countries.id')->select('warehousemaster.id', 'countries.name AS countryName')->where('warehousemaster.status', '1')->where('warehousemaster.deleted', '0')->get()->toArray();
        // $data['warehouseList'] = \App\Model\Warehouse::warehouseCountryMap()->toArray();
        // $data['webUserList'] = User:: where('registeredBy', 'website')->where('status', '1')->where('deleted', '0')->count();
        // $data['appUserList'] = User:: where('registeredBy', 'app')->where('status', '1')->where('deleted', '0')->count();
        // $data['fileClaimRequest'] = \App\Model\Contactus:: where('reason_id', '11')->whereIn('status', array('1', '2'))->where('deleted', '0')->count();
        // $data['contactUsRequest'] = \App\Model\Contactus:: where('reason_id', '!=', '11')->whereIn('status', array('1', '2'))->where('deleted', '0')->count();

        return view('Administrator.dashboard.index', $data);
    }

    public function searchbyselection($searchKey, $searchType) {

        if ($searchType == 'shipmentId') {

            $shipmentRecord = \App\Model\Shipment::find($searchKey);
            if(!empty($shipmentRecord))
            {
                return 'shipment_'.$shipmentRecord->id;
            }
            else {
                return '-1';
            }                
            
        } elseif ($searchType == 'orderId') {
            $searchOrderArr = array(
                'idFrom' => '',
                'idTo' => '',
                'orderIdTo' => '',
                'orderIdFrom' => '',
                'unitTo' => '',
                'unitFrom' => '',
                'orderStatus' => '',
                'prepaid' => '',
                'deliveryCompanyId' => '',
                'dispatchCompanyId' => '',
                'shipmentType' => '',
                'totalFrom' => '',
                'totalTo' => '',
                'user' => '',
                'receiver' => '',
                'paymentStatus' => '',
                'toCountry' => '',
                'warehouseId' => '',
                'labelType' => '',
                'itemType' => '',
                'packed' => ''
            );

            $searchOrderArr['orderIdFrom'] = $searchKey;

            $searchOrder = \Input::get('searchOrder', $searchOrderArr);

            \Session::push('ORDERDATA.searchOrder', $searchOrder);

            return 2;
        } elseif ($searchType == 'customerID') {
            $searchByUnit = \Input::get('searchByUnit', $searchKey);

            \Session::push('USERDATA.searchByUnit', $searchByUnit);

            return 3;
        } elseif ($searchType == 'autoShipmentID') {
            $shipmentRecord = \App\Model\Autoshipment::find($searchKey);
            if(!empty($shipmentRecord))
            {
                return 'Autoshipment_'.$shipmentRecord->id;
            }
            else{
                return '-1';
            } 
                
        }elseif($searchType == 'trackingID') {            
            $shipmentRecord = \App\Model\Shipmentdelivery::where('tracking',$searchKey)->orWhere('tracking2',$searchKey)->first();
            if(!empty($shipmentRecord)) {
                return 'shipment_'.$shipmentRecord->shipmentId;
            } else {
                return '-1';
            }
            
        } else {
            return '-1';
        }
    }

    public function getSearchOption($searchKey) {
        $data = array();
        if ($searchKey == 'customer') {
            $listofarray = \App\Model\Country::where('status', '1')->orderby('name', 'asc')->get()->toArray();
        } else {
            $listofarray = \App\Model\Warehouse::join('countries', 'warehousemaster.countryId', '=', 'countries.id')->select('warehousemaster.id', 'countries.name AS countryName')->where('warehousemaster.status', '1')->where('warehousemaster.deleted', '0')->get()->toArray();
        }

        echo json_encode($listofarray);
        exit;
    }

    public function getCountryName($searchId) {
        $countryName = \App\Model\Country::select('name')->where('status', '1')->where('id', $searchId)->get();


        echo json_encode($countryName);
        exit;
    }

    public function getWarehouseName($searchId) {
        //$warehouseName = \App\Model\Warehouse::join('countries', 'warehousemaster.countryId', '=', 'countries.id')->select('countries.name AS countryName')->where('warehousemaster.status', '1')->where('warehousemaster.deleted', '0')->where('warehousemaster.id', $searchId)->get();
        $warehosueName = \App\Model\Warehouse::warehouseCountryMap();

        echo json_encode($warehouseName);
        exit;
    }

    /*
     * Function to get Chart data with search options values
     */

    public function getChartsRecords($searchKey, Request $request) {
        $data = array();
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $searchId = $request->searchId;

        $data['currency'] = \App\Helpers\customhelper::getCurrencySymbolCode();

        if ($searchKey == 'revenue-chart') {
            $listofarray = \App\Model\Dashboard::getRevenueData($searchId, $startDate, $endDate);
            if (!empty($listofarray)) {
                foreach ($listofarray as $list) {
                    $data['data'][] = $list->totalCost;
                }
            }
        } else if ($searchKey == 'shipment-chart') {
            $listofarray = \App\Model\Dashboard::getShipmentData($searchId, $startDate, $endDate);
            if (!empty($listofarray)) {
                foreach ($listofarray as $list) {
                    $data['data'][] = $list->totalShipment;
                }
            }
        } else {
            $listofarray = \App\Model\Dashboard::getCustomerData($searchId, $startDate, $endDate);
            if (!empty($listofarray)) {
                foreach ($listofarray as $list) {
                    $data['data'][] = $list->totalUser;
                }
            }
        }

        echo json_encode($data);
        exit;
    }

    /*
     * Function to get order records
     * 
     */

    public function getOrderRecords(Request $request) {
        $data = array();

        $startDate = $request->startDate;
        $endDate = $request->endDate;

        $countryList = \App\Model\Dashboard::getCountryList($startDate, $endDate);
        //select stmd_countries.name, count(stmd_orders.id) maxorder from stmd_countries join stmd_shipments on stmd_shipments.toCountry = stmd_countries.id join stmd_orders on stmd_orders.shipmentId=stmd_shipments.id where stmd_orders.status="5" group by stmd_shipments.toCountry

        echo json_encode($countryList);
    }

    /*
     * Function to get shipment status
     */

    public function getShipmentStatus(Request $request) {
        $data = array();

        $startDate = date('Y-m-d', strtotime($request->startDate));
        $endDate = date('Y-m-d', strtotime($request->endDate));
        $keyword = $request->keyword;
        $tblchoice = (!empty($request->tblchoice) ? $request->tblchoice : '');
        // dd(DB::getQueryLog());

        if (!empty($tblchoice)) {
            /* Data fetching from procurement */
            $shipmentStatusName = array("requestforcost" => "requestforcost", "submitted" => "submitted", "completed" => "completed", "rejected" => "rejected");
            $data['shipmentClassname'] = array("requestforcost" => "ltGreenCir", "submitted" => "drkGreenCir", "completed" => "orangeCir", "rejected" => "ltRedCir");
            foreach ($shipmentStatusName as $key => $status) {
                $shipmentStatus[$status] = \App\Model\Procurement::select('id')->where('procurementType', $keyword)->where('status', '' . $key . '')->where('deleted', '0')->whereBetween('createdOn', array($startDate, $endDate))->count();
                $data['statusname'][$status] = $shipmentStatus[$status];
            }
        } else {
            /* Data fetching from shipment */
            $shipmentStatusName = array("0" => "Shipment Submiited", "1" => "In Warehouse", "2" => "In Transit", "3" => "Customs clearing", "4" => "Destination warehouse", "5" => "Out for delivery", "6" => "Delivered");
            $data['shipmentClassname'] = array("Shipment Submiited" => "skyBlueCir", "In Warehouse" => "ltGreenCir", "In Transit" => "drkGreenCir", "Customs clearing" => "yellGreeCir", "Destination warehouse" => "orangeCir", "Out for delivery" => "ltRedCir", "Delivered" => "RedCir");
            foreach ($shipmentStatusName as $key => $status) {
                $shipmentStatus[$status] = \App\Model\Shipment::select('id')->where('shipmentType', $keyword)->where('shipmentStatus', '' . $key . '')->where('deleted', '0')->whereBetween('createdOn', array($startDate, $endDate))->count();
                $data['statusname'][$status] = $shipmentStatus[$status];
            }
        }
        echo json_encode($data);
    }

    /*
     * Function to get Complete Orders 
     */

    public function getCompletedOrder(Request $request) {
        $data = array();
        $startDate = date('Y-m-d', strtotime($request->startDate));
        $endDate = date('Y-m-d', strtotime($request->endDate));

        $data['shopformeOrderList'] = \App\Model\Dashboard::getCompletedOrder($startDate, $endDate, "shopforme");
        $data['autopartsOrderList'] = \App\Model\Dashboard::getCompletedOrder($startDate, $endDate, "autopart");
        $data['autoshipmentOrderList'] = \App\Model\Dashboard::getCompletedOrder($startDate, $endDate, "autoshipment");
        echo json_encode($data);
    }

    /*
     * Function to get Total Shipment
     */

    public function getTotalShipment(Request $request) {
        $data = array();

        $startDate = date('Y-m-d', strtotime($request->startDate));
        $endDate = date('Y-m-d', strtotime($request->endDate));

        $data['shopformeShipmentList'] = \App\Model\Dashboard::getTotalShipment($startDate, $endDate, "shopforme");
        $data['autopartsShipmentList'] = \App\Model\Dashboard::getTotalShipment($startDate, $endDate, "autopart");
        $data['autoshipmentList'] = \App\Model\Dashboard::getTotalShipment($startDate, $endDate, "autoshipment");
        echo json_encode($data);
    }

    /*
     * Function to get total Revenue
     */

    public function getTotalRevenue(Request $request) {
        $data = array();

        $startDate = date('Y-m-d', strtotime($request->startDate));
        $endDate = date('Y-m-d', strtotime($request->endDate));

        $data['shopformeRevenue'] = \App\Model\Dashboard::getTotalRevenue($startDate, $endDate, "shopforme");
        $data['autopartsRevenue'] = \App\Model\Dashboard::getTotalRevenue($startDate, $endDate, "autopart");
        $data['autoshipmentRevenue'] = \App\Model\Dashboard::getTotalRevenue($startDate, $endDate, "autoshipment");

        echo json_encode($data);
    }

    /*
     * Function to get top 10 country of maximum customers
     */

    public function gettopCustomers(Request $request) {
        $data = array();

        $startDate = date('Y-m-d', strtotime($request->startDate));
        $endDate = date('Y-m-d', strtotime($request->endDate));

        $userList = \App\Model\Dashboard::gettopCustomers($startDate, $endDate);

        echo json_encode($userList);
    }

    public function getshippingnotifications(Request $request) {

        $adminUserId = $request->session()->get('admin_session_user_id');
        $newShipmentData = $newOrderData = $newProcurementData = $newAutopartData = $newBuycarData = $newAutoshipmentData = $totalNotificationData = 0;
        $data = array();

        $userLastLogin = \App\Model\Adminuserlog::where('adminUserId', $adminUserId)->orderBy('id', 'desc')->take('1')->offset('1')->first();
        if (!empty($userLastLogin)) {
            $lastLoginDateTime = $userLastLogin->adminUserInTime;
            $newShipmentData = \App\Model\Shipment::select('id')->where('deleted', '0')->where('createdOn', '>=', $lastLoginDateTime)->get()->count();
            $totalNotificationData += $newShipmentData;
            $newOrderData = \App\Model\Order::select('id')->where('createdDate', '>=', $lastLoginDateTime)->get()->count();
            $totalNotificationData += $newOrderData;
            $newProcurementData = \App\Model\Procurement::select('id')->where('procurementType', 'shopforme')->where('deleted', '0')->where('createdOn', '>=', $lastLoginDateTime)->get()->count();
            $totalNotificationData += $newProcurementData;
            $newAutopartData = \App\Model\Procurement::select('id')->where('procurementType', 'autopart')->where('deleted', '0')->where('createdOn', '>=', $lastLoginDateTime)->get()->count();
            $totalNotificationData += $newAutopartData;
            $newBuycarData = \App\Model\Procurement::select('id')->where('procurementType', 'buycarforme')->where('deleted', '0')->where('createdOn', '>=', $lastLoginDateTime)->get()->count();
            $totalNotificationData += $newBuycarData;
            $newAutoshipmentData = \App\Model\Autoshipment::select('id')->where('deleted', '0')->where('createdOn', '>=', $lastLoginDateTime)->get()->count();
            $totalNotificationData += $newAutoshipmentData;
        }

        $data['newShipmentData'] = $newShipmentData;
        $data['newOrderData'] = $newOrderData;
        $data['newProcurementData'] = $newProcurementData;
        $data['newAutopartData'] = $newAutopartData;
        $data['newBuycarData'] = $newBuycarData;
        $data['newAutoshipmentData'] = $newAutoshipmentData;
        $data['totalNotificationData'] = $totalNotificationData;

        return view('Administrator.dashboard.shipmentnotification', $data);
    }

    public function getcontactnotifications(Request $request) {

        $adminUserId = $request->session()->get('admin_session_user_id');
        $newFileclaimData = $newShippingquoteData = $newOthercontactData = $totalContactData = 0;
        $data = array();
        $userLastLogin = \App\Model\Adminuserlog::where('adminUserId', $adminUserId)->orderBy('id', 'desc')->take('1')->offset('1')->first();

        if (!empty($userLastLogin)) {
            $lastLoginDateTime = $userLastLogin->adminUserInTime;
            $newFileclaimData = \App\Model\Contactus::notificationCount('claim_a_file', $lastLoginDateTime);
            $totalContactData += $newFileclaimData;
            $newShippingquoteData = \App\Model\Contactus::notificationCount('shipping_quote', $lastLoginDateTime);
            $totalContactData += $newShippingquoteData;
        }

        $data['newFileclaimData'] = $newFileclaimData;
        $data['newShippingquoteData'] = $newShippingquoteData;
        $data['totalContactData'] = $totalContactData;

        return view('Administrator.dashboard.contactnotification', $data);
    }

}
