<?php

namespace App\Http\Controllers\Administrator;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Tax;
use App\Model\Taxrate;
use App\Model\Zone;
use App\libraries\dbHelpers;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Config;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use customhelper;

class TaxrateController extends Controller {

    public $_perPage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
        $this->_perPage = 10;
    }

    public function index(Route $route, $id = 0, Request $request) {

        /*  IF VALID TAXID  */
        $taxsettings = Tax::find($id);
        if(count($taxsettings)== 0){
            dd('No tax Id found! Hit back button of your browser');
        }
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.TaxSettings'), Auth::user()->id);
        if($findRole['canView'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }

        $data = array();
        if (\Request::isMethod('post')) {
            $searchDisplay = \Input::get('searchDisplay', $this->_perPage);

            $field = \Input::get('field', 'id');
            $type = \Input::get('type', 'desc');

            /*  SET SESSION VALUE FOR SORTING  */
            \Session::forget('TAXRATEDATA');
            \Session::push('TAXRATEDATA.searchDisplay', $searchDisplay);
            \Session::push('TAXRATEDATA.field', $field);
            \Session::push('TAXRATEDATA.type', $type);

            $param['field'] = $field;
            $param['type'] = $type;
            $param['searchDisplay'] = $searchDisplay;
        } else {
            $sortField = \Session::get('TAXRATEDATA.field');
            $sortType = \Session::get('TAXRATEDATA.type');
            $searchDisplay = \Session::get('TAXRATEDATA.searchDisplay');

            $param['field'] = !empty($sortField) ? $sortField[0] : 'id';
            $param['type'] = !empty($sortType) ? $sortType[0] : 'asc';
            $param['searchDisplay'] = !empty($searchDisplay) ? $searchDisplay[0] : $this->_perPage;
        }

        /* BUILD SORTING ARRAY */
        $sort = array(
            'zoneId' => array('current' => 'sorting'),
            'rateValue' => array('current' => 'sorting'),
            'rateType' => array('current' => 'sorting'),
        );

        /* SET SORTING ARRAY  */
        $sort[$param['field']]['current'] = ($param['type'] == 'asc') ? 'sorting_asc' : 'sorting_desc';

        /* FETCH RATE LIST  */
        $taxrateData = Taxrate::getTaxRateList($param, $id);
        if(count($taxrateData) > 0){
            $data['page'] = $taxrateData->currentPage();
        }

        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Tax Rate";
        $data['contentTop'] = array('breadcrumbText' => 'Tax Rate', 'contentTitle' => 'Tax Rate', 'pageInfo' => 'In this section you can define taxes to be used within your store');
        $data['pageTitle'] = "Tax Rate";
        $data['taxId'] = $id;
        $data['taxrateData'] = $taxrateData;
        $data['searchData'] = $param;
        $data['sort'] = $sort;
        $data['canView'] = $findRole['canView'];
        $data['canAdd'] = $findRole['canAdd'];
        $data['canEdit'] = $findRole['canEdit'];
        $data['canDelete'] = $findRole['canDelete'];
        $data['leftMenuSelection'] = array('menuMain' => 'leftNavSettings', 'menuSub' => 'leftNavShippingChargesSettings3', 'menuSubSub' => 'leftNavTaxSystem43');
        
        return view('Administrator.taxrate.index', $data);
    }

    /**
     * Method for add edit page
     * @param integer $id
     * @param integer $taxId
     * @param type $page
     * @return array
     */
    public function addedit($id = '0', $taxId, $page = '') {
        $data = array();
        /* SET DATA FOR VIEW  */
        $data['title'] = "Administrative Panel :: Tax Rate";
        $data['contentTop'] = array('breadcrumbText' => 'Tax Rate', 'contentTitle' => 'Tax Rate', 'pageInfo' => 'In this section you can define taxes to be used within your store');
        $data['page'] = !empty($page) ? $page : '1';
        
        
        if (!empty($id)) {
            /*  FIND USER ROLE  */
            $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.TaxSettings'), Auth::user()->id);
            if($findRole['canEdit'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            /*  EDIT  */
            $data['pageTitle'] = "Edit Tax Rate";
            $data['id'] = $id;
            $data['taxId'] = $taxId;
            $data['page'] = $page;
            $data['action'] = 'Edit';
            /*  GET TAX RATE RECORD  */
            $data['rateData'] = Taxrate::find($id);
            /*  GET ZONE INFO  */
            $data['zoneData'] = Zone::where('status', '1')->orderBy('name', 'asc')->get();
        } else {
            /*  FIND USER ROLE  */
            $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.TaxSettings'), Auth::user()->id);
            if($findRole['canAdd'] == 0){
                return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
            }
            /*  ADD  */
            $data['id'] = 0;
            $data['action'] = 'Add';
            $data['pageTitle'] = "Add Tax Rate";
            $data['taxId'] = $taxId;
            $data['page'] = $page;
            /*  GET ZONE INFO  */
            $data['zoneData'] = Zone::where('status', '1')->orderBy('name', 'asc')->get();
        }
        return view('Administrator.taxrate.add', $data);
    }


    /**
     * Method used to save tax rate information
     * @param integer $id
     * @param string $taxId
     * @param Request $page
     * @return type
     */
    public function savedata($id, $taxId, $page, Request $request) {

        $data = array();

        $data['page'] = !empty($page) ? $page : '1';
        $taxRate = new Taxrate;
        
        /*  EDIT  */
        if($id != 0) {
        /*  GET TAX RATE INFO  */
        $taxRate = Taxrate::find($id);
        $taxRate->modifiedBy = Auth::user()->id;
        $taxRate->modifiedOn = Config::get('constants.CURRENTDATE');
        } 
        else { 
            /*  ADD  */
            $checkDuplicate = Taxrate::where('taxId', $taxId)->where('zoneId', $request->zoneId)->where('deleted', '0')->get();
            /*  CHECKING EXISTING RATE FOR A ZONE  */
            if(count($checkDuplicate) > 0){
                return redirect('/administrator/taxrate/'.$taxId.'?page='.$page)->with('errorMessage', 'Rate already exists for the selected Zone');
            }
        $taxRate->createdBy = Auth::user()->id;
        $taxRate->createdOn = Config::get('constants.CURRENTDATE');
        }
        $taxRate->taxId = $taxId;
        $taxRate->zoneId = $request->zoneId;
        $taxRate->rateValue = $request->rateValue;
        $taxRate->rateType = $request->rateType;
        /*  SAVING RECORD  */
        $taxRate->save();
        

        return redirect('/administrator/taxrate/'.$taxId.'?page='.$page)->with('successMessage', 'Tax Rate information saved successfuly.');
        
    }


    /**
     * Method used to delete
     * @param integer $id
     * @param integer $page
     * @return type
     */
    public function delete($id, $taxId, $page) {
        /*  FIND USER ROLE  */
        $findRole = customhelper::seePermission(Config::get('constants.PermissionMenuIds.TaxSettings'), Auth::user()->id); // call the helper function
        if($findRole['canDelete'] == 0){
            return \Redirect::to('administrator/authentication')->with('errorMessage', Config::get('constants.textMenuRestriction.restrictedText'));
        }
        $page = !empty($page) ? $page : '1';

        $createrModifierId = Auth::user()->id;
        if (!empty($id)) {
            /*  DELETING RECORD  */
            if (Taxrate::deleteRecord($id, $createrModifierId)) {
                return \Redirect::to('administrator/taxrate/' . $taxId . '?page=' . $page)->with('successMessage', 'Tax deleted successfully.');
            } else {
                return \Redirect::to('administrator/taxsettings/' . $taxId . '?page=' . $page)->with('errorMessage', 'Error in operation!');
            }
        } else {
            return \Redirect::to('administrator/taxsettings/' . $taxId . '?page=' . $page)->with('errorMessage', 'Error in operation!');
        }
    }

}
