<?php

namespace App\Exceptions;

use Exception;
use Log;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
		if ($exception instanceof \Illuminate\Http\Exceptions\PostTooLargeException){
            return \Redirect::to('administrator/media')->with('errorMessage', 'File size is too big.');
        }
        
        if($this->isHttpException($exception)){
			$logFile = 'laravel.log';

			Log::useDailyFiles(storage_path().'/logs/'.$logFile);
			//echo Route::getCurrentRoute()->getAction();

			

			//echo $exception->getStatusCode();exit;
            switch ($exception->getStatusCode()) {

                case 404:
                    Log::info('Unable to find page.');
                    return response()->view('errors.404',[],404);

                    break;

                case 500:
					
					Log::error('In Server is really going wrong.');
                    return response()->view('errors.500',[],500);

                    break;
				 default:
                    return $this->renderHttpException($e);
                break;

            }

        }
        return parent::render($request, $exception);
    }
}
