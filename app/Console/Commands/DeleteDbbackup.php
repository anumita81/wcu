<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DeleteDbbackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dbbackup:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete old backup records';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $recordToRetain = \App\Model\Dbbackuplog::select('id')->orderBy('id','DESC')->skip(4)->take(1)->first();
        $recordsToDelete = \App\Model\Dbbackuplog::where('id','<',$recordToRetain['id'])->get();
        $error = array();
        if($recordsToDelete->count()>0)
        {
            foreach($recordsToDelete as $eachRecordToDelete)
            {
                $fileData = \App\Model\Dbbackuplog::find($eachRecordToDelete->id);
                $filepath = public_path('backups/'.$fileData->backupFile);
                if(file_exists($filepath))
                {
                    unlink($filepath);
                }

                if(!$fileData->delete())
                {
                    $error[] = $eachRecordToDelete->id;
                }
            }
            
            if(!empty($error))
                $this->error("Records for ids ".implode(",",$error)." unable to delete");
            else
                $this->info("Old backup deleted successfully");
        }
        else
        {
            $this->error("No backup records available to delete");
        }
        
        
    }
}
