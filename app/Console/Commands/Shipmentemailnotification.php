<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Config;
use customhelper;
use DB;

class Shipmentemailnotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Shipmentemailnotification:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This is used to send notification is queue created during shipment creation and delivery creation';
    
    protected $process;
    public $fileName = "";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
           
            $shipmentNotificationQuery = \App\Model\Shipmentemailnotification::whereRaw('(emailSent = "0" OR smsSent="0")');
            $selectedIds = $shipmentNotificationQuery->pluck('id')->toArray();
            //print_r($selectedQuery);
            $shipmentNotificationData = $shipmentNotificationQuery->get();
            //print_r($shipmentNotificationData);
            //exit;
            //print_r($emailScheduledNotificationData); die;
            if($shipmentNotificationData->count() > 0) {
                \App\Model\Shipmentemailnotification::whereIn("id",$selectedIds)->update(["emailSent"=>"1","smsSent"=>"1"]);
                foreach($shipmentNotificationData as $eachNotification) {
                    $emailSent = 1;
                    $smsSent = 1;
                    $emailSentOn = "";
                    $smsSentOn = "";
                    $emailTemplateKey = 'create_shipment_notification';
                    if($eachNotification->action == 'create_delivery')
                        $emailTemplateKey = 'create_delivery_notification';
                    $isEmailSent = \App\Model\Shipment::notificationEmail($eachNotification->shipmentId,$emailTemplateKey);
                    if($isEmailSent)
                    {
                        $emailSent = "2";
                        $emailSentOn = date("Y-m-d H:i:s");
                    }
                    $isSmsSent = \App\Model\Shipment::notificationSMS($eachNotification->shipmentId,$eachNotification->deliveryId);
                    if($isSmsSent)
                    {
                        $smsSent = "2";
                        $smsSentOn = date("Y-m-d H:i:s");
                    }
                    \App\Model\Shipmentemailnotification::where("id",$eachNotification->id)->update(["emailSent"=>$emailSent,"emailSentOn"=>$emailSentOn,"smsSent"=>$smsSent,"smsSentOn"=>$smsSentOn]);
                }
                $this->info('Email sent');
            } else {
                $this->info('No scheduled notification left');
            }

        } catch (ProcessFailedException $exception) {
            $this->error('Email sent failed');
        }
    }
}
