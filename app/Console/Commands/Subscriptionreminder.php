<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Config;
use customhelper;
use DB;

class Subscriptionreminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Subscriptionreminder:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscription notification';
    
    protected $process;
    public $fileName = "";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $startDate = Config::get('constants.CURRENTDATE');
            
            $endDate = date('Y-m-d H:i:s', strtotime($startDate.' + 7 days'));
            
            $emailSubcriptionNotificationData = \App\Model\Usersubscription::whereRaw('expiryDate > "'.$startDate.'" AND expiryDate <="'.$endDate.'" AND paymentStatus="paid"')->get();
            if($emailSubcriptionNotificationData->count() > 0) {
                
                foreach($emailSubcriptionNotificationData as $eachRecord) {
                    $orderId = '';
                    $customer = \App\Model\User::find($eachRecord->userId);
                    $userSubscriptionData = \App\Model\Usersubscription::where('userId', $eachRecord->userId)->first();

                    $accountManager= \App\Model\UserAdmin::find($customer->salesRepresentativeId);
                    
                    $link = Config::get('constants.frontendUrl');
        
                    $replace['[NAME]'] = $customer->firstName . " " . $customer->lastName;

                    $replace['[DATE]']  = date('m-d-Y', strtotime($userSubscriptionData->expiryDate));      
                  
                        $replace['[WEBSITE_LINK]'] = '<a href="' . $link . '">Click here</a>';

                        $emailTemplate = \App\Model\Emailtemplate::where('templateKey',"subscription_reminder")->first();
                        if(!empty($emailTemplate))
                        {
                            $to = $customer->email;
                            $cc = $accountManager->email;
                            $sendMail = customhelper::SendMailWithCC($emailTemplate, $replace, $to, $cc);
                           
                        }
                    
                    
                    if($eachRecord->smsSentStatus == '0') {
                        $replace['[WEBSITE_LINK]'] = $link;
                        $smsTemplate = \App\Model\Smstemplate::where('templateKey', "subscription_reminder")->first();
                        if(!empty($smsTemplate))
                        {
                            $toMobile = trim($customer->isdCode.$customer->contactNumber);
                            $isSendMsg = customhelper::sendMSG($toMobile,$replace,$smsTemplate);                           
                        }
                    }
                }
                
                $this->info('Email sent');
            } else {
                $this->info('No scheduled notification left');
            }

        } catch (ProcessFailedException $exception) {
            $this->error('Email sent failed');
        }
    }
}
