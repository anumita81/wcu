<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Config;
use customhelper;
use DB;

class ScheduleNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ScheduleNotification:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Shipment and order scheduled notification';
    
    protected $process;
    public $fileName = "";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
           
            $emailScheduledNotificationData = \App\Model\Schedulenotification::whereRaw('date(scheduleDate)="'.date('Y-m-d').'"')->whereRaw('(emailSentStatus = "0" OR smsSentStatus="0")')->get();
            if($emailScheduledNotificationData->count() > 0) {
                
                foreach($emailScheduledNotificationData as $eachRecord) {
                    $orderId = '';
                    $customer = \App\Model\User::find($eachRecord->customerId);
                    
                    if($eachRecord->notificationType == 'S')
                    {
                        $shipmentId = \App\Model\Shipment::find($eachRecord->referenceId)->id;

                        $orderInfo = \App\Model\Order::where('shipmentId',$shipmentId)->first();
                        if(!empty($orderInfo))
                        {
                            $orderId = $orderInfo->orderNumber;
                        }
                    }
                    else
                    {
                        $orderInfo = \App\Model\Order::find($eachRecord->referenceId);
                        $shipmentId = $orderInfo->shipmentId;
                        $orderId = $orderInfo->orderNumber;
                    }
                    $link = Config::get('constants.frontendUrl');
        
                    $replace['[NAME]'] = $customer->firstName . " " . $customer->lastName;
                    $replace['[SHIPMENTID]'] = $shipmentId;
                    $replace['[ORDERID]'] = $orderId;
                    
                            
                    if($eachRecord->emailSentStatus == '0') {
                        $replace['[WEBSITE_LINK]'] = '<a href="' . $link . '">Click here</a>';
                        $emailTemplate = \App\Model\Emailtemplate::where('id',$eachRecord->emailTemplateId)->first();
                        if(!empty($emailTemplate))
                        {
                            $to = $customer->email;
                            $sendMail = customhelper::SendMail($emailTemplate, $replace, $to);
                            if($sendMail)
                            {
                                \App\Model\Schedulenotification::where('id',$eachRecord->id)->update(['emailSentStatus'=>'1']);
                            }
                        }
                    }
                    
                    if($eachRecord->smsSentStatus == '0') {
                        $replace['[WEBSITE_LINK]'] = $link;
                        $smsTemplate = \App\Model\Smstemplate::where('id', $eachRecord->smsTemplateId)->first();
                        if(!empty($smsTemplate))
                        {
                            $toMobile = trim($customer->isdCode.$customer->contactNumber);
                            $isSendMsg = customhelper::sendMSG($toMobile,$replace,$smsTemplate);
                            if($isSendMsg)
                            {
                                \App\Model\Schedulenotification::where('id',$eachRecord->id)->update(['smsSentStatus'=>'1']);
                            }
                        }
                    }
                }
                
                $this->info('Email sent');
            } else {
                $this->info('No scheduled notification left');
            }

        } catch (ProcessFailedException $exception) {
            $this->error('Email sent failed');
        }
    }
}
