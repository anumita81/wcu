<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Config;
use customhelper;
use Illuminate\Support\Facades\DB;

class Groupinvitation extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'groupinvitation:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Used to notify customers when they are invited for any group';
    protected $process;
    public $fileName = "";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        try {
            $pendigInvitationQry = \App\Model\Groupmembers::where("status","0")->where("deleted","0")->where("emailSent","0");
            $selectedIds = $pendigInvitationQry->pluck('id')->toArray();
            $pendigInvitationData = $pendigInvitationQry->get();
            if($pendigInvitationData->count() > 0) {
                $replace = array();
                \App\Model\Groupmembers::whereIn("id",$selectedIds)->update(["emailSent"=>"1"]);
                $emailTemplate = \App\Model\Emailtemplate::where('templateKey','group_invitation')->first();
                foreach($pendigInvitationData as $eachInvitationData) {
                    $groupDetails = \App\Model\Groupdetails::find($eachInvitationData->groupId);
                    $userDetails = \App\Model\User::find($eachInvitationData->userId);
                    $replace['[NAME]'] = $userDetails->firstName.' '.$userDetails->lastName;
                    $replace['[GROUP_NAME]'] = "<strong>".$groupDetails->groupName."</strong>";
                    $websiteLink = Config::get('constants.frontendUrl') . "my-group/group-info";
                    $replace['[WEBSITE_LINK]'] = ' <a href="'.$websiteLink.'">Click</a> ';
                    $to = $userDetails->email;
                    $sendMail = customhelper::SendMail($emailTemplate, $replace, $to);
                    \App\Model\Groupmembers::where("id",$eachInvitationData->id)->update(["emailSent"=>"2"]);
                }
                
                $this->info('Email sent');
                
            } else {
                $this->info('No scheduled notification left');
            }
        } catch (Exception $ex) {
            $this->error('Email sent failed');
            
        }
    }
}