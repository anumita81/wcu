<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Config;
use customhelper;
use Illuminate\Support\Facades\DB;

class QuickshipoutStatusNotification extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quickshipout:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Used to notify customers for their quick shipout shipments status change';
    protected $process;
    public $fileName = "";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        try {

            $quickShipout = \App\Model\Quickshipoutstatusnotification::where("emailSentStatus", "0")->orWhere("smsSentStatus", "0")->get();
            //print_r($quickShipout); die;
            $selectedIds = $quickShipout->pluck('id')->toArray();

            $quickShioutData = $quickShipout;

            $html = "";

            //print_r($quickShioutData); die;
            if (count($quickShioutData) > 0) {
                ///Update Statusnotification by 1 in emailsent and smssent
                \App\Model\Quickshipoutstatusnotification::whereIn("id", $selectedIds)->update(["emailSentStatus" => "1", "smsSentStatus" => "1"]);
                foreach ($quickShioutData as $eachShipout) {

                    $html = "";
                    $sms = "";
                    $userInfo = \App\Model\User::where('email', $eachShipout->useremail)->first();

                    $shipmentDeliveryStatus = \App\Model\Shipmentdelivery::deliveryStatusQuickShipout();
                    $shipemntDeliveryKeys = array_keys($shipmentDeliveryStatus);


                    if ($eachShipout->quickshipoutType == "0") {
                        $quickShipoutStatuslog = \App\Model\Quickshipoutstatuslog::where('commercialInvoiceId', $eachShipout['quickshipoutId'])->get();
                        $emailTemplateKey = "quickshipoutair_status_change";
                    }
                    if ($eachShipout->quickshipoutType == "1") {
                        $quickShipoutStatuslog = \App\Model\Quickshipoutoceanstatuslog::where('quickshipoutId', $eachShipout['quickshipoutId'])->get();
                        $emailTemplateKey = "quickshipoutocean_status_change";

                        $quickShipoutData = \App\Model\Quickshipoutocean::find($eachShipout->quickshipoutId);

                        $replace['[CONTAINERTRACKINGURL]'] = $quickShipoutData->containerTrackingUrl;
                        $replace['[CONTAINERNUMBER]'] = $quickShipoutData->containerNumber;
                        $replace['[NOOFBOXESSHIPPED]'] = $eachShipout->noofBoxesShipped;
                        $replace['[TOTALNOOFBOXES]'] = $eachShipout->totalNoofBoxes;
                    }


                    $statusdata = array();
                    $statusCompletedIndex = 0;
                    if ($quickShipoutStatuslog->count() > 0) {
                        foreach ($quickShipoutStatuslog as $eachStatus) {
                            $statusdata[] = array(
                                'status' => $shipmentDeliveryStatus[$eachStatus->status],
                                'date' => $eachStatus->updatedOn,
                                'completed' => true,
                                'visited' => true,
                            );
                            $statusCompletedIndex++;
                        }
                    }

                    for ($index = $statusCompletedIndex; $index < count($shipemntDeliveryKeys); $index++) {
                        $statusdata[] = array(
                            'status' => $shipmentDeliveryStatus[$shipemntDeliveryKeys[$index]],
                            'date' => '',
                            'completed' => FALSE,
                            'visited' => FALSE,
                        );
                    }


                    //print_r($statusdata); die;


                    $replace['[NAME]'] = $userInfo['firstName'] . " " . $userInfo['lastName'];

                    $replace['[SHIPMENTID]'] = $eachShipout->quickshipoutId;



                    $html = "<table width='100%' border='0' bgcolor='#ececec' cellspacing='0' cellpadding='0'>
                     <tr>
                     <td style='padding:0; margin:0; background:#ececec' bgcolor='#ececec' align='center' valign='top'>
                     <table width='600' border='0' cellspacing='0' cellpadding='0' align='center' class='tableWrap' bgcolor='#ffffff' style='background:#ffffff;'>
                   
                     <tr>
                        <td align='center' valign='top' style='padding-left: 15px; padding-right: 15px; padding-top: 0; padding-bottom: 0; margin: 0;'>
                            <table width='100%' border='0' cellspacing='0' cellpadding='0' class='tracker'><tr>";


                    foreach ($statusdata as $statusname) {
                        if ($statusname['visited'] == true) {
                            $visited = "<span style='width: 16px; height: 16px; box-sizing: border-box; border: 2px solid #43a047; border-radius: 100%; display: inline-block; background: #43a047; position: relative; z-index: 1; text-align: center;'></span>";
                        } else {
                            $visited = "<span style='width: 16px; height: 16px; box-sizing: border-box; border: 2px solid #c9c9c9; border-radius: 100%; display: inline-block; background: #ffffff; position: relative; z-index: 1; text-align: center;'></span>";
                        }
                        $html .= "<td align='left' valign='top' class='tracker-item' width='25%'
                                        style='padding: 0; margin: 0;'>
                                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                                            <tr>
                                                <td align='left' valign='top' height='20' style='padding: 0; margin: 0;'>
                                                   " . $visited . " 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align='left' valign='top'
                                                    style='padding: 0; margin: 0; font-size: 13px; line-height: 1.4; font-weight: 500; color: #323232; font-family: Roboto, sans-serif;'>
                                                    " . $statusname['status'] . "</td>
                                            </tr>
                                            <tr>
                                                <td align='left' valign='top'
                                                    style='padding: 0; margin: 0; font-size: 13px; line-height: 1.4; font-weight: 400; color: #c9c9c9; font-family: Roboto, sans-serif;'>
                                                    " . $statusname['date'] . "</td>
                                            </tr>
                                        </table>
                                    </td>";
                        $sms.=$statusname['status'] . ",";
                    }



                    $html .= "</tr></table>
                        </td>
                    </tr>
                   
                </table>
            </td>
        </tr>
    </table>";

                    ///////////////////////////////////////replace by html/////////////////////////////////////////////// 
                    $replace['[QSTATUS]'] = $html;

                    $emailTemplate = \App\Model\Emailtemplate::where('templateKey', $emailTemplateKey)->first();
                    $to = $userInfo->email; //"anumita.banerjee@indusnet.co.in";

                    $sendMail = customhelper::SendMail($emailTemplate, $replace, $to);

                    $smsTemplate = \App\Model\Smstemplate::where('templateKey', $emailTemplateKey)->first();
                    $replace['[QSTATUS]'] = $sms;
                    $toMobile = trim($userInfo->isdCode . $userInfo->contactNumber);
                    $isSendMsg = customhelper::sendMSG($toMobile, $replace, $smsTemplate);

                    if ($sendMail || $isSendMsg) {
                        \App\Model\Quickshipoutstatusnotification::where("id", $eachShipout->id)->update(["emailSentStatus" => "2", "smsSentStatus" => "2", "emailSentOn" => date("Y-m-d H:i:s"), "smssentOn" => date("Y-m-d H:i:s")]);
                    }

                    $replace['[SHIPMENT_STATUS]'] = $shipmentDeliveryStatus[$eachStatus->status];
                    \App\Model\User::sendPushNotification($userInfo->id, 'quickshipout_status_change', '1', $replace);
                }

                $this->info('Email sent');
            } else {
                $this->info('No upcomming notifications exist');
            }
        } catch (ProcessFailedException $exception) {
            $this->error('Email sent failed');
        }
    }

}
