<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Config;
use customhelper;
use Illuminate\Support\Facades\DB;

class QuickshipoutUserNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quickshipout:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Used to notify customers for their quick shipout shipments';
    
    protected $process;
    public $fileName = "";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            
            $quickShipout = \App\Model\Quickshipoutnotification::where("emailSent","0")->orWhere("smsSent","0")->get();
            if(!empty($quickShipout)) {
                foreach($quickShipout as $eachShipout)
                {
                    $emailSent = "1";
                    $smsSent = "1";
                    if($eachShipout->emailSent !="0")
                        $emailSent = $eachShipout->emailSent;
                    if($eachShipout->smsSent !="0")
                        $smsSent = $eachShipout->smsSent;
                    \App\Model\Quickshipoutnotification::where("id",$eachShipout->id)->update(["smsSent"=>$smsSent,"emailSent"=>$emailSent]);
                }
                
                $processedNotofications = \App\Model\Quickshipoutnotification::where("emailSent","1")->orWhere("smsSent","1")->get();
                if(!empty($processedNotofications))
                {
                    foreach($processedNotofications as $eachNotification)
                    {
                        $userInfo = \App\Model\User::find($eachNotification->userId);

                        if($eachNotification->quickshipoutType == 1)
                        {
                            $quickShipoutOceanInfo = \App\Model\Quickshipoutocean::find($eachNotification->quickshipoutId);

                            $quickShipoutInvoicedetails = json_decode($quickShipoutOceanInfo->invoiceDetails);

                            $invoicePackage = $quickShipoutInvoicedetails[0];

                            foreach($invoicePackage->packages as $package)
                            {
                                if($package->unit == $eachNotification->unitNumber)
                                {
                                    $replace['[ORDER_NUMBER]'] = $package->orderNumber;
                                }
                            }
                        }
                        
                        $replace['[NAME]'] = $userInfo->firstName . " " . $userInfo->lastName;

                        if($eachNotification->quickshipoutType == 0)
                        {
                            $replace['[STORE_NAME]'] = $eachNotification->storeName;
                            $replace['[TRACKING_NUMBER]'] = $eachNotification->trackingNumber;
                           
                        }

                        $replace['[ITEM_QTY]'] = $eachNotification->itemQty;
                        $replace['[PRODUCT_NAME]'] = $eachNotification->productName;

                        $shipmentType = strtolower($eachNotification->shipmentType);
                        if($shipmentType=='packed shipments')
                        {
                            $shipmentType = "packed";
                        }
                        $templateKey = $shipmentType."_quickshipout_customer_notification";
            
                        if($eachNotification->emailSent == "1")
                        {
                            $emailTemplate = \App\Model\Emailtemplate::where('templateKey',$templateKey)->first();
                            $to = $userInfo->email;
                            $sendMail = customhelper::SendMail($emailTemplate, $replace, $to);
                            if($sendMail)
                                \App\Model\Quickshipoutnotification::where("id",$eachNotification->id)->update(["emailSent"=>"2","emailSentOn"=>date("Y-m-d H:i:s")]);
                        }
                        if($eachNotification->smsSent == "1")
                        {
                            $smsTemplate = \App\Model\Smstemplate::where('templateKey',$templateKey)->first();
                            $toMobile = trim($userInfo->isdCode.$userInfo->contactNumber);
                            $isSendMsg = customhelper::sendMSG($toMobile,$replace,$smsTemplate);
                            if($isSendMsg)
                                \App\Model\Quickshipoutnotification::where("id",$eachNotification->id)->update(["smsSent"=>"2","smsSentOn"=>date("Y-m-d H:i:s")]);
                        }
                        
                        $replace['[QUICKSHIPOUT_TYPE]'] = $eachNotification->shipmentType;
                        \App\Model\User::sendPushNotification($userInfo->id, 'quickshipout_notification', '1', $replace);
                    }
                }
            
            $this->info('Email sent');
            } else {
                $this->info('No upcoming notifications exist');
            }

        } catch (ProcessFailedException $exception) {
            $this->error('Email sent failed');
        }
    }
}
