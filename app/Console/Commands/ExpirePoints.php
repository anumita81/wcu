<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExpirePoints extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ExpirePoints:expirepoints';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Points expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        //
        $getPointTransaction = \App\Model\Fundpointtransaction::where('expiredDate', '<', date('Y-m-d'))->where('type', 'A')->get();

        foreach ($getPointTransaction as $key => $val) {
            /* UPDATE FUND POINT TRANSACTION */
            $fpt = \App\Model\Fundpointtransaction::find($val['id']);
            $fpt->type = 'E';
            $fpt->save();

            /* UPDATE MAIN FUND POINT TABLE */
            $getMainPoint = \App\Model\Fundpoint::where('userId', $val['userId'])->first();

            $fp = \App\Model\Fundpoint::find($getMainPoint->id);
            $new_point = ( $fp->point - $fpt->point);
            $fp->point = $new_point;
            $fp->save();

            \App\Model\User::sendPushNotification($val['userId'], 'fund_point_expiry', '1');
        }

        $this->info('Cron End');
    }

}
