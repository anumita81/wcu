<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Procurementitem extends Model {

    public $table;
    protected $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.PROCUREMENTITEM');
        $this->prefix = DB::getTablePrefix();
    }

    public function deliverycompany() {
        return $this->hasOne('App\Model\Deliverycompany', 'id', 'deliveryCompanyId');
    }

    public static function allStatus() {
        return array('submitted' => 'Submitted', 'orderplaced' => 'Order Placed', 'pickedup' => 'Picked Up', 'received' => 'Received', 'unavailable' => 'Unavailable');
    }

    /**
     * Method used to fetch procurement item details
     * @param integer $id
     * @return object
     */
    public static function getItemDetails($id) {
        $procurement = new Procurement;
        $procurementItem = new Procurementitem;
        $category = new Sitecategory;
        $subcategory = new Sitecategory;
        $product = new Siteproduct;
        $store = new Stores;

        $procurementItemTable = $procurementItem->prefix . $procurementItem->table;
        $procurementTable = $procurementItem->prefix . $procurement->table;

        $where = "procurementType='shopforme' AND $procurementItemTable.deleted= '0' AND $procurementTable.deleted= '0'";

        $resultset = Procurementitem::select(array("$procurementItem->table.id", "$procurementItem->table.itemImage", "$procurementItem->table.siteCategoryId", "$procurementItem->table.siteSubCategoryId", "$procurementItem->table.siteProductId", "$procurementItem->table.websiteUrl",
                    "$procurementItem->table.itemName", "$procurementItem->table.options", "$procurementItem->table.itemPrice", "$procurementItem->table.itemQuantity", "$procurementItem->table.itemShippingCost",
                    "$procurementItem->table.itemTotalCost", "$procurementItem->table.deliveryCompanyId", "$procurementItem->table.trackingNumber", "$procurementItem->table.receivedDate", "$procurementItem->table.status",
                    "$category->table.categoryName", "SC.categoryName as subcategoryName", "$product->table.productName", "$product->table.weight", "$store->table.storeName", "$procurement->table.procurementLocked"
                ))
                ->join($procurement->table, "$procurementItem->table.procurementId", '=', "$procurement->table.id")
                ->leftJoin($category->table, "$procurementItem->table.siteCategoryId", '=', "$category->table.id")
                ->leftJoin($subcategory->table . " AS SC", "$procurementItem->table.siteSubCategoryId", '=', "SC.id")
                ->leftJoin($product->table, "$procurementItem->table.siteProductId", '=', "$product->table.id")
                ->leftJoin($store->table, "$procurementItem->table.storeId", '=', "$store->table.id")
                ->whereRaw($where)
                ->where("$procurementItem->table.procurementId", $id)
                ->orderBy("$procurementItem->table.id", "ASC")
                ->get();

        return $resultset;
    }
    
    /**
     * Method used to fetch procurement item details
     * @param integer $id
     * @return object
     */
    public static function getSelectedItemDetails($id,$procurementItemIds = '-1') {
        $procurement = new Procurement;
        $procurementItem = new Procurementitem;
        $category = new Sitecategory;
        $subcategory = new Sitecategory;
        $product = new Siteproduct;
        $store = new Stores;

        $procurementItemTable = $procurementItem->prefix . $procurementItem->table;
        $procurementTable = $procurementItem->prefix . $procurement->table;

        $where = "procurementType='shopforme' AND $procurementItemTable.deleted= '0' AND $procurementTable.deleted= '0'";

        $resultset = Procurementitem::select(array("$procurementItem->table.id", "$procurementItem->table.itemImage", "$procurementItem->table.siteCategoryId", "$procurementItem->table.siteSubCategoryId", "$procurementItem->table.siteProductId", "$procurementItem->table.websiteUrl",
                    "$procurementItem->table.itemName", "$procurementItem->table.options", "$procurementItem->table.itemPrice", "$procurementItem->table.itemQuantity", "$procurementItem->table.itemShippingCost",
                    "$procurementItem->table.itemTotalCost", "$procurementItem->table.deliveryCompanyId", "$procurementItem->table.trackingNumber", "$procurementItem->table.receivedDate", "$procurementItem->table.status",
                    "$category->table.categoryName", "SC.categoryName as subcategoryName", "$product->table.productName", "$product->table.weight", "$store->table.storeName", "$procurement->table.procurementLocked"
                ))
                ->join($procurement->table, "$procurementItem->table.procurementId", '=', "$procurement->table.id")
                ->leftJoin($category->table, "$procurementItem->table.siteCategoryId", '=', "$category->table.id")
                ->leftJoin($subcategory->table . " AS SC", "$procurementItem->table.siteSubCategoryId", '=', "SC.id")
                ->leftJoin($product->table, "$procurementItem->table.siteProductId", '=', "$product->table.id")
                ->leftJoin($store->table, "$procurementItem->table.storeId", '=', "$store->table.id")
                ->whereRaw($where)
                ->where("$procurementItem->table.procurementId", $id)
                ->whereIn("$procurementItem->table.id", explode('^',$procurementItemIds))
                ->orderBy("$procurementItem->table.id", "ASC")
                ->get();

        return $resultset;
    }

    /**
     * Method used to calculate item weight
     * @param integer $id
     * @return integer
     */
    public static function calculateItemWeight($id) {
        $procurementItem = new Procurementitem;
        $product = new Siteproduct;

        $procurementItemTable = $procurementItem->prefix . $procurementItem->table;
        $productTable = $procurementItem->prefix . $product->table;

        $resultset = Procurementitem::select(DB::raw("sum($procurementItemTable.itemQuantity*$productTable.weight) AS totalWeight"), DB::raw("sum($procurementItemTable.itemQuantity) AS totalQuantity"))
                ->leftJoin($product->table, "$procurementItem->table.siteProductId", '=', "$product->table.id")
                ->where('procurementId', $id)
                ->where('deleted', '0')
                ->where('siteProductId', '>', 0)
                ->first();

        return $resultset;
    }

    public static function checkstatus($id) {
        $where = "deleted= '0' AND status != 'received' AND status != 'unavailable'";

        $resultset = Procurementitem::whereRaw($where)
                ->where('procurementId', $id)
                ->count('id');

        return $resultset;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifier) {
        if (empty($id))
            return false;
        //DB::enableQueryLog();

        $row = false;

        $row = Procurementitem::where('id', $id)
                ->update(array('deleted' => '1', 'deletedBy' => $createrModifier));

        //dd(DB::getQueryLog());

        return $row;
    }

    /**
     * Method used to fetch procurement item details
     * @param integer $id
     * @return object
     */
    public static function getCarDetails($id) {
        $procurement = new Procurement;
        $procurementItem = new Procurementitem;
        $make = new Automake;
        $model = new Automodel;
        $autoWebsite = new Autowebsite;
        $state = new State;
        $city = new City;

        $procurementItemTable = $procurementItem->prefix . $procurementItem->table;
        $procurementTable = $procurementItem->prefix . $procurement->table;

        $where = "$procurementTable.id = $id  AND $procurementTable.procurementType='buycarforme' AND $procurementItemTable.deleted= '0' AND $procurementTable.deleted= '0'";

        $resultset = Procurementitem::select(array("$procurementItem->table.*", "$make->table.name AS makeName", "$model->table.name AS modelName", "$autoWebsite->table.name AS websiteName", "$state->table.name AS stateName", "$city->table.name AS cityName"
                ))
                ->join($procurement->table, "$procurementItem->table.procurementId", '=', "$procurement->table.id")
                ->leftJoin($make->table, "$procurementItem->table.makeId", '=', "$make->table.id")
                ->leftJoin($model->table, "$procurementItem->table.modelId", '=', "$model->table.id")
                ->leftJoin($autoWebsite->table, "$procurementItem->table.carwebsiteId", '=', "$autoWebsite->table.id")
                ->leftJoin($state->table, "$state->table.id", '=', "$procurementItem->table.pickupStateId")
                ->leftJoin($city->table, "$city->table.id", '=', "$procurementItem->table.pickupCityId")
                ->where($make->table . '.type', '0')
                ->where($model->table . '.makeType', '0')
                ->whereRaw($where)
                ->get();

        return $resultset;
    }

    /**
     * Method used to fetch Auto Parts details
     * @param procurement Id as $id
     * @return procurement Item(Auto Parts) details in $resultset
     */
    public static function getAutoPartsItemDetails($id) {
        $procurement = new Procurement;
        $procurementItem = new Procurementitem;
        $make = new Automake;
        $model = new Automodel;
        $category = new Sitecategory;
        $subcategory = new Sitecategory;
        $product = new Siteproduct;
        $store = new Stores;

        $procurementItemTable = $procurementItem->prefix . $procurementItem->table;
        $procurementTable = $procurementItem->prefix . $procurement->table;

        $where = "$procurementTable.id = $id  AND $procurementTable.procurementType='autopart' AND $procurementItemTable.deleted= '0' AND $procurementTable.deleted= '0'";

        $resultset = Procurementitem::select(array("$procurementItem->table.*", "$procurement->table.shippingMethod", "$procurement->table.shippingMethodId", "$procurement->table.paymentStatus", "$procurement->table.toZipCode", "$procurement->table.toAddress", "$procurement->table.warehouseId", "$procurement->table.totalCost", "$procurement->table.totalTax", "$procurement->table.totalInsurance", "$procurement->table.totalClearingDuty", "$procurement->table.isDutyCharged", "$procurement->table.urgent", "$procurement->table.fromName", "$procurement->table.fromEmail", "$procurement->table.fromPhone", "$procurement->table.fromCountry", "$procurement->table.fromState", "$procurement->table.fromCity", "$procurement->table.fromAddress", "$procurement->table.fromZipCode", "$procurement->table.fromName", "$procurement->table.toName", "$procurement->table.toEmail", "$procurement->table.toPhone", "$procurement->table.toCountry", "$procurement->table.toState", "$procurement->table.toCity", "$procurement->table.estimateDeliveryDate", "$procurement->table.deliveredOn", "$procurement->table.createdOn", "$procurement->table.totalWeight", "$procurement->table.totalProcurementCost", "$procurement->table.totalItemCost", "$procurement->table.totalProcessingFee", "$procurement->table.urgentPurchaseCost", "$procurement->table.procurementLocked", "$procurement->table.shippingMethod", "$procurement->table.status AS procurementStatus", "$procurement->table.procurementLocked", "$procurement->table.fromCountry", "$procurement->table.fromState", "$procurement->table.fromCity", "$procurement->table.toCountry", "$procurement->table.toState", "$procurement->table.toCity", "$procurement->table.totalWeight", "$procurement->table.totalProcurementCost", "$procurement->table.totalItemCost", "$procurement->table.totalProcessingFee", "$procurement->table.urgentPurchaseCost", "$category->table.categoryName",
                    "SC.categoryName as subcategoryName", "$product->table.productName", "$store->table.storeName"))
                ->join($procurement->table, "$procurementItem->table.procurementId", '=', "$procurement->table.id")
                ->leftJoin($make->table, "$procurementItem->table.makeId", '=', "$make->table.id")
                ->leftJoin($model->table, "$procurementItem->table.modelId", '=', "$model->table.id")
                ->leftJoin($category->table, "$procurementItem->table.siteCategoryId", '=', "$category->table.id")
                ->leftJoin($subcategory->table . " AS SC", "$procurementItem->table.siteSubCategoryId", '=', "SC.id")
                ->leftJoin($product->table, "$procurementItem->table.siteProductId", '=', "$product->table.id")
                ->leftJoin($store->table, "$procurementItem->table.storeId", '=', "$store->table.id")
                ->whereRaw($where)
                ->where("$procurementItem->table.procurementId", $id)
                ->orderBy("$procurementItem->table.id", "ASC")
                ->get();


        return $resultset;
    }
/**
     * Method used to fetch procurement item details
     * @param integer $id
     * @return object
     */
    public static function getSelectedAutoPartsItemDetails($id,$procurementItemIds = '-1') {
        $procurement = new Procurement;
        $procurementItem = new Procurementitem;
        $category = new Sitecategory;
        $subcategory = new Sitecategory;
        $product = new Siteproduct;
        $store = new Stores;

        $procurementItemTable = $procurementItem->prefix . $procurementItem->table;
        $procurementTable = $procurementItem->prefix . $procurement->table;

       $where = "$procurementTable.id = $id  AND $procurementTable.procurementType='autopart' AND $procurementItemTable.deleted= '0' AND $procurementTable.deleted= '0'";

        $resultset = Procurementitem::select(array("$procurementItem->table.*", "$procurement->table.shippingMethod", "$procurement->table.shippingMethodId", "$procurement->table.paymentStatus", "$procurement->table.toZipCode", "$procurement->table.toAddress", "$procurement->table.warehouseId", "$procurement->table.totalCost", "$procurement->table.totalTax", "$procurement->table.totalInsurance", "$procurement->table.totalClearingDuty", "$procurement->table.isDutyCharged", "$procurement->table.urgent", "$procurement->table.fromName", "$procurement->table.fromEmail", "$procurement->table.fromPhone", "$procurement->table.fromCountry", "$procurement->table.fromState", "$procurement->table.fromCity", "$procurement->table.fromAddress", "$procurement->table.fromZipCode", "$procurement->table.fromName", "$procurement->table.toName", "$procurement->table.toEmail", "$procurement->table.toPhone", "$procurement->table.toCountry", "$procurement->table.toState", "$procurement->table.toCity", "$procurement->table.estimateDeliveryDate", "$procurement->table.deliveredOn", "$procurement->table.createdOn", "$procurement->table.totalWeight", "$procurement->table.totalProcurementCost", "$procurement->table.totalItemCost", "$procurement->table.totalProcessingFee", "$procurement->table.urgentPurchaseCost", "$procurement->table.procurementLocked", "$procurement->table.shippingMethod", "$procurement->table.status AS procurementStatus", "$procurement->table.procurementLocked", "$procurement->table.fromCountry", "$procurement->table.fromState", "$procurement->table.fromCity", "$procurement->table.toCountry", "$procurement->table.toState", "$procurement->table.toCity", "$procurement->table.totalWeight", "$procurement->table.totalProcurementCost", "$procurement->table.totalItemCost", "$procurement->table.totalProcessingFee", "$procurement->table.urgentPurchaseCost", "$category->table.categoryName",
                    "SC.categoryName as subcategoryName", "$product->table.productName", "$store->table.storeName"))
                ->join($procurement->table, "$procurementItem->table.procurementId", '=', "$procurement->table.id")
                ->leftJoin($category->table, "$procurementItem->table.siteCategoryId", '=', "$category->table.id")
                ->leftJoin($subcategory->table . " AS SC", "$procurementItem->table.siteSubCategoryId", '=', "SC.id")
                ->leftJoin($product->table, "$procurementItem->table.siteProductId", '=', "$product->table.id")
                ->leftJoin($store->table, "$procurementItem->table.storeId", '=', "$store->table.id")
                ->whereRaw($where)
                ->where("$procurementItem->table.procurementId", $id)
                ->whereIn("$procurementItem->table.id", explode('^',$procurementItemIds))
                ->orderBy("$procurementItem->table.id", "ASC")
                ->get();

        return $resultset;
    }

    /**
     * Method used to fetch procurement item details
     * @param integer $id
     * @return object
     */
    public static function getOtherVehicleItemDetails($id) {
        $procurement = new Procurement;
        $procurementItem = new Procurementitem;
        $make = new Automake;
        $model = new Automodel;

        $procurementItemTable = $procurementItem->prefix . $procurementItem->table;
        $procurementTable = $procurementItem->prefix . $procurement->table;

        $where = "$procurementTable.id = $id  AND $procurementTable.procurementType='othervehicle' AND $procurementItemTable.deleted= '0' AND $procurementTable.deleted= '0'";

        $resultset = Procurementitem::select(array("$procurementItem->table.*", "$make->table.name AS makeName", "$model->table.name AS modelName", "$procurement->table.procurementLocked", "$procurement->table.fromCountry", "$procurement->table.fromState", "$procurement->table.fromCity", "$procurement->table.toCountry", "$procurement->table.toState", "$procurement->table.toCity", "$procurement->table.totalWeight", "$procurement->table.totalProcurementCost", "$procurement->table.totalItemCost", "$procurement->table.totalProcessingFee", "$procurement->table.urgentPurchaseCost",))
                ->join($procurement->table, "$procurementItem->table.procurementId", '=', "$procurement->table.id")
                ->leftJoin($make->table, "$procurementItem->table.makeId", '=', "$make->table.id")
                ->leftJoin($model->table, "$procurementItem->table.modelId", '=', "$model->table.id")
                ->whereRaw($where)
                ->where("$procurementItem->table.procurementId", $id)
                ->orderBy("$procurementItem->table.id", "ASC")
                ->get();


        return $resultset;
    }

    public static function fetchUsedFieldIds($field = '') {
        $resultset = Procurementitem::distinct()->whereNotNull($field)->where("deleted", "0")->pluck($field)->toArray();

        return $resultset;
    }

}
