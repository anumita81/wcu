<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Autowarehouseimage extends Model {

    public $table;
    public $timestamps = false;
    public $prefix; 
    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.AUTOWAREHOUSEIMAGE');
        $this->prefix = DB::getTablePrefix();
    }
    
    public static function getAllImages($shipmentId,$fromPage = 'web') {
        
        $imageArr = array();
        
        $allData = Autowarehouseimage::where('shipmentId',$shipmentId)->get();
        if(!empty($allData)) {
            foreach($allData as $eachData) {
                if($fromPage == 'api')
                    $imageArr[$eachData->imageSection]['imagePath'] = url('uploads/auto/carimages/thumb/'.$eachData->imagePath);
                else if($fromPage == 'mobile') {
                    $imageArr[$eachData->imageSection]['imagePath'] = url('uploads/auto/carimages/thumb/'.$eachData->imagePath);
                    $imageArr[$eachData->imageSection]['actualImagePath'] = url("uploads/auto/carimages/".$shipmentId."/".$eachData->imagePath);
                }
                else
                    $imageArr[$eachData->imageSection]['imagePath'] = $eachData->imagePath;
                $imageArr[$eachData->imageSection]['id'] = $eachData->id;
            }
        }
        
        return $imageArr;
    }

}
