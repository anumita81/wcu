<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Unidentifieditemdetails extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.UNIDENTIFIEDITEMDETAILS');
        $this->prefix = DB::getTablePrefix();
    }
    
    public static function getItemlList($param) {
        
        
        $unidentifiedItem = new Unidentifieditemdetails;
        $unidentifiedItemNote = new Unidentifiednotes;
        $unidentifiedItemTable = $unidentifiedItem->prefix.$unidentifiedItem->table;
        $unidentifiedItemNoteTable = $unidentifiedItemNote->prefix.$unidentifiedItemNote->table;
        
        $where = "$unidentifiedItemTable.type='unidentified' AND deleted='0'";
        if(!empty($param['searchUnidentified']['trackingNumber']))
        {
            $where .= " AND (trackingNumber='".$param['searchUnidentified']['trackingNumber']."' OR trackingNumber2='".$param['searchUnidentified']['trackingNumber']."')";
        }
        
        if(!empty($param['searchUnidentified']['store']))
        {
            $where .= " AND store='".$param['searchUnidentified']['store']."'";
        }
        
        if(!empty($param['searchUnidentified']['unitNumber']))
        {
            $where .= " AND unitOnPackage =  '".$param['searchUnidentified']['unitNumber']."'";
        }
        
        if(!empty($param['searchUnidentified']['deliveryCompany']))
        {
            $where .= " AND deliveryCompany='".$param['searchUnidentified']['deliveryCompany']."'";
        }

        if(!empty($param['searchUnidentified']['status']))
        {
            $where .= " AND status='".$param['searchUnidentified']['status']."'";
        }
        
        if(!empty($param['searchUnidentified']['id']))
        {
            $where .= " AND $unidentifiedItemTable.id='".$param['searchUnidentified']['id']."'";
        }
        
        
        if ($param['searchByCreatedOn'] != '') {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($unidentifiedItemTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $unidentifiedItemTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($unidentifiedItemTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($unidentifiedItemTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }
        
        $resultSet = Unidentifieditemdetails::select(array("$unidentifiedItem->table.id as itemId","$unidentifiedItem->table.*"))->whereRaw($where)
                ->leftJoin($unidentifiedItemNote->table,"$unidentifiedItemNote->table.itemId","=","$unidentifiedItem->table.id")
                ->orderBy($param['field'], $param['type'])
                ->groupBy("$unidentifiedItem->table.id")
                ->paginate($param['searchDisplay']);
        
        return $resultSet;
    }
    
    public static function getFraudList($param) {
        
        
        $unidentifiedItem = new Unidentifieditemdetails;
        $unidentifiedItemNote = new Unidentifiednotes;
        $unidentifiedItemTable = $unidentifiedItem->prefix.$unidentifiedItem->table;
        $unidentifiedItemNoteTable = $unidentifiedItemNote->prefix.$unidentifiedItemNote->table;
        
        $where = "$unidentifiedItemTable.type='fraud' AND deleted='0'";
        if(!empty($param['searchUnidentified']['trackingNumber']))
        {
            $where .= " AND (trackingNumber='".$param['searchUnidentified']['trackingNumber']."' OR trackingNumber2='".$param['searchUnidentified']['trackingNumber']."')";
        }
        
        if(!empty($param['searchUnidentified']['store']))
        {
            $where .= " AND store='".$param['searchUnidentified']['store']."'";
        }
        
        if(!empty($param['searchUnidentified']['unitNumber']))
        {
            $where .= " AND unitOnPackage =  '".$param['searchUnidentified']['unitNumber']."'";
        }
        
        if(!empty($param['searchUnidentified']['unitNumber']))
        {
            $where .= " AND type='resolvenote' AND note like '%".$param['searchUnidentified']['unitNumber']."%'";
        }
        
        if(!empty($param['searchUnidentified']['deliveryCompany']))
        {
            $where .= " AND deliveryCompany='".$param['searchUnidentified']['deliveryCompany']."'";
        }

        if(!empty($param['searchUnidentified']['status']))
        {
            $where .= " AND status='".$param['searchUnidentified']['status']."'";
        }
        
          
        if(!empty($param['searchUnidentified']['id']))
        {
            $where .= " AND $unidentifiedItemTable.id='".$param['searchUnidentified']['id']."'";
        }
        
        
        
        if ($param['searchByCreatedOn'] != '') {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($unidentifiedItemTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $unidentifiedItemTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($unidentifiedItemTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($unidentifiedItemTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }
        

        $resultSet = Unidentifieditemdetails::select(array('*','id as itemId'))->whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->groupBy("$unidentifiedItem->table.id")
                ->paginate($param['searchDisplay']);
        
        return $resultSet;
    }

}
