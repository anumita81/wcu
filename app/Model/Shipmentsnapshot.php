<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Config;

class Shipmentsnapshot extends Model
{
    public $table;
    public $timestamps = false;

    public function __construct() {

        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.SHIPMENTSNAPSHOT');
    }
    
    public static function deliverywiseSnapshot($shipmentId) {
        
        $return = array();
        $snapshotCount = Shipmentsnapshot::selectRaw('count(id) as snapshotCount,deliveryId')->where('shipmentId',$shipmentId)->groupBy('deliveryId')->get();

        if($snapshotCount->count()>0)
        {
            foreach($snapshotCount as $eachSnapshot)
            {
                $return[$eachSnapshot->deliveryId] = $eachSnapshot->snapshotCount; 
            }
        }
        
        return $return;
        
    }
}