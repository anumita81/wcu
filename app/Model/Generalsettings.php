<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Generalsettings extends Model {

    public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.SETTINGS');
    }

    /**
     * Method used to fetch settings data
     * @param string $group
     * @return object
     */
    public static function getSettingsData($group = 'siteSettings') {
        $data = array();

        $where = "groupName='" . $group . "'";

        $resultSet = Generalsettings::whereRaw($where)
                ->select('*')
                ->get();

        return $resultSet;
    }

}
