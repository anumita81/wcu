<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Shippingsettings extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $methodTable;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.SETTINGS');
        $this->methodTable = Config::get('constants.dbTable.SHIPPINGMETHOD');
    }

    public $timestamps = false;

    public static function getSettingsData() {
        $data = array();

        $where = "groupName='shipmentSettings'";

        $resultSet = Shippingsettings::whereRaw($where)
                ->select('*')
                ->get();

        return $resultSet;
    }

    public static function getShippingMethodData($param) {
        $resultSet = DB::table('shippingmethods')
                ->select('*')
                ->where('deleted', '0')
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = DB::table('shippingmethods')->where('shippingid', $id)
                ->update(array('active' => $newStatus));

        return $row;
    }

    public static function findData($id) {

        $resultSet = DB::table('shippingmethods')
                        ->select('*')
                        ->where('shippingid', $id)->first();

        return $resultSet;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = DB::table('shippingmethods')->where('shippingid', $id)
                ->update(array('active'=>'N','deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    public static function saveRecord($recordsArray, $id = '') {
        $row = false;
        if (empty($id)) {
            $row = DB::table('shippingmethods')->insert($recordsArray);
        } else {
            $row = DB::table('shippingmethods')->where('shippingid', $id)
                    ->update($recordsArray);
        }

        return $row;
    }

    public static function getFillShipSettingsData() {
        $data = array();

        $where = "groupName='FillandShip'";

        $resultSet = Shippingsettings::whereRaw($where)
                ->select('*')
                ->get();

        return $resultSet;
    }

}
