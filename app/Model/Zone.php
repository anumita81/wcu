<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

//use App\libraries\helpers;
//use App\libraries\dbHelpers;

class Zone extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.ZONE');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to fetch Zone list
     * @param array $paramCountry
     * @return object
     */
    public static function getZoneList($param) {
        $where = '1';
        
        $resultSet = Zone::whereRaw($where)
                ->select(array('id', 'name', 'status'))
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }


    /**
     * Method used to change State status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Zone::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /*
    * $addrInfo = array('country'=>$countryid,'state'=>stateid,'city'=>$cityid)
    */
    public static function getZoneWithAddrOld($addrInfo) {

        $zoneCountry = new Zonecountry;
        $zoneState = new Zonestate;
        $zone = new Zone;
        $zoneCountryTab = $zoneCountry->table;
        $zoneTab = $zone->table;
        $zonestateTab = $zoneState->table;
        $returnZones = array();
        $zoneCoutryData = Zone::select("$zoneTab.id")
                      ->join("$zoneCountryTab","$zoneCountryTab.zoneId","=","$zoneTab.id")
                      ->where("status","1")
                      ->where("countryId",$addrInfo['country'])->orderBy('id','asc')->get()->toArray();

        if(!empty($zoneCoutryData)) {

            foreach($zoneCoutryData as $eachcountryZone)
            {
                $ifStateExist = Zonestate::select('zoneId',\DB::raw('count(zoneId) as numCountry'))
                            ->groupBy('zoneId')
                            ->havingRaw('zoneId = '.$eachcountryZone['id'])
                            ->get()->toArray();

                if(empty($ifStateExist))
                {
                  $returnZones[] = $eachcountryZone['id'];
                  continue;
                }            
                $zoneStateData = Zone::select("$zoneTab.id")
                      ->join("$zonestateTab","$zonestateTab.zoneId","=","$zoneTab.id")
                      ->where("status","1")
                      ->where("stateId",$addrInfo['state'])
                      ->where("$zonestateTab.zoneId",$eachcountryZone)->get()->toArray();
                if(!empty($zoneStateData)) {
                    foreach($zoneStateData as $eachStateZone) {
                        $returnZones[] = $eachStateZone['id'];
                    }
                }
            }
        }

        return $returnZones;             
        
    }
    
    /*
    * $addrInfo = array('country'=>$countryid,'state'=>stateid,'city'=>$cityid)
    */
    public static function getZoneWithAddr($addrInfo) {
        $zoneCountry = new Zonecountry;
        $zoneState = new Zonestate;
        $zoneCity = new Zonecity;
        $zone = new Zone;
        $zoneCountryTab = $zoneCountry->table;
        $zoneTab = $zone->table;
        $zonestateTab = $zoneState->table;
        $zonecityTab = $zoneCity->table;
        $returnZones = array();
        $zoneCoutryStateCityData = Zone::select("$zoneTab.id")
                      ->join("$zoneCountryTab","$zoneCountryTab.zoneId","=","$zoneTab.id")
                      ->join("$zonestateTab","$zonestateTab.zoneId","=","$zoneTab.id")
                      ->join("$zonecityTab","$zonecityTab.zoneId","=","$zoneTab.id")
                      ->where("status","1")
                      ->where("countryId",$addrInfo['country'])
                      ->where("stateId",$addrInfo['state'])
                      ->where("cityId",$addrInfo['city'])->orderBy('id','asc')->first();
        if(!empty($zoneCoutryStateCityData))
        {
            $returnZones[] = $zoneCoutryStateCityData->id;
        }
        else
        {
            $zoneCoutryStateData = Zone::select("$zoneTab.id")
                      ->join("$zoneCountryTab","$zoneCountryTab.zoneId","=","$zoneTab.id")
                      ->join("$zonestateTab","$zonestateTab.zoneId","=","$zoneTab.id")
                      ->leftjoin("$zonecityTab","$zonecityTab.zoneId","=","$zoneTab.id")
                      ->where("status","1")->where("countryId",$addrInfo['country'])
                      ->where("stateId",$addrInfo['state'])->whereNull("cityid")->orderBy('id','asc')->first();
            
            if(!empty($zoneCoutryStateData))
            {
                $returnZones[] = $zoneCoutryStateData->id;
            }
            else
            {
                $zoneCoutryData = Zone::select("$zoneTab.id")
                      ->join("$zoneCountryTab","$zoneCountryTab.zoneId","=","$zoneTab.id")
                      ->leftjoin("$zonestateTab","$zonestateTab.zoneId","=","$zoneTab.id")
                      ->leftjoin("$zonecityTab","$zonecityTab.zoneId","=","$zoneTab.id")  
                      ->where("status","1")->where("countryId",$addrInfo['country'])
                      ->whereNull("cityid")->whereNull("stateId")->orderBy('id','asc')->first();
                if(!empty($zoneCoutryData))
                {
                    $returnZones[] = $zoneCoutryData->id;
                }
            }
        }
        
        
        return $returnZones;   


        
    }

    

}
