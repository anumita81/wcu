<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Personalcharge extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.PERSONALCHARGE');
        $this->prefix = DB::getTablePrefix();
    }

    public static function getList($param) {
        $personalcharge = new Personalcharge;
        $make = new Automake;
        $model = new Automodel;

        $where = "1";

        $modelTable = $personalcharge->prefix . $model->table;

        if (!empty($param['searchByMake']))
            $where .= "  AND $modelTable.makeId ='" . $param['searchByMake'] . "'";

        $resultSet = Personalcharge::select(array("$personalcharge->table.id", "$personalcharge->table.charge", "$make->table.name AS makeName", "$model->table.name AS modelName",))
                ->leftJoin($model->table, "$personalcharge->table.modelId", '=', "$model->table.id")
                ->leftJoin($make->table, "$model->table.makeId", '=', "$make->table.id")
                ->where($personalcharge->table . '.deleted', '0')
                ->whereRaw($where)
                //   ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Personalcharge::where('id', $id)->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

}
