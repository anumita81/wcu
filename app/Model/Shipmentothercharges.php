<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
use Spatie\Activitylog\Traits\LogsActivity;

class Shipmentothercharges extends Model {

    use LogsActivity;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    protected $prefix;
    public $timestamps = false;
    protected static $logName = 'othercharges';
    protected static $logAttributes = ['id', 'shipmentId', 'otherChargeId', 'otherChargeName', 'otherChargeAmount', 'notes'];

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.SHIPMENTOTHERCHARGES');
        $this->prefix = DB::getTablePrefix();
    }
    
    public static function getDeliverywiseCharges($shipmentId,$type='other') {
        $result = array();
        $where = "otherChargeId!=0";
        if($type == 'manual')
            $where = "otherChargeId=0";
        $data = Shipmentothercharges::where('shipmentId', $shipmentId)->whereRaw($where)->get();
        if(count($data)>0)
        {
            foreach($data as $eachData)
            {
                $result[$eachData->deliveryId][] = $eachData;
            }
        }
        //dd($result);
        return $result;
    }
    
    public static function getDeliverywiseNewCharges($deliveryId,$type='other') {
        $result = array();
        $where = "otherChargeId!=0";
        if($type == 'manual')
            $where = "otherChargeId=0";
        $data = Shipmentothercharges::where('deliveryId', $deliveryId)->whereRaw($where)->get();
//        if(count($data)>0)
//        {
//            foreach($data as $eachData)
//            {
//                $result[$eachData->deliveryId][] = $eachData;
//            }
//        }
        //dd($result);
        return $data;
    }

}
