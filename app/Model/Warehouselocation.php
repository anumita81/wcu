<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Warehouselocation extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    protected $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.WAREHOUSELOCATION');
        $this->prefix = DB::getTablePrefix();
    }

   
    /**
     * Method used to fetch Shipment Rows list
     * @param array $param
     * @return object
     */
    public static function getRowList($param) {
        $where = "type='R'";

        $resultSet = Warehouselocation::whereRaw($where)
                ->select(array('id', 'name', 'createdOn', 'status'))
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }
    
    public static function getInUseDataRow() {
        
        $fillnshipWarehouse = new Fillshipwarehouselocation;
        $shipmentWarehouse = new Shipmentwarehouselocation;
        $warehouselocation = new Warehouselocation;
        $resultData = array();
        
        $resultSet = Warehouselocation::select("$warehouselocation->table.id")
                    ->join("$shipmentWarehouse->table","$warehouselocation->table.id","=","$shipmentWarehouse->table.warehouseRowId")
                    ->where("$warehouselocation->table.type",'R')
                    ->get();
        foreach($resultSet as $eachData)
        {
            if(!in_array($eachData->id,$resultData))
            {
                $resultData[] = $eachData->id;
            }
        }
        
        $resultFillshipSet = Warehouselocation::select("$warehouselocation->table.id")
                    ->join("$fillnshipWarehouse->table","$warehouselocation->table.id","=","$fillnshipWarehouse->table.warehouseRowId")
                    ->where("$warehouselocation->table.type",'R')
                    ->get();
        
        foreach($resultFillshipSet as $eachFillshipData)
        {
            if(!in_array($eachFillshipData->id,$resultData))
            {
                $resultData[] = $eachFillshipData->id;
            }
        }
        
        return $resultData;

    }
    
    public static function getInUseDataZone() {
        
        $fillnshipWarehouse = new Fillshipwarehouselocation;
        $shipmentWarehouse = new Shipmentwarehouselocation;
        $warehouselocation = new Warehouselocation;
        $resultData = array();
        
        $resultSet = Warehouselocation::select("$warehouselocation->table.id")
                    ->join("$shipmentWarehouse->table","$warehouselocation->table.id","=","$shipmentWarehouse->table.warehouseZoneId")
                    ->where("$warehouselocation->table.type",'Z')
                    ->get();
        foreach($resultSet as $eachData)
        {
            if(!in_array($eachData->id,$resultData))
            {
                $resultData[] = $eachData->id;
            }
        }
        
        $resultFillshipSet = Warehouselocation::select("$warehouselocation->table.id")
                    ->join("$fillnshipWarehouse->table","$warehouselocation->table.id","=","$fillnshipWarehouse->table.warehouseZoneId")
                    ->where("$warehouselocation->table.type",'Z')
                    ->get();
        
        foreach($resultFillshipSet as $eachFillshipData)
        {
            if(!in_array($eachFillshipData->id,$resultData))
            {
                $resultData[] = $eachFillshipData->id;
            }
        }
        
        return $resultData;

    }

    /**
     * Method used to fetch Shipment Rows list
     * @param array $param
     * @return object
     */
    public static function getZoneList($param) {
        $warehouselocation = new Warehouselocation;
        $warehouselocationTable = $warehouselocation->prefix . $warehouselocation->table;


        $where = "$warehouselocationTable.type='Z'";

        if (!empty($param['searchByRow']))
            $where .= "  AND $warehouselocationTable.parentId ='" . $param['searchByRow'] . "'";

        $resultSet = Warehouselocation::whereRaw($where)
                ->select(array("$warehouselocation->table.id", "$warehouselocation->table.name", "rowTable.name as rowName", "$warehouselocation->table.status", "$warehouselocation->table.createdOn"))
                ->leftJoin("$warehouselocation->table AS rowTable", "rowTable.id", '=', "$warehouselocation->table.parentId")
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to change Deliverycompany status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Warehouselocation::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

}
