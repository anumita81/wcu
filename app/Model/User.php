<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;
use Config;
use Auth;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class User extends Authenticatable {

    use Notifiable,
        HasApiTokens;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.User');
        $this->prefix = DB::getTablePrefix();
    }

    public function addressbook() {
        return $this->hasMany('App\Model\Addressbook', 'UserId', 'id');
    }

    /**
     * Method used to fetch Users list
     * @param array $param
     * @return object
     */
    public static function getUserList($param, $type = '') {
        $User = new User;
        $city = new City;
        $country = new Country;
        $state = new State;
        $UserTable = $User->prefix . $User->table;
        DB::enableQueryLog();

        $where = "$UserTable.deleted = '0'";

        if (!empty($param['searchBycontactPersonName']))
            $where .= " AND $UserTable.contactPersonName LIKE '%" . addslashes($param['searchBycontactPersonName']) . "%'";

        if (!empty($param['searchBylastName']))
            $where .= " AND $UserTable.lastName LIKE '%" . addslashes($param['searchBylastName']) . "%'";

        if (!empty($param['searchByEmail']))
            $where .= " AND $UserTable.email LIKE '%" . addslashes($param['searchByEmail']) . "%'";


        if ($param['searchByCreatedOn'] != '') {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($UserTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $UserTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($UserTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($UserTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        if (!empty($param['searchByUser'])) {
            $UserArray = implode("','", $param['searchByUser']);
            $where .= " AND $UserTable.id IN ('" . $UserArray . "')";
        }

        if ($type == 'export') {
            $resultSet = User::whereRaw($where)
                    ->select(array("$User->table.id", "unit", "title", "contactPersonName", "email", "contactNumber", "$User->table.createdOn", "dateOfBirth", "$User->table.status", "$city->table.name AS cityName", "$country->table.name AS countryName", "$state->table.name AS stateName",))
                    ->leftJoin($country->table, "$User->table.countryId", '=', "$country->table.id")
                    ->leftJoin($state->table, "$User->table.stateId", '=', "$state->table.id")
                    ->leftJoin($city->table, "$User->table.cityId", '=', "$city->table.id")
                    ->orderBy($param['field'], $param['type'])
                    ->get();
        } else{
               $resultSet = User::whereRaw($where)
                    ->select(array("$User->table.id", "contactPersonName", "email", "company", "$User->table.createdOn", "$User->table.status", "activationToken"))
                    ->orderBy($param['field'], $param['type'])
                    ->paginate($param['searchDisplay']);
 
        
        }

        //dd(DB::getQueryLog());

       // print_r($resultSet);

        return $resultSet;
    }


   

    /**
     * Method used to change State status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = User::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = User::where('id', $id)
                ->update(array('deleted' => 1, 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }




    public static function exportCustomerData($param) {


        if (isset($param['searchByCondition']) && !empty($param['searchByCondition'])) {

            if ($param['searchByCondition'] == 'numberofshipment') {
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company' => 'Company',
                    'shippingMethod' => 'Shipping Method',
                    'createdOn' => 'Created On',
                    'numberofshipment' => 'Number of Shipments',
                );

                $records = User::getCustomerShippingMethodData($param, '1');
            } elseif ($param['searchByCondition'] == 'shipmentweight') {
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company' => 'Company',
                    'totalWeight' => 'Shipment Weight',
                    'createdOn' => 'Created On',
                );

                $records = User::getCustomerShipmentWeightData($param, 1);
            } elseif ($param['searchByCondition'] == 'destinationcity') {
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company' => 'Company',
                    'toCountry' => 'Destination Country',
                    'toState' => 'Destination State',
                    'toCity' => 'Destination City',
                    'numberofshipment' => 'Number of Shipments',
                );

                $records = User::getCustomerDestinationData($param, '1');
            } elseif ($param['searchByCondition'] == 'registrationdate') {
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company' => 'Company',
                    'createdOn' => 'Registered On',
                    'registeredBy' => 'Registered From',
                );

                $records = User::getCustomerRegistrationData($param, '1');
            } elseif ($param['searchByCondition'] == 'numberoforder') {
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company' => 'Company',
                    'numberoforder' => 'Number Of Orders'
                );

                $records = User::getCustomerOrderNumData($param, '1');
            } elseif ($param['searchByCondition'] == 'orderdate') {
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company' => 'Company',
                    'shipmentId' => 'Shipment ID',
                    'orderNumber' => 'Order Number',
                    'createdDate' => 'Last Order Date'
                );

                $records = User::getCustomerOrderDateData($param, '1');
            } elseif ($param['searchByCondition'] == 'spentamount') {
                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company' => 'Company',
                    'paidAmount' => 'Spent Amount',
                );

                $records = User::getCustomerSpendingData($param, 1);
            } elseif ($param['searchByCondition'] == 'procurementshipmenttype') {

                if ($param['searchCustomer']['servicetype'] == 'shipment') {
                    $headers = array(
                        'unit' => 'Unit Number',
                        'name' => 'Name',
                        'email' => 'Email',
                        'contactNumber' => 'Contact',
                        'company' => 'Company',
                        'shipmentId' => 'Shipment ID',
                        'orderNumber' => 'Order Number',
                        'createdDate' => 'Created On'
                    );
                } else if ($param['searchCustomer']['servicetype'] == 'shop_for_me') {
                    $headers = array(
                        'unit' => 'Unit Number',
                        'name' => 'Name',
                        'email' => 'Email',
                        'contactNumber' => 'Contact',
                        'company' => 'Company',
                        'shipmentId' => 'Procurement ID',
                        'procurementType' => 'Type',
                        'createdDate' => 'Created On'
                    );
                } else if ($param['searchCustomer']['servicetype'] == 'auto') {
                    $headers = array(
                        'unit' => 'Unit Number',
                        'name' => 'Name',
                        'email' => 'Email',
                        'contactNumber' => 'Contact',
                        'company' => 'Company',
                        'shipmentId' => 'Shipment ID',
                        'orderNumber' => 'Order Number',
                        'createdDate' => 'Created On'
                    );
                }

                $records = User::getCustomerServiceType($param, 1);
            } elseif ($param['searchByCondition'] == 'customertotalspent') {

                $headers = array(
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'company' => 'Company',
                    'shipmentId' => 'Shipment ID',
                    'type' => 'Type',
                    'totalCost' => 'Amount Spent',
                    'createdOn' => 'Created On'
                );

                $records = User::getCustomerLifetimeData($param, 1);
            }
            //print_r($records);exit;
            if (!empty($records)) {
                foreach ($records as $index => $data) {
                    //print_r($data);
                    foreach ($data as $fieldName => $eachField) {
                        if (!empty($headers[$fieldName])) {
                            $fieldDisplayName = $headers[$fieldName];
                        } else {
                            $fieldDisplayName = $fieldName;
                        }

                        $exportData[$index][$fieldDisplayName] = $eachField;
                    }
                }
            }

            return $exportData;
        }
    }


}
