<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Model\Zone;
use App\Model\User;
use Config;

class Shippingcharges extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.SHIPPINGCHARGES');
    }

    public static function getData($param) {

        $formatedData = array();

        $shippingChargesData = DB::table('shippingcharges')
                ->join('shippingmethods', 'shippingcharges.shippingId', '=', 'shippingmethods.shippingid')
                ->join('zones', 'shippingcharges.zoneId', '=', 'zones.id')
                ->select('shippingcharges.*', 'shippingmethods.shipping as shippingMethod', 'zones.name as zoneName')
                ->where('shippingmethods.active', 'Y')
                ->where('zones.status', '1');
        if (!empty($param['searchData']['searchMethod']))
            $shippingChargesData->where('shippingcharges.shippingId', $param['searchData']['searchMethod']);
        if (!empty($param['searchData']['searchZone']))
            $shippingChargesData->where('zoneId', $param['searchData']['searchZone']);

        $shippingChargesData = $shippingChargesData->get();


        foreach ($shippingChargesData as $eachData) {
            $formatedData[$eachData->zoneId]['name'] = $eachData->zoneName;
            $formatedData[$eachData->zoneId]['zoneId'] = $eachData->zoneId;
            $formatedData[$eachData->zoneId]['data'][$eachData->shippingId]['name'] = $eachData->shippingMethod;
            $formatedData[$eachData->zoneId]['data'][$eachData->shippingId]['shippingId'] = $eachData->shippingId;
            $shippingMethod = Zone::where('id', $eachData->pzoneId)->first();
            $formatedData[$eachData->zoneId]['data'][$eachData->shippingId]['data'][$eachData->pzoneId]['sourceZone'] = $shippingMethod->name;
            $formatedData[$eachData->zoneId]['data'][$eachData->shippingId]['data'][$eachData->pzoneId]['data'][] = $eachData;
        }

        //echo '<pre>';print_r($formatedData);echo '</pre>';exit;
        return $formatedData;
    }

    /**
     * Method used to fetch shipping charge details
     * @param array $param
     * @return object
     */
    public static function getShippingChargeData($param) {
        $chargesData = array();
        $destinationAddress = array('country' => $param['toCountry'],
            'state' => $param['toState'],
            'city' => $param['toCity'],
        );
        $sourceAddress = array('country' => $param['fromCountry'],
            'state' => $param['fromState'],
            'city' => $param['fromCity'],
        );

        $destinationZone = Zone::getZoneWithAddr($destinationAddress);
        $sourceZone = Zone::getZoneWithAddr($sourceAddress);

        // DB::enableQueryLog();
        if(array_key_exists('totalProcurementCost', $param))
        {
            $resultSet = Shippingcharges::where('shippingId', $param['shippingId'])
                    ->where('minWeight', '<=', $param['totalWeight'])
                    ->where('maxWeight', '>=', $param['totalWeight'])
                    ->where('minCost', '<=', $param['totalProcurementCost'])
                    ->where('maxCost', '>=', $param['totalProcurementCost'])
                    ->whereIn('zoneId', $destinationZone)
                    ->whereIn('pzoneId', $sourceZone)
                    ->first();
        }
        else
        {
            $resultSet = Shippingcharges::where('shippingId', $param['shippingId'])
                    ->where('minWeight', '<=', $param['totalWeight'])
                    ->where('maxWeight', '>=', $param['totalWeight'])
                    ->whereIn('zoneId', $destinationZone)
                    ->whereIn('pzoneId', $sourceZone)
                    ->first();
        }

        return $resultSet;
    }

    public static function checkIfChargesExist($shipmentData, $shipmentDetailsData, $shippingId, $total_chargeable_weight,$totalItemCost = '0.00') {
        
        $destinationAddress = array('country' => $shipmentData['toCountry'],
            'state' => $shipmentData['toState'],
            'city' => $shipmentData['toCity'],
        );
        $sourceAddress = array('country' => $shipmentData['warehouseCountry'],
            'state' => $shipmentData['warehouseState'],
            'city' => $shipmentData['warehouseCity'],
        );
       
        $destinationZone = Zone::getZoneWithAddr($destinationAddress);
        $sourceZone = Zone::getZoneWithAddr($sourceAddress);

       
       // DB::enableQueryLog();

        if($totalItemCost !='0.00')
        {
            $chargesData = Shippingcharges::where('shippingId', $shippingId)
                    ->where('minWeight', '<=', "'".$total_chargeable_weight."'")
                    ->where('maxWeight', '>=', "'".$total_chargeable_weight."'")
                    ->where('minCost', '<=', "'".$totalItemCost."'")
                    ->where('maxCost', '>=', "'".$totalItemCost."'")
                    ->whereIn('zoneId', $destinationZone)
                    ->whereIn('pzoneId', $sourceZone)
                    ->first();
        }
        else
        {
            $chargesData = Shippingcharges::where('shippingId', $shippingId)
                    ->where('minWeight', '<=', "'".$total_chargeable_weight."'")
                    ->where('maxWeight', '>=', "'".$total_chargeable_weight."'")
                    ->whereIn('zoneId', $destinationZone)
                    ->whereIn('pzoneId', $sourceZone)
                    ->first();
        }

        //dd(DB::getQueryLog());

        //print_r($chargesData);


        if (!empty($chargesData)) {
            return $chargesData;
        } else {
            return array();
        }
    }

    public static function calculateShippingCharge($param, $shippingCharge) {
        $chargesDetails = array();
        $otherChargeAmount = 0;
        $inventroyChargeAmount = 0;
        $storageChargeAmount = 0;
        $duty = 0;
        $insurance = 0;
        $numItems = 0;
        $packagingInventory = 0;
        $effectiveRate = 0;

        if (isset($param['userId']) && !empty($param['userId'])) {
            $billingAddr = Addressbook::select('id', 'countryId')->where('isDefaultBilling', '1')->where('userId', $param['userId'])->first();

            if (isset($param['paymentMethod']) && !empty($param['paymentMethod'])) {
                $taxCharge = Paymenttaxsettings::where('deleted', '0')->where('paymentMethodId', $param['paymentMethod'])->where('countryId', $billingAddr->countryId)->first();
            }
        }

        $chargesDetails['shippingCost'] = round(($shippingCharge->flatRate + ($shippingCharge->shippingChargePerpound * $param['totalWeight']) + $shippingCharge->dispatchCost + ($shippingCharge->dispatchCostPerpound * $param['totalWeight'])), 2);
        $chargesDetails['clearing'] = round(($shippingCharge->clearingPortHandling + ($shippingCharge->clearingPortHandlingPerpound * $param['totalWeight'])), 2);

        $duty = $insurance = 0;
        if (!empty($shippingCharge->customDutyPercent))
            $duty = trim($param['totalProcurementCost']) * ($shippingCharge->customDutyPercent / 100);
        if (!empty($shippingCharge->insurancePercent))
            $insurance = trim($param['totalProcurementCost']) * ($shippingCharge->insurancePercent / 100);
        $chargesDetails['duty'] = round($duty, 2);
        $chargesDetails['insurance'] = round($insurance, 2);
        $chargesDetails['discount'] = 0;
        $chargesDetails['tax'] = 0;
        $chargesDetails['clearingDuty'] = $chargesDetails['clearing'] + $chargesDetails['duty'];
        $chargesDetails['isDutyCharged'] = !empty($chargesDetails['duty']) ? true : false;

        if (!empty($taxCharge)) {
            if ($taxCharge->taxtype == '0')
                $chargesDetails['tax'] = $taxCharge->taxValue;
            else if ($taxCharge->taxtype == '1')
                $chargesDetails['tax'] = round(($chargesDetails['shippingCost'] * ($taxCharge->taxValue / 100)), 2);
        }

        if (isset($param['inventoryCharge']) && !empty($param['inventoryCharge']))
            $inventroyChargeAmount = $param['inventoryCharge'];

        if (isset($param['storageCharge']) && !empty($param['storageCharge']))
            $storageChargeAmount = $param['storageCharge'];

        if (isset($param['otherChargeCost']) && !empty($param['otherChargeCost']))
            $otherChargeAmount = $param['otherChargeCost'];

        /* CALCULATE TOTAL SHIPPING COST */
        $chargesDetails['totalShippingCost'] = $chargesDetails['shippingCost'] + $chargesDetails['clearing'] + $chargesDetails['duty'];

        /* CALCULATE TOTAL COST */
        $chargesDetails['total'] = $chargesDetails['totalShippingCost'] + $inventroyChargeAmount + $storageChargeAmount + $otherChargeAmount;

        /* CALCULATE EFFECTIVE RATE */
        if (isset($param['totalWeight']) && !empty($param['totalWeight']) && $param['totalWeight']!='0.00' && $param['totalWeight'] != '0')
            $effectiveRate = round((($chargesDetails['shippingCost'] + $chargesDetails['clearing'] + $chargesDetails['duty']) / $param['totalWeight']), 2);

        $chargesDetails['effectiveRate'] = $effectiveRate;

        return $chargesDetails;
    }

    public static function calculateShippingCharges($productDetails, $chargesRecord, $total_chargeable_weight, $paymentMethod = '', $shipmentId = '-1', $userId = '-1') {

        $otherCharges = array();
        $otherChargeAmount = 0;
        $storageChargeAmount = 0;

        $selectedPackagingCharges = Packagingcharges::where('weightFrom', '<=', $total_chargeable_weight)->where('weightTo', '>=', $total_chargeable_weight)->where('status', '1')->where('deleted', '0')->first();
        $shipmentStorageCharge = Shipment::select('storageCharge')->where('id', $shipmentId)->first();
        if (isset($shipmentStorageCharge->storageCharge) && $shipmentStorageCharge->storageCharge != '')
            $storageChargeAmount = $shipmentStorageCharge->storageCharge;
        if ($shipmentId != '-1')
            $otherCharges = Shipmentothercharges::where('shipmentId', $shipmentId)->get();
        if ($userId != '-1') {
            $billingAddr = Addressbook::select('id', 'countryId')->where('isDefaultBilling', '1')->where('userId', $userId)->first();
            if ($paymentMethod != '') {
                $taxCharge = Paymenttaxsettings::where('deleted', '0')->where('paymentMethodId', $paymentMethod)->where('countryId', $billingAddr->countryId)->first();
            }
        }
        if (count($otherCharges) > 0) {
            foreach ($otherCharges as $eachCharge) {
                $otherChargeAmount += $eachCharge->otherChargeAmount;
            }
        }
        $chargesDetails = array();
        $duty = 0;
        $insurance = 0;
        $totalValue = 0;
        $numItems = 0;
        $packagingInventory = 0;

        $chargesDetails['shippingCost'] = round(($chargesRecord->flatRate + ($chargesRecord->shippingChargePerpound * $total_chargeable_weight) + $chargesRecord->dispatchCost + ($chargesRecord->dispatchCostPerpound * $total_chargeable_weight)), 2);
        $chargesDetails['clearing'] = round(($chargesRecord->clearingPortHandling + ($chargesRecord->clearingPortHandlingPerpound * $total_chargeable_weight)), 2);

        foreach ($productDetails as $eachProduct) {

            $totalValue += ($eachProduct['value'] * $eachProduct['quantity']);
            $numItems += $eachProduct['quantity'];
        }
        $duty = $totalValue * ($chargesRecord->customDutyPercent / 100);
        $insurance = $totalValue * ($chargesRecord->insurancePercent / 100);
        $chargesDetails['duty'] = round($duty, 2);
        $chargesDetails['insurance'] = round($insurance, 2);
        $chargesDetails['discount'] = 0;
        $chargesDetails['tax'] = 0;
        if (!empty($taxCharge)) {
            if ($taxCharge->taxtype == '0')
                $chargesDetails['tax'] = $taxCharge->taxValue;
            else if ($taxCharge->taxtype == '1')
                $chargesDetails['tax'] = round(($chargesDetails['shippingCost'] * ($taxCharge->taxValue / 100)), 2);
        }

        $chargesDetails['otherCharges'] = $otherChargeAmount + $storageChargeAmount;
        if (count($selectedPackagingCharges) > 0)
            $packagingInventory = $selectedPackagingCharges->amount;
        $packagingInventory += ($numItems * $chargesRecord->inventoryCharge);
        $chargesDetails['packagingInventory'] = round($packagingInventory, 2);
        $chargesDetails['storage'] = 0;
        $chargesDetails['total'] = $chargesDetails['shippingCost'] + $chargesDetails['clearing'] + $chargesDetails['duty'] + $chargesDetails['insurance'] + $chargesDetails['tax'] + $chargesDetails['otherCharges'] + $chargesDetails['packagingInventory'] + $chargesDetails['storage'];
        $effectiveRate = 0;
        if ($total_chargeable_weight != 0)
            $effectiveRate = round((($chargesDetails['shippingCost'] + $chargesDetails['clearing'] + $chargesDetails['duty']) / $total_chargeable_weight), 2);
        $chargesDetails['effectiveRate'] = $effectiveRate;

        return $chargesDetails;
    }

}
