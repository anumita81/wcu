<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Shipmentpackage extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    protected $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.SHIPMENTPACKAGE');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Shipmentpackage::where('id', $id)
                ->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedByType' => 'admin', 'deletedOn' => Config::get('constants.CURRENTDATE')));
        
        $packageInfo = Shipmentpackage::find($id);
        $totalItemCost = Shipmentpackage::where('deliveryId',$packageInfo->deliveryId)->where('deleted','0')->sum('itemTotalCost');
        Shipmentdelivery::where('id',$packageInfo->deliveryId)->update(['totalItemCost'=>$totalItemCost]);
        return $row;
    }
    
    public static function calculatetotalchargebleweight($packageDetailsData) {

        $totalChargeableWeight = 0;
        foreach ($packageDetailsData as $key => $eachPackage) {
            $totalChargeableWeight += $eachPackage->weight;
        }

        return $totalChargeableWeight;
    }

    public static function getpackageLimit($packageChargeableWeight,$deliveryChargeableWeight) {
        
        if($deliveryChargeableWeight==0 || $packageChargeableWeight ==0)
            return array();

        $percent = ($packageChargeableWeight / $deliveryChargeableWeight) * 100;
        $percent = round($percent,2);

        $chargeableWeightFactor = \App\Model\Generalsettings::where('settingsKey', 'acceptable_package_error')->first();
        $acceptableError = $chargeableWeightFactor->settingsValue;
        $return = array();
        if ((100-$acceptableError) <= $percent && $percent <= (100 + $acceptableError)) {
            $return['message'] = "<b>Final Package is within acceptable values</b>";
            $return['code'] = 1;

        } elseif ($percent < (100-$acceptableError)) {
            $return['message'] = "<font color='red'>Delivery Dimensions are too small by ".($percent - 100)."%</font>";
            $return['code'] = 2;

        } else {
            $return['message'] = "<font color='blue'>Delivery Dimensions are too big by ".($percent - 100)."%</font>";
            $return['code'] = 3;

        }

        return $return;
    }
    
    public static function fetchUsedFieldIds($field = '') {
        $resultset = Shipmentpackage::distinct()->whereNotNull($field)->where("deleted", "0")->pluck($field)->toArray();

        return $resultset;
    }
    
    public static function allItemReturned($shipmentId = 0,$deliveryId = 0,$lookFor="delivery",$includeRequested = false) {
        
        $allItemReturned = "0";
        if($lookFor == "delivery")
        {
            $numDeliveryPackage = Shipmentpackage::where('deliveryId',$deliveryId)->where('deleted','0')->get()->count();
            if($includeRequested)
                $numDeliveryPackageReturned = Shipmentpackage::where('itemReturn','!=',"0")->where('deliveryId',$deliveryId)->where('deleted','0')->get()->count();
            else
                $numDeliveryPackageReturned = Shipmentpackage::where('itemReturn','2')->where('deliveryId',$deliveryId)->where('deleted','0')->get()->count();
            if($numDeliveryPackage == $numDeliveryPackageReturned && $numDeliveryPackage>0)
            {
                $allItemReturned = "1";
            }
        }
        else if($lookFor == "shipment")
        {
            $numShipmentPackage = Shipmentpackage::where('shipmentId',$shipmentId)->where('deleted','0')->get()->count();
            $numShipmentPackageReturned = Shipmentpackage::where('itemReturn','!=','0')->where('shipmentId',$shipmentId)->where('deleted','0')->get()->count();
            if($numShipmentPackage == $numShipmentPackageReturned)
            {
                $allItemReturned = "1";
            }
        }
        
        return $allItemReturned;
    }
    
    public static function allItemDeleted($shipmentId = 0, $deliveryId = 0) {
        
        $noOfitemsInDelivery = Shipmentpackage::where('deliveryId',$deliveryId)->get()->count();
        $noOfDeletedItems = Shipmentpackage::where('deliveryId',$deliveryId)->where('deleted','1')->get()->count();
        
        if($noOfitemsInDelivery == $noOfDeletedItems) {
            return "1";
        } else {
            return "0";
        }
        
    }

    public static function itemReturnRequested($shipmentId = 0,$deliveryId = 0,$lookFor="delivery") {
        
        $itemReturnRequested = "0";
        if($lookFor == "delivery")
        {
            $numDeliveryPackageReturnRequested = Shipmentpackage::where('itemReturn','1')->where('deliveryId',$deliveryId)->where('deleted','0')->get()->count();
            if($numDeliveryPackageReturnRequested>0)
            {
                $itemReturnRequested = "1";
            }
        }
        else if($lookFor == "shipment")
        {
            $numShipmentPackageReturnRequested = Shipmentpackage::where('itemReturn','1')->where('shipmentId',$shipmentId)->where('deleted','0')->get()->count();
            if($numShipmentPackageReturnRequested>0)
            {
                $itemReturnRequested = "1";
            }
        }
        
        return $itemReturnRequested;
        
    }

}
