<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Packagingcharges extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.PACKAGECHARGE');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to fetch Country list
     * @param array $param
     * @return object
     */
    public static function getPackageChargesList($param) {

        $charges = new Packagingcharges;
        $where = 1;
  
        
        $resultSet = Packagingcharges::select('*')
                ->where("$charges->table.deleted", "=", '0')
                ->whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId='') {
        if (empty($id))
            return false;

        $row = false;

        $row = Packagingcharges::where('id', $id)
                ->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }


    /**
     * Method used to change status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Packagingcharges::where('id', $id)
                ->update(array('status' => $newStatus));

        return $row;
    }
    
    public static function validateNumbers($weightFrom, $weightTo, $id ='')
    {
        $charges = new Packagingcharges;
        
        $chargesTable = $charges->prefix.$charges->table;       
        

        if(empty($id))
        {
            $resultSet = Packagingcharges::whereRaw('(('.$weightFrom.' BETWEEN weightFrom AND weightTo) or ('.$weightTo.' BETWEEN weightFrom AND weightTo)) and deleted ="0"')->get();
        }else{
            $resultSet = Packagingcharges::whereRaw('(('.$weightFrom.' BETWEEN weightFrom AND weightTo) or ('.$weightTo.' BETWEEN weightFrom AND weightTo)) and deleted ="0" and id <> '.$id)->get();
        }        

       
        return $resultSet;
      
    }


}
