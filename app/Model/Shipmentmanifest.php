<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Shipmentmanifest extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.CONSOLIDATED_SHIPMENTMANIFEST');
        $this->prefix = DB::getTablePrefix();
    }
    
    public static function getData($param) {

        $adminUser = new UserAdmin;
        $shipmentManifest = new Shipmentmanifest;
        $commercialInvTable = $shipmentManifest->prefix.$shipmentManifest->table;

        $where = "$commercialInvTable.deleted = '0'";

        if (!empty($param['searchTracking']['name']))
            $where .= " AND $commercialInvTable.invoiceName LIKE '%" . $param['searchTracking']['name'] . "%'"; 

        if ($param['searchByCreatedOn'] != '') {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($commercialInvTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $commercialInvTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($commercialInvTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($commercialInvTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        $resultSet = Shipmentmanifest::select(array("$shipmentManifest->table.id", "$shipmentManifest->table.invoiceName" , "$shipmentManifest->table.invoiceFile", "$shipmentManifest->table.createdOn", "$adminUser->table.email"))
                ->leftJoin($adminUser->table, "$shipmentManifest->table.createdBy", '=', "$adminUser->table.id")
                ->whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);


        return $resultSet;
    }

}