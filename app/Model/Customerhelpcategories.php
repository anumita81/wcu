<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
use Auth;

class Customerhelpcategories extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.CUSTOMERHRLPCAT');
        $this->prefix = DB::getTablePrefix();
    }

    

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Customerhelpcategories::where('id', $id)->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        $sections = new Customerhelpsection;

        $row = Customerhelpsection::where('catId', $id)
                ->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return '1';
    }
    
    /**
     * Method used to change Admin User status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return object
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Customerhelpcategories::where('id', $id)
                ->update(array('status' => $newStatus, 'updatedBy' => Auth::user()->id, 'updatedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to get the categoroies with number of sections
     * @return object
     */
    public static function getallcat() {
        $row = false;

        $sections = new Customerhelpsection;
        $cat = new Customerhelpcategories;

        $row = Customerhelpcategories::where("$cat->table.deleted", '0')->where("$cat->table.status", '1')
                ->where("$sections->table.status", '1')
                ->where("$sections->table.status", '1')
                ->select(array(
                    "$cat->table.id", "$cat->table.name", "$cat->table.slug", 
                    "$cat->table.orders"))
                ->selectRaw('COUNT('.$sections->prefix.$sections->table.'.id) as count')
                ->leftJoin($sections->table, "$sections->table.catId", '=', "$cat->table.id")
                ->groupBy("$cat->table.id")
                ->orderby("$cat->table.orders", "asc")
                ->get();

        return $row;
    }


    /**
     * Method used to get the category by id
     * @return object
     */
    public static function getcatbyid($id) {
        $row = false;
        $row = Customerhelpcategories::where('id', $id)->first();
        return $row;
    }


}
