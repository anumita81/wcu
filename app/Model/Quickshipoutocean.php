<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Quickshipoutocean extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.QUICKSHIPOUTOCEAN');
        $this->prefix = DB::getTablePrefix();
    }

    public static function shipmenttype() {
        return array(1=>'Ocean – Normal Items',2=>'Ocean Equipment',3=>'Ocean – Cars',4=>'Ocean – Fill and Ship');
    }
    
     public static function getData($param) {

        $adminUser = new UserAdmin;
        $quickshipoutocean = new Quickshipoutocean;
        $quickshipoutoceanTable = $quickshipoutocean->prefix.$quickshipoutocean->table;

        $where = "$quickshipoutoceanTable.deleted = '0'";

        // if (!empty($param['searchTracking']['name']))
        //     $where .= " AND $quickshipoutoceanTable.invoiceName LIKE '%" . $param['searchTracking']['name'] . "%'";
        // if (!empty($param['searchTracking']['trackingNumber']))
        //     $where .= " AND $shipmentTrackingTable.trackingNumber ='" . $param['searchTracking']['trackingNumber'] . "'";
        // if (!empty($param['searchTracking']['unitNumber']))
        //     $where .= " AND $quickshipoutoceanTable.invoiceDetails LIKE '%" . $param['searchTracking']['unitNumber'] . "%'";

        if ($param['searchByCreatedOn'] != '') {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($quickshipoutoceanTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $quickshipoutoceanTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($quickshipoutoceanTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($quickshipoutoceanTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        $resultSet = Quickshipoutocean::select(array("$quickshipoutocean->table.id", "$quickshipoutocean->table.invoiceName" , "$quickshipoutocean->table.invoiceFile", "$quickshipoutocean->table.createdOn", "$adminUser->table.email","$quickshipoutocean->table.shipoutLocked"))
                ->leftJoin($adminUser->table, "$quickshipoutocean->table.createdBy", '=', "$adminUser->table.id")
                ->whereRaw($where)
                ->groupBy("$quickshipoutocean->table.id")
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);


        return $resultSet;
    }
}

