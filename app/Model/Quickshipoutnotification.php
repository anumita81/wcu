<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Quickshipoutnotification extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.QUICKSHIPOUTNOTIFICATION');
        $this->prefix = DB::getTablePrefix();
    }
    
    public static function getNotificationHistory($quickShipoutId) {
        
        $return = array();
        $data = Quickshipoutnotification::where('quickshipoutId',$quickShipoutId)->orderBy('id','asc')->get();
        if(!empty($data))
        {
            foreach($data as $eachdata)
            {
                $emailSent = "0";
                $smsSent = "0";
                if($eachdata->emailSent == "2")
                    $emailSent = "1";
                if($eachdata->smsSent == "2")
                    $smsSent = "1";
                if($eachdata->quickshipoutType == "0")
                {
                    $return[$eachdata->unitNumber][$eachdata->trackingNumber]['emailSent'] = $emailSent;
                    $return[$eachdata->unitNumber][$eachdata->trackingNumber]['smsSent'] = $smsSent;

                }else{
                    $return[$eachdata->unitNumber]['emailSent'] = $emailSent;
                    $return[$eachdata->unitNumber]['smsSent'] = $smsSent;
                }
                
            }
        }
        
        $oldLogdata = Quickshipoutlog::where('quickshipoutId',$quickShipoutId)->orderBy('id','asc')->get();
        
        if(!empty($data))
        {
            foreach($oldLogdata as $eachLogdata)
            {
                if(empty($return[$eachLogdata->unitNumber][$eachLogdata->trackingNumber]))
                {
                    $return[$eachLogdata->unitNumber][$eachLogdata->trackingNumber]['emailSent'] = $eachLogdata->emailSent;
                    $return[$eachLogdata->unitNumber][$eachLogdata->trackingNumber]['smsSent'] = $eachLogdata->smsSent;
                }
            }
        }
        
        return $return;
    }
}

