<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
use Auth;
use customhelper;

class Fundpoint extends Model {

    public $table;
    public $timestamps = false;
    protected $fillable = ['user'];

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.FUNDPOINT');
    }

    /**
     * This function is used to calculate referral bonus points and 
     * add data to respective tables also send email notification to user who gets referral bonus
     *  
     * @param type Array of format array('userId'=>User id of user who is placing an order ,
     * 'paidAmount' => amount for which referral bonus will be calculated);
     * @return boolean|string
     */
    public static function calculateReferralBonus($param) {

        $userId = $param['userId']; // User id of user who is placing an order
        $userInfo = User::find($userId);
        $pointsToAdd = 0;
        /* Check if user exist */
        if (!empty($userInfo)) {
            /* Check if used has used any referral code while registering */
            if ($userInfo->referredBy != '') {
                $referredByUser = $userInfo->referredBy;
                /* Check if referred user already received referral points for this user */
                $isBonusAdded = Fundpointtransaction::where('userId', $referredByUser)->where('senderId', $userId)->where('type', 'A')->where('reason', 'referral_bonus')->count();
                if ($isBonusAdded > 0) {
                    return 'Point Already Added';
                } else {
                    /* Check referral type settings if absolute value or per doller charge set */
                    $chekReferralsettingsType = Generalsettings::where('settingsKey', 'refferal_pont_type')->first();
                    if ($chekReferralsettingsType->settingsValue == '0') {
                        /* If absolute value is set fund with absolute value */
                        $absolutePoint = Generalsettings::where('settingsKey', 'absolute_value')->first();
                        $pointsToAdd = $absolutePoint->settingsValue;
                    } else {
                        /* If point per amount doller is set */
                        $pointAddSettings = Generalsettings::where('settingsKey', 'perdollar_value')->first();
                        $pointAddSettingsParts = explode(':', $pointAddSettings->settingsValue);
                        $pointAddDoller = $pointAddSettingsParts[0]; // Doller value
                        $pointAddPerSettingsDoller = $pointAddSettingsParts[1]; //point value
                        $maxPoints = Generalsettings::where('settingsKey', 'maxpoints_value')->first();
                        /* Calculate referral point value and check if it exceeds maximum point (as set in configuration) */
                        $pointsToAdd = min($maxPoints->settingsValue, (floor($param['paidAmount'] / $pointAddDoller) * $pointAddPerSettingsDoller));
                    }
                }

                /* Check if there is points to fund to user */
                if ($pointsToAdd != 0) {
                    $isPointDataExist = Fundpoint::where('userId', $referredByUser)->count();

                    if ($isPointDataExist > 0) {
                        /* If point exist for user then update */
                        Fundpoint::where('userId', $referredByUser)->increment('point', $pointsToAdd);
                    } else {
                        /* If point not exist insert row */
                        $fundPoint = new Fundpoint;
                        $fundPoint->userId = $referredByUser;
                        $fundPoint->point = $pointsToAdd;
                        $fundPoint->save();
                    }
                    /* Insert point transaction record */
                    $pointTransaction = new Fundpointtransaction;
                    $pointTransaction->userId = $referredByUser;
                    $pointTransaction->senderId = $userId;
                    $pointTransaction->point = $pointsToAdd;
                    $pointTransaction->reason = 'referral_bonus';
                    $pointTransaction->type = 'A';
                    $pointTransaction->date = Config::get('constants.CURRENTDATE');
                    $pointTransaction->save();

                    /* Send user notification email */
                    $referralUser = User::find($referredByUser);
                    $emailTemplate = array();
                    $to = $referralUser->email;
                    $replace['[NAME]'] = $referralUser->firstName . ' ' . $referralUser->lastName;
                    $replace['[POINT]'] = $pointsToAdd;
                    //$replace['[CODEUSENAME]'] = $request->firstName . ' ' . $request->lastName;
                    $emailTemplate = Emailtemplate::where('templateKey', 'referral_bonus')->first();
                    customhelper::SendMail($emailTemplate, $replace, $to);

                    return TRUE;
                } else {
                    return 'No point to add';
                }
            } else
                return 'No referral exist';
        } else
            return 'User not exist';
    }

}
