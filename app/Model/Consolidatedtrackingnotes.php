<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Consolidatedtrackingnotes extends Model {

    public $table;
    protected $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.CONSOLIDATEDNOTES');
        $this->prefix = DB::getTablePrefix();
    }

    

}
