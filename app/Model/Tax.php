<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

//use App\libraries\helpers;
//use App\libraries\dbHelpers;

class Tax extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.TAXSYSTEM');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to fetch tax list
     * @param array $param
     * @return object
     */
    public static function getTaxList($param) {
        //$tax = new Tax;

        $where = "1";
        $resultSet = Tax::where('deleted', '0')->orderBy($param['field'], $param['type'])->paginate($param['searchDisplay']);

        //    dd(DB::getQueryLog()); 

        return $resultSet;
    }

    /**
     * Method used to change tax status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Tax::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Tax::where('id', $id)->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }


    /**
     * Method used to fetch data for Excel
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function getAdminUserListExcel($param) { 
        $where = '1';
        $string = '';
        foreach($param as $key => $val){
            if($val == 'taxName'){
                $string .= DB::getTablePrefix().Config::get('constants.dbTable.TAXSYSTEM').'.taxName as `Tax Name`, ';
            }
            if($val == 'addressType'){
                $string .= 'IF('.DB::getTablePrefix().Config::get('constants.dbTable.TAXSYSTEM').".addressType  = 'S', 'Shipping', 'Billing') as `Rates Depend On`, ";
            }
            if($val == 'priority'){
                $string .= DB::getTablePrefix().Config::get('constants.dbTable.TAXSYSTEM').".priority as `Priority`, ";
            }
            if($val == 'regNumber'){
                $string .= DB::getTablePrefix().Config::get('constants.dbTable.TAXSYSTEM').".regNumber as `Tax Registration Number`, ";
            }
            if($val == 'taxDisplayName'){
                $string .= DB::getTablePrefix().Config::get('constants.dbTable.TAXSYSTEM').".taxDisplayName as `Tax Display Name`, ";
            }
            
        }
        $string_trimmed = substr(trim("$string"), 0, -1);
        #dd($string_trimmed);
        $resultSet = Tax::whereRaw($where)->where('deleted', '0')->addSelect(array(DB::raw($string_trimmed))); 
        return $resultSet;
    }

}
