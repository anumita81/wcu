<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Paymenttransaction extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {

        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.TRANSACTIONS');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to pay through PayPal Pro DoDirect payment gateway
     * @param request $request 
     * @return string
     */
    public static function paypaypalpro($checkoutData) {

        /* GET SETTINGS VALUES */

        $getSettings = \App\Model\Paymentgatewaysettings::where("paymentGatewayId", '1')->get();

        /* CALL FUNCTION TO GET THE DEFAULT CURRENCY */
        $api_version = '85.0';
        $api_endpoint = $getSettings[4]->configurationValues;
        $api_username = $getSettings[1]->configurationValues;
        $api_password = $getSettings[2]->configurationValues;
        $api_signature = $getSettings[3]->configurationValues;

        /* SET VARIABLES */
        $cardNumber = $checkoutData['cardNumber'];
        $expMonth = $checkoutData['expMonth'];
        $expYear = $checkoutData['expYear'];
        $cardCode = $checkoutData['cardCode'];
        $expMonthYear = $expMonth . "" . $expYear;

        /* SET THE AMOUNT IN USD FORMAT */
        if ($checkoutData['defaultCurrency'] != 'USD')
            $currencyExchangeRate = Currency::currencyExchangeRate($checkoutData['defaultCurrency'], 'USD');
        else
            $currencyExchangeRate = 1;

        $amount = round(($checkoutData['amount'] * $currencyExchangeRate), 2);

        /* SET THE CUSTOMER DETAILS */
        $customerFirstName = $checkoutData['customerFirstName'];
        $customerLastName = $checkoutData['customerLastName'];
        /* $customerAddress = $checkoutData['customerAddress'];
          $customerCity = $checkoutData['customerCity'];
          $customerState = $checkoutData['customerState'];
          $customerZip = $checkoutData['customerZip'];
          $customerCountry = $checkoutData['customerCountry']; */
        $customerAddress = "14 Main Street";
        $customerCity = "Pecan Springs";
        $customerState = "TX";
        $customerZip = "44628";
        $customerCountry = "USA";

        /* SET OTHER VARIABLES */
        $description = "From ShoptoMyDoor";


        $request_params = array
            (
            'METHOD' => 'DoDirectPayment',
            'USER' => $api_username,
            'PWD' => $api_password,
            'SIGNATURE' => $api_signature,
            'VERSION' => $api_version,
            'PAYMENTACTION' => 'Sale',
            'IPADDRESS' => $_SERVER['REMOTE_ADDR'],
            'ACCT' => $cardNumber,
            'EXPDATE' => $expMonthYear,
            'CVV2' => $cardCode,
            'FIRSTNAME' => $customerFirstName,
            'LASTNAME' => $customerLastName,
            'STREET' => $customerAddress,
            'CITY' => $customerCity,
            'STATE' => $customerState,
            'COUNTRYCODE' => $customerCountry,
            'ZIP' => $customerZip,
            'AMT' => $amount,
            'CURRENCYCODE' => 'USD',
            'DESC' => $description
        );

        $nvp_string = '';
        foreach ($request_params as $var => $val) {
            $nvp_string .= '&' . $var . '=' . urlencode($val);
        }


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_VERBOSE, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_URL, $api_endpoint);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string);

        $result = curl_exec($curl);
        curl_close($curl);

        parse_str($result, $nvp_response_array);
        
        if (isset($nvp_response_array["ACK"])) {
            $message = $nvp_response_array;
        } else {
            $message = 'error';
        }

        return $message;
    }

    /**
     * Method used to get the default currency 
     * @param integer $id 
     * @return array
     */
    public static function getSettingsDetails($id) {

        /* CALL FUNCTION TO GET THE SETTINGS */
        $dataSettings = Paymentmethod::configurationSettings($id)->toArray();

        $data = array();

        if ($id == 2) {
            foreach ($dataSettings as $key => $val) {

                if ($val['configurationKeys'] == 'API Login ID') {
                    $API_Key = $val['configurationValues'];
                }

                if ($val['configurationKeys'] == 'Transaction key') {
                    $Transaction_Key = $val['configurationValues'];
                }

                if ($val['configurationKeys'] == 'Test/Live mode') {
                    $Url = $val['configurationValues'];

                    if ($Url == "Test") {
                        $setUrl = "https://apitest.authorize.net/xml/v1/request.api";
                    } else {
                        $setUrl = "https://api.authorize.net/xml/v1/request.api";
                    }
                }
            }

            $data['setUrl'] = $setUrl;
            $data['API_Key'] = $API_Key;
            $data['Transaction_Key'] = $Transaction_Key;
        } else {
            foreach ($dataSettings as $key => $val) {

                if ($val['configurationKeys'] == 'Test/Live mode') {
                    $Url = $val['configurationValues'];
                    if ($Url == "Test") {
                        $setUrl = "https://api-3t.sandbox.paypal.com/nvp";
                    } else {
                        $setUrl = "https://api-3t.paypal.com/nvp";
                    }
                }

                if ($val['configurationKeys'] == 'API access username') {
                    $API_Key = $val['configurationValues'];
                }

                if ($val['configurationKeys'] == 'API access password') {
                    $API_Password = $val['configurationValues'];
                }

                if ($val['configurationKeys'] == 'API signature') {
                    $API_Signature = $val['configurationValues'];
                }
            }

            $data['setUrl'] = $setUrl;
            $data['API_Key'] = $API_Key;
            $data['API_Password'] = $API_Password;
            $data['API_Signature'] = $API_Signature;
        }

        return $data;
    }

    /**
     * Method used to pay through Authorize.net payment gateway
     * @param array $checkoutData 
     * @return string
     */
    public static function payauthorizedotnet($checkoutData) {
        $response = array();

        /* GET SETTINGS VALUES */
        $getSettings = \App\Model\Paymentgatewaysettings::where("paymentGatewayId", '2')->get();


        /* SET VARIABLES */
        $cardNumber = $checkoutData['cardNumber'];
        $expMonth = $checkoutData['expMonth'];
        $expYear = $checkoutData['expYear'];
        $cardCode = $checkoutData['cardCode'];
        $expMonthYear = $expYear . "-" . $expMonth;

        $url = $getSettings[3]->configurationValues;
        $API_Key = $getSettings[0]->configurationValues;
        $transactionKey = $getSettings[1]->configurationValues;

        /* SET THE AMOUNT IN USD FORMAT */
        if ($checkoutData['defaultCurrency'] != 'USD')
            $currencyExchangeRate = Currency::currencyExchangeRate($checkoutData['defaultCurrency'], 'USD');
        else
            $currencyExchangeRate = 1;

        $amount = round(($checkoutData['amount'] * $currencyExchangeRate), 2);

        /* SET THE CUSTOMER DETAILS */
        $customerFirstName = $checkoutData['customerFirstName'];
        $customerLastName = $checkoutData['customerLastName'];
        $shippingFirstName = $checkoutData['shippingFirstName'];
        $shippingLastName = $checkoutData['shippingLastName'];
        $customerAddress = substr($checkoutData['customerAddress'],0,55);
        $customerCity = $checkoutData['customerCity'];
        $customerState = $checkoutData['customerState'];
        $customerZip = !empty($checkoutData['customerZip']) ? $checkoutData['customerZip'] : "";
        $customerCountry = $checkoutData['customerCountry'];
        $customerShippingAddress = substr($checkoutData['customerShippingAddress'],0,55);
        $customerShippingCity = $checkoutData['customerShippingCity'];
        $customerShippingState = $checkoutData['customerShippingState'];
        $customerShippingZip = $checkoutData['customerShippingZip'];
        $customerShippingCountry = $checkoutData['customerShippingCountry']; 
//        $customerAddress = "14 Main Street";
//        $customerCity = "Pecan Springs";
//        $customerState = "TX";
//        $customerZip = "44628";
//        $customerCountry = "USA";
//        $customerShippingAddress = "14 Main Street";
//        $customerShippingCity = "Pecan Springs";
//        $customerShippingState = "TX";
//        $customerShippingZip = "44628";
//        $customerShippingCountry = "USA";

        /* SET OTHER VARIABLES */
        if(empty($checkoutData['invoiceNumber']))
            $invoiceNumber = "INV-" . rand(00, 99) . "-" . time();
        else
            $invoiceNumber = "shipment-".$checkoutData['invoiceNumber'];
        
        $description = "From ShoptoMyDoor";


        $query = <<<EOT
<createTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
  <merchantAuthentication>
    <name>$API_Key</name>
    <transactionKey>$transactionKey</transactionKey>
  </merchantAuthentication>
  <transactionRequest>
    <transactionType>authCaptureTransaction</transactionType>
    <amount>$amount</amount>
    <payment>
      <creditCard>
        <cardNumber>$cardNumber</cardNumber>
        <expirationDate>$expMonthYear</expirationDate>
        <cardCode>$cardCode</cardCode>
      </creditCard>
    </payment>
    <order>
     <invoiceNumber>$invoiceNumber</invoiceNumber>
     <description>$description</description>
    </order>
    <billTo>
      <firstName>$customerFirstName</firstName>
      <lastName>$customerLastName</lastName>
      <company></company>
      <address>$customerAddress</address>
      <city>$customerCity</city>
      <state>$customerState</state>
      <zip>$customerZip</zip>
      <country>$customerCountry</country>
    </billTo>
    <shipTo>
      <firstName>$customerFirstName</firstName>
      <lastName>$customerLastName</lastName>
      <company></company>
      <address>$customerShippingAddress</address>
      <city>$customerShippingCity</city>
      <state>$customerShippingState</state>
      <zip>$customerShippingZip</zip>
      <country>$customerShippingCountry</country>
    </shipTo>
  </transactionRequest>
</createTransactionRequest>
EOT;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$query");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        $xml = @simplexml_load_string($output, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $response = json_decode($json, TRUE);

        return $response;
    }

    public static function paystack($reference) {
        //$url = "https://api.paystack.co/transaction/verify/" . $reference;
        //$key = "sk_test_9d2b7788b8810d03d8c7a7315b7107f609c4ab40";

        /* GET SETTINGS VALUES */
        $getSettings = \App\Model\Paymentgatewaysettings::where("paymentGatewayId", '9')->get();

        $url = $getSettings[0]->configurationValues . $reference;
        $key = $getSettings[3]->configurationValues;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_GET, 1);
        //curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $headers = [
            'Authorization: Bearer ' . $key,
            'Content-Type: application/json',
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $request = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($request, true);
        if (!empty($result['status']) && $result['status'] == '1')
            return true;
        else
            return FALSE;
    }

    public static function getPaymentHistory($param) {
        $paymenttransaction = new Paymenttransaction;
        $procurement = new Procurement;
        $shipment = new Shipment;
        $autoshipment = new Autoshipment;
        $paymentMethod = new Paymentmethod;
        $fillnship = new Fillship;

        $resultSet = Paymenttransaction::select(array("$paymenttransaction->table.*", "$procurement->table.paidCurrencyCode", "$procurement->table.exchangeRate", "$autoshipment->table.paidCurrencyCode AS autoPaidCurrencyCode", "$autoshipment->table.exchangeRate AS autoExchangeRate", "$paymentMethod->table.paymentMethod","$shipment->table.exchangeRate as shipmentExchangeRate","$shipment->table.paidCurrencyCode as shipmentPaidCurrency","$fillnship->table.exchangeRate as fillnshipExchangeRate","$fillnship->table.paidCurrencyCode as fillnshipPaidCurrency"))
                        ->leftJoin($procurement->table, "$paymenttransaction->table.paidForId", '=', "$procurement->table.id")
                        ->leftJoin($shipment->table, "$paymenttransaction->table.paidForId", '=', "$shipment->table.id")
                        ->leftJoin($autoshipment->table, "$paymenttransaction->table.paidForId", '=', "$autoshipment->table.id")
                        ->leftJoin($fillnship->table, "$paymenttransaction->table.paidForId", '=', "$fillnship->table.id")
                        ->leftJoin($paymentMethod->table, "$paymenttransaction->table.paymentMethodId", '=', "$paymentMethod->table.id")
                        ->where($paymenttransaction->table . '.userId', $param['userId'])
                        ->orderby('id', 'desc')
                        ->take($param['perPage'])
                        ->skip($param['offset'])->get();

        return $resultSet;
    }
    
    public static function processpayeezy($param) {

        /* GET SETTINGS VALUES */
        $getSettings = \App\Model\Paymentgatewaysettings::where("paymentGatewayId", '11')->get();
        
        /*$serviceURL = 'https://api-cert.payeezy.com/v1/transactions';
        $apiKey = "pOoAOXqd3ygP1AgVrpnUFvAtfG2xiJhv";
        $apiSecret = "1bf66a81f3652db20349123d8dcfb4c008c57ac7bb4e4b0d9764a6f7c00f57b4";
        $token = "fdoa-df022c768f40f1c7c1e346ef7352b36d2fa7a8d9fb6b40fd";*/
        
        $serviceURL = $getSettings[0]->configurationValues;
        $apiKey = $getSettings[1]->configurationValues;
        $apiSecret = $getSettings[3]->configurationValues;
        $token = $getSettings[4]->configurationValues;
        
        
        $nonce = strval(hexdec(bin2hex(openssl_random_pseudo_bytes(4))));
        $timestamp = strval(time()*1000); //time stamp in milli seconds
        $merchant_ref = "";

        $data = array(
            'merchant_ref' => (!empty($param['shipmentId']) ? $param['shipmentId'] : ""),
            'transaction_type' => "purchase",
            'method' => 'credit_card',
            'amount' => $param['amount'],
            "partial_redemption" => "false",
            'currency_code' => $param['currency_code'],
            'credit_card' => array(
                'type' => $param['type'],
                'cardholder_name' => $param['cardholder_name'],
                'card_number' => $param['card_number'],
                'exp_date' => $param['exp_date'],
                'cvv' => $param['cvv'],
            ),
            'level2' => array(
                'customer_ref' => (!empty($param['userUnit']) ? $param['userUnit'] : "")
            )
        );
        $payload = json_encode($data, JSON_FORCE_OBJECT);

        $data = $apiKey . $nonce . $timestamp . $token . $payload;
        $hashAlgorithm = "sha256";
        $hmac = hash_hmac($hashAlgorithm, $data, $apiSecret, false);
        $hmac_enc = base64_encode($hmac);

        $curl = curl_init($serviceURL);

        $headers = array(
            'Content-Type: application/json',
            'apikey:' . strval($apiKey),
            'token:' . strval($token),
            'Authorization:' . $hmac_enc,
            'nonce:' . $nonce,
            'timestamp:' . $timestamp,
        );

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);

        curl_setopt($curl, CURLOPT_VERBOSE, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $json_response = curl_exec($curl);



        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $response = json_decode($json_response, true);

        curl_close($curl);
        
        return $response;

    }

}
