<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Fillshipbox extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.FILLSHIPBOXES');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to fetch fill&ship box list
     * @param array $param
     * @return object
     */
    public static function getList($param) {
        $where = '1';

        $resultSet = Fillshipbox::whereRaw($where)
                ->addSelect(array('id', 'name', 'clearingAndDuty', 'warehousing', 'orderby', 'deleted'))
                ->addSelect(DB::raw('CONCAT(length, "x", width, "x", height) AS dimension'))
                ->where(Config::get('constants.dbTable.FILLSHIPBOXES') . '.deleted', '0')
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to fetch single fill&ship box record
     * @param int $id
     * @return object
     */
    public static function getListById($id) {
        $resultSet = Fillshipbox::where('id', $id)->where('deleted', '0')->get();
        return $resultSet;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Fillshipbox::where('id', $id)->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

}
