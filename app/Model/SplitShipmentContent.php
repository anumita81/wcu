<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Config;

class SplitShipmentContent extends Model
{
	public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended

        $this->table = Config::get('constants.dbTable.SPLITSHIPMENTCONTENTS');
    }

	public function splitShipmentTypes(){

		return array('s'=>'Sea Shipping','a'=>'Air Shipping','r'=>'Return Shipping','n'=>'Note');
	}

    public function getData($sortOrder = '') {

        if($sortOrder != '')
            $resultSet = SplitShipmentContent::orderBy('splitShipmentType',$sortOrder)->get();
        else
    	   $resultSet = SplitShipmentContent::orderBy('id','desc')->get();
    	//print_r($resultSet);
    	return $resultSet;

    }
}
