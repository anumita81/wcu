<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
class Warehouse extends Model
{
   
    public $table;
    public $timestamps = false;
    protected $prefix;
    protected $fillable = ['address', 'address2', 'cityId', 'stateId', 'countryId', 'zipcode', 'rate'];



    public function __construct() {
        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.WAREHOUSE');
        $this->prefix = DB::getTablePrefix();
    }

    public function country() {
        return $this->hasOne('App\Model\Country', 'id', 'countryId');
    }

    public function state() {
        return $this->hasOne('App\Model\State', 'id', 'stateId');
    }

    public function city() {
        return $this->hasOne('App\Model\City', 'id', 'cityId');
    }

    /**
     * Get the warehouses associated with the country
     * @return object
     */
    public static function warehouseCountryMap() {
        $warehouseMaster = new Warehouse();
        $warehouseMasterTable = $warehouseMaster->table;

        $countries = new Country();
        $countriesTable = $countries->table;
        
        $city = new City();
        $citiesTable = $city->table;

        $resultSet = Warehouse::leftJoin($countriesTable, $countriesTable.'.id', '=', $warehouseMasterTable.'.countryId')
        ->leftJoin($citiesTable, $citiesTable.'.id', '=', $warehouseMasterTable.'.cityId')
        ->select(array($warehouseMasterTable.'.id', $countriesTable.'.name', $countriesTable.'.code', $warehouseMasterTable.'.countryId as countryId',DB::raw($city->prefix.$citiesTable.".name as cityName")))->orderBy('id')
        ->where($warehouseMasterTable.'.deleted', '0')
        ->get();

        return $resultSet;
    }
    
    /**
     * Get the warehouses associated with the country
     * @return object
     */
    public static function countryWarehoseData() {
        $warehouseMaster = new Warehouse();
        $warehouseMasterTable = $warehouseMaster->table;

        $countries = new Country();
        $countriesTable = $countries->table;

        $resultSet = Warehouse::leftJoin($countriesTable, $countriesTable.'.id', '=', $warehouseMasterTable.'.countryId')
        ->select(array($warehouseMasterTable.'.id', $countriesTable.'.name', $countriesTable.'.code', $warehouseMasterTable.'.countryId as countryId' ))->orderBy('id')
        ->where($warehouseMasterTable.'.deleted', '0')
        ->groupBy($warehouseMasterTable.'.countryId')
        ->get();

        return $resultSet;
    }

/**
     * Get the warehouses associated with the country
     * @return object
     */
    public static function warehouseData() {
        $warehouseMaster = new Warehouse();
        $warehouseMasterTable = $warehouseMaster->table;
        $city = new City();
        $countries = new Country();
        $countriesTable = $countries->table;
        $citiesTable = $city->table;

        $resultSet = Warehouse::leftJoin($countriesTable, $countriesTable.'.id', '=', $warehouseMasterTable.'.countryId')
        ->leftJoin($citiesTable, $citiesTable.'.id', '=', $warehouseMasterTable.'.cityId')
        ->select(array($warehouseMasterTable.'.id', $countriesTable.'.name', $countriesTable.'.code', $warehouseMasterTable.'.countryId as countryId', DB::raw($city->prefix.$citiesTable.".name as cityName" )))->orderBy('id')
        ->where($warehouseMasterTable.'.deleted', '0')
        ->groupBy($warehouseMasterTable.'.countryId')
        ->get();

        return $resultSet;
    }

    /**
     * Get the warehouses associated with the country
     * @return object
     */
    public static function getUserWarehouseList($userId) {
        $warehouseMaster = new Warehouse();
        $warehouseMasterTable = $warehouseMaster->table;
        $countries = new Country();
        $city = new City();
        $countriesTable = $countries->table;
        $citiesTable = $city->table;
        $addressbook = Addressbook::where('userId',$userId)->where('isDefaultShipping',1)->first();
        $resultSet = array();

        if(!empty($addressbook))
        {
            $resultSet = Warehouse::leftJoin($countriesTable, $countriesTable.'.id', '=', $warehouseMasterTable.'.countryId')
            ->leftJoin($citiesTable, $citiesTable.'.id', '=', $warehouseMasterTable.'.cityId')
            ->select(array($warehouseMasterTable.'.id', $countriesTable.'.name', $countriesTable.'.code', $warehouseMasterTable.'.image',DB::raw($city->prefix.$citiesTable.".name as cityName" )))->orderBy('id')
            ->where($warehouseMasterTable.'.deleted', '0')
            ->whereRaw("FIND_IN_SET($addressbook->countryId, shipToCountries)")
            ->get();
        }

        return $resultSet;
    }

    /**
     * Update the warehouses 
     * @param integer $id
     * @param integer $wId
     * @return object
     */
    public static function updateWarehouseCountryMap($id, $wId){

        Adminwarehousemapping::insert(
         array(
            'adminUserId'       =>   $id, 
            'warehouseId'       =>   $wId
         ));

    }

    /**
     * Delete the warehouse 
     * @param integer $id
     * @return object
     */
    public static function deleteWarehouseCountryMap($id){
        Adminwarehousemapping::where('adminUserId', '=', $id)->delete();

    }

    /**
     *  Get the warehouse list
     * @param array $param
     * @return object
     */
    public static function getWarehouseList($param) {
        $city = new City;
        $country = new Country;
        $state = new State;
        $warehouse = new Warehouse;

        $resultSet = Warehouse::select(array("$warehouse->table.id", "$city->table.name AS cityName", "$warehouse->table.status", "$country->table.name AS countryName", "$state->table.name AS stateName", "$warehouse->table.address", "$warehouse->table.address2", "$warehouse->table.zipcode", "$warehouse->table.image", "$warehouse->table.rate",))
                ->leftJoin($country->table, "$warehouse->table.countryId", '=', "$country->table.id")
                ->leftJoin($state->table, "$warehouse->table.stateId", '=', "$state->table.id")
                ->leftJoin($city->table, "$warehouse->table.cityId", '=', "$city->table.id")
                ->where($warehouse->table . '.deleted', '0')
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);
        return $resultSet;
    }

    /**
     * Method used to change Country status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Warehouse::where('id', $id)
                ->update(array('status' => $newStatus));

        return $row;
    }
     /**
     * Get the warehouses associated with the country
     * @return object
     */
    public static function getWarehouseRecord($userId, $warehouseId) {
        $warehouseMaster = new Warehouse();
        $warehouseMasterTable = $warehouseMaster->table;
        $countries = new Country();       
        $state = new State;
        $city = new City();
        $countriesTable = $countries->table;
        $citiesTable = $city->table;
        $stateTable = $state->table;
        $addressbook = Addressbook::where('userId',$userId)->where('isDefaultShipping',1)->first();
        $resultSet = array();

        if(!empty($addressbook))
        {
            $resultSet = Warehouse::leftJoin($countriesTable, $countriesTable.'.id', '=', $warehouseMasterTable.'.countryId')
            ->leftJoin($citiesTable, $citiesTable.'.id', '=', $warehouseMasterTable.'.cityId')
            ->leftJoin($state->table, $warehouseMasterTable.".stateId", '=', "$state->table.id")
            ->where($warehouseMasterTable.'.deleted', '0')
            ->where($warehouseMasterTable.'.id', $warehouseId)
            ->whereRaw("FIND_IN_SET($addressbook->countryId, shipToCountries)")            
            ->select(array($warehouseMasterTable.".countryId as countryId", "$city->table.id AS cityId", "$state->table.id AS stateId", "$city->table.name AS cityName", "$countries->table.name AS countryName", "$state->table.name AS stateName"))->get();
        }

        return $resultSet;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId='') {
        if (empty($id))
            return false;

        $row = false;

        $row = Warehouse::where('id', $id)
                ->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }
}
