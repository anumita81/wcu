<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Permission extends Model
{
    protected $table = 'adminmenumaster';
    public $timestamps = false;
    protected $fillable = ['id', 'adminMenuName', 'adminMenuParentId', 'adminMenuIcon', 'status', 'adminMenuSlug', 'adminMenuGroupName', 'adminMenuOrder'];

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
    }

    public static function menuCreation($adminMenuId, $userTypeId){

        $resultSet =  DB::table('adminmenudefaultpermission')->insert(
                ['adminMenuId' => $adminMenuId, 'userTypeId' => $userTypeId, 'adminMenuPermission' => 0]
            );

        return $resultSet;
    }

    public static function getPermissionRecords($id, $fieldType) {

        $records = DB::table('adminmenudefaultpermission')->select('adminMenuId', 'id', 'userTypeId', 'permissionView', 'permissionAdd', 'permissionEdit', 'permissionDelete')
        ->where($fieldType, '=', $id)
        ->orderBy('id', 'ASC')
        ->get();
        return $records;
    }

    
}
