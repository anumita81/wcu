<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class County extends Model {

    public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.COUNTY');
    }

    /**
     * Method used to fetch county list
     * @param array $param
     * @return object
     */
    public static function getCountyList($param) {
        $where = 'deleted = "0"';

        if (!empty($param['searchByState']))
            $where .= "  AND stateId ='" . $param['searchByState'] . "'";

        $resultSet = County::whereRaw($where)
                ->select(array('id', 'name', 'status'))
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to change county status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = County::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = County::where('id', $id)
                ->update(array('deleted' => 1, 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to fetch county list for ajax
     * @param array $param
     * @return object
     */
    public static function getCountyData($param = 'all') {
        $where = 'status="1"';

        if (!empty($param != 'all'))
            $where .= "  AND stateId ='" . $param . "'";

        $resultSet = DB::table('counties')
                ->join('states', 'states.id', '=', 'counties.stateId')
                ->join('countries', 'countries.code', '=', 'states.countryCode')
                ->select('countries.name as country_name', 'states.name as state_name', 'counties.id', 'counties.name')
                ->where('counties.status', 1)
                ->get();

        return $resultSet;
    }

}
