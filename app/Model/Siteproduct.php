<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Config;

class Siteproduct extends Model
{
    public $table;
    public $timestamps = false;

    public function __construct() {

        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.SITEPRODUCT');
    }

    public function getData($param) {

    	$where = '1';

        if (!empty($param['searchData']))
        {
        	if(!empty($param['searchData']['productName']))
            	$where .= "  AND productName like '" . $param['searchData']['productName'] . "%'";
            if(!empty($param['searchData']['weight']))
            	$where .= "  AND weight= '" . $param['searchData']['weight'] . "'";
            if(!empty($param['searchData']['categoryId']))
            	$where .= "  AND categoryId= '" . $param['searchData']['categoryId'] . "'";
            if(!empty($param['searchData']['subCategoryId']))
            	$where .= "  AND subCategoryId= '" . $param['searchData']['subCategoryId'] . "'";
        }
        
        /* Print query ->toSql() dd($resultSet); */
        $resultSet = Siteproduct::whereRaw($where)
                ->orderBy($param['sortField'], $param['sortOrder'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    public static function getListByCatid($catId){
        $where = '1';
        $resultSet = Siteproduct::whereRaw($where)->where('categoryId', $catId)->where('status', '1')->get();
        return $resultSet;
    }
    
     public static function getListBySubCatid($catId){
        $where = '1';
        $resultSet = Siteproduct::whereRaw($where)->where('subCategoryId', $catId)->where('status', '1')->get();
        return $resultSet;
    }
    
    public static function getSubcategoryMappedProduct() {
        
        $products = Siteproduct::where('status', '1')->get();
        $resultSet = array();
        
        if(!empty($products))
        {
            foreach($products as $eachProduct)
            {
                $resultSet[$eachProduct->subCategoryId][$eachProduct->id] = $eachProduct->productName;
            }
        }
        
        return $resultSet;
    }
}
