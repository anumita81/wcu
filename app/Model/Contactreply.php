<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Contactreply extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.CONTACTREPLY');
        $this->prefix = DB::getTablePrefix();
    }


    /**
     * Method used to fetch replied records with admin user name
     * @param array $param
     * @return object
     */
    public static function getList($id) {
        $replied = new Contactreply;
        $adminuser = new UserAdmin;

        $where = "1";

        $repliedTable = $replied->prefix . $replied->table;
        $adminuserTable = $adminuser->prefix . $adminuser->table;

        $resultSet = Contactreply::select(array("$replied->table.id", "$replied->table.replyText", 
            "$replied->table.repliedBy", "$replied->table.repliedOn", "$adminuser->table.firstName", "$adminuser->table.lastName"))
                ->leftJoin($adminuser->table, "$replied->table.repliedBy", '=', "$adminuser->table.id")
                ->where("$replied->table.contactusId", $id)
                ->orderby("$replied->table.id", 'desc')->get();

        return $resultSet;
    }
}
