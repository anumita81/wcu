<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Emailtemplate extends Model {

    public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.EMAILTEMPLATE');
    }

    /**
     * Method used to fetch email template list
     * @param array $param
     * @return object
     */
    public static function getEmailTemplateList($param) {
        $where = "deleted='0' AND status='1'";

        if (!empty($param['searchByType']))
            $where .= " AND templateType = '" . $param['searchByType'] . "'";
      

        $resultSet = Emailtemplate::whereRaw($where)
                ->select(array('id', 'templateKey', 'templateSubject', 'templateBody', 'fromName', 'fromEmail', 'createdOn', 'createdBy', 'updatedOn', 'updatedBy', 'deleted', 'status'))
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

}
