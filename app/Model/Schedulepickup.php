<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Schedulepickup extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    protected $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.SCHEDULEPICKUP');
        $this->imageTable = Config::get('constants.dbTable.SCHEDULEIMAGE');
        $this->prefix = DB::getTablePrefix();
    }

    /*  public function addressbook() {
      return $this->hasMany('App\Model\Addressbook', 'userId', 'id');
      }
     */

    /**
     * Method used to fetch users list
     * @param array $param
     * @return object
     */
    public static function getScheduleList($param, $type = '') {
        $schedulepickup = new Schedulepickup;
        $user = new User;
        $city = new City;
        $country = new Country;
        $state = new State;

        $scheduleTable = $schedulepickup->prefix . $schedulepickup->table;
        $userTable = $schedulepickup->prefix . $user->table;

        $where = "$scheduleTable.deleted = '0' AND $scheduleTable.status = '1'"; 

        if (!empty($param['searchSchedule']['pickuptype']))
            $where .= " AND $scheduleTable.pickupType = " . $param['searchSchedule']['pickuptype'];

        if (!empty($param['searchSchedule']['pickupCountry']))
            $where .= " AND $scheduleTable.scheduleFromCountry = " . $param['searchSchedule']['pickupCountry'];

        if (!empty($param['searchSchedule']['pickupState']))
            $where .= " AND $scheduleTable.scheduleFromState = '" . $param['searchSchedule']['pickupState'] . "'";

        if (!empty($param['searchSchedule']['pickupCity']))
            $where .= " AND $scheduleTable.scheduleFromCity = '" . $param['searchSchedule']['pickupCity'] . "'";

        if (!empty($param['searchSchedule']['pickupName']))
            $where .= " AND $scheduleTable.scheduleFromName = '" . $param['searchSchedule']['pickupName'] . "'";

        if (!empty($param['searchSchedule']['pickupPhone']))
            $where .= " AND $scheduleTable.scheduleFromPhone = '" . $param['searchSchedule']['pickupPhone'] . "'";

        if (!empty($param['searchSchedule']['pickupEmail']))
            $where .= " AND $scheduleTable.scheduleFromEmail = '" . $param['searchSchedule']['pickupEmail'] . "'";

        if (!empty($param['searchSchedule']['scheduleTocountry']))
            $where .= " AND $scheduleTable.scheduleTocountry = '" . $param['searchSchedule']['scheduleTocountry'] . "'";

        if (!empty($param['searchSchedule']['scheduleToState']))
            $where .= " AND $scheduleTable.scheduleToState = '" . $param['searchSchedule']['scheduleToState'] . "'";

        if (!empty($param['searchSchedule']['scheduleToCity']))
            $where .= " AND $scheduleTable.scheduleToCity = '" . $param['searchSchedule']['scheduleToCity'] . "'";

        if (!empty($param['searchSchedule']['scheduleToName']))
            $where .= " AND $scheduleTable.scheduleToName = '" . $param['searchSchedule']['scheduleToName'] . "'";

        if (!empty($param['searchSchedule']['scheduleToPhone']))
            $where .= " AND $scheduleTable.scheduleToPhone = '" . $param['searchSchedule']['scheduleToPhone'] . "'";

        if (!empty($param['searchSchedule']['scheduleToEmail']))
            $where .= " AND $scheduleTable.scheduleToEmail = '" . $param['searchSchedule']['scheduleToEmail'] . "'";

        if (!empty($param['searchSchedule']['minWeight']))
            $where .= " AND $scheduleTable.totalWeight >= '" . $param['searchSchedule']['minWeight'] . "'";

        if (!empty($param['searchSchedule']['maxWeight']))
            $where .= " AND $scheduleTable.totalWeight <= '" . $param['searchSchedule']['maxWeight'] . "'";

         if (!empty($param['searchSchedule']['minPackage']))
            $where .= " AND $scheduleTable.totalPackages >= '" . $param['searchSchedule']['minPackage'] . "'";

        if (!empty($param['searchSchedule']['maxWeight']))
            $where .= " AND $scheduleTable.totalPackages <= '" . $param['searchSchedule']['maxWeight'] . "'";


        if ($param['searchByCreatedOn'] != '') {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($scheduleTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $scheduleTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($scheduleTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($scheduleTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

       
            $resultSet = Schedulepickup::select(array("$schedulepickup->table.id", "$schedulepickup->table.userId", "$schedulepickup->table.scheduleFromAddress","$user->table.firstName", "$user->table.lastName", "$user->table.email", "$schedulepickup->table.requestedPickupTime", "$schedulepickup->table.totalPackages", "$schedulepickup->table.requestReceivedTime", "$schedulepickup->table.scheduleStatus", "$schedulepickup->table.createdOn", "$city->table.name AS cityName", "$country->table.name AS countryName", "$state->table.name AS stateName"))
                    ->leftJoin($country->table, "$schedulepickup->table.scheduleFromCountry", '=', "$country->table.id")
                    ->leftJoin($state->table, "$schedulepickup->table.scheduleFromState", '=', "$state->table.id")
                    ->leftJoin($city->table, "$schedulepickup->table.scheduleFromCity", '=', "$city->table.id")
                    ->leftJoin($user->table, "$schedulepickup->table.userId", '=', "$user->table.id")
                    ->whereRaw($where)
                    ->groupBy("$schedulepickup->table.id")
                    ->orderBy($param['field'], $param['type'])
                    ->paginate($param['searchDisplay']);
       

        return $resultSet;
    }

    
    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Schedulepickup::where('id', $id)
                ->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    public static function getScheduleDeatil($id)
    {
        $schedulepickup = new Schedulepickup;
        $user = new User;
        $city = new City;
        $country = new Country;
        $state = new State;
        $driver = new Driver;

        $scheduleTable = $schedulepickup->prefix . $schedulepickup->table;
        $userTable = $schedulepickup->prefix . $user->table;


        $resultSet = Schedulepickup::select(array("$schedulepickup->table.id", "$schedulepickup->table.userId", "$schedulepickup->table.scheduleFromAddress","$user->table.firstName", "$user->table.lastName", "$user->table.email", "$schedulepickup->table.totalWeight", "$schedulepickup->table.totalWeightUnit", "$schedulepickup->table.pickupType", "$driver->table.driverName", "$schedulepickup->table.assignedDate", "$schedulepickup->table.requestedPickupTime", "$schedulepickup->table.totalPackages", "$schedulepickup->table.requestReceivedTime", "$schedulepickup->table.scheduleStatus", "$schedulepickup->table.scheduleFromAddress", "$schedulepickup->table.scheduleFromAddress2", "$schedulepickup->table.scheduleFromName", "$schedulepickup->table.scheduleFromCompany", "$schedulepickup->table.scheduleFromPhone", "$schedulepickup->table.scheduleFromEmail", "$schedulepickup->table.scheduleTocountry", "$schedulepickup->table.scheduleToCity", "$schedulepickup->table.scheduleToState", "$schedulepickup->table.scheduleToAddress", "$schedulepickup->table.scheduleToName", "$schedulepickup->table.scheduleToCompany", "$schedulepickup->table.scheduleToAddress2", "$schedulepickup->table.scheduleToPhone", "$schedulepickup->table.scheduleToEmail", "$schedulepickup->table.scheduleFromResidence", "$schedulepickup->table.scheduleFromResidenceCode", "$schedulepickup->table.createdOn", "$city->table.name AS scheduleFromCityName", "$country->table.name AS scheduleFromCountryName", "$state->table.name AS scheduleFromStateName", "$schedulepickup->table.instructions", "$schedulepickup->table.scheduleFromZipcode", "$schedulepickup->table.scheduleToZipcode"))
                    ->leftJoin($country->table, "$schedulepickup->table.scheduleFromCountry", '=', "$country->table.id")
                    ->leftJoin($state->table, "$schedulepickup->table.scheduleFromState", '=', "$state->table.id")
                    ->leftJoin($city->table, "$schedulepickup->table.scheduleFromCity", '=', "$city->table.id")
                    ->leftJoin($user->table, "$schedulepickup->table.userId", '=', "$user->table.id")  
                    ->leftJoin($driver->table, "$schedulepickup->table.assignedDriverId", '=', "$driver->table.id")                   
                    ->where("$schedulepickup->table.id", $id)
                    ->get();



        return $resultSet;
                    

    }

    public static function getImage($schedulepickupid)
    {
        
        
        $resultSet = Scheduleimage::select('*')->where('scheduleId', $schedulepickupid)->get();

        return $resultSet;

    }

}
