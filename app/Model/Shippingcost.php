<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Shippingcost extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.SHIPPINGCOST');
        $this->prefix = DB::getTablePrefix();
    }

    public function make() {
        return $this->hasOne('App\Model\Automake', 'id', 'makeId');
    }

    public function model() {
        return $this->hasOne('App\Model\Automodel', 'id', 'modelId');
    }

    public function zone() {
        return $this->hasOne('App\Model\Zone', 'id', 'zoneId');
    }

    public function pzone() {
        return $this->hasOne('App\Model\Zone', 'id', 'pzoneId');
    }

    public static function getList($param) {
        $shippingcost = new Shippingcost;
        $make = new Automake;
        $model = new Automodel;

        $where = "1";

        $shippingcostTable = $shippingcost->prefix . $shippingcost->table;

        if (!empty($param['searchByMake']))
            $where .= "  AND $shippingcostTable.makeId ='" . $param['searchByMake'] . "'";

        $resultSet = Shippingcost::select(array("$shippingcost->table.id", "$shippingcost->table.shippingOption", "$shippingcost->table.cost", "$make->table.name AS makeName", "$model->table.name AS modelName",))
                ->leftJoin($make->table, "$shippingcost->table.makeId", '=', "$make->table.id")
                ->leftJoin($model->table, "$shippingcost->table.modelId", '=', "$model->table.id")
                ->where($shippingcost->table . '.deleted', '0')
                ->whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    public static function getChargesData($param) {
        $result = array();
        $where = '1';
        if(!empty($param['searchByMake']))
            $where = 'makeId = '.$param['searchByMake'];
        $data = Shippingcost::where('deleted','0')->whereRaw($where)->with('make','model','zone','pzone')->get()->toArray();
        foreach($data as $eachData)
        {
            //dd($eachData);
            $result[$eachData['zoneId']]['name'] = $eachData['zone']['name'];
            //$result[$eachData['zoneId']]['data'][$eachData['pzoneId']]['name'] = $eachData['pzone']['name'];
            $result[$eachData['zoneId']]['data'][$eachData['pzoneId'].$eachData['makeId'].$eachData['modelId']] = $eachData;
        }

        return $result;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Shippingcost::where('id', $id)->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    public static function checkIfDataExist($request,$updateId = '-1') {

        $data = Shippingcost::where('zoneId',$request->zoneId)->where('pzoneId',$request->pzoneId)->where('makeId',$request->makeId)->where('modelId',$request->modelId)->get();
        if(count($data)>0)
            return true;
        else
            return false;

    }

    public static function getShippingCost($itemPrice,$param,$autoData) {

        $shippingCosts = array('totalShippingCost'=>'0.00','insurance'=>'0.00');
        $destinationAddress = array('country' => $param['toCountry'],
            'state' => $param['toState'],
            'city' => $param['toCity'],
        );
        $sourceAddress = array('country' => $param['fromCountry'],
            'state' => $param['fromState'],
            'city' => $param['fromCity'],
        );

        $destinationZone = Zone::getZoneWithAddr($destinationAddress);
        $sourceZone = Zone::getZoneWithAddr($sourceAddress);

        if($autoData['make']!='' && $autoData['model']!='')
        {
            $resultSet = Shippingcost::where('makeId', $autoData['make'])
                ->where('modelId', $autoData['model'])
                ->whereIn('zoneId', $destinationZone)
                ->whereIn('pzoneId', $sourceZone)
                ->first();
            if(!empty($resultSet))
            {
                $shippingCosts['totalShippingCost'] = round(($resultSet->shippingCharge+$resultSet->clearingPortHandlingCharge),2);
                if($itemPrice >'0')
                    $shippingCosts['insurance'] = round(($itemPrice*($resultSet->insurancePercent/100)),2);
            }
        }

        return $shippingCosts;
    }

}
