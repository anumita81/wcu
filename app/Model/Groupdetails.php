<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
use Auth;

class Groupdetails extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.GROUPDETAILS');
        $this->prefix = DB::getTablePrefix();
    }
    
    public static function getUserGroupList($param) {
        $where = '1';
        $user = new User;
        $groupDetails = new Groupdetails;
        $resultSet = Groupdetails::whereRaw($where)
                ->select(array("$groupDetails->table.*","$user->table.firstName","$user->table.lastName"))
                ->join("$user->table","$groupDetails->table.coordinatorId","=","$user->table.id")
                ->orderBy($groupDetails->table.".".$param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }
}