<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Adminmenuuserpermission extends Model {

    public $table;
    public $timestamps = false;
    protected $fillable = ['adminMenuId', 'adminUserId', 'permissionView', 'permissionAdd', 'permissionEdit', 'permissionDelete'];

    public function __construct() {
        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.ADMINUSERROLEPERM');
    }

    /**
     * Get the role from master table 
     * @param integer $menuId
     * @return object
     */
    public static function recordmasterRole($menuId) {

        $resultSet = Adminmenumaster::where('id', '=', $menuId)->get();
        return $resultSet;
    }

}
