<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Shipmenttrackingnumber extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.SHIPMENTTRACKINGNO');
        $this->prefix = DB::getTablePrefix();
    }
    
    public static function getShipmentRecords($trackinNums) {
        
        $shipmentMapping = array();
        foreach($trackinNums as $eachTrackingNums) {
            $shipmentRecord = \App\Model\Shipmentdelivery::where('tracking',$eachTrackingNums->trackingNumber)->orWhere('tracking2',$eachTrackingNums->trackingNumber)->first();
            if(!empty($shipmentRecord))
            {
                $shipmentMapping[$eachTrackingNums->trackingNumber] = $shipmentRecord->shipmentId;
            }
        }
        
        return $shipmentMapping;
    }

    
}
