<?php


namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Autopickupcost extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.AUTOPICKUPCOST');
        $this->prefix = DB::getTablePrefix();
    }

    /** This function is used to fetch and create all pickup cost mapped with city
    * @return <Array>
    */
    public static function cityWiseMappedData() {

        /* Fetch all auto pickup cost */
        $data = Autopickupcost::all();
        $return = array();
        if(!empty($data))
        {
            foreach($data as $eachData)
            {
                // create array in format array(cityid=>autopickupcost)
                $return[$eachData->warehouseId][$eachData->cityId] = $eachData->pickupCost;
            }
        }

        return $return;
    } 

    public static function getPickupCost($warehouseId , $sourceCity) {

        $pickupCost = '0.00';
        if($sourceCity!='')
        {
            $data = Autopickupcost::where('warehouseId',$warehouseId)->where('cityId',$sourceCity)->first();
            if(!empty($data))
                $pickupCost = $data->pickupCost;
        }
        return round($pickupCost,2);
    } 


}