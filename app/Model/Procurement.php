<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Procurement extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.PROCUREMENT');
        $this->prefix = DB::getTablePrefix();
    }

    public static function allStatus() {
        return array('draft' => 'Draft', 'requestforcost' => 'Requested For Cost', 'submitted' => 'Submitted', 'itemreceived' => 'Item Received', 'completed' => 'Completed', 'rejected' => 'Rejected');
    }

    /**
     * Method used to fetch users list
     * @param array $param
     * @return object
     */
    public static function getShopForMeList($param, $type = '', $procurementType = '') {
        $procurement = new Procurement;
        $user = new User;
        $city = new City;
        $state = new State;
        $country = new Country;
        $warehouse = new Warehouse;

        $procurementItem = new Procurementitem;

        $procurementTable = $procurement->prefix . $procurement->table;
        $userTable = $procurement->prefix . $user->table;

        if ($procurementType == '') {
            $where = "procurementType='shopforme' AND $procurementTable.deleted= '0'";
        } else {
            $where = "$procurementTable.deleted= '0'";
        }

        if (!empty($param['searchProcurement']['idFrom']))
            $where .= " AND $procurementTable.id >= " . $param['searchProcurement']['idFrom'];

        if (!empty($param['searchProcurement']['idTo']))
            $where .= " AND $procurementTable.id <= " . $param['searchProcurement']['idTo'];

        if (!empty($param['searchProcurement']['deliveryCompanyId']))
            $where .= " AND $procurement->prefix$procurementItem->table.deliveryCompanyId = " . $param['searchProcurement']['deliveryCompanyId'];

        if (!empty($param['searchProcurement']['status']))
            $where .= " AND $procurementTable.status = '" . $param['searchProcurement']['status'] . "'";

        if (!empty($param['searchProcurement']['paymentStatus']))
            $where .= " AND $procurementTable.paymentStatus = '" . $param['searchProcurement']['paymentStatus'] . "'";

        if (!empty($param['searchProcurement']['totalCostFrom']))
            $where .= " AND $procurementTable.totalProcurementCost >= '" . $param['searchProcurement']['totalCostFrom'] . "'";

        if (!empty($param['searchProcurement']['totalCostTo']))
            $where .= " AND $procurementTable.totalProcurementCost <= '" . $param['searchProcurement']['totalCostTo'] . "'";

        if (!empty($param['searchProcurement']['user']))
            $where .= " AND ($userTable.firstName LIKE '%" . $param['searchProcurement']['user'] . "%' OR $userTable.lastName LIKE '%" . $param['searchProcurement']['user'] . "%' OR $userTable.email LIKE '%" . $param['searchProcurement']['user'] . "%')";
        
        if (!empty($param['searchProcurement']['unitNumber']))
            $where .= " AND $userTable.unit  = '" . $param['searchProcurement']['unitNumber'] . "'";
        
        if (!empty($param['searchProcurement']['toCountry']))
            $where .= " AND $procurementTable.toCountry = " . $param['searchProcurement']['toCountry'];

        if (!empty($param['searchProcurement']['toState']))
            $where .= " AND $procurementTable.toState = " . $param['searchProcurement']['toState'];

        if (!empty($param['searchProcurement']['toCity']))
            $where .= " AND $procurementTable.toCity = " . $param['searchProcurement']['toCity'];

        if (!empty($param['searchWarehouse']))
            $where .= " AND $procurementTable.warehouseId = " . $param['searchWarehouse'];

        if (!empty($param['searchProcurement']['procurementType']))
            $where .= " AND $procurementTable.procurementType = " . '"' . $param['searchProcurement']['procurementType'] . '"';

        if ($param['searchByCreatedOn'] != '') {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($procurementTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $procurementTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($procurementTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($procurementTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        if ($param['field'] != 'firstName') {
            $param['field'] = $procurement->table . "." . $param['field'];
        } else {
            $param['field'] = $user->table . "." . $param['field'];
        }

        if (!empty($param['searchByshipment'])) {
            $sfmArray = implode("','", $param['searchByshipment']);
            $where .= " AND $procurementTable.id IN ('" . $sfmArray . "')";
        }


        if ($type == 'export') {
            $resultSet = $resultSet = Procurement::select(array("$procurement->table.id as procurementid",
                        "$procurement->table.totalProcurementCost as totalcost", "$procurement->table.totalProcurementCost as totalcost", "$procurement->table.status", "$procurement->table.paymentStatus as paymentstatus", "$procurement->table.createdOn", DB::raw("CONCAT($userTable.firstName, ' ', $userTable.lastName) AS customer"), "$state->table.name AS destinationStateName", "$city->table.name AS destinationCityName", "$country->table.name AS destinationCountryName"
                    ))
                    ->leftJoin($user->table, "$user->table.id", '=', "$procurement->table.userId")
                    ->leftJoin($state->table, "$state->table.id", '=', "$procurement->table.toState")
                    ->leftJoin($city->table, "$city->table.id", '=', "$procurement->table.toCity")
                    ->leftJoin($country->table, "$country->table.id", '=', "$procurement->table.toCountry")
                    ->leftJoin($warehouse->table, "$procurement->table.warehouseId", '=', "$warehouse->table.id")
                    ->leftJoin($country->table . " AS WC", "$warehouse->table.countryId", '=', "WC.id")
                    ->leftJoin($procurementItem->table, "$procurementItem->table.procurementId", '=', "$procurement->table.id")
                    ->whereRaw($where)
                    ->orderBy($param['field'], $param['type'])
                    ->get();
        } else {

            $resultSet = Procurement::select(array("$procurement->table.id", "$procurement->table.userId",
                        "$procurement->table.toCountry", "$procurement->table.toState", "$procurement->table.createdOn",
                        "$procurement->table.toCity", "$procurement->table.totalProcurementCost",
                        "$procurement->table.status", "$procurement->table.paymentStatus",
                        "$user->table.firstName", "$user->table.lastName", "$user->table.email",
                        "$city->table.name AS toCityName", "$state->table.name AS toStateName",
                        "$country->table.name AS toCountryName", "$procurement->table.warehouseId",
                        "WC.name as warehouseName", "$procurement->table.procurementType"
                    ))
                    ->leftJoin($user->table, "$procurement->table.userId", '=', "$user->table.id")
                    ->leftJoin($city->table, "$procurement->table.toCity", '=', "$city->table.id")
                    ->leftJoin($state->table, "$procurement->table.toState", '=', "$state->table.id")
                    ->leftJoin($country->table, "$procurement->table.toCountry", '=', "$country->table.id")
                    ->leftJoin($warehouse->table, "$procurement->table.warehouseId", '=', "$warehouse->table.id")
                    ->leftJoin($country->table . " AS WC", "$warehouse->table.countryId", '=', "WC.id")
                    ->leftJoin($procurementItem->table, "$procurementItem->table.procurementId", '=', "$procurement->table.id")
                    ->whereRaw($where)
                    ->groupBy("$procurement->table.id")
                    ->orderBy($param['field'], $param['type'])
                    ->paginate($param['searchDisplay']);
        }

        return $resultSet;
    }

    /**
     * Method used to fetch shop for me details
     * @return object
     */
    public static function getShopForMeDetails($id) {
        $country = new Country;
        $warehouse = new Warehouse;
        $procurement = new Procurement;
        $user = new User;
        $city = new City;
        $state = new State;
        $country = new Country;
        $procurementItem = new Procurementitem;

        $procurementItemTable = $procurement->prefix . $procurementItem->table;


        $resultSet = Procurement::where("$procurement->table.id", $id)
                ->select(array("$procurement->table.*", "$user->table.unit AS userUnit", "$country->table.name AS warehouseName", "TC.name AS toCityName", "TS.name AS toStateName", "TCO.name AS toCountryName",
                    "FC.name AS fromCityName", "FS.name AS fromStateName", "FCO.name AS fromCountryName",
                    DB::raw("(SELECT SUM(itemQuantity) FROM $procurementItemTable WHERE deleted ='0' AND procurementId = $id)  AS totalQuantity"), DB::raw("(SELECT COUNT(id) FROM $procurementItemTable WHERE  deleted ='0' AND procurementId = $id)  AS totalPackage"),
                    DB::raw("(SELECT SUM(itemShippingCost) FROM $procurementItemTable WHERE deleted ='0' AND procurementId = $id)  AS totalIWarehouseShippingCost")))
                ->leftJoin($user->table, "$procurement->table.userId", '=', "$user->table.id")
                ->leftJoin($warehouse->table, "$procurement->table.warehouseId", '=', "$warehouse->table.id")
                ->leftJoin($country->table, "$warehouse->table.countryId", '=', "$country->table.id")
                ->leftJoin($city->table . " AS TC", "$procurement->table.toCity", '=', "TC.id")
                ->leftJoin($state->table . " AS TS", "$procurement->table.toState", '=', "TS.id")
                ->leftJoin($country->table . " AS TCO", "$procurement->table.toCountry", '=', "TCO.id")
                ->leftJoin($city->table . " AS FC", "$procurement->table.fromCity", '=', "FC.id")
                ->leftJoin($state->table . " AS FS", "$procurement->table.fromState", '=', "FS.id")
                ->leftJoin($country->table . " AS FCO", "$procurement->table.fromCountry", '=', "FCO.id")
                ->first();

        return $resultSet;
    }

    /**
     * Method used to fetch warehouse list
     * @return object
     */
    public static function getWarehouseList() {
        $country = new Country;
        $warehouse = new Warehouse;


        $resultSet = Warehouse::where("$warehouse->table.deleted", '0')
                ->where("$warehouse->table.status", '1')
                ->select(array("$warehouse->table.id", "$country->table.name AS countryName"))
                ->leftJoin($country->table, "$warehouse->table.countryId", '=', "$country->table.id")
                ->orderBy("name", "asc")
                ->get();

        return $resultSet;
    }

    /**
     * Method used to check procurement status for individual  item
     * @param integer $id
     * @return integer
     */
    public static function updaterejectedstatus($id) {
        $where = "deleted= '0' AND status != 'unavailable'";

        $resultset = Procurementitem::whereRaw($where)
                ->where('procurementId', $id)
                ->count('id');

        if ($resultset == 0) {
            $row = Procurement::where('id', $id)
                    ->update(array('status' => 'rejected'));
        }

        return $resultset;
    }

    /**
     * Method used to check procurement status for individual  item
     * @param integer $id
     * @return integer
     */
    public static function calculatecost($id) {
        $procurementItem = new Procurementitem;
        $procurement = Procurement::find($id);

        /* FETCH TOTAL ITEM COST */
        $procurementTotalItemCost = DB::table($procurementItem->table)->where('procurementId', $id)->where('deleted', '0')->sum('itemTotalCost');

        /* FETCH TOTAL PROCESSING FEE */
        $whereRaw = " $procurementTotalItemCost BETWEEN feeLeft AND feeRight";
        $resultSet = Procurementfees::select(array('feeType', 'value'))
                ->where("deleted", "0")
                ->where("warehouseId", $procurement->warehouseId)
                ->whereRaw($whereRaw)
                ->first();

        if ($resultSet->feeType == 'A')
            $procurementTotalProcessingFee = $resultSet->value;
        else
            $procurementTotalProcessingFee = ($resultSet->value / 100) * $procurementTotalItemCost;

        /* CALCULATE TOTAL PROCUREMENT COST */
        $urgentPurchaseCost = !empty($procurement->urgentPurchaseCost) ? $procurement->urgentPurchaseCost : 0;
        $totalProcurementCost = $procurementTotalItemCost + $procurementTotalProcessingFee + $urgentPurchaseCost;

        /* SAVE PROCUREMENT COST DETAILS */
        $procurement->totalItemCost = $procurementTotalItemCost;
        $procurement->totalProcessingFee = $procurementTotalProcessingFee;
        $procurement->totalProcurementCost = $totalProcurementCost;



        $procurement->save();
    }

    /**
     * Method used to calculate procurement processing fee
     * @param array $param
     * @return integer
     */
    public static function calculateProcessingFee($param) {
        $procurementProcessingFee = 0;

        /* FETCH TOTAL PROCESSING FEE */
        $whereRaw = $param['totalItemCost'] . " BETWEEN feeLeft AND feeRight";

        $resultSet = Procurementfees::select(array('feeType', 'value'))
                ->where("deleted", "0")
                ->where("warehouseId", $param['warehouseId'])
                ->whereRaw($whereRaw)
                ->first();

        if (!empty($resultSet)) {
            if ($resultSet->feeType == 'A')
                $procurementProcessingFee = $resultSet->value;
            else
                $procurementProcessingFee = ($resultSet->value / 100) * $param['totalItemCost'];
        }


        return $procurementProcessingFee;
    }

    /**
     * Method used to change State status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Procurement::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));


        return $row;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Procurement::where('id', $id)
                ->update(array('deleted' => 1, 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to get buy my car list
     * @param array $param
     * @return object
     */
    public static function getautoPickupList($param) {

        $autoShipment = new Procurement;
        $autoPickup = new Procurementitem;
        $autoWebsite = new Autowebsite;
        $user = new User;
        $state = new State;
        $city = new City;

        $userTable = $autoShipment->prefix . $user->table;
        $autoPickupTable = $autoShipment->prefix . $autoPickup->table;
        $autoShipmentTable = $autoShipment->prefix . $autoShipment->table;

        $where = "$autoShipmentTable.procurementType = 'buycarforme'";

        if (!empty($param)) {
            
            if (!empty($param['searchAutoPickup']['id']))
                $where .= " AND $autoShipmentTable.id = '" . $param['searchAutoPickup']['id'] . "'";
            
            if (!empty($param['searchAutoPickup']['unit']))
                $where .= " AND $userTable.unit = '" . $param['searchAutoPickup']['unit'] . "'";

            if ($param['searchAutoPickup']['status'] != '')
                $where .= " AND $autoPickupTable.status = '" . $param['searchAutoPickup']['status'] . "'";

            if (!empty($param['searchAutoPickup']['user']))
                $where .= " AND ($userTable.firstName LIKE '%" . $param['searchAutoPickup']['user'] . "%' OR $userTable.lastName LIKE '%" . $param['searchAutoPickup']['user'] . "%' OR $userTable.email LIKE '%" . $param['searchAutoPickup']['user'] . "%')";
            
            if(!empty($param['searchAutoPickup']['vinNumber']))
                $where .= " AND $autoPickupTable.vinNumber = '". $param['searchAutoPickup']['vinNumber'] ."'";
            
            if(!empty($param['searchAutoPickup']['make']))
                $where .= " AND $autoPickupTable.makeId = '". $param['searchAutoPickup']['make'] ."'";
            
            if(!empty($param['searchAutoPickup']['model']))
                $where .= " AND $autoPickupTable.modelId = '". $param['searchAutoPickup']['model'] ."'";
            
            if(!empty($param['searchAutoPickup']['year']))
                $where .= " AND $autoPickupTable.year = '". $param['searchAutoPickup']['year'] ."'";
            
            if(!empty($param['searchAutoPickup']['color']))
                $where .= " AND $autoPickupTable.options = '". $param['searchAutoPickup']['color'] ."'";
            
            if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($autoShipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $autoShipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($autoShipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($autoShipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }
        }

        $resultSet = Procurement::select(array("$autoShipment->table.id", "$autoShipment->table.createdOn", "$autoPickup->table.id AS itemId", "$autoShipment->table.status", "$user->table.firstName", "$user->table.lastName", "$user->table.email", "$autoWebsite->table.name AS websiteName", "$state->table.name AS stateName", "$city->table.name AS cityName"))
                ->leftjoin($autoPickup->table, "$autoShipment->table.id", '=', "$autoPickup->table.procurementId")
                ->leftJoin($autoWebsite->table, "$autoWebsite->table.id", '=', "$autoPickup->table.carWebsiteId")
                ->leftJoin($user->table, "$user->table.id", '=', "$autoShipment->table.userId")
                ->leftJoin($state->table, "$state->table.id", '=', "$autoShipment->table.fromState")
                ->leftJoin($city->table, "$city->table.id", '=', "$autoShipment->table.fromCity")
                ->whereRaw($where)
                ->groupBy("$autoShipment->table.id")
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to get car details
     * @param int $id
     * @return object
     */
    public static function getSingleCarDetails($id) {
        $autoShipment = new Procurement;
        $autoPickup = new Procurementitem;
        $make = new Automake;
        $model = new Automodel;
        $user = new User;

        $resultSet = Procurement:: select(array("$autoPickup->table.*", "$make->table.name AS makeName", "$model->table.name AS modelName",))
                ->leftjoin($autoPickup->table, "$autoShipment->table.id", '=', "$autoPickup->table.procurementId")
                ->leftJoin($make->table, "$make->table.id", '=', "$autoPickup->table.makeId")
                ->leftJoin($model->table, "$model->table.id", '=', "$autoPickup->table.modelId")
                ->leftJoin($user->table, "$user->table.id", '=', "$autoShipment->table.userId")
                ->where($make->table . '.type', '0')
                ->where($model->table . '.makeType', '0')
                ->where($autoPickup->table . '.id', $id)
                ->first();

        return $resultSet;
    }

    /**
     * Method used to fetch a single car pickup request details
     * @param int $id
     * @return object
     */
    public static function getPickupRequestDetails($id) {

        $autoShipment = new Procurement;
        $autoPickup = new Procurementitem;
        $make = new Automake;
        $model = new Automodel;
        $autoWebsite = new Autowebsite;
        $state = new State;
        $city = new City;
        $user = new User;


        $resultSet = Procurement:: select(array("$autoPickup->table.*", "$autoShipment->table.status AS procurementStatus", "$autoShipment->table.totalItemCost", "$autoShipment->table.defaultCurrencyCode", "$autoShipment->table.paidCurrencyCode", "$autoShipment->table.exchangeRate", "$autoShipment->table.totalProcessingFee", "$autoShipment->table.totalPickupCost", "$autoShipment->table.totalShippingCost", "$autoShipment->table.totalCost", "$autoShipment->table.paymentStatus", "$user->table.id AS userId", "$user->table.unit AS userUnit", "$autoWebsite->table.name AS websiteName", "$state->table.name AS stateName", "$city->table.name AS cityName", "$make->table.name AS makeName", "$model->table.name AS modelName"))
                ->leftjoin($autoPickup->table, "$autoShipment->table.id", '=', "$autoPickup->table.procurementId")
                ->leftJoin($make->table, "$make->table.id", '=', "$autoPickup->table.makeId")
                ->leftJoin($model->table, "$model->table.id", '=', "$autoPickup->table.modelId")
                ->leftJoin($autoWebsite->table, "$autoWebsite->table.id", '=', "$autoPickup->table.carWebsiteId")
                ->leftJoin($state->table, "$state->table.id", '=', "$autoShipment->table.fromState")
                ->leftJoin($city->table, "$city->table.id", '=', "$autoShipment->table.fromCity")
                ->leftJoin($user->table, "$autoShipment->table.userId", '=', "$user->table.id")
                ->where($autoShipment->table . '.id', $id)
                ->where($autoShipment->table . '.procurementType', 'buycarforme')
                ->where($autoShipment->table . '.deleted', '0')
                ->where($autoPickup->table . '.deleted', '0')
                ->where($make->table . '.type', '0')
                ->where($model->table . '.makeType', '0')
                ->first();

        return $resultSet;
    }

    /**
     * Method used to fetch a Location Name 
     * @param string $locationName
     * @return array
     */
    public static function getCityState($locationName) {
        $state = new State;
        $city = new City;

        $result = City::select(array("$state->table.name as stateName", "$state->table.id as stateId", "$city->table.name AS cityName", "$city->table.id AS cityId"))
                ->leftJoin($state->table, "$state->table.code", '=', "$city->table.stateCode")
                ->orWhere("$city->table.name", 'like', "%" . $locationName . "%")
                ->get();

        return $result;
    }

    /////////////////////////////// Auto Parts Methods /////////////////////////////////////////

    /**
     * Method used to auto parts list
     * @param array $param
     * @return object
     */
    public static function getautoPartsList($param, $type = '') {

        $autoShipment = new Procurement;
        $autoPickup = new Procurementitem;
        $autoWebsite = new Autowebsite;
        $user = new User;
        $state = new State;
        $city = new City;
        $country = new Country;

        $userTable = $autoShipment->prefix . $user->table;
        $autoPickupTable = $autoShipment->prefix . $autoPickup->table;
        $autoShipmentTable = $autoShipment->prefix . $autoShipment->table;



        //$procurementItem = new Procurementitem;

        $where = "$autoShipmentTable.procurementType = 'autopart' AND $autoShipmentTable.deleted='0'";

        if (!empty($param)) {
            if (!empty($param['searchAutoParts']['totalCostFrom']))
                $where .= " AND $autoShipmentTable.totalProcurementCost >= '" . $param['searchAutoParts']['totalCostFrom'] . "'";

            if (!empty($param['searchAutoParts']['totalCostTo']))
                $where .= " AND $autoShipmentTable.totalProcurementCost <= '" . $param['searchAutoParts']['totalCostTo'] . "'";

            if (isset($param['searchAutoParts']['status']) && $param['searchAutoParts']['status'] != '')
                $where .= " AND $autoPickupTable.status = '" . $param['searchAutoParts']['status'] . "'";

            if (!empty($param['searchAutoParts']['user']))
                $where .= " AND ($userTable.firstName LIKE '%" . $param['searchAutoParts']['user'] . "%' OR $userTable.lastName LIKE '%" . $param['searchAutoParts']['user'] . "%' OR $userTable.email LIKE '%" . $param['searchAutoParts']['user'] . "%')";

            if (!empty($param['searchAutoParts']['unitNumber']))
                $where .= " AND $userTable.unit  = '" . $param['searchAutoParts']['unitNumber'] . "'";
            
            if (!empty($param['searchAutoParts']['idFrom']))
                $where .= " AND $autoShipmentTable.id >= " . $param['searchAutoParts']['idFrom'];

            if (!empty($param['searchAutoParts']['idTo']))
                $where .= " AND $autoShipmentTable.id <= " . $param['searchAutoParts']['idTo'];

            if (!empty($param['searchAutoParts']['toCountry']))
                $where .= " AND $autoShipmentTable.toCountry = " . $param['searchAutoParts']['toCountry'];

            if (!empty($param['searchAutoParts']['toState']))
                $where .= " AND $autoShipmentTable.toState = " . $param['searchAutoParts']['toState'];

            if (!empty($param['searchAutoParts']['toCity']))
                $where .= " AND $autoShipmentTable.toCity = " . $param['searchAutoParts']['toCity'];

            if (!empty($param['searchAutoParts']['deliveryCompanyId']))
                $where .= " AND $autoPickupTable.deliveryCompanyId = " . $param['searchAutoParts']['deliveryCompanyId'];


            if (isset($param['searchByCreatedOn']) && $param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($autoShipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $autoShipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($autoShipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($autoShipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }
        }
        if(isset($param['field'])){
            if ($param['field'] != 'firstName') {
                $param['field'] = $autoShipment->table . "." . $param['field'];
            } else {
                $param['field'] = $user->table . "." . $param['field'];
            }
        }
        
        

        if (!empty($param['searchByshipment'])) {
            $autoPartsArray = implode("','", $param['searchByshipment']);
            $where .= " AND $autoShipmentTable.id IN ('" . $autoPartsArray . "')";
        }

        if ($type == 'export') {
            $resultSet = Procurement::select(array("$autoShipment->table.id as procurementid",
                        "$autoShipment->table.totalProcurementCost as totalcost",
                        "$autoShipment->table.status", "$autoShipment->table.paymentStatus as paymentstatus",
                        DB::raw("CONCAT($userTable.firstName, ' ', $userTable.lastName) AS customer"),
                        "$state->table.name AS destinationStateName",
                        "$city->table.name AS destinationCityName",
                        "$country->table.name AS destinationCountryName", "$autoShipment->table.createdOn",
                    ))
                    ->leftjoin($autoPickup->table, "$autoShipment->table.id", '=', "$autoPickup->table.procurementId")
                    ->leftJoin($autoWebsite->table, "$autoWebsite->table.id", '=', "$autoPickup->table.carWebsiteId")
                    ->leftJoin($user->table, "$user->table.id", '=', "$autoShipment->table.userId")
                    ->leftJoin($state->table, "$state->table.id", '=', "$autoShipment->table.toState")
                    ->leftJoin($city->table, "$city->table.id", '=', "$autoShipment->table.toCity")
                    ->leftJoin($country->table, "$country->table.id", '=', "$autoShipment->table.toCountry")
                    ->whereRaw($where)
                    ->orderBy($param['field'], $param['type'])->orderBy("$autoShipment->table.id", "desc")
                    ->get();
        } else {
            $resultSet = Procurement::select(array("$autoShipment->table.*", "$autoPickup->table.id AS itemId", "$autoPickup->table.status AS itemStatus", "$user->table.firstName", "$user->table.lastName", "$user->table.email", "$autoWebsite->table.name AS websiteName", "$state->table.name AS stateName", "$city->table.name AS cityName", "$country->table.name AS countryName"))
                    ->leftjoin($autoPickup->table, "$autoShipment->table.id", '=', "$autoPickup->table.procurementId")
                    ->leftJoin($autoWebsite->table, "$autoWebsite->table.id", '=', "$autoPickup->table.carWebsiteId")
                    ->leftJoin($user->table, "$user->table.id", '=', "$autoShipment->table.userId")
                    ->leftJoin($state->table, "$state->table.id", '=', "$autoShipment->table.toState")
                    ->leftJoin($city->table, "$city->table.id", '=', "$autoShipment->table.toCity")
                    ->leftJoin($country->table, "$country->table.id", '=', "$autoShipment->table.toCountry")
                    ->whereRaw($where)
                    ->groupBy("$autoShipment->table.id")
                    ->orderBy("$autoShipment->table.id", "desc")
                    ->orderBy($param['field'], $param['type'])
                    ->paginate($param['searchDisplay']);
        }
     

        return $resultSet;
    }

    /**
     * Method used to fetch auto parts request details
     * @param int $id
     * @return object
     */
    public static function getAutopartsDetails($id) {
        //DB::enablequerylog();

        $autoShipment = new Procurement;
        $autoPickup = new Procurementitem;
        $make = new Automake;
        $model = new Automodel;
        $state = new State;
        $city = new City;
        $user = new User;
        $country = new Country;
        $warehouse = new Warehouse;
        $shippingMethod = new Shippingmethods;

        $procurementItemTable = $autoShipment->prefix . $autoPickup->table;

        $resultSet = Procurement:: select(array("$autoPickup->table.*", "$autoShipment->table.isCurrencyChanged", "$autoShipment->table.paidCurrencyCode", "$autoShipment->table.exchangeRate", "$autoShipment->table.defaultCurrencyCode", "$autoShipment->table.shippingMethod", "$autoShipment->table.paymentMethodId", "$autoShipment->table.shippingMethodId", "$autoShipment->table.paymentStatus", "$autoShipment->table.toZipCode", "$autoShipment->table.toAddress", "$autoShipment->table.warehouseId", "$autoShipment->table.totalCost", "$autoShipment->table.totalTax", "$autoShipment->table.totalInsurance", "$autoShipment->table.totalClearingDuty", "$autoShipment->table.isDutyCharged", "$autoShipment->table.urgent", "$autoShipment->table.fromName", "$autoShipment->table.fromEmail", "$autoShipment->table.fromPhone", "$autoShipment->table.fromCountry", "$autoShipment->table.fromState", "$autoShipment->table.fromCity", "$autoShipment->table.fromAddress", "$autoShipment->table.fromZipCode", "$autoShipment->table.fromName", "$autoShipment->table.toName", "$autoShipment->table.toEmail", "$autoShipment->table.toPhone", "$autoShipment->table.toCountry", "$autoShipment->table.toState", "$autoShipment->table.toCity", "$autoShipment->table.estimateDeliveryDate", "$autoShipment->table.deliveredOn", "$country->table.name AS warehouseName", "$autoShipment->table.createdOn", "$autoShipment->table.totalWeight", "$autoShipment->table.totalProcurementCost", "$autoShipment->table.totalItemCost", "$autoShipment->table.totalProcessingFee", "$autoShipment->table.urgentPurchaseCost", "$autoShipment->table.procurementLocked", "$autoShipment->table.shippingMethod", "$autoShipment->table.status AS procurementStatus", "$user->table.id AS userId", "$user->table.unit AS userUnit", "$country->table.name AS warehouseName", "TC.name AS toCityName", "TS.name AS toStateName", "TCO.name AS toCountryName",
                    "FC.name AS fromCityName", "FS.name AS fromStateName", "FCO.name AS fromCountryName", "$shippingMethod->table.shipping", DB::raw("(SELECT SUM(itemQuantity) FROM $procurementItemTable WHERE deleted ='0' AND procurementId = $id)  AS totalQuantity"), DB::raw("(SELECT COUNT(id) FROM $procurementItemTable WHERE  deleted ='0' AND procurementId = $id)  AS totalPackage"),
                    DB::raw("(SELECT SUM(itemShippingCost) FROM $procurementItemTable WHERE deleted ='0' AND procurementId = $id)  AS totalShippingCost")))
                ->leftjoin($autoPickup->table, "$autoShipment->table.id", '=', "$autoPickup->table.procurementId")
                ->leftJoin($warehouse->table, "$autoShipment->table.warehouseId", '=', "$warehouse->table.id")
                ->leftJoin($make->table, "$make->table.id", '=', "$autoPickup->table.makeId")
                ->leftJoin($model->table, "$model->table.id", '=', "$autoPickup->table.modelId")
                ->leftJoin($country->table, "$warehouse->table.countryId", '=', "$country->table.id")
                ->leftJoin($city->table . " AS TC", "$autoShipment->table.toCity", '=', "TC.id")
                ->leftJoin($state->table . " AS TS", "$autoShipment->table.toState", '=', "TS.id")
                ->leftJoin($country->table . " AS TCO", "$autoShipment->table.toCountry", '=', "TCO.id")
                ->leftJoin($city->table . " AS FC", "$autoShipment->table.fromCity", '=', "FC.id")
                ->leftJoin($state->table . " AS FS", "$autoShipment->table.fromState", '=', "FS.id")
                ->leftJoin($country->table . " AS FCO", "$autoShipment->table.fromCountry", '=', "FCO.id")
                ->leftJoin($user->table, "$autoShipment->table.userId", '=', "$user->table.id")
                ->leftJoin($shippingMethod->table, "$autoShipment->table.shippingMethodId", '=', "$shippingMethod->table.shippingid")
                ->where($autoShipment->table . '.id', $id)
                ->where($autoShipment->table . '.procurementType', 'autopart')
                ->where($autoShipment->table . '.deleted', '0')
                ->where($autoPickup->table . '.deleted', '0')
                ->first();
        // dd( DB::getQueryLog());

        return $resultSet;
    }

    /////////////////////////////// Other Vehicles Methods /////////////////////////////////////////

    /**
     * Method used to Other Vehicles list
     * @param array $param
     * @return object
     */
    public static function getOtherVehicleList($param) {

        $procurement = new Procurement;
        $procurementItem = new Procurementitem;
        $autoWebsite = new Autowebsite;
        $user = new User;
        $state = new State;
        $city = new City;

        $userTable = $procurement->prefix . $user->table;
        $procurementItemTable = $procurement->prefix . $procurementItem->table;
        $procurementTable = $procurement->prefix . $procurement->table;

        $where = "$procurementTable.procurementType = 'othervehicle'";

        if (!empty($param)) {
            if (!empty($param['searchOtherVehicle']['unitFrom']))
                $where .= " AND $userTable.unit >= '" . $param['searchOtherVehicle']['unitFrom'] . "'";

            if (!empty($param['searchOtherVehicle']['unitTo']))
                $where .= " AND $userTable.unit <= '" . $param['searchOtherVehicle']['unitFrom'] . "'";

            if ($param['searchOtherVehicle']['status'] != '')
                $where .= " AND $autoPickupTable.status = '" . $param['searchOtherVehicle']['status'] . "'";

            if (!empty($param['searchOtherVehicle']['user']))
                $where .= " AND ($userTable.firstName LIKE '%" . $param['searchOtherVehicle']['user'] . "%' OR $userTable.lastName LIKE '%" . $param['searchOtherVehicle']['user'] . "%' OR $userTable.email LIKE '%" . $param['searchOtherVehicle']['user'] . "%')";

            if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($procurementTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $procurementTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($procurementTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($procurementTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }
        }

        $resultSet = Procurement::select(array("$procurement->table.*", "$procurementItem->table.id AS itemId", "$procurementItem->table.status AS itemStatus", "$user->table.firstName", "$user->table.lastName", "$user->table.email", "$autoWebsite->table.name AS websiteName", "$state->table.name AS stateName", "$city->table.name AS cityName"))
                ->leftjoin($procurementItem->table, "$procurement->table.id", '=', "$procurementItem->table.procurementId")
                ->leftJoin($autoWebsite->table, "$autoWebsite->table.id", '=', "$procurementItem->table.carWebsiteId")
                ->leftJoin($user->table, "$user->table.id", '=', "$procurement->table.userId")
                ->leftJoin($state->table, "$state->table.id", '=', "$procurementItem->table.pickupStateId")
                ->leftJoin($city->table, "$city->table.id", '=', "$procurementItem->table.pickupCityId")
                ->whereRaw($where)
                ->groupBy("$procurement->table.id")
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to fetch a single car pickup request details
     * @param int $id
     * @return object
     */
    public static function getOtherVehicleDetails($id) {
        $procurement = new Procurement;
        $procurementItem = new Procurementitem;
        $make = new Automake;
        $model = new Automodel;
        $state = new State;
        $city = new City;
        $user = new User;
        $country = new Country;
        $warehouse = new Warehouse;

        $procurementItemTable = $procurement->prefix . $procurementItem->table;

        $resultSet = Procurement:: select(array("$procurementItem->table.*", "$procurement->table.urgent", "$procurement->table.fromName", "$procurement->table.fromEmail", "$procurement->table.fromEmail", "$procurement->table.toName", "$procurement->table.toEmail", "$procurement->table.toPhone", "$procurement->table.estimateDeliveryDate", "$procurement->table.deliveredOn", "$country->table.name AS warehouseName", "$procurement->table.createdOn", "$procurement->table.fromCountry", "$procurement->table.fromState", "$procurement->table.fromCity", "$procurement->table.toCountry", "$procurement->table.toState", "$procurement->table.toCity", "$procurement->table.totalWeight", "$procurement->table.totalProcurementCost", "$procurement->table.totalItemCost", "$procurement->table.totalProcessingFee", "$procurement->table.urgentPurchaseCost", "$procurement->table.procurementLocked", "$procurement->table.shippingMethod", "$procurement->table.status AS procurementStatus", "$user->table.id AS userId", "$user->table.unit AS userUnit", "$country->table.name AS warehouseName", "TC.name AS toCityName", "TS.name AS toStateName", "TCO.name AS toCountryName",
                    "FC.name AS fromCityName", "FS.name AS fromStateName", "FCO.name AS fromCountryName", "$make->table.name AS makeName", "$model->table.name AS modelName", DB::raw("(SELECT SUM(itemQuantity) FROM $procurementItemTable WHERE deleted ='0' AND procurementId = $id)  AS totalQuantity"), DB::raw("(SELECT COUNT(id) FROM $procurementItemTable WHERE  deleted ='0' AND procurementId = $id)  AS totalPackage"),
                    DB::raw("(SELECT SUM(itemShippingCost) FROM $procurementItemTable WHERE deleted ='0' AND procurementId = $id)  AS totalShippingCost")))
                ->leftjoin($procurementItem->table, "$procurement->table.id", '=', "$procurementItem->table.procurementId")
                ->leftJoin($warehouse->table, "$procurement->table.warehouseId", '=', "$warehouse->table.id")
                ->leftJoin($make->table, "$make->table.id", '=', "$procurementItem->table.makeId")
                ->leftJoin($model->table, "$model->table.id", '=', "$procurementItem->table.modelId")
                ->leftJoin($country->table, "$warehouse->table.countryId", '=', "$country->table.id")
                ->leftJoin($city->table . " AS TC", "$procurement->table.toCity", '=', "TC.id")
                ->leftJoin($state->table . " AS TS", "$procurement->table.toState", '=', "TS.id")
                ->leftJoin($country->table . " AS TCO", "$procurement->table.toCountry", '=', "TCO.id")
                ->leftJoin($city->table . " AS FC", "$procurement->table.fromCity", '=', "FC.id")
                ->leftJoin($state->table . " AS FS", "$procurement->table.fromState", '=', "FS.id")
                ->leftJoin($country->table . " AS FCO", "$procurement->table.fromCountry", '=', "FCO.id")
                ->leftJoin($user->table, "$procurement->table.userId", '=', "$user->table.id")
                ->where($procurement->table . '.id', $id)
                ->where($procurement->table . '.procurementType', 'othervehicle')
                ->where($make->table . '.type', '2')
                ->where($model->table . '.makeType', '2')
                ->where($procurement->table . '.deleted', '0')
                ->where($procurementItem->table . '.deleted', '0')
                ->first();

        return $resultSet;
    }

    public static function saveBuyCarForMe($userId, $warehouseId, $postData,$dataSource = 'web') {

        $transactionId = 0;
        $paymentStatus = 'unpaid';
        $transactionErrorMsg = array();
        $paymentErrorMessage = "";
        $userShippingAddress = \App\Model\Addressbook::where('userId', $userId)->where('isDefaultShipping', '1')->with('country', 'state', 'city')->first();
        $userBillingAddress = \App\Model\Addressbook::where('userId', $userId)->where('isDefaultBilling', '1')->with('country', 'state', 'city')->first();
        $userDetails = User::find($userId);
        $warehouseDetails = Warehouse::find($warehouseId);
        if ($postData->hasFile('fileItem')) {
            $image = $postData->file('fileItem');
            $imageName = time() . '_' . $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/auto/carpickup/' . $userId);
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0777);
            }
            $image->move($destinationPath, $imageName);
        }
        DB::beginTransaction();
        
        if ($postData->paymentMethodKey == 'bank_online_transfer' || $postData->paymentMethodKey == 'bank_pay_at_bank' || $postData->paymentMethodKey == 'bank_account_pay' || $postData->paymentMethodKey == 'check_cash_payment')
            $paymentStatus = 'paid';
        
        if ($postData->paymentMethodKey == 'paypal_checkout') {
            /* $checkoutData = array();
              $checkoutData['cardNumber'] = $postData->cardNumber;
              $checkoutData['expMonth'] = $postData->expMonth;
              $checkoutData['expYear'] = $postData->expYear;
              $checkoutData['cardCode'] = $postData->cardCode;
              $checkoutData['customerFirstName'] = $userDetails->firstName;
              $checkoutData['customerLastName'] = $userDetails->lastName;
              $checkoutData['customerAddress'] = $userBillingAddress->address;
              $checkoutData['customerCity'] = isset($userBillingAddress->cityId)?$userBillingAddress->city->name : "";
              $checkoutData['customerState'] = isset($userBillingAddress->stateId)?$userBillingAddress->state->name : "";
              $checkoutData['customerCountry'] = $userBillingAddress->country->name;
              $checkoutData['customerZip'] = $userBillingAddress->zipcode;
              $checkoutData['amount'] = round($postData->totalCost,2);
              $checkoutData['defaultCurrency'] = $postData->defaultCurrency;
              $checkoutReturn = Paymenttransaction::paypaypalpro($checkoutData);
              if($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'uccessWithWarning')
              {
              $trasactionId = $checkoutReturn['TRANSACTIONID'];
              $paymentStatus = 'paid';
              }
              else
              return 'payment_error'; */
        }
        if ($postData->paymentMethodKey == 'credit_debit_card') {
            $getPaymentGateway = Paymentmethod::where('paymentMethodKey', 'credit_debit_card')->first();
            $checkoutData = array();
            $checkoutData['cardNumber'] = $postData->cardNumber;
            $checkoutData['expMonth'] = $postData->expMonth;
            $checkoutData['expYear'] = $postData->expYear;
            $checkoutData['cardCode'] = $postData->cardCode;
            $checkoutData['customerFirstName'] = $userDetails->firstName;
            $checkoutData['customerLastName'] = $userDetails->lastName;
            $checkoutData['customerAddress'] = $userBillingAddress->address;
            $checkoutData['customerCity'] = isset($userBillingAddress->cityId) ? $userBillingAddress->city->name : "";
            $checkoutData['customerState'] = isset($userBillingAddress->stateId) ? $userBillingAddress->state->name : "";
            $checkoutData['customerCountry'] = $userBillingAddress->country->name;
            $checkoutData['customerZip'] = $userBillingAddress->zipcode;
            $checkoutData['amount'] = round($postData->totalCost, 2);
            $checkoutData['defaultCurrency'] = $postData->defaultCurrency;
            /* Payment gateway paypal */
            if ($getPaymentGateway->paymentGatewayId == '1') {
                $checkoutReturn = Paymenttransaction::paypaypalpro($checkoutData);
                if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'successWithWarning')) {
                    $paymentStatus = 'paid';
                    $transactionId = $checkoutReturn['TRANSACTIONID'];
                    $transactionData = json_encode($checkoutReturn);
                } else if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Failure')) {
                    $paymentStatus = 'failed';
                    $transactionErrorMsg = json_encode($checkoutReturn);
                    $paymentErrorMessage = $checkoutReturn["L_LONGMESSAGE0"];
                    //return array('payment_error'=>'1','error_msg'=>$paymentErrorMessage);
                    
                } else if ($checkoutReturn == 'error') {
                    $paymentStatus = 'failed';
                    $paymentErrorMessage = "Payment Error";
                    //return array('payment_error'=>'1','error_msg'=>$paymentErrorMessage);
                }

                /* if (is_array($checkoutReturn) && ($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'uccessWithWarning')) {
                  $trasactionId = $checkoutReturn['TRANSACTIONID'];
                  $transactionData = json_encode($checkoutReturn);
                  $paymentStatus = 'paid';
                  } else
                  return 'payment_error'; */
            } else  { /* Payment gateway Authorize.net */
                $checkoutData['customerShippingAddress'] = $userBillingAddress->address;
                $checkoutData['customerShippingCity'] = isset($userBillingAddress->cityId) ? $userBillingAddress->city->name : "";
                $checkoutData['customerShippingState'] = isset($userBillingAddress->stateId) ? $userBillingAddress->state->name : "";
                $checkoutData['customerShippingCountry'] = $userBillingAddress->country->name;
                $checkoutData['customerShippingZip'] = $userBillingAddress->zipcode;
                $checkoutData['shippingFirstName'] = $userDetails->firstName;
                $checkoutData['shippingLastName'] = $userDetails->lastName;

                $checkoutReturn = Paymenttransaction::payauthorizedotnet($checkoutData);
                //print_r($checkoutReturn);
                if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Ok') {
                    $paymentStatus = 'paid';
                    $transactionId = $checkoutReturn['transactionResponse']['transId'];
                    $transactionData = json_encode($checkoutReturn['transactionResponse']);
                } else if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Error') {
                    $paymentStatus = 'failed';
                    $transactionErrorMsg = json_encode($checkoutReturn['transactionResponse']);
                    if (isset($checkoutReturn['transactionResponse']['errors']['error'][0]))
                        $paymentErrorMessage = 'Payment failed. ' . $checkoutReturn['transactionResponse']['errors']['error'][0]['errorText'];
                    else
                        $paymentErrorMessage = 'Payment failed. '.$checkoutReturn['transactionResponse']['errors']['error']['errorText'];
                    //return array('payment_error' => '1', 'error_msg' => $paymentErrorMessage);
                }

                /* if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Ok') {
                  $paymentStatus = 'paid';
                  $trasactionId = $checkoutReturn['transactionResponse']['transId'];
                  $transactionData = json_encode($checkoutReturn['transactionResponse']);
                  } else {
                  return 'payment_error';
                  } */
            }
        } else if ($postData->paymentMethodKey == 'ewallet') {
            if (isset($postData->ewalletId) && $postData->ewalletId != '') {
                $ewalletData = Ewallet::find($postData->ewalletId);
                $ewalletData->amount = round(($ewalletData->amount - $postData->totalCost), 2);
                $ewalletData->debit = $ewalletData->debit + $postData->totalCost;
                $ewalletData->save();

                $ewalletTransaction = new Ewallettransaction;
                $ewalletTransaction->userId = $userId;
                $ewalletTransaction->ewalletId = $postData->ewalletId;
                $ewalletTransaction->amount = $postData->totalCost;
                $ewalletTransaction->transactionType = 'debit';
                $ewalletTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                $ewalletTransaction->save();

                $paymentStatus = 'paid';
            }
        } else if ($postData->paymentMethodKey == 'paystack_checkout') {
            
            if($dataSource == 'app') {
                
                if (!empty($postData->paystackData)) {
                    $checkoutReturn = Paymenttransaction::paystack($postData->paystackCreatedReference);
                    if ($checkoutReturn) {
                        $paymentMode = 'online';
                        $paymentStatus = 'paid';
                        $trasactionId = $postData->paystackData['trans'];
                        $transactionData = json_encode($postData->paystackData);
                    }
                } else
                    $paymentStatus = 'unpaid';
            } else {
            
            if(!empty($postData->paystackData['trans']))
                $findRecord = \App\Model\Paystackreferencehistory::whereRaw("referenceNo = $postData->paystackCreatedReference and processed='0'")->first();
            else
                $findRecord = \App\Model\Paystackreferencehistory::whereRaw("referenceNo = $postData->paystackCreatedReference")->first();
            if(!empty($findRecord))
            {
                $paymentTransacionData = \App\Model\Paymenttransaction::where("transactionData","like",'%"reference":"'.$postData->paystackCreatedReference.'"%')->first();
                if(empty($paymentTransacionData) || (!empty($paymentTransacionData) && $paymentTransacionData->status=='unpaid'))
                {
                    \App\Model\Paystackreferencehistory::where("id",$findRecord->id)->update(["processed"=>"1"]);
                    if (!empty($postData->paystackData)) {
                        $checkoutReturn = Paymenttransaction::paystack($postData->paystackCreatedReference);
                        if ($checkoutReturn) {
                            $paymentStatus = 'paid';
                            if(!empty($postData->paystackData['trans']))
                                $trasactionId = $postData->paystackData['trans'];
                            else if(!empty($postData->paystackData['id']))
                                $transactionId = $postData->paystackData['id'];
                            else
                                $transactionId = '123456789';
                            $transactionData = json_encode($postData->paystackData);
                        }
                        else
                        {
                            $paymentStatus = 'failed';
                            $transactionErrorMsg = json_encode($postData->paystackData);
                            $paymentErrorMessage = "Payment Failed";
                        }
                    }   
                }
                else
                {
                    $paymentStatus = 'pastackProcessed';
                }
            }
            else
            {
                $paymentStatus = 'pastackProcessed';
            }
        }
        
        if($paymentStatus == 'pastackProcessed')
        {
            return array('pasytack_processed'=>'1');
        }
            
        } else if($postData->paymentMethodKey == 'paypalstandard') {

            if(isset($postData->paypalData))
            {   
                $paymentStatus = 'paid';
                $transactionId = $postData->paypalData['orderID'];
                $transactionData = json_encode($postData->paypalData);
            }
        }
        
        if($postData->paymentMethodKey == 'payeezy') {
            
            if(!empty($postData->payeezyData)) {
                $checkoutData['amount'] = $postData->payeezyData["paidAmount"];
                $checkoutData['method'] = $postData->payeezyData["paymentType"];
                $checkoutData['currency_code'] = "USD";
                $checkoutData['type'] = $postData->payeezyData["cardType"];
                $checkoutData['cardholder_name'] = $postData->payeezyData["cardHolderName"];
                $checkoutData['card_number'] = $postData->payeezyData['ccardNumber'];
                $checkoutData['exp_date'] = $postData->payeezyData['expiryMonth'].substr($postData->payeezyData['expiryYear'],2);
                $checkoutData['cvv'] = $postData->payeezyData['cvvCode'];
                $checkoutData['userUnit'] = (!empty($userDetails->unit) ? $userDetails->unit : "");
                $checkoutReturn = Paymenttransaction::processpayeezy($checkoutData);
                
                if(is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'success' && !empty($checkoutReturn['transaction_status']) && $checkoutReturn['transaction_status'] == 'approved')
                {
                    $paymentStatus = 'paid';
                    $paymentMode = 'online';
                    $transactionId = $checkoutReturn['transaction_id'];
                    $transactionData = json_encode($checkoutReturn);
                }
                else if(is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'failed')
                {
                    $paymentStatus = 'failed';
                    $paymentErrorMessage = 'Payment failed. ' .$checkoutReturn['Error']['messages'][0]['description'];
                }
                else
                {
                    $paymentStatus = 'failed';
                    $paymentErrorMessage = 'Payment failed. Something Went Wrong ';
                }
                
            } else {
                $paymentStatus = 'error';
            }
        }
        
        
        $procurementObj = new Procurement;
        $procurementItemObj = new Procurementitem;

        $procurementObj->userId = $userId;
        $procurementObj->warehouseId = $warehouseId;
        $procurementObj->fromCountry = $postData->fromCountry;
        $procurementObj->fromState = $postData->fromState;
        $procurementObj->fromCity = $postData->fromCity;
        $procurementObj->toCountry = $userShippingAddress->countryId;
        $procurementObj->toState = $userShippingAddress->stateId;
        $procurementObj->toCity = $userShippingAddress->cityId;
        $procurementObj->toAddress = $userShippingAddress->address;
        $procurementObj->toZipCode = $userShippingAddress->zipcode;
        $procurementObj->toName = $userShippingAddress->firstName . ' ' . $userShippingAddress->lastName;
        $procurementObj->toEmail = $userShippingAddress->email;
        $procurementObj->toPhone = '+' . $userShippingAddress->isdCode . ' ' . $userShippingAddress->phone;
        $procurementObj->procurementType = 'buycarforme';
        $procurementObj->totalItemCost = $postData->price;
        $procurementObj->totalProcessingFee = $postData->processingFee;
        $procurementObj->urgentPurchaseCost = '0.00';
        $procurementObj->totalProcurementCost = $postData->processingFee + $postData->price;
        $procurementObj->totalWeight = '0.00';
        $procurementObj->status = 'submitted';
        $procurementObj->procurementLocked = 'Y';
        $procurementObj->totalPickupCost = $postData->pickupCost;
        $procurementObj->totalClearingDuty = '0.00';
        $procurementObj->isDutyCharged = '0';
        if (isset($postData->isInsuranceCharged) && $postData->isInsuranceCharged == 'Y') {
            $procurementObj->totalInsurance = $postData->insuranceCost;
        } else {
            $procurementObj->totalInsurance = '0.00';
        }
        $procurementObj->totalShippingCost = $postData->shippingCost;
        $procurementObj->totalCost = $postData->totalCost;
        $procurementObj->totalTax = $postData->tax;
        $procurementObj->totalDiscount = $postData->discountAmount;
        $procurementObj->couponcodeApplied = $postData->couponCode;
        $procurementObj->paymentStatus = $paymentStatus;
        $procurementObj->paymentMethodId = $postData->paymentMethodId;
        if (isset($postData->isCurrencyChanged))
            $procurementObj->isCurrencyChanged = $postData->isCurrencyChanged;
        else
            $procurementObj->isCurrencyChanged = 'N';
        $procurementObj->defaultCurrencyCode = $postData->defaultCurrency;
        $procurementObj->paidCurrencyCode = $postData->currencyCode;
        $procurementObj->exchangeRate = $postData->exchangeRate;
        $procurementObj->createdBy = $userId;
        $procurementObj->createdByType = 'user';
        $procurementObj->createdOn = Config::get('constants.CURRENTDATE');
        $procurementSave = $procurementObj->save();

        $procurementId = $procurementObj->id;

        $procurementItemObj->procurementId = $procurementId;
        $procurementItemObj->websiteUrl = $postData->carUrl;
        $procurementItemObj->carWebsiteId = $postData->website;
        $procurementItemObj->makeId = $postData->make;
        $procurementItemObj->modelId = $postData->model;
        $procurementItemObj->year = $postData->year;
        $procurementItemObj->options = $postData->color;
        $procurementItemObj->milage = $postData->milage;
        $procurementItemObj->vinNumber = $postData->vinnumber;
        if (isset($postData->itemImage) && $postData->itemImage != '')
            $procurementItemObj->itemImage = $postData->itemImage;
        $procurementItemObj->itemPrice = $postData->price;
        $procurementItemObj->itemQuantity = '1';
        $procurementItemObj->itemTotalCost = $postData->price;
        $procurementItemObj->status = 'submitted';
        $procurementItemObj->receivedDate = date('Y-m-d');
        $procItemSave = $procurementItemObj->save();
        $procurementItemId = $procurementItemObj->id;

        $procurementitemstatus = new Procurementitemstatus;
        $procurementitemstatus->procurementId = $procurementId;
        $procurementitemstatus->procurementItemId = $procurementItemId;
        $procurementitemstatus->oldStatus = 'none';
        $procurementitemstatus->status = 'submitted';
        $procurementitemstatus->updatedOn = Config::get('constants.CURRENTDATE');
        $procurementitemstatus->save();
        
        if(!empty($procurementId)) {
            if(isset($postData->tempDataId) && !empty($postData->tempDataId)) {

                $tempCartData = \App\Model\Temporarycartdata::find($postData->tempDataId);
                $tempCartData->isDataSaved = 'Y';
                $tempCartData->dataSavedId = $procurementId;
                $tempCartData->save();
            }
        }

        

        if (isset($postData->couponCode) && $postData->couponCode != '') {
            if (isset($postData->point) && $postData->point != '') {
                $pointData = Fundpoint::where('userId', $userId)->first();
                if (count($pointData) > 0) {
                    Fundpoint::where('userId', $userId)->update(['point' => ($pointData->point + $postData->point)]);
                } else {
                    $fundPoint = new Fundpoint;
                    $fundPoint->userId = $userId;
                    $fundPoint->point = $postData->point;
                    $fundPoint->save();
                }
            }

            $logData['code'] = $postData->couponCode;
            $logData['userId'] = $postData->userId;
            $logData['totalAmount'] = $postData->totalcostWithoutDiscount;
            $logData['couponId'] = $postData->couponId;
            if ($postData->discountAmount != '' || $postData->discountAmount != '0.00') {
                $logData['discountAmount'] = $postData->discountAmount;
            }
            if ($postData->point != '' || $postData->point != '0.00') {
                $logData['discountPoint'] = $postData->point;
            }

            Couponlog::insertLog($logData);
        }

        $param = array();
        $param['userId'] = $userId;
        $param['paidAmount'] = round(($postData->shippingCost + $postData->pickupCost + $postData->processingFee), 2);
        Fundpoint::calculateReferralBonus($param);

        Usercart::where('userId', $userId)->where('type', 'buy_a_car')->delete();
        
        if ($paymentStatus == 'failed' && $paymentErrorMessage != '') {
            
            $paymentTransaction = new Paymenttransaction;
            $paymentTransaction->userId = $userId;
            $paymentTransaction->paymentMethodId = $postData->paymentMethodId;
            $paymentTransaction->paidFor = 'buyacar';
            $paymentTransaction->status = $paymentStatus;
            $paymentTransaction->paidForId = '';
            $paymentTransaction->amountPaid = $postData->totalCost;
            if ($transactionId != 0) {
                $paymentTransaction->transactionId = $transactionId;
                $paymentTransaction->transactionData = $transactionData;
            }
            if ($postData->paymentMethodKey == 'wire_transfer') {
                $paymentTransaction->transactionData = json_encode(array(
                    'poNumber' => $postData->poNumber,
                    'companyName' => $postData->companyName,
                    'buyerName' => $postData->buyerName,
                    'position' => $postData->position,
                ));
            }
            if (!empty($transactionErrorMsg))
                $paymentTransaction->errorMsg = $transactionErrorMsg;
            $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
            $paymentTransaction->save();
            DB::commit();
            return array('payment_error'=>'1','error_msg'=>$paymentErrorMessage);
        } else {
            
            /*  PREPARE DATA FOR INVOICE PARTICULARS */

            $invoiceData = array(
                'shipment' => array(
                    'totalItemCost' => $postData->price,
                    'totalProcessingFee' => $postData->processingFee,
                    'urgentPurchaseCost' => '0.00',
                    'pickupCost' => $postData->pickupCost,
                    'shippingCost' => $postData->shippingCost,
                    'totalProcurementCost' => $postData->processingFee + $postData->price,
                    'insuranceCost' => '0.00',
                    'taxCost' => round($postData->tax, 2),
                    'discount' => $postData->discountAmount,
                    'point' => $postData->point,
                    'currencySymbol' => $postData->currencySymbol,
                    'currencyCode' => $postData->currencyCode,
                    'exchangeRate' => round($postData->exchangeRate, 2),
                ),
                'payment' => array(
                    'paymentMethodId' => $postData->paymentMethodId,
                    'paymentMethodName' => $postData->paymentMethodName,
                    'paymentMethodKey' => $postData->paymentMethodKey,
                ),
                'warehouse' => array(
                    'address' => $warehouseDetails->address,
                    'zipcode' => $warehouseDetails->zipcode,
                    'fromCountry' => $postData->countryName,
                    'fromState' => $postData->stateName,
                    'fromCity' => $postData->cityName,
                ),
                'itemDetails' => array(
                    'itemId' => $procurementItemId,
                    'website' => $postData->websiteName,
                    'url' => $postData->carUrl,
                    'make' => $postData->makeName,
                    'model' => $postData->modelName,
                    'year' => $postData->year,
                    'color' => $postData->color,
                ),
                'shippingaddress' => array(
                    'toCountry' => $userShippingAddress->country->name,
                    'toState' => ($userShippingAddress->stateId != '') ? $userShippingAddress->state->name : '',
                    'toCity' => ($userShippingAddress->cityId != '') ? $userShippingAddress->city->name : '',
                    'toAddress' => $userShippingAddress->address,
                    'toZipCode' => $userShippingAddress->zipcode,
                    'toName' => $userShippingAddress->firstName . ' ' . $userShippingAddress->lastName,
                    'toEmail' => $userShippingAddress->email,
                    'toPhone' => '+' . $userShippingAddress->isdCode . ' ' . $userShippingAddress->phone,
                )
            );

            if (isset($postData->isInsuranceCharged) && $postData->isInsuranceCharged == 'Y') {
                $invoiceData['shipment']['insuranceCost'] = round($postData->insuranceCost, 2);
            }
            if ($postData->paymentMethodKey == 'wire_transfer') {
                $invoiceData['payment']['poNumber'] = $postData->poNumber;
                $invoiceData['payment']['companyName'] = $postData->companyName;
                $invoiceData['payment']['buyerName'] = $postData->buyerName;
                $invoiceData['payment']['position'] = $postData->position;
            }
            if ($postData->paymentMethodKey == 'paypal_checkout' || $postData->paymentMethodKey == 'credit_debit_card') {
                $invoiceData['payment']['cardNumber'] = $postData->cardNumber;
                $invoiceData['payment']['cartType'] = $postData->cardType;
            }

            /*  INSERT DATA INTO INVOICE TABLE */
            $invoice = new Invoice;
            if ($paymentStatus == 'unpaid') {
                $invoiceUniqueId = 'INV' . $userDetails->unit . '-' . $procurementId . '-' . date('Ymd');
                $invoice->invoiceType = 'invoice';
            } else if ($paymentStatus == 'paid') {
                $invoiceUniqueId = 'RECP' . $userDetails->unit . '-' . $procurementId . '-' . date('Ymd');
                $invoice->invoiceType = 'receipt';
            }

            $invoice->invoiceUniqueId = $invoiceUniqueId;
            $invoice->procurementId = $procurementId;
            $invoice->type = 'buycarforme';
            $invoice->userUnit = $userDetails->unit;
            $invoice->userFullName = $userDetails->firstName . ' ' . $userDetails->lastName;
            $invoice->userEmail = $userDetails->email;
            $invoice->userContactNumber = $userDetails->contactNumber;
            $invoice->billingName = $userBillingAddress->title . ' ' . $userBillingAddress->firstName . ' ' . $userBillingAddress->lastName;
            $invoice->billingEmail = $userBillingAddress->email;
            $invoice->billingAddress = $userBillingAddress->address;
            $invoice->billingAlternateAddress = $userBillingAddress->alternateAddress;
            $invoice->billingCity = isset($userBillingAddress->city) ? $userBillingAddress->city->name : '';
            $invoice->billingState = isset($userBillingAddress->state) ? $userBillingAddress->state->name : '';
            $invoice->billingCountry = $userBillingAddress->country->name;
            $invoice->billingZipcode = $userBillingAddress->zipcode;
            $invoice->billingPhone = '+' . $userBillingAddress->isdCode . ' ' . $userBillingAddress->phone;
            if ($userBillingAddress->altIsdCode != '')
                $invoice->billingAlternatePhone = '+' . $userBillingAddress->altIsdCode . ' ' . $userBillingAddress->alternatePhone;
            else
                $invoice->billingAlternatePhone = $userBillingAddress->alternatePhone;
            $invoice->totalBillingAmount = $postData->totalCost;
            $invoice->invoiceParticulars = json_encode($invoiceData);
            if (isset($postData->paymentMethodId) && $postData->paymentMethodId != '')
                $invoice->paymentMethodId = $postData->paymentMethodId;
            $invoice->paymentStatus = $paymentStatus;
            $invoice->createdOn = Config::get('constants.CURRENTDATE');
            $invoice->save();

            $invoiceId = $invoice->id;

            $paymentTransaction = new Paymenttransaction;
            $paymentTransaction->userId = $userId;
            $paymentTransaction->paymentMethodId = $postData->paymentMethodId;
            $paymentTransaction->paidFor = 'buyacar';
            $paymentTransaction->status = $paymentStatus;
            $paymentTransaction->paidForId = $procurementId;
            $paymentTransaction->invoiceId = $invoice->id;
            $paymentTransaction->amountPaid = $postData->totalCost;
            if (!empty($transactionId)) {
                $paymentTransaction->transactionId = $transactionId;
                $paymentTransaction->transactionData = $transactionData;
            }
            if ($postData->paymentMethodKey == 'wire_transfer') {
                $paymentTransaction->transactionData = json_encode(array(
                    'poNumber' => $postData->poNumber,
                    'companyName' => $postData->companyName,
                    'buyerName' => $postData->buyerName,
                    'position' => $postData->position,
                ));
            }
            if (!empty($transactionErrorMsg))
                $paymentTransaction->errorMsg = $transactionErrorMsg;
            $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
            $paymentTransaction->save();
            DB::commit();
        }

        
        if ($procurementSave && $procItemSave) {
            return array('procurementId' => $procurementId, 'invoiceId' => $invoiceId, 'userEmail' => $userDetails->email, 'invoiceUniqueId' => $invoiceUniqueId, 'paymentStatus' => $paymentStatus);
        } else {
            return false;
        }
    }

    /**
     * Method for get details for the cars I have bought
     * @param interger $id
     * @return object
     */
    public static function getcarsihaveboughtdetails($userId, $perPage, $offset1) {
        $statusExcept = "completed";

        $procurement = new Procurement;
        $procurementTable = $procurement->prefix . $procurement->table;

        $procurementItem = new Procurementitem;
        $procurementItemTable = $procurementItem->table;

        $procurementitemstatus = new Procurementitemstatus;
        $procurementItemTable = $procurementitemstatus->table;

        $city = new City;
        $state = new State;
        $country = new Country;
        $autoWebsite = new Autowebsite;
        $make = new Automake;
        $model = new Automodel;

        $resultSet = Procurement::select('*', "ITEM.carWebsiteId", "ITEM.id", "FROMCOUNTRY.name AS FROMCOUNTRY", "TOCOUNTRY.name AS TOCOUNTRY", "FROMSTATE.name as FROMSTATE", "TOSTATE.name as TOSTATE", "FROMCITY.name as FROMCITY", "TOCITY.name as TOCITY", "CARWEBSITE.name as CARWEBSITENAME", "MAKENAME.name as MAKENAME", "MODELNAME.name as MODELNAME")
                ->leftJoin($country->table . " AS FROMCOUNTRY", $procurement->table . ".fromCountry", '=', "FROMCOUNTRY.id")
                ->leftJoin($country->table . " AS TOCOUNTRY", $procurement->table . ".toCountry", '=', "TOCOUNTRY.id")
                ->leftJoin($state->table . " AS FROMSTATE", $procurement->table . ".fromState", '=', "FROMSTATE.id")
                ->leftJoin($state->table . " AS TOSTATE", $procurement->table . ".toState", '=', "TOSTATE.id")
                ->leftJoin($city->table . " AS FROMCITY", $procurement->table . ".fromCity", '=', "FROMCITY.id")
                ->leftJoin($city->table . " AS TOCITY", $procurement->table . ".toCity", '=', "TOCITY.id")
                ->leftJoin($procurementItem->table . " AS ITEM", $procurement->table . ".id", '=', "ITEM.procurementId")
                ->leftJoin($make->table . " AS MAKENAME", "ITEM.makeId", '=', "MAKENAME.id")
                ->leftJoin($model->table . " AS MODELNAME", "ITEM.modelId", '=', "MODELNAME.id")
                ->leftJoin($autoWebsite->table . " AS CARWEBSITE", "CARWEBSITE.id", '=', "ITEM.carWebsiteId")
                ->where($procurement->table . '.status', '!=', "'" . $statusExcept . "'")
                ->where($procurement->table . '.deleted', '0')
                ->where($procurement->table . '.procurementType', 'buycarforme')
                ->where($procurement->table . '.userId', '=', $userId)
                ->orderBy($procurement->table . '.id', 'desc')
                //->paginate($param['searchDisplay']);
                ->take($perPage)->skip($offset1)
                ->get();

        return $resultSet;
    }

    /**
     * Method for get total count for the cars I have bought
     * @param interger $id
     * @return integer
     */
    public static function gettotalcarsihaveboughtdetails($userId) {
        $statusExcept = "completed";

        $procurement = new Procurement;
        $procurementTable = $procurement->prefix . $procurement->table;

        $procurementItem = new Procurementitem;
        $procurementItemTable = $procurementItem->table;

        $procurementitemstatus = new Procurementitemstatus;
        $procurementItemTable = $procurementitemstatus->table;

        $city = new City;
        $state = new State;
        $country = new Country;
        $autoWebsite = new Autowebsite;
        $make = new Automake;
        $model = new Automodel;

        $resultSetCount = Procurement::select('*', "ITEM.carWebsiteId", "ITEM.id", "FROMCOUNTRY.name AS FROMCOUNTRY", "TOCOUNTRY.name AS TOCOUNTRY", "FROMSTATE.name as FROMSTATE", "TOSTATE.name as TOSTATE", "FROMCITY.name as FROMCITY", "TOCITY.name as TOCITY", "CARWEBSITE.name as CARWEBSITENAME", "MAKENAME.name as MAKENAME", "MODELNAME.name as MODELNAME")
                ->leftJoin($country->table . " AS FROMCOUNTRY", $procurement->table . ".fromCountry", '=', "FROMCOUNTRY.id")
                ->leftJoin($country->table . " AS TOCOUNTRY", $procurement->table . ".toCountry", '=', "TOCOUNTRY.id")
                ->leftJoin($state->table . " AS FROMSTATE", $procurement->table . ".fromState", '=', "FROMSTATE.id")
                ->leftJoin($state->table . " AS TOSTATE", $procurement->table . ".toState", '=', "TOSTATE.id")
                ->leftJoin($city->table . " AS FROMCITY", $procurement->table . ".fromCity", '=', "FROMCITY.id")
                ->leftJoin($city->table . " AS TOCITY", $procurement->table . ".toCity", '=', "TOCITY.id")
                ->leftJoin($procurementItem->table . " AS ITEM", $procurement->table . ".id", '=', "ITEM.procurementId")
                ->leftJoin($make->table . " AS MAKENAME", "ITEM.makeId", '=', "MAKENAME.id")
                ->leftJoin($model->table . " AS MODELNAME", "ITEM.modelId", '=', "MODELNAME.id")
                ->leftJoin($autoWebsite->table . " AS CARWEBSITE", "CARWEBSITE.id", '=', "ITEM.carWebsiteId")
                ->where($procurement->table . '.status', '!=', "'" . $statusExcept . "'")
                ->where($procurement->table . '.deleted', '0')
                ->where($procurement->table . '.procurementType', 'buycarforme')
                ->where($procurement->table . '.userId', '=', $userId)
                ->orderBy($procurement->table . '.id', 'desc')
                //->paginate($param['searchDisplay']);
                //->take($perPage)->skip($offset1)
                ->count();

        return $resultSetCount;
    }

    public static function fetchUsedFieldIds($field) {

        $resultset = Procurement::distinct()->whereNotNull($field)->where("deleted", "0")->pluck($field)->toArray();

        return $resultset;
    }



    /**
     * Method used to fetch all type pf procurements
     * @param array $param
     * @param string $type
     * @return object
     */
    public static function getProcurementList($param, $type = '', $selected = '') {
        $procurement = new Procurement;
        $user = new User;
        $city = new City;
        $state = new State;
        $country = new Country;
        $warehouse = new Warehouse;

        $procurementItem = new Procurementitem;

        $procurementTable = $procurement->prefix . $procurement->table;
        $userTable = $procurement->prefix . $user->table;

        $where = "$procurementTable.deleted= '0'";


        if (!empty($param['searchProcurement']['idFrom']))
            $where .= " AND $procurementTable.id >= " . $param['searchProcurement']['idFrom'];

        if (!empty($param['searchProcurement']['idTo']))
            $where .= " AND $procurementTable.id <= " . $param['searchProcurement']['idTo'];

        if (!empty($param['searchProcurement']['deliveryCompanyId']))
            $where .= " AND $procurement->prefix$procurementItem->table.deliveryCompanyId = " . $param['searchProcurement']['deliveryCompanyId'];

        if (!empty($param['searchProcurement']['status']))
            $where .= " AND $procurementTable.status = '" . $param['searchProcurement']['status'] . "'";

        if (!empty($param['searchProcurement']['paymentStatus']))
            $where .= " AND $procurementTable.paymentStatus = '" . $param['searchProcurement']['paymentStatus'] . "'";

        if (!empty($param['searchProcurement']['totalCostFrom']))
            $where .= " AND $procurementTable.totalProcurementCost >= '" . $param['searchProcurement']['totalCostFrom'] . "'";

        if (!empty($param['searchProcurement']['totalCostTo']))
            $where .= " AND $procurementTable.totalProcurementCost <= '" . $param['searchProcurement']['totalCostTo'] . "'";

        if (!empty($param['searchProcurement']['user']))
            $where .= " AND ($userTable.firstName LIKE '%" . $param['searchProcurement']['user'] . "%' OR $userTable.lastName LIKE '%" . $param['searchProcurement']['user'] . "%' OR $userTable.email LIKE '%" . $param['searchProcurement']['user'] . "%')";

        if (!empty($param['searchProcurement']['toCountry']))
            $where .= " AND $procurementTable.toCountry = " . $param['searchProcurement']['toCountry'];

        if (!empty($param['searchProcurement']['toState']))
            $where .= " AND $procurementTable.toState = " . $param['searchProcurement']['toState'];

        if (!empty($param['searchProcurement']['toCity']))
            $where .= " AND $procurementTable.toCity = " . $param['searchProcurement']['toCity'];

        if (!empty($param['searchWarehouse']))
            $where .= " AND $procurementTable.warehouseId = " . $param['searchWarehouse'];

        if(isset($param['searchOffer'])){
            $where .= " AND couponcodeApplied = '". $param['searchOffer'] . "'";
        }

        if ($param['searchByCreatedOn'] != '') {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($procurementTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $procurementTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($procurementTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($procurementTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        if ($param['field'] != 'firstName') {
            $param['field'] = $procurement->table . "." . $param['field'];
        } else {
            $param['field'] = $user->table . "." . $param['field'];
        }

        if (!empty($param['searchByshipment'])) {
            $sfmArray = implode("','", $param['searchByshipment']);
            $where .= " AND $procurementTable.id IN ('" . $sfmArray . "')";
        }


        if ($type == 'export') {

            if (!empty($selected)) {
                $where = "$procurementTable.id IN (" . implode(',', $selected) . ")";
            }

            $resultSet = $resultSet = Procurement::select(array("$procurement->table.id as id",
                        DB::raw("CONCAT($userTable.firstName, ' ', $userTable.lastName) AS customer"),
                        "$state->table.name AS destinationStateName", "$city->table.name AS destinationCityName",
                        "$country->table.name AS destinationCountryName", 
                        "$procurement->table.totalCost as totalcost", "$procurement->table.totalDiscount as totalDiscount",
                        "$procurement->table.totalWeight", 
                        "$procurement->table.paymentStatus as paymentstatus", "$procurement->table.createdOn",
                        "$procurement->table.status"
                    ))
                    ->leftJoin($user->table, "$user->table.id", '=', "$procurement->table.userId")
                    ->leftJoin($state->table, "$state->table.id", '=', "$procurement->table.toState")
                    ->leftJoin($city->table, "$city->table.id", '=', "$procurement->table.toCity")
                    ->leftJoin($country->table, "$country->table.id", '=', "$procurement->table.toCountry")
                    ->leftJoin($warehouse->table, "$procurement->table.warehouseId", '=', "$warehouse->table.id")
                    ->leftJoin($country->table . " AS WC", "$warehouse->table.countryId", '=', "WC.id")
                    ->leftJoin($procurementItem->table, "$procurementItem->table.procurementId", '=', "$procurement->table.id")
                    ->whereRaw($where)
                    ->orderBy($param['field'], $param['type'])
                    ->get();
        } else {

            $resultSet = Procurement::select(array("$procurement->table.id", "$procurement->table.userId",
                        "$procurement->table.toCountry", "$procurement->table.toState", "$procurement->table.createdOn",
                        "$procurement->table.toCity", "$procurement->table.totalProcurementCost",
                        "$procurement->table.status", "$procurement->table.paymentStatus", "$procurement->table.totalWeight", 
                        "$procurement->table.totalDiscount", "$procurement->table.totalCost",
                        "$user->table.firstName", "$user->table.lastName", "$user->table.email",
                        "$city->table.name AS toCityName", "$state->table.name AS toStateName",
                        "$country->table.name AS toCountryName", "$procurement->table.warehouseId"
                    ))
                    ->leftJoin($user->table, "$procurement->table.userId", '=', "$user->table.id")
                    ->leftJoin($city->table, "$procurement->table.toCity", '=', "$city->table.id")
                    ->leftJoin($state->table, "$procurement->table.toState", '=', "$state->table.id")
                    ->leftJoin($country->table, "$procurement->table.toCountry", '=', "$country->table.id")
                    ->leftJoin($warehouse->table, "$procurement->table.warehouseId", '=', "$warehouse->table.id")
                    ->leftJoin($country->table . " AS WC", "$warehouse->table.countryId", '=', "WC.id")
                    ->leftJoin($procurementItem->table, "$procurementItem->table.procurementId", '=', "$procurement->table.id")
                    ->whereRaw($where)
                    ->groupBy("$procurement->table.id")
                    ->orderBy($param['field'], $param['type'])
                    ->paginate($param['searchDisplay']);
        }

        return $resultSet;
    }

}
