<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Newsletter extends Model {

    public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.NEWSLETTER');
    }

    /**
     * Method used to fetch email template list
     * @param array $param
     * @return object
     */
    public static function getNewsletterList($param) {
        
        $where = "deleted='0' AND status='1' AND expired='0'"; 
        
        $resultSet = Newsletter::select('*')                
                ->whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

}
