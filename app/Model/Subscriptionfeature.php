<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Subscriptionfeature extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    protected $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.SUBSCRIPTIONFEATURE');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to change status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Subscriptionfeature::where('id', $id)
                ->update(array('status' => $newStatus));

        return $row;
    }

    /**
     * Method used to change status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeFeatureStatus($id, $createrModifierId, $type, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        if ($type == 'free')
            $field = 'freeFeature';
        else
            $field = 'subscribedFeature';

        $row = Subscriptionfeature::where('id', $id)
                ->update(array($field => $newStatus));

        return $row;
    }

}
