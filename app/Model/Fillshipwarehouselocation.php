<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
use Spatie\Activitylog\Traits\LogsActivity;

class Fillshipwarehouselocation extends Model {

    use LogsActivity;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    protected $prefix;
    public $timestamps = false;
    protected static $logName = 'fillship_warehouseLocation';

    protected static $logAttributes = ['id', 'fillshipId', 'warehouseRowId', 'warehouseZoneId'];

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.FILLSHIPWAREHOUSE');
        $this->prefix = DB::getTablePrefix();
    }
    
    public static function getWarehouLocation($shipmentids) {
        

        $shipmentWarehouse = new Fillshipwarehouselocation;
        $warehouselocation = new Warehouselocation; 
        $shipmentLabel = new Shipmentlabelprintlog;

        $shipmentWarehouseTable = $shipmentWarehouse->prefix . $shipmentWarehouse->table;
        $warehouselocationTable = $shipmentWarehouse->prefix . $warehouselocation->table;
        $shipmentLabelTable = $shipmentLabel->prefix . $shipmentLabel->table;
        
        $resultSet = Fillshipwarehouselocation::select(array("$shipmentWarehouse->table.fillshipId as id",
                    DB::raw("GROUP_CONCAT(CONCAT(".$shipmentWarehouse->prefix."warehouseRow.name,' ',".$shipmentWarehouse->prefix."warehouseZone.name) SEPARATOR ',') as shipmentLocation")
                    ))
                ->leftJoin("$warehouselocation->table AS warehouseRow", "warehouseRow.id", '=', "$shipmentWarehouse->table.warehouseRowId")
                ->leftJoin("$warehouselocation->table AS warehouseZone", "warehouseZone.id", '=', "$shipmentWarehouse->table.warehouseZoneId")
                ->whereRaw("$shipmentWarehouseTable.fillshipId in (".$shipmentids.")")
                ->groupBy("$shipmentWarehouse->table.fillshipId")
                ->get();
        return $resultSet;
    }

}
