<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Automodel extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.AUTOMODEL');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to fetch model record list
     * @param array $param
     * @return object
     */
    public static function getList($param) {
        $make = new Automake;
        $model = new Automodel;

        $where = "1";

        $modelTable = $model->prefix . $model->table;

        if (!empty($param['searchByMake']))
            $where .= "  AND $modelTable.makeId ='" . $param['searchByMake'] . "'";

        $resultSet = Automodel::select(array("$model->table.id", "$model->table.insurance", "$model->table.makeType", "$make->table.name AS makeName", "$model->table.name",))
                ->leftJoin($make->table, "$model->table.makeId", '=', "$make->table.id")
                ->where($model->table . '.deleted', '0')
                ->whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Automodel::where('id', $id)->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }
    
    /**
     * Method used to get Make and Model
     * @return array
     */
    
    public static function getMakeModelList()
    {       
        $make = new Automake;
        $model = new Automodel;
        
        
        $resultSet = Automodel::select(array("$model->table.id AS modelId", "$model->table.name AS modelName", "$make->table.name AS makeName", "$make->table.id AS makeId",))
                ->leftJoin($make->table, "$model->table.makeId", '=', "$make->table.id")
                ->where($make->table . '.deleted', '0')
                ->where($model->table . '.deleted', '0')
                ->orderBy("$make->table.name", "asc")
                ->get();
        
        return $resultSet;
    }

}
