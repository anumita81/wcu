<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Consolidatedtrackingnumber extends Model {

    public $table;
    protected $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.CONSOLIDATEDNUMBERS');
        $this->prefix = DB::getTablePrefix();
    }

}
