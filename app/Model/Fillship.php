<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
use customhelper;

class Fillship extends Model {

    public $table;
    protected $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.FILLSHIP');
        $this->prefix = DB::getTablePrefix();
    }

    public function filshipboxinfo() {
        return $this->hasOne('App\Model\Fillshipbox', 'id', 'boxId');
    }

    public static function getShipmentList($param, $type="") {

        DB::enablequerylog();

        $fillnship = new Fillship;
        $fillnshipItem = new Fillshipitem;
        $user = new User;
        $city = new City;
        $state = new State;
        $country = new Country;
        $warehouse = new Warehouse;

        $fillnshipTable = $fillnship->prefix . $fillnship->table;
        $userTable = $user->prefix . $user->table;

        $where = "$fillnshipTable.deleted= '0'";


        if (!empty($param['searchShipment']['status']))
            $where .= " AND $fillnshipTable.status = '" . $param['searchShipment']['status'] . "'";

        if (!empty($param['searchShipment']['unit']))
            $where .= " AND $userTable.unit = '" . $param['searchShipment']['unit'] . "'";

        if (!empty($param['searchShipment']['shipmentId']))
            $where .= " AND $fillnshipTable.id = '" . $param['searchShipment']['shipmentId'] . "'";

        if (!empty($param['searchShipment']['deliveryCompanyId']))
            $where .= " AND $fillnshipTable.id = '" . $param['searchShipment']['deliveryCompanyId'] . "'";

        if (!empty($param['searchShipment']['receiver']))
            $where .= " AND $fillnshipTable.userId = '" . $param['searchShipment']['receiver'] . "'";


        if (!empty($param['searchShipment']['paymentStatus']))
            $where .= " AND $fillnshipTable.paymentStatus = '" . $param['searchShipment']['paymentStatus'] . "'";

        if (!empty($param['searchShipment']['totalCost']))
            $where .= " AND $fillnshipTable.totalCost >= '" . $param['searchShipment']['totalCost'] . "'";

        if (!empty($param['searchShipment']['toCountry']))
            $where .= " AND $fillnshipTable.toCountry = '" . $param['searchShipment']['toCountry'] . "'";

        if (!empty($param['searchShipment']['toState']))
            $where .= " AND $fillnshipTable.toState = '" . $param['searchShipment']['toState'] . "'";

        if (!empty($param['searchShipment']['toCity']))
            $where .= " AND $fillnshipTable.toCity = '" . $param['searchShipment']['toCity'] . "'";

        if(isset($param['searchOffer'])){
            $where .= " AND couponcodeApplied = '". $param['searchOffer'] . "'";
        }

        if ($param['searchByCreatedOn'] != '' || !empty($param['searchByCreatedOn'])) {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($fillnshipTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $fillnshipTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($fillnshipTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($fillnshipTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        if (!empty($param['searchByshipment'])) {
            $shipmentArray = implode("','", $param['searchByshipment']);
            $where .= " AND $fillnshipTable.id IN ('" . $shipmentArray . "')";
        }



        if ($type == 'export') {
           
            $resultSet = $resultSet = Fillship::select(array("$fillnship->table.id as shipmentid",
                        "$fillnship->table.totalCost as totalcost",
                        "$fillnship->table.status", "$fillnship->table.paymentStatus as paymentstatus",
                        DB::raw("CONCAT($userTable.firstName, ' ', $userTable.lastName) AS customer"),
                        "$state->table.name AS destinationStateName",
                        "$city->table.name AS destinationCityName",
                        "$country->table.name AS destinationCountryName", "$fillnship->table.createdOn",
                    ))
                    ->leftJoin($user->table, "$fillnship->table.userId", '=', "$user->table.id")
                    ->leftJoin($city->table, "$fillnship->table.toCity", '=', "$city->table.id")
                    ->leftJoin($state->table, "$fillnship->table.toState", '=', "$state->table.id")
                    ->leftJoin($country->table, "$fillnship->table.toCountry", '=', "$country->table.id")
                    ->leftJoin($warehouse->table, "$fillnship->table.warehouseId", '=', "$warehouse->table.id")
                    ->leftJoin($country->table . " AS WC", "$warehouse->table.countryId", '=', "WC.id")
                    ->leftJoin($fillnshipItem->table, "$fillnshipItem->table.fillshipId", '=', "$fillnship->table.id")
                    ->whereRaw($where)
                    ->groupBy("$fillnship->table.id")
                    ->orderBy($param['field'], $param['type'])->orderBy("$fillnship->table.id", "desc")
                    ->get();
             //dd( DB::getQueryLog());       
        }else{
            $resultSet = Fillship::select(array("$fillnship->table.id AS shipmentId", "$fillnship->table.userId","$fillnship->table.shippOutOpt",
                    "$fillnship->table.toCountry", "$fillnship->table.toState", "$fillnship->table.createdOn",
                    "$fillnship->table.toCity", "$fillnship->table.totalCost",
                    "$fillnship->table.status", "$fillnship->table.paymentStatus", "$fillnship->table.totalDiscount",
                    "$fillnship->table.totalChargeableWeight as totalWeight",
                    "$user->table.unit", "$user->table.firstName", "$user->table.lastName", "$user->table.email",
                    "$city->table.name AS toCityName", "$state->table.name AS toStateName",
                    "$country->table.name AS toCountryName", "$fillnship->table.warehouseId",
                    "WC.name as warehouseName"))
                ->leftJoin($user->table, "$fillnship->table.userId", '=', "$user->table.id")
                ->leftJoin($city->table, "$fillnship->table.toCity", '=', "$city->table.id")
                ->leftJoin($state->table, "$fillnship->table.toState", '=', "$state->table.id")
                ->leftJoin($country->table, "$fillnship->table.toCountry", '=', "$country->table.id")
                ->leftJoin($warehouse->table, "$fillnship->table.warehouseId", '=', "$warehouse->table.id")
                ->leftJoin($country->table . " AS WC", "$warehouse->table.countryId", '=', "WC.id")
                ->leftJoin($fillnshipItem->table, "$fillnshipItem->table.fillshipId", '=', "$fillnship->table.id")
                ->whereRaw($where)
                ->groupBy("$fillnship->table.id")
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        }

        
        //dd( DB::getQueryLog());
        return $resultSet;
    }

    /**
     * Method used to get from city, state, country
     * @param integer $id
     * @return boolean
     */
    public static function getFromCountryetc($id) {
        if (empty($id))
            return false;

        $row = false;

        $fillship = new Fillship;
        $country = new Country;
        $state = new State;
        $city = new City;

        $resultSet = Fillship::select(
                        "$country->table.name as fromCountryName", "$state->table.name as fromStateName", "$city->table.name as fromCityName", "$fillship->table.id"
                )
                ->leftJoin("$country->table", "$country->table.id", "=", "$fillship->table.fromCountry")
                ->leftJoin("$state->table", "$state->table.id", "=", "$fillship->table.fromState")
                ->leftJoin("$city->table", "$city->table.id", "=", "$fillship->table.fromCity")
                ->where("$fillship->table.id", $id)
                ->where("$fillship->table.deleted", '0')
                ->first();

        return $resultSet;
    }

    /**
     * Method used to get to city, state, country
     * @param integer $id
     * @return boolean
     */
    public static function getToCountryetc($id) {
        if (empty($id))
            return false;

        $row = false;

        $fillship = new Fillship;
        $country = new Country;
        $state = new State;
        $city = new City;

        $resultSet = Fillship::select(
                        "$country->table.name as toCountryName", "$state->table.name as toStateName", "$city->table.name as toCityName", "$fillship->table.id"
                )
                ->leftJoin("$country->table", "$country->table.id", "=", "$fillship->table.toCountry")
                ->leftJoin("$state->table", "$state->table.id", "=", "$fillship->table.toState")
                ->leftJoin("$city->table", "$city->table.id", "=", "$fillship->table.toCity")
                ->where("$fillship->table.id", $id)
                ->where("$fillship->table.deleted", '0')
                ->first();

        return $resultSet;
    }

    /**
     * Method used to get shipment and payment method name
     * @param integer $id
     * @return boolean
     */
    public static function getShipmentAndPayment($id) {
        $fillship = new Fillship;
        $paymentmethod = new Paymentmethod;
        $shippingmethod = new Shippingmethods;

        $resultSet = Fillship::select(
                        "$paymentmethod->table.paymentMethod as paymentMethodId", "$shippingmethod->table.shipping as shippingMethodId", "$fillship->table.id"
                )
                ->leftJoin("$paymentmethod->table", "$paymentmethod->table.id", "=", "$fillship->table.paymentMethodId")
                ->leftJoin("$shippingmethod->table", "$shippingmethod->table.shippingid", "=", "$fillship->table.shippingMethodId")
                ->where("$fillship->table.id", $id)
                ->where("$fillship->table.deleted", '0')
                ->first();

        return $resultSet;
    }

    public static function proceedShipout($fillshipShipmentId, $userId, $ewalletTransferAmount = '0.00') {

        $fillshipShipmentInfo = Fillship::find($fillshipShipmentId);
        $user = User::find($userId);

        $orderObj = new \App\Model\Order;
        $orderObj->shipmentId = $fillshipShipmentId;
        $orderObj->userId = $userId;
        $orderObj->totalCost = $fillshipShipmentInfo->totalCost;
        $orderObj->status = '2';
        $orderObj->type = 'fillship';
        $orderObj->createdBy = $userId;
        $orderObj->save();
        $orderNumber = \App\Model\Order::generateOrderNumber($orderObj->id, $orderObj->userId);
        $orderObj->orderNumber = $orderNumber;
        $orderObj->save();

        if ($ewalletTransferAmount != '0.00') {
            $userEwalletInfo = Ewallet::where('recipient', $userId)->first();
            if (!empty($userEwalletInfo)) {
                $userEwallet = Ewallet::where('recipient', $userId)->increment('amount', $ewalletTransferAmount);
            } else {
                $userEwalletInfo = new Ewallet;
                $userEwalletInfo->ewalletId = strtoupper(md5(uniqid(rand())));
                $userEwalletInfo->purchaser = 'Shoptomydoor';
                $userEwalletInfo->recipient = $userId;
                $userEwalletInfo->recipientEmail = $user->email;
                $userEwalletInfo->message = 'E wallet credited for fill and ship';
                $userEwalletInfo->amount = $ewalletTransferAmount;
                $userEwalletInfo->debit = $ewalletTransferAmount;
                $userEwalletInfo->status = '1';
                $userEwalletInfo->addDate = date('Y-m-d');
                $userEwalletInfo->save();
            }
            $ewalletTransaction = new \App\Model\Ewallettransaction;
            $ewalletTransaction->userId = $userId;
            $ewalletTransaction->ewalletId = $userEwalletInfo->id;
            $ewalletTransaction->amount = $ewalletTransferAmount;
            $ewalletTransaction->transactionType = 'credit';
            $ewalletTransaction->transactionOn = Config::get('constants.CURRENTDATE');
            $ewalletTransaction->save();
        }

        Fillship::where('id', $fillshipShipmentId)->update(['shippOutOpt' => '1']);

        $emailTemplate = Emailtemplate::where('templateKey', 'fillship_shippout_request')->first();
        $to = $user->email;
        $replace['[NAME]'] = $user->firstName . ' ' . $user->lastName;
        $replace['[SHIPMENTID]'] = $orderNumber;
        $isSend = customhelper::SendMail($emailTemplate, $replace, $to);
        
        $emailTemplate = Emailtemplate::where('templateKey', 'user_shipmybox_request')->first();
        //$to = UserAdmin::find(1)->email;
        $to = "contact@shoptomydoor.com";
        $replace['[NAME]'] = $user->firstName . ' ' . $user->lastName;
        $replace['[SHIPMENTID]'] = $fillshipShipmentId;
        $replace['[ORDERID]'] = $orderNumber;
        $isSend = customhelper::SendMail($emailTemplate, $replace, $to);
        
        $contact = new Contactus;
        $contact->name = $user->firstName . " " . $user->lastName;
        $contact->address = "";
        $contact->alternateAddress = "";
        $contact->country = $user->countryId;
        $contact->state = $user->stateId;
        $contact->city = $user->cityId;
        $contact->company = $user->company;
        $contact->phone = $user->contactNumber;
        $contact->email = $user->email;
        $contact->subject = "Fill & Ship Box Shipout Request";
        $contact->email = $user->email;
        $contact->message = "Fill and ship box shipout request for shipment #".$fillshipShipmentId." having order #".$orderNumber;
        $contact->status = '1';
        $contact->reason_id = '13';
        $contact->postedOn = Config::get('constants.CURRENTDATE');
        $contact->save();

        return 1;
    }

    /**
     * Method used to fetch Fill n ship details
     * @return object
     */
    public static function getFillnShipDetails($id) {

        $fillnship = new Fillship;
        $fillnshipItem = new Fillshipitem;
        $fillnshipbox = new Fillshipbox;
        $user = new User;
        $city = new City;
        $state = new State;
        $country = new Country;
        $warehouse = new Warehouse;

        $fillnshipItemTable = $fillnship->prefix . $fillnshipItem->table;


        $resultSet = Fillship::where("$fillnship->table.id", $id)
                ->select(array("$fillnship->table.*", "$user->table.unit AS userUnit", "$country->table.name AS warehouseName", "TC.name AS toCityName", "$fillnshipbox->table.chargeableWeight AS boxWeight", "TS.name AS toStateName", "TCO.name AS toCountryName",
                    "FC.name AS fromCityName", "FS.name AS fromStateName", "FCO.name AS fromCountryName",
                    DB::raw("(SELECT SUM(itemQuantity) FROM $fillnshipItemTable WHERE deleted ='0' AND 	fillshipId = $id)  AS totalQuantity"), DB::raw("(SELECT COUNT(id) FROM $fillnshipItemTable WHERE  deleted ='0' AND 	fillshipId = $id)  AS totalPackage")))
                ->leftJoin($user->table, "$fillnship->table.userId", '=', "$user->table.id")
                ->leftJoin($warehouse->table, "$fillnship->table.warehouseId", '=', "$warehouse->table.id")
                ->leftJoin($country->table, "$warehouse->table.countryId", '=', "$country->table.id")
                ->leftJoin($city->table . " AS TC", "$fillnship->table.toCity", '=', "TC.id")
                ->leftJoin($state->table . " AS TS", "$fillnship->table.toState", '=', "TS.id")
                ->leftJoin($country->table . " AS TCO", "$fillnship->table.toCountry", '=', "TCO.id")
                ->leftJoin($city->table . " AS FC", "$fillnship->table.fromCity", '=', "FC.id")
                ->leftJoin($state->table . " AS FS", "$fillnship->table.fromState", '=', "FS.id")
                ->leftJoin($country->table . " AS FCO", "$fillnship->table.fromCountry", '=', "FCO.id")
                ->leftJoin($fillnshipbox->table, "$fillnship->table.boxId", '=', "$fillnshipbox->table.id")
                ->first();

        return $resultSet;
    }

     /**
     * Method to fetch delivery details data
     * @param array $data
     * @return array
     */
    public static function getDeliveryDetails($data) {
        if (empty($data))
            return false;

        $deliveryData = [];
        $deliveryData['deliveries'] = [];

        $totalPackage = $totalWeight = $totalChargeableWeight = $totalValue = $totalDelivery = 0;
        $itemTypeList = '';

        foreach ($data as $row) {
            if (!array_key_exists($row['deliveryId'], $deliveryData['deliveries'])) {
                $totalWeight = $totalWeight + $row['deliveryWeight'];
                $totalChargeableWeight = $totalChargeableWeight + $row['deliveryChargeableWeight'];
                
                $deliveryData['deliveries'][$row['deliveryId']]['id'] = $row['deliveryId'];
                $deliveryData['deliveries'][$row['deliveryId']]['length'] = $row['deliveryLength'];
                $deliveryData['deliveries'][$row['deliveryId']]['width'] = $row['deliveryWidth'];
                $deliveryData['deliveries'][$row['deliveryId']]['height'] = $row['deliveryHeight'];
                $deliveryData['deliveries'][$row['deliveryId']]['weight'] = $row['deliveryWeight'];
                $deliveryData['deliveries'][$row['deliveryId']]['chargeableWeight'] = $row['deliveryChargeableWeight'];
                $deliveryData['deliveries'][$row['deliveryId']]['totalItemCost'] = $row['totalItemCost'];
                $deliveryData['deliveries'][$row['deliveryId']]['shippingCost'] = $row['shippingCost'];
                $deliveryData['deliveries'][$row['deliveryId']]['clearingDutyCost'] = $row['clearingDutyCost'];
                $deliveryData['deliveries'][$row['deliveryId']]['isDutyCharged'] = $row['isDutyCharged'];
                $deliveryData['deliveries'][$row['deliveryId']]['inventoryCharge'] = $row['inventoryCharge'];
                $deliveryData['deliveries'][$row['deliveryId']]['otherChargeCost'] = $row['otherChargeCost'];
                $deliveryData['deliveries'][$row['deliveryId']]['totalCost'] = $row['totalCost'];
                $deliveryData['deliveries'][$row['deliveryId']]['snapshot'] = $row['snapshot'];
                $deliveryData['deliveries'][$row['deliveryId']]['snapshotRequestedOn'] = $row['snapshotRequestedOn'];
                $deliveryData['deliveries'][$row['deliveryId']]['recount'] = $row['recount'];
                $deliveryData['deliveries'][$row['deliveryId']]['recountRequestedOn'] = $row['recountRequestedOn'];
                $deliveryData['deliveries'][$row['deliveryId']]['reweigh'] = $row['reweigh'];
                $deliveryData['deliveries'][$row['deliveryId']]['reweighRequestedOn'] = $row['reweighRequestedOn'];
                $deliveryData['deliveries'][$row['deliveryId']]['received'] = $row['received'];
                $deliveryData['deliveries'][$row['deliveryId']]['deliveryAddedBy'] = $row['deliveryAddedBy'];
                $deliveryData['deliveries'][$row['deliveryId']]['wrongInventory'] = $row['wrongInventory'];
                $deliveryData['deliveries'][$row['deliveryId']]['shippingMethodId'] = $row['shippingMethodId'];
                $totalDelivery++;
            }
            if(!isset($deliveryData['deliveries'][$row['deliveryId']]['totalQty']))
                $deliveryData['deliveries'][$row['deliveryId']]['totalQty'] = 0;
            $deliveryData['deliveries'][$row['deliveryId']]['totalQty'] = $deliveryData['deliveries'][$row['deliveryId']]['totalQty'] + $row['itemQuantity'];
            $totalPackage = $totalPackage + $row['itemQuantity'];
            $totalValue += $row['itemTotalCost'];
            $itemTypeList .= $row['itemType'] . ',';

            $deliveryData['deliveries'][$row['deliveryId']]['packages'][] = array(
                'id' => $row['packageId'],
                'siteCategoryId' => $row['itemCategoryId'],
                'categoryName' => $row['itemCategoryName'],
                'siteSubCategoryId' => $row['itemSubCategoryId'],
                'subcategoryName' => $row['itemSubCategoryName'],
                'siteProductId' => $row['itemProductId'],
                'productName' => $row['itemProductName'],
                'storeId' => $row['storeId'],
                'storeName' => $row['storeName'],
                'itemName' => $row['itemName'],
                'websiteUrl' => $row['itemWebsiteUrl'],
                'weight' => $row['itemWeight'],
                'options' => $row['itemOptions'],
                'itemDescription' => $row['itemDescription'],
                'itemMake' => $row['itemMake'],
                'itemModel' => $row['itemModel'],
                'note' => $row['itemNote'],
                'snapshotImage' => $row['itemSnapshotImage'],
                'snapshotOpt' => $row['snapshotOpt'],
                'itemType' => $row['itemType'],
                'itemQuantity' => $row['itemQuantity'],
                'itemPrice' => $row['itemPrice'],
                'itemShippingCost' => $row['itemShippingCost'],
                'itemTotalCost' => $row['itemTotalCost'],
                'type' => $row['packageType'],
                'deliveryCompany' => $row['deliveryCompany'],
                'deliveredOn' => $row['deliveredOn'],
                'tracking' => $row['tracking'],
                'trackingLock' => $row['trackingLock'],
                'tracking2' => $row['tracking2'],
                'deliveryNotes' => $row['deliveryNotes'],
            );
        }
        
        $deliveryData['totalPackage'] = $totalPackage;
        $deliveryData['totalWeight'] = $totalWeight;
        $deliveryData['totalDelivery'] = $totalDelivery;
        $deliveryData['totalChargeableWeight'] = $totalChargeableWeight;
        $deliveryData['totalValue'] = $totalValue;
        $deliveryData['itemType'] = rtrim($itemTypeList, ',');

        return $deliveryData;
    }

     /**
     * Method used to fetch warehouse list
     * @return object
     */
    public static function getWareHouseLocationList($id) {
        if (empty($id))
            return false;

        $fillship = new Fillship;
        $fillshipWarehouse = new Fillshipwarehouselocation;
        $warehouselocation = new Warehouselocation;


        $resultSet = Fillship::where("$fillship->table.id", $id)
                ->select(array("$fillshipWarehouse->table.id", "$fillshipWarehouse->table.warehouseRowId", "$fillshipWarehouse->table.warehouseZoneId", "warehouseRow.name AS rowName", "warehouseZone.name AS zoneName"))
                ->join($fillshipWarehouse->table, "$fillshipWarehouse->table.fillshipId", '=', "$fillship->table.id")
                ->join("$warehouselocation->table AS warehouseRow", "warehouseRow.id", '=', "$fillshipWarehouse->table.warehouseRowId")
                ->join("$warehouselocation->table AS warehouseZone", "warehouseZone.id", '=', "$fillshipWarehouse->table.warehouseZoneId")
                ->get();

        return $resultSet;
    }

     /**
     * Method used to fetch shipment data
     * @return object
     */
    public static function getShipmentHistory($id) {
        $tableName = Config::get('constants.dbTable.ACTIVITYLOG');
        $activityLog = DB::table($tableName)->where('log_name', 'fillship_warehouseLocation')->where('properties->attributes->fillshipId', $id)->limit(5)->orderby('id', 'desc')->get();

        $activityLogData = array();
        if (!empty($activityLog)) {
            foreach ($activityLog as $row) {
                $properties = json_decode($row->properties);
                if ($row->causer_type == 'App\Model\UserAdmin')
                    $user = UserAdmin::find($row->causer_id);
                else
                    $user = User::find($row->causer_id);

                $username = $user->email;

                if ($row->description == 'created') {
                    $warehouseRow = Warehouselocation::where('id', $properties->attributes->warehouseRowId)->first();
                    $warehouseZone = Warehouselocation::where('id', $properties->attributes->warehouseZoneId)->first();
                    if (!empty($warehouseRow) && !empty($warehouseZone))
                        $activityLogData[] = "Location  $warehouseRow->name $warehouseZone->name added. Added by $username on $row->created_at.";
                } else if ($row->description == 'deleted') {
                    $warehouseRow = Warehouselocation::where('id', $properties->attributes->warehouseRowId)->first();
                    $warehouseZone = Warehouselocation::where('id', $properties->attributes->warehouseZoneId)->first();

                    if (!empty($warehouseRow) && !empty($warehouseZone))
                        $activityLogData[] = "Location  $warehouseRow->name $warehouseZone->name deleted. Deleted by $username on $row->created_at.";
                } else {
                    $warehouseRowOld = Warehouselocation::where('id', $properties->old->warehouseRowId)->first();
                    $warehouseZoneOld = Warehouselocation::where('id', $properties->old->warehouseZoneId)->first();

                    $warehouseRow = Warehouselocation::where('id', $properties->attributes->warehouseRowId)->first();
                    $warehouseZone = Warehouselocation::where('id', $properties->attributes->warehouseZoneId)->first();
                    if (!empty($warehouseRow) && !empty($warehouseZone) && !empty($warehouseRowOld) && !empty($warehouseZoneOld))
                        $activityLogData[] = "Location  $warehouseRowOld->name $warehouseZoneOld->name updated to $warehouseRow->name $warehouseZone->name. Updated by $username on $row->created_at.";
                }
            }
        }

        return $activityLogData;
    }


    public static function exportallFillshipReportForSpecialOffer($param, $type="", $selected = '') {

        DB::enablequerylog();

        $fillnship = new Fillship;
        $fillnshipItem = new Fillshipitem;
        $user = new User;
        $city = new City;
        $state = new State;
        $country = new Country;
        $warehouse = new Warehouse;

        $fillnshipTable = $fillnship->prefix . $fillnship->table;
        $userTable = $user->prefix . $user->table;

        $where = "$fillnshipTable.deleted= '0'";


        if (!empty($param['searchShipment']['status']))
            $where .= " AND $fillnshipTable.status = '" . $param['searchShipment']['status'] . "'";

        if (!empty($param['searchShipment']['unit']))
            $where .= " AND $userTable.unit = '" . $param['searchShipment']['unit'] . "'";

        if (!empty($param['searchShipment']['shipmentId']))
            $where .= " AND $fillnshipTable.id = '" . $param['searchShipment']['shipmentId'] . "'";

        if (!empty($param['searchShipment']['deliveryCompanyId']))
            $where .= " AND $fillnshipTable.id = '" . $param['searchShipment']['deliveryCompanyId'] . "'";

        if (!empty($param['searchShipment']['receiver']))
            $where .= " AND $fillnshipTable.userId = '" . $param['searchShipment']['receiver'] . "'";


        if (!empty($param['searchShipment']['paymentStatus']))
            $where .= " AND $fillnshipTable.paymentStatus = '" . $param['searchShipment']['paymentStatus'] . "'";

        if (!empty($param['searchShipment']['totalCost']))
            $where .= " AND $fillnshipTable.totalCost >= '" . $param['searchShipment']['totalCost'] . "'";

        if (!empty($param['searchShipment']['toCountry']))
            $where .= " AND $fillnshipTable.toCountry = '" . $param['searchShipment']['toCountry'] . "'";

        if (!empty($param['searchShipment']['toState']))
            $where .= " AND $fillnshipTable.toState = '" . $param['searchShipment']['toState'] . "'";

        if (!empty($param['searchShipment']['toCity']))
            $where .= " AND $fillnshipTable.toCity = '" . $param['searchShipment']['toCity'] . "'";

        if(isset($param['searchOffer'])){
            $where .= " AND couponcodeApplied = '". $param['searchOffer'] . "'";
        }

        if ($param['searchByCreatedOn'] != '' || !empty($param['searchByCreatedOn'])) {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($fillnshipTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $fillnshipTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($fillnshipTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($fillnshipTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        if (!empty($param['searchByshipment'])) {
            $shipmentArray = implode("','", $param['searchByshipment']);
            $where .= " AND $fillnshipTable.id IN ('" . $shipmentArray . "')";
        }

        if ($type == 'export') {

            if (!empty($selected)) {
                $where = "$fillnshipTable.id IN (" . implode(',', $selected) . ")";
            }
           
            $resultSet = $resultSet = Fillship::select(array("$fillnship->table.id as id",
                        DB::raw("CONCAT($userTable.firstName, ' ', $userTable.lastName) AS customer"),
                        "$fillnship->table.totalCost as totalcost",
                        "$fillnship->table.totalDiscount as totalDiscount",
                        "$fillnship->table.status", 
                        "$state->table.name AS destinationStateName",
                        "$city->table.name AS destinationCityName",
                        "$country->table.name AS destinationCountryName", 
                        "$fillnship->table.totalChargeableWeight as totalWeight",
                        "$fillnship->table.paymentStatus as paymentstatus",
                        "$fillnship->table.createdOn"
                    ))
                    ->leftJoin($user->table, "$fillnship->table.userId", '=', "$user->table.id")
                    ->leftJoin($city->table, "$fillnship->table.toCity", '=', "$city->table.id")
                    ->leftJoin($state->table, "$fillnship->table.toState", '=', "$state->table.id")
                    ->leftJoin($country->table, "$fillnship->table.toCountry", '=', "$country->table.id")
                    ->leftJoin($warehouse->table, "$fillnship->table.warehouseId", '=', "$warehouse->table.id")
                    ->leftJoin($country->table . " AS WC", "$warehouse->table.countryId", '=', "WC.id")
                    ->leftJoin($fillnshipItem->table, "$fillnshipItem->table.fillshipId", '=', "$fillnship->table.id")
                    ->whereRaw($where)
                    ->groupBy("$fillnship->table.id")
                    ->orderBy($param['field'], $param['type'])->orderBy("$fillnship->table.id", "desc")
                    ->get();
     
        }
        #dd($resultSet);
        return $resultSet;
    }

    public static function calculateBoxFill($shipmentInfo) {
        
        if(!is_array($shipmentInfo))
         $shipmentInfo = $shipmentInfo->toArray();
        
        $shippingMethoddetails = Shippingmethods::where('shippingid',$shipmentInfo['shippingMethodId'])->first();
        $boxFillPercent = 0;
        if($shippingMethoddetails->sea == 'Y') {
            $boxVolume = $shipmentInfo['boxLength'] * $shipmentInfo['boxHeight'] * $shipmentInfo['boxWidth'];
            $shipmentItems = Fillshipitem::where('fillshipId',$shipmentInfo['id'])->where('deleted','0')->get();
            if(!empty($shipmentItems)) {
                $itemVolume = 0;
                foreach($shipmentItems as $eachItem) {
                    $itemVolume += $eachItem->length * $eachItem->width * $eachItem->height; 
               }
            }
            
            $boxFillPercent = round(($itemVolume /$boxVolume) * 100, 2);
        }
        else {
            $boxFillPercent = round(($shipmentInfo['totalChargeableWeight'] / $shipmentInfo['boxChargeableWeight']) * 100, 2);
        }
        
        return $boxFillPercent;
        
    }

    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Fillship::where('id', $id)
                ->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }


      

}
