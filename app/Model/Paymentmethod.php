<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Paymentmethod extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    protected $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.PAYMENTMETHOD');
        $this->prefix = DB::getTablePrefix();
    }

    /*  public function addressbook() {
      return $this->hasMany('App\Model\Addressbook', 'userId', 'id');
      }
     */

    /**
     * Method used to fetch payment method list
     * @param array $param
     * @param string $type
     * @return object
     */
    public static function getPaymentMethodList($param, $type = '') {
        $payment = new Paymentmethod;
        $country = new Country;
        $paymentGateway = new Paymentgateway;

        $paymentTable = $payment->prefix . $payment->table;

        $where = "$paymentTable.deleted = '0' AND $paymentTable.isussd = '0'";


        $resultSet = Paymentmethod::select(array("$payment->table.*", "$paymentGateway->table.paymentGatewayName"))
                ->leftJoin("$paymentGateway->table", $paymentGateway->table . '.id', '=', "$payment->table.paymentGatewayId")
                ->whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);


        return $resultSet;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = PaymentMethod::where('id', $id)
                ->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    public static function configurationSettings($id) {

        $paymentGateway = new Paymentgateway;
        $gatewayConfiguration = new Paymentgatewaysettings;
        $where = "1";

        $resultSet = Paymentgateway::select(array("$gatewayConfiguration->table.*", "$paymentGateway->table.*"))
                ->leftJoin("$gatewayConfiguration->table", $paymentGateway->table . '.id', '=', "$gatewayConfiguration->table.paymentGatewayId")
                ->where($paymentGateway->table . '.id', '=', $id)
                ->whereRaw($where)
                ->get();

        return $resultSet;
    }

    /**
     * Method used to change Country status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = PaymentMethod::where('id', $id)
                ->update(array('status' => $newStatus));

        return $row;
    }

    /**
     * Method used to fetch payment method list
     * @param string $countryCode
     * @return object
     */
    public static function getallpaymentmethods($countryCode = '') {
        $where = "status = '1' AND deleted = '0'  AND isussd = '0'";

        //  DB::enableQueryLog();

        if ($countryCode != '')
            $where .= " AND FIND_IN_SET($countryCode, countries)";

        $resultSet = Paymentmethod::select(array("id", "paymentMethod", "paymentMethodKey", "paymentDetails", "minallowedvalue", "maxallowedvalue"))
                        ->whereRaw($where)
                        ->orderBy("orderby", "ASC")
                        ->get()->toArray();

        $data = array();

        if (!empty($resultSet)) {
            foreach ($resultSet as $count => $row) {
                $paymentMethodId = '-1';
                foreach ($row as $key => $value) {
                    if($key == 'id')
                        $paymentMethodId = $value;
                    $data[$count][$key] = $value;

                    $resultSet = Paymenttaxsettings::select(array("taxValue", "taxtype"))
                            ->where('countryId', $countryCode)
                            ->where('paymentMethodId',$paymentMethodId)
                            ->where('deleted', '0')
                            ->first();
                    $data[$count]['surcharge'] = '0.00';
                    $data[$count]['surchargeType'] = '0';
                    if(!empty($resultSet))
                    {
                        $data[$count]['surcharge'] = $resultSet->taxValue;
                        $data[$count]['surchargeType'] = $resultSet->taxtype;
                    }
                }
            }
        }

        return $data;
    }

}
