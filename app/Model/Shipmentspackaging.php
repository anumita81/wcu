<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Config;

class Shipmentspackaging extends Model
{
    public $table;
    public $timestamps = false;

    public function packageaddedby() {
        return $this->hasOne('App\Model\UserAdmin', 'id', 'createdBy');
    }
    
    public function __construct() {

        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.SHIPMENTPACKAGING');
    }
}