<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Currencyconversion extends Model {

    public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.CURRENCYCONVERSION');
    }

    /**
     * Method used to fetch currency list
     * @param array $param
     * @return object
     */
    public static function getCurrencyList($param) {   
        $resultSet = Currencyconversion::where('deleted', '0')
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);
        
        return $resultSet;
    }

    /**
     * Method used to change currency status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Currencyconversion::where('id', $id)
                ->update(array('status' => $newStatus, 'updatedBy' => $createrModifierId, 'updatedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Currency::where('id', $id)
                ->update(array('deleted' => 1, 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }


    /**
     * This function used to get exchange rate between default currency and selected currency
     * @param type $from_currency
     * @param type $to_currency
     * @return type
     */
    public static function currencyExchangeRate($from_currency, $to_currency) {
        $combined = $from_currency . "_" . $to_currency;

        //API Key generated for free user - Changed in 19-02-2019

        //$apikey = "b96697ff702b0b239207";

        $apikey = "4d5cf943-36c4-4a02-ae69-3627b5b65253";
        //$curlUrl = 'http://free.currencyconverterapi.com/api/v3/convert?q=' . $combined .'&compact=ultra&apiKey=' .$apikey;
        $curlUrl = 'https://api.currconv.com/api/v7/convert?q='.$combined.'&compact=ultra&apiKey='.$apikey;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL =>  $curlUrl,
        ));

        $resp = curl_exec($curl);
        curl_close($curl);
        $decoded = json_decode($resp);

        /* IF EXCHANGE RATE NOT FOUND */
        if (isset($decoded->query->count) &&  $decoded->query->count== 0) {
            $exchangeRate = 1;
        }else if(!empty($decoded)) {
            $exchangeRate = $decoded->$combined;
        }else
        {
            $exchangeRate = 1;
        }
        // } else if(!empty($decoded->results)) {
        //     $exchangeRate = $decoded->results->$combined->val;
        // }
        
        //echo $exchangeRate; die;
        return $exchangeRate;
    }
    
    /**
     * This function used to get costs after conversion to selected currency
     * @param type $amount
     * @param type $exchangeRate
     * @return type
     */
    public static function currencyConverter($amount,$exchangeRate) {

        return round(($amount * $exchangeRate),2);
    }

}
