<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Locationtype extends Model {

    public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.LOCATIONTYPE');
    }

    /**
     * Method used to fetch location type list
     * @param array $param
     * @return object
     */
    public static function getTypeList($param) {
        $where = '1';

        /* Print query ->toSql() dd($resultSet); */
        $resultSet = Locationtype::whereRaw($where)
                ->select(array('id', 'name', 'status'))
                ->where('deleted','0')
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to change location type status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Locationtype::where('id', $id)
                ->update(array('status' => $newStatus));

        return $row;
    }
    
    public static function deleteRecord($id) {
        if (empty($id))
            return false;

        $row = false;

        $row = Locationtype::where('id', $id)
                ->update(array('deleted' => '1'));

        return $row;
    }

}
