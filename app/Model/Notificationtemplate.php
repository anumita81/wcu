<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Notificationtemplate extends Model {

    public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.NOTIFICATIONTEMPLATE');
    }

    /**
     * Method used to fetch email template list
     * @param array $param
     * @return object
     */
    public static function getEmailTemplateList($param) {
        $where = "deleted='0' AND status='1'";

        if (!empty($param['searchByType']))
            $where .= " AND templateType = '" . $param['searchByType'] . "'";


        $resultSet = Notificationtemplate::whereRaw($where)
                ->select(array('id', 'templateKey', 'templateBody'))
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

}
