<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Contactus extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.CONTACTUS');
        $this->prefix = DB::getTablePrefix();
    }

    public static function getData($param) {
        #DB::enableQueryLog();
        $where = '1 ';
        if($param['searchData']!='')
        {
            $where .= 'AND status="'.$param['searchData'].'"';
        }

        /*if($param['searchByReason']!="" && $param['searchByReason']!="0"){
            $where .= 'AND reason_id="'.$param['searchByReason'].'"';
        }*/

        if($param['searchByType']!=""){
            if($param['searchByType'] == 11){
                $where .= 'AND reason_id="'.$param['searchByType'].'"';
            } else {
                if($param['searchByReason']==""){
                    $where .= 'AND reason_id NOT IN (11)';
                } else {
                    $where .= 'AND reason_id="'.$param['searchByReason'].'"';
                }
            }
        }


        $resultSet = Contactus::whereRaw($where)
                ->where("deleted", '0')
                ->orderBy($param['sortField'], $param['sortOrder'])
                ->paginate($param['searchDisplay']);
#dd(DB::getQueryLog());
        return $resultSet;

    }

    public function getAllStatus() {

        return array('1' => 'Submitted', '2' => 'Viewed', '3' => 'Replied', '4' => 'Resolved');
    }
    
    public static function notificationCount($reasonKey,$lastLoginTime) {
        
        $contactus = new Contactus;
        $contactusReason = new Contactreason;
        
        $data = Contactus::select("$contactus->table.id")
                ->join($contactusReason->table,"$contactusReason->table.id","=","$contactus->table.reason_id")
                ->where("$contactusReason->table.keyname","$reasonKey")
                ->where("$contactus->table.postedOn",'>=',$lastLoginTime)
                ->get()->count();
        
        return $data;
    }
}
