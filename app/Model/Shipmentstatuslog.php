<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Shipmentstatuslog extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.SHIPMENTSTATUSLOG');
    }
    public $timestamps = false;
    
    public static function allStatus() {
        return array('in_warehouse'=>'In Warehouse','in_transit'=>'In Transit','custom_clearing'=>'Custom Clearing','destination_warehouse'=>'Destination Warehouse','out_for_delivery'=>'Out For Delivery','delivered'=>'Delivered');
    }

    public static function getDeliverywiseStatusLog($shipmentId) {
        $result = array();
        $statusLogData = Shipmentstatuslog::where('shipmentId',$shipmentId)->get();
        if($statusLogData->count()>0) {
            foreach($statusLogData as $eachData) {
                $result[$eachData->deliveryId][$eachData->status] = $eachData->updatedOn;
            }
        }
        
        return $result;
    }
}
