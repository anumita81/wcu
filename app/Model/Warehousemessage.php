<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Warehousemessage extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.WAREHOUSEMESSAGE');
    }

    /**
     * Method used to fetch Country list
     * @param array $param
     * @return object
     */
    public static function getWarehousemessageList($param) {
        $where = 'status = "1"';

        $resultSet = Warehousemessage::whereRaw($where)
                ->select('*')
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    
    
}
