<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
use Auth;
use Carbon\Carbon;

class Usersubscription extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.USERSUBSCRIPTION');
        $this->prefix = DB::getTablePrefix();
    }

    public static function getcustomerDetailsBySubscription($param)
    {
    	$userSubscription = new Usersubscription;
    	$user = new User;

        $userSubscriptionTable = $userSubscription->prefix . $userSubscription->table;
        $userTable = $userSubscription->prefix . $user->table;


        $where = " $userTable.deleted = '0' AND $userTable.status='1'";

       // echo "<pre>"; print_r($param); die;

        if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($userSubscriptionTable.subscribedOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $userSubscriptionTable.subscribedOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($userSubscriptionTable.subscribedOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($userSubscriptionTable.subscribedOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
        }

       //DB:: enableQueryLog();

        $resultSet = Usersubscription::select(array("$user->table.id as userId", "$user->table.firstName", "$user->table.lastName", "$user->table.email", "$userSubscription->table.*", "$user->table.unit"))
                ->leftJoin($user->table, "$userSubscription->table.userId", '=', "$user->table.id")
                ->whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

       //dd(DB::getQuerylog());

        return $resultSet;

    }


}
