<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
use Auth;

class Faq extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.FAQ');
        $this->prefix = DB::getTablePrefix();
    }

    

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Faq::where('id', $id)->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }
    
    /**
     * Method used to change Admin User status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return object
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Faq::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => Auth::user()->id, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

}
