<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Location extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.LOCATION');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to fetch location list
     * @param array $param
     * @return object
     */
    public static function getLocationList($param) {
        $location = new Location;
        $city = new City;
        $country = new Country;
        $state = new State;

        $where = "1";

        $locationTable = $location->prefix . $location->table;

        if (!empty($param['searchByCountry']))
            $where .= "  AND $locationTable.countryId ='" . $param['searchByCountry'] . "'";

        if (!empty($param['searchByState']))
            $where .= "  AND $locationTable.stateId ='" . $param['searchByState'] . "'";

        if (!empty($param['searchByCity']))
            $where .= "  AND $locationTable.cityId ='" . $param['searchByCity'] . "'";

        $resultSet = Location::select(array("$location->table.id", "$location->table.businessName", "$location->table.email", "$location->table.displayOrder", "$location->table.status", "$city->table.name AS cityName", "$country->table.name AS countryName", "$state->table.name AS stateName",))
                ->leftJoin($country->table, "$location->table.countryId", '=', "$country->table.id")
                ->leftJoin($state->table, "$location->table.stateId", '=', "$state->table.id")
                ->leftJoin($city->table, "$location->table.cityId", '=', "$city->table.id")
                ->where($location->table . '.deleted', '0')
                ->whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to change location status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Location::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Location::where('id', $id)
                ->update(array('deleted' => 1, 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

}
