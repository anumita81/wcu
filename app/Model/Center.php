<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Center extends Model {

    public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.CENTER');
    }

    /**
     * Method used to fetch county list
     * @param array $param
     * @return object
     */
    public static function getCenterList($param) {
        $where = 'deleted = "0"';

                $resultSet = Center::whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to change county status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Center::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Center::where('id', $id)
                ->update(array('deleted' => 1, 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }


}