<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Taxrate extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.TAXRATE');
        $this->prefix = DB::getTablePrefix();
    }


    public static function getTaxRateList($param, $taxId){
        $where = '1';
        //DB::enableQueryLog();
        $resultSet = Taxrate::whereRaw($where)
        ->leftJoin(Config::get('constants.dbTable.ZONE'), Config::get('constants.dbTable.ZONE').'.id', '=', Config::get('constants.dbTable.TAXRATE').'.zoneId')
        ->leftJoin(Config::get('constants.dbTable.TAXSYSTEM'), Config::get('constants.dbTable.TAXSYSTEM').'.id', '=', Config::get('constants.dbTable.TAXRATE').'.taxId')
        ->addSelect(array(Config::get('constants.dbTable.TAXRATE').'.id', Config::get('constants.dbTable.TAXRATE').'.taxId', Config::get('constants.dbTable.TAXRATE').'.zoneId', 
            Config::get('constants.dbTable.TAXRATE').'.rateValue', Config::get('constants.dbTable.TAXRATE').'.rateType', Config::get('constants.dbTable.TAXRATE').'.deleted',
            Config::get('constants.dbTable.ZONE').'.name', Config::get('constants.dbTable.TAXSYSTEM').'.formula'))
        ->where('taxId', $taxId)->where(Config::get('constants.dbTable.TAXRATE').'.deleted', '0')->orderBy($param['field'], $param['type'])->paginate($param['searchDisplay']);
        //$resultSet = DB::getQueryLog();
        //dd($resultSet);
        return $resultSet;
    }


    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Taxrate::where('id', $id)->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }
}
