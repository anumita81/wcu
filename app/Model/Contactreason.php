<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Contactreason extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.CONTACTREASON');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to change record status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return object
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Contactreason::where('id', $id)
                ->update(array('status' => $newStatus));

        return $row;
    }
}
