<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class City extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.CITY');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to fetch city list
     * @param array $param
     * @return object
     */
    public static function getCityList($param) {
       // DB::enableQueryLog();

        $city = new City;
        $country = new Country;
        $state = new State;

        $where = "1";

        $cityTable = $city->prefix . $city->table;

        if (!empty($param['searchByCountry']))
            $where .= "  AND $cityTable.countryCode ='" . $param['searchByCountry'] . "'";

        if (!empty($param['searchByState']))
            $where .= "  AND $cityTable.stateCode ='" . $param['searchByState'] . "'";

        $resultSet = country::select(array("$city->table.id", "$city->table.name AS cityName", "$city->table.status", 
            "$country->table.name AS countryName", "$state->table.name AS stateName", "$country->table.id AS countryId",
            "$state->table.id AS stateId")
        )->leftJoin($state->table, "$state->table.countryCode",'=',"$country->table.code")
        ->leftjoin("$city->table",function($join) use ($city,$state,$country){
            $join->on("$city->table.countryCode",'=',"$country->table.code")
                ->on("$city->table.stateCode",'=',"$state->table.code");
        })
        ->where($city->table.'.deleted', '0')
        ->where($city->table.'.status', '1')
        ->where($state->table.'.deleted', '0')
        ->where($state->table.'.status', '1')
        ->where($country->table.'.status','1')
        ->whereRaw($where)
        ->orderBy($param['field'], $param['type'])
        ->paginate($param['searchDisplay']);
        
       // dd(DB::getQueryLog()); 




        return $resultSet;
    }

    /**
     * Method used to change city status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = City::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = City::where('id', $id)
                ->update(array('deleted' => 1, 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));
        $zoneCityExist = Zonecity::where('cityId',$id)->get()->toArray();
        if(count($zoneCityExist)>0)
        {
            Zonecity::where('cityId',$id)->delete();
        }
        

        return $row;
    }


    /**
     * Method used to fetch all city list with country id
     * @param array $param
     * @return object
     */
    public static function getAllCityList() {
        $city = new City;
        $country = new Country;
        $state = new State;

        $cityTable = $city->prefix.$city->table;
        
        $resultSet = country::select(array("$city->table.id", "$city->table.name AS cityName", "$city->table.status", 
            "$country->table.name AS countryName", "$state->table.name AS stateName", "$country->table.id AS countryId",
            "$state->table.id AS stateId")
        )->leftJoin($state->table, "$state->table.countryCode",'=',"$country->table.code")
        ->leftjoin("$city->table",function($join) use ($city,$state,$country){
            $join->on("$city->table.countryCode",'=',"$country->table.code")
                ->on("$city->table.stateCode",'=',"$state->table.code");
        })
        ->where($city->table.'.deleted', '0')
        ->where($city->table.'.status', '1')
        ->where($state->table.'.deleted', '0')
        ->where($state->table.'.status', '1')
        ->where($country->table.'.status','1')
        ->get();
        
        return $resultSet;
    }

}
