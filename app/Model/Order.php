<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Order extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {

        parent::__construct();
        $this->prefix = DB::getTablePrefix();
        $this->table = Config::get('constants.dbTable.ORDER');
    }

    public static function allOrderStatus() {
        return array('Not finished', 'Submitted', 'Processed', 'Declined', 'Failed', 'Complete');
    }

    public static function allOrderStatusClasses() {
        return array('btn-default', 'btn-info', 'btn-primary', 'btn-warning', 'btn-danger', 'btn-success');
    }
    
    public static function generateOrderNumber($orderId,$userId) {
        
        $orderStr = 'OD-'.$orderId.$userId.substr(rand(11111111,99999999),0,5);
        $orderNumber = Order::where('orderNumber',$orderStr)->get();
        if($orderNumber->count() > 0)
            Order::generateOrderNumber ($orderId, $userId);
        else 
            return $orderStr;
    }

    public static function orderList($param = array()) {

        $shipment = new Shipment;
        $shipmentDelivery = new Shipmentdelivery;
        $shipmentWarehouse = new Shipmentwarehouselocation;
        $warehouselocation = new Warehouselocation;

        $shipmentTable = $shipment->prefix.$shipment->table;
        $shipmentDeliveryTable = $shipmentDelivery->prefix.$shipmentDelivery->table;
        $shipmentWarehouseTable = $shipment->prefix . $shipmentWarehouse->table;
        $warehouselocationTable = $warehouselocation->prefix . $warehouselocation->table;


        $viewOrder = new Order;
        $viewOrderTable = $viewOrder->prefix . $viewOrder->table;

        $viewUser = new User;
        $viewUserTable = $viewUser->prefix . $viewUser->table;

        $where = "1 = 1";

        if (!empty($param['searchOrder']['idFrom']))
            $where .= " AND $shipmentTable.id >= " . $param['searchOrder']['idFrom'];

        if (!empty($param['searchOrder']['idTo']))
            $where .= " AND $shipmentTable.id <= " . $param['searchOrder']['idTo'];

        if (!empty($param['searchOrder']['orderIdFrom']))
            $where .= " AND $viewOrderTable.orderNumber = '" . $param['searchOrder']['orderIdFrom']."'";

        if (!empty($param['searchOrder']['orderIdTo']))
            $where .= " AND $viewOrderTable.orderNumber <= " . $param['searchOrder']['orderIdTo'];

        if (!empty($param['searchOrder']['unitFrom']))
            $where .= " AND $viewUserTable.unit >= '" . $param['searchOrder']['unitFrom'] . "'";

        if (!empty($param['searchOrder']['unitTo']))
            $where .= " AND $viewUserTable.unit = '" . $param['searchOrder']['unitTo'] . "'";
        
        if (!empty($param['searchOrder']['warehouseId']))
            $where .= " AND $shipmentTable.warehouseId = '" . $param['searchOrder']['warehouseId'] . "'";
        
        if (isset($param['searchOrder']['rows']) && !empty($param['searchOrder']['rows'])) {
            $where .= " AND $shipmentWarehouseTable.warehouseRowId = '" . $param['searchOrder']['rows'] . "'";
        }

        if (isset($param['searchOrder']['zones']) && !empty($param['searchOrder']['zones'])) {
            $where .= " AND $shipmentWarehouseTable.warehouseZoneId = '" . $param['searchOrder']['zones'] . "'";
        }
        
        if (!empty($param['searchOrder']['shipmentType']))
            $where .= " AND $shipmentTable.partucilarShipmentType = '" . $param['searchOrder']['shipmentType'] . "'";

        if (!empty($param['searchOrder']['totalFrom']))
            $where .= " AND $viewOrderTable.totalCost >= '" . $param['searchOrder']['totalFrom'] . "'";

        if (!empty($param['searchOrder']['totalTo']))
            $where .= " AND $viewOrderTable.totalCost <= '" . $param['searchOrder']['totalTo'] . "'";

        if (!empty($param['searchOrder']['user']))
            $where .= " AND ($viewUserTable.firstName LIKE '%" . $param['searchOrder']['user'] . "%' OR $viewUserTable.lastName LIKE '%" . $param['searchOrder']['user'] . "%' OR $viewUserTable.email LIKE '%" . $param['searchOrder']['user'] . "%')";

        if (!empty($param['searchOrder']['shippingMethod']))
            $where .= " AND $shipmentDeliveryTable.shippingMethodId = '".$param['searchOrder']['shippingMethod']."'";

        if (!empty($param['searchOrder']['itemType']))
            $where .= " AND $viewShipmentDetailsTable.itemType = '" . $param['searchOrder']['itemType'] . "'";
        
        if(isset($param['searchOrder']['packed']) && !empty($param['searchOrder']['packed'])) {
                $where .= " AND $shipmentTable.packed = '".$param['searchOrder']['packed']."'";
        }


        if ($param['searchByCreatedOn'] != '') {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($viewOrderTable.createdDate) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $viewOrderTable.createdDate BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($viewOrderTable.createdDate) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($viewOrderTable.createdDate) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        if($param['field'] == 'firstName'){
            $orderbyText = "$viewUser->table";
        } else {
            $orderbyText = "$viewOrder->table";
        }
        if(empty($param['searchOrder']['shippingMethod']) && empty($param['searchOrder']['rows']) && empty($param['searchOrder']['zones'])) {
            
            $resultSet = Order::select("$viewOrder->table.userId", "$viewOrder->table.orderNumber", "$viewOrder->table.shipmentId","$viewOrder->table.status", "$viewOrder->table.type", "$viewUser->table.firstName", "$viewUser->table.lastName",
                "$viewOrder->table.totalCost", "$viewOrder->table.createdDate", "$viewOrder->table.id", "$viewOrder->table.shipmentId","$shipment->table.packed")
            ->join("$shipment->table", "$viewOrder->table.shipmentId", "=", "$shipment->table.id") 
            ->leftJoin("$viewUser->table", "$viewOrder->table.userId", "=", "$viewUser->table.id")    
            //->where($viewOrder->table.".status", '5')
            ->whereRaw($where)
            ->orderBy($orderbyText.".".$param['field'], $param['type'])
            ->paginate($param['searchDisplay']);
            return $resultSet;
        } else if(!empty($param['searchOrder']['shippingMethod']) && empty($param['searchOrder']['rows']) && empty($param['searchOrder']['zones']) ) {
            
            $resultSet = Order::select("$viewOrder->table.userId", "$viewOrder->table.orderNumber", "$viewOrder->table.shipmentId","$viewOrder->table.status", "$viewOrder->table.type", "$viewUser->table.firstName", "$viewUser->table.lastName",
                "$viewOrder->table.totalCost", "$viewOrder->table.createdDate", "$viewOrder->table.id", "$viewOrder->table.shipmentId","$shipment->table.packed")
            ->join("$shipment->table", "$viewOrder->table.shipmentId", "=", "$shipment->table.id") 
            ->leftJoin("$viewUser->table", "$viewOrder->table.userId", "=", "$viewUser->table.id") 
            ->leftJoin("$shipmentDelivery->table","$shipmentDelivery->table.shipmentId","=","$shipment->table.id")
            //->where($viewOrder->table.".status", '5')
            ->whereRaw($where)
            ->groupBy("$shipment->table.id")
            ->orderBy($orderbyText.".".$param['field'], $param['type'])
            ->paginate($param['searchDisplay']);
            return $resultSet;
        } else {
            $resultSet = Order::select("$viewOrder->table.userId", "$viewOrder->table.orderNumber", "$viewOrder->table.shipmentId","$viewOrder->table.status", "$viewOrder->table.type", "$viewUser->table.firstName", "$viewUser->table.lastName",
                "$viewOrder->table.totalCost", "$viewOrder->table.createdDate", "$viewOrder->table.id", "$viewOrder->table.shipmentId","$shipment->table.packed")
            ->join("$shipment->table", "$viewOrder->table.shipmentId", "=", "$shipment->table.id") 
            ->leftJoin("$viewUser->table", "$viewOrder->table.userId", "=", "$viewUser->table.id") 
            ->leftJoin("$shipmentDelivery->table","$shipmentDelivery->table.shipmentId","=","$shipment->table.id")
            ->leftJoin($shipmentWarehouse->table, "$shipmentWarehouse->table.shipmentId", '=', "$shipment->table.id")
            ->leftJoin("$warehouselocation->table AS warehouseRow", "warehouseRow.id", '=', "$shipmentWarehouse->table.warehouseRowId")
            ->leftJoin("$warehouselocation->table AS warehouseZone", "warehouseZone.id", '=', "$shipmentWarehouse->table.warehouseZoneId")        
            ->whereRaw($where)
            ->groupBy("$shipment->table.id")
            ->orderBy($orderbyText.".".$param['field'], $param['type'])
            ->paginate($param['searchDisplay']);
            return $resultSet;
        }
    }

    public static function orderwiseChargeableWeight($orderData = array()) {

        $orderwiseData = array();

        foreach ($orderData as $eachOrderData) {
            $deliveryData = ViewShipmentDetails::where("shipmentId", $eachOrderData->id)->where('deleted', '0')->where('packageDeleted', '0')->where('packageType', 'I')->get()->toarray();
            $delivery = Shipment::getDeliveryDetails($deliveryData);
            $orderwiseData[$eachOrderData->orderId] = $delivery['totalChargeableWeight'];
        }

        return $orderwiseData;
    }

    /**
     * This function used <return> data Object used for excel export
     * @param type $param
     * @param type $selectedUser
     * @param type $selectedFields
     * @param type $shipmentType
     * @return type
     */
    public function exportData($param = array(), $selectedUser = array(), $selectedFields = array(), $shipmentType = 'S') {

        $where = '';
        $select = array();
        $shipment = new Shipment;
        $shipmentDelivery = new Shipmentdelivery;
        $shipmentWarehouse = new Shipmentwarehouselocation;
        $warehouselocation = new Warehouselocation;
        $warehouse = new Warehouse;
        $country = new Country;
        $city = new City;


        $shipmentTable = $shipment->prefix . $shipment->table;
        $shipmentDeliveryTable = $shipmentDelivery->prefix . $shipmentDelivery->table;
        $shipmentWarehouseTable = $shipment->prefix . $shipmentWarehouse->table;
        $warehouselocationTable = $warehouselocation->prefix . $warehouselocation->table;
        $warehouseTable = $warehouse->prefix.$warehouse->table;
        $countryTable = $country->prefix.$country->table;
        $cityTable = $city->prefix.$city->table;

        $viewOrder = new Order;
        $viewOrderTable = $viewOrder->prefix . $viewOrder->table;

        $viewUser = new User;
        $viewUserTable = $viewUser->prefix . $viewUser->table;



        $where = "1 = 1";

        if (!empty($param)) {

            if (!empty($param['searchOrder']['idFrom']))
                $where .= " AND $shipmentTable.id >= " . $param['searchOrder']['idFrom'];

            if (!empty($param['searchOrder']['idTo']))
                $where .= " AND $shipmentTable.id <= " . $param['searchOrder']['idTo'];

            if (!empty($param['searchOrder']['orderIdFrom']))
                $where .= " AND $viewOrderTable.id >= " . $param['searchOrder']['orderIdFrom'];

            if (!empty($param['searchOrder']['orderIdTo']))
                $where .= " AND $viewOrderTable.id <= " . $param['searchOrder']['orderIdTo'];

            if (!empty($param['searchOrder']['unitFrom']))
                $where .= " AND $viewUserTable.unit >= '" . $param['searchOrder']['unitFrom'] . "'";

            if (!empty($param['searchOrder']['unitTo']))
                $where .= " AND $viewUserTable.unit = '" . $param['searchOrder']['unitTo'] . "'";


            if (!empty($param['searchOrder']['totalFrom']))
                $where .= " AND $viewOrderTable.totalCost >= '" . $param['searchOrder']['totalFrom'] . "'";

            if (!empty($param['searchOrder']['totalTo']))
                $where .= " AND $viewOrderTable.totalCost <= '" . $param['searchOrder']['totalTo'] . "'";

            if (!empty($param['searchOrder']['user']))
                $where .= " AND ($viewUserTable.firstName LIKE '%" . $param['searchOrder']['user'] . "%' OR $viewUserTable.lastName LIKE '%" . $param['searchOrder']['user'] . "%' OR $viewUserTable.email LIKE '%" . $param['searchOrder']['user'] . "%')";


            if (!empty($param['searchOrder']['shippingMethod']))
                $where .= " AND $shipmentDeliveryTable.shippingMethodId = '" . $param['searchOrder']['shippingMethod'] . "'";


            if (!empty($param['searchOrder']['itemType']))
                $where .= " AND $viewShipmentDetailsTable.itemType = '" . $param['searchOrder']['itemType'] . "'";

            if (!empty($param['searchOrder']['warehouseId']))
                $where .= " AND $shipmentTable.warehouseId = '" . $param['searchOrder']['warehouseId'] . "'";

            if (isset($param['searchOrder']['rows']) && !empty($param['searchOrder']['rows'])) {
                $where .= " AND $shipmentWarehouseTable.warehouseRowId = '" . $param['searchOrder']['rows'] . "'";
            }

            if (isset($param['searchOrder']['zones']) && !empty($param['searchOrder']['zones'])) {
                $where .= " AND $shipmentWarehouseTable.warehouseZoneId = '" . $param['searchOrder']['zones'] . "'";
            }

            if (!empty($param['searchOrder']['shipmentType']))
                $where .= " AND $shipmentTable.partucilarShipmentType = '" . $param['searchOrder']['shipmentType'] . "'";


            if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($viewOrderTable.createdDate) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $viewOrderTable.createdDate BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($viewOrderTable.createdDate) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($viewOrderTable.createdDate) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }
        }

        if (!empty($selectedUser)) {
            $where = "$viewOrderTable.id IN (" . implode(',', $selectedUser) . ")";
        }

        if (!empty($selectedFields)) {
            foreach ($selectedFields as $eachSearchfield) {

                if ($eachSearchfield == 'orderid')
                    $select[] = DB::raw("DISTINCT $viewOrderTable.orderNumber as 'Order Id'");
                if ($eachSearchfield == 'shipmentid')
                    $select[] = DB::raw("$shipmentTable.id as 'Shipment Id'");
                if ($eachSearchfield == 'unit')
                    $select[] = DB::raw("$viewUserTable.unit as Unit");
                if ($eachSearchfield == 'name')
                    $select[] = DB::raw("CONCAT($viewUserTable.firstName, ' ', $viewUserTable.lastName) AS 'Customer Name'");
                if ($eachSearchfield == 'email')
                    $select[] = DB::raw("$viewUserTable.email as 'Email Address'");
                if ($eachSearchfield == 'phone')
                    $select[] = DB::raw("$viewUserTable.contactNumber as 'Contact Number'");
                if ($eachSearchfield == 'status') {
                    $select[] = DB::raw("case shipmentStatus
                                    when '0' then 'Shipment Submiited'
                                    when '1' then 'In Warehouse'
                                    when '2' then 'In Transit'
                                    when '3' then 'Customs clearing'
                                    when '4' then 'In destination warehouse'
                                    when '5' then 'Out for delivery'
                                    when '6' then 'Delivered'
                                end as 'Shipment Status'");
                }
                if ($eachSearchfield == 'type')
                    $select[] = DB::raw("shipmentType as 'Shipment Type'");
                if ($eachSearchfield == 'orderstatus')
                    $select[] = DB::raw("case orderStatus
                                    when '0' then 'Not Paid'
                                    when '1' then 'Submitted'
                                    when '2' then 'Processed'
                                    when '3' then 'Declined'
                                    when '4' then 'Failed'
                                    when '5' then 'Complete'
                                end as 'Order Status'");
                if ($eachSearchfield == 'total')
                    $select[] = DB::raw("$shipmentTable.totalCost as Total");
                if ($eachSearchfield == 'chargeableWeight')
                    $select[] = DB::raw("(SELECT sum(DISTINCT deliveryChargeableWeight) FROM $shipmentDelivery WHERE shipmentId = $shipmentTable.id ) as 'Chargeable Weight'");
                if ($eachSearchfield == 'packed')
                // $select[] = DB::raw("IF((allowedLabels='Y' and allowedPackaging='Y'),'Packed','Not Packed') as packed");$shipmentTable.packed
                    $select[] = DB::raw("case packed
                                    when 'not_started' then 'Not Started'
                                    when 'started' then 'Started'
                                    when 'packing_complete' then 'Packing Complete'
                                end as 'Packaging Status'");
                if ($eachSearchfield == 'orderDate')
                    $select[] = DB::raw("createdDate as Date");
                if ($eachSearchfield == 'countryName')
                    $select[] = DB::raw("toCountryName as 'Destination Country'");
                if ($eachSearchfield == 'stateName')
                    $select[] = DB::raw("toStateName as 'Destination State'");
                if ($eachSearchfield == 'cityName')
                    $select[] = DB::raw("toCityName as 'Destination City'");
                if ($eachSearchfield == 'shipmentLocation') {
                    $select[] = DB::raw("stmd_warehouseRow.name as 'Warehouse Row'");
                    $select[] = DB::raw("stmd_warehouseZone.name as 'Warehouse Zone'");
                }
                if ($eachSearchfield == 'warehouseLocation')
                    $select[] = DB::raw("concat($countryTable.name,' (',$cityTable.name,')') as 'Warehouse Location'");
            }
            
            //print_r($select);exit;
        }
        //Order::select("$viewOrder->table.userId", "$viewOrder->table.orderNumber", "$viewOrder->table.shipmentId", "$viewOrder->table.status", "$viewOrder->table.type", "$viewUser->table.firstName", "$viewUser->table.lastName", "$viewOrder->table.totalCost", "$viewOrder->table.createdDate", "$viewOrder->table.id", "$viewOrder->table.shipmentId", "$shipment->table.packed", DB::raw("stmd_warehouseRow.name as 'Warehouse Row'"), DB::raw("stmd_warehouseZone.name as 'Warehouse Zone'"), DB::raw("concat($countryTable.name,' (',$cityTable.name,')') as 'Warehouse Location'"))
        $resultSet = Order::select($select)
                ->join("$shipment->table", "$viewOrder->table.shipmentId", "=", "$shipment->table.id")
                ->leftJoin("$viewUser->table", "$viewOrder->table.userId", "=", "$viewUser->table.id")
                ->leftJoin("$shipmentDelivery->table", "$shipmentDelivery->table.shipmentId", "=", "$shipment->table.id")
                ->leftJoin($shipmentWarehouse->table, "$shipmentWarehouse->table.shipmentId", '=', "$shipment->table.id")
                ->leftJoin("$warehouselocation->table AS warehouseRow", "warehouseRow.id", '=', "$shipmentWarehouse->table.warehouseRowId")
                ->leftJoin("$warehouselocation->table AS warehouseZone", "warehouseZone.id", '=', "$shipmentWarehouse->table.warehouseZoneId")
                ->leftJoin("$warehouse->table","$warehouse->table.id","=","$shipment->table.warehouseId")
                ->leftJoin("$country->table","$country->table.id","=","$warehouse->table.countryId")
                ->leftJoin("$city->table","$city->table.id","=","$warehouse->table.cityId")
                ->whereRaw($where)
                ->groupBy("$shipment->table.id")
                ->get();
        return $resultSet;
    }

    /**
     * This function used for get address book for default shipment
     * @param type $userId
     * @return object
     */
    public static function getaddressbookShip($userId){

        $abook = new Addressbook;
        $abookTable = $abook->prefix . $abook->table;

        $country = new Country;
        $countryTable = $country->prefix . $country->table;

        $state = new State;
        $stateTable = $state->prefix . $state->table;

        $city = new City;
        $cityTable = $city->prefix . $city->table;

        $resultSet = Addressbook::select("$abook->table.isDefaultShipping", "$abook->table.isDefaultBilling", 
            "$abook->table.title", "$abook->table.firstName", "$abook->table.lastName", "$abook->table.phone", 
            "$abook->table.zipcode", "$abook->table.address", "$abook->table.alternateAddress", "$abook->table.email", 
            "$abook->table.alternatePhone", "$country->table.name as countryName", "$city->table.name as cityName", "$state->table.name as stateName")
        ->leftJoin("$country->table", "$abook->table.countryId", "=", "$country->table.id")
        ->leftJoin("$city->table", "$abook->table.cityId", "=", "$city->table.id")
        ->leftJoin("$state->table", "$abook->table.stateId", "=", "$state->table.id")
        ->where("$abook->table.userId", $userId)->where("$abook->table.isDefaultShipping", '1')
        ->get();
        return $resultSet;
    }

    /**
     * This function used for get address book for default shipment
     * @param type $userId
     * @return object
     */
    public static function getaddressbookBill($userId){

        $abook = new Addressbook;
        $abookTable = $abook->prefix . $abook->table;

        $country = new Country;
        $countryTable = $country->prefix . $country->table;

        $state = new State;
        $stateTable = $state->prefix . $state->table;

        $city = new City;
        $cityTable = $city->prefix . $city->table;

        $resultSet = Addressbook::select("$abook->table.userId", "$abook->table.isDefaultShipping", "$abook->table.isDefaultBilling", 
            "$abook->table.title", "$abook->table.firstName", "$abook->table.lastName", "$abook->table.phone", 
            "$abook->table.zipcode", "$abook->table.address", "$abook->table.alternateAddress", "$abook->table.email", 
            "$abook->table.alternatePhone", "$country->table.name as countryName", "$city->table.name as cityName", "$state->table.name as stateName")
        ->leftJoin("$country->table", "$abook->table.countryId", "=", "$country->table.id")
        ->leftJoin("$city->table", "$abook->table.cityId", "=", "$city->table.id")
        ->leftJoin("$state->table", "$abook->table.stateId", "=", "$state->table.id")
        ->where("$abook->table.userId", $userId)->where("$abook->table.isDefaultBilling", '1')
        ->get();
        return $resultSet;
    }
    
    public static function getPackedOrders($orderNumber = '0') {
        
        $order = new Order;
        $shipment = new Shipment;
        
        if($orderNumber == '0')
        {
            $data = Order::select("$order->table.id","$order->table.orderNumber","$shipment->table.id as shipmentId")
                ->join("$shipment->table","$shipment->table.id","=","$order->table.shipmentId")
                ->where("$order->table.status","2")
                ->where("$order->table.type","shipment")
                ->where("$shipment->table.packed","packing_complete")
                ->where("$shipment->table.deleted","0")
                ->get()->toArray();
        }
        else
        {
            $data = Order::select("$order->table.id","$order->table.orderNumber","$shipment->table.id as shipmentId")
                ->join("$shipment->table","$shipment->table.id","=","$order->table.shipmentId")
                ->where("$order->table.status","2")
                ->where("$order->table.type","shipment")
                ->where("$shipment->table.packed","packing_complete")
                ->where("$shipment->table.deleted","0")
                ->where("orderNumber",$orderNumber)
                ->first();
        }
        return $data;
    }

}
