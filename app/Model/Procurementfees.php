<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Procurementfees extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.PROCUREMENTFEES');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to fetch Country list
     * @param array $param
     * @return object
     */
    public static function getFeesList($param) {
        $warehouse = new Warehouse;
        $fees = new Procurementfees;
        $country = new Country;

        $feesTable = $fees->prefix . $fees->table;

        $where = 1;

        if (!empty($param['searchWarehouse']) || $param['searchWarehouse'] != '') {
            $where = "$feesTable.warehouseId ='" . $param['searchWarehouse'] . "'";
        }


        $resultSet = Procurementfees::select(array("$fees->table.*", "$warehouse->table.address", "$country->table.name AS countryName"))
                ->leftJoin($warehouse->table, "$fees->table.warehouseId", '=', "$warehouse->table.id")
                ->leftJoin($country->table, "$warehouse->table.countryId", '=', "$country->table.id")
                ->where("$fees->table.deleted", "=", '0')
                ->whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    public static function getWarehouseList() {

        $country = new Country;
        $warehouse = new Warehouse;

        $resultSet = Warehouse::select(array("$warehouse->table.id", "$country->table.name AS countryName", "$warehouse->table.address"))
                ->leftJoin($country->table, "$warehouse->table.countryId", '=', "$country->table.id")
                ->where($warehouse->table . '.deleted', '0')
                ->get();

        return $resultSet;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Procurementfees::where('id', $id)
                ->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to get duplicate range of fees if exist for same warehouse
     * @param int $warehouseId
     * @param decimal $lowerVal
     * @param decimal $upperval
     * @return array
     */
    public static function checkduplicate($warehouseId, $lowerVal, $upperval) {


        $result = Procurementfees::whereRaw('(' . $lowerVal . ' BETWEEN feeLeft AND feeRight or ' . $upperval . ' BETWEEN feeLeft AND feeRight) AND deleted="0" AND warehouseId = ' . $warehouseId)->get();

        return $result;
    }

    public static function getProcessingFee($warehouseId, $itemPrice) {

        $data = Procurementfees::whereRaw($itemPrice . '>=feeLeft AND ' . $itemPrice . '<=feeRight AND deleted="0" and warehouseId=' . $warehouseId)->first();
        if (!empty($data)) {
            if ($data->feeType == 'P')
                $processingFee = $itemPrice * ($data->value / 100);
            else if ($data->feeType == 'A')
                $processingFee = $data->value;
        } else
            $processingFee = '0.00';

        return round($processingFee, 2);
    }

}
