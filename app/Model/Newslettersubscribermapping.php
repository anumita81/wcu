<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
class Newslettersubscribermapping extends Model
{
   
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.NEWSLETTERSUBSCRIBERMAPPING');
        $this->prefix = DB::getTablePrefix();
    }

    
}
