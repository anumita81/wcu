<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

//use App\libraries\helpers;
//use App\libraries\dbHelpers;

class Zonecity extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.ZONE_CITY');
        $this->prefix = DB::getTablePrefix();
    }

    public static function getData() {

        $return = array();

        $data = Zonecity::select('zoneId',\DB::raw('count(zoneId) as numCountry'))
                            ->groupBy('zoneId')
                            ->get();

        foreach($data as $eachData)
        {
            $return[$eachData->zoneId] = $eachData->numCountry;
        }

        return $return;
    }

    public static function zoneWiseData($zoneId) {

        $city = new City;
        $country = new Country;
        $state = new State;
        $zoneCiy = new Zonecity;
        $resultSet = DB::table("$country->table")
                ->join("$state->table", "$state->table.countryCode", '=', "$country->table.code")   
                ->join("$city->table",function($join) use ($country,$state,$city){
                        $join->on("$city->table.countryCode",'=',"$country->table.code")
                            ->on("$city->table.stateCode",'=',"$state->table.code");
                })
                ->join("$zoneCiy->table", "$zoneCiy->table.cityid", '=', "$city->table.id")
                ->select("$country->table.name as countryName", "$state->table.name as stateName", "$city->table.id","$city->table.name")
                ->where('zoneId',$zoneId)
                ->where($city->table.'.deleted', '0')
                ->where($city->table.'.status', '1')
                ->where($state->table.'.deleted', '0')
                ->where($state->table.'.status', '1')
                ->where($country->table.'.status','1')        
                ->get();       

        return $resultSet;
    }
}