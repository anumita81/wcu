<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
use Spatie\Activitylog\Traits\LogsActivity;
use customhelper;
use Carbon\Carbon;

class Shipment extends Model {

    use LogsActivity;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;
    protected static $logName = 'shipment';
    protected static $logAttributes = ['id', 'toCountry', 'toState', 'toCity', 'toName', 'toCompany', 'toAddress', 'toPhone', 'toEmail', 'shipmentType', 'storageCharge'];

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.SHIPMENT');
        $this->prefix = DB::getTablePrefix();
    }

    public function shipmentdelivery() {
        return $this->hasMany('App\Model\Shipmentdelivery', 'shipmentId', 'id');
    }

    public function warehouselocation() {
        return $this->hasMany('App\Model\Shipmentwarehouselocation', 'shipmentId', 'id');
    }

    public static function allStatus() {
        return array('Shipment Submiited', 'In Warehouse', 'In Transit', 'Customs clearing', 'In destination warehouse', 'Out for delivery', 'Delivered');
    }

    public static function allParticularShipmenttype() {
        return array(1 => 'Normal', 2 => 'Hazmat', 3 => 'Lithium', 4 => 'Secured', 5 => 'Fragile', 6 => 'Liquid');
    }

    public static function typeWiseUserShipment($userId, $isGroupShipment="0") {

        $typeWiseShipment = $notShippedoutFillnship = array();
        $unpaidShipments = Shipment::select('id', 'partucilarShipmentType', 'warehouseId')->where('userId', $userId)->where('paymentStatus', 'unpaid')->whereRaw('(paymentMethodId is null or paymentMethodId=0)')->where('deleted', '0')->where('isGroupShipment', $isGroupShipment)->get();
        foreach ($unpaidShipments as $eachUnpaidShipment) {
            $typeWiseShipment[$eachUnpaidShipment->warehouseId][$eachUnpaidShipment->partucilarShipmentType][] = $eachUnpaidShipment->id;
        }
        if($isGroupShipment == '0'){
            $notShippedoutFillnship = Fillship::select('id', 'warehouseId')->where('userId', $userId)->where('shippOutOpt', '0')->where('deleted', '0')->get();
            foreach ($notShippedoutFillnship as $eachNotpaidFillnship) {
                $typeWiseShipment[$eachNotpaidFillnship->warehouseId]['fillnship'][] = $eachNotpaidFillnship->id;
            }
        }
        

        return $typeWiseShipment;
    }

    public static function getShipmentList($param) {

        $shipmentWarehouse = new Shipmentwarehouselocation;
        $warehouselocation = new Warehouselocation;
        $shipmentLabel = new Shipmentlabelprintlog;

        $shipmentMain = new Shipment;
        $shipmentDelivery = new Shipmentdelivery;
        $shipmentItem = new Shipmentpackage;
        $user = new User;

        $shipmentWarehouseTable = $shipmentMain->prefix . $shipmentWarehouse->table;
        $warehouselocationTable = $warehouselocation->prefix . $warehouselocation->table;
        $shipmentLabelTable = $shipmentLabel->prefix . $shipmentLabel->table;

        $shipmentMainTable = $shipmentMain->prefix . $shipmentMain->table;
        $shipmentDeliveryTable = $shipmentDelivery->prefix . $shipmentDelivery->table;
        $shipmentItemTable = $shipmentMain->prefix . $shipmentItem->table;
        $userTable = $user->prefix . $user->table;

        // $where = "$shipmentMainTable.deleted = 0";

        $where = 1;
        $having = 1;
        $shipmentTableSearchFields = array();
        if ($param['isSearched'] == 1) {
            if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($shipmentMainTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $shipmentMainTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($shipmentMainTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);

                    $where .= "  AND date($shipmentMainTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }
            if (!empty($param['searchWarehouse']))
                $where .= " AND $shipmentMainTable.warehouseId = " . $param['searchWarehouse'];

            if (!empty($param['searchShipment']['idFrom']))
                $where .= " AND $shipmentMainTable.id >= " . $param['searchShipment']['idFrom'];

            if (!empty($param['searchShipment']['idTo']))
                $where .= " AND $shipmentMainTable.id <= " . $param['searchShipment']['idTo'];

            if (!empty($param['searchShipment']['userUnit']))
                $where .= " AND $userTable.unit = '" . $param['searchShipment']['userUnit'] . "'";

            if (!empty($param['searchShipment']['prepaid']))
                $where .= " AND $shipmentMainTable.prepaid = '" . $param['searchShipment']['prepaid'] . "'";

            if (!empty($param['searchShipment']['shipmentStatus']))
                $where .= " AND $shipmentMainTable.shipmentStatus = '" . $param['searchShipment']['shipmentStatus'] . "'";

            if (!empty($param['searchShipment']['paymentStatus']))
                $where .= " AND $shipmentMainTable.paymentStatus = '" . $param['searchShipment']['paymentStatus'] . "'";

            if (!empty($param['searchShipment']['shipmentType']))
                $where .= " AND $shipmentMainTable.partucilarShipmentType = '" . $param['searchShipment']['shipmentType'] . "'";

            if (!empty($param['searchShipment']['totalFrom']))
                $having .= " AND sum($shipmentDeliveryTable.totalCost) >= '" . $param['searchShipment']['totalFrom'] . "'";

            if (!empty($param['searchShipment']['totalTo']))
                $having .= " AND sum($shipmentDeliveryTable.totalCost) <= '" . $param['searchShipment']['totalTo'] . "'";

            if (!empty($param['searchShipment']['itemCostFrom']))
                $having .= " AND sum($shipmentDeliveryTable.totalItemCost) >= '" . $param['searchShipment']['itemCostFrom'] . "'";

            if (!empty($param['searchShipment']['itemCostTo']))
                $having .= " AND sum($shipmentDeliveryTable.totalItemCost) <= '" . $param['searchShipment']['itemCostTo'] . "'";


            if (!empty($param['searchShipment']['warehouseId']))
                $where .= " AND $shipmentMainTable.warehouseId = '" . $param['searchShipment']['warehouseId'] . "'";

            if (!empty($param['searchShipment']['labelType'])) {
                $labelSearch = "Shipping Label";
                if ($param['searchShipment']['labelType'] == "red_star") {
                    $labelSearch = "Red Star Label";
                } else if ($param['searchShipment']['labelType'] == "dhl") {
                    $labelSearch = "DHL Label";
                } else if ($param['searchShipment']['labelType'] == "nations_delivery") {
                    $labelSearch = "Nations Delivery Label";
                }
                $where .= " AND $shipmentLabelTable.labelType = '" . $labelSearch . "'";
            }

            if (!empty($param['searchShipment']['deliveryCompanyId']))
                $where .= " AND $shipmentDeliveryTable.deliveryCompanyId = " . $param['searchShipment']['deliveryCompanyId'];
            if (!empty($param['searchShipment']['dispatchCompanyId']))
                $where .= " AND $shipmentDeliveryTable.dispatchCompanyId = " . $param['searchShipment']['dispatchCompanyId'];

            if (!empty($param['searchShipment']['user']))
                $where .= " AND ($userTable.firstName LIKE '%" . $param['searchShipment']['user'] . "%' OR $userTable.email LIKE '%" . $param['searchShipment']['user'] . "%')";

            if (!empty($param['searchShipment']['receiver']))
                $where .= " AND ($shipmentMainTable.toName LIKE '%" . $param['searchShipment']['receiver'] . "%' OR $shipmentMainTable.toEmail LIKE '%" . $param['searchShipment']['receiver'] . "%')";

            if (!empty($param['searchShipment']['toCountry']))
                $where .= " AND $shipmentMainTable.toCountry= " . $param['searchShipment']['toCountry'];

            if (!empty($param['searchShipment']['itemType']))
                $where .= " AND $viewShipmentDetailsTable.itemType = '" . $param['searchShipment']['itemType'] . "'";

            if (!empty($param['searchShipment']['declaredValueFrom']))
                $where .= " AND $viewShipmentDetailsTable.declaredValue >= '" . $param['searchShipment']['declaredValueFrom'] . "'";

            if (!empty($param['searchShipment']['declaredValueTo']))
                $where .= " AND $viewShipmentDetailsTable.declaredValue <= '" . $param['searchShipment']['declaredValueTo'] . "'";

            if (isset($param['searchByshipment']) && !empty($param['searchByshipment'])) {
                $where .= " AND $shipmentMainTable.id =" . $param['searchByshipment'][0];
            }

            if (isset($param['searchShipment']['shippingMethod']) && !empty($param['searchShipment']['shippingMethod'])) {
                $where .= " AND $shipmentDeliveryTable.shippingMethodId = " . $param['searchShipment']['shippingMethod'];
            }

            if (isset($param['searchShipment']['store']) && !empty($param['searchShipment']['store'])) {
                $where .= " AND $shipmentItemTable.storeId = " . $param['searchShipment']['store'];
            }

            if (isset($param['searchShipment']['category']) && !empty($param['searchShipment']['category'])) {
                $where .= " AND $shipmentItemTable.siteCategoryId = " . $param['searchShipment']['category'];
            }

            if (isset($param['searchShipment']['subcategory']) && !empty($param['searchShipment']['subcategory'])) {
                $where .= " AND $shipmentItemTable.siteSubCategoryId = " . $param['searchShipment']['subcategory'];
            }

            if (isset($param['searchShipment']['product']) && !empty($param['searchShipment']['product'])) {
                $where .= " AND $shipmentItemTable.siteProductId = " . $param['searchShipment']['product'];
            }

            if (isset($param['searchShipment']['packed']) && !empty($param['searchShipment']['packed'])) {
                $where .= " AND $shipmentMainTable.packed = '" . $param['searchShipment']['packed'] . "'";
            }

            if (isset($param['searchShipment']['rows']) && !empty($param['searchShipment']['rows'])) {
                $where .= " AND $shipmentWarehouseTable.warehouseRowId = '" . $param['searchShipment']['rows'] . "'";
            }

            if (isset($param['searchShipment']['zones']) && !empty($param['searchShipment']['zones'])) {
                $where .= " AND $shipmentWarehouseTable.warehouseZoneId = '" . $param['searchShipment']['zones'] . "'";
            }

            if (isset($param['searchOffer'])) {
                $where .= " AND $shipmentMainTable.couponcodeApplied = '" . $param['searchOffer'] . "'";
            }
        }


        if ($param['isSearched'] == 1) {
            //echo 'd';exit;
            if (!empty($param['searchShipment']['store']) || !empty($param['searchShipment']['category']) || !empty($param['searchShipment']['subcategory']) || !empty($param['searchShipment']['product'])) {
                $resultSet = Shipment::select(array("$shipmentMain->table.*"))
                        ->join("$shipmentLabel->table", "$shipmentLabel->table.shipmentId", "=", "$shipmentMain->table.id")
                        ->leftJoin($shipmentDelivery->table, "$shipmentMain->table.id", "=", "$shipmentDelivery->table.shipmentId")
                        ->leftJoin($shipmentItem->table, "$shipmentItem->table.id", "=", "$shipmentMain->table.id")
                        ->leftJoin($user->table, "$user->table.id", "=", "$shipmentMain->table.userId")
                        ->leftJoin($shipmentWarehouse->table, "$shipmentWarehouse->table.shipmentId", '=', "$shipmentMain->table.id")
                        ->leftJoin("$warehouselocation->table AS warehouseRow", "warehouseRow.id", '=', "$shipmentWarehouse->table.warehouseRowId")
                        ->leftJoin("$warehouselocation->table AS warehouseZone", "warehouseZone.id", '=', "$shipmentWarehouse->table.warehouseZoneId")
                        ->where("shipments.deleted", "0")->where("shipments.isGroupShipment", "0")->whereRaw($where)->groupBy("$shipmentMain->table.id")->havingRaw($having)->orderBy("shipments.id", "desc")
                        ->paginate($param['searchDisplay']);
            } else if ((!empty($param['searchShipment']['labelType'])) && empty($param['searchShipment']['rows']) && empty($param['searchShipment']['zones'])) {
                $resultSet = Shipment::select(array("$shipmentMain->table.*"))
                        ->join("$shipmentLabel->table", "$shipmentLabel->table.shipmentId", "=", "$shipmentMain->table.id")
                        ->leftJoin($shipmentDelivery->table, "$shipmentMain->table.id", "=", "$shipmentDelivery->table.shipmentId")
                        ->leftJoin($user->table, "$user->table.id", "=", "$shipmentMain->table.userId")
                        ->where("shipments.deleted", "0")->where("shipments.isGroupShipment", "0")->whereRaw($where)->groupBy("$shipmentMain->table.id")->havingRaw($having)->orderBy("shipments.id", "desc")
                        ->paginate($param['searchDisplay']);
            } else if (empty($param['searchShipment']['labelType']) && (!empty($param['searchShipment']['rows']) || !empty($param['searchShipment']['zones']))) {
                $resultSet = Shipment::select(array("$shipmentMain->table.*"))
                        ->leftJoin($shipmentDelivery->table, "$shipmentMain->table.id", "=", "$shipmentDelivery->table.shipmentId")
                        ->leftJoin($user->table, "$user->table.id", "=", "$shipmentMain->table.userId")
                        ->leftJoin($shipmentWarehouse->table, "$shipmentWarehouse->table.shipmentId", '=', "$shipmentMain->table.id")
                        ->leftJoin("$warehouselocation->table AS warehouseRow", "warehouseRow.id", '=', "$shipmentWarehouse->table.warehouseRowId")
                        ->leftJoin("$warehouselocation->table AS warehouseZone", "warehouseZone.id", '=', "$shipmentWarehouse->table.warehouseZoneId")
                        ->where("shipments.deleted", "0")->where("shipments.isGroupShipment", "0")->whereRaw($where)->groupBy("$shipmentMain->table.id")->havingRaw($having)->orderBy("shipments.id", "desc")
                        ->paginate($param['searchDisplay']);
            } else if (!empty($param['searchShipment']['labelType']) && (!empty($param['searchShipment']['rows']) || !empty($param['searchShipment']['zones']))) {
                $resultSet = Shipment::select(array("$shipmentMain->table.*"))
                        ->join("$shipmentLabel->table", "$shipmentLabel->table.shipmentId", "=", "$shipmentMain->table.id")
                        ->leftJoin($shipmentDelivery->table, "$shipmentMain->table.id", "=", "$shipmentDelivery->table.shipmentId")
                        ->leftJoin($user->table, "$user->table.id", "=", "$shipmentMain->table.userId")
                        ->leftJoin($shipmentWarehouse->table, "$shipmentWarehouse->table.shipmentId", '=', "$shipmentMain->table.id")
                        ->leftJoin("$warehouselocation->table AS warehouseRow", "warehouseRow.id", '=', "$shipmentWarehouse->table.warehouseRowId")
                        ->leftJoin("$warehouselocation->table AS warehouseZone", "warehouseZone.id", '=', "$shipmentWarehouse->table.warehouseZoneId")
                        ->where("shipments.deleted", "0")->where("shipments.isGroupShipment", "0")->whereRaw($where)->groupBy("$shipmentMain->table.id")->havingRaw($having)->orderBy("shipments.id", "desc")
                        ->paginate($param['searchDisplay']);
            } else {
                $resultSet = Shipment::select(array("$shipmentMain->table.*"))
                        ->leftJoin($shipmentDelivery->table, "$shipmentMain->table.id", "=", "$shipmentDelivery->table.shipmentId")
                        ->leftJoin($user->table, "$user->table.id", "=", "$shipmentMain->table.userId")
                        ->where("shipments.deleted", "0")->where("shipments.isGroupShipment", "0")->whereRaw($where)->groupBy("$shipmentMain->table.id")->havingRaw($having)->orderBy("shipments.id", "desc")
                        ->paginate($param['searchDisplay']);
            }
        } else {
            //DB::enableQueryLog();
            $resultSet = Shipment::where("shipments.deleted", "0")->where('isGroupShipment', '0')->orderBy("shipments.id", "desc")->paginate($param['searchDisplay']);

            // dd(DB::getQueryLog());
        }
        //dd(DB::getQueryLog());        
        return $resultSet;
    }


    public static function getGroupShipmentList($param)
    {
        $shipmentWarehouse = new Shipmentwarehouselocation;
        $warehouselocation = new Warehouselocation;
        $shipmentLabel = new Shipmentlabelprintlog;

        $shipmentMain = new Shipment;
        $shipmentDelivery = new Shipmentdelivery;
        $shipmentItem = new Shipmentpackage;
        $user = new User;
        $group = new \App\Model\Groupdetails;

        $shipmentWarehouseTable = $shipmentMain->prefix . $shipmentWarehouse->table;
        $warehouselocationTable = $warehouselocation->prefix . $warehouselocation->table;
        $shipmentLabelTable = $shipmentLabel->prefix . $shipmentLabel->table;

        $shipmentMainTable = $shipmentMain->prefix . $shipmentMain->table;
        $shipmentDeliveryTable = $shipmentDelivery->prefix . $shipmentDelivery->table;
        $shipmentItemTable = $shipmentMain->prefix . $shipmentItem->table;
        $userTable = $user->prefix . $user->table;
        $groupTable = $user->prefix . $group->table;

        // $where = "$shipmentMainTable.deleted = 0";

        $where = 1;
        $having = 1;
        $shipmentTableSearchFields = array();
        if ($param['isSearched'] == 1) {
            if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($shipmentMainTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $shipmentMainTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($shipmentMainTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);

                    $where .= "  AND date($shipmentMainTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }
            if (!empty($param['searchWarehouse']))
                $where .= " AND $shipmentMainTable.warehouseId = " . $param['searchWarehouse'];

            if (!empty($param['searchShipment']['idFrom']))
                $where .= " AND $shipmentMainTable.id >= " . $param['searchShipment']['idFrom'];

            if (!empty($param['searchShipment']['idTo']))
                $where .= " AND $shipmentMainTable.id <= " . $param['searchShipment']['idTo'];

            if (!empty($param['searchShipment']['userUnit']))
                $where .= " AND $userTable.unit = '" . $param['searchShipment']['userUnit'] . "'";

            if (!empty($param['searchShipment']['prepaid']))
                $where .= " AND $shipmentMainTable.prepaid = '" . $param['searchShipment']['prepaid'] . "'";

            if (!empty($param['searchShipment']['shipmentStatus']))
                $where .= " AND $shipmentMainTable.shipmentStatus = '" . $param['searchShipment']['shipmentStatus'] . "'";

            if (!empty($param['searchShipment']['paymentStatus']))
                $where .= " AND $shipmentMainTable.paymentStatus = '" . $param['searchShipment']['paymentStatus'] . "'";

            if (!empty($param['searchShipment']['shipmentType']))
                $where .= " AND $shipmentMainTable.partucilarShipmentType = '" . $param['searchShipment']['shipmentType'] . "'";

            if (!empty($param['searchShipment']['totalFrom']))
                $having .= " AND sum($shipmentDeliveryTable.totalCost) >= '" . $param['searchShipment']['totalFrom'] . "'";

            if (!empty($param['searchShipment']['totalTo']))
                $having .= " AND sum($shipmentDeliveryTable.totalCost) <= '" . $param['searchShipment']['totalTo'] . "'";

            if (!empty($param['searchShipment']['itemCostFrom']))
                $having .= " AND sum($shipmentDeliveryTable.totalItemCost) >= '" . $param['searchShipment']['itemCostFrom'] . "'";

            if (!empty($param['searchShipment']['itemCostTo']))
                $having .= " AND sum($shipmentDeliveryTable.totalItemCost) <= '" . $param['searchShipment']['itemCostTo'] . "'";


            if (!empty($param['searchShipment']['warehouseId']))
                $where .= " AND $shipmentMainTable.warehouseId = '" . $param['searchShipment']['warehouseId'] . "'";

            if (!empty($param['searchShipment']['labelType'])) {
                $labelSearch = "Shipping Label";
                if ($param['searchShipment']['labelType'] == "red_star") {
                    $labelSearch = "Red Star Label";
                } else if ($param['searchShipment']['labelType'] == "dhl") {
                    $labelSearch = "DHL Label";
                } else if ($param['searchShipment']['labelType'] == "nations_delivery") {
                    $labelSearch = "Nations Delivery Label";
                }
                $where .= " AND $shipmentLabelTable.labelType = '" . $labelSearch . "'";
            }

            if (!empty($param['searchShipment']['deliveryCompanyId']))
                $where .= " AND $shipmentDeliveryTable.deliveryCompanyId = " . $param['searchShipment']['deliveryCompanyId'];
            if (!empty($param['searchShipment']['dispatchCompanyId']))
                $where .= " AND $shipmentDeliveryTable.dispatchCompanyId = " . $param['searchShipment']['dispatchCompanyId'];

            if (!empty($param['searchShipment']['user']))
                $where .= " AND ($userTable.firstName LIKE '%" . $param['searchShipment']['user'] . "%' OR $userTable.email LIKE '%" . $param['searchShipment']['user'] . "%')";

            if (!empty($param['searchShipment']['receiver']))
                $where .= " AND ($shipmentMainTable.toName LIKE '%" . $param['searchShipment']['receiver'] . "%' OR $shipmentMainTable.toEmail LIKE '%" . $param['searchShipment']['receiver'] . "%')";

            if (!empty($param['searchShipment']['toCountry']))
                $where .= " AND $shipmentMainTable.toCountry= " . $param['searchShipment']['toCountry'];

            if (!empty($param['searchShipment']['itemType']))
                $where .= " AND $viewShipmentDetailsTable.itemType = '" . $param['searchShipment']['itemType'] . "'";

            if (!empty($param['searchShipment']['declaredValueFrom']))
                $where .= " AND $viewShipmentDetailsTable.declaredValue >= '" . $param['searchShipment']['declaredValueFrom'] . "'";

            if (!empty($param['searchShipment']['declaredValueTo']))
                $where .= " AND $viewShipmentDetailsTable.declaredValue <= '" . $param['searchShipment']['declaredValueTo'] . "'";

            if (isset($param['searchByshipment']) && !empty($param['searchByshipment'])) {
                $where .= " AND $shipmentMainTable.id =" . $param['searchByshipment'][0];
            }

            if (isset($param['searchShipment']['shippingMethod']) && !empty($param['searchShipment']['shippingMethod'])) {
                $where .= " AND $shipmentDeliveryTable.shippingMethodId = " . $param['searchShipment']['shippingMethod'];
            }

            if (isset($param['searchShipment']['store']) && !empty($param['searchShipment']['store'])) {
                $where .= " AND $shipmentItemTable.storeId = " . $param['searchShipment']['store'];
            }

            if (isset($param['searchShipment']['category']) && !empty($param['searchShipment']['category'])) {
                $where .= " AND $shipmentItemTable.siteCategoryId = " . $param['searchShipment']['category'];
            }

            if (isset($param['searchShipment']['subcategory']) && !empty($param['searchShipment']['subcategory'])) {
                $where .= " AND $shipmentItemTable.siteSubCategoryId = " . $param['searchShipment']['subcategory'];
            }

            if (isset($param['searchShipment']['product']) && !empty($param['searchShipment']['product'])) {
                $where .= " AND $shipmentItemTable.siteProductId = " . $param['searchShipment']['product'];
            }

            if (isset($param['searchShipment']['packed']) && !empty($param['searchShipment']['packed'])) {
                $where .= " AND $shipmentMainTable.packed = '" . $param['searchShipment']['packed'] . "'";
            }

            if (isset($param['searchShipment']['rows']) && !empty($param['searchShipment']['rows'])) {
                $where .= " AND $shipmentWarehouseTable.warehouseRowId = '" . $param['searchShipment']['rows'] . "'";
            }

            if (isset($param['searchShipment']['zones']) && !empty($param['searchShipment']['zones'])) {
                $where .= " AND $shipmentWarehouseTable.warehouseZoneId = '" . $param['searchShipment']['zones'] . "'";
            }

            if (isset($param['searchOffer'])) {
                $where .= " AND $shipmentMainTable.couponcodeApplied = '" . $param['searchOffer'] . "'";
            }
        }


        if ($param['isSearched'] == 1) {
            //echo 'd';exit;
            if (!empty($param['searchShipment']['store']) || !empty($param['searchShipment']['category']) || !empty($param['searchShipment']['subcategory']) || !empty($param['searchShipment']['product'])) {
                $resultSet = Shipment::select(array("$shipmentMain->table.*"))
                        ->join("$shipmentLabel->table", "$shipmentLabel->table.shipmentId", "=", "$shipmentMain->table.id")
                        ->leftJoin($shipmentDelivery->table, "$shipmentMain->table.id", "=", "$shipmentDelivery->table.shipmentId")
                        ->leftJoin($shipmentItem->table, "$shipmentItem->table.id", "=", "$shipmentMain->table.id")
                        ->leftJoin($user->table, "$user->table.id", "=", "$shipmentMain->table.userId")
                        ->leftJoin($group->table, "$group->table.id", "=", "$shipmentMain->table.groupId")
                        ->leftJoin($shipmentWarehouse->table, "$shipmentWarehouse->table.shipmentId", '=', "$shipmentMain->table.id")
                        ->leftJoin("$warehouselocation->table AS warehouseRow", "warehouseRow.id", '=', "$shipmentWarehouse->table.warehouseRowId")
                        ->leftJoin("$warehouselocation->table AS warehouseZone", "warehouseZone.id", '=', "$shipmentWarehouse->table.warehouseZoneId")
                        ->where("shipments.deleted", "0")->where("shipments.isGroupShipment", "1")->whereRaw($where)->groupBy("$shipmentMain->table.id")->havingRaw($having)->orderBy("shipments.id", "desc")
                        ->paginate($param['searchDisplay']);
            } else if ((!empty($param['searchShipment']['labelType'])) && empty($param['searchShipment']['rows']) && empty($param['searchShipment']['zones'])) {
                $resultSet = Shipment::select(array("$shipmentMain->table.*"))
                        ->join("$shipmentLabel->table", "$shipmentLabel->table.shipmentId", "=", "$shipmentMain->table.id")
                        ->leftJoin($shipmentDelivery->table, "$shipmentMain->table.id", "=", "$shipmentDelivery->table.shipmentId")
                        ->leftJoin($user->table, "$user->table.id", "=", "$shipmentMain->table.userId")
                        ->leftJoin($group->table, "$group->table.id", "=", "$shipmentMain->table.groupId")
                        ->where("shipments.deleted", "0")->where("shipments.isGroupShipment", "1")->whereRaw($where)->groupBy("$shipmentMain->table.id")->havingRaw($having)->orderBy("shipments.id", "desc")
                        ->paginate($param['searchDisplay']);
            } else if (empty($param['searchShipment']['labelType']) && (!empty($param['searchShipment']['rows']) || !empty($param['searchShipment']['zones']))) {
                $resultSet = Shipment::select(array("$shipmentMain->table.*"))
                        ->leftJoin($shipmentDelivery->table, "$shipmentMain->table.id", "=", "$shipmentDelivery->table.shipmentId")
                        ->leftJoin($user->table, "$user->table.id", "=", "$shipmentMain->table.userId")
                        ->leftJoin($shipmentWarehouse->table, "$shipmentWarehouse->table.shipmentId", '=', "$shipmentMain->table.id")
                        ->leftJoin("$warehouselocation->table AS warehouseRow", "warehouseRow.id", '=', "$shipmentWarehouse->table.warehouseRowId")
                        ->leftJoin("$warehouselocation->table AS warehouseZone", "warehouseZone.id", '=', "$shipmentWarehouse->table.warehouseZoneId")
                        ->leftJoin($group->table, "$group->table.id", "=", "$shipmentMain->table.groupId")
                        ->where("shipments.deleted", "0")->where("shipments.isGroupShipment", "1")->whereRaw($where)->groupBy("$shipmentMain->table.id")->havingRaw($having)->orderBy("shipments.id", "desc")
                        ->paginate($param['searchDisplay']);
            } else if (!empty($param['searchShipment']['labelType']) && (!empty($param['searchShipment']['rows']) || !empty($param['searchShipment']['zones']))) {
                $resultSet = Shipment::select(array("$shipmentMain->table.*"))
                        ->join("$shipmentLabel->table", "$shipmentLabel->table.shipmentId", "=", "$shipmentMain->table.id")
                        ->leftJoin($shipmentDelivery->table, "$shipmentMain->table.id", "=", "$shipmentDelivery->table.shipmentId")
                        ->leftJoin($user->table, "$user->table.id", "=", "$shipmentMain->table.userId")
                        ->leftJoin($shipmentWarehouse->table, "$shipmentWarehouse->table.shipmentId", '=', "$shipmentMain->table.id")
                        ->leftJoin("$warehouselocation->table AS warehouseRow", "warehouseRow.id", '=', "$shipmentWarehouse->table.warehouseRowId")
                        ->leftJoin("$warehouselocation->table AS warehouseZone", "warehouseZone.id", '=', "$shipmentWarehouse->table.warehouseZoneId")
                        ->leftJoin($group->table, "$group->table.id", "=", "$shipmentMain->table.groupId")
                        ->where("shipments.deleted", "0")->where("shipments.isGroupShipment", "1")->whereRaw($where)->groupBy("$shipmentMain->table.id")->havingRaw($having)->orderBy("shipments.id", "desc")
                        ->paginate($param['searchDisplay']);
            } else {
                $resultSet = Shipment::select(array("$shipmentMain->table.*"))
                        ->leftJoin($shipmentDelivery->table, "$shipmentMain->table.id", "=", "$shipmentDelivery->table.shipmentId")
                        ->leftJoin($user->table, "$user->table.id", "=", "$shipmentMain->table.userId")
                        ->leftJoin($group->table, "$group->table.id", "=", "$shipmentMain->table.groupId")
                        ->where("shipments.deleted", "0")->where("shipments.isGroupShipment", "1")->whereRaw($where)->groupBy("$shipmentMain->table.id")->havingRaw($having)->orderBy("shipments.id", "desc")
                        ->paginate($param['searchDisplay']);
            }
        } else {
            //DB::enableQueryLog();
            $resultSet = Shipment::select(array("$shipmentMain->table.*", "$group->table.groupName"))->where("shipments.deleted", "0")->leftJoin($group->table, "$group->table.id", "=", "$shipmentMain->table.groupId")->where('isGroupShipment', '1')->orderBy("shipments.id", "desc")->paginate($param['searchDisplay']);

            // dd(DB::getQueryLog());
        }
        //dd(DB::getQueryLog());        
        return $resultSet;
    }

    /**
     * Method used to fetch users list
     * @param array $param
     * @return object
     */
    public static function getShipmentListOld($param) {

        DB::enableQueryLog();

        $viewShipment = new Viewshipment;
        $viewShipmentDetails = new ViewShipmentDetails;
        $shipmentWarehouse = new Shipmentwarehouselocation;
        $warehouselocation = new Warehouselocation;
        $shipmentLabel = new Shipmentlabelprintlog;

        $shipmentMain = new Shipment;
        $shipmentDelivery = new Shipmentdelivery;
        $user = new User;


        $viewShipmentTable = $viewShipment->prefix . $viewShipment->table;
        $viewShipmentDetailsTable = $viewShipmentDetails->prefix . $viewShipmentDetails->table;
        $shipmentWarehouseTable = $viewShipment->prefix . $shipmentWarehouse->table;
        $warehouselocationTable = $viewShipment->prefix . $warehouselocation->table;
        $shipmentLabelTable = $shipmentLabel->prefix . $shipmentLabel->table;

        $shipmentMainTable = $shipmentMain->prefix . $shipmentMain->table;
        $shipmentDeliveryTable = $shipmentDelivery->prefix . $shipmentDelivery->table;
        $userTable = $user->prefix . $user->table;

        // $where = "$shipmentMainTable.deleted = 0";

        $where = 1;
        $shipmentTableSearchFields = array();
        if (!empty($param['searchWarehouse']))
            $where .= " AND $shipmentMainTable.warehouseId = " . $param['searchWarehouse'];

        if (!empty($param['searchShipment']['idFrom']))
            $where .= " AND $shipmentMainTable.id >= " . $param['searchShipment']['idFrom'];

        if (!empty($param['searchShipment']['idTo']))
            $where .= " AND $shipmentMainTable.id <= " . $param['searchShipment']['idTo'];

        if (!empty($param['searchShipment']['unitFrom']))
            $where .= " AND $viewShipmentTable.unit >= '" . $param['searchShipment']['unitFrom'] . "'";

        if (!empty($param['searchShipment']['unitTo']))
            $where .= " AND $viewShipmentTable.unit <= '" . $param['searchShipment']['unitFrom'] . "'";

        if (!empty($param['searchShipment']['prepaid']))
            $where .= " AND $shipmentMainTable.prepaid = '" . $param['searchShipment']['prepaid'] . "'";

        if (!empty($param['searchShipment']['shipmentStatus']))
            $where .= " AND $shipmentMainTable.shipmentStatus = '" . $param['searchShipment']['shipmentStatus'] . "'";

        if (!empty($param['searchShipment']['paymentStatus']))
            $where .= " AND $shipmentMainTable.paymentStatus = '" . $param['searchShipment']['paymentStatus'] . "'";

        if (!empty($param['searchShipment']['shipmentType']))
            $where .= " AND $shipmentMainTable.partucilarShipmentType = '" . $param['searchShipment']['shipmentType'] . "'";

        if (!empty($param['searchShipment']['totalFrom']))
            $where .= " AND $shipmentMainTable.total >= '" . $param['searchShipment']['totalFrom'] . "'";

        if (!empty($param['searchShipment']['totalTo']))
            $where .= " AND $shipmentMainTable.total <= '" . $param['searchShipment']['totalTo'] . "'";


        if (!empty($param['searchShipment']['warehouseId']))
            $where .= " AND $shipmentMainTable.warehouseId = '" . $param['searchShipment']['warehouseId'] . "'";

        if (!empty($param['searchShipment']['labelType'])) {
            $labelSearch = "Shipping Label";
            if ($param['searchShipment']['labelType'] == "red_star") {
                $labelSearch = "Red Star Label";
            } else if ($param['searchShipment']['labelType'] == "dhl") {
                $labelSearch = "DHL Label";
            } else if ($param['searchShipment']['labelType'] == "nations_delivery") {
                $labelSearch = "Nations Delivery Label";
            }
            $where .= " AND $shipmentLabelTable.labelType = '" . $labelSearch . "'";
        }

        if (!empty($param['searchShipment']['deliveryCompanyId']))
            $where .= " AND $shipmentDeliveryTable.deliveryCompanyId = " . $param['searchShipment']['deliveryCompanyId'];
        if (!empty($param['searchShipment']['dispatchCompanyId']))
            $where .= " AND $shipmentDeliveryTable.dispatchCompanyId = " . $param['searchShipment']['dispatchCompanyId'];

        if (!empty($param['searchShipment']['user']))
            $where .= " AND ($viewShipmentTable.customerName LIKE '%" . $param['searchShipment']['user'] . "%' OR $viewShipmentTable.customerEmail LIKE '%" . $param['searchShipment']['user'] . "%')";

        if (!empty($param['searchShipment']['receiver']))
            $where .= " AND ($viewShipmentTable.toName LIKE '%" . $param['searchShipment']['receiver'] . "%' OR $viewShipmentTable.toEmail LIKE '%" . $param['searchShipment']['receiver'] . "%')";


        if (!empty($param['searchShipment']['itemType']))
            $where .= " AND $viewShipmentDetailsTable.itemType = '" . $param['searchShipment']['itemType'] . "'";

        if (!empty($param['searchShipment']['declaredValueFrom']))
            $where .= " AND $viewShipmentDetailsTable.declaredValue >= '" . $param['searchShipment']['declaredValueFrom'] . "'";

        if (!empty($param['searchShipment']['declaredValueTo']))
            $where .= " AND $viewShipmentDetailsTable.declaredValue <= '" . $param['searchShipment']['declaredValueTo'] . "'";

        if (isset($param['searchByshipment']) && !empty($param['searchByshipment'])) {
            $where .= " AND $shipmentMainTable.id =" . $param['searchByshipment'][0];
        }

        if (isset($param['searchShipment']['shippingMethod']) && !empty($param['searchShipment']['shippingMethod'])) {
            $where .= " AND $shipmentDeliveryTable.shippingMethodId = " . $param['searchShipment']['shippingMethod'];
        }

        if (isset($param['searchShipment']['store']) && !empty($param['searchShipment']['store'])) {
            $where .= " AND $viewShipmentDetailsTable.storeId = " . $param['searchShipment']['store'];
        }

        if (isset($param['searchShipment']['category']) && !empty($param['searchShipment']['category'])) {
            $where .= " AND $viewShipmentDetailsTable.itemCategoryId = " . $param['searchShipment']['category'];
        }

        if (isset($param['searchShipment']['subcategory']) && !empty($param['searchShipment']['subcategory'])) {
            $where .= " AND $viewShipmentDetailsTable.itemSubCategoryId = " . $param['searchShipment']['subcategory'];
        }

        if (isset($param['searchShipment']['product']) && !empty($param['searchShipment']['product'])) {
            $where .= " AND $viewShipmentDetailsTable.itemProductId = " . $param['searchShipment']['product'];
        }

        if (isset($param['searchShipment']['packed']) && !empty($param['searchShipment']['packed'])) {
            $where .= " AND $shipmentMainTable.packed = '" . $param['searchShipment']['packed'] . "'";
        }

        if (isset($param['searchShipment']['rows']) && !empty($param['searchShipment']['rows'])) {
            $where .= " AND $shipmentWarehouseTable.warehouseRowId = '" . $param['searchShipment']['rows'] . "'";
        }

        if (isset($param['searchShipment']['zones']) && !empty($param['searchShipment']['zones'])) {
            $where .= " AND $shipmentWarehouseTable.warehouseZoneId = '" . $param['searchShipment']['zones'] . "'";
        }

        if (isset($param['searchOffer'])) {
            $where .= " AND $shipmentMainTable.couponcodeApplied = '" . $param['searchOffer'] . "'";
        }



        if ((!empty($param['searchShipment']['labelType'])) && empty($param['searchShipment']['rows']) && empty($param['searchShipment']['zones'])) {
            //echo 'a';exit;
            $resultSet = Viewshipment::select(array("$viewShipment->table.*", 'deliveryCompany',
                        DB::raw("GROUP_CONCAT(itemType) as itemType"), 'partucilarShipmentType',
                        "toCityName AS cityName", "toCountryName AS countryName", "toStateName AS stateName"
                    ))
                    ->leftJoin("$viewShipmentDetails->table", "$viewShipment->table.id", "=", "$viewShipmentDetails->table.shipmentId")
                    ->leftJoin("$shipmentLabel->table", "$shipmentLabel->table.shipmentId", "=", "$viewShipment->table.id")
                    ->whereRaw($where)
                    ->groupBy("$viewShipment->table.id")
                    ->orderBy($param['field'], $param['type'])
                    ->paginate($param['searchDisplay']);
        } else if (empty($param['searchShipment']['labelType']) && (!empty($param['searchShipment']['rows']) || !empty($param['searchShipment']['zones']))) {
            //echo 's';exit;
            $resultSet = Viewshipment::select(array("$viewShipment->table.*", 'deliveryCompany',
                        DB::raw("GROUP_CONCAT(itemType) as itemType"), 'partucilarShipmentType',
                        "toCityName AS cityName", "toCountryName AS countryName", "toStateName AS stateName", "$shipmentWarehouse->table.warehouseRowId", "$shipmentWarehouse->table.warehouseZoneId",
                        DB::raw("GROUP_CONCAT(CONCAT(" . $viewShipment->prefix . "warehouseRow.name,' '," . $viewShipment->prefix . "warehouseZone.name) SEPARATOR ',') as shipmentLocation"))
                    )
                    ->leftJoin("$viewShipmentDetails->table", "$viewShipment->table.id", "=", "$viewShipmentDetails->table.shipmentId")
                    ->leftJoin($shipmentWarehouse->table, "$shipmentWarehouse->table.shipmentId", '=', "$viewShipment->table.id")
                    ->leftJoin("$warehouselocation->table AS warehouseRow", "warehouseRow.id", '=', "$shipmentWarehouse->table.warehouseRowId")
                    ->leftJoin("$warehouselocation->table AS warehouseZone", "warehouseZone.id", '=', "$shipmentWarehouse->table.warehouseZoneId")
                    ->whereRaw($where)
                    ->groupBy("$viewShipment->table.id")
                    ->orderBy($param['field'], $param['type'])
                    ->paginate($param['searchDisplay']);
        } else if (!empty($param['searchShipment']['labelType']) && (!empty($param['searchShipment']['rows']) || !empty($param['searchShipment']['zones']))) {
            //echo 'd';exit;
            $resultSet = Viewshipment::select(array("$viewShipment->table.*", 'deliveryCompany',
                        DB::raw("GROUP_CONCAT(itemType) as itemType"), 'partucilarShipmentType',
                        "toCityName AS cityName", "toCountryName AS countryName", "toStateName AS stateName", "$shipmentWarehouse->table.warehouseRowId", "$shipmentWarehouse->table.warehouseZoneId",
                        DB::raw("GROUP_CONCAT(CONCAT(" . $viewShipment->prefix . "warehouseRow.name,' '," . $viewShipment->prefix . "warehouseZone.name) SEPARATOR ',') as shipmentLocation"))
                    )
                    ->leftJoin("$viewShipmentDetails->table", "$viewShipment->table.id", "=", "$viewShipmentDetails->table.shipmentId")
                    ->leftJoin($shipmentWarehouse->table, "$shipmentWarehouse->table.shipmentId", '=', "$viewShipment->table.id")
                    ->leftJoin("$warehouselocation->table AS warehouseRow", "warehouseRow.id", '=', "$shipmentWarehouse->table.warehouseRowId")
                    ->leftJoin("$warehouselocation->table AS warehouseZone", "warehouseZone.id", '=', "$shipmentWarehouse->table.warehouseZoneId")
                    ->leftJoin("$shipmentLabel->table", "$shipmentLabel->table.shipmentId", "=", "$viewShipment->table.id")
                    ->whereRaw($where)
                    ->groupBy("$viewShipment->table.id")
                    ->orderBy($param['field'], $param['type'])
                    ->paginate($param['searchDisplay']);
        } else if (!empty($param['searchShipment']['user']) || !empty($param['searchShipment']['receiver']) || (!empty($param['searchShipment']['declaredValueFrom']) && !empty($param['searchShipment']['declaredValueTo'])) || !empty($param['searchShipment']['shippingMethod']) || !empty($param['searchShipment']['store']) || !empty($param['searchShipment']['category']) || !empty($param['searchShipment']['subcategory']) || !empty($param['searchShipment']['product']) || !empty($param['searchShipment']['unitFrom']) && !empty($param['searchShipment']['unitTo'])) {
            if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($viewShipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $viewShipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($viewShipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($viewShipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }
            //echo 'e';exit;
            $resultSet = Viewshipment::select(array("$viewShipment->table.*", 'deliveryCompany',
                        DB::raw("GROUP_CONCAT(itemType) as itemType"), 'partucilarShipmentType',
                        "toCityName AS cityName", "toCountryName AS countryName", "toStateName AS stateName"
                    ))
                    ->leftJoin("$viewShipmentDetails->table", "$viewShipment->table.id", "=", "$viewShipmentDetails->table.shipmentId")
                    ->whereRaw($where)
                    ->groupBy("$viewShipment->table.id")
                    ->orderBy($param['field'], $param['type'])
                    ->paginate($param['searchDisplay']);
        } else if (!empty($param['searchShipment']['deliveryCompanyId']) || !empty($param['searchShipment']['dispatchCompanyId'])) {
            if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($shipmentMainTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $shipmentMainTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($shipmentMainTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);

                    $where .= "  AND date($shipmentMainTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }
            $resultSet = Shipment::select(array("$shipmentMain->table.*"))
                    ->leftJoin($shipmentDelivery->table, "$shipmentMain->table.id", "=", "$shipmentDelivery->table.shipmentId")
                    ->where("shipments.deleted", "0")->whereRaw($where)->groupBy("$shipmentMain->table.id")->orderBy("shipments.id", "desc")
                    ->paginate($param['searchDisplay']);
        } else {
            if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($shipmentMainTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $shipmentMainTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($shipmentMainTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);

                    $where .= "  AND date($shipmentMainTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }
            //DB::enableQueryLog();
            $resultSet = Shipment::where("shipments.deleted", "0")->whereRaw($where)->orderBy("shipments.id", "desc")->paginate($param['searchDisplay']);

            // dd(DB::getQueryLog());
        }


        //dd(DB::getQueryLog());        
        return $resultSet;
    }

    public static function getDeletedDelivery($id) {
        $viewShipment = new Viewshipment;
        $viewShipmentDetails = new ViewShipmentDetails;

        $viewShipmentTable = $viewShipment->prefix . $viewShipment->table;
        $viewShipmentDetailsTable = $viewShipmentDetails->prefix . $viewShipmentDetails->table;

        $resultSet = Viewshipment::select(array("deliveryId", "customerName", "deletedByEmail",
                    DB::raw("GROUP_CONCAT(itemName) as itemName, GROUP_CONCAT((CASE WHEN itemType='N' THEN 'Normal' WHEN itemType='H' THEN 'Hazmat' ELSE 'Secured' END)) AS itemType "),
                    DB::raw("SUM(itemPrice) as itemPrice, SUM(itemQuantity) as itemQuantity"),
                ))
                ->leftJoin("$viewShipmentDetails->table", "$viewShipment->table.id", "=", "$viewShipmentDetails->table.shipmentId")
                ->distinct("$viewShipmentTable.id")
                ->where("$viewShipmentDetails->table.shipmentId", $id)
                ->where("$viewShipmentDetails->table.deleted", 1)
                ->groupBy("deliveryId")
                ->get();

        return $resultSet;
    }

    /**
     * Method used to fetch warehouse list
     * @return object
     */
    public static function getWareHouseLocationList($id) {
        if (empty($id))
            return false;

        $shipment = new Shipment;
        $shipmentWarehouse = new Shipmentwarehouselocation;
        $warehouselocation = new Warehouselocation;
        $adminUser = new UserAdmin;


        $resultSet = Shipment::where("$shipment->table.id", $id)
                ->select(array("$shipmentWarehouse->table.id", "$shipmentWarehouse->table.warehouseRowId", "$shipmentWarehouse->table.warehouseZoneId", "warehouseRow.name AS rowName", "warehouseZone.name AS zoneName", "adminUser.firstName", "adminUser.lastName"))
                ->join($shipmentWarehouse->table, "$shipmentWarehouse->table.shipmentId", '=', "$shipment->table.id")
                ->join("$warehouselocation->table AS warehouseRow", "warehouseRow.id", '=', "$shipmentWarehouse->table.warehouseRowId")
                ->join("$warehouselocation->table AS warehouseZone", "warehouseZone.id", '=', "$shipmentWarehouse->table.warehouseZoneId")
                ->join("$adminUser->table AS adminUser", function($join) {
                    $join->on("adminUser.id", '=', "shipment_warehouse_locations.createdBy")
                    ->orWhere('adminUser.id', '=', "stmd_shipment_warehouse_locations.modifiedBy");
                })
                ->get();

        return $resultSet;
    }

    /**
     * Method used to fetch shipment data
     * @return object
     */
    public static function getShipmentHistory($id) {
        $tableName = Config::get('constants.dbTable.ACTIVITYLOG');
        $activityLog = DB::table($tableName)->where('log_name', 'warehouseLocation')->where('properties->attributes->shipmentId', $id)->limit(5)->orderby('id', 'desc')->get();

        $activityLogData = array();
        if (!empty($activityLog)) {
            foreach ($activityLog as $row) {
                $properties = json_decode($row->properties);
                if ($row->causer_type == 'App\Model\UserAdmin')
                    $user = UserAdmin::find($row->causer_id);
                else
                    $user = User::find($row->causer_id);

                $username = $user->email;

                if ($row->description == 'created') {
                    $warehouseRow = Warehouselocation::where('id', $properties->attributes->warehouseRowId)->first();
                    $warehouseZone = Warehouselocation::where('id', $properties->attributes->warehouseZoneId)->first();
                    if (!empty($warehouseRow) && !empty($warehouseZone))
                        $activityLogData[] = "Location  $warehouseRow->name $warehouseZone->name added. Added by $username on $row->created_at.";
                } else if ($row->description == 'deleted') {
                    $warehouseRow = Warehouselocation::where('id', $properties->attributes->warehouseRowId)->first();
                    $warehouseZone = Warehouselocation::where('id', $properties->attributes->warehouseZoneId)->first();

                    if (!empty($warehouseRow) && !empty($warehouseZone))
                        $activityLogData[] = "Location  $warehouseRow->name $warehouseZone->name deleted. Deleted by $username on $row->created_at.";
                } else {
                    $warehouseRowOld = Warehouselocation::where('id', $properties->old->warehouseRowId)->first();
                    $warehouseZoneOld = Warehouselocation::where('id', $properties->old->warehouseZoneId)->first();

                    $warehouseRow = Warehouselocation::where('id', $properties->attributes->warehouseRowId)->first();
                    $warehouseZone = Warehouselocation::where('id', $properties->attributes->warehouseZoneId)->first();
                    if (!empty($warehouseRow) && !empty($warehouseZone) && !empty($warehouseRowOld) && !empty($warehouseZoneOld))
                        $activityLogData[] = "Location  $warehouseRowOld->name $warehouseZoneOld->name updated to $warehouseRow->name $warehouseZone->name. Updated by $username on $row->created_at.";
                }
            }
        }

        return $activityLogData;
    }

    /**
     * Method used to fetch warehouse list
     * @return object
     */
    public static function getWarehouseList() {
        $country = new Country;
        $warehouse = new Warehouse;
        $city = new City();
        $citiesTable = $city->table;

        $resultSet = Warehouse::where("$warehouse->table.deleted", '0')
                ->where("$warehouse->table.status", '1')
                ->select(array("$warehouse->table.id", "$country->table.name AS countryName", DB::raw($city->prefix . $citiesTable . ".name as cityName")))
                ->leftJoin($country->table, "$warehouse->table.countryId", '=', "$country->table.id")
                ->leftJoin($citiesTable, $citiesTable . '.id', '=', $warehouse->table . '.cityId')
                ->orderBy("$country->table.name", "asc")
                ->get();

        return $resultSet;
    }

    public static function getWarehouseMappedList() {
        $country = new Country;
        $warehouse = new Warehouse;
        $result = array();

        $resultSet = Warehouse::where("$warehouse->table.deleted", '0')
                ->where("$warehouse->table.status", '1')
                ->select(array("$warehouse->table.id", "$country->table.name AS countryName"))
                ->leftJoin($country->table, "$warehouse->table.countryId", '=', "$country->table.id")
                ->orderBy("name", "asc")
                ->get();

        if (!empty($resultSet)) {
            foreach ($resultSet as $eachResult) {
                $result[$eachResult->id] = $eachResult->countryName;
            }
        }

        return $result;
    }

    /**
     * Method used to change State status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = User::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Shipment::where('id', $id)
                ->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * This function used <return> data Object used for excel export
     * @param type $param
     * @param type $selectedUser
     * @param type $selectedFields
     * @param type $shipmentType
     * @return type
     */
    public function exportData($param = array(), $selectedUser = array(), $selectedFields = array(), $shipmentType = 'S') {
        $where = '';
        $select = array();
        $viewShipment = new Viewshipment;
        $viewShipmentDetails = new ViewShipmentDetails;
        $shipmentDeliveries = new Shipmentdelivery;
        $shipmentsMain = new Shipment;
        $shipmentWarehouse = new Shipmentwarehouselocation;
        $warehouselocation = new Warehouselocation;


        $viewShipmentTable = $viewShipment->prefix . $viewShipment->table;
        $viewShipmentDetailsTable = $viewShipmentDetails->prefix . $viewShipmentDetails->table;
        $shipmentDeliveriesTable = $shipmentDeliveries->prefix . $shipmentDeliveries->table;
        $shipmentWarehouseTable = $viewShipment->prefix . $shipmentWarehouse->table;
        $warehouselocationTable = $viewShipment->prefix . $warehouselocation->table;

        $where = "$viewShipmentTable.deleted = '0'";

        if (!empty($param)) {
            if (!empty($param['searchShipment']['idFrom']))
                $where .= " AND $viewShipmentTable.id >= " . $param['searchShipment']['idFrom'];

            if (!empty($param['searchShipment']['idTo']))
                $where .= " AND $viewShipmentTable.id <= " . $param['searchShipment']['idTo'];

            if (!empty($param['searchShipment']['unitFrom']))
                $where .= " AND $viewShipmentTable.unit >= '" . $param['searchShipment']['unitFrom'] . "'";

            if (!empty($param['searchShipment']['unitTo']))
                $where .= " AND $viewShipmentTable.unit <= '" . $param['searchShipment']['unitFrom'] . "'";

            if (!empty($param['searchShipment']['deliveryCompanyId']))
                $where .= " AND $viewShipmentDetailsTable.deliveryCompanyId = " . $param['searchShipment']['deliveryCompanyId'];
            if (!empty($param['searchShipment']['dispatchCompanyId']))
                $where .= " AND $viewShipmentDetailsTable.dispatchCompanyId = " . $param['searchShipment']['dispatchCompanyId'];

            if (!empty($param['searchShipment']['prepaid']))
                $where .= " AND $viewShipmentTable.prepaid = '" . $param['searchShipment']['prepaid'] . "'";

            if (!empty($param['searchShipment']['shipmentStatus']))
                $where .= " AND $viewShipmentTable.shipmentStatus = '" . $param['searchShipment']['shipmentStatus'] . "'";

            if (!empty($param['searchShipment']['paymentStatus']))
                $where .= " AND $viewShipmentTable.paymentStatus = '" . $param['searchShipment']['paymentStatus'] . "'";

            if (!empty($param['searchShipment']['shipmentType']))
                $where .= " AND $viewShipmentTable.shipmentType = '" . $param['searchShipment']['shipmentType'] . "'";

            if (!empty($param['searchShipment']['totalFrom']))
                $where .= " AND $viewShipmentTable.total >= '" . $param['searchShipment']['totalFrom'] . "'";

            if (!empty($param['searchShipment']['totalTo']))
                $where .= " AND $viewShipmentTable.total <= '" . $param['searchShipment']['totalTo'] . "'";

            if (!empty($param['searchShipment']['user']))
                $where .= " AND ($viewShipmentTable.customerName LIKE '%" . $param['searchShipment']['user'] . "%' OR $viewShipmentTable.customerEmail LIKE '%" . $param['searchShipment']['user'] . "%')";

            if (!empty($param['searchShipment']['receiver']))
                $where .= " AND ($viewShipmentTable.toName LIKE '%" . $param['searchShipment']['receiver'] . "%' OR $viewShipmentTable.toEmail LIKE '%" . $param['searchShipment']['receiver'] . "%')";

            if (!empty($param['searchShipment']['warehouseId']))
                $where .= " AND $viewShipmentTable.warehouseId = '" . $param['searchShipment']['warehouseId'] . "'";

            /* if (!empty($param['searchShipment']['labelType']))
              $where .= " AND $shipmentLabelTable.labelType = '" . $param['searchShipment']['labelType'] . "'"; */

            if (!empty($param['searchShipment']['itemType']))
                $where .= " AND $viewShipmentDetailsTable.itemType = '" . $param['searchShipment']['itemType'] . "'";

            if (!empty($param['searchShipment']['declaredValueFrom']))
                $where .= " AND $viewShipmentDetailsTable.declaredValue >= '" . $param['searchShipment']['declaredValueFrom'] . "'";

            if (!empty($param['searchShipment']['declaredValueTo']))
                $where .= " AND $viewShipmentDetailsTable.declaredValue <= '" . $param['searchShipment']['declaredValueTo'] . "'";

            if (isset($param['searchShipment']['packed']) && !empty($param['searchShipment']['packed'])) {
                $where .= " AND $viewShipmentTable.packed = '" . $param['searchShipment']['packed'] . "'";
            }
            if (isset($param['searchByshipment']) && !empty($param['searchByshipment'])) {
                $where .= " AND $viewShipmentTable.id =" . $param['searchByshipment'][0];
            }

            if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($viewShipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $viewShipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($viewShipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($viewShipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }
        }

        if (!empty($selectedUser)) {
            $where = "$viewShipmentTable.id IN (" . implode(',', $selectedUser) . ")";
        }

        if (!empty($selectedFields)) {
            foreach ($selectedFields as $eachSearchfield) {
                $select[] = DB::raw("$viewShipmentTable.id as 'ID'");


                if ($eachSearchfield == 'paymentMethod')
                    $select[] = DB::raw("paymentMethod as 'paymentMethod'");
                if ($eachSearchfield == 'paymentDate')
                    $select[] = DB::raw("paymentReceivedOn as 'paymentReceivedOn'");


                if ($eachSearchfield == 'warehouse')
                    $select[] = DB::raw("$viewShipmentTable.warehouseCountryName as Warehouse");
                if ($eachSearchfield == 'unit')
                    $select[] = DB::raw("$viewShipmentTable.unit as Unit");
                if ($eachSearchfield == 'name')
                    $select[] = DB::raw("$viewShipmentTable.customerName as 'customerName'");
                if ($eachSearchfield == 'email')
                    $select[] = DB::raw("$viewShipmentTable.customerEmail as 'customerEmail'");
                if ($eachSearchfield == 'number')
                    $select[] = DB::raw("$viewShipmentTable.toPhone as 'toPhone'");
                if ($eachSearchfield == 'status') {
                    $select[] = DB::raw("case shipmentStatus
                                    when '0' then 'Shipment Submiited'
                                    when '1' then 'In Warehouse'
                                    when '2' then 'In Transit'
                                    when '3' then 'Customs clearing'
                                    when '4' then 'In destination warehouse'
                                    when '5' then 'Out for delivery'
                                    when '6' then 'Delivered'
                                end as 'shipmentStatus'");
                }
                if ($eachSearchfield == 'type') {
                    $select[] = DB::raw("case shipmentType 
                                    when 'shopforme' then 'Shop For Me'
                                    when 'autopart' then 'Auto Parts'
                                    when 'othershipment' then 'Other Shipment'
                                end as 'shipmentType'");
                }
                if ($eachSearchfield == 'paymentstatus') {
                    $select[] = DB::raw("case paymentStatus
                                    when 'unpaid' then 'Not Paid'
                                    when 'paid' then 'Paid'
                                end as 'paymentStatus'");
                }
                if ($eachSearchfield == 'countryName')
                    $select[] = DB::raw("toCountryName as 'countryName'");
                if ($eachSearchfield == 'stateName')
                    $select[] = DB::raw("toStateName as 'stateName'");
                if ($eachSearchfield == 'cityName')
                    $select[] = DB::raw("toCityName as 'cityName'");

                if ($eachSearchfield == 'shippingCost')
                    $select[] = DB::raw("totalShippingCost as 'totalShippingCost'");
                if ($eachSearchfield == 'clearing')
                    $select[] = DB::raw("totalClearingDuty as 'totalClearingDuty'");
                if ($eachSearchfield == 'storage')
                    $select[] = DB::raw("$viewShipmentTable.storageCharge as 'storage'");

                if ($eachSearchfield == 'otherCost')
                    $select[] = DB::raw("totalOtherCharges as 'totalOtherCharges'");
                if ($eachSearchfield == 'tax')
                    $select[] = DB::raw("totalTax as 'tax'");
                if ($eachSearchfield == 'discount')
                    $select[] = DB::raw("totalDiscount as 'discount'");

                if ($eachSearchfield == 'packed')
                    $select[] = DB::raw("case packed
                                    when 'not_started' then 'Not Started'
                                    when 'started' then 'Started'
                                    when 'packing_complete' then 'Packing Complete'
                                end as 'packageStatus'");

                if ($eachSearchfield == 'totalCost')
                    $select[] = DB::raw("$viewShipmentTable.totalCost as 'totalCost'");

                if ($eachSearchfield == 'location')
                    $select[] = DB::raw("GROUP_CONCAT(CONCAT(" . $viewShipment->prefix . "warehouseRow.name,' '," . $viewShipment->prefix . "warehouseZone.name) SEPARATOR ',') as 'shipmentLocation'");

                //
            }
        }

        $resultSet = Viewshipment::select($select)
                        ->leftJoin("$viewShipmentDetails->table", "$viewShipment->table.id", "=", "$viewShipmentDetails->table.shipmentId")
                        ->leftJoin($shipmentWarehouse->table, "$shipmentWarehouse->table.shipmentId", '=', "$viewShipment->table.id")
                        ->leftJoin("$warehouselocation->table AS warehouseRow", "warehouseRow.id", '=', "$shipmentWarehouse->table.warehouseRowId")
                        ->leftJoin("$warehouselocation->table AS warehouseZone", "warehouseZone.id", '=', "$shipmentWarehouse->table.warehouseZoneId")
                        ->whereRaw($where)
                        ->groupBy("$viewShipment->table.id")->get();

        $data = array();
        foreach ($resultSet as $key => $val) {
            foreach ($selectedFields as $eachSearchfield) {
                if ($eachSearchfield != 'id')
                    $data['shipments'][$key]['ID'] = $val->ID;
                if ($eachSearchfield == 'warehouse')
                    $data['shipments'][$key]['Warehouse'] = $val->Warehouse;
                if ($eachSearchfield == 'unit')
                    $data['shipments'][$key]['Unit'] = $val->Unit;
                if ($eachSearchfield == 'name')
                    $data['shipments'][$key]['Customer Name'] = $val->customerName;
                if ($eachSearchfield == 'email')
                    $data['shipments'][$key]['Email Address'] = $val->customerEmail;
                if ($eachSearchfield == 'number')
                    $data['shipments'][$key]['Contact Number'] = $val->toPhone;
                if ($eachSearchfield == 'countryName')
                    $data['shipments'][$key]['Destination Country'] = $val->countryName;
                if ($eachSearchfield == 'stateName')
                    $data['shipments'][$key]['Destination State'] = $val->stateName;
                if ($eachSearchfield == 'cityName')
                    $data['shipments'][$key]['Destination City'] = $val->cityName;
                if ($eachSearchfield == 'type')
                    $data['shipments'][$key]['Shipment Type'] = $val->shipmentType;
                if ($eachSearchfield == 'status')
                    $data['shipments'][$key]['Shipment Status'] = $val->shipmentStatus;
                if ($eachSearchfield == 'paymentMethod')
                    $data['shipments'][$key]['Payment Method'] = $val->paymentMethod;
                if ($eachSearchfield == 'paymentDate')
                    $data['shipments'][$key]['Payment Date'] = $val->paymentReceivedOn;
                if ($eachSearchfield == 'paymentstatus')
                    $data['shipments'][$key]['Payment Status'] = $val->paymentStatus;
                if ($eachSearchfield == 'shippingCost')
                    $data['shipments'][$key]['Shipping Cost'] = $val->totalShippingCost;
                if ($eachSearchfield == 'clearing')
                    $data['shipments'][$key]['Clearing and Port Handling Charge'] = $val->totalClearingDuty;
                if ($eachSearchfield == 'storage')
                    $data['shipments'][$key]['Storage Charge'] = $val->storage;
                if ($eachSearchfield == 'inventory')
                    $data['shipments'][$key]['Inventory Charge'] = Shipmentdelivery::where("shipmentId", $val->ID)->sum('inventoryCharge');
                if ($eachSearchfield == 'otherCost')
                    $data['shipments'][$key]['Other Cost'] = $val->totalOtherCharges;
                if ($eachSearchfield == 'tax')
                    $data['shipments'][$key]['VAT'] = $val->tax;
                if ($eachSearchfield == 'discount')
                    $data['shipments'][$key]['Discount'] = $val->discount;
                if ($eachSearchfield == 'packed')
                    $data['shipments'][$key]['Packaging Status'] = $val->packageStatus;
                if ($eachSearchfield == 'totalCost')
                    $data['shipments'][$key]['Total Cost'] = $val->totalCost;
                if ($eachSearchfield == 'location')
                    $data['shipments'][$key]['Shipment Location'] = $val->shipmentLocation;
            }
        }

        //print_r($data); die;

        return $data;
    }

    /**
     * Method to fetch delivery details report (for export under report)
     * @param array $records
     * @param string $need
     * @return array
     */
    public static function exportDataDeliveryReport($records, $need) {
        $where = '';
        $select = array();
        $viewShipment = new Viewshipment;
        $viewShipmentDetails = new ViewShipmentDetails;

        $shiplog = new Shipmentstatuslog;
        $shippingMethod = new Shippingmethods;


        $viewShipmentTable = $viewShipment->prefix . $viewShipment->table;
        $viewShipmentDetailsTable = $viewShipmentDetails->prefix . $viewShipmentDetails->table;
        $shippingMethodTable = $shippingMethod->prefix . $shippingMethod->table;
        $data = array();

        foreach ($records as $key => $record) {
            if ($need != 'selected') {
                $rec = ViewShipmentDetails::where("shipmentId", $record['ID'])->where('deleted', '0')
                                ->where('packageDeleted', '0')->where('packageType', 'I')->groupby('deliveryId')->get()->toArray();
            } else {
                $rec = ViewShipmentDetails::where("shipmentId", $record)->where('deleted', '0')
                                ->where('packageDeleted', '0')->where('packageType', 'I')->groupby('deliveryId')->get()->toArray();
            }
            $cnt = 1;
            foreach ($rec as $key1 => $val1) {

                $queryGetDuplicateDeliveryId = ViewShipmentDetails::where('deliveryId', $val1["deliveryId"])->orderby('deliveryId', 'asc')->get()->toArray();
                $p = '';
                if (count($queryGetDuplicateDeliveryId) > 1) {
                    foreach ($queryGetDuplicateDeliveryId as $key1 => $val1) {
                        $data['delivery'][$key][$key1]['Shipment ID'] = $val1['shipmentId'];
                        $data['delivery'][$key][$key1]['Delivery ID'] = "Delivery " . $cnt;
                        $items = ViewShipmentDetails::select(['itemName'])->where('deliveryId', $val1["deliveryId"])->get()->toArray();
                        foreach ($items as $valitem) {
                            $p .= $valitem['itemName'] . ", ";
                        }
                        $data['delivery'][$key][$key1]['Items'] = rtrim($p, ', ');
                        /* GET SHIPIING METHOD NAME */
                        if (!empty($val1['shippingMethodId'])) {
                            $shippingmethodName = Shippingmethods::select(['shipping'])->where('shippingid', $val1['shippingMethodId'])->first()->toArray();
                            $data['delivery'][$key][$key1]['Shipping Method'] = $shippingmethodName['shipping'];
                        } else {
                            $data['delivery'][$key][$key1]['Shipping Method'] = $val1['shippingMethodId'];
                        }
                        $data['delivery'][$key][$key1]['Snapshot'] = $val1['snapshot'];
                        $data['delivery'][$key][$key1]['Recount'] = $val1['recount'];
                        $data['delivery'][$key][$key1]['Reweight'] = $val1['reweigh'];
                        $data['delivery'][$key][$key1]['Chargeable Weight'] = $val1['deliveryChargeableWeight'];
                        /* GET SHIPMENT STATUS */
                        $ship = Shipmentstatuslog::select(['status'])->where('shipmentId', $val1['shipmentId'])->orderby("id", 'desc')->take(1)->get()->toArray();
                        if (!empty($ship)) {

                            switch ($ship[0]['status']) {
                                case "in_warehouse":
                                    $shipTxt = "In Warehouse";
                                    break;
                                case "in_transit":
                                    $shipTxt = "In Transit";
                                    break;
                                case "custom_clearing":
                                    $shipTxt = "Custom Clearing";
                                    break;
                                case "destination_warehouse":
                                    $shipTxt = "Destination Warehouse";
                                    break;
                                case "out_for_delivery":
                                    $shipTxt = "Out For Delivery";
                                    break;
                                case "delivered":
                                    $shipTxt = "Delivered";
                                    break;
                                default:
                                    $shipTxt = $ship[0]['status'];
                            }
                            $data['delivery'][$key][$key1]['Status'] = $shipTxt;
                        } else {
                            $data['delivery'][$key][$key1]['Status'] = '';
                        }
                    }
                } else {
                    $data['delivery'][$key][$key1]['Shipment ID'] = $val1['shipmentId'];
                    $data['delivery'][$key][$key1]['Delivery ID'] = "Delivery " . $cnt;
                    $data['delivery'][$key][$key1]['Items'] = $val1['itemName'];
                    /* GET SHIPIING METHOD NAME */
                    if (!empty($val1['shippingMethodId'])) {
                        $shippingmethodName = Shippingmethods::select(['shipping'])->where('shippingid', $val1['shippingMethodId'])->first()->toArray();
                        $data['delivery'][$key][$key1]['Shipping Method'] = $shippingmethodName['shipping'];
                    } else {
                        $data['delivery'][$key][$key1]['Shipping Method'] = $val1['shippingMethodId'];
                    }
                    $data['delivery'][$key][$key1]['Snapshot'] = $val1['snapshot'];
                    $data['delivery'][$key][$key1]['Recount'] = $val1['recount'];
                    $data['delivery'][$key][$key1]['Reweight'] = $val1['reweigh'];
                    $data['delivery'][$key][$key1]['Chargeable Weight'] = $val1['deliveryChargeableWeight'];
                    /* GET SHIPMENT STATUS */
                    $ship = Shipmentstatuslog::select(['status'])->where('shipmentId', $val1['shipmentId'])->orderby("id", 'desc')->take(1)->get()->toArray();
                    if (!empty($ship)) {
                        switch ($ship[0]['status']) {
                            case "in_warehouse":
                                $shipTxt = "In Warehouse";
                                break;
                            case "in_transit":
                                $shipTxt = "In Transit";
                                break;
                            case "custom_clearing":
                                $shipTxt = "Custom Clearing";
                                break;
                            case "destination_warehouse":
                                $shipTxt = "Destination Warehouse";
                                break;
                            case "out_for_delivery":
                                $shipTxt = "Out For Delivery";
                                break;
                            case "delivered":
                                $shipTxt = "Delivered";
                                break;
                            default:
                                $shipTxt = $ship[0]['status'];
                        }
                        $data['delivery'][$key][$key1]['Status'] = $shipTxt;
                    } else {
                        $data['delivery'][$key][$key1]['Status'] = '';
                    }
                }
                $cnt++;
            }
        }
        return $data;
    }

    /**
     * Method to fetch delivery details data
     * @param array $data
     * @return array
     */
    public static function getDeliveryDetails($data) {
        if (empty($data))
            return false;

        $deliveryData = [];
        $deliveryData['deliveries'] = [];

        $member = array();

        $totalPackage = $totalWeight = $totalChargeableWeight = $totalValue = $totalDelivery = 0;
        $itemTypeList = '';

        foreach ($data as $row) {
            if (!array_key_exists($row['deliveryId'], $deliveryData['deliveries'])) {
                $totalWeight = $totalWeight + $row['deliveryWeight'];
                $totalChargeableWeight = $totalChargeableWeight + $row['deliveryChargeableWeight'];

                $deliveryData['deliveries'][$row['deliveryId']]['id'] = $row['deliveryId'];
                $deliveryData['deliveries'][$row['deliveryId']]['length'] = $row['deliveryLength'];
                $deliveryData['deliveries'][$row['deliveryId']]['width'] = $row['deliveryWidth'];
                $deliveryData['deliveries'][$row['deliveryId']]['height'] = $row['deliveryHeight'];
                $deliveryData['deliveries'][$row['deliveryId']]['weight'] = $row['deliveryWeight'];
                $deliveryData['deliveries'][$row['deliveryId']]['chargeableWeight'] = $row['deliveryChargeableWeight'];
                $deliveryData['deliveries'][$row['deliveryId']]['totalItemCost'] = $row['totalItemCost'];
                $deliveryData['deliveries'][$row['deliveryId']]['shippingCost'] = $row['shippingCost'];
                $deliveryData['deliveries'][$row['deliveryId']]['clearingDutyCost'] = $row['clearingDutyCost'];
                $deliveryData['deliveries'][$row['deliveryId']]['isDutyCharged'] = $row['isDutyCharged'];
                $deliveryData['deliveries'][$row['deliveryId']]['inventoryCharge'] = $row['inventoryCharge'];
                $deliveryData['deliveries'][$row['deliveryId']]['otherChargeCost'] = $row['otherChargeCost'];
                $deliveryData['deliveries'][$row['deliveryId']]['totalCost'] = $row['totalCost'];
                $deliveryData['deliveries'][$row['deliveryId']]['snapshot'] = $row['snapshot'];
                $deliveryData['deliveries'][$row['deliveryId']]['snapshotRequestedOn'] = $row['snapshotRequestedOn'];
                $deliveryData['deliveries'][$row['deliveryId']]['recount'] = $row['recount'];
                $deliveryData['deliveries'][$row['deliveryId']]['recountRequestedOn'] = $row['recountRequestedOn'];
                $deliveryData['deliveries'][$row['deliveryId']]['reweigh'] = $row['reweigh'];
                $deliveryData['deliveries'][$row['deliveryId']]['reweighRequestedOn'] = $row['reweighRequestedOn'];
                $deliveryData['deliveries'][$row['deliveryId']]['received'] = $row['received'];
                $deliveryData['deliveries'][$row['deliveryId']]['deliveryAddedBy'] = $row['deliveryAddedBy'];
                $deliveryData['deliveries'][$row['deliveryId']]['wrongInventory'] = $row['wrongInventory'];
                $deliveryData['deliveries'][$row['deliveryId']]['shippingMethodId'] = $row['shippingMethodId'];
                $deliveryData['deliveries'][$row['deliveryId']]['deliveryAllItemReturned'] = Shipmentpackage::allItemReturned(0, $row['deliveryId']);
                $deliveryData['deliveries'][$row['deliveryId']]['deliveryAllItemDeleted'] = Shipmentpackage::allItemDeleted(0, $row['deliveryId']);
                $deliveryData['deliveries'][$row['deliveryId']]['maxStorageDate'] = $row['maxStorageDate'];
                $deliveryData['deliveries'][$row['deliveryId']]['storageCharge'] = $row['storageCharge'];
                $deliveryData['deliveries'][$row['deliveryId']]['createdOn'] = $row['deliveryCreatedOn'];
                if(isset($row['memberId']))
                {
                    if(!empty($row['memberId']) || $row['memberId']!="NULL")
                    {
                        $member =  User::selectRaw("CONCAT(firstName, ' ', lastName) AS memberName, unit")->where('id', $row['memberId'])->first()->toArray();

                        $deliveryData['deliveries'][$row['deliveryId']]['memberName'] = $member['memberName'];
                        $deliveryData['deliveries'][$row['deliveryId']]['unit'] = $member['unit'];
                    }

                }
                
                $deliveryStorageCharge = '0.00';
                $maxStorageDateRaw = isset($row['maxStorageDate']) ? $row['maxStorageDate'] : '';
                $maxStorageDate = Carbon::parse($maxStorageDateRaw);
                $now = Carbon::now();
                $diff = $maxStorageDate->diffInDays($now, FALSE);
                if ($diff > 0 && !empty($row['storageCharge'])) {
                    $deliveryStorageCharge = $diff * $row['storageCharge'];
                    $deliveryStorageCharge = $deliveryStorageCharge;
                }
                $deliveryData['deliveries'][$row['deliveryId']]['deliveryStorageCharge'] = customhelper::getCurrencySymbolFormat(round($deliveryStorageCharge, 2));
                $totalDelivery++;
            }
            if (!isset($deliveryData['deliveries'][$row['deliveryId']]['totalQty']))
                $deliveryData['deliveries'][$row['deliveryId']]['totalQty'] = 0;
            $deliveryData['deliveries'][$row['deliveryId']]['totalQty'] = $deliveryData['deliveries'][$row['deliveryId']]['totalQty'] + $row['itemQuantity'];
            $totalPackage = $totalPackage + $row['itemQuantity'];
            if($row['packageDeleted'] == '0')
                $totalValue += $row['itemTotalCost'];
            $itemTypeList .= $row['itemType'] . ',';

            $deliveryData['deliveries'][$row['deliveryId']]['packages'][] = array(
                'id' => $row['packageId'],
                'siteCategoryId' => $row['itemCategoryId'],
                'categoryName' => $row['itemCategoryName'],
                'siteSubCategoryId' => $row['itemSubCategoryId'],
                'subcategoryName' => $row['itemSubCategoryName'],
                'siteProductId' => $row['itemProductId'],
                'productName' => $row['itemProductName'],
                'storeId' => $row['storeId'],
                'storeName' => $row['storeName'],
                'itemName' => $row['itemName'],
                'websiteUrl' => $row['itemWebsiteUrl'],
                'weight' => $row['itemWeight'],
                'options' => $row['itemOptions'],
                'itemDescription' => $row['itemDescription'],
                'itemMake' => $row['itemMake'],
                'itemModel' => $row['itemModel'],
                'note' => $row['itemNote'],
                'snapshotImage' => $row['itemSnapshotImage'],
                'snapshotOpt' => $row['snapshotOpt'],
                'itemType' => $row['itemType'],
                'itemQuantity' => $row['itemQuantity'],
                'itemMinPrice' => $row['itemMinPrice'],
                'itemPrice' => $row['itemPrice'],
                'itemShippingCost' => $row['itemShippingCost'],
                'itemTotalCost' => $row['itemTotalCost'],
                'type' => $row['packageType'],
                'deliveryCompany' => $row['deliveryCompany'],
                'deliveredOn' => $row['deliveredOn'],
                'tracking' => $row['tracking'],
                'trackingLock' => $row['trackingLock'],
                'tracking2' => $row['tracking2'],
                'deliveryNotes' => $row['deliveryNotes'],
                'itemDiscountedInvoiceFile' => $row['itemDiscountedInvoiceFile'],
                'itemReturn' => $row['itemReturn'],
                'returnLabel' => $row['returnLabel'],
                'packageDeletedBy' => !empty($row['packageDeletedBy'])?$row['packageDeletedBy']:0,
                'packageDeleted' => $row['packageDeleted'],
            );
        }

        $deliveryData['totalPackage'] = $totalPackage;
        $deliveryData['totalWeight'] = $totalWeight;
        $deliveryData['totalDelivery'] = $totalDelivery;
        $deliveryData['totalChargeableWeight'] = $totalChargeableWeight;
        $deliveryData['totalValue'] = $totalValue;
        $deliveryData['itemType'] = rtrim($itemTypeList, ',');

        return $deliveryData;
    }

    public static function getDeliveryDetailsForApi($data) {
        if (empty($data))
            return false;

        $deliveryData = [];
        $deliveryData['deliveries'] = [];
        $uniqueDelivery = array();
        $totalPackage = $totalWeight = $totalChargeableWeight = $totalValue = $totalDelivery = 0;
        $itemTypeList = '';
        $index = 0;
        foreach ($data as $row) {

            if (!in_array($row['deliveryId'], $uniqueDelivery)) {
                $index++;
                $deliveryIndex = $index - 1;
                $uniqueDelivery[] = $row['deliveryId'];
                $totalWeight = $totalWeight + $row['deliveryWeight'];
                $totalChargeableWeight = $totalChargeableWeight + $row['deliveryChargeableWeight'];

                $deliveryData['deliveries'][$deliveryIndex]['id'] = $index;
                $deliveryData['deliveries'][$deliveryIndex]['deliveryid'] = $row['deliveryId'];
                $deliveryData['deliveries'][$deliveryIndex]['length'] = $row['deliveryLength'];
                $deliveryData['deliveries'][$deliveryIndex]['width'] = $row['deliveryWidth'];
                $deliveryData['deliveries'][$deliveryIndex]['height'] = $row['deliveryHeight'];
                $deliveryData['deliveries'][$deliveryIndex]['weight'] = $row['deliveryWeight'];
                $deliveryData['deliveries'][$deliveryIndex]['chargeableWeight'] = $row['deliveryChargeableWeight'];
                $deliveryData['deliveries'][$deliveryIndex]['totalItemCost'] = $row['totalItemCost'];
                $deliveryData['deliveries'][$deliveryIndex]['shippingCost'] = $row['shippingCost'];
                $deliveryData['deliveries'][$deliveryIndex]['clearingDutyCost'] = $row['clearingDutyCost'];
                $deliveryData['deliveries'][$deliveryIndex]['isDutyCharged'] = $row['isDutyCharged'];
                $deliveryData['deliveries'][$deliveryIndex]['inventoryCharge'] = $row['inventoryCharge'];
                $deliveryData['deliveries'][$deliveryIndex]['otherChargeCost'] = $row['otherChargeCost'];
                $deliveryData['deliveries'][$deliveryIndex]['totalCost'] = $row['totalCost'];
                $deliveryData['deliveries'][$deliveryIndex]['snapshot'] = $row['snapshot'];
                $deliveryData['deliveries'][$deliveryIndex]['snapshotRequestedOn'] = $row['snapshotRequestedOn'];
                $deliveryData['deliveries'][$deliveryIndex]['recount'] = $row['recount'];
                $deliveryData['deliveries'][$deliveryIndex]['recountRequestedOn'] = $row['recountRequestedOn'];
                $deliveryData['deliveries'][$deliveryIndex]['reweigh'] = $row['reweigh'];
                $deliveryData['deliveries'][$deliveryIndex]['reweighRequestedOn'] = $row['reweighRequestedOn'];
                $deliveryData['deliveries'][$deliveryIndex]['received'] = $row['received'];
                $deliveryData['deliveries'][$deliveryIndex]['deliveryAddedBy'] = $row['deliveryAddedBy'];
                $deliveryData['deliveries'][$deliveryIndex]['wrongInventory'] = $row['wrongInventory'];
                $deliveryData['deliveries'][$deliveryIndex]['shippingMethodId'] = $row['shippingMethodId'];
                $totalDelivery++;
            }
            $deliveryData['deliveries'][$deliveryIndex]['packages'][] = array(
                'id' => $row['packageId'],
                'siteCategoryId' => $row['itemCategoryId'],
                'categoryName' => $row['itemCategoryName'],
                'siteSubCategoryId' => $row['itemSubCategoryId'],
                'subcategoryName' => $row['itemSubCategoryName'],
                'siteProductId' => $row['itemProductId'],
                'productName' => $row['itemProductName'],
                'storeId' => $row['storeId'],
                'storeName' => $row['storeName'],
                'itemName' => $row['itemName'],
                'websiteUrl' => $row['itemWebsiteUrl'],
                'weight' => $row['itemWeight'],
                'options' => $row['itemOptions'],
                'itemDescription' => $row['itemDescription'],
                'itemMake' => $row['itemMake'],
                'itemModel' => $row['itemModel'],
                'note' => $row['itemNote'],
                'snapshotImage' => $row['itemSnapshotImage'],
                'snapshotOpt' => $row['snapshotOpt'],
                'itemType' => $row['itemType'],
                'itemQuantity' => $row['itemQuantity'],
                'itemPrice' => $row['itemPrice'],
                'itemShippingCost' => $row['itemShippingCost'],
                'itemTotalCost' => $row['itemTotalCost'],
                'type' => $row['packageType'],
                'deliveryCompany' => $row['deliveryCompany'],
                'deliveredOn' => $row['deliveredOn'],
                'tracking' => $row['tracking'],
                'trackingLock' => $row['trackingLock'],
                'tracking2' => $row['tracking2'],
                'deliveryNotes' => $row['deliveryNotes'],
            );
            /* if(!isset($deliveryData['deliveries'][$deliveryIndex]['totalQty']))
              $deliveryData['deliveries'][$deliveryIndex]['totalQty'] = 0;
              $deliveryData['deliveries'][$deliveryIndex]['totalQty'] = $deliveryData['deliveries'][$deliveryIndex]['totalQty'] + $row['itemQuantity']; */
            $totalPackage = $totalPackage + $row['itemQuantity'];
            $totalValue += $row['itemTotalCost'];
            $itemTypeList .= $row['itemType'] . ',';

            if (!in_array($row['deliveryId'], $uniqueDelivery)) {
                
            }
        }

        /* $deliveryData['totalPackage'] = $totalPackage;
          $deliveryData['totalWeight'] = $totalWeight;
          $deliveryData['totalDelivery'] = $totalDelivery;
          $deliveryData['totalChargeableWeight'] = $totalChargeableWeight;
          $deliveryData['totalValue'] = $totalValue;
          $deliveryData['itemType'] = rtrim($itemTypeList, ','); */

        return $deliveryData;
    }

    public static function getImages($user) {
        $procurement = new Procurement;
        $procurementitem = new Procurementitem;

        $procurementTable = $procurement->prefix . $procurement->table;
        $procurementitemTable = $procurement->prefix . $procurementitem->table;



        $where = "stmd_procurement_shipments.userId = " . $user . "";



        $resultSet = Procurement::select(array("$procurementTable.id as shipmentId", "$procurementitemTable.id", "$procurementitemTable.itemImage", "$procurementTable.procurementType as type"))
                ->leftjoin($procurementitemTable, "$procurementTable.id", '=', "$procurementitemTable.procurementId")
                ->whereRaw($where)
                ->whereNotNull('procurement_items.itemImage')
                ->get();

        return $resultSet;
    }

    public static function getAutoImages($user) {
        $autoshipment = new Autoshipment;
        $autoshipmentitem = new Autoshipmentitemimage;

        $autoshipmentTable = $autoshipment->prefix . $autoshipment->table;
        $autoshipmentitemTable = $autoshipment->prefix . $autoshipmentitem->table;



        $where = "$autoshipmentTable.userId = " . $user . "";

        $selectVal = "Select 'shipacar'";


        $resultSet = Autoshipment::select(array("$autoshipment->table.id as shipmentId", "$autoshipmentitem->table.id", "$autoshipmentitem->table.filename"))->selectSub($selectVal, 'type')
                ->leftjoin($autoshipmentitem->table, "$autoshipment->table.id", '=', "$autoshipmentitem->table.autoShipmentId")
                ->whereRaw($where)
                ->whereNotNull("$autoshipmentitem->table.filename")
                ->get();

        return $resultSet;
    }

    public static function getImageById($user, $itemId, $shipmentId) {
        $procurement = new Procurement;
        $procurementitem = new Procurementitem;

        $procurementTable = $procurement->prefix . $procurement->table;
        $procurementitemTable = $procurement->prefix . $procurementitem->table;



        $where = "stmd_procurement_shipments.userId = " . $user . " AND stmd_procurement_items.id =" . $itemId . " AND stmd_procurement_items.procurementId = " . $shipmentId . "";



        $resultSet = Procurement::select(array("$procurementTable.id as shipmentId", "$procurementitemTable.id", "$procurementitemTable.itemImage", "$procurementTable.procurementType as type"))
                ->leftjoin($procurementitemTable, "$procurementTable.id", '=', "$procurementitemTable.procurementId")
                ->whereRaw($where)
                ->whereNotNull('procurement_items.itemImage')
                ->get();

        return $resultSet;
    }

    public static function getAutoImageById($user, $itemId, $shipmentId) {
        $autoshipment = new Autoshipment;
        $autoshipmentitem = new Autoshipmentitemimage;

        $autoshipmentTable = $autoshipment->prefix . $autoshipment->table;
        $autoshipmentitemTable = $autoshipment->prefix . $autoshipmentitem->table;



        $where = "$autoshipmentTable.userId = " . $user . " AND $autoshipmentitemTable.id = " . $itemId . " AND $autoshipmentitemTable.autoShipmentId =" . $shipmentId . "";

        $selectVal = "Select 'shipacar'";


        $resultSet = Autoshipment::select(array("$autoshipment->table.id as shipmentId", "$autoshipmentitem->table.id", "$autoshipmentitem->table.filename"))->selectSub($selectVal, 'type')
                ->leftjoin($autoshipmentitem->table, "$autoshipment->table.id", '=', "$autoshipmentitem->table.autoShipmentId")
                ->whereRaw($where)
                ->whereNotNull("$autoshipmentitem->table.filename")
                ->get();

        return $resultSet;
    }

    public static function statusWiseStatistics() {

        $shipmentObj = new Shipment;
        $shipmentStatsData = DB::table($shipmentObj->table)
                ->select('shipmentStatus', DB::raw('count(id) as statusStats'))
                ->groupBy('shipmentStatus')
                ->where('deleted', '0')
                ->get();
        $resultData = array();
        if (!empty($shipmentStatsData)) {
            foreach ($shipmentStatsData as $eachStatsData) {
                $resultData[$eachStatsData->shipmentStatus] = $eachStatsData->statusStats;
            }
        }

        return $resultData;
    }

    /**
     * This function used export data for special offer report generation
     * @param type $param
     * @param type $selectedUser
     * @param type $selectedFields
     * @param type $shipmentType
     * @return type
     */
    public function exportDataSpecialOfferReport($param = array(), $selectedUser = array(), $selectedFields = array(), $shipmentType = 'S') {
        $where = '';
        $select = array();
        $viewShipment = new Viewshipment;
        $viewShipmentDetails = new ViewShipmentDetails;
        $shipmentDeliveries = new Shipmentdelivery;
        $shipmentsMain = new Shipment;


        $viewShipmentTable = $viewShipment->prefix . $viewShipment->table;
        $viewShipmentDetailsTable = $viewShipmentDetails->prefix . $viewShipmentDetails->table;
        $shipmentDeliveriesTable = $shipmentDeliveries->prefix . $shipmentDeliveries->table;

        $where = "$viewShipmentTable.deleted = '0'";

        if (!empty($param)) {
            if (!empty($param['searchShipment']['idFrom']))
                $where .= " AND $viewShipmentTable.id >= " . $param['searchShipment']['idFrom'];

            if (!empty($param['searchShipment']['idTo']))
                $where .= " AND $viewShipmentTable.id <= " . $param['searchShipment']['idTo'];

            if (!empty($param['searchShipment']['unitFrom']))
                $where .= " AND $viewShipmentTable.unit >= '" . $param['searchShipment']['unitFrom'] . "'";

            if (!empty($param['searchShipment']['unitTo']))
                $where .= " AND $viewShipmentTable.unit <= '" . $param['searchShipment']['unitFrom'] . "'";

            if (!empty($param['searchShipment']['deliveryCompanyId']))
                $where .= " AND $viewShipmentDetailsTable.deliveryCompanyId = " . $param['searchShipment']['deliveryCompanyId'];
            if (!empty($param['searchShipment']['dispatchCompanyId']))
                $where .= " AND $viewShipmentDetailsTable.dispatchCompanyId = " . $param['searchShipment']['dispatchCompanyId'];

            if (!empty($param['searchShipment']['prepaid']))
                $where .= " AND $viewShipmentTable.prepaid = '" . $param['searchShipment']['prepaid'] . "'";

            if (!empty($param['searchShipment']['shipmentStatus']))
                $where .= " AND $viewShipmentTable.shipmentStatus = '" . $param['searchShipment']['shipmentStatus'] . "'";

            if (!empty($param['searchShipment']['paymentStatus']))
                $where .= " AND $viewShipmentTable.paymentStatus = '" . $param['searchShipment']['paymentStatus'] . "'";

            if (!empty($param['searchShipment']['shipmentType']))
                $where .= " AND $viewShipmentTable.shipmentType = '" . $param['searchShipment']['shipmentType'] . "'";

            if (!empty($param['searchShipment']['totalFrom']))
                $where .= " AND $viewShipmentTable.total >= '" . $param['searchShipment']['totalFrom'] . "'";

            if (!empty($param['searchShipment']['totalTo']))
                $where .= " AND $viewShipmentTable.total <= '" . $param['searchShipment']['totalTo'] . "'";

            if (!empty($param['searchShipment']['user']))
                $where .= " AND ($viewShipmentTable.customerName LIKE '%" . $param['searchShipment']['user'] . "%' OR $viewShipmentTable.customerEmail LIKE '%" . $param['searchShipment']['user'] . "%')";

            if (!empty($param['searchShipment']['receiver']))
                $where .= " AND ($viewShipmentTable.toName LIKE '%" . $param['searchShipment']['receiver'] . "%' OR $viewShipmentTable.toEmail LIKE '%" . $param['searchShipment']['receiver'] . "%')";

            if (!empty($param['searchShipment']['warehouseId']))
                $where .= " AND $viewShipmentTable.warehouseId = '" . $param['searchShipment']['warehouseId'] . "'";

            /* if (!empty($param['searchShipment']['labelType']))
              $where .= " AND $shipmentLabelTable.labelType = '" . $param['searchShipment']['labelType'] . "'"; */

            if (!empty($param['searchShipment']['itemType']))
                $where .= " AND $viewShipmentDetailsTable.itemType = '" . $param['searchShipment']['itemType'] . "'";

            if (!empty($param['searchShipment']['declaredValueFrom']))
                $where .= " AND $viewShipmentDetailsTable.declaredValue >= '" . $param['searchShipment']['declaredValueFrom'] . "'";

            if (!empty($param['searchShipment']['declaredValueTo']))
                $where .= " AND $viewShipmentDetailsTable.declaredValue <= '" . $param['searchShipment']['declaredValueTo'] . "'";

            if (isset($param['searchShipment']['packed']) && !empty($param['searchShipment']['packed'])) {
                $where .= " AND $viewShipmentTable.packed = '" . $param['searchShipment']['packed'] . "'";
            }
            if (isset($param['searchByshipment']) && !empty($param['searchByshipment'])) {
                $where .= " AND $viewShipmentTable.id =" . $param['searchByshipment'][0];
            }

            if (isset($param['searchOffer'])) {
                $where .= " AND couponcodeApplied = '" . $param['searchOffer'] . "'";
            }

            if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($viewShipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $viewShipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($viewShipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($viewShipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }
        }

        if (!empty($selectedUser)) {
            $where = "$viewShipmentTable.id IN (" . implode(',', $selectedUser) . ")";
        }

        if (!empty($selectedFields)) {
            foreach ($selectedFields as $eachSearchfield) {

                if ($eachSearchfield == 'id')
                    $select[] = DB::raw("$viewShipmentTable.id as 'ID'");

                if ($eachSearchfield == 'customer')
                    $select[] = DB::raw("$viewShipmentTable.customerName as 'customerName'");

                if ($eachSearchfield == 'paymentstatus') {
                    $select[] = DB::raw("case paymentStatus
                                    when 'unpaid' then 'Not Paid'
                                    when 'paid' then 'Paid'
                                end as 'paymentStatus'");
                }
                if ($eachSearchfield == 'destinationCountryName')
                    $select[] = DB::raw("toCountryName as 'countryName'");
                if ($eachSearchfield == 'destinationStateName')
                    $select[] = DB::raw("toStateName as 'stateName'");
                if ($eachSearchfield == 'destinationCityName')
                    $select[] = DB::raw("toCityName as 'cityName'");
                if ($eachSearchfield == 'totalDiscount')
                    $select[] = DB::raw("totalDiscount as 'discount'");

                if ($eachSearchfield == 'totalcost')
                    $select[] = DB::raw("$viewShipmentTable.totalCost as 'totalCost'");
                if ($eachSearchfield == 'createdOn')
                    $select[] = DB::raw("$viewShipmentTable.createdOn as 'createdOn'");
                if ($eachSearchfield == 'totalWeight')
                    $select[] = DB::raw("$viewShipmentTable.totalWeight as 'totalWeight'");
            }
        }

        $resultSet = Viewshipment::select($select)
                        ->leftJoin("$viewShipmentDetails->table", "$viewShipment->table.id", "=", "$viewShipmentDetails->table.shipmentId")
                        ->whereRaw($where)
                        ->groupBy("$viewShipment->table.id")->get();

        $data = array();
        foreach ($resultSet as $key => $val) {
            foreach ($selectedFields as $eachSearchfield) {


                if ($eachSearchfield == 'id')
                    $data['shipments'][$key]['ID'] = $val->ID;
                if ($eachSearchfield == 'customer')
                    $data['shipments'][$key]['Customer Name'] = $val->customerName;
                if ($eachSearchfield == 'email')
                    $data['shipments'][$key]['Email Address'] = $val->customerEmail;
                if ($eachSearchfield == 'number')
                    $data['shipments'][$key]['Contact Number'] = $val->toPhone;
                if ($eachSearchfield == 'destinationCountryName')
                    $data['shipments'][$key]['Destination Country'] = $val->countryName;
                if ($eachSearchfield == 'destinationStateName')
                    $data['shipments'][$key]['Destination State'] = $val->stateName;
                if ($eachSearchfield == 'destinationCityName')
                    $data['shipments'][$key]['Destination City'] = $val->cityName;
                if ($eachSearchfield == 'type')
                    $data['shipments'][$key]['Shipment Type'] = $val->shipmentType;
                if ($eachSearchfield == 'status')
                    $data['shipments'][$key]['Shipment Status'] = $val->shipmentStatus;
                if ($eachSearchfield == 'paymentMethod')
                    $data['shipments'][$key]['Payment Method'] = $val->paymentMethod;
                if ($eachSearchfield == 'paymentDate')
                    $data['shipments'][$key]['Payment Date'] = $val->paymentReceivedOn;
                if ($eachSearchfield == 'paymentstatus')
                    $data['shipments'][$key]['Payment Status'] = $val->paymentStatus;
                if ($eachSearchfield == 'shippingCost')
                    $data['shipments'][$key]['Shipping Cost'] = $val->totalShippingCost;
                if ($eachSearchfield == 'clearing')
                    $data['shipments'][$key]['Clearing and Port Handling Charge'] = $val->totalClearingDuty;
                if ($eachSearchfield == 'storage')
                    $data['shipments'][$key]['Storage Charge'] = $val->storage;
                if ($eachSearchfield == 'inventory')
                    $data['shipments'][$key]['Inventory Charge'] = Shipmentdelivery::where("shipmentId", $val->ID)->sum('inventoryCharge');
                if ($eachSearchfield == 'otherCost')
                    $data['shipments'][$key]['Other Cost'] = $val->totalOtherCharges;
                if ($eachSearchfield == 'tax')
                    $data['shipments'][$key]['VAT'] = $val->tax;
                if ($eachSearchfield == 'totalDiscount')
                    $data['shipments'][$key]['Discount Received'] = $val->discount;
                if ($eachSearchfield == 'totalWeight')
                    $data['shipments'][$key]['Total Weight'] = $val->totalWeight;
                if ($eachSearchfield == 'packageStatus')
                    $data['shipments'][$key]['Package Status'] = $val->packageStatus;
                if ($eachSearchfield == 'totalcost')
                    $data['shipments'][$key]['Total Cost'] = $val->totalCost;
                if ($eachSearchfield == 'createdOn')
                    $data['shipments'][$key]['Date'] = $val->createdOn;
            }
        }
    }

    public static function getShipmentListByUser($param) {
        DB::enableQueryLog();

        $shipmentObj = new Shipment;
        $shipmentPackages = new Shipmentpackage;
        $country = new Country;
        $adminUser = new UserAdmin;
        $store = new Stores;
        $storeCountry = new Storecountrymapping;
        $category = new Sitecategory;
        $product = new Siteproduct;
        $shipmentdelivery = new Shipmentdelivery;

        $shipmentObjTable = $shipmentObj->prefix . $shipmentObj->table;
        $shipmentPackagesTable = $shipmentObj->prefix . $shipmentPackages->table;
        $storeTable = $shipmentObj->prefix . $store->table;
        $storeCountryTable = $shipmentObj->prefix . $storeCountry->table;
        $categoryTable = $shipmentObj->prefix . $category->table;
        $adminUserTable = $shipmentObj->prefix . $adminUser->table;
        $shipmentdeliveryTable = $shipmentObj->prefix . $shipmentdelivery->table;

        $where = "stmd_adminUser.deleted = '0'";

        if (!empty($param)) {

            if (isset($param['searchWarehouse']['searchByEmail']) && !empty($param['searchWarehouse']['searchByEmail'])) {
                $where .= " AND stmd_adminUser.email = '" . $param['searchWarehouse']['searchByEmail'] . "'";
            }


            if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($shipmentObjTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $shipmentObjTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($shipmentObjTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($shipmentObjTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }
        }



        $resultSet = Shipment::select(array("$shipmentObj->table.id as shipmentId", "$shipmentObj->table.shipmentType", "$shipmentObj->table.shipmentStatus", "$shipmentObj->table.firstReceived", "$shipmentObj->table.totalCost", "$shipmentObj->table.maxStorageDate", "$shipmentObj->table.id as shipmentId", "$shipmentObj->table.createdOn", "$shipmentObj->table.modifiedOn", "$shipmentPackages->table.itemName", "location.name AS locationName", "adminUser.firstName", "adminUser.lastName", "adminUser.email as userEmail", "adminUser.contactno as contactNumber", DB::RAW("(select categoryName FROM $categoryTable where " . $categoryTable . ".id = " . $shipmentPackagesTable . ".siteCategoryId) as categoryName"), DB::RAW("(select categoryName FROM $categoryTable where " . $categoryTable . ".id = " . $shipmentPackagesTable . ".siteSubCategoryId) as subCategoryName"), DB::RAW("(select count('id') FROM $shipmentdeliveryTable where " . $shipmentdeliveryTable . ".shipmentId = " . $shipmentObjTable . ".id) as noofdelivery"), "$store->table.storeName"))
                ->join("$country->table AS location", "location.id", '=', "$shipmentObj->table.toCountry")
                ->join("$shipmentPackages->table", "$shipmentPackages->table.shipmentId", '=', "$shipmentObj->table.id")
                ->join("$store->table", "$store->table.id", '=', "$shipmentPackages->table.storeId")
                ->join("$category->table", "$category->table.id", '=', "$shipmentPackages->table.siteCategoryId")
                ->leftjoin("$shipmentdelivery->table", "$shipmentdelivery->table.shipmentId", '=', "$shipmentObj->table.id")
                ->join("$adminUser->table AS adminUser", function($join) {
                    $join->on("adminUser.id", '=', "shipments.createdBy")
                    ->orWhere('adminUser.id', '=', "stmd_shipments.modifiedBy");
                })
                ->whereRaw($where)
                //->groupBy("$shipmentObj->table.id")
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        // dd(DB::getQueryLog());

        return $resultSet;
    }

    public static function getPackagingReport($param) {
        DB::enableQueryLog();

        $shipmentObj = new Shipment;
        $shipmentPackages = new Shipmentpackage;
        $shipmentDispatch = new Dispatchcompany;
        $deliveryCompany = new Deliverycompany;
        $shipmentdelivery = new Shipmentdelivery;
        $shipmentStatuslog = new Shipmentstatuslog;
        $adminUser = new UserAdmin;


        $shipmentObjTable = $shipmentObj->prefix . $shipmentObj->table;
        $shipmentPackagesTable = $shipmentObj->prefix . $shipmentPackages->table;
        $shipmentdeliveryTable = $shipmentObj->prefix . $shipmentdelivery->table;
        $shipmentStausLogTable = $shipmentObj->prefix . $shipmentStatuslog->table;
        $adminUserTable = $shipmentObj->prefix . $adminUser->table;

        $where = "stmd_adminUser.deleted = '0'";

        if (!empty($param)) {

            if (isset($param['searchWarehouse']['searchByEmail']) && !empty($param['searchWarehouse']['searchByEmail'])) {
                $where .= " AND stmd_adminUser.email = '" . $param['searchWarehouse']['searchByEmail'] . "'";
            }


            if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($shipmentObjTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $shipmentObjTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($shipmentObjTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($shipmentObjTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }
        }


        $result = Shipment::select("$shipmentObj->table.id as shipmentId", "$deliveryCompany->table.name as deliveryCompany", "$shipmentDispatch->table.name as dispatchCompany", "$shipmentdelivery->table.expectedDispatchDate", DB::RAW("(select status FROM $shipmentStausLogTable where " . $shipmentStausLogTable . ".shipmentId = " . $shipmentObjTable . ".id order by " . $shipmentStausLogTable . ".id DESC LIMIT 0,1) as shipmentStatus"), DB::RAW("(select updatedOn FROM $shipmentStausLogTable where " . $shipmentStausLogTable . ".shipmentId = " . $shipmentObjTable . ".id order by " . $shipmentStausLogTable . ".id DESC LIMIT 0,1) as stageDate"), DB::RAW("(select count('id') FROM $shipmentPackagesTable where " . $shipmentPackagesTable . ".shipmentId = " . $shipmentObjTable . ".id) as pacakages"), DB::RAW("(select SUM('weight') FROM $shipmentPackagesTable where " . $shipmentPackagesTable . ".shipmentId = " . $shipmentObjTable . ".id) as totalWeight"))
                ->join("$shipmentPackages->table", "$shipmentPackages->table.shipmentId", '=', "$shipmentObj->table.id")
                ->join("$shipmentdelivery->table", "$shipmentdelivery->table.shipmentId", '=', "$shipmentObj->table.id")
                ->leftjoin("$deliveryCompany->table", "$deliveryCompany->table.id", '=', "$shipmentPackages->table.deliveryCompanyId")
                ->leftjoin("$shipmentDispatch->table", "$shipmentDispatch->table.id", '=', "$shipmentdelivery->table.dispatchCompanyId")
                ->join("$adminUser->table AS adminUser", function($join) {
                    $join->on("adminUser.id", '=', "shipments.createdBy")
                    ->orWhere('adminUser.id', '=', "stmd_shipments.modifiedBy");
                })
                ->whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->groupBy("$shipmentObj->table.id")
                ->paginate($param['searchDisplay']);


        // dd(DB::getQueryLog());

        return $result;
    }

    public static function exportWarehouseData($param) {
        //print_r($param); die;
        $shipmentObj = new Shipment;
        $shipmentPackages = new Shipmentpackage;
        $country = new Country;
        $adminUser = new UserAdmin;
        $store = new Stores;
        $storeCountry = new Storecountrymapping;
        $category = new Sitecategory;
        $product = new Siteproduct;
        $shipmentDispatch = new Dispatchcompany;
        $deliveryCompany = new Deliverycompany;
        $shipmentdelivery = new Shipmentdelivery;
        $shipmentStatuslog = new Shipmentstatuslog;

        $shipmentObjTable = $shipmentObj->prefix . $shipmentObj->table;
        $shipmentPackagesTable = $shipmentObj->prefix . $shipmentPackages->table;
        $storeTable = $shipmentObj->prefix . $store->table;
        $storeCountryTable = $shipmentObj->prefix . $storeCountry->table;
        $categoryTable = $shipmentObj->prefix . $category->table;
        $adminUserTable = $shipmentObj->prefix . $adminUser->table;
        $shipmentdeliveryTable = $shipmentObj->prefix . $shipmentdelivery->table;
        $shipmentStausLogTable = $shipmentObj->prefix . $shipmentStatuslog->table;
        $adminUserTable = $shipmentObj->prefix . $adminUser->table;



        $where = "stmd_adminUser.deleted = '0'";

        if (!empty($param)) {

            if (isset($param['searchWarehouse']['searchByEmail']) && !empty($param['searchWarehouse']['searchByEmail'])) {
                $where .= " AND stmd_adminUser.email = '" . $param['searchWarehouse']['searchByEmail'] . "'";
            }


            if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($shipmentObjTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $shipmentObjTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($shipmentObjTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($shipmentObjTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }


            if ($param['searchByCondition'] == 'onpackaging') {
                $resultSet = Shipment::select("$shipmentObj->table.id as shipmentId", "$deliveryCompany->table.name as deliveryCompany", "$shipmentDispatch->table.name as dispatchCompany", "$shipmentdelivery->table.expectedDispatchDate", DB::RAW("(select status FROM $shipmentStausLogTable where " . $shipmentStausLogTable . ".shipmentId = " . $shipmentObjTable . ".id order by " . $shipmentStausLogTable . ".id DESC LIMIT 0,1) as shipmentStatus"), DB::RAW("(select updatedOn FROM $shipmentStausLogTable where " . $shipmentStausLogTable . ".shipmentId = " . $shipmentObjTable . ".id order by " . $shipmentStausLogTable . ".id DESC LIMIT 0,1) as stageDate"), DB::RAW("(select count('id') FROM $shipmentPackagesTable where " . $shipmentPackagesTable . ".shipmentId = " . $shipmentObjTable . ".id) as pacakages"), DB::RAW("(select SUM('weight') FROM $shipmentPackagesTable where " . $shipmentPackagesTable . ".shipmentId = " . $shipmentObjTable . ".id) as totalWeight"))
                        ->join("$shipmentPackages->table", "$shipmentPackages->table.shipmentId", '=', "$shipmentObj->table.id")
                        ->join("$shipmentdelivery->table", "$shipmentdelivery->table.shipmentId", '=', "$shipmentObj->table.id")
                        ->leftjoin("$deliveryCompany->table", "$deliveryCompany->table.id", '=', "$shipmentPackages->table.deliveryCompanyId")
                        ->leftjoin("$shipmentDispatch->table", "$shipmentDispatch->table.id", '=', "$shipmentdelivery->table.dispatchCompanyId")
                        ->join("$adminUser->table AS adminUser", function($join) {
                            $join->on("adminUser.id", '=', "shipments.createdBy")
                            ->orWhere('adminUser.id', '=', "stmd_shipments.modifiedBy");
                        })
                        ->whereRaw($where)
                        ->orderBy($param['field'], $param['type'])
                        ->groupBy("$shipmentObj->table.id")
                        ->get();
            } else {
                $resultSet = Shipment::select(array("$shipmentObj->table.id as shipmentId", "$shipmentObj->table.shipmentType", DB::RAW("(select status FROM $shipmentStausLogTable where " . $shipmentStausLogTable . ".shipmentId = " . $shipmentObjTable . ".id order by " . $shipmentStausLogTable . ".id DESC LIMIT 0,1) as shipmentStatus"), "$shipmentObj->table.firstReceived", "$shipmentObj->table.totalCost", "$shipmentObj->table.maxStorageDate", "$shipmentObj->table.id as shipmentId", "$shipmentObj->table.createdOn", "$shipmentObj->table.modifiedOn", "location.name AS locationName", "adminUser.firstName", "adminUser.lastName", "adminUser.email as userEmail", "adminUser.contactno as contactNumber", DB::RAW("(select categoryName FROM $categoryTable where " . $categoryTable . ".id = " . $shipmentPackagesTable . ".siteCategoryId) as categoryName"), DB::RAW("(select categoryName FROM $categoryTable where " . $categoryTable . ".id = " . $shipmentPackagesTable . ".siteSubCategoryId) as subCategoryName"), DB::RAW("(select count('id') FROM $shipmentdeliveryTable where " . $shipmentdeliveryTable . ".shipmentId = " . $shipmentObjTable . ".id) as noofdelivery"), "$store->table.storeName"))
                        ->join("$country->table AS location", "location.id", '=', "$shipmentObj->table.toCountry")
                        ->join("$shipmentPackages->table", "$shipmentPackages->table.shipmentId", '=', "$shipmentObj->table.id")
                        ->join("$shipmentdelivery->table", "$shipmentdelivery->table.shipmentId", '=', "$shipmentObj->table.id")
                        ->join("$store->table", "$store->table.id", '=', "$shipmentPackages->table.storeId")
                        ->join("$category->table", "$category->table.id", '=', "$shipmentPackages->table.siteCategoryId")
                        ->join("$adminUser->table AS adminUser", function($join) {
                            $join->on("adminUser.id", '=', "shipments.createdBy")
                            ->orWhere('adminUser.id', '=', "stmd_shipments.modifiedBy");
                        })
                        ->whereRaw($where)
                        //->groupBy("$shipmentObj->table.id")
                        ->orderBy($param['field'], $param['type'])
                        ->get();
            }
        }

        return $resultSet;
    }

    /**
     * Method used to fetch users shipment count and 
     * not delever count
     * @param array $userId,$shipmentStatus
     * @return count(int)
     */
    public static function getUserShipmentAndNotDeleverCount($userId, $shipmentStatus = '') {

        if (!empty($userId)) {

            $viewShipment = new Shipment;
            //$viewShipmentDetails = new ViewShipmentDetails;        

            $viewShipmentTable = $viewShipment->prefix . $viewShipment->table;
            //$viewShipmentDetailsTable = $viewShipmentDetails->prefix . $viewShipmentDetails->table;

            $where = "$viewShipmentTable.userId=$userId AND $viewShipmentTable.deleted = '0' and hideFromCustomer='N'";

            if (!empty($shipmentStatus))
                $where .= " AND $viewShipmentTable.paymentStatus='paid' AND $viewShipmentTable.shipmentStatus != '" . $shipmentStatus . "'";
            else
                $where .= " AND $viewShipmentTable.paymentStatus='unpaid'";



            $resultCount = Shipment::select(array("$viewShipment->table.*"))
                    ->whereRaw($where)
                    ->count();


            return $resultCount;
        } else {

            return 0;
        }
    }

    public static function notificationEmail($id, $templateKey = 'create_shipment_notification') {

        $shipment = Viewshipment::find($id);
        $deliveryData = ViewShipmentDetails::where("shipmentId", $id)->where('deleted', '0')->where('packageDeleted', '0')->where('packageType', 'I')->orderBy('deliveryId', 'asc')->get()->toarray();
        $shipmentDelivery = Shipment::getDeliveryDetails($deliveryData);

        $emailTemplate = Emailtemplate::where('templateKey', $templateKey)->first();
        $to = $shipment->fromEmail;

        $replace['[NAME]'] = $shipment->fromName;
        $link = Config::get('constants.frontendUrl') . 'login/';
        $replace['[WEBSITE_LINK]'] = '<a href="' . $link . '">Click here</a>';
        $replace['[SHIPMENTID]'] = "#" . $id;

        $deliveryCount = 1;
        $deliveryDetailsContent = '';
        foreach ($shipmentDelivery['deliveries'] as $eachDeliveryDetails) {
            $deliveryDetailsContent .="<p>Delivery $deliveryCount:";
            $deliveryDetailsContent .="<p>Store: " . $eachDeliveryDetails['packages'][0]['storeName'] . "</p>";
            $deliveryDetailsContent .="<p>Tracking #:" . $eachDeliveryDetails['packages'][0]['tracking'] . "</p>";
            $deliveryDetailsContent .="<p>Delivered:" . $eachDeliveryDetails['packages'][0]['deliveredOn'] . "</p>";
            if (!empty($eachDeliveryDetails['packages'][0]['deliveryNotes'])) {
                $deliveryDetailsContent .="<p>Delivery Note:<strong>" . $eachDeliveryDetails['packages'][0]['deliveryNotes'] . "</strong></p>";
            }
            $deliveryDetailsContent .= '<table border="1px"><tr><th>Items / products ordered</th><th>Value Per Item</th><th>Quantity</th></tr>';
            foreach ($eachDeliveryDetails['packages'] as $eachPackage) {
                $deliveryDetailsContent .="<tr><td>" . $eachPackage['itemName'] . "</td>";
                $deliveryDetailsContent .="<td>" . $eachPackage['itemPrice'] . "</td>";
                $deliveryDetailsContent .="<td>" . $eachPackage['itemQuantity'] . "</td></tr>";
            }
            $deliveryDetailsContent .="</table>";
            $deliveryCount++;
        }
        $replace['[DELIVERY_DETAILS]'] = $deliveryDetailsContent;

        $isSend = 0;
        if (customhelper::SendMail($emailTemplate, $replace, $to))
            $isSend = 1;

        return $isSend;
    }

    public static function notificationSMS($shipmentId, $deliveryId) {
        $replaceVar = array();
        $shipment = Viewshipment::find($shipmentId);
        $user = User::find($shipment->userId);
        $deliveryInfo = Shipmentdelivery::find($deliveryId);
        $itemDetails = Shipmentpackage::where('deliveryId', $deliveryId)->where('deleted', '0')->get()->toArray();
        $storeDetails = array();
        if ($itemDetails[0]['storeId'] != '' && $itemDetails[0]['storeId'] != '0')
            $storeDetails = Stores::find($itemDetails[0]['storeId']);
        $toMobile = trim($user->isdCode . $user->contactNumber);
        $smsTemplate = Smstemplate::where('templateKey', 'create_shipment_notification')->first();
        if ($toMobile) {
            $replaceVar['[ITEM]'] = count($itemDetails) . " Item(s)";
            if (!empty($storeDetails->storeURL))
                $replaceVar['[STORE_LINK]'] = $storeDetails->storeURL;
            $replaceVar['[WAREHOUSE]'] = $shipment->fromCountryName . " (" . $shipment->fromCityName . ")";
            $link = Config::get('constants.frontendUrl');
            $replaceVar['[WEBSITE_LINK]'] = $link;
            $isSendMsg = customhelper::sendMSG($toMobile, $replaceVar, $smsTemplate);
        }

        return $isSendMsg;
    }

    public static function updateDeliveryFields($deliveryId, $field = "totalItemCost") {

        if ($field == "totalItemCost") {
            $updateValue = "0.00";
            $deliveryTotalItemCost = Shipmentpackage::where('deliveryId', $deliveryId)->where("deleted","0")->sum('itemTotalCost');
            if (!empty($deliveryTotalItemCost)) {
                $updateValue = $deliveryTotalItemCost;
            }
        }

        Shipmentdelivery::where('id', $deliveryId)->update([$field => $updateValue]);

        return true;
    }

    public static function getallShipmentData($param, $export = 0) {
        $user = new User;
        $shipment = new Shipment;
        $city = new City;
        $country = new Country;
        $state = new State;
        $paymentmethod = new Paymentmethod;
        $adminUser = new UserAdmin;

        $userTable = $user->prefix . $user->table;
        $shipmentTable = $user->prefix . $shipment->table;
        $cityTable = $shipment->prefix . $city->table;
        $countryTable = $user->prefix . $country->table;
        $stateTable = $user->prefix . $state->table;
        $paymentmethodTable = $user->prefix . $paymentmethod->table;
        $adminUserTable = $user->prefix . $adminUser->table;


        $where = "$userTable.deleted = '0' AND $shipmentTable.deleted = '0'";

        if (isset($param['searchAccount']['createdOn']) && !empty($param['searchAccount']['createdOn'])) {
            if ($param['searchAccount']['createdOn'] == 'thismonth')
                $where .= "  AND MONTH($shipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchAccount']['createdOn'] == 'thisweek')
                $where .= "  AND $shipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchAccount']['createdOn'] == 'today')
                $where .= "  AND date($shipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchAccount']['createdOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchAccount']['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($shipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

       //print_r($param['searchAccount']); die;

        if (isset($param['searchAccount']['accountType']) && !empty($param['searchAccount']['accountType'])) {
                    if ($param['searchAccount']['accountType'] == "paid") {
                             if ($export == 0) {
                                  $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit, $paymentmethodTable.paymentMethod, $shipmentTable.paymentReceivedOn as paymentDate, (CASE WHEN $shipmentTable.paidByUserType='user' THEN 'Automatic' ELSE CONCAT($adminUserTable.firstName, ' ',$adminUserTable.lastName) END) as processor")
                                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                                            ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                                            ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                                            ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                                            ->leftJoin($paymentmethod->table, "$paymentmethod->table.id", '=', "$shipment->table.paymentMethodId")
                                            ->leftjoin($adminUser->table, "$adminUser->table.id", '=', "$shipment->table.paidByUserId")
                                            ->whereRaw($where)
                                            ->where("$shipment->table.paymentStatus", "paid") 
                                            ->groupBy($shipment->table.".id")
                                            ->orderBy($param['field'], $param['type'])->paginate($param['searchDisplay']);
                                } else {
                                    $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit, $paymentmethodTable.paymentMethod, $shipmentTable.paymentReceivedOn as paymentDate, (CASE WHEN $shipmentTable.paidByUserType='user' THEN 'Automatic' ELSE CONCAT($adminUserTable.firstName, ' ',$adminUserTable.lastName) END) as processor")
                                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                                            ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                                            ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                                            ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                                            ->leftjoin($adminUser->table, "$adminUser->table.id", '=', "$shipment->table.paidByUserId")
                                            ->leftJoin($paymentmethod->table, "$paymentmethod->table.id", '=', "$shipment->table.paymentMethodId")
                                            ->whereRaw($where)
                                            ->where("$shipment->table.paymentStatus", "paid")
                                            ->groupBy($shipment->table.".id")
                                            ->orderBy($param['field'], $param['type'])
                                            ->get()->toArray();
                                }

                    }else{
                            if ($export == 0) {
                              $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit")
                                        ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                                        ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                                        ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                                        ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                                        ->whereRaw($where)
                                        ->where("$shipment->table.paymentStatus", "unpaid")
                                        ->groupBy($shipment->table.".id")
                                        ->orderBy($param['field'], $param['type'])->paginate($param['searchDisplay']);
                            } else {
                                $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit")
                                        ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                                        ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                                        ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                                        ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                                        ->whereRaw($where)
                                        ->where("$shipment->table.paymentStatus", "unpaid")
                                        ->groupBy($shipment->table.".id")
                                        ->orderBy($param['field'], $param['type'])
                                        ->get()->toArray();
                            }
                    }
                }else{
                        if ($export == 0) {
                          $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit")
                                    ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                                    ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                                    ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                                    ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                                    ->whereRaw($where)
                                    ->groupBy($shipment->table.".id")
                                    ->orderBy($param['field'], $param['type'])->paginate($param['searchDisplay']);
                        } else {
                            $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit")
                                    ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                                    ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                                    ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                                    ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                                    ->whereRaw($where)
                                    ->groupBy($shipment->table.".id")
                                    ->orderBy($param['field'], $param['type'])
                                    ->get()->toArray();
                        }
                }

   
        
        return $resultSet;
    }

    public static function getallShipmentDatawithStorage($param, $export = 0) {
        $user = new User;
        $shipment = new Shipment;
        $city = new City;
        $country = new Country;
        $state = new State;
        $paymentmethod = new Paymentmethod;
        $adminUser = new UserAdmin;

        $userTable = $user->prefix . $user->table;
        $shipmentTable = $user->prefix . $shipment->table;
        $cityTable = $shipment->prefix . $city->table;
        $countryTable = $user->prefix . $country->table;
        $stateTable = $user->prefix . $state->table;
        $paymentmethodTable = $user->prefix . $paymentmethod->table;
        $adminUserTable = $user->prefix . $adminUser->table;

        $where = "$userTable.deleted = '0' AND $shipmentTable.deleted = '0' AND $shipmentTable.storageCharge <> 0";

        if (isset($param['searchAccount']['createdOn']) && !empty($param['searchAccount']['createdOn'])) {
            if ($param['searchAccount']['createdOn'] == 'thismonth')
                $where .= "  AND MONTH($shipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchAccount']['createdOn'] == 'thisweek')
                $where .= "  AND $shipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchAccount']['createdOn'] == 'today')
                $where .= "  AND date($shipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchAccount']['createdOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchAccount']['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($shipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }



     if (isset($param['searchAccount']['accountType']) && !empty($param['searchAccount']['accountType'])) {
            if ($param['searchAccount']['accountType'] == "paid") {
                if ($export == 0) {
                    $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit, $paymentmethodTable.paymentMethod, $shipmentTable.paymentReceivedOn as paymentDate, (CASE WHEN $shipmentTable.paidByUserType='user' THEN 'Automatic' ELSE CONCAT($adminUserTable.firstName, ' ',$adminUserTable.lastName) END) as processor")
                        ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                        ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                        ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                        ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                        ->leftjoin($adminUser->table, "$adminUser->table.id", '=', "$shipment->table.paidByUserId")
                        ->leftJoin($paymentmethod->table, "$paymentmethod->table.id", '=', "$shipment->table.paymentMethodId")
                        ->whereRaw($where)
                        ->where("$shipment->table.paymentStatus", "paid")
                        ->groupBy($shipment->table . ".id")
                        ->orderBy($param['field'], $param['type'])
                        ->paginate($param['searchDisplay']);
                } else {
                    $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit, $paymentmethodTable.paymentMethod, $shipmentTable.paymentReceivedOn as paymentDate, (CASE WHEN $shipmentTable.paidByUserType='user' THEN 'Automatic' ELSE CONCAT($adminUserTable.firstName, ' ',$adminUserTable.lastName) END) as processor")
                        ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                        ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                        ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                        ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                        ->leftjoin($adminUser->table, "$adminUser->table.id", '=', "$shipment->table.paidByUserId")
                        ->leftJoin($paymentmethod->table, "$paymentmethod->table.id", '=', "$shipment->table.paymentMethodId")
                        ->whereRaw($where)
                        ->where("$shipment->table.paymentStatus", "paid")
                        ->groupBy($shipment->table . ".id")
                        ->orderBy($param['field'], $param['type'])
                        ->get()->toArray();
                }

            }
            else{

                if ($export == 0) {
                    $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit")
                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                            ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                            ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                            ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                            ->whereRaw($where)
                            ->where("$shipment->table.paymentStatus", "unpaid")
                            ->groupBy($shipment->table . ".id")
                            ->orderBy($param['field'], $param['type'])
                            ->paginate($param['searchDisplay']);
                } else {
                    $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit")
                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                            ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                            ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                            ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                            ->whereRaw($where)
                            ->where("$shipment->table.paymentStatus", "unpaid")
                            ->groupBy($shipment->table . ".id")
                            ->orderBy($param['field'], $param['type'])
                            ->get()->toArray();
                }


            }

        }



        


        return $resultSet;
    }

    public static function getallShipmentDataShopforme($param, $export = 0) {
        $user = new User;
        $shipment = new Procurement;
        $shipmentItem = new Procurementitem;
        $city = new City;
        $country = new Country;
        $state = new State;
        $paymentmethod = new Paymentmethod;
        $adminUser = new UserAdmin;

        $userTable = $user->prefix . $user->table;
        $shipmentTable = $user->prefix . $shipment->table;
        $shipmentItemTable = $user->prefix . $shipmentItem->table;
        $cityTable = $shipment->prefix . $city->table;
        $countryTable = $user->prefix . $country->table;
        $stateTable = $user->prefix . $state->table;
        $paymentmethodTable = $user->prefix . $paymentmethod->table;
        $adminUserTable = $user->prefix . $adminUser->table;

        $where = "$userTable.deleted = '0' AND $shipmentTable.deleted = '0' AND $shipmentTable.procurementType = 'shopforme'";

        if (isset($param['searchAccount']['createdOn']) && !empty($param['searchAccount']['createdOn'])) {
            if ($param['searchAccount']['createdOn'] == 'thismonth')
                $where .= "  AND MONTH($shipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchAccount']['createdOn'] == 'thisweek')
                $where .= "  AND $shipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchAccount']['createdOn'] == 'today')
                $where .= "  AND date($shipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchAccount']['createdOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchAccount']['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($shipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

     if (isset($param['searchAccount']['accountType']) && !empty($param['searchAccount']['accountType'])) {
            if ($param['searchAccount']['accountType'] == "paid") {
                if ($export == 0) {
                    $resultSet = Procurement::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit, $paymentmethodTable.paymentMethod, $shipmentTable.paymentReceivedOn as paymentDate, (CASE WHEN $shipmentTable.paidByUserType='user' THEN 'Automatic' ELSE CONCAT($adminUserTable.firstName, ' ',$adminUserTable.lastName) END) as processor")
                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                            ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                            ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                            ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                            ->leftJoin($paymentmethod->table, "$paymentmethod->table.id", '=', "$shipment->table.paymentMethodId")
                            ->leftjoin($adminUser->table, "$adminUser->table.id", '=', "$shipment->table.paidByUserId")
                            ->whereRaw($where)
                            ->where("$shipment->table.paymentStatus", "paid")
                            ->groupBy($shipment->table . ".id")
                            ->orderBy($shipment->table . ".createdOn", $param['type'])
                            ->paginate($param['searchDisplay']);
                } else {
                    $resultSet = Procurement::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit, $paymentmethodTable.paymentMethod, $shipmentTable.paymentReceivedOn as paymentDate, (CASE WHEN $shipmentTable.paidByUserType='user' THEN 'Automatic' ELSE CONCAT($adminUserTable.firstName, ' ',$adminUserTable.lastName) END) as processor")
                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                            ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                            ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                            ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                            ->leftJoin($paymentmethod->table, "$paymentmethod->table.id", '=', "$shipment->table.paymentMethodId")
                            ->leftjoin($adminUser->table, "$adminUser->table.id", '=', "$shipment->table.paidByUserId")
                            ->whereRaw($where)
                            ->where("$shipment->table.paymentStatus", "paid")
                            ->groupBy($shipment->table . ".id")
                            ->orderBy($shipment->table . ".createdOn", $param['type'])
                            ->get()->toArray();
                }
            }
            else{
                if ($export == 0) {
                    $resultSet = Procurement::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit")
                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                            ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                            ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                            ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                            ->whereRaw($where)
                            ->where("$shipment->table.paymentStatus", "unpaid")
                            ->groupBy($shipment->table . ".id")
                            ->orderBy($shipment->table . ".createdOn", $param['type'])
                            ->paginate($param['searchDisplay']);
                } else {
                    $resultSet = Procurement::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit")
                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                            ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                            ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                            ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                            ->whereRaw($where)
                            ->where("$shipment->table.paymentStatus", "unpaid")
                            ->groupBy($shipment->table . ".id")
                            ->orderBy($shipment->table . ".createdOn", $param['type'])
                            ->get()->toArray();
                }

            }
        }

        

        //dd(DB::getQueryLog());
        return $resultSet;
    }

    public static function getallShipmentDataAutoparts($param, $export = 0) {
        $user = new User;
        $shipment = new Procurement;
        $shipmentItem = new Procurementitem;
        $city = new City;
        $country = new Country;
        $state = new State;
        $paymentmethod = new Paymentmethod;
        $adminUser = new UserAdmin;

        $userTable = $user->prefix . $user->table;
        $shipmentTable = $user->prefix . $shipment->table;
        $shipmentItemTable = $user->prefix . $shipmentItem->table;
        $cityTable = $shipment->prefix . $city->table;
        $countryTable = $user->prefix . $country->table;
        $stateTable = $user->prefix . $state->table;
        $paymentmethodTable = $user->prefix . $paymentmethod->table;
        $adminUserTable = $user->prefix . $adminUser->table;

        $where = "$userTable.deleted = '0' AND $shipmentTable.deleted = '0' AND $shipmentTable.procurementType = 'autopart'";

        if (isset($param['searchAccount']['createdOn']) && !empty($param['searchAccount']['createdOn'])) {
            if ($param['searchAccount']['createdOn'] == 'thismonth')
                $where .= "  AND MONTH($shipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchAccount']['createdOn'] == 'thisweek')
                $where .= "  AND $shipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchAccount']['createdOn'] == 'today')
                $where .= "  AND date($shipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchAccount']['createdOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchAccount']['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($shipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

         if (isset($param['searchAccount']['accountType']) && !empty($param['searchAccount']['accountType'])) {
            if ($param['searchAccount']['accountType'] == "paid") {

                if ($export == 0) {
                    $resultSet = Procurement::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit, $paymentmethodTable.paymentMethod, $shipmentTable.paymentReceivedOn as paymentDate, (CASE WHEN $shipmentTable.paidByUserType='user' THEN 'Automatic' ELSE CONCAT($adminUserTable.firstName, ' ',$adminUserTable.lastName) END) as processor")
                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                            ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                            ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                            ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                            ->leftJoin($paymentmethod->table, "$paymentmethod->table.id", '=', "$shipment->table.paymentMethodId")
                            ->leftjoin($adminUser->table, "$adminUser->table.id", '=', "$shipment->table.paidByUserId")
                            ->whereRaw($where)
                            ->where("$shipment->table.paymentStatus", "paid")
                            ->groupBy($shipment->table . ".id")
                            ->orderBy($shipment->table . ".createdOn", $param['type'])
                            ->paginate($param['searchDisplay']);
                } else {
                    $resultSet = Procurement::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit, $paymentmethodTable.paymentMethod, $shipmentTable.paymentReceivedOn as paymentDate, (CASE WHEN $shipmentTable.paidByUserType='user' THEN 'Automatic' ELSE CONCAT($adminUserTable.firstName, ' ',$adminUserTable.lastName) END) as processor")
                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                            ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                            ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                            ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                            ->leftJoin($paymentmethod->table, "$paymentmethod->table.id", '=', "$shipment->table.paymentMethodId")
                            ->leftjoin($adminUser->table, "$adminUser->table.id", '=', "$shipment->table.paidByUserId")
                            ->whereRaw($where)
                            ->where("$shipment->table.paymentStatus", "paid")
                            ->groupBy($shipment->table . ".id")
                            ->orderBy($shipment->table . ".createdOn", $param['type'])
                            ->get()->toArray();
                }
            }else{

                    if ($export == 0) {
                        $resultSet = Procurement::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit")
                                ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                                ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                                ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                                ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                                ->whereRaw($where)
                                ->where("$shipment->table.paymentStatus", "unpaid")
                                ->groupBy($shipment->table . ".id")
                                ->orderBy($shipment->table . ".createdOn", $param['type'])
                                ->paginate($param['searchDisplay']);
                    } else {
                        $resultSet = Procurement::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit")
                                ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                                ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                                ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                                ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                                ->whereRaw($where)
                                ->where("$shipment->table.paymentStatus", "unpaid")
                                ->groupBy($shipment->table . ".id")
                                ->orderBy($shipment->table . ".createdOn", $param['type'])
                                ->get()->toArray();
                    }

            }
        }



        return $resultSet;
    }

    public static function getallShipmentDataBuyacar($param, $export = 0) {
        $user = new User;
        $shipment = new Procurement;
        $shipmentItem = new Procurementitem;
        $city = new City;
        $country = new Country;
        $state = new State;
        $paymentmethod = new Paymentmethod;
        $adminUser = new UserAdmin;

        $userTable = $user->prefix . $user->table;
        $shipmentTable = $user->prefix . $shipment->table;
        $shipmentItemTable = $user->prefix . $shipmentItem->table;
        $cityTable = $shipment->prefix . $city->table;
        $countryTable = $user->prefix . $country->table;
        $stateTable = $user->prefix . $state->table;
        $paymentmethodTable = $user->prefix . $paymentmethod->table;
        $adminUserTable = $user->prefix . $adminUser->table;

        $where = "$userTable.deleted = '0' AND $shipmentTable.deleted = '0' AND $shipmentTable.procurementType = 'buycarforme'";

        if (isset($param['searchAccount']['createdOn']) && !empty($param['searchAccount']['createdOn'])) {
            if ($param['searchAccount']['createdOn'] == 'thismonth')
                $where .= "  AND MONTH($shipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchAccount']['createdOn'] == 'thisweek')
                $where .= "  AND $shipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchAccount']['createdOn'] == 'today')
                $where .= "  AND date($shipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchAccount']['createdOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchAccount']['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($shipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }
         if (isset($param['searchAccount']['accountType']) && !empty($param['searchAccount']['accountType'])) {
            if ($param['searchAccount']['accountType'] == "paid") {
                    if ($export == 0) {
                        $resultSet = Procurement::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city, $countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit, $paymentmethodTable.paymentMethod, $shipmentTable.paymentReceivedOn as paymentDate, (CASE WHEN $shipmentTable.paidByUserType='user' THEN 'Automatic' ELSE CONCAT($adminUserTable.firstName, ' ',$adminUserTable.lastName) END) as processor")
                                ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                                ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                                ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                                ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                                ->leftJoin($paymentmethod->table, "$paymentmethod->table.id", '=', "$shipment->table.paymentMethodId")
                                ->leftjoin($adminUser->table, "$adminUser->table.id", '=', "$shipment->table.paidByUserId")
                                ->whereRaw($where)
                                ->where("$shipment->table.paymentStatus", "paid")
                                ->groupBy($shipment->table . ".id")
                                ->orderBy($shipment->table . ".createdOn", $param['type'])
                                ->paginate($param['searchDisplay']);
                    } else {
                        $resultSet = Procurement::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city, $countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit, $paymentmethodTable.paymentMethod, $shipmentTable.paymentReceivedOn as paymentDate, (CASE WHEN $shipmentTable.paidByUserType='user' THEN 'Automatic' ELSE CONCAT($adminUserTable.firstName, ' ',$adminUserTable.lastName) END) as processor")
                                        ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                                        ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                                        ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                                        ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                                        ->leftJoin($paymentmethod->table, "$paymentmethod->table.id", '=', "$shipment->table.paymentMethodId")
                                        ->leftjoin($adminUser->table, "$adminUser->table.id", '=', "$shipment->table.paidByUserId")
                                        ->whereRaw($where)
                                        ->where("$shipment->table.paymentStatus", "paid")
                                        ->groupBy($shipment->table . ".id")
                                        ->orderBy($shipment->table . ".createdOn", $param['type'])
                                        ->get()->toArray();
                    }
                }else{

                    if ($export == 0) {
                        $resultSet = Procurement::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city, $countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit")
                                ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                                ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                                ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                                ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                                ->whereRaw($where)
                                ->where("$shipment->table.paymentStatus", "unpaid")
                                ->groupBy($shipment->table . ".id")
                                ->orderBy($shipment->table . ".createdOn", $param['type'])
                                ->paginate($param['searchDisplay']);
                    } else {
                        $resultSet = Procurement::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city, $countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit")
                                ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                                ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                                ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                                ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                                ->whereRaw($where)
                                ->where("$shipment->table.paymentStatus", "unpaid")
                                ->groupBy($shipment->table . ".id")
                                ->orderBy($shipment->table . ".createdOn", $param['type'])
                                ->get()->toArray();
                    }

            }
        }



        return $resultSet;
    }

    public static function getallShipmentDataShipacar($param, $export = 0) {
        $user = new User;
        $shipment = new Autoshipment;
        $city = new City;
        $country = new Country;
        $state = new State;
        $paymentmethod = new Paymentmethod;
        $adminUser = new UserAdmin;

        $userTable = $user->prefix . $user->table;
        $shipmentTable = $user->prefix . $shipment->table;
        $cityTable = $shipment->prefix . $city->table;
        $countryTable = $user->prefix . $country->table;
        $stateTable = $user->prefix . $state->table;
        $paymentmethodTable = $user->prefix . $paymentmethod->table;
        $adminUserTable = $user->prefix . $adminUser->table;

        $where = "$userTable.deleted = '0' AND $shipmentTable.deleted = '0'";

        if (isset($param['searchAccount']['createdOn']) && !empty($param['searchAccount']['createdOn'])) {
            if ($param['searchAccount']['createdOn'] == 'thismonth')
                $where .= "  AND MONTH($shipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchAccount']['createdOn'] == 'thisweek')
                $where .= "  AND $shipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchAccount']['createdOn'] == 'today')
                $where .= "  AND date($shipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchAccount']['createdOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchAccount']['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($shipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        if (isset($param['searchAccount']['accountType']) && !empty($param['searchAccount']['accountType'])) {
            if ($param['searchAccount']['accountType'] == "paid") {
                if ($export == 0) {
                    $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city, $countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit, $paymentmethodTable.paymentMethod, $shipmentTable.paymentReceivedOn as paymentDate, (CASE WHEN $shipmentTable.paidByUserType='user' THEN 'Automatic' ELSE CONCAT($adminUserTable.firstName, ' ',$adminUserTable.lastName) END) as processor")
                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                            ->leftJoin($country->table, "$shipment->table.destinationCountry", '=', "$country->table.id")
                            ->leftJoin($state->table, "$shipment->table.destinationState", '=', "$state->table.id")
                            ->leftJoin($city->table, "$shipment->table.destinationCity", '=', "$city->table.id")
                            ->leftJoin($paymentmethod->table, "$paymentmethod->table.id", '=', "$shipment->table.paymentMethodId")
                            ->leftjoin($adminUser->table, "$adminUser->table.id", '=', "$shipment->table.paidByUserId")
                            ->whereRaw($where)
                            ->where("$shipment->table.paymentStatus", "paid")
                            ->groupBy($shipment->table . ".id")
                            ->orderBy($param['field'], $param['type'])
                            ->paginate($param['searchDisplay']);
                } else {
                    $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city, $countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit, $paymentmethodTable.paymentMethod, $shipmentTable.paymentReceivedOn as paymentDate, (CASE WHEN $shipmentTable.paidByUserType='user' THEN 'Automatic' ELSE CONCAT($adminUserTable.firstName, ' ',$adminUserTable.lastName) END) as processor")
                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                            ->leftJoin($country->table, "$shipment->table.destinationCountry", '=', "$country->table.id")
                            ->leftJoin($state->table, "$shipment->table.destinationState", '=', "$state->table.id")
                            ->leftJoin($city->table, "$shipment->table.destinationCity", '=', "$city->table.id")
                           ->leftJoin($paymentmethod->table, "$paymentmethod->table.id", '=', "$shipment->table.paymentMethodId")
                            ->leftjoin($adminUser->table, "$adminUser->table.id", '=', "$shipment->table.paidByUserId")
                            ->whereRaw($where)
                            ->where("$shipment->table.paymentStatus", "paid")
                            ->groupBy($shipment->table . ".id")
                            ->orderBy($param['field'], $param['type'])
                            ->get()->toArray();
                }
            }
            else{
                if ($export == 0) {
                    $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city, $countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit")
                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                            ->leftJoin($country->table, "$shipment->table.destinationCountry", '=', "$country->table.id")
                            ->leftJoin($state->table, "$shipment->table.destinationState", '=', "$state->table.id")
                            ->leftJoin($city->table, "$shipment->table.destinationCity", '=', "$city->table.id")
                            ->whereRaw($where)
                            ->where("$shipment->table.paymentStatus", "paid")
                            ->groupBy($shipment->table . ".id")
                            ->orderBy($param['field'], $param['type'])
                            ->paginate($param['searchDisplay']);
                } else {
                    $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city, $countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit")
                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                            ->leftJoin($country->table, "$shipment->table.destinationCountry", '=', "$country->table.id")
                            ->leftJoin($state->table, "$shipment->table.destinationState", '=', "$state->table.id")
                            ->leftJoin($city->table, "$shipment->table.destinationCity", '=', "$city->table.id")
                            ->whereRaw($where)
                            ->where("$shipment->table.paymentStatus", "paid")
                            ->groupBy($shipment->table . ".id")
                            ->orderBy($param['field'], $param['type'])
                            ->get()->toArray();
                }

            }
        }

        


        return $resultSet;
    }

    public static function getallShipmentShippingMethodData($param, $export = 0) {
        $shipment = new Shipment;
        $shipmentDelivery = new Shipmentdelivery;
        $shippingMethod = new Shippingmethods;


        $shipmentTable = $shipment->prefix . $shipment->table;
        $shipmentDeliveryTable = $shipment->prefix . $shipmentDelivery->table;
        $shippingMethodTable = $shipment->prefix . $shippingMethod->table;

        $where = "$shipmentTable.deleted = '0' AND $shippingMethodTable.deleted = '0'  AND $shipmentDeliveryTable.deleted = '0' AND $shipmentTable.paymentStatus = 'paid' AND $shipmentTable.shippingMethod = 'Y'";

        if (isset($param['searchAccount']['shippingMethodId']) && !empty($param['searchAccount']['shippingMethodId']))
            $where .= " AND  $shipmentDeliveryTable.shippingMethodId=" . $param['searchAccount']['shippingMethodId'];

        if (isset($param['searchAccount']['createdOn']) && !empty($param['searchAccount']['createdOn'])) {
            if ($param['searchAccount']['createdOn'] == 'thismonth')
                $where .= "  AND MONTH($shipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchAccount']['createdOn'] == 'thisweek')
                $where .= "  AND $shipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchAccount']['createdOn'] == 'today')
                $where .= "  AND date($shipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchAccount']['createdOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchAccount']['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($shipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }
        DB::enableQueryLog();
        if ($export == 0) {
            $resultSet = Shipmentdelivery::selectRaw("$shipmentTable.*, SUM($shipmentDeliveryTable.shippingCost) as totalShippingCost, SUM($shipmentDeliveryTable.clearingDutyCost) as totalClearingcost, SUM($shipmentDeliveryTable.inventoryCharge) as totalInventoryCharge")
                    ->leftJoin($shipment->table, "$shipmentDelivery->table.shipmentId", '=', "$shipment->table.id")
                    ->leftJoin($shippingMethod->table, "$shipmentDelivery->table.shippingMethodId", '=', "$shippingMethod->table.shippingid")
                    ->whereRaw($where)
                    ->groupBy($shipment->table . '.id')
                    ->orderBy($param['field'], $param['type'])
                    ->paginate($param['searchDisplay']);
        } else {
            $resultSet = Shipmentdelivery::selectRaw("count($shipmentTable.id) as totalShipment, SUM($shipmentDeliveryTable.shippingCost) as totalShippingCost, SUM($shipmentDeliveryTable.clearingDutyCost) as totalClearingcost, $shipmentTable.totalInsurance,  SUM($shipmentDeliveryTable.inventoryCharge) as totalInventoryCharge")
                    ->leftJoin($shipment->table, "$shipmentDelivery->table.shipmentId", '=', "$shipment->table.id")
                    ->leftJoin($shippingMethod->table, "$shipmentDelivery->table.shippingMethodId", '=', "$shippingMethod->table.shippingid")
                    ->whereRaw($where)
                    ->groupBy($shipment->table . '.id')
                    ->orderBy($param['field'], $param['type'])
                    ->get()->toArray();
        }
        //dd(DB::getQueryLog());

        return $resultSet;
    }

    public static function getallFillshipShippingMethodData($param, $export = 0) {
        $shipment = new Fillship;
        $shippingMethod = new Shippingmethods;
        $user = new User;

        $shipmentTable = $user->prefix . $shipment->table;
        $shippingMethodTable = $user->prefix . $shippingMethod->table;



        $where = "$shipmentTable.deleted = '0' AND $shippingMethodTable.deleted = '0'  AND $shipmentTable.paymentStatus = 'paid' AND $shipmentTable.shippingMethod = 'Y'";

        if (isset($param['searchAccount']['shippingMethodId']) && !empty($param['searchAccount']['shippingMethodId']))
            $where .= " AND  $shipmentTable.shippingMethodId=" . $param['searchAccount']['shippingMethodId'];

        if (isset($param['searchAccount']['createdOn']) && !empty($param['searchAccount']['createdOn'])) {
            if ($param['searchAccount']['createdOn'] == 'thismonth')
                $where .= "  AND MONTH($shipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchAccount']['createdOn'] == 'thisweek')
                $where .= "  AND $shipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchAccount']['createdOn'] == 'today')
                $where .= "  AND date($shipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchAccount']['createdOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchAccount']['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($shipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        if ($export == 0) {
            $resultSet = Fillship::selectRaw("$shipmentTable.id, $shipmentTable.totalShippingCost, $shipmentTable.totalClearingDuty as totalClearingcost, $shipmentTable.totalInsurance")
                    ->leftJoin($shippingMethod->table, "$shipment->table.shippingMethodId", '=', "$shippingMethod->table.shippingid")
                    ->whereRaw($where)
                    ->orderBy($shipment->table . '.createdOn', $param['type'])
                    ->paginate($param['searchDisplay']);
        } else {
            $resultSet = Fillship::selectRaw("count($shipmentTable.id) as totalShipment, $shipmentTable.totalShippingCost, $shipmentTable.totalClearingDuty as totalClearingcost, $shipmentTable.totalInsurance")
                            ->leftJoin($shippingMethod->table, "$shipment->table.shippingMethodId", '=', "$shippingMethod->table.shippingid")
                            ->whereRaw($where)
                            ->orderBy($shipment->table . '.createdOn', $param['type'])
                            ->get()->toArray();
        }


        return $resultSet;
    }

    public static function getallShipmentDatawithoutStorage($param, $export = 0) {
        $user = new User;
        $shipment = new Shipment;
        $shipmentDelivery = new Shipmentdelivery;
        $city = new City;
        $country = new Country;
        $state = new State;
        $paymentmethod = new Paymentmethod;
        $adminUser = new UserAdmin;

        $userTable = $user->prefix . $user->table;
        $shipmentTable = $user->prefix . $shipment->table;
        $shipmentDeliveryTable = $user->prefix . $shipmentDelivery->table;
        $cityTable = $user->prefix . $city->table;
        $countryTable = $user->prefix . $country->table;
        $stateTable = $user->prefix . $state->table;
        $paymentmethodTable = $user->prefix . $paymentmethod->table;
        $adminUserTable = $user->prefix . $adminUser->table;

        $where = "$userTable.deleted = '0' AND $shipmentTable.deleted = '0' AND $shipmentTable.paymentStatus = 'unpaid'";

        if (isset($param['searchAccount']['storagedays']) && !empty($param['searchAccount']['storagedays'])) {
            $where .= " AND  $shipmentDeliveryTable.maxStorageDate > CURDATE() AND $shipmentDeliveryTable.maxStorageDate <= (CURDATE() + INTERVAL " . $param['searchAccount']['storagedays'] . " DAY) ";
        }


        if (isset($param['searchAccount']['createdOn']) && !empty($param['searchAccount']['createdOn'])) {
            if ($param['searchAccount']['createdOn'] == 'thismonth')
                $where .= "  AND MONTH($shipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchAccount']['createdOn'] == 'thisweek')
                $where .= "  AND $shipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchAccount']['createdOn'] == 'today')
                $where .= "  AND date($shipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchAccount']['createdOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchAccount']['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($shipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }
         if (isset($param['searchAccount']['accountType']) && !empty($param['searchAccount']['accountType'])) {
            if ($param['searchAccount']['accountType'] == "paid") {
                if ($export == 0) {
                    $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit, $paymentmethodTable.paymentMethod, $shipmentTable.paymentReceivedOn as paymentDate, (CASE WHEN $shipmentTable.paidByUserType='user' THEN 'Automatic' ELSE CONCAT($adminUserTable.firstName, ' ',$adminUserTable.lastName) END) as processor")
                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                            ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                            ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                            ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                            ->leftJoin($shipmentDelivery->table, "$shipment->table.id", '=', "$shipmentDelivery->table.shipmentId")                            
                            ->leftJoin($paymentmethod->table, "$paymentmethod->table.id", '=', "$shipment->table.paymentMethodId")
                            ->leftjoin($adminUser->table, "$adminUser->table.id", '=', "$shipment->table.paidByUserId")
                            ->whereRaw($where)
                            ->where("$shipment->table.paymentStatus", "paid")
                            ->groupBy($shipment->table . ".id")
                            ->orderBy($param['field'], $param['type'])
                            ->paginate($param['searchDisplay']);
                } else {
                    $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit, $paymentmethodTable.paymentMethod, $shipmentTable.paymentReceivedOn as paymentDate, (CASE WHEN $shipmentTable.paidByUserType='user' THEN 'Automatic' ELSE CONCAT($adminUserTable.firstName, ' ',$adminUserTable.lastName) END) as processor")
                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                            ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                            ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                            ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                            ->leftJoin($shipmentDelivery->table, "$shipment->table.id", '=', "$shipmentDelivery->table.shipmentId")                            
                            ->leftJoin($paymentmethod->table, "$paymentmethod->table.id", '=', "$shipment->table.paymentMethodId")
                            ->leftjoin($adminUser->table, "$adminUser->table.id", '=', "$shipment->table.paidByUserId")
                            ->whereRaw($where)
                            ->where("$shipment->table.paymentStatus", "paid")
                            ->groupBy($shipment->table . ".id")
                            ->orderBy($param['field'], $param['type'])
                            ->get()->toArray();
                }
            }
            else{
                if ($export == 0) {
                    $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit")
                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                            ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                            ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                            ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                            ->leftJoin($shipmentDelivery->table, "$shipment->table.id", '=', "$shipmentDelivery->table.shipmentId")
                            ->whereRaw($where)
                            ->groupBy($shipment->table . ".id")
                            ->orderBy($param['field'], $param['type'])
                            ->paginate($param['searchDisplay']);
                } else {
                    $resultSet = Shipment::selectRaw("$shipmentTable.*, CONCAT($userTable.firstName,' ',$userTable.lastName) as name, CONCAT($userTable.isdCode,' ',$userTable.contactNumber) as contactNumber, $cityTable.name as city,$countryTable.name as country, $stateTable.name as state, $userTable.email, $userTable.unit")
                            ->leftjoin($user->table, "$user->table.id", '=', "$shipment->table.userId")
                            ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                            ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                            ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                            ->leftJoin($shipmentDelivery->table, "$shipment->table.id", '=', "$shipmentDelivery->table.shipmentId")
                            ->whereRaw($where)
                            ->groupBy($shipment->table . ".id")
                            ->orderBy($param['field'], $param['type'])
                            ->get()->toArray();
                }

            }
        }

        

        // dd(DB::getQueryLog());
        return $resultSet;
    }

    public static function getValueofitemsReport($param, $export = 0) {

        $shipment = new Shipment;
        $shipmentItems = new Shipmentpackage;
        $user = new User;
        $city = new City;
        $country = new Country;
        $state = new State;
        $paymentmethod = new Paymentmethod;
        $adminUser = new UserAdmin;

        $shipmentTable = $shipment->prefix . $shipment->table;
        $userTable = $user->prefix . $user->table;
        $paymentmethodTable = $user->prefix . $paymentmethod->table;
        $adminUserTable = $user->prefix . $adminUser->table;

        $where = "$shipmentTable.deleted = '0'";
        $having = "1";
        //print_r($param);exit;
        if ($param['searchAccount']['createdOn'] != '') {
            if ($param['searchAccount']['createdOn'] == 'thismonth')
                $where .= "  AND MONTH($shipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchAccount']['createdOn'] == 'thisweek')
                $where .= "  AND $shipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchAccount']['createdOn'] == 'today')
                $where .= "  AND date($shipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchAccount']['createdOn'] == 'custom' && !empty($param['searchAccount']['searchByDate'])) {
                $searchDate = explode('-', $param['searchAccount']['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($shipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        if (!empty($param['searchAccount']['unitNumber'])) {
            $where .= " AND $userTable.unit = '" . $param['searchAccount']['unitNumber'] . "'";
        }

        if (!empty($param['searchAccount']['fromItemValue'])) {
            $having .= " AND sum(itemTotalCost) >= " . $param['searchAccount']['fromItemValue'];
        }

        if (!empty($param['searchAccount']['toItemValue'])) {
            $having .= " AND sum(itemTotalCost) <= " . $param['searchAccount']['toItemValue'];
        }
        if (isset($param['searchAccount']['accountType']) && !empty($param['searchAccount']['accountType'])) {
            if ($param['searchAccount']['accountType'] == "paid") {
                 if ($export == 0) {
                    $resultSet = Shipment::select(array("$shipment->table.*", DB::raw("sum(itemTotalCost) as itemTotalCost"), DB::raw("count(shipmentId) as numItems"), DB::raw("CONCAT($userTable.firstName,' ',$userTable.lastName) as customer"),"unit", "$user->table.email", "contactNumber", "toCountry", "toState", "toCity", "$city->table.name as city", "$country->table.name as country", "$state->table.name as state", "$paymentmethod->table.paymentMethod", "$shipment->table.paymentReceivedOn as paymentDate", DB::raw("(CASE WHEN $shipmentTable.paidByUserType='user' THEN 'Automatic' ELSE CONCAT($adminUserTable.firstName, ' ',$adminUserTable.lastName) END) as processor")))
                        ->leftJoin("$shipmentItems->table", "$shipment->table.id", "=", "$shipmentItems->table.shipmentId")
                        ->leftJoin("$user->table", "$user->table.id", "=", "$shipment->table.userId")
                        ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                        ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                        ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                        ->leftJoin($paymentmethod->table, "$paymentmethod->table.id", '=', "$shipment->table.paymentMethodId")
                        ->leftjoin($adminUser->table, "$adminUser->table.id", '=', "$shipment->table.paidByUserId")
                        ->whereRaw($where)
                        ->where("$shipment->table.paymentStatus", "paid")
                        ->groupBy("$shipment->table.id")
                        ->havingRaw($having)
                        ->orderBy($param['field'], $param['type'])
                        ->paginate($param['searchDisplay']);
                } else {
                    $resultSet = Shipment::select(array("$shipment->table.*", DB::raw("sum(itemTotalCost) as itemTotalCost"), DB::raw("count(shipmentId) as numItems"), DB::raw("CONCAT($userTable.firstName,' ',$userTable.lastName) as customer"), "unit", "$user->table.email", "contactNumber", "toCountry", "toState", "toCity", "$city->table.name as city", "$country->table.name as country", "$state->table.name as state", "$paymentmethod->table.paymentMethod", "$shipment->table.paymentReceivedOn as paymentDate", DB::raw("(CASE WHEN $shipmentTable.paidByUserType='user' THEN 'Automatic' ELSE CONCAT($adminUserTable.firstName, ' ',$adminUserTable.lastName) END) as processor")))
                            ->leftJoin("$shipmentItems->table", "$shipment->table.id", "=", "$shipmentItems->table.shipmentId")
                            ->leftJoin("$user->table", "$user->table.id", "=", "$shipment->table.userId")
                            ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                            ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                            ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                            ->leftJoin($paymentmethod->table, "$paymentmethod->table.id", '=', "$shipment->table.paymentMethodId")
                            ->leftjoin($adminUser->table, "$adminUser->table.id", '=', "$shipment->table.paidByUserId")
                            ->whereRaw($where)
                            ->where("$shipment->table.paymentStatus", "paid")
                            ->havingRaw($having)
                            ->groupBy("$shipment->table.id")
                            ->orderBy($param['field'], $param['type'])
                            ->get()->toArray();
                }
            }else{
                 if ($export == 0) {
                    $resultSet = Shipment::select(array("$shipment->table.*", DB::raw("sum(itemTotalCost) as itemTotalCost"), DB::raw("count(shipmentId) as numItems"), DB::raw("CONCAT($userTable.firstName,' ',$userTable.lastName) as customer"),"unit", "$user->table.email", "contactNumber", "toCountry", "toState", "toCity", "$city->table.name as city", "$country->table.name as country", "$state->table.name as state"))
                        ->leftJoin("$shipmentItems->table", "$shipment->table.id", "=", "$shipmentItems->table.shipmentId")
                        ->leftJoin("$user->table", "$user->table.id", "=", "$shipment->table.userId")
                        ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                        ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                        ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                        ->whereRaw($where)
                        ->where("$shipment->table.paymentStatus", "unpaid")
                        ->groupBy("$shipment->table.id")
                        ->havingRaw($having)
                        ->orderBy($param['field'], $param['type'])
                        ->paginate($param['searchDisplay']);
                } else {
                    $resultSet = Shipment::select(array("$shipment->table.*", DB::raw("sum(itemTotalCost) as itemTotalCost"), DB::raw("count(shipmentId) as numItems"), DB::raw("CONCAT($userTable.firstName,' ',$userTable.lastName) as customer"), "unit", "$user->table.email", "contactNumber", "toCountry", "toState", "toCity", "$city->table.name as city", "$country->table.name as country", "$state->table.name as state"))
                        ->leftJoin("$shipmentItems->table", "$shipment->table.id", "=", "$shipmentItems->table.shipmentId")
                        ->leftJoin("$user->table", "$user->table.id", "=", "$shipment->table.userId")
                        ->leftJoin($country->table, "$shipment->table.toCountry", '=', "$country->table.id")
                        ->leftJoin($state->table, "$shipment->table.toState", '=', "$state->table.id")
                        ->leftJoin($city->table, "$shipment->table.toCity", '=', "$city->table.id")
                        ->whereRaw($where)
                        ->where("$shipment->table.paymentStatus", "unpaid")
                        ->havingRaw($having)
                        ->groupBy("$shipment->table.id")
                        ->orderBy($param['field'], $param['type'])
                        ->get()->toArray();
                }

            }
        }

       

        //dd(DB::getQueryLog());        
        return $resultSet;
    }

    public static function exportAccountData($param) {
        //print_r($param);exit;

        $records = array();
        $exportData = array();
        if (isset($param['searchByCondition']) && !empty($param['searchByCondition'])) {

            if ($param['searchByCondition'] == 'valueofitem') {
                $headers = array(
                    'id' => 'Shipment#',
                    'unit' => 'Unit Number',
                    'customer' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'city' => 'City',
                    'state' => 'State',
                    'country' => 'Country',
                    'createdOn' => 'Created On',
                    'numItems' => 'Number of Items',
                    'itemTotalCost' => 'Item Total Cost',
                    'paymentStatus' => 'Payment Status',
                );

                $records = Shipment::getValueofitemsReport($param, 1);
            } elseif ($param['searchByCondition'] == 'allshipment') {

                if (isset($param['searchAccount']['accountType']) && !empty($param['searchAccount']['accountType'])) {
                    if ($param['searchAccount']['accountType'] == "paid") {
                        $headers = array(
                            'unit' => 'Unit Number',
                            'name' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city' => 'City',
                            'state' => 'State',
                            'country' => 'Country',
                            'createdOn' => 'Created On',
                            'paymentStatus' => 'Payment Status',
                            'totalCost' => 'Cost of Shipments',
                            'processor' => 'Processor',
                            'paymentMethod'  => 'Payment Method',
                            'paymentDate'  => 'Payment Date',
                        );


                        $records = Shipment::getallShipmentData($param, 1);
                    }else{
                         $headers = array(
                            'unit' => 'Unit Number',
                            'name' => 'Name',
                            'email' => 'Email',
                            'contactNumber' => 'Contact',
                            'city' => 'City',
                            'state' => 'State',
                            'country' => 'Country',
                            'createdOn' => 'Created On',
                            'paymentStatus' => 'Payment Status',
                            'totalCost' => 'Cost of Shipments',
                        );


                        $records = Shipment::getallShipmentData($param, 1);

                    }
                }
            } elseif ($param['searchByCondition'] == 'withstoragecharge') {
                $headers = array(
                    'id' => 'Shipment#',
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'city' => 'City',
                    'state' => 'State',
                    'country' => 'Country',
                    'createdOn' => 'Created On',
                    'totalCost' => 'Cost of Shipments',
                    'maxStorageDate' => 'Storage Expiry Date',
                );

                $records = Shipment::getallShipmentDatawithStorage($param, 1);
            } elseif ($param['searchByCondition'] == 'shopforme') {
                $headers = array(
                    'id' => 'Shipment#',
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'city' => 'City',
                    'state' => 'State',
                    'country' => 'Country',
                    'createdOn' => 'Created On',
                    'totalItemCost' => 'Cost of Items',
                    'totalProcessingFee' => 'Processing Fees',
                    'totalShippingCost' => 'Total Shipping Cost',
                    'totalProcurementCost' => 'Shop for me Cost',
                    'totalCost' => 'Total Cost'
                );


                $records = Shipment::getallShipmentDataShopforme($param, 1);
            } elseif ($param['searchByCondition'] == 'autopart') {
                $headers = array(
                    'id' => 'Shipment#',
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'city' => 'City',
                    'state' => 'State',
                    'country' => 'Country',
                    'createdOn' => 'Created On',
                    'totalItemCost' => 'Cost of Items',
                    'totalProcessingFee' => 'Processing Fees',
                    'totalShippingCost' => 'Total Shipping Cost',
                    'totalProcurementCost' => 'Auto Parts Cost',
                    'totalCost' => 'Total Cost'
                );

                $records = Shipment::getallShipmentDataAutoparts($param, 1);
            } elseif ($param['searchByCondition'] == 'buyacar') {
                $headers = array(
                    'id' => 'Shipment#',
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'city' => 'City',
                    'state' => 'State',
                    'country' => 'Country',
                    'createdOn' => 'Created On',
                    'totalItemCost' => 'Cost of Items',
                    'totalProcessingFee' => 'Processing Fees',
                    'totalShippingCost' => 'Total Shipping Cost',
                    'totalProcurementCost' => 'Buy a Car Cost',
                    'totalCost' => 'Total Cost'
                );


                $records = Shipment::getallShipmentDataBuyacar($param, 1);
            } elseif ($param['searchByCondition'] == 'shipacar') {
                $headers = array(
                    'id' => 'Shipment#',
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'city' => 'City',
                    'state' => 'State',
                    'country' => 'Country',
                    'createdOn' => 'Created On',
                    'totalItemCost' => 'Cost of Items',
                    'totalProcessingFee' => 'Processing Fees',
                    'totalShippingCost' => 'Total Shipping Cost',
                    'totalProcurementCost' => 'Buy a Car Cost',
                    'totalCost' => 'Total Cost'
                );


                $records = Shipment::getallShipmentDataShipacar($param, 1);
            } elseif ($param['searchByCondition'] == 'revenuewithshippingmethod') {
                if (isset($param['searchAccount']['tabType']) && !empty($param['searchAccount']['tabType'])) {
                    if ($param['searchAccount']['tabType'] == "shipment") {
                        $headers = array(
                            'totalShipment' => 'Total No of Shipment',
                            'totalShippingCost' => 'Total Shipping Cost',
                            'totalClearingcost' => 'Total Clearing Charge',
                            'totalDispatchCharge' => 'Total Dispatch Cost',
                            'totalInsurance' => 'Total Insurance',
                            'totalCustomDuty' => 'Custom Duty',
                            'totalInventoryCharge' => 'Total Inventory Charge'
                        );

                        $records = Shipment::getallShipmentShippingMethodData($param, 1);
                    } else if ($param['searchAccount']['tabType'] == "fillship") {
                        $headers = array(
                            'totalShipment' => 'Total No of Shipment',
                            'totalShippingCost' => 'Total Shipping Cost',
                            'totalClearingcost' => 'Total Clearing Charge',
                            'totalDispatchCharge' => 'Total Dispatch Cost',
                            'totalInsurance' => 'Total Insurance',
                            'totalCustomDuty' => 'Custom Duty'
                        );

                        $records = Shipment::getallFillshipShippingMethodData($param, 1);
                    }
                } else {
                    $headers = array(
                        'totalShipment' => 'Total No of Shipment',
                        'totalShippingCost' => 'Total Shipping Cost',
                        'totalClearingcost' => 'Total Clearing Charge',
                        'totalDispatchCharge' => 'Total Dispatch Cost',
                        'totalInsurance' => 'Total Insurance',
                        'totalCustomDuty' => 'Custom Duty',
                        'totalInventoryCharge' => 'Total Inventory Charge'
                    );

                    $records = Shipment::getallShipmentShippingMethodData($param, 1);
                }

                for ($i = 0; $i < count($records); $i++) {
                    $records[$i]['totalDispatchCharge'] = "0.00";
                    $records[$i]['totalCustomDuty'] = "0.00";
                }
            } elseif ($param['searchByCondition'] == 'withoutstoragecharge') {
                $headers = array(
                    'id' => 'Shipment#',
                    'unit' => 'Unit Number',
                    'name' => 'Name',
                    'email' => 'Email',
                    'contactNumber' => 'Contact',
                    'city' => 'City',
                    'state' => 'State',
                    'country' => 'Country',
                    'createdOn' => 'Created On',
                    'totalCost' => 'Cost of Shipments',
                    'maxStorageDate' => 'Storage Expiry Date',
                );


                $records = Shipment::getallShipmentDatawithoutStorage($param, 1);
            }
        }

        if (!empty($records)) {
            foreach ($records as $index => $data) {
                foreach ($headers as $fieldName => $fieldDisplayName) {
                    $exportData[$index][$fieldDisplayName] = $data[$fieldName];
                }
            }
        }


        return $exportData;
    }

    public static function getShipmentLocations($shipmentids) {

        $viewShipment = new Viewshipment;
        $viewShipmentDetails = new ViewShipmentDetails;
        $shipmentWarehouse = new Shipmentwarehouselocation;
        $warehouselocation = new Warehouselocation;
        $shipmentLabel = new Shipmentlabelprintlog;

        $viewShipmentTable = $viewShipment->prefix . $viewShipment->table;
        $viewShipmentDetailsTable = $viewShipmentDetails->prefix . $viewShipmentDetails->table;
        $shipmentWarehouseTable = $viewShipment->prefix . $shipmentWarehouse->table;
        $warehouselocationTable = $viewShipment->prefix . $warehouselocation->table;
        $shipmentLabelTable = $shipmentLabel->prefix . $shipmentLabel->table;

        $where = "$viewShipmentTable.deleted = '0'";

        $resultSet = Viewshipment::select(array("$viewShipment->table.id", DB::raw("CONCAT(" . $viewShipmentTable . ".dispatchCompany,'<br><strong>Estimated Date:</strong> <br>'," . $viewShipmentTable . ".expectedDispatchDate) as dispatchcompany"), DB::raw("GROUP_CONCAT(CONCAT(" . $viewShipment->prefix . "warehouseRow.name,' '," . $viewShipment->prefix . "warehouseZone.name) SEPARATOR ',') as shipmentLocation"),
                ))
                ->leftJoin($shipmentWarehouse->table, "$shipmentWarehouse->table.shipmentId", '=', "$viewShipment->table.id")
                ->leftJoin("$warehouselocation->table AS warehouseRow", "warehouseRow.id", '=', "$shipmentWarehouse->table.warehouseRowId")
                ->leftJoin("$warehouselocation->table AS warehouseZone", "warehouseZone.id", '=', "$shipmentWarehouse->table.warehouseZoneId")
                ->whereRaw("$viewShipmentTable.id in (" . $shipmentids . ")")
                ->where("$viewShipment->table.deleted", "0")
                ->groupBy("$viewShipment->table.id")
                ->orderBy('id', 'desc')
                ->get();
        return $resultSet;
    }

}
