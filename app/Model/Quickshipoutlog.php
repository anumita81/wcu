<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Quickshipoutlog extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.QUICKSHIPOUTLOG');
        $this->prefix = DB::getTablePrefix();
    }
    
    public static function getNotificationLog($quickShipoutId) {
        
        $return = array();
        $data = Quickshipoutlog::where('quickshipoutId',$quickShipoutId)->orderBy('id','asc')->get();
        if(!empty($data))
        {
            $counter = 0;
            foreach($data as $eachdata)
            {
                $return[$counter]['emailSent'] = $eachdata->emailSent;
                $return[$counter]['smsSent'] = $eachdata->smsSent;
                $return[$counter]['notificationId'] = $eachdata->id;
                $counter++;
            }
        }
        
        return $return;
    }
}