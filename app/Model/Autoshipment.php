<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
use Auth;
use PDF;
use Mail;

class Autoshipment extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.AUTOSHIPMENT');
        $this->prefix = DB::getTablePrefix();
    }

    public static function allStatus() {
        return array('submitted'=>'Shipment Submiited','orderplaced'=>'Booked','pickedup'=>'Picked Up','received'=>'In Warehouse','customverification'=>'Customs Verification','sailed'=>'Sailed','customclearing'=>'Customs Clearing','availpickup'=>'Available for Pick Up','completed'=>'Completed');
    }

    public static function getAutoshipmentList($param) {
        $shipment = new Autoshipment;
        $user = new User;
        
        $where = "1";

        $shipmentTable = $shipment->prefix . $shipment->table;
        $userTable = $shipment->prefix . $user->table;
        
        $where .= "  AND $shipmentTable.deleted ='0'";
        if (!empty($param)) {
            
            if (!empty($param['searchAutoshipment']['unit']))
                $where .= " AND $userTable.unit = '" . $param['searchAutoshipment']['unit'] . "'";
            if (!empty($param['searchAutoshipment']['useremail']))
                $where .= " AND $userTable.email = '" . $param['searchAutoshipment']['useremail'] . "'";
            
            if (!empty($param['searchAutoshipment']['shipment']))
                $where .= " AND $shipmentTable.id = '" . $param['searchAutoshipment']['shipment'] . "'";
            if (!empty($param['searchAutoshipment']['shipmentStatus']))
                $where .= " AND $shipmentTable.shipmentStatus = '" . $param['searchAutoshipment']['shipmentStatus'] . "'";
            
            if(!empty($param['searchAutoshipment']['vinNumber']))
                $where .= " AND $shipmentTable.vinNumber = '". $param['searchAutoshipment']['vinNumber'] ."'";
            
            if(!empty($param['searchAutoshipment']['make']))
                $where .= " AND $shipmentTable.makeId = '". $param['searchAutoshipment']['make'] ."'";
            
            if(!empty($param['searchAutoshipment']['model']))
                $where .= " AND $shipmentTable.modelId = '". $param['searchAutoshipment']['model'] ."'";
            
            if(!empty($param['searchAutoshipment']['year']))
                $where .= " AND $shipmentTable.year = '". $param['searchAutoshipment']['year'] ."'";
            
            if(!empty($param['searchAutoshipment']['color']))
                $where .= " AND $shipmentTable.color = '". $param['searchAutoshipment']['color'] ."'";
            
            if(!empty($param['searchAutoshipment']['carCondition']))
                $where .= " AND $shipmentTable.carCondition = '". $param['searchAutoshipment']['carCondition'] ."'";
            
            if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($shipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $shipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($shipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($shipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }
        }

        $resultSet = Autoshipment::select(array("$shipment->table.*", "$user->table.unit AS userUnit", "$user->table.firstName", "$user->table.lastName"))
                ->leftJoin($user->table, "$shipment->table.userId", '=', "$user->table.id")
                ->whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    public static function getWarehouseList() {
        $country = new Country;
        $warehouse = new Warehouse;
       

        $resultSet = Warehouse::where("$warehouse->table.deleted", '0')
                ->where("$warehouse->table.status", '1')
                ->select(array("$warehouse->table.id", "$country->table.name AS countryName"))
                ->leftJoin($country->table, "$warehouse->table.countryId", '=', "$country->table.id")
                ->orderBy("name", "asc")
                ->get();

        return $resultSet;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Autoshipment::where('id', $id)->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }
    
    public static function  saveShipMyCar($postData,$userId,$warehouseId) {
        
        $save = 1;
        $transactionId = 0;
        $paymentStatus = 'unpaid';
        $transactionErrorMsg = array();
        $paymentErrorMessage = "";
        DB::beginTransaction();
        $userDetails = User::find($userId);
        $warehouseDetails = Warehouse::find($warehouseId);
        $userBillingAddress = \App\Model\Addressbook::where('userId', $userId)->where('isDefaultBilling', '1')->first();
        $autoShipment = new Autoshipment;
        $postDataArr = $postData->all();
        $skipArray = array('tempDataId','payeezyData','vehicleImage','paypalData','pickupPhoneCode','receiverPhoneCode','locationTypeName','paystackData','paystackCreatedReference','insuranceCost','totalcostWithoutDiscount','point','couponId','ewalletId','cardNumber','expMonth','expYear','cardCode','cardType','paymentMethodKey','poNumber','companyName','buyerName','position','paymentMethodId','paymentMethodName','isInsuranceCharged','itemImage','carTitle','type','makeName','modelName','countryName','stateName','cityName','itemImagePath','currencySymbol','shippingCost','carUrl','price','vinnumber','shippingCountryName','shippingStateName','shippingCityName');
        foreach($postDataArr as $field => $value)
        {
            if(in_array($field, $skipArray))
                continue;
            if($field == 'pickupDate')
            {
                $dateString = substr($value,0,(strrpos($value,'(')-1));
                $value = date('Y-m-d',strtotime($dateString));
            }
            
            if($field == 'defaultCurrency')
                $autoShipment->defaultCurrencyCode = $value;
            else if($field == 'currencyCode')
                $autoShipment->paidCurrencyCode = $value;
            else if($field == 'tax')
                $autoShipment->taxCost = $value;
            else if($field == 'couponCode')
                $autoShipment->couponcodeApplied = $value;
            else
                $autoShipment->$field = $value;
        }
        if (isset($postDataArr['isInsuranceCharged']) && $postDataArr['isInsuranceCharged'] == 'Y') {
            $autoShipment->insuranceCost = $postDataArr['insuranceCost'];
        }
        else
        {
            $autoShipment->insuranceCost = '0.00';
        }
        
        /* Online payment processing */
        
        /*if($postData->paymentMethodKey == 'paypal_checkout')
        {
            $checkoutData = array();
            $checkoutData['cardNumber'] = $postData->cardNumber;
            $checkoutData['expMonth'] = $postData->expMonth;
            $checkoutData['expYear'] = $postData->expYear;
            $checkoutData['cardCode'] = $postData->cardCode;
            $checkoutData['customerFirstName'] = $userDetails->firstName;
            $checkoutData['customerLastName'] = $userDetails->lastName;
            $checkoutData['customerAddress'] = $userBillingAddress->address;
            $checkoutData['customerCity'] = isset($userBillingAddress->cityId)?$userBillingAddress->city->name : "";
            $checkoutData['customerState'] = isset($userBillingAddress->stateId)?$userBillingAddress->state->name : "";
            $checkoutData['customerCountry'] = $userBillingAddress->country->name;
            $checkoutData['customerZip'] = $userBillingAddress->zipcode;
            $checkoutData['amount'] = round($postData->totalCost,2);
            $checkoutData['defaultCurrency'] = $postData->defaultCurrency;
            $checkoutReturn = Paymenttransaction::paypaypalpro($checkoutData);
            //print_r($checkoutReturn);exit;
            if($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'uccessWithWarning')
            {
                $trasactionId = $checkoutReturn['TRANSACTIONID'];
                $paymentStatus = 'paid';
            }
            else
                return 'payment_error';
        } */
        if ($postData->paymentMethodKey == 'bank_online_transfer' || $postData->paymentMethodKey == 'bank_pay_at_bank' || $postData->paymentMethodKey == 'bank_account_pay' || $postData->paymentMethodKey == 'check_cash_payment')
            $paymentStatus = 'paid';
        
        if($postData->paymentMethodKey == 'credit_debit_card')
        {
            $getPaymentGateway = Paymentmethod::where('paymentMethodKey','credit_debit_card')->first();
            
            $shippingNameParts = explode(' ',$postData->recceiverName);
            $checkoutData = array();
            $checkoutData['cardNumber'] = $postData->cardNumber;
            $checkoutData['expMonth'] = $postData->expMonth;
            $checkoutData['expYear'] = $postData->expYear;
            $checkoutData['cardCode'] = $postData->cardCode;
            $checkoutData['customerFirstName'] = $userDetails->firstName;
            $checkoutData['customerLastName'] = $userDetails->lastName;
            $checkoutData['customerAddress'] = $userBillingAddress->address;
            $checkoutData['customerCity'] = isset($userBillingAddress->cityId)?$userBillingAddress->city->name : "";
            $checkoutData['customerState'] = isset($userBillingAddress->stateId)?$userBillingAddress->state->name : "";
            $checkoutData['customerCountry'] = $userBillingAddress->country->name;
            $checkoutData['customerZip'] = $userBillingAddress->zipcode;
            $checkoutData['amount'] = round($postData->totalCost,2);
            $checkoutData['defaultCurrency'] = $postData->defaultCurrency;
            /* Payment gateway paypal */
            if($getPaymentGateway->paymentGatewayId == '1')
            {
                $checkoutReturn = Paymenttransaction::paypaypalpro($checkoutData);
                
                if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'successWithWarning')) {
                    $paymentStatus = 'paid';
                    $transactionId = $checkoutReturn['TRANSACTIONID'];
                    $transactionData = json_encode($checkoutReturn);
                } else if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Failure')) {
                    $paymentStatus = 'failed';
                    $transactionErrorMsg = json_encode($checkoutReturn);
                    $paymentErrorMessage = $checkoutReturn["L_LONGMESSAGE0"];
                    
                } else if ($checkoutReturn == 'error') {
                    $paymentStatus = 'failed';
                    $paymentErrorMessage = "Payment Error";
                }
                
                //print_r($checkoutReturn);exit;
                /*if(is_array($checkoutReturn) && ($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'uccessWithWarning'))
                {
                    $trasactionId = $checkoutReturn['TRANSACTIONID'];
                    $transactionData = json_encode($checkoutReturn);
                    $paymentStatus = 'paid';
                }
                else
                    return 'payment_error';*/
            }
            else
            {
                $checkoutData['customerShippingAddress'] = $postData->receiverAddress;
                $checkoutData['customerShippingCity'] = $postData->shippingCityName;
                $checkoutData['customerShippingState'] = $postData->shippingStateName;
                $checkoutData['customerShippingCountry'] = $postData->shippingCountryName;
                $checkoutData['customerShippingZip'] = '';
                $checkoutData['shippingFirstName'] = $shippingNameParts[0];
                $checkoutData['shippingLastName'] = $shippingNameParts[1];

                $checkoutReturn = Paymenttransaction::payauthorizedotnet($checkoutData);
                
                if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Ok') {
                    $paymentStatus = 'paid';
                    $transactionId = $checkoutReturn['transactionResponse']['transId'];
                    $transactionData = json_encode($checkoutReturn['transactionResponse']);
                } else if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Error') {
                    $paymentStatus = 'failed';
                    $transactionErrorMsg = json_encode($checkoutReturn['transactionResponse']);
                    if(isset($checkoutReturn['transactionResponse']['errors']['error'][0]))
                        $paymentErrorMessage = 'Payment failed. '.$checkoutReturn['transactionResponse']['errors']['error'][0]['errorText'];
                    else
                        $paymentErrorMessage = 'Payment failed. '.$checkoutReturn['transactionResponse']['errors']['error']['errorText'];
                }

                /*if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Ok') {
                        $paymentStatus = 'paid';
                    $trasactionId = $checkoutReturn['transactionResponse']['transId'];
                    $transactionData = json_encode($checkoutReturn['transactionResponse']);
                    }
                    else
                    {
                        return 'payment_error';
                    }*/
            }
        }
        else if($postData->paymentMethodKey == 'ewallet')
        {
            if(isset($postData->ewalletId) && $postData->ewalletId!='')
            {
                $ewalletData = Ewallet::find($postData->ewalletId);
                $ewalletData->amount = round(($ewalletData->amount-$postData->totalCost),2);
                $ewalletData->debit = $ewalletData->debit + $postData->totalCost;
                $ewalletData->save();
                
                $ewalletTransaction = new Ewallettransaction;
                $ewalletTransaction->userId = $userId;
                $ewalletTransaction->ewalletId = $postData->ewalletId;
                $ewalletTransaction->amount = $postData->totalCost;
                $ewalletTransaction->transactionType = 'debit';
                $ewalletTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                $ewalletTransaction->save();
                
                $paymentStatus = 'paid';
                
            }
            
        } else if ($postData->paymentMethodKey == 'paystack_checkout') {
            
            if (!empty($postData->paystackData)) {

                if (!empty($postData->paystackData['trans']))
                    $findRecord = \App\Model\Paystackreferencehistory::whereRaw("referenceNo = $postData->paystackCreatedReference and processed='0'")->first();
                else
                    $findRecord = \App\Model\Paystackreferencehistory::whereRaw("referenceNo = $postData->paystackCreatedReference")->first();
                if (!empty($findRecord)) {
                    $paymentTransacionData = \App\Model\Paymenttransaction::where("transactionData", "like", '%"reference":"' . $postData->paystackCreatedReference . '"%')->first();
                    if (empty($paymentTransacionData) || (!empty($paymentTransacionData) && $paymentTransacionData->status == 'unpaid')) {
                        \App\Model\Paystackreferencehistory::where("id", $findRecord->id)->update(["processed" => "1"]);
                        if (!empty($postData->paystackData)) {
                            $checkoutReturn = Paymenttransaction::paystack($postData->paystackCreatedReference);
                            if ($checkoutReturn) {
                                $paymentStatus = 'paid';
                                if (!empty($postData->paystackData['trans']))
                                    $trasactionId = $postData->paystackData['trans'];
                                else if (!empty($postData->paystackData['id']))
                                    $transactionId = $postData->paystackData['id'];
                                else
                                    $transactionId = '123456789';
                                $transactionData = json_encode($postData->paystackData);
                            }
                            else {
                                $paymentStatus = 'failed';
                                $transactionErrorMsg = json_encode($postData->paystackData);
                                $paymentErrorMessage = "Payment Failed";
                            }
                        }
                    }
                }
                

//                $checkoutReturn = Paymenttransaction::paystack($postData->paystackCreatedReference);
//                if($checkoutReturn)
//                {
//                    $paymentStatus = 'paid';
//                    $transactionId = $postData->paystackData['trans'];
//                    $transactionData = json_encode($postData->paystackData);
//                }
//                else
//                {
//                    $paymentStatus = 'failed';
//                    $transactionErrorMsg = json_encode($postData->paystackData);
//                    $paymentErrorMessage = "Payment Failed";
//                }
                        
            }
            
            if($paymentStatus == 'pastackProcessed')
            {
                return array('pasytack_processed'=>'1');
            }
            
        } else if($postData->paymentMethodKey == 'paypalstandard') {
            if(!empty($postData->paypalData))
            {
                $paymentStatus = 'paid';
                $transactionId = $postData->paypalData['orderID'];
                $transactionData = json_encode($postData->paypalData);
            }
        } else if($postData->paymentMethodKey == 'payeezy') {
            
            if(!empty($postData->payeezyData)) {
                $checkoutData['amount'] = $postData->payeezyData["paidAmount"];
                $checkoutData['method'] = $postData->payeezyData["paymentType"];
                $checkoutData['currency_code'] = "USD";
                $checkoutData['type'] = $postData->payeezyData["cardType"];
                $checkoutData['cardholder_name'] = $postData->payeezyData["cardHolderName"];
                $checkoutData['card_number'] = $postData->payeezyData['ccardNumber'];
                $checkoutData['exp_date'] = $postData->payeezyData['expiryMonth'].substr($postData->payeezyData['expiryYear'],2);
                $checkoutData['cvv'] = $postData->payeezyData['cvvCode'];
                $checkoutData['userUnit'] = (!empty($userDetails->unit) ? $userDetails->unit : "");
                $checkoutReturn = Paymenttransaction::processpayeezy($checkoutData);
                
                if(is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'success' && !empty($checkoutReturn['transaction_status']) && $checkoutReturn['transaction_status'] == 'approved')
                {
                    $paymentStatus = 'paid';
                    $paymentMode = 'online';
                    $transactionId = $checkoutReturn['transaction_id'];
                    $transactionData = json_encode($checkoutReturn);
                }
                else if(is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'failed')
                {
                    $paymentStatus = 'failed';
                    $paymentErrorMessage = 'Payment failed. ' .$checkoutReturn['Error']['messages'][0]['description'];
                }
                else
                {
                    $paymentStatus = 'failed';
                    $paymentErrorMessage = 'Payment failed. Something Went Wrong ';
                }
                
            } else {
                $paymentStatus = 'error';
            }
        }
       

        $autoShipment->paymentStatus = $paymentStatus;
        $autoShipment->paymentMethodId = $postData->paymentMethodId;
        $autoShipment->userId = $userId;
        $autoShipment->warehouseId = $warehouseId;
        $autoShipment->shipmentStatus = 'submitted';
        $autoShipment->createdBy = $userId;
        $autoShipment->createdByType = 'user';
        $autoShipment->createdOn = Config::get('constants.CURRENTDATE');
        
        $autoShipment->save();
        $autoShipmentId = $autoShipment->id;
        
        if(!empty($autoShipmentId)) {
            if(isset($postData->tempDataId) && !empty($postData->tempDataId)) {

                $tempCartData = \App\Model\Temporarycartdata::find($postData->tempDataId);
                $tempCartData->isDataSaved = 'Y';
                $tempCartData->dataSavedId = $autoShipmentId;
                $tempCartData->save();
            }
        }
        
        $autoshipmentStatusLog = new Autoshipmentstatuslog;
        $autoshipmentStatusLog->autoshipmentId = $autoShipmentId;
        $autoshipmentStatusLog->oldStatus = 'none';
        $autoshipmentStatusLog->status = 'submitted';
        $autoshipmentStatusLog->updatedOn = Config::get('constants.CURRENTDATE');
        $autoshipmentStatusLog->save();
        if($postData->itemImage!='')
        {
            $autoshipmentImage = new Autoshipmentitemimage;
            $autoshipmentImage->autoShipmentId = $autoShipmentId;
            $autoshipmentImage->imageType = 'carpicture';
            $autoshipmentImage->filename = $postData->itemImage;
            $autoshipmentImage->status = '1';
            $autoshipmentImage->createdBy = $userId;
            $autoshipmentImage->createdByType = 'user';
            $autoshipmentImage->createdOn = Config::get('constants.CURRENTDATE');
            $autoshipmentImage->save();
        }
        if($postData->carTitle!='')
        {
            $autoshipmentImage = new Autoshipmentitemimage;
            $autoshipmentImage->autoShipmentId = $autoShipmentId;
            $autoshipmentImage->imageType = 'cartitle';
            $autoshipmentImage->filename = $postData->carTitle;
            $autoshipmentImage->status = '1';
            $autoshipmentImage->createdBy = $userId;
            $autoshipmentImage->createdByType = 'user';
            $autoshipmentImage->createdOn = Config::get('constants.CURRENTDATE');
            $autoshipmentImage->save();
        }
        
        if(isset($postData->couponCode) && $postData->couponCode != '')
        {
            if(isset($postData->point) && $postData->point!='')
            {
                $pointData = Fundpoint::where('userId',$userId)->first();
                if(count($pointData)>0)
                {
                    Fundpoint::where('userId',$userId)->update(['point'=>($pointData->point+$postData->point)]);
                }
                else
                {
                    $fundPoint = new Fundpoint;
                    $fundPoint->userId = $userId;
                    $fundPoint->point = $postData->point;
                    $fundPoint->save();
                }
            }
        
            $logData['code'] = $postData->couponCode;
            $logData['userId'] = $postData->userId;
            $logData['totalAmount'] = $postData->totalcostWithoutDiscount;
            $logData['couponId'] = $postData->couponId;
            if($postData->discountAmount!='' || $postData->discountAmount!='0.00')
            {
                $logData['discountAmount'] = $postData->discountAmount;
            }
            if($postData->point!='' || $postData->point!='0.00')
            {
                $logData['discountPoint'] = $postData->point;
            }
            
            Couponlog::insertLog($logData);
        }
        
        $param = array();
        $param['userId'] = $userId;
        $param['paidAmount'] = round(($postData->shippingCost+$postData->pickupCost),2);
        Fundpoint::calculateReferralBonus($param);
        
        Usercart::where('userId',$userId)->where('type','ship_my_car')->delete();
        
        if ($paymentStatus == 'failed' && $paymentErrorMessage != '') {

            $paymentTransaction = new Paymenttransaction;
            $paymentTransaction->userId = $userId;
            $paymentTransaction->paymentMethodId = $postData->paymentMethodId;
            $paymentTransaction->paidFor = 'shipacar';
            $paymentTransaction->status = $paymentStatus;
            $paymentTransaction->paidForId = '';
            $paymentTransaction->amountPaid = $postData->totalCost;
            $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
            if (!empty($transactionId)) {
                $paymentTransaction->transactionId = $transactionId;
                $paymentTransaction->transactionData = $transactionData;
            }
            if ($postData->paymentMethodKey == 'wire_transfer') {
                $paymentTransaction->transactionData = json_encode(array(
                    'poNumber' => $postData->poNumber,
                    'companyName' => $postData->companyName,
                    'buyerName' => $postData->buyerName,
                    'position' => $postData->position,
                ));
            }
            if (!empty($transactionErrorMsg))
                $paymentTransaction->errorMsg = $transactionErrorMsg;
            $paymentTransaction->save();
            DB::commit();
            return array('payment_error' => '1', 'error_msg' => $paymentErrorMessage);
        } else {
            
            /* Create order of auto shipments */
            $orderObj = new \App\Model\Order;
            $orderObj->shipmentId = $autoShipmentId;
            $orderObj->userId = $userId;
            $orderObj->totalCost = $postData->totalCost;
            $orderObj->status = '2';
            $orderObj->type = 'autoshipment';
            $orderObj->createdBy = $userId;
            $orderObj->save();
            $orderNumber = \App\Model\Order::generateOrderNumber($orderObj->id, $orderObj->userId);
            $orderObj->orderNumber = $orderNumber;
            $orderObj->save();

            /*  PREPARE DATA FOR INVOICE PARTICULARS */

            $invoiceData = array(
                'shipment' => array(
                    'totalItemCost' => $postData->price,
                    'totalProcessingFee' => '0.00',
                    'urgentPurchaseCost' => '0.00',
                    'pickupCost' => $postData->pickupCost,
                    'shippingCost' => $postData->shippingCost,
                    'totalProcurementCost' => '0.00',
                    'insuranceCost' => '0.00',
                    'taxCost' => round($postData->tax, 2),
                    'discount' => $postData->discountAmount,
                    'point' => $postData->point,
                    'currencySymbol' => $postData->currencySymbol,
                    'currencyCode' => $postData->currencyCode,
                    'exchangeRate' => round($postData->exchangeRate, 2),
                ),
                'payment' => array(
                    'paymentMethodId' => $postData->paymentMethodId,
                    'paymentMethodName' => $postData->paymentMethodName,
                    'paymentMethodKey' => $postData->paymentMethodKey,
                    'poNumber' => '',
                    'companyName' => '',
                    'buyerName' => '',
                    'position' => '',
                    'cardNumber' => '',
                    'cardType' => '',
                ),
                'warehouse' => array(
                    'address' => $warehouseDetails->address,
                    'zipcode' => $warehouseDetails->zipcode,
                    'fromCountry' => $postData->countryName,
                    'fromState' => $postData->stateName,
                    'fromCity' => $postData->cityName,
                ),
                'itemDetails' => array(
                    'itemId' => $autoShipmentId,
                    'url' => $postData->carUrl,
                    'make' => $postData->makeName,
                    'model' => $postData->modelName,
                    'year' => $postData->year,
                    'color' => $postData->color,
                ),
                'shippingaddress' => array(
                    'toCountry' => $postData->shippingCountryName,
                    'toState' => $postData->shippingStateName,
                    'toCity' => $postData->shippingCityName,
                    'toAddress' => $postData->receiverAddress,
                    'toZipCode' => '',
                    'toName' => $postData->recceiverName,
                    'toEmail' => $postData->receiverEmail,
                    'toPhone' => $postData->receiverPhone,
                )
            );

            if (isset($postData->isInsuranceCharged) && $postData->isInsuranceCharged == 'Y') {
                $invoiceData['shipment']['insuranceCost'] = $postData->insuranceCost;
            }
            if ($postData->paymentMethodKey == 'wire_transfer') {
                $invoiceData['payment']['poNumber'] = $postData->poNumber;
                $invoiceData['payment']['companyName'] = $postData->companyName;
                $invoiceData['payment']['buyerName'] = $postData->buyerName;
                $invoiceData['payment']['position'] = $postData->position;
            }
            if ($postData->paymentMethodKey == 'paypal_checkout' || $postData->paymentMethodKey == 'credit_debit_card') {
                $invoiceData['payment']['cardNumber'] = $postData->cardNumber;
                $invoiceData['payment']['cartType'] = $postData->cardType;
            }

            /*  INSERT DATA INTO INVOICE TABLE */
            //$invoiceUniqueId = 'INV' . $userDetails->unit . '-' . $autoShipmentId . '-' . date('Ymd');
            $invoice = new Invoice;
            if ($paymentStatus == 'unpaid') {
                $invoiceUniqueId = 'INV' . $userDetails->unit . '-' . $autoShipmentId . '-' . date('Ymd');
                $invoice->invoiceType = 'invoice';
            } else if ($paymentStatus == 'paid') {
                $invoiceUniqueId = 'RECP' . $userDetails->unit . '-' . $autoShipmentId . '-' . date('Ymd');
                $invoice->invoiceType = 'receipt';
            }
            $invoice->invoiceUniqueId = $invoiceUniqueId;
            $invoice->shipmentId = $autoShipmentId;
            $invoice->type = 'autoshipment';
            $invoice->userUnit = $userDetails->unit;
            $invoice->userFullName = $userDetails->firstName . ' ' . $userDetails->lastName;
            $invoice->userEmail = $userDetails->email;
            $invoice->userContactNumber = $userDetails->contactNumber;
            $invoice->billingName = $userBillingAddress->title . ' ' . $userBillingAddress->firstName . ' ' . $userBillingAddress->lastName;
            $invoice->billingEmail = $userBillingAddress->email;
            $invoice->billingAddress = $userBillingAddress->address;
            $invoice->billingAlternateAddress = $userBillingAddress->alternateAddress;
            $invoice->billingCity = isset($userBillingAddress->city) ? $userBillingAddress->city->name : '';
            $invoice->billingState = isset($userBillingAddress->state) ? $userBillingAddress->state->name : '';
            $invoice->billingCountry = $userBillingAddress->country->name;
            $invoice->billingZipcode = $userBillingAddress->zipcode;
            $invoice->billingPhone = '+' . $userBillingAddress->isdCode . ' ' . $userBillingAddress->phone;
            if ($userBillingAddress->altIsdCode != '')
                $invoice->billingAlternatePhone = '+' . $userBillingAddress->altIsdCode . ' ' . $userBillingAddress->alternatePhone;
            else
                $invoice->billingAlternatePhone = $userBillingAddress->alternatePhone;
            $invoice->totalBillingAmount = $postData->totalCost;
            $invoice->invoiceParticulars = json_encode($invoiceData);
            if (isset($postData->paymentMethodId) && $postData->paymentMethodId != '')
                $invoice->paymentMethodId = $postData->paymentMethodId;
            $invoice->paymentStatus = $paymentStatus;
            $invoice->createdOn = Config::get('constants.CURRENTDATE');
            $invoice->save();

            $invoiceId = $invoice->id;

            $paymentTransaction = new Paymenttransaction;
            $paymentTransaction->userId = $userId;
            $paymentTransaction->paymentMethodId = $postData->paymentMethodId;
            $paymentTransaction->paidFor = 'shipacar';
            $paymentTransaction->status = $paymentStatus;
            $paymentTransaction->paidForId = $autoShipmentId;
            $paymentTransaction->invoiceId = $invoice->id;
            $paymentTransaction->amountPaid = $postData->totalCost;
            $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
            if (!empty($transactionId)) {

                $paymentTransaction->transactionId = $transactionId;
                $paymentTransaction->transactionData = $transactionData;
            }
            if ($postData->paymentMethodKey == 'wire_transfer') {
                $paymentTransaction->transactionData = json_encode(array(
                    'poNumber' => $postData->poNumber,
                    'companyName' => $postData->companyName,
                    'buyerName' => $postData->buyerName,
                    'position' => $postData->position,
                ));
            }
            if (!empty($transactionErrorMsg))
                $paymentTransaction->errorMsg = $transactionErrorMsg;
            $paymentTransaction->save();
            DB::commit();
        }

        if($save == 1)
            return array('autoShipmentId'=>$autoShipmentId,'invoiceId'=>$invoiceId,'userEmail'=>$userDetails->email,'invoiceUniqueId'=>$invoiceUniqueId,'paymentStatus'=>$paymentStatus);
        
    }
    
    public static function saveBuyaCar($procurementId) {
        
        $procurementObj = Procurement::find($procurementId);
        $procurementItemDetails = Procurementitem::where('procurementId',$procurementId)->first();
        $warehouseDetails = Warehouse::find($procurementObj->warehouseId);
        $autoShipment = new Autoshipment;
        DB::beginTransaction();
        $autoShipment->userId = $procurementObj->userId;
        $autoShipment->warehouseId = $procurementObj->warehouseId;
        $autoShipment->pickupAddress = $warehouseDetails->address;
        $autoShipment->pickupCountry = $warehouseDetails->countryId;
        $autoShipment->pickupState = $warehouseDetails->stateId;
        $autoShipment->pickupCity = $warehouseDetails->cityId;
        $autoShipment->destinationCountry = $procurementObj->toCountry;
        $autoShipment->destinationState = $procurementObj->toState;
        $autoShipment->destinationCity = $procurementObj->toCity;
        $autoShipment->receiverAddress = $procurementObj->toAddress;
        $autoShipment->recceiverName = $procurementObj->toName;
        $autoShipment->receiverEmail = $procurementObj->toEmail;
        $autoShipment->receiverPhone = $procurementObj->toPhone;
        $autoShipment->itemPrice = $procurementObj->totalItemCost;
        $autoShipment->pickupCost = $procurementObj->totalPickupCost;
        $autoShipment->insuranceCost = $procurementObj->totalInsurance;
        $autoShipment->shipmentCost = $procurementObj->totalShippingCost;
        $autoShipment->isCurrencyChanged = $procurementObj->isCurrencyChanged;
        $autoShipment->defaultCurrencyCode = $procurementObj->defaultCurrencyCode;
        $autoShipment->paidCurrencyCode = $procurementObj->paidCurrencyCode;
        $autoShipment->exchangeRate = $procurementObj->exchangeRate;
        $autoShipment->taxCost = $procurementObj->totalTax;
        $autoShipment->totalCost = ($procurementObj->totalPickupCost+$procurementObj->totalInsurance+$procurementObj->totalShippingCost+$procurementObj->totalTax);
        $autoShipment->shipmentStatus = 'received';
        $autoShipment->paymentStatus = $procurementObj->paymentStatus;
        $autoShipment->paymentMethodId = $procurementObj->paymentMethodId;
        $autoShipment->paymentReceivedOn = $procurementObj->paymentReceivedOn;
        $autoShipment->discountAmount = $procurementObj->totalDiscount;
        $autoShipment->couponcodeApplied = $procurementObj->couponcodeApplied;
        $autoShipment->websiteLink = $procurementItemDetails->websiteUrl;
        $autoShipment->makeId = $procurementItemDetails->makeId;
        $autoShipment->modelId = $procurementItemDetails->modelId;
        $autoShipment->year = $procurementItemDetails->year;
        $autoShipment->color = $procurementItemDetails->options;
        $autoShipment->milage = $procurementItemDetails->milage;
        $autoShipment->vinNumber = $procurementItemDetails->vinNumber;
        $autoShipment->carCondition = 'drivable';
        $autoShipment->deliveryNotes = $procurementItemDetails->deliveryNotes;
        $autoShipment->createdBy = Auth::user()->id;
        $autoShipment->createdByType = 'admin';
        $autoShipment->createdOn = Config::get('constants.CURRENTDATE');
        $autoShipment->pickupDate = Config::get('constants.CURRENTDATE');
        $autoShipment->save();
        $autoShipmentId = $autoShipment->id;
        
        /* Create order of auto shipments */
        $orderObj = new \App\Model\Order;
        $orderObj->shipmentId = $autoShipmentId;
        $orderObj->userId = $procurementObj->userId;
        $orderObj->totalCost = $autoShipment->totalCost;
        $orderObj->status = '2';
        $orderObj->type = 'autoshipment';
        $orderObj->createdBy = Auth::user()->id;
        $orderObj->save();
        $orderNumber = \App\Model\Order::generateOrderNumber($orderObj->id, $orderObj->userId);
        $orderObj->orderNumber = $orderNumber;
        $orderObj->save();
        
        $procurementLogData = Procurementitemstatus::where('procurementId',$procurementId)->get();
        foreach($procurementLogData as $eachData)
        {
            $autoshipmentStatusLog = new Autoshipmentstatuslog;
            $autoshipmentStatusLog->autoshipmentId = $autoShipmentId;
            $autoshipmentStatusLog->oldStatus = $eachData->oldStatus;
            $autoshipmentStatusLog->status = $eachData->status;
            $autoshipmentStatusLog->updatedOn = $eachData->updatedOn;
            $autoshipmentStatusLog->save();
        }
        
        if($procurementItemDetails->itemImage!='')
        {
            $autoshipmentImage = new Autoshipmentitemimage;
            $autoshipmentImage->autoShipmentId = $autoShipmentId;
            $autoshipmentImage->imageType = 'carpicture';
            $autoshipmentImage->filename = $procurementItemDetails->itemImage;
            $autoshipmentImage->status = '1';
            $autoshipmentImage->createdBy = Auth::user()->id;
            $autoshipmentImage->createdByType = 'admin';
            $autoshipmentImage->createdOn = Config::get('constants.CURRENTDATE');
            $autoshipmentImage->save();
        }
        
        Invoice::where('procurementId',$procurementId)->update(['shipmentId'=>$autoShipmentId]);
        
        $procurementObj->status = 'completed'; 
        $procurementObj->save();
        
        DB::commit();
        return true;
        
    }


    /**
     * Method for get warehouse details for auto section
     * @param interger $id
     * @return object
     */
    public static function getcarwarehousedetails($userId, $perPage, $offset1) {
        $statusExcept = "completed";

        $autoshipment = new Autoshipment;
        $procurementTable = $autoshipment->prefix . $autoshipment->table;

        $procurementItem = new Procurementitem;
        $procurementItemTable = $procurementItem->table;

        $procurementitemstatus = new Procurementitemstatus;
        $procurementItemTable = $procurementitemstatus->table;

        $city = new City;
        $state = new State;
        $country = new Country;
        $autoWebsite = new Autowebsite;
        $make = new Automake;
        $model = new Automodel;
        
        $resultSet = Autoshipment::select(

                    $autoshipment->table.'.id as AutoId', 
                    $autoshipment->table.'.milage', 
                    $autoshipment->table.'.vinNumber',
                    $autoshipment->table.'.websiteLink',
                    $autoshipment->table.'.color',
                    $autoshipment->table.'.carCondition',
                    $autoshipment->table.'.year',
                    
                    $autoshipment->table.'.pickupPhone',
                    $autoshipment->table.'.pickupEmail',
                    $autoshipment->table.'.pickupLocationType',
                    $autoshipment->table.'.pickupDate',
                    $autoshipment->table.'.pickupName',
                    $autoshipment->table.'.pickupAddress',

                    $autoshipment->table.'.recceiverName',
                    $autoshipment->table.'.receiverAddress',
                    $autoshipment->table.'.receiverPhone',
                    $autoshipment->table.'.receiverEmail',

                    $autoshipment->table.'.pickupCost',
                    $autoshipment->table.'.insuranceCost',
                    $autoshipment->table.'.taxCost',
                    $autoshipment->table.'.shipmentCost',
                    $autoshipment->table.'.totalCost',

                    $autoshipment->table.'.isCurrencyChanged',
                    $autoshipment->table.'.exchangeRate',
                    $autoshipment->table.'.paidCurrencyCode',
                    $autoshipment->table.'.defaultCurrencyCode',

                    $autoshipment->table.'.paymentStatus',
                    $autoshipment->table.'.shipmentStatus',
                    $autoshipment->table.'.paymentMethodId',


                    "PFROMCOUNTRY.name AS PFROMCOUNTRY", "DTOCOUNTRY.name AS DTOCOUNTRY", 
                    "PFROMSTATE.name as PFROMSTATE", "DTOSTATE.name as DTOSTATE", "PFROMCITY.name as PFROMCITY", "DTOCITY.name as DTOCITY",
                    "MAKENAME.name as MAKENAME", "MODELNAME.name as MODELNAME")
                    
                    ->leftJoin($country->table . " AS PFROMCOUNTRY", $autoshipment->table.".pickupCountry", '=', "PFROMCOUNTRY.id")
                    ->leftJoin($country->table . " AS DTOCOUNTRY", $autoshipment->table.".destinationCountry", '=', "DTOCOUNTRY.id")
                    ->leftJoin($state->table . " AS PFROMSTATE", $autoshipment->table.".pickupState", '=', "PFROMSTATE.id")
                    ->leftJoin($state->table . " AS DTOSTATE", $autoshipment->table.".destinationState", '=', "DTOSTATE.id")
                    ->leftJoin($city->table . " AS PFROMCITY", $autoshipment->table.".pickupCity", '=', "PFROMCITY.id")
                    ->leftJoin($city->table . " AS DTOCITY", $autoshipment->table.".destinationCity", '=', "DTOCITY.id")

                    ->leftJoin($make->table . " AS MAKENAME", $autoshipment->table.".makeId", '=', "MAKENAME.id")
                    ->leftJoin($model->table . " AS MODELNAME", $autoshipment->table.".modelId", '=', "MODELNAME.id")

                    ->where($autoshipment->table.'.deleted', '0')
                    ->where($autoshipment->table.'.userId', '=', $userId)
                    ->orderBy($autoshipment->table.'.id', 'desc')
                    ->take($perPage)->skip($offset1)
                    ->get();

        return $resultSet;
    }



    /**
     * Method for get warehouse details for auto section
     * @param interger $id
     * @return object
     */
    public static function getautoshipmentdetails($shipmentId) {

        $autoshipment = new Autoshipment;
        $procurementTable = $autoshipment->prefix . $autoshipment->table;

        $city = new City;
        $state = new State;
        $country = new Country;
        $autoWebsite = new Autowebsite;
        $make = new Automake;
        $model = new Automodel;

        $paymentmethod = new Paymentmethod;
        
        $resultSet = Autoshipment::select(

                    $autoshipment->table.'.id as AutoId', 
                    $autoshipment->table.'.milage', 
                    $autoshipment->table.'.vinNumber',
                    $autoshipment->table.'.websiteLink',
                    $autoshipment->table.'.color',
                    $autoshipment->table.'.carCondition',
                    $autoshipment->table.'.year',
                    
                    $autoshipment->table.'.pickupPhone',
                    $autoshipment->table.'.pickupEmail',
                    $autoshipment->table.'.pickupLocationType',
                    $autoshipment->table.'.pickupDate',
                    $autoshipment->table.'.pickupName',
                    $autoshipment->table.'.pickupAddress',

                    $autoshipment->table.'.recceiverName',
                    $autoshipment->table.'.receiverAddress',
                    $autoshipment->table.'.receiverPhone',
                    $autoshipment->table.'.receiverEmail',

                    $autoshipment->table.'.pickupCost',
                    $autoshipment->table.'.insuranceCost',
                    $autoshipment->table.'.taxCost',
                    $autoshipment->table.'.shipmentCost',
                    $autoshipment->table.'.totalCost',

                    $autoshipment->table.'.isCurrencyChanged',
                    $autoshipment->table.'.exchangeRate',
                    $autoshipment->table.'.paidCurrencyCode',
                    $autoshipment->table.'.defaultCurrencyCode',

                    $autoshipment->table.'.paymentStatus',
                    $autoshipment->table.'.shipmentStatus',
                    $autoshipment->table.'.paymentMethodId',

                    $autoshipment->table.'.couponCodeApplied',
                    $autoshipment->table.'.discountAmount',
                   // $autoshipment->table.'.discountCost',


                    "PFROMCOUNTRY.name AS PFROMCOUNTRY", "DTOCOUNTRY.name AS DTOCOUNTRY", 
                    "PFROMSTATE.name as PFROMSTATE", "DTOSTATE.name as DTOSTATE", "PFROMCITY.name as PFROMCITY", "DTOCITY.name as DTOCITY",
                    "MAKENAME.name as MAKENAME", "MODELNAME.name as MODELNAME", "PAYMENTMETHOD.paymentMethod as PAYMETHOD")
                    
                    ->leftJoin($country->table . " AS PFROMCOUNTRY", $autoshipment->table.".pickupCountry", '=', "PFROMCOUNTRY.id")
                    ->leftJoin($country->table . " AS DTOCOUNTRY", $autoshipment->table.".destinationCountry", '=', "DTOCOUNTRY.id")
                    ->leftJoin($state->table . " AS PFROMSTATE", $autoshipment->table.".pickupState", '=', "PFROMSTATE.id")
                    ->leftJoin($state->table . " AS DTOSTATE", $autoshipment->table.".destinationState", '=', "DTOSTATE.id")
                    ->leftJoin($city->table . " AS PFROMCITY", $autoshipment->table.".pickupCity", '=', "PFROMCITY.id")
                    ->leftJoin($city->table . " AS DTOCITY", $autoshipment->table.".destinationCity", '=', "DTOCITY.id")

                    ->leftJoin($make->table . " AS MAKENAME", $autoshipment->table.".makeId", '=', "MAKENAME.id")
                    ->leftJoin($model->table . " AS MODELNAME", $autoshipment->table.".modelId", '=', "MODELNAME.id")

                    ->leftJoin($paymentmethod->table . " AS PAYMENTMETHOD", $autoshipment->table.".paymentMethodId", '=', "PAYMENTMETHOD.id")

                    ->where($autoshipment->table.'.deleted', '0')
                    ->first();

        return $resultSet;
    }

    /**
     * Method for get total warehouse for auto section
     * @param interger $id
     * @return integer
     */
    public static function gettotalcarwarehousedetails($userId) {
        
        $statusExcept = "completed";

        $autoshipment = new Autoshipment;
        $procurementTable = $autoshipment->prefix . $autoshipment->table;

        $procurementItem = new Procurementitem;
        $procurementItemTable = $procurementItem->table;

        $procurementitemstatus = new Procurementitemstatus;
        $procurementItemTable = $procurementitemstatus->table;

        $city = new City;
        $state = new State;
        $country = new Country;
        $autoWebsite = new Autowebsite;
        $make = new Automake;
        $model = new Automodel;
        
        $resultSetCount = Autoshipment::select(

                    $autoshipment->table.'.id as AutoId', 
                    $autoshipment->table.'.milage', 
                    $autoshipment->table.'.vinNumber',
                    $autoshipment->table.'.websiteLink',
                    $autoshipment->table.'.color',
                    $autoshipment->table.'.carCondition',
                    $autoshipment->table.'.year',
                    
                    $autoshipment->table.'.pickupPhone',
                    $autoshipment->table.'.pickupEmail',
                    $autoshipment->table.'.pickupLocationType',
                    $autoshipment->table.'.pickupDate',
                    $autoshipment->table.'.pickupName',
                    $autoshipment->table.'.pickupAddress',

                    $autoshipment->table.'.recceiverName',
                    $autoshipment->table.'.receiverAddress',
                    $autoshipment->table.'.receiverPhone',
                    $autoshipment->table.'.receiverEmail',

                    $autoshipment->table.'.pickupCost',
                    $autoshipment->table.'.insuranceCost',
                    $autoshipment->table.'.taxCost',
                    $autoshipment->table.'.shipmentCost',
                    $autoshipment->table.'.totalCost',

                    $autoshipment->table.'.isCurrencyChanged',
                    $autoshipment->table.'.exchangeRate',
                    $autoshipment->table.'.paidCurrencyCode',
                    $autoshipment->table.'.defaultCurrencyCode',

                    $autoshipment->table.'.paymentStatus',
                    $autoshipment->table.'.shipmentStatus',
                    $autoshipment->table.'.paymentMethodId',


                    "PFROMCOUNTRY.name AS PFROMCOUNTRY", "DTOCOUNTRY.name AS DTOCOUNTRY", 
                    "PFROMSTATE.name as PFROMSTATE", "DTOSTATE.name as DTOSTATE", "PFROMCITY.name as PFROMCITY", "DTOCITY.name as DTOCITY",
                    "MAKENAME.name as MAKENAME", "MODELNAME.name as MODELNAME")
                    
                    ->leftJoin($country->table . " AS PFROMCOUNTRY", $autoshipment->table.".pickupCountry", '=', "PFROMCOUNTRY.id")
                    ->leftJoin($country->table . " AS DTOCOUNTRY", $autoshipment->table.".destinationCountry", '=', "DTOCOUNTRY.id")
                    ->leftJoin($state->table . " AS PFROMSTATE", $autoshipment->table.".pickupState", '=', "PFROMSTATE.id")
                    ->leftJoin($state->table . " AS DTOSTATE", $autoshipment->table.".destinationState", '=', "DTOSTATE.id")
                    ->leftJoin($city->table . " AS PFROMCITY", $autoshipment->table.".pickupCity", '=', "PFROMCITY.id")
                    ->leftJoin($city->table . " AS DTOCITY", $autoshipment->table.".destinationCity", '=', "DTOCITY.id")

                    ->leftJoin($make->table . " AS MAKENAME", $autoshipment->table.".makeId", '=', "MAKENAME.id")
                    ->leftJoin($model->table . " AS MODELNAME", $autoshipment->table.".modelId", '=', "MODELNAME.id")

                    ->where($autoshipment->table.'.deleted', '0')
                    ->where($autoshipment->table.'.userId', '=', $userId)
                    ->orderBy($autoshipment->table.'.id', 'desc')
                    //->take($perPage)->skip($offset1)
                    ->count();

        return $resultSetCount;
    }
    
    public static function fetchUsedFieldIds($field) {
        
        $resultset = Autoshipment::distinct()->whereNotNull($field)->where("deleted", "0")->pluck($field)->toArray();

        return $resultset;
    }


    public static function getAutoshipmentListForReport($param, $type = '', $selected = '') {
        $shipment = new Autoshipment;
        $user = new User;

        $city = new City;
        $state = new State;
        $country = new Country;
        
        $where = "1";

        $shipmentTable = $shipment->prefix . $shipment->table;
        $userTable = $shipment->prefix . $user->table;
        
        $where .= "  AND $shipmentTable.deleted ='0'";
        if (!empty($param)) {
            
            if (!empty($param['searchAutoshipment']['unit']))
                $where .= " AND $userTable.unit = '" . $param['searchAutoshipment']['unit'] . "'";
            if (!empty($param['searchAutoshipment']['useremail']))
                $where .= " AND $userTable.email = '" . $param['searchAutoshipment']['useremail'] . "'";
            
            if (!empty($param['searchAutoshipment']['shipment']))
                $where .= " AND $shipmentTable.id = '" . $param['searchAutoshipment']['shipment'] . "'";
            if (!empty($param['searchAutoshipment']['shipmentStatus']))
                $where .= " AND $shipmentTable.shipmentStatus = '" . $param['searchAutoshipment']['shipmentStatus'] . "'";
            if(isset($param['searchOffer'])){
                $where .= " AND couponcodeApplied = '". $param['searchOffer'] . "'";
            }
            
            if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($shipmentTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $shipmentTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($shipmentTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($shipmentTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }
        }

        if($type == 'export'){

            if (!empty($selected)) {
                $where = "$shipmentTable.id IN (" . implode(',', $selected) . ")";
            }

            
            $resultSet = Autoshipment::select(array(
                "$shipment->table.id as id", 
                "$shipment->table.id as id", 

                "$user->table.unit AS userUnit", 
                DB::raw("CONCAT($userTable.firstName, ' ', $userTable.lastName) AS customer"),
                

                "$shipment->table.totalCost AS totalcost",
                "$shipment->table.discountAmount AS totalDiscount",
                "$state->table.name AS destinationStateName", "$city->table.name AS destinationCityName",
                "$country->table.name AS destinationCountryName",

                "$shipment->table.paymentStatus as paymentstatus",
                "$shipment->table.createdOn AS createdOn"
                ))
                ->leftJoin($user->table, "$shipment->table.userId", '=', "$user->table.id")

                ->leftJoin($state->table, "$state->table.id", '=', "$shipment->table.destinationState")
                ->leftJoin($city->table, "$city->table.id", '=', "$shipment->table.destinationCity")
                ->leftJoin($country->table, "$country->table.id", '=', "$shipment->table.destinationCountry")

                ->whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->get();
        } else {

        $resultSet = Autoshipment::select(array("$shipment->table.*", "$user->table.unit AS userUnit", "$user->table.firstName", "$user->table.lastName",
                "$state->table.name AS toStateName", "$city->table.name AS toCityName",
                "$country->table.name AS toCountryName"
                ))
                ->leftJoin($user->table, "$shipment->table.userId", '=', "$user->table.id")

                ->leftJoin($state->table, "$state->table.id", '=', "$shipment->table.destinationState")
                ->leftJoin($city->table, "$city->table.id", '=', "$shipment->table.destinationCity")
                ->leftJoin($country->table, "$country->table.id", '=', "$shipment->table.destinationCountry")

                ->whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);
        }

        return $resultSet;
    }
    
    /**
     * Method used to resize image and save the same
     * @param string $image_name
     * @param integer $new_width
     * @param integer $new_height
     * @param string $uploadDir
     * @param string $moveToDir
     * @return array
     */
    public static function createThumbnail($image_name,$new_width,$new_height,$uploadDir,$moveToDir)
    { 

        /* CREATING THE PATH OF THE IMAGE */
        $path = $uploadDir . '/' . $image_name;

        $mime = getimagesize($path);


        if($mime['mime']=='image/png') { 
            $src_img = imagecreatefrompng($path);
        }
        if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
            $src_img = imagecreatefromjpeg($path);
        } 
        if($mime['mime']=='image/gif') { 
            $src_img = imagecreatefromgif($path);
        }
        if($mime['mime']=='image/x-ms-bmp') { 
            $src_img = imagecreatefrombmp($path);
        }  

        $old_x          =   imageSX($src_img);
        $old_y          =   imageSY($src_img);

        if($old_x > $old_y) 
        {
            $thumb_w    =   $new_width;
            $thumb_h    =   $old_y*($new_height/$old_x);
        }

        if($old_x < $old_y) 
        {
            $thumb_w    =   $old_x*($new_width/$old_y);
            $thumb_h    =   $new_height;
        }

        if($old_x == $old_y) 
        {
            $thumb_w    =   $new_width;
            $thumb_h    =   $new_height;
        }

        $dst_img        =   ImageCreateTrueColor($thumb_w,$thumb_h);

        imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 


        /* NEW SAVE LOCATION */
        $new_thumb_loc = $moveToDir . $image_name;

        if($mime['mime']=='image/png') {
            $result = imagepng($dst_img,$new_thumb_loc,8);
        }
        if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg' || $mime['mime']=='image/gif' || $mime['mime']=='image/x-ms-bmp') {
            $result = imagejpeg($dst_img,$new_thumb_loc,80);
        }

        imagedestroy($dst_img); 
        imagedestroy($src_img);

        return $result;
    }
    
    public static function getshipmycarallcharges($formData = array()) {

        $results = array(); 
        
        $userId = $formData['userId'];
        $warehouseId = $formData['warehouseId'];
        $itemPrice = "";
        $fromCity = "";
        $insuranceCost = "0.00";
        if (abs($formData['itemPrice']) != '')
            $itemPrice = abs($formData['itemPrice']);
        if ($formData['pickupCity'] != '')
            $fromCity = $formData['pickupCity'];

        $param = array(
            'fromCountry' => $formData['pickupCountry'],
            'fromState' => $formData['pickupState'],
            'fromCity' => $fromCity,
            'toCountry' => $formData['destinationCountry'],
            'toState' => $formData['destinationState'],
            'toCity' => $formData['destinationCity']
        );
        $autoData = array('make' => $formData['makeId'], 'model' => $formData['modelId']);
        $shippingCostsArr = \App\Model\Shippingcost::getShippingCost($itemPrice, $param, $autoData);
        if (!empty($shippingCostsArr)) {
            $shippingCost = $shippingCostsArr['totalShippingCost'];
            $insuranceCost = $shippingCostsArr['insurance'];
        }
        $pickupCost = \App\Model\Autopickupcost::getPickupCost($warehouseId, $fromCity);
        $totalCost = round(($pickupCost + $shippingCost + $insuranceCost), 2);

        $currencyCode = \App\Helpers\customhelper::getCurrencySymbolCode('', true);
        $currencySymbol = \App\Helpers\customhelper::getCurrencySymbolCode($currencyCode);
        $exchangeRate = 1;
        
        $results['currencyCode'] = $currencyCode;
        $results['defaultCurrency'] = $currencyCode;
        $results['currencySymbol'] = $currencySymbol;
        $results['exchangeRate'] = $exchangeRate;
        $results['pickupCost'] = $pickupCost;
        $results['shippingCost'] = $shippingCost;
        $results['totalCost'] = $totalCost;
        $results['insuranceCost'] = $insuranceCost;
        $results['isInsuranceCharged'] = 'Y';
         
        return $results;
    }
    
    public static function processAuthoshipmentPayment($postData, $userId, $warehouseId,$dataSource = 'web') {
        
        
        $transactionId = $save = 0;
        $paymentStatus = 'unpaid';
        $transactionErrorMsg = array();
        $paymentErrorMessage = "";
        $userShippingAddress = \App\Model\Addressbook::where('userId', $userId)->where('isDefaultShipping', '1')->with('country', 'state', 'city')->first();
        $userBillingAddress = \App\Model\Addressbook::where('userId', $userId)->where('isDefaultBilling', '1')->with('country', 'state', 'city')->first();
        $userDetails = User::find($userId);
        $warehouseDetails = Warehouse::find($warehouseId);
        
        DB::beginTransaction();
        
        if($postData->paymentMethodKey == 'credit_debit_card')
        {
            $getPaymentGateway = Paymentmethod::where('paymentMethodKey','credit_debit_card')->first();
            
            $shippingNameParts = explode(' ',$postData->recceiverName);
            $checkoutData = array();
            $checkoutData['cardNumber'] = $postData->cardNumber;
            $checkoutData['expMonth'] = $postData->expMonth;
            $checkoutData['expYear'] = $postData->expYear;
            $checkoutData['cardCode'] = $postData->cardCode;
            $checkoutData['customerFirstName'] = $userDetails->firstName;
            $checkoutData['customerLastName'] = $userDetails->lastName;
            $checkoutData['customerAddress'] = $userBillingAddress->address;
            $checkoutData['customerCity'] = isset($userBillingAddress->cityId)?$userBillingAddress->city->name : "";
            $checkoutData['customerState'] = isset($userBillingAddress->stateId)?$userBillingAddress->state->name : "";
            $checkoutData['customerCountry'] = $userBillingAddress->country->name;
            $checkoutData['customerZip'] = $userBillingAddress->zipcode;
            $checkoutData['amount'] = round($postData->totalCost,2);
            $checkoutData['defaultCurrency'] = $postData->defaultCurrency;
            /* Payment gateway paypal */
            if($getPaymentGateway->paymentGatewayId == '1')
            {
                $checkoutReturn = Paymenttransaction::paypaypalpro($checkoutData);
                
                if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'successWithWarning')) {
                    $paymentStatus = 'paid';
                    $transactionId = $checkoutReturn['TRANSACTIONID'];
                    $transactionData = json_encode($checkoutReturn);
                } else if (isset($checkoutReturn["ACK"]) && ($checkoutReturn["ACK"] == 'Failure')) {
                    $paymentStatus = 'failed';
                    $transactionErrorMsg = json_encode($checkoutReturn);
                    $paymentErrorMessage = $checkoutReturn["L_LONGMESSAGE0"];
                    
                } else if ($checkoutReturn == 'error') {
                    $paymentStatus = 'failed';
                    $paymentErrorMessage = "Payment Error";
                }
                
                //print_r($checkoutReturn);exit;
                /*if(is_array($checkoutReturn) && ($checkoutReturn["ACK"] == 'Success' || $checkoutReturn["ACK"] == 'uccessWithWarning'))
                {
                    $trasactionId = $checkoutReturn['TRANSACTIONID'];
                    $transactionData = json_encode($checkoutReturn);
                    $paymentStatus = 'paid';
                }
                else
                    return 'payment_error';*/
            }
            else
            {
                $checkoutData['customerShippingAddress'] = $postData->receiverAddress;
                $checkoutData['customerShippingCity'] = $postData->shippingCityName;
                $checkoutData['customerShippingState'] = $postData->shippingStateName;
                $checkoutData['customerShippingCountry'] = $postData->shippingCountryName;
                $checkoutData['customerShippingZip'] = '';
                $checkoutData['shippingFirstName'] = $shippingNameParts[0];
                $checkoutData['shippingLastName'] = $shippingNameParts[1];

                $checkoutReturn = Paymenttransaction::payauthorizedotnet($checkoutData);
                
                if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Ok') {
                    $paymentStatus = 'paid';
                    $transactionId = $checkoutReturn['transactionResponse']['transId'];
                    $transactionData = json_encode($checkoutReturn['transactionResponse']);
                } else if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Error') {
                    $paymentStatus = 'failed';
                    $transactionErrorMsg = json_encode($checkoutReturn['transactionResponse']);
                    if(isset($checkoutReturn['transactionResponse']['errors']['error'][0]))
                        $paymentErrorMessage = 'Payment failed. '.$checkoutReturn['transactionResponse']['errors']['error'][0]['errorText'];
                    else
                        $paymentErrorMessage = 'Payment failed. '.$checkoutReturn['transactionResponse']['errors']['error']['errorText'];
                }

                /*if (is_array($checkoutReturn) && $checkoutReturn['messages']['resultCode'] == 'Ok') {
                        $paymentStatus = 'paid';
                    $trasactionId = $checkoutReturn['transactionResponse']['transId'];
                    $transactionData = json_encode($checkoutReturn['transactionResponse']);
                    }
                    else
                    {
                        return 'payment_error';
                    }*/
            }
        }
        else if($postData->paymentMethodKey == 'ewallet')
        {
            if(isset($postData->ewalletId) && $postData->ewalletId!='')
            {
                $ewalletData = Ewallet::find($postData->ewalletId);
                $ewalletData->amount = round(($ewalletData->amount-$postData->totalCost),2);
                $ewalletData->debit = $ewalletData->debit + $postData->totalCost;
                $ewalletData->save();
                
                $ewalletTransaction = new Ewallettransaction;
                $ewalletTransaction->userId = $userId;
                $ewalletTransaction->ewalletId = $postData->ewalletId;
                $ewalletTransaction->amount = $postData->totalCost;
                $ewalletTransaction->transactionType = 'debit';
                $ewalletTransaction->transactionOn = Config::get('constants.CURRENTDATE');
                $ewalletTransaction->save();
                
                $paymentStatus = 'paid';
                
            }
            
        } else if ($postData->paymentMethodKey == 'paystack_checkout') {

            if ($dataSource == 'app') {

                if (!empty($postData->paystackData)) {
                    $checkoutReturn = Paymenttransaction::paystack($postData->paystackCreatedReference);
                    if ($checkoutReturn) {
                        $paymentMode = 'online';
                        $paymentStatus = 'paid';
                        $trasactionId = $postData->paystackData['trans'];
                        $transactionData = json_encode($postData->paystackData);
                    }
                } else
                    $paymentStatus = 'unpaid';
            } else {

                if (!empty($postData->paystackData['trans']))
                    $findRecord = \App\Model\Paystackreferencehistory::whereRaw("referenceNo = $postData->paystackCreatedReference and processed='0'")->first();
                else
                    $findRecord = \App\Model\Paystackreferencehistory::whereRaw("referenceNo = $postData->paystackCreatedReference")->first();
                if (!empty($findRecord)) {
                    $paymentTransacionData = \App\Model\Paymenttransaction::where("transactionData", "like", '%"reference":"' . $postData->paystackCreatedReference . '"%')->first();
                    if (empty($paymentTransacionData) || (!empty($paymentTransacionData) && $paymentTransacionData->status == 'unpaid')) {
                        \App\Model\Paystackreferencehistory::where("id", $findRecord->id)->update(["processed" => "1"]);
                        if (!empty($postData->paystackData)) {
                            $checkoutReturn = Paymenttransaction::paystack($postData->paystackCreatedReference);
                            if ($checkoutReturn) {
                                $paymentStatus = 'paid';
                                if (!empty($postData->paystackData['trans']))
                                    $trasactionId = $postData->paystackData['trans'];
                                else if (!empty($postData->paystackData['id']))
                                    $transactionId = $postData->paystackData['id'];
                                else
                                    $transactionId = '123456789';
                                $transactionData = json_encode($postData->paystackData);
                            }
                            else {
                                $paymentStatus = 'failed';
                                $transactionErrorMsg = json_encode($postData->paystackData);
                                $paymentErrorMessage = "Payment Failed";
                            }
                        }
                    } else {
                        $paymentStatus = 'pastackProcessed';
                    }
                } else {
                    $paymentStatus = 'pastackProcessed';
                }
            }
            
            if($paymentStatus == 'pastackProcessed')
            {
                return array('pasytack_processed'=>'1');
            }
            
        } else if($postData->paymentMethodKey == 'paypalstandard') {
            if(!empty($postData->paypalData))
            {
                $paymentStatus = 'paid';
                $transactionId = $postData->paypalData['orderID'];
                $transactionData = json_encode($postData->paypalData);
            }
        } else if($postData->paymentMethodKey == 'payeezy') {
            
            if(!empty($postData->payeezyData)) {
                $checkoutData['amount'] = $postData->payeezyData["paidAmount"];
                $checkoutData['method'] = $postData->payeezyData["paymentType"];
                $checkoutData['currency_code'] = "USD";
                $checkoutData['type'] = $postData->payeezyData["cardType"];
                $checkoutData['cardholder_name'] = $postData->payeezyData["cardHolderName"];
                $checkoutData['card_number'] = $postData->payeezyData['ccardNumber'];
                $checkoutData['exp_date'] = $postData->payeezyData['expiryMonth'].substr($postData->payeezyData['expiryYear'],2);
                $checkoutData['cvv'] = $postData->payeezyData['cvvCode'];
                $checkoutData['userUnit'] = (!empty($userDetails->unit) ? $userDetails->unit : "");
                $checkoutReturn = Paymenttransaction::processpayeezy($checkoutData);
                
                if(is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'success' && !empty($checkoutReturn['transaction_status']) && $checkoutReturn['transaction_status'] == 'approved')
                {
                    $paymentStatus = 'paid';
                    $paymentMode = 'online';
                    $transactionId = $checkoutReturn['transaction_id'];
                    $transactionData = json_encode($checkoutReturn);
                }
                else if(is_array($checkoutReturn) && !empty($checkoutReturn['validation_status']) && $checkoutReturn['validation_status'] == 'failed')
                {
                    $paymentStatus = 'failed';
                    $paymentErrorMessage = 'Payment failed. ' .$checkoutReturn['Error']['messages'][0]['description'];
                }
                else
                {
                    $paymentStatus = 'failed';
                    $paymentErrorMessage = 'Payment failed. Something Went Wrong ';
                }
                
            } else {
                $paymentStatus = 'error';
            }
        }
        
        $shipmentData = Autoshipment::find($postData->id);
        
        $shipmentData->totalCost = $postData->totalCost;
        $shipmentData->taxCost = $postData->tax;
        $shipmentData->paymentStatus = $paymentStatus;
        $shipmentData->paymentMethodId = $postData->paymentMethodId;
        if (isset($postData->isCurrencyChanged))
            $shipmentData->isCurrencyChanged = $postData->isCurrencyChanged;
        else
            $shipmentData->isCurrencyChanged = 'N';
        $shipmentData->defaultCurrencyCode = $postData->defaultCurrency;
        $shipmentData->paidCurrencyCode = $postData->currencyCode;
        $shipmentData->exchangeRate = $postData->exchangeRate;
        if($shipmentData->save())
            $save = 1;
        
        $autoShipmentId = $postData->id;
        
        if(isset($postData->couponCode) && $postData->couponCode != '')
        {
            if(isset($postData->point) && $postData->point!='')
            {
                $pointData = Fundpoint::where('userId',$userId)->first();
                if(count($pointData)>0)
                {
                    Fundpoint::where('userId',$userId)->update(['point'=>($pointData->point+$postData->point)]);
                }
                else
                {
                    $fundPoint = new Fundpoint;
                    $fundPoint->userId = $userId;
                    $fundPoint->point = $postData->point;
                    $fundPoint->save();
                }
            }
        
            $logData['code'] = $postData->couponCode;
            $logData['userId'] = $postData->userId;
            $logData['totalAmount'] = $postData->totalcostWithoutDiscount;
            $logData['couponId'] = $postData->couponId;
            if($postData->discountAmount!='' || $postData->discountAmount!='0.00')
            {
                $logData['discountAmount'] = $postData->discountAmount;
            }
            if($postData->point!='' || $postData->point!='0.00')
            {
                $logData['discountPoint'] = $postData->point;
            }
            
            Couponlog::insertLog($logData);
        }
        
        $param = array();
        $param['userId'] = $userId;
        $param['paidAmount'] = round(($postData->shippingCost+$postData->pickupCost),2);
        Fundpoint::calculateReferralBonus($param);
        
        Usercart::where('userId',$userId)->where('type','process_autoshipment')->delete();
        
        if ($paymentStatus == 'failed' && $paymentErrorMessage != '') {

            $paymentTransaction = new Paymenttransaction;
            $paymentTransaction->userId = $userId;
            $paymentTransaction->paymentMethodId = $postData->paymentMethodId;
            $paymentTransaction->paidFor = 'shipacar';
            $paymentTransaction->status = $paymentStatus;
            $paymentTransaction->paidForId = '';
            $paymentTransaction->amountPaid = $postData->totalCost;
            $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
            if (!empty($transactionId)) {
                $paymentTransaction->transactionId = $transactionId;
                $paymentTransaction->transactionData = $transactionData;
            }
            if ($postData->paymentMethodKey == 'wire_transfer') {
                $paymentTransaction->transactionData = json_encode(array(
                    'poNumber' => $postData->poNumber,
                    'companyName' => $postData->companyName,
                    'buyerName' => $postData->buyerName,
                    'position' => $postData->position,
                ));
            }
            if (!empty($transactionErrorMsg))
                $paymentTransaction->errorMsg = $transactionErrorMsg;
            $paymentTransaction->save();
            DB::commit();
            return array('payment_error' => '1', 'error_msg' => $paymentErrorMessage);
        } else {
            $orderObj = new \App\Model\Order;
            $orderObj->shipmentId = $autoShipmentId;
            $orderObj->userId = $userId;
            $orderObj->totalCost = $postData->totalCost;
            $orderObj->status = '2';
            $orderObj->type = 'autoshipment';
            $orderObj->createdBy = $userId;
            $orderObj->save();
            $orderNumber = \App\Model\Order::generateOrderNumber($orderObj->id, $orderObj->userId);
            $orderObj->orderNumber = $orderNumber;
            $orderObj->save();

            $invoiceData = array(
                'shipment' => array(
                    'totalItemCost' => $postData->price,
                    'totalProcessingFee' => '0.00',
                    'urgentPurchaseCost' => '0.00',
                    'pickupCost' => $postData->pickupCost,
                    'shippingCost' => $postData->shippingCost,
                    'totalProcurementCost' => '0.00',
                    'insuranceCost' => '0.00',
                    'taxCost' => round($postData->tax, 2),
                    'discount' => $postData->discountAmount,
                    'point' => $postData->point,
                    'currencySymbol' => $postData->currencySymbol,
                    'currencyCode' => $postData->currencyCode,
                    'exchangeRate' => round($postData->exchangeRate, 2),
                ),
                'payment' => array(
                    'paymentMethodId' => $postData->paymentMethodId,
                    'paymentMethodName' => $postData->paymentMethodName,
                    'paymentMethodKey' => $postData->paymentMethodKey,
                    'poNumber' => '',
                    'companyName' => '',
                    'buyerName' => '',
                    'position' => '',
                    'cardNumber' => '',
                    'cardType' => '',
                ),
                'warehouse' => array(
                    'address' => $warehouseDetails->address,
                    'zipcode' => $warehouseDetails->zipcode,
                    'fromCountry' => $postData->countryName,
                    'fromState' => $postData->stateName,
                    'fromCity' => $postData->cityName,
                ),
                'itemDetails' => array(
                    'itemId' => $autoShipmentId,
                    'url' => $postData->carUrl,
                    'make' => $postData->makeName,
                    'model' => $postData->modelName,
                    'year' => $postData->year,
                    'color' => $postData->color,
                ),
                'shippingaddress' => array(
                    'toCountry' => $postData->shippingCountryName,
                    'toState' => $postData->shippingStateName,
                    'toCity' => $postData->shippingCityName,
                    'toAddress' => $postData->receiverAddress,
                    'toZipCode' => '',
                    'toName' => $postData->recceiverName,
                    'toEmail' => $postData->receiverEmail,
                    'toPhone' => $postData->receiverPhone,
                )
            );

            if (isset($postData->isInsuranceCharged) && $postData->isInsuranceCharged == 'Y') {
                $invoiceData['shipment']['insuranceCost'] = $postData->insuranceCost;
            }
            if ($postData->paymentMethodKey == 'wire_transfer') {
                $invoiceData['payment']['poNumber'] = $postData->poNumber;
                $invoiceData['payment']['companyName'] = $postData->companyName;
                $invoiceData['payment']['buyerName'] = $postData->buyerName;
                $invoiceData['payment']['position'] = $postData->position;
            }
            if ($postData->paymentMethodKey == 'paypal_checkout' || $postData->paymentMethodKey == 'credit_debit_card') {
                $invoiceData['payment']['cardNumber'] = $postData->cardNumber;
                $invoiceData['payment']['cartType'] = $postData->cardType;
            }
            /*  INSERT DATA INTO INVOICE TABLE */
            //$invoiceUniqueId = 'INV' . $userDetails->unit . '-' . $autoShipmentId . '-' . date('Ymd');
            $invoice = new Invoice;
            if ($paymentStatus == 'unpaid') {
                $invoiceUniqueId = 'INV' . $userDetails->unit . '-' . $autoShipmentId . '-' . date('Ymd');
                $invoice->invoiceType = 'invoice';
            } else if ($paymentStatus == 'paid') {
                $invoiceUniqueId = 'RECP' . $userDetails->unit . '-' . $autoShipmentId . '-' . date('Ymd');
                $invoice->invoiceType = 'receipt';
            }
            $invoice->invoiceUniqueId = $invoiceUniqueId;
            $invoice->shipmentId = $autoShipmentId;
            $invoice->type = 'autoshipment';
            $invoice->userUnit = $userDetails->unit;
            $invoice->userFullName = $userDetails->firstName . ' ' . $userDetails->lastName;
            $invoice->userEmail = $userDetails->email;
            $invoice->userContactNumber = $userDetails->contactNumber;
            $invoice->billingName = $userBillingAddress->title . ' ' . $userBillingAddress->firstName . ' ' . $userBillingAddress->lastName;
            $invoice->billingEmail = $userBillingAddress->email;
            $invoice->billingAddress = $userBillingAddress->address;
            $invoice->billingAlternateAddress = $userBillingAddress->alternateAddress;
            $invoice->billingCity = isset($userBillingAddress->city) ? $userBillingAddress->city->name : '';
            $invoice->billingState = isset($userBillingAddress->state) ? $userBillingAddress->state->name : '';
            $invoice->billingCountry = $userBillingAddress->country->name;
            $invoice->billingZipcode = $userBillingAddress->zipcode;
            $invoice->billingPhone = '+' . $userBillingAddress->isdCode . ' ' . $userBillingAddress->phone;
            if ($userBillingAddress->altIsdCode != '')
                $invoice->billingAlternatePhone = '+' . $userBillingAddress->altIsdCode . ' ' . $userBillingAddress->alternatePhone;
            else
                $invoice->billingAlternatePhone = $userBillingAddress->alternatePhone;
            $invoice->totalBillingAmount = $postData->totalCost;
            $invoice->invoiceParticulars = json_encode($invoiceData);
            if (isset($postData->paymentMethodId) && $postData->paymentMethodId != '')
                $invoice->paymentMethodId = $postData->paymentMethodId;
            $invoice->paymentStatus = $paymentStatus;
            $invoice->createdOn = Config::get('constants.CURRENTDATE');
            $invoice->save();

            $invoiceId = $invoice->id;

            $paymentTransaction = new Paymenttransaction;
            $paymentTransaction->userId = $userId;
            $paymentTransaction->paymentMethodId = $postData->paymentMethodId;
            $paymentTransaction->paidFor = 'shipacar';
            $paymentTransaction->status = $paymentStatus;
            $paymentTransaction->paidForId = $autoShipmentId;
            $paymentTransaction->invoiceId = $invoice->id;
            $paymentTransaction->amountPaid = $postData->totalCost;
            $paymentTransaction->transactionOn = Config::get('constants.CURRENTDATE');
            if (!empty($transactionId)) {

                $paymentTransaction->transactionId = $transactionId;
                $paymentTransaction->transactionData = $transactionData;
            }
            if ($postData->paymentMethodKey == 'wire_transfer') {
                $paymentTransaction->transactionData = json_encode(array(
                    'poNumber' => $postData->poNumber,
                    'companyName' => $postData->companyName,
                    'buyerName' => $postData->buyerName,
                    'position' => $postData->position,
                ));
            }
            if (!empty($transactionErrorMsg))
                $paymentTransaction->errorMsg = $transactionErrorMsg;
            $paymentTransaction->save();
            
            DB::commit();
        }

        
        if($save == 1)
            return array('autoShipmentId'=>$autoShipmentId,'invoiceId'=>$invoiceId,'userEmail'=>$userDetails->email,'invoiceUniqueId'=>$invoiceUniqueId,'paymentStatus'=>$paymentStatus);
        
        
    }


}
