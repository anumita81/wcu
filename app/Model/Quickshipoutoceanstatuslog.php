<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Quickshipoutoceanstatuslog extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.QUICKSHIPOUTOCEANSTATUSLOG');
        $this->prefix = DB::getTablePrefix();
    }
    
    public static function getStatusLog($quickshipoutId) {
        
        $result = array();
        $statusLogData = Quickshipoutoceanstatuslog::where('quickshipoutId',$quickshipoutId)->get();
        if($statusLogData->count()>0) {
            foreach($statusLogData as $eachData) {
                $result[$eachData->status] = $eachData->updatedOn;
            }
        }
        
        return $result;
    }
    
}
