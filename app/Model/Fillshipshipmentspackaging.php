<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Config;

class Fillshipshipmentspackaging extends Model
{
    public $table;
    public $timestamps = false;

    public function packageaddedby() {
        return $this->hasOne('App\Model\UserAdmin', 'id', 'createdBy');
    }
    
    public function __construct() {

        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.FILLSHIPSHIPMENTPACKAGING');
    }
    
    public static function getpackageLimit($packageChargeableWeight,$deliveryChargeableWeight) {
        
        if($deliveryChargeableWeight==0 || $packageChargeableWeight ==0)
            return array();

        $percent = round((($packageChargeableWeight / $deliveryChargeableWeight) * 100),2);

        $chargeableWeightFactor = \App\Model\Generalsettings::where('settingsKey', 'acceptable_package_error')->first();
        $acceptableError = $chargeableWeightFactor->settingsValue;
        $return = array();
        if ((100-$acceptableError) <= $percent && $percent <= (100 + $acceptableError)) {
            $return['message'] = "<b>Final Package is within acceptable values</b>";
            $return['code'] = 1;

        } elseif ($percent < (100-$acceptableError)) {
            $return['message'] = "<font color='red'>Delivery Dimensions are too small by ".($percent - 100)."%</font>";
            $return['code'] = 2;

        } else {
            $return['message'] = "<font color='blue'>Delivery Dimensions are too big by ".($percent - 100)."%</font>";
            $return['code'] = 3;

        }

        return $return;
    }
}