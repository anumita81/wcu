<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Stores extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $timestamps = false;

    public $prefix;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.STORES');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to fetch store record list
     * @param array $param
     * @return object
     */
    public static function getStoreList($param) {
        $store = new Stores;
        $storeCountry = new Storecountrymapping;
        
        $where = "1";

        $storeTable = $store->prefix . $store->table;
        $storeCountryTable = $store->prefix . $storeCountry->table;



        if (!empty($param['searchStore']))
            $where .= "  AND $storeTable.storeType ='" . $param['searchStore'] . "'";

        if (!empty($param['searchCountry']))
            $where .= "  AND $storeCountryTable.originalCountryId ='" . $param['searchCountry'] . "'";
        if (!empty($param['searchCategory']))
            $where .= "  AND $storeCountryTable.categoryId ='" . $param['searchCategory'] . "'";

        $resultSet = Stores::select(array("$store->table.id", "$store->table.storeName", "$store->table.storeURL", "$store->table.storeIcon", "$store->table.storeType", "$store->table.shopDirect"))
                ->leftJoin($storeCountry->table, "$store->table.id", '=', "$storeCountry->table.storeId")
                ->where($store->table . '.deleted', '0')
                ->whereRaw($where)
                ->groupBy("$store->table.id")
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Stores::where('id', $id)
                ->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /* Method to get country of STMD warehouses
     * return string;
     */

    public static function getCountryName($storeId) {
        $countryName = $result = array();

        $countryId = Storecountrymapping::select('countryId')->where('storeId', $storeId)->get();

        if (!empty($countryId)) {
            foreach ($countryId as $countryid) {

                $warehouseId = Warehouse::where('id', $countryid['countryId'])->pluck('countryId')->toArray();

                

                $result[] = Country::where('id', $warehouseId[0])->pluck('name')->toArray();
            }

            foreach ($result as $row) {
                if (!in_array($row[0], $countryName)) {
                    $countryName[] = $row[0];
                }
            }
            echo implode(" , ", $countryName);
        }
    }

    /* Method to get Category of Stores
     * return string;
     */

    public static function getCategoryName($storeId) {
        $categoryName = $result = array();

        $categoryId = Storecountrymapping::select('categoryId')->where('storeId', $storeId)->get();

        if (!empty($categoryId)) {
            foreach ($categoryId as $categoryid) {

                $result[] = Sitecategory::where('id', $categoryid['categoryId'])->pluck('categoryName')->toArray();
            }

            foreach ($result as $row) {
                //print_r($row);
                if (!empty($row)) {
                    if (!in_array($row[0], $categoryName)) {
                        $categoryName[] = $row[0];
                    }
                }
            }

            echo implode(" , ", $categoryName);
        }
    }

    /**
     * Method used to fetch store record list
     * @param array $param
     * @return object
     */
    public static function getAllStores($param) {
        $store = new Stores;
        $storeCountry = new Storecountrymapping;

        $storeTable = $store->prefix . $store->table;
        $storeCountryTable = $store->prefix . $storeCountry->table;

        $where = "$storeTable.storeType ='" . $param['type'] . "'";

        if (!empty($param['warehouseId']))
            $where .= "  AND $storeCountryTable.countryId ='" . $param['warehouseId'] . "'";


        $resultSet = Stores::select(array("$store->table.id", "$store->table.storeName"))
                ->leftJoin($storeCountry->table, "$store->table.id", '=', "$storeCountry->table.storeId")
                ->where($store->table . '.deleted', '0')
                ->whereRaw($where)
                ->groupBy("$store->table.id")
                ->get();

        return $resultSet;
    }

    /**
     * Method used to fetch store record list
     * @param array $param
     * @return object
     */
    public static function getAllTypeStores($param) {
        $store = new Stores;
        $storeCountry = new Storecountrymapping;

        $storeTable = $store->prefix . $store->table;
        $storeCountryTable = $store->prefix . $storeCountry->table;

       // $where = "$storeTable.storeType ='" . $param['type'] . "'";

        if (!empty($param['warehouseId']))
            $where = "$storeCountryTable.countryId ='" . $param['warehouseId'] . "'";


        $resultSet = Stores::select(array("$store->table.id", "$store->table.storeName"))
                ->leftJoin($storeCountry->table, "$store->table.id", '=', "$storeCountry->table.storeId")
                ->where($store->table . '.deleted', '0')
                ->whereRaw($where)
                ->groupBy("$store->table.id")
                ->orderBy($store->table.'.storeName', 'asc')
                ->get()->toArray();
       
        return $resultSet;
    }

    /**
     * Method used to fetch category record list
     * @param array $param
     * @return object
     */
    public static function getAllCategoriesByStore($param) {
        $store = new Stores;
        $category = new Sitecategory;
        $storeCountry = new Storecountrymapping;

        $storeTable = $store->prefix . $store->table;
        $storeCountryTable = $store->prefix . $storeCountry->table;
        $categoryTable = $store->prefix . $category->table;

        $where = "$categoryTable.parentCategoryId = '-1' AND $categoryTable.status='1'";

        if (!empty($param['type']))
            $where .= "  AND $categoryTable.type ='" . $param['type'] . "'";

        if (!empty($param['storeId']))
            $where .= "  AND $storeCountryTable.storeId ='" . $param['storeId'] . "'";

        //DB::enableQueryLog();
        $resultSet = Sitecategory::select(array("$category->table.id", "$category->table.categoryName"))
                ->leftJoin($storeCountry->table, "$category->table.id", '=', "$storeCountry->table.categoryId")
                ->whereRaw($where)
                ->groupBy("$category->table.id")
                ->get();
        //dd(DB::getQueryLog());
        return $resultSet;
    }

    /**
     * Method used to fetch customer record list with store
     * @param array $param
     * @return object
     */

    public static function getcustomerDetailsByStore($param)
    {

        //print_r($param); die;
        $store = new Stores;
        $category = new Sitecategory;
        $storeCountry = new Storecountrymapping;
        $userList = new User;
        $procurement = new Procurement;
        $procurementItem = new Procurementitem;
        $product = new Siteproduct;


        $storeTable = $store->prefix . $store->table;
        $storeCountryTable = $store->prefix . $storeCountry->table;
        $categoryTable = $store->prefix . $category->table;
        $userTable = $store->prefix . $userList->table;
        $procurementTable = $store->prefix . $procurement->table;
        $procurementItemTable = $store->prefix . $procurementItem->table;
        $productTable = $store->prefix . $product->table;


        $where = "$procurementItemTable.deleted ='0' AND $procurementTable.paymentStatus='paid' AND $procurementTable.deleted='0'";

        if (!empty($param['searchStore']['storeId']))
            $where .= "  AND $procurementItemTable.storeId ='" . $param['searchStore']['storeId'] . "'";

        if (!empty($param['searchStore']['category']))
            $where .= "  AND $procurementItemTable.siteCategoryId ='" . $param['searchStore']['category'] . "'";

        if (!empty($param['searchStore']['subCategory']))
            $where .= "  AND $procurementItemTable.siteSubCategoryId ='" . $param['searchStore']['subCategory'] . "'";

        if (!empty($param['searchStore']['product']))
            $where .= "  AND $procurementItemTable.siteProductId ='" . $param['searchStore']['product'] . "'";

         if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($procurementTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $procurementTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($procurementTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($procurementTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }

        //DB::enableQueryLog();
        $resultset = Procurementitem::select(array("$store->table.storeName", "$procurement->table.createdOn", DB::RAW("(select categoryName FROM $categoryTable where ".$categoryTable.".id = ".$procurementItemTable.".siteCategoryId) as categoryName"), DB::RAW("(select categoryName FROM $categoryTable where ".$categoryTable.".id = ".$procurementItemTable.".siteSubCategoryId) as subCategoryName"), "$procurementItem->table.itemName", DB::RAW("CONCAT($userTable.firstName, ' ', $userTable.lastName) As customerName"), "$userList->table.unit", "$userList->table.email As userMail", "$userList->table.contactNumber", "$procurementItem->table.itemPrice", "$procurementItem->table.itemQuantity"))
                    ->leftJoin($procurement->table, "$procurement->table.id", '=', "$procurementItem->table.procurementId")
                    ->leftJoin($userList->table, "$userList->table.id", '=', "$procurement->table.userId")
                    ->leftJoin($product->table, "$product->table.id", '=', "$procurementItem->table.siteProductId")
                    ->leftJoin($store->table, "$store->table.id", '=', "$procurementItem->table.storeId")
                    ->whereRaw($where)
                    ->orderBy($param['field'], $param['type'])
                    ->paginate($param['searchDisplay']);
        //dd(DB::getQueryLog());

        return $resultset;
    }


    public static function exportStore($param)
    {

        //print_r($param); die;
        $store = new Stores;
        $category = new Sitecategory;
        $storeCountry = new Storecountrymapping;
        $userList = new User;
        $procurement = new Procurement;
        $procurementItem = new Procurementitem;
        $product = new Siteproduct;


        $storeTable = $store->prefix . $store->table;
        $storeCountryTable = $store->prefix . $storeCountry->table;
        $categoryTable = $store->prefix . $category->table;
        $userTable = $store->prefix . $userList->table;
        $procurementTable = $store->prefix . $procurement->table;
        $procurementItemTable = $store->prefix . $procurementItem->table;
        $productTable = $store->prefix . $product->table;


        $where = "$procurementItemTable.deleted ='0' AND $procurementTable.paymentStatus='paid' AND $procurementTable.deleted='0'";

        if (!empty($param['searchStore']['storeId']))
            $where .= "  AND $procurementItemTable.storeId ='" . $param['searchStore']['storeId'] . "'";

        if (!empty($param['searchStore']['category']))
            $where .= "  AND $procurementItemTable.siteCategoryId ='" . $param['searchStore']['category'] . "'";

        if (!empty($param['searchStore']['subCategory']))
            $where .= "  AND $procurementItemTable.siteSubCategoryId ='" . $param['searchStore']['subCategory'] . "'";

        if (!empty($param['searchStore']['product']))
            $where .= "  AND $procurementItemTable.siteProductId ='" . $param['searchStore']['product'] . "'";

         if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($procurementTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $procurementTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($procurementTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($procurementTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }

        //DB::enableQueryLog();
        $resultset = Procurementitem::select(array("$store->table.storeName", "$procurement->table.createdOn as PurchaseDate", DB::RAW("(select categoryName FROM $categoryTable where ".$categoryTable.".id = ".$procurementItemTable.".siteCategoryId) as categoryName"), DB::RAW("(select categoryName FROM $categoryTable where ".$categoryTable.".id = ".$procurementItemTable.".siteSubCategoryId) as subCategoryName"), "$procurementItem->table.itemName", DB::RAW("CONCAT($userTable.firstName, ' ', $userTable.lastName) As customerName"), "$userList->table.unit", "$userList->table.email As userMail", "$userList->table.contactNumber", "$procurementItem->table.itemPrice", "$procurementItem->table.itemQuantity"))
                    ->leftJoin($procurement->table, "$procurement->table.id", '=', "$procurementItem->table.procurementId")
                    ->leftJoin($userList->table, "$userList->table.id", '=', "$procurement->table.userId")
                    ->leftJoin($product->table, "$product->table.id", '=', "$procurementItem->table.siteProductId")
                    ->leftJoin($store->table, "$store->table.id", '=', "$procurementItem->table.storeId")
                    ->whereRaw($where)
                    ->get();
        //dd(DB::getQueryLog());

        return $resultset;
    }


}
