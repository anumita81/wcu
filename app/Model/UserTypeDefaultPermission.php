<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class UserTypeDefaultPermission extends Model
{
    protected $table;
    public $timestamps = false;
    public $prefix;
    protected $fillable = ['adminMenuId', 'userTypeId', 'permissionView', 'permissionAdd', 'permissionEdit', 'permissionDelete'];

    public function __construct() {
        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.ADMINUSERRTPEDEFAULTPERM');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Get the permission of an user by the user Id
     * @param integer $id
     * @return object
     */
    public static function getPermissionByUserTypeId($id) {

        $records = UserTypeDefaultPermission::select('adminMenuId', 'id', 'userTypeId', 'permissionView', 
            'permissionAdd', 'permissionEdit', 'permissionDelete')
        ->where('userTypeId', '=', $id)
        ->orderBy('id', 'ASC')
        ->get();
        
        return $records;
    }

    
}
