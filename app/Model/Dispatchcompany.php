<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Dispatchcompany extends Model {

    public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.DISPATCHCOMPANY');
    }

    /**
     * Method used to fetch dispatch company list
     * @param array $param
     * @return object
     */
    public static function getCompanyList($param) {
        $where = '1';

        $resultSet = Dispatchcompany::whereRaw($where)
                ->select(array('id', 'name', 'status'))
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to change dispatch company status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Dispatchcompany::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

}
