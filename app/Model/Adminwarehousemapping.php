<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
class Adminwarehousemapping extends Model
{
   
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.ADMINWAREHOUSEMAPPING');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Get the warehouse associated with the admin user
     * @param integer $id
     * @return object
     */
    public static function warehouseMap($id) {
        $warehouseMaster = new Warehouse();
        $warehouseMasterTable = $warehouseMaster->table;

        $warehousemapping = new Adminwarehousemapping;
        $warehousemappingTable = $warehousemapping->table;

        $countries = new Country();
        $countriesTable = $countries->table;
        
        $resultSet = Adminwarehousemapping::
                        leftJoin($warehouseMasterTable, $warehouseMasterTable.'.id', '=', $warehousemappingTable.'.warehouseId')
                        ->leftJoin($countriesTable, $countriesTable.'.id', '=', $warehouseMasterTable.'.countryId')
                        ->select(array($warehouseMasterTable.'.id', $countriesTable.'.name'))
                        ->where($warehousemappingTable.'.adminUserId', '=', $id)->get();

        return $resultSet;
    }

}
