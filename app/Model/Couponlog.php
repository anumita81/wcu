<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Couponlog extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.COUPONLOG');
        $this->prefix = DB::getTablePrefix();
    }

    public static function insertLog($data = array()) {
        
        $couponLog = new Couponlog;
        $offerLogcondition = '';
        $couponLog->code = $data['code'];
        $couponLog->userId = $data['userId'];
        $couponLog->createdOn = Config::get('constants.CURRENTDATE');
        $couponLog->totalAmount = $data['totalAmount'];
        if(!empty($data['discountAmount']))
            $couponLog->discountAmount = $data['discountAmount'];
        if(!empty($data['discountPoint']))
            $couponLog->discountPoint = $data['discountPoint'];
        if(isset($data['couponId']))
        {
            $getOfferCondition = Offercondition::where('offerId',$data['couponId'])->get();
            if(!empty($getOfferCondition))
            {
                $conditionArr = array();
                foreach($getOfferCondition as $eachCondition)
                {
                    $conditionArr[] =  $eachCondition->keyword;
                }

                $offerLogcondition = json_encode($conditionArr);
            }
        }
        $couponLog->conditions = $offerLogcondition;
        
        if($couponLog->save())
            return TRUE;
        else
            return FALSE;
    }

}
