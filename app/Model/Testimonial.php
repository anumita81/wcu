<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Testimonial extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.TESTIMONIALS');
    }

    /**
     * Method used to fetch Country list
     * @param array $param
     * @return object
     */
    public static function getTestimonialList($param) {
        $where = 'deleted = "0"';

        $resultSet = Testimonial::whereRaw($where)
                ->select('*')
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to change Country status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Testimonial::where('id', $id)
                ->update(array('status' => $newStatus));

        return $row;
    }

     /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId='') {
        if (empty($id))
            return false;

        $row = false;

        $row = Testimonial::where('id', $id)
                ->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }


}
