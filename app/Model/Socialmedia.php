<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Socialmedia extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
   public $table;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.SOCIALMEDIA');
    }
    public $timestamps = false;

    public static function getSettingsData()
    {
    	$data = array();

       	$where = "1";


        $resultSet = Socialmedia::whereRaw($where)
                ->select('*')
                ->get();
        //dd($resultSet);
        return $resultSet;
    }
}
