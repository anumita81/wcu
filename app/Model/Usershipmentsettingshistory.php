<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Usershipmentsettingshistory extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.USERSHIPMENTSETTINGSHISTORY');
        $this->prefix = DB::getTablePrefix();
    }

}
