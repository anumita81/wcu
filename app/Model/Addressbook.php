<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Addressbook extends Model {

    public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.ADDRESSBOOK');
    }

    public function country() {
        return $this->hasOne('App\Model\Country', 'id', 'countryId');
    }

    public function state() {
        return $this->hasOne('App\Model\State', 'id', 'stateId');
    }

    public function city() {
        return $this->hasOne('App\Model\City', 'id', 'cityId');
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Addressbook::where('id', $id)
                ->update(array('deleted' => 1, 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    public static function fetchUsedFieldIds($field = '') {
        $resultset = Addressbook::distinct()->whereNotNull($field)->where("deleted", "0")->pluck($field)->toArray();

        return $resultset;
    }

}
