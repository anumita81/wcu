<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Fillshipshipmentstatuslog extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.FILLSHIPSHIPMENTSTATUSLOG');
        $this->prefix = DB::getTablePrefix();
    }
    public $timestamps = false;
    
    public static function allStatus() {
        return array('in_warehouse'=>'In Warehouse','in_transit'=>'In Transit','custom_clearing'=>'Custom Clearing','destination_warehouse'=>'Destination Warehouse','out_for_delivery'=>'Out For Delivery','delivered'=>'Delivered');
    }

    public static function getStatusLog($shipmentId) {

        $shipmentDeliveryStatus = Fillshipshipmentstatuslog::allStatus();
        $shipemntDeliveryKeys = array_keys($shipmentDeliveryStatus);
        $itemArr = array('deliveryId' => '','shippingMethod'=>'','shippingMethodLogo'=>'');
        $statusdata = array();
        $shipmentStatusLog = Fillshipshipmentstatuslog::where('fillshipShipmentId', $shipmentId)->get();
        $statusCompletedIndex = 0;
        if ($shipmentStatusLog->count() > 0) {
            foreach ($shipmentStatusLog as $eachStatus) {
                $statusdata[] = array(
                    'status' => $shipmentDeliveryStatus[$eachStatus->status],
                    'date' => $eachStatus->updatedOn,
                    'completed' => true,
                    'visited' => true,
                );
                $statusCompletedIndex++;
            }
        }
        for ($index = $statusCompletedIndex; $index < count($shipemntDeliveryKeys); $index++) {
            $statusdata[] = array(
                'status' => $shipmentDeliveryStatus[$shipemntDeliveryKeys[$index]],
                'date' => '',
                'completed' => FALSE,
                'visited' => FALSE,
            );
        }

        return $statusdata;
    }

    public static function getShipmentwiseStatusLog($shipmentId) {
        
        $result = array();
        $statusLogData = Fillshipshipmentstatuslog::where('fillshipShipmentId',$shipmentId)->get();
        if($statusLogData->count()>0) {
            foreach($statusLogData as $eachData) {
                $result[$eachData->status] = \Carbon\Carbon::parse($eachData->updatedOn)->format('Y-m-d');
            }
        }
        
        return $result;
    }

}
