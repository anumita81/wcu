<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Sitecategory extends Model {

    public $table;
    public $timestamps = false;

    public function __construct() {

        parent::__construct();
        $this->table = Config::get('constants.dbTable.SITECATEGORY');
    }

    public static function getData($param) {

        $where = '1';

        if (!empty($param['searchData']))
            $where .= "  AND categoryName like '" . $param['searchData'] . "%'";

        if (!empty($param['searchCategory']))
            $where .= "  AND parentCategoryId = '" . $param['searchCategory'] . "%'";

        /* Print query ->toSql() dd($resultSet); */
        $resultSet = Sitecategory::whereRaw($where)
                ->select(array('id', 'parentCategoryId', 'categoryName', 'status'))
                ->orderBy($param['sortField'], $param['sortOrder'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    public static function getCategoryList($getData = 'parent') {

        $formatedCategories = array();
        if ($getData == 'parent')
            $categoryListRecords = Sitecategory::where('parentCategoryId', '-1')->get()->toArray();
        else
            $categoryListRecords = Sitecategory::all()->toArray();
        foreach ($categoryListRecords as $value) {
            $formatedCategories[$value['id']] = $value['categoryName'];
        }
        return $formatedCategories;
    }

    public static function getSubCategory($parentCategory) {

        $formatedCategories = array();
        $categoryListRecords = Sitecategory::where('parentCategoryId', $parentCategory)->get()->toArray();
        $i = 0;
        foreach ($categoryListRecords as $value) {
            $formatedCategories[$i]['id'] = $value['id'];
            $formatedCategories[$i]['category'] = $value['categoryName'];
            $i++;
        }
        return $formatedCategories;
    }

    public function deleteChild($parentId) {

        Sitecategory::where('parentCategoryId', $parentId)->delete();

        return true;
    }

    public static function getCategorySubcategory($startAt) {
        $where = "1";
        $resultSet = Sitecategory::whereRaw($where)->where('parentCategoryId', '=', $startAt)->get();

        return $resultSet;
    }

    public static function getOnlyParents() {
        $where = "1";
        $resultSet = Sitecategory::whereRaw($where)->where('parentCategoryId', '=', '-1')->get();

        return $resultSet;
    }

    /**
     * Method for get only categories
     * @return array
     */
    public static function getAllCategories() {
        $resultSet = Sitecategory::where('parentCategoryId', '=', '-1')->get();
        return $resultSet;
    }

    /**
     * Method for get only subcategories
     * @return array
     */
    public static function getAllSubcategories() {
        $resultSet = Sitecategory::where('parentCategoryId', '!=', '-1')->get();
        return $resultSet;
    }
    
    public static function getMappedSubcategory() {
        $resultSet = array();
        $allSubCategory = Sitecategory::getAllSubcategories();
        if(!empty($allSubCategory))
        {
            foreach($allSubCategory as $eachSubcat) {
                $resultSet[$eachSubcat->parentCategoryId][$eachSubcat->id] = $eachSubcat->categoryName;
            }
        }
        return $resultSet;
    }

}
