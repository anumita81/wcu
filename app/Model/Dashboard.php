<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Dashboard extends Model {

    public $table;
    protected $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.SHIPMENT');
        $this->prefix = DB::getTablePrefix();
    }

    public static function getRevenueData($searchId, $startDate, $endDate) {
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));

        $shipment = new \App\Model\Shipment();
        $shipmentTable = $shipment->prefix . $shipment->table;

        $autoshipment = new \App\Model\Autoshipment();
        $autoshipmentTable = $shipment->prefix . $autoshipment->table;

        $whereShipment = "1";

        if (!empty($searchId))
            $whereShipment .= " AND $shipmentTable.warehouseId = " . $searchId;

        $whereAutoshipment = "1";

        if (!empty($searchId))
            $whereAutoshipment .= " AND $autoshipmentTable.warehouseId = " . $searchId;

        //DB::enableQueryLog();
        //$resultSet = DB::SELECT(DB::RAW("SELECT totalCost FROM $viewShipmentTable where " . $where));

        $first = \App\Model\Autoshipment::select(DB::raw("sum(totalCost) AS totalCost"))
                ->where("$autoshipment->table.paymentStatus", 'paid')
                ->where("$autoshipment->table.deleted", '0')
                ->whereRaw($whereAutoshipment)
                ->whereBetween("$autoshipment->table.paymentReceivedOn", array($startDate, $endDate))
                ->groupBy(DB::RAW("DATE_FORMAT($autoshipmentTable.paymentReceivedOn, '%Y-%m-%d')"));

        $resultSet = \App\Model\Shipment::select(DB::raw("sum(totalCost) AS totalCost"))
                        ->where("$shipment->table.paymentStatus", 'paid')
                        ->where("$shipment->table.shippingMethod", 'Y')
                        ->where("$shipment->table.deleted", '0')
                        ->whereRaw($whereShipment)
                        ->whereBetween("$shipment->table.paymentReceivedOn", array($startDate, $endDate))
                        ->groupBy(DB::RAW("DATE_FORMAT($shipmentTable.paymentReceivedOn, '%Y-%m-%d')"))->union($first)->get();


        //dd(DB::getQueryLog());

        return $resultSet;
    }

    public static function getShipmentData($searchId, $startDate, $endDate) {
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));

        $shipment = new \App\Model\Shipment();
        $shipmentTable = $shipment->prefix . $shipment->table;

        $autoshipment = new \App\Model\Autoshipment();
        $autoshipmentTable = $shipment->prefix . $autoshipment->table;

        $whereShipment = "1";

        if (!empty($searchId))
            $whereShipment .= " AND $shipmentTable.warehouseId = " . $searchId;

        $whereAutoshipment = "1";

        if (!empty($searchId))
            $whereAutoshipment .= " AND $autoshipmentTable.warehouseId = " . $searchId;

        //DB::enableQueryLog();
        //$resultSet = DB::SELECT(DB::RAW("SELECT totalCost FROM $viewShipmentTable where " . $where));

        $first = \App\Model\Autoshipment::select(DB::raw("count(DISTINCT id) AS totalShipment"))
                ->where("$autoshipment->table.paymentStatus", 'paid')
                ->where("$autoshipment->table.deleted", '0')
                ->whereRaw($whereAutoshipment)
                ->whereBetween("$autoshipment->table.createdOn", array($startDate, $endDate))
                ->groupBy(DB::RAW("DATE_FORMAT($autoshipmentTable.createdOn, '%Y-%m-%d')"));

        $resultSet = \App\Model\Shipment::select(DB::raw("count(DISTINCT id) AS totalShipment"))
                        ->where("$shipment->table.paymentStatus", 'paid')
                        ->where("$shipment->table.shippingMethod", 'Y')
                        ->where("$shipment->table.deleted", '0')
                        ->whereRaw($whereShipment)
                        ->whereBetween("$shipment->table.createdOn", array($startDate, $endDate))
                        ->groupBy(DB::RAW("DATE_FORMAT($shipmentTable.createdOn, '%Y-%m-%d')"))->union($first)->get();
        //dd(DB::getQueryLog());

        return $resultSet;
    }

    public static function getCustomerData($searchId, $startDate, $endDate) {
        $user = new User;

        $userTable = $user->prefix . $user->table;

        $where = "1";

//        if (!empty($searchId))
//            $where .= " AND countryId = " . $searchId;

        $resultSet = \App\Model\User::select(DB::raw("count(id) AS totalUser"))
                        ->where("status", '1')
                        ->where("deleted", '0')
                        ->whereRaw($where)
                        ->whereBetween("createdOn", array($startDate, $endDate))
                        ->groupBy(DB::RAW("DATE_FORMAT(createdOn, '%Y-%m-%d')"))->get();

        //dd(DB::getQueryLog());

        return $resultSet;
    }

    public static function getCountryList_old($startDate, $endDate) {
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));

        $resultSet = DB::SELECT(DB::RAW("select stmd_countries.name, stmd_countries.id, count(stmd_orders.id) maxorder from stmd_countries join stmd_shipments on stmd_shipments.toCountry = stmd_countries.id join stmd_orders on stmd_orders.shipmentId=stmd_shipments.id where stmd_orders.status='5' and stmd_shipments.createdOn between '" . $startDate . "' and '" . $endDate . "' group by stmd_shipments.toCountry"));


        return $resultSet;
    }

    public static function getCountryList($startDate, $endDate) {
        $data = array();
        // DB::enableQueryLog(); 

        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));

        $order = new \App\Model\Order();
        $orderTable = $order->prefix . $order->table;

        $countries = new \App\Model\Country();
        $countriesTable = $order->prefix . $countries->table;

        $shipment = new \App\Model\Shipment();
        $shipmentTable = $order->prefix . $shipment->table;

        $autoshipment = new \App\Model\Autoshipment();
        $autoshipmentTable = $order->prefix . $autoshipment->table;

        $first = \App\Model\Order::select("$countries->table.id", "$countries->table.name", DB::RAW("count( DISTINCT $orderTable.shipmentId) AS maxorder"))
                ->leftJoin("$shipment->table", "$order->table.shipmentId", "=", "$shipment->table.id")
                ->leftJoin("$countries->table", "$countries->table.id", '=', "$shipment->table.toCountry")
                ->where("$shipment->table.deleted", '0')
                ->where("$order->table.type", 'shipment')
                ->where("$order->table.status", '5')
                ->whereBetween("$order->table.createdDate", array($startDate, $endDate))
                ->distinct("$order->table.shipmentId")
                ->groupBy("$shipment->table.toCountry");

        $resultSet = \App\Model\Order::select("$countries->table.id", "$countries->table.name", DB::RAW("count(DISTINCT $orderTable.shipmentId) AS maxorder"))
                        ->leftJoin("$autoshipment->table", "$order->table.shipmentId", "=", "$autoshipment->table.id")
                        ->leftJoin("$countries->table", "$countries->table.id", '=', "$autoshipment->table.destinationCountry")
                        ->where("$autoshipment->table.deleted", '0')
                        ->where("$order->table.type", 'autoshipment')
                        ->where("$order->table.status", '5')
                        ->whereBetween("$order->table.createdDate", array($startDate, $endDate))
                        ->distinct("$order->table.shipmentId")
                        ->groupBy("$autoshipment->table.destinationCountry")->union($first)->get();
//dd(DB::getQueryLog());
//        $resultSet = DB::SELECT(DB::RAW("select stmd_countries.name, stmd_countries.id, count(stmd_orders.id) maxorder from stmd_countries join stmd_shipments on stmd_shipments.toCountry = stmd_countries.id join stmd_orders on stmd_orders.shipmentId=stmd_shipments.id where stmd_orders.status='5' and stmd_shipments.createdOn between '" . $startDate . "' and '" . $endDate . "' group by stmd_shipments.toCountry"));

        $result = $resultSet->toArray();
        if (!empty($result)) {
            foreach ($result as $row) {
                if (array_key_exists($row['id'], $data)) {
                    $data[$row['id']]['name'] = $row['name'];
                    $data[$row['id']]['maxorder'] = $data[$row['id']]['maxorder'] + $row['maxorder'];
                } else {
                    $data[$row['id']]['name'] = $row['name'];
                    $data[$row['id']]['maxorder'] = $row['maxorder'];
                }
            }
        }
        return $data;
    }

    public static function getCompletedOrder($startDate, $endDate, $type) {

        $order = new \App\Model\Order();
        $orderTable = $order->prefix . $order->table;

        $shipment = new \App\Model\Shipment();
        $shipmentTable = $order->prefix . $shipment->table;

        $autoshipment = new \App\Model\Autoshipment();
        $autoshipmentTable = $order->prefix . $autoshipment->table;

        if ($type == "autoshipment") {
            $resultSet = \App\Model\Order::select(DB::RAW("count(DISTINCT $orderTable.shipmentId) AS maxorder"))
                            ->leftJoin("$autoshipment->table", "$order->table.shipmentId", "=", "$autoshipment->table.id")
                            ->where("$autoshipment->table.deleted", '0')
                            ->where("$order->table.type", 'autoshipment')
                            ->where("$order->table.status", '5')
                            ->whereBetween("$order->table.createdDate", array($startDate, $endDate))
                            ->groupBy(DB::RAW("DATE_FORMAT($orderTable.createdDate, '%Y-%m-%d')"))->get();
        } else {
            $resultSet = \App\Model\Order::select(DB::RAW("count(DISTINCT $orderTable.shipmentId) AS maxorder"))
                            ->leftJoin("$shipment->table", "$order->table.shipmentId", "=", "$shipment->table.id")
                            ->where("$shipment->table.deleted", '0')
                            ->where("$shipment->table.shipmentType", $type)
                            ->where("$order->table.type", 'shipment')
                            ->where("$order->table.status", '5')
                            ->whereBetween("$order->table.createdDate", array($startDate, $endDate))
                            ->groupBy(DB::RAW("DATE_FORMAT($orderTable.createdDate, '%Y-%m-%d')"))->get();
        }
        //$resultSet = DB::SELECT(DB::RAW("select count(stmd_orders.id) maxorder from stmd_shipments join stmd_orders on stmd_orders.shipmentId=stmd_shipments.id where stmd_orders.status='5' and stmd_shipments.shipmentType = '" . $type . "' and stmd_shipments.createdOn between '" . $startDate . "' and '" . $endDate . "' group by stmd_orders.shipmentId"));
        // dd(DB::getQueryLog());

        return $resultSet;
    }

    public static function getTotalShipment($startDate, $endDate, $type) {

        $shipment = new \App\Model\Shipment();
        $shipmentTable = $shipment->prefix . $shipment->table;

        $autoshipment = new \App\Model\Autoshipment();
        $autoshipmentTable = $shipment->prefix . $autoshipment->table;

        if ($type == "autoshipment") {
            $resultSet = \App\Model\Autoshipment::select(DB::RAW("count(DISTINCT $autoshipmentTable.id) AS maxshipment"))
                            // ->where("$autoshipment->table.paymentStatus", 'paid')
                            ->where("$autoshipment->table.deleted", '0')
                            ->whereBetween("$autoshipment->table.createdOn", array($startDate, $endDate))
                            ->groupBy(DB::RAW("DATE_FORMAT($autoshipmentTable.createdOn, '%Y-%m-%d')"))->get();
        } else {
            $resultSet = \App\Model\Shipment::select(DB::RAW("count(DISTINCT $shipmentTable.id) AS maxshipment"))
                            // ->where("$shipment->table.paymentStatus", 'paid')
                            //   ->where("$shipment->table.shippingMethod", 'Y')
                            ->where("$shipment->table.shipmentType", $type)
                            ->where("$shipment->table.deleted", '0')
                            ->whereBetween("$shipment->table.createdOn", array($startDate, $endDate))
                            ->groupBy(DB::RAW("DATE_FORMAT($shipmentTable.createdOn, '%Y-%m-%d')"))->get();
        }

        // $resultSet = DB::SELECT(DB::RAW("select count(stmd_shipments.id) maxshipment from stmd_shipments where stmd_shipments.paymentStatus='paid' and stmd_shipments.shippingMethod = 'Y' and stmd_shipments.shipmentType = '" . $type . "' and stmd_shipments.createdOn between '" . $startDate . "' and '" . $endDate . "' group by stmd_shipments.id"));

        return $resultSet;
    }

    public static function getTotalRevenue($startDate, $endDate, $type) {
        $shipment = new \App\Model\Shipment();
        $shipmentTable = $shipment->prefix . $shipment->table;

        $autoshipment = new \App\Model\Autoshipment();
        $autoshipmentTable = $shipment->prefix . $autoshipment->table;

        if ($type == "autoshipment") {
            $resultSet = \App\Model\Autoshipment::select(DB::RAW("sum($autoshipmentTable.totalCost) AS revenue"))
                            ->where("$autoshipment->table.paymentStatus", 'paid')
                            ->where("$autoshipment->table.deleted", '0')
                            ->whereBetween("$autoshipment->table.createdOn", array($startDate, $endDate))
                            ->distinct("$autoshipment->table.id")
                            ->groupBy(DB::RAW("DATE_FORMAT($autoshipmentTable.createdOn, '%Y-%m-%d')"))->get();
        } else {
            $resultSet = \App\Model\Shipment::select(DB::RAW("sum($shipmentTable.totalCost) AS revenue"))
                            ->where("$shipment->table.paymentStatus", 'paid')
                            ->where("$shipment->table.shippingMethod", 'Y')
                            ->where("$shipment->table.shipmentType", $type)
                            ->where("$shipment->table.deleted", '0')
                            ->whereBetween("$shipment->table.createdOn", array($startDate, $endDate))
                            ->distinct("$shipment->table.id")
                            ->groupBy(DB::RAW("DATE_FORMAT($shipmentTable.createdOn, '%Y-%m-%d')"))->get();
        }

        //  $resultSet = DB::SELECT(DB::RAW("select sum(stmd_shipments.totalcost) revenue from stmd_shipments where stmd_shipments.paymentStatus='paid' and stmd_shipments.shipmentType = '" . $type . "' and stmd_shipments.createdOn between '" . $startDate . "' and '" . $endDate . "' group by stmd_shipments.id"));

        return $resultSet;
    }

    public static function gettopCustomers($startDate, $endDate) {
        $result = $data = array();

        $order = new \App\Model\Order();
        $orderTable = $order->prefix . $order->table;

        $warehouse = new \App\Model\Warehouse();
        $warehouseTable = $order->prefix . $warehouse->table;

        $countries = new \App\Model\Country();
        $countriesTable = $order->prefix . $countries->table;

        $shipment = new \App\Model\Shipment();
        $shipmentTable = $order->prefix . $shipment->table;

        $autoshipment = new \App\Model\Autoshipment();
        $autoshipmentTable = $order->prefix . $autoshipment->table;

        $first = \App\Model\Order::select("$countries->table.id", "$countries->table.name", DB::RAW("count( DISTINCT $orderTable.userId) AS maxuser"))
                ->leftJoin("$shipment->table", "$order->table.shipmentId", "=", "$shipment->table.id")
                ->leftJoin("$warehouse->table", "$warehouse->table.id", "=", "$shipment->table.warehouseId")
                ->leftJoin("$countries->table", "$countries->table.id", '=', "$warehouse->table.countryId")
                ->where("$shipment->table.deleted", '0')
                ->where("$order->table.type", 'shipment')
                ->whereBetween("$order->table.createdDate", array($startDate, $endDate))
                ->distinct("$order->table.userId")
                ->groupBy("$countries->table.id");

        $resultSet = \App\Model\Order::select("$countries->table.id", "$countries->table.name", DB::RAW("count(DISTINCT $orderTable.userId) AS maxuser"))
                        ->leftJoin("$autoshipment->table", "$order->table.shipmentId", "=", "$autoshipment->table.id")
                        ->leftJoin("$warehouse->table", "$warehouse->table.id", "=", "$autoshipment->table.warehouseId")
                        ->leftJoin("$countries->table", "$countries->table.id", '=', "$warehouse->table.countryId")
                        ->where("$autoshipment->table.deleted", '0')
                        ->where("$order->table.type", 'autoshipment')
                        ->whereBetween("$order->table.createdDate", array($startDate, $endDate))
                        ->distinct("$order->table.userId")
                        ->groupBy("$countries->table.id")->union($first)->get();

        // $resultSet = DB::SELECT(DB::RAW("select stmd_countries.name, count(stmd_users.id) maxuser from stmd_countries join stmd_address_book on stmd_address_book.countryId = stmd_countries.id join stmd_users on stmd_address_book.userId = stmd_users.id where stmd_users.status='1' and stmd_address_book.isDefaultShipping = '1' and stmd_users.createdOn between '" . $startDate . "' and '" . $endDate . "' group by stmd_countries.id"));

        $result = $resultSet->toArray();
        $count = 0;

        if (!empty($result)) {
            foreach ($result as $row) {
                if (array_key_exists($row['id'], $data)) {
                    $data[$row['id']]['name'] = $row['name'];
                    $data[$row['id']]['maxuser'] = $data[$row['id']]['maxuser'] + $row['maxuser'];
                } else {
                    $data[$row['id']]['name'] = $row['name'];
                    $data[$row['id']]['maxuser'] = $row['maxuser'];
                }
            }
        }
        return $data;
    }

}
