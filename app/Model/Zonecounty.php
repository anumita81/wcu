<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

//use App\libraries\helpers;
//use App\libraries\dbHelpers;

class Zonecounty extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.ZONE_COUNTY');
        $this->prefix = DB::getTablePrefix();
    }

    public static function getData() {

        $return = array();

        $data = Zonecounty::select('zoneId',\DB::raw('count(zoneId) as numCountry'))
                            ->groupBy('zoneId')
                            ->get();

        foreach($data as $eachData)
        {
            $return[$eachData->zoneId] = $eachData->numCountry;
        }

        return $return;
    }

    public static function zoneWiseData($zoneId) {

        $resultSet = DB::table('zone_counties')
            ->join('counties','counties.id','=','zone_counties.countyId')
            ->join('states', 'states.id', '=', 'counties.stateId')
            ->join('countries', 'countries.code', '=', 'states.countryCode')
            ->select('countries.name as country_name', 'states.name as state_name', 'counties.id','counties.name')
            ->where('zoneId',$zoneId)
            ->get();       

        return $resultSet;
    }
}