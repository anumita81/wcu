<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
use customhelper;

class Shipmentitempriceupdatelog extends Model
{
   
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.SHIPMENTITEMPRICEUPDATELOG');
        $this->prefix = DB::getTablePrefix();
    }
    
    public static function getLogData($shipmentId) {
        
        $result = array();
        $logData = Shipmentitempriceupdatelog::where('shipmentId',$shipmentId)->get();
        foreach($logData as $eachIndex => $eachLogData) {

            $result[$eachLogData->deliveryId][$eachIndex]['oldPrice'] = customhelper::getCurrencySymbolFormat($eachLogData->oldPrice);
            $result[$eachLogData->deliveryId][$eachIndex]['newPrice'] = customhelper::getCurrencySymbolFormat($eachLogData->newPrice);
            $result[$eachLogData->deliveryId][$eachIndex]['itemName'] = $eachLogData->itemName;
            $result[$eachLogData->deliveryId][$eachIndex]['actionType'] = $eachLogData->actionType;
            $result[$eachLogData->deliveryId][$eachIndex]['updatedByType'] = $eachLogData->updatedByType;
            if($eachLogData->updatedByType == 'admin')
            {
                $userInfo = UserAdmin::select('firstName','lastName')->find($eachLogData->updatedBy);
                $result[$eachLogData->deliveryId][$eachIndex]['updatedBy'] = $userInfo->firstName.' '.$userInfo->lastName;
            }
            else
            {
                $userInfo = User::select('firstName','lastName')->find($eachLogData->updatedBy);
                $result[$eachLogData->deliveryId][$eachIndex]['updatedBy'] = 'customer';
            }
            $result[$eachLogData->deliveryId][$eachIndex]['updatedOn'] = date('h:i a, m/d/Y',strtotime($eachLogData->updatedOn));     
        }
        return $result;
    }
}
