<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
use Auth;

class Customerhelpsection extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.CUSTOMERHRLPSEC');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to fetch category list
     * @param array $param
     * @return object
     */
    public static function getList($param) {
        $section = new Customerhelpsection;
        $cat = new Customerhelpcategories;

        $sectionTable = $section->prefix . $section->table;

        $where = "1";
        if (!empty($param['searchByCat']))
            $where .= "  AND $section->prefix$section->table.catId ='" . $param['searchByCat'] . "'";

        $resultSet = Customerhelpsection::select(array(
            "$section->table.id", 
            "$section->table.title", 
            "$section->table.content", 
            "$section->table.orders", 
            "$section->table.status", 
            "$cat->table.id as catId",
            "$cat->table.name as catName"
            ))
                ->leftJoin($cat->table, "$section->table.catId", '=', "$cat->table.id")
                ->where($section->table . '.deleted', '0')
                ->whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Customerhelpsection::where('id', $id)->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }
    
    /**
     * Method used to change Admin User status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return object
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Customerhelpsection::where('id', $id)
                ->update(array('status' => $newStatus, 'updatedBy' => Auth::user()->id, 'updatedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }


    /**
     * Method used to get the sections by category
     * @param integer $id
     * @return object
     */
    public static function getallsection($id) {
        $row = false;

        $row = Customerhelpsection::where("deleted", '0')->where("status", '1')->where("catId", $id)
                ->orderby("orders", "asc")
                ->get();

        return $row;
    }

}
