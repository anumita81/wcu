<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Groupcoordinatorcommission extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.COORDINATORCOMMISSION');
        $this->prefix = DB::getTablePrefix();
    }
    
    public static function getData($param) {
        
        $groupDetails = new Groupdetails;
        $gropCommission = new Groupcoordinatorcommission;
        $user = new User;
        
        $groupDetailsTable = $groupDetails->prefix.$groupDetails->table;
        $gropCommissionTable = $gropCommission->prefix.$gropCommission->table;
        $userTable = $user->prefix.$user->table;
        
        $where = '1';

        if(!empty($param['searchData']['groupName'])) {
            $where .= " AND $groupDetailsTable.groupName like '%".$param['searchData']['groupName']."%'";
        }
        
        if(!empty($param['searchData']['paymentStatus'])) {
            $where .= " AND $gropCommissionTable.isRedem='".$param['searchData']['paymentStatus']."'";
        }
        
        if(!empty($param['searchData']['coordinator'])) {
            $where .= " AND concat($userTable.firstName,' ',$userTable.lastName) like '%".$param['searchData']['coordinator']."%'";
        }
        
        $resultSet = Groupcoordinatorcommission::select(array("$gropCommission->table.*","$user->table.firstName","$user->table.lastName","$groupDetails->table.groupName","$groupDetails->table.coordinatorId"))->join("$groupDetails->table","$gropCommission->table.groupId","=","$groupDetails->table.id")
                ->join("$user->table","$user->table.id","=","$gropCommission->table.coordinatorId")
                ->whereRaw($where)
                ->orderBy($gropCommission->table.".".$param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
        
    }

}
