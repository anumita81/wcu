<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Autopickuprequest extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.AUTOPICKUP');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to fetch car pickup request list
     * @param array $param
     * @return object
     */
    public static function getList($param) {
        $autoPickup = new Autopickuprequest;
        $autoWebsite = new Autowebsite;
        $user = new User;
        $state = new State;
        $city = new City;

        $userTable = $autoPickup->prefix . $user->table;
        $autoPickupTable = $autoPickup->prefix . $autoPickup->table;

        $where = "1";

        if (!empty($param)) {
            if (!empty($param['searchAutoPickup']['unitFrom']))
                $where .= " AND $userTable.unit >= '" . $param['searchAutoPickup']['unitFrom'] . "'";

            if (!empty($param['searchAutoPickup']['unitTo']))
                $where .= " AND $userTable.unit <= '" . $param['searchAutoPickup']['unitFrom'] . "'";

            if ($param['searchAutoPickup']['status'] != '')
                $where .= " AND $autoPickupTable.status = '" . $param['searchAutoPickup']['status'] . "'";

            if (!empty($param['searchAutoPickup']['user']))
                $where .= " AND ($userTable.firstName LIKE '%" . $param['searchAutoPickup']['user'] . "%' OR $userTable.lastName LIKE '%" . $param['searchAutoPickup']['user'] . "%' OR $userTable.email LIKE '%" . $param['searchAutoPickup']['user'] . "%')";

            if ($param['searchByCreatedOn'] != '') {
                if ($param['searchByCreatedOn'] == 'thismonth')
                    $where .= "  AND MONTH($autoPickupTable.postedOn) ='" . \Carbon\Carbon::now()->month . "'";
                else if ($param['searchByCreatedOn'] == 'thisweek')
                    $where .= "  AND $autoPickupTable.postedOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
                else if ($param['searchByCreatedOn'] == 'today')
                    $where .= "  AND date($autoPickupTable.postedOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
                else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                    $searchDate = explode('-', $param['searchByDate']);
                    $searchByStartDate = trim($searchDate[0]);
                    $searchByEndDate = trim($searchDate[1]);
                    $where .= "  AND date($autoPickupTable.postedOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
                }
            }
        }

        $resultSet = Autopickuprequest::select(array("$autoPickup->table.id", "$autoPickup->table.status", "$autoPickup->table.postedOn", "$user->table.firstName", "$user->table.lastName", "$user->table.email", "$autoWebsite->table.name AS websiteName"
                    , "$state->table.name AS stateName", "$city->table.name AS cityName"))
                ->leftJoin($autoWebsite->table, "$autoWebsite->table.id", '=', "$autoPickup->table.websiteId")
                ->leftJoin($user->table, "$user->table.id", '=', "$autoPickup->table.userId")
                ->leftJoin($state->table, "$state->table.id", '=', "$autoPickup->table.stateId")
                ->leftJoin($city->table, "$city->table.id", '=', "$autoPickup->table.cityId")
                ->whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to fetch a single car details
     * @param int $id
     * @return object
     */
    public static function getCarDetails($id) {
        $autoPickup = new Autopickuprequest;
        $make = new Automake;
        $model = new Automodel;

        $resultSet = Autopickuprequest:: select(array("$autoPickup->table.*", "$autoPickup->table.carPurchaseUrl", "$autoPickup->table.carPrice", "$autoPickup->table.milage",
                    "$autoPickup->table.vinNumber", "$autoPickup->table.color", "$autoPickup->table.carImage", "$make->table.name AS makeName", "$model->table.name AS modelName",))
                ->leftJoin($make->table, "$make->table.id", '=', "$autoPickup->table.makeId")
                ->leftJoin($model->table, "$model->table.id", '=', "$autoPickup->table.modelId")
                ->leftJoin($user->table, "$user->table.id", '=', "$autoPickup->table.userId")
                ->where($autoPickup->table . '.id', $id)
                ->first();

        return $resultSet;
    }

    /**
     * Method used to fetch a single car pickup request details
     * @param int $id
     * @return object
     */
    public static function getPickupRequestDetails($id) {
        $autoPickup = new Autopickuprequest;
        $make = new Automake;
        $model = new Automodel;
        $autoWebsite = new Autowebsite;
        $user = new User;
        $state = new State;
        $city = new City;

        $resultSet = Autopickuprequest:: select(array("$autoPickup->table.*", "$user->table.firstName", "$user->table.unit", "$user->table.lastName", "$user->table.email", "$autoWebsite->table.name AS websiteName",
                    "$state->table.name AS stateName", "$city->table.name AS cityName", "$make->table.name AS makeName", "$model->table.name AS modelName",))
                ->leftJoin($make->table, "$make->table.id", '=', "$autoPickup->table.makeId")
                ->leftJoin($model->table, "$model->table.id", '=', "$autoPickup->table.modelId")
                ->leftJoin($user->table, "$user->table.id", '=', "$autoPickup->table.userId")
                ->leftJoin($autoWebsite->table, "$autoWebsite->table.id", '=', "$autoPickup->table.websiteId")
                ->leftJoin($state->table, "$state->table.id", '=', "$autoPickup->table.stateId")
                ->leftJoin($city->table, "$city->table.id", '=', "$autoPickup->table.cityId")
                ->where($autoPickup->table . '.id', $id)
                ->first();

        return $resultSet;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Automodel::where('id', $id)->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

}
