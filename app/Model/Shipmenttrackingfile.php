<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Shipmenttrackingfile extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    protected $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.SHIPMENTTRACKINGFILE');
        $this->prefix = DB::getTablePrefix();
    }

    public static function trackingFileExist() {
        
        $shipmentTrackingFile = new Shipmenttrackingfile;
        $shipmentTracking = new Shipmenttracking;
        
        $shipmentFiles = Shipmenttracking::select("$shipmentTracking->table.id",DB::raw("count(trackingId)"))
                        ->leftJoin($shipmentTrackingFile->table,"$shipmentTrackingFile->table.trackingId","=","$shipmentTracking->table.id")
                        ->where("deleted","0")->groupBy("trackingId")->get()->toArray();
        //print_r($shipmentFiles);
    }
}
