<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Usernotification extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $timestamps = false;
    public $prefix;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.USERNOTIFICATION');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to fetch Notification list
     * @param array $param
     * @return object
     */
    public static function getNotificationList($param) {
        $where = '1';

        /* Print query ->toSql() dd($resultSet); */
        $resultSet = Usernotification::where('userId', $param['userId'])
                ->select(array('id', 'sentOn'))
                ->where("type", '1')
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

}
