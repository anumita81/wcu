<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
use Auth;

class Shipmentlabelprintlog extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
   public $table;
   public $prefix;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.SHIPMENTLABELLOG');
        $this->prefix = DB::getTablePrefix();
    }
    public $timestamps = false;
    
    public static function logEntry($shipmentId,$type,$fileName = '') {
        
        $labelRecordExist = Shipmentlabelprintlog::where('shipmentId',$shipmentId)->where('labelType',$type)->get();
        if($labelRecordExist->count() > 0)
        {
            $shipmentLog = Shipmentlabelprintlog::find($labelRecordExist[0]->id);
        }
        else
        {
            $shipmentLog = new Shipmentlabelprintlog;
        }
        $shipmentLog->shipmentId = $shipmentId;
        $shipmentLog->labelType = $type;
        $shipmentLog->createdBy = Auth::user()->id;
        $shipmentLog->createdByEmail = Auth::user()->email;
        $shipmentLog->createdOn = Config::get('constants.CURRENTDATE');
        if($fileName !='')
            $shipmentLog->labelFile = $fileName;
        $shipmentLog->save();
    }
}
