<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Ewallet extends Model {

    public $table;
    public $timestamps = false;

    public function __construct() {

        parent::__construct();
        $this->table = Config::get('constants.dbTable.EWALLET');
    }

    public function getData($param) {

        $where = '1';
        if (!empty($param['searchData'])) {
            if (!empty($param['searchData']['dateRangeSelection']) && $param['searchData']['dateRangeSelection'] != 'all') {
                $selection = $param['searchData']['dateRangeSelection'];
                if ($selection == 'thismonth') {
                    $param['searchData']['dateRange'] = date('m/01/Y') . ' - ' . date('m/t/Y');
                } elseif ($selection == 'thisweek') {
                    $param['searchData']['dateRange'] = date('m/d/Y', strtotime("previous monday")) . ' - ' . date('m/d/y');
                } elseif ($selection == 'today') {
                    $param['searchData']['dateRange'] = date('m/d/Y') . ' - ' . date('m/d/Y');
                }

                $dateParts = explode(' - ', $param['searchData']['dateRange']);
                $where .=" AND (addDate between " . strtotime($dateParts[0]) . " AND " . strtotime($dateParts[1]) . ")";
            }
            if (!empty($param['searchData']['recipientEmail']))
                $where .= "  AND recipientEmail like '" . $param['searchData']['recipientEmail'] . "%'";
            if (!empty($param['searchData']['orderId']))
                $where .= "  AND orderId= '" . $param['searchData']['orderId'] . "'";
            if (!empty($param['searchData']['gcId']))
                $where .= "  AND gcId= '" . $param['searchData']['gcId'] . "'";
            if (!empty($param['searchData']['status']))
                $where .= "  AND status= '" . $param['searchData']['status'] . "'";
        }

        /* Print query ->toSql() dd($resultSet); */
        $resultSet = Ewallet::whereRaw($where)
                ->orderBy($param['sortField'], $param['sortOrder'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    public function getAllStatus() {

        return array('0' => 'Pending', '1' => 'Active', '2' => 'Blocked', '3' => 'Disabled', '4' => 'Expired', '5' => 'Used');
    }

}
