<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

//use App\libraries\helpers;
//use App\libraries\dbHelpers;

class State extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.STATE');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to fetch State list
     * @param array $param
     * @return object
     */
    public static function getStateList($param) {
        $where = 'deleted = "0"';

        if (!empty($param['searchByCountry']))
            $where .= "  AND countryCode ='" . $param['searchByCountry'] . "'";

        /* Print query ->toSql() dd($resultSet); */
        $resultSet = State::whereRaw($where)
                ->select(array('id', 'code', 'name', 'status'))
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }
    
    /**
     * Method used to change State status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = State::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = State::where('id', $id)
                ->update(array('deleted' => 1, 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    public static function getStateData($param = 'all')
    {
        
        if (!empty($param!='all'))
            $where .= "  AND countryCode ='" . $param . "'";
            $resultSet = DB::table('states')
            ->join('countries', 'countries.code', '=', 'states.countryCode')
            ->select('countries.name as country_name', 'states.id', 'states.name')
            ->where('states.status','1')
            ->where('states.deleted','0')
            ->where('countries.status','1')
            ->get();       

        return $resultSet;
    }

    /**
     * Method used to get all states with country id
     * @return array
     */
    public static function getAllStateList()
    {
        $resultSet = DB::table('states')
            ->join('countries', 'countries.code', '=', 'states.countryCode')
            ->select('countries.name as country_name', 'states.id', 'states.name', 'countries.id as countryId')
            ->where('states.status','1')
            ->where('states.deleted','0')
            ->get();       

        return $resultSet;
    }

}
