<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Autoshipmentstatuslog extends Model {

    public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.AUTOSHIPMENTSTATUSLOG');
    }
    
    public static function getOrderStatusProgress($autoshipmentId) {
    	$formattedData = array();
    	$data = Autoshipmentstatuslog::where('autoshipmentId',$autoshipmentId)->get();
    	if(count($data)>0)
    	{
    		foreach ($data as $key => $eachData) {
    			$formattedData[$eachData->status] = $eachData->updatedOn;
    		}
    	}

    	return $formattedData;
    }

}
