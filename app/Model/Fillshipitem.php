<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Fillshipitem extends Model {

    public $table;
    protected $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.FILLSHIPITEM');
        $this->prefix = DB::getTablePrefix();
    }

    public function deliverycompany() {
        return $this->hasOne('App\Model\Deliverycompany', 'id', 'deliveryCompanyId');
    }

    public static function getItems($fillshipId){
    	$fillshipitem = new Fillshipitem;
    	$category = new Sitecategory;
    	$prod = new Siteproduct;
        $store = new Stores;

    	$fillshipitemTable = $fillshipitem->prefix . $fillshipitem->table;
    	$categoryTable = $category->prefix . $category->table;
    	$prodTable = $prod->prefix . $prod->table;
        $storeTable = $store->prefix . $store->table;

    	//DB::enableQueryLog();

    	$resultSet = Fillshipitem::select(
    		"$fillshipitem->table.id",
    		"$fillshipitem->table.fillshipId",
                "$fillshipitem->table.itemName",
    		"$fillshipitem->table.siteCategoryId", 
    		"$fillshipitem->table.siteSubCategoryId", 
    		"$fillshipitem->table.siteProductId",
    		"$fillshipitem->table.itemPrice",
    		"$fillshipitem->table.itemQuantity",
                "$fillshipitem->table.length",
                "$fillshipitem->table.width",
                "$fillshipitem->table.height",
                "$fillshipitem->table.weight",
                "$fillshipitem->table.itemWeight",
    		"$fillshipitem->table.itemTotalCost",
    		"$fillshipitem->table.status",
    		"$fillshipitem->table.deleted",
    		"$category->table.categoryName",
    		"ssc.categoryName as subcat",
    		"$prod->table.productName",
    		"$prod->table.image",
                "$store->table.storeName"
    		)

        ->leftJoin("$category->table", "$category->table.id", "=", "$fillshipitem->table.siteCategoryId")    
        ->leftJoin("$category->table as ssc", "ssc.id", "=", "$fillshipitem->table.siteSubCategoryId")  
        ->leftJoin("$prod->table", "$prod->table.id", "=", "$fillshipitem->table.siteProductId")
        ->leftJoin("$store->table","$store->table.id", "=" ,"$fillshipitem->table.storeId")
        ->where($fillshipitem->table.".fillshipId", $fillshipId)
        ->where($fillshipitem->table.".deleted", '0')
        ->get();

        //dd(DB::getQueryLog());

        return $resultSet;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifier) {
        if (empty($id))
            return false;
        //DB::enableQueryLog();

        $row = false;

        $row = Fillshipitem::where('id', $id)
                ->update(array('deleted' => '1', 'deletedBy' => $createrModifier));

        //dd(DB::getQueryLog());

        return $row;
    }

}
