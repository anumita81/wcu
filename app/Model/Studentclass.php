<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Studentclass extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.STUDENTCLASS');
        $this->prefix = DB::getTablePrefix();
    }

    public static function getClassList($param) {
        #DB::enableQueryLog();
        $where = '1 ';
       

        $resultSet = Studentclass::whereRaw($where)
                ->where("deleted", '0')
                ->paginate($param['searchDisplay']);
#dd(DB::getQueryLog());
        return $resultSet;

    }

 /**
     * Method used to change State status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Studentclass::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Studentclass::where('id', $id)
                ->update(array('deleted' => 1, 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

}
