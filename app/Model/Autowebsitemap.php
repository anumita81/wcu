<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Autowebsitemap extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.AUTOWEBSITEMAPPING');
        $this->prefix = DB::getTablePrefix();
    }
    public function make() {
        return $this->belongsTo('App\Model\Automake', 'makeId', 'id')->orderBy('name');
    }

    public function model() {
        return $this->belongsTo('App\Model\Automodel', 'modelId', 'id')->orderBy('name');
    }

    public static function getMakeModelMap($autoWebsiteId)
    {
        $website = new Autowebsitemap;
        
        $resultSet = Autowebsitemap::select(DB::raw('CONCAT(makeId, ":" ,modelId) AS modelMakeMap'))
                ->where('websiteId', $autoWebsiteId)
                ->get();

        return $resultSet;
    }
}
