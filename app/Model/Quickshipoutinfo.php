<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Quickshipoutinfo extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.QUICKSHIPOUTINFO');
        $this->prefix = DB::getTablePrefix();
    }
    
    public static function getData($param) {
        
        $quickshipout = new Quickshipoutinfo;
        $quickshipoutTable = $quickshipout->prefix.$quickshipout->table;
        $where = "1";
        
        if (!empty($param['searchTracking']['heading']))
            $where .= " AND $quickshipoutTable.heading LIKE '%" . $param['searchTracking']['heading'] . "%'"; 
        
        if ($param['searchByCreatedOn'] != '') {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($quickshipoutTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $quickshipoutTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($quickshipoutTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($quickshipoutTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }
        
        $resultSet = Quickshipoutinfo::where("deleted","0")->whereRaw($where)->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);
        
        return $resultSet;
    }
}

