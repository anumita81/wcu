<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Commercialinvoice extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.CONSOLIDATED_COMMERCIALINVOICE');
        $this->prefix = DB::getTablePrefix();
    }
    
    public static function getData($param) {

        $adminUser = new UserAdmin;
        $commercialInvoice = new Commercialinvoice;
        $commercialInvTable = $commercialInvoice->prefix.$commercialInvoice->table;
        $shipmentTracking = new Shipmenttrackingnumber;
        $shipmentTrackingTable = $shipmentTracking->prefix.$shipmentTracking->table;
      

        $where = "$commercialInvTable.deleted = '0'";

        if (!empty($param['searchTracking']['shipperInfo']))
            $where .= " AND $commercialInvTable.shipperInfo LIKE '%" . $param['searchTracking']['shipperInfo'] . "%'";
        if (!empty($param['searchTracking']['unitNumber']))
            $where .= " AND $commercialInvTable.invoiceDetails LIKE '%\"unit\":\"" . $param['searchTracking']['unitNumber'] . "\"%'";
        if (!empty($param['searchTracking']['trackingNumber']))
            $where .= " AND $commercialInvTable.invoiceDetails LIKE '%\"tracking\":\"" . $param['searchTracking']['trackingNumber'] . "\"%'";
        if (!empty($param['searchTracking']['originCountry']))
            $where .= " AND $shipmentTrackingTable.originCountry LIKE '%". $param['searchTracking']['originCountry'] . "%'";
        if (!empty($param['searchTracking']['email']))
            $where .= " AND ".$adminUser->table.".email LIKE '%" . $param['searchTracking']['email'] . "%'";
        if (!empty($param['searchTracking']['airBillNumber']))
            $where .= " AND $commercialInvTable.airBillNumber ='" . $param['searchTracking']['airBillNumber'] . "'";
        if (!empty($param['searchTracking']['shipoutLocked']))
            $where .= " AND $commercialInvTable.shipoutLocked ='" . $param['searchTracking']['shipoutLocked'] . "'";

        if ($param['searchByCreatedOn'] != '') {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($commercialInvTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $commercialInvTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($commercialInvTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($commercialInvTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        $resultSet = Commercialinvoice::select(array("$commercialInvoice->table.id", "$commercialInvoice->table.shipperInfo", "$commercialInvoice->table.originCountry", "$commercialInvoice->table.airBillNumber", "$commercialInvoice->table.invoiceName" , "$commercialInvoice->table.invoiceFile", "$commercialInvoice->table.createdOn", "$adminUser->table.email","$commercialInvoice->table.shipoutLocked"))
                ->leftJoin($adminUser->table, "$commercialInvoice->table.createdBy", '=', "$adminUser->table.id")
                ->leftJoin($shipmentTracking->table, "$shipmentTracking->table.trackingId", "=", "$commercialInvoice->table.shipmentTrackingId")
                ->whereRaw($where)
                ->groupBy("$commercialInvoice->table.id")
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);


        return $resultSet;
    }

}