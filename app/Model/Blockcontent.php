<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Config;

class Blockcontent extends Model {

    public $table;
    public $timestamps = false;

    public function __construct() {

        parent::__construct();
        $this->table = Config::get('constants.dbTable.BLOCKCONTENT');
    }
    
    /**
     * This function used to return all available block content types
     * @return Array
     */
    public static function allBlockcontentType() {
        return array('why_shoptomydoor'=>'Why Shoptomydoor','how_it_works'=>'How it Works','service'=>'Service','value_added_servic'=>'Value Added Services','shop_for_me'=>'Shop For Me');
    }

    /**
     * Method used to fetch block content record data
     * @param int $id
     * @return object
     */
    public function getData($id = '-1',$search = '') {

        if ($id == '-1') {
            $where = '1';
            if($search!='')
            {
                $where = "slug='".$search."'";
            }
            $record = Blockcontent::whereRaw($where)->get();
        } else {
            $record = Blockcontent::find($id);
        }

        return $record;
    }

}
