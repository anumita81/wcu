<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Config;

class Zonecountry extends Model
{
    public $table;
    public $timestamps = false;

    public function __construct() {

        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.ZONE_COUNTRY');
    }

    public static function getData() {

        $return = array();

        $data = Zonecountry::select('zoneId',\DB::raw('count(zoneId) as numCountry'))
                            ->groupBy('zoneId')
                            ->get();

        foreach($data as $eachData)
        {
            $return[$eachData->zoneId] = $eachData->numCountry;
        }

        return $return;
    }

    public static function zoneWiseData($zoneId) {

        $country = new Country;
        $zoneCoutry = new Zonecountry;
        $data = DB::table("$zoneCoutry->table")
            ->join("$country->table", "$country->table.id", '=', "$zoneCoutry->table.countryId")
            ->select("$country->table.name", "$country->table.id")
            ->where($country->table.'.status','1')    
            ->where('zoneId',$zoneId)
            ->get(); 
        return $data;
    }
}