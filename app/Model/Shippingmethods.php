<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Shippingmethods extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.SHIPPINGMETHOD');
    }

    public static function getAllShippingMethods() {

        $allMethods = Shippingmethods::select('shippingid', 'shipping')->get();
        $methodArr = array();
        foreach ($allMethods as $eachMethod) {
            $methodArr[$eachMethod->shippingid] = $eachMethod->shipping;
        }

        return $methodArr;
    }

    /**
     * Method used to calculate shipping charge details
     * @param array $param
     * @return object
     */
    public static function calculateShippingMethodCharges($param = array()) {
        $shippingChargeMethodData = array();
        

        $whereRaw = " weight_min<='" . $param['totalWeight'] . "' AND (weight_limit='0' OR weight_limit>='" . $param['totalWeight'] . "')";

        if (isset($param['shippingId']) && !empty($param['shippingId']))
            $whereRaw .= " AND shippingid=" . $param['shippingId'];
        //DB::enableQueryString();
        $shippingMethodList = Shippingmethods::select('shippingid', 'shipping', 'sea', 'shipping_time', 'days', 'companyLogo', 'learn_more')
                ->where('code', '')
                ->where('active', 'Y')
                ->where('deleted', '0')
                ->whereRaw($whereRaw)
                ->orderBy('orderby', 'asc')
                ->get();
       // dd(DB::getQueryString());

       //print_r($shippingMethodList); 

        if (!empty($shippingMethodList)) {
            $i = 0;
            foreach ($shippingMethodList as $key => $shippingMethod) {
                $param['shippingId'] = $shippingMethod->shippingid;
                $shippingChargeData = Shippingcharges::getShippingChargeData($param);
               //print_r($shippingChargeData); 
                if (!empty($shippingChargeData)) {
                    $shippingChargeDetails = Shippingcharges::calculateShippingCharge($param, $shippingChargeData);
                    if (!empty($shippingChargeDetails)) {
                        $shippingChargeDetails['shippingId'] = $shippingMethod->shippingid;
                        $shippingChargeDetails['shipping'] = $shippingMethod->shipping;
                        $shippingChargeDetails['sea'] = $shippingMethod->sea;
                        if($shippingMethod->sea == 'Y')
                        {
                           $shippingChargeDetails['shipping_time'] = ceil(($shippingMethod->days/7))." Weeks";
                       }else{
                           $shippingChargeDetails['shipping_time'] = $shippingMethod->shipping_time;  
                       }
                        $shippingChargeDetails['days'] = $shippingMethod->days;
                        $shippingChargeDetails['companyLogo'] = $shippingMethod->companyLogo;
                        $shippingChargeDetails['learn_more'] = $shippingMethod->learn_more;
                        $shippingChargeMethodData[$i] = $shippingChargeDetails;
                        $i++;
                    }
                }
            }
        }
        //print_r($shippingChargeMethodData); die;

        return $shippingChargeMethodData;
    }

    public static function getShippingMethodsAndCharges($shipmentData, $shipmentDetailsData, $paymentMethod = '', $shipmentType = '', $shippingId = '-1') {

        $chargesAndMethods = array();
        $total_weight = 0;
        $total_chargeable_weight = 0;
        $totalCost = '0.00';
        $deliveryIdData = array();
        foreach ($shipmentDetailsData as $eachData) {
            if (in_array($eachData['deliveryId'], $deliveryIdData))
                continue;
            $deliveryIdData[] = $eachData['deliveryId'];
            $total_weight += $eachData['deliveryChargeableWeight'];
            $total_chargeable_weight += $eachData['deliveryChargeableWeight'];
            $totalCost += $eachData['itemTotalCost'];
        }

        $weight_condition = " weight_min<='$total_chargeable_weight' AND (weight_limit='0' OR weight_limit>='$total_chargeable_weight')";
        if ($shippingId != '-1')
            $weight_condition .= " AND shippingid=" . $shippingId;


        $matchedShipmentMethod = Shippingmethods::select('shippingid', 'shipping', 'shipping_time', 'sea', 'days')->where('code', '')
                        ->where('active', 'Y')
                        ->where('deleted', '0')
                        ->whereRaw($weight_condition)->orderBy('orderby', 'asc')->get();

        if (!empty($matchedShipmentMethod)) {
            $i = 0;
            foreach ($matchedShipmentMethod as $key => $eachShipmentMethod) {

                $shippingChargesExist = Shippingcharges::checkIfChargesExist($shipmentData, $shipmentDetailsData, $eachShipmentMethod->shippingid, $total_chargeable_weight,$totalCost);

                if (!empty($shippingChargesExist)) {

                    $shippingChargesDetails = Shippingcharges::calculateShippingCharges($shipmentDetailsData, $shippingChargesExist, $total_chargeable_weight, $paymentMethod, $shipmentData['id'], $shipmentData['userId']);

                    if (!empty($shippingChargesDetails)) {
                        $shippingChargesDetails['shippingId'] = $eachShipmentMethod->shippingid;
                        $shippingChargesDetails['shipping'] = $eachShipmentMethod->shipping;
                        if($eachShipmentMethod->sea == 'Y')
                        {
                           $shippingChargesDetails['shipping_time'] = ceil(($eachShipmentMethod->days/7)) ." Weeks";
                       }else{
                           $shippingChargesDetails['shipping_time'] = $eachShipmentMethod->shipping_time;  
                       }
                        
                        $shippingChargesDetails['days'] = $eachShipmentMethod->days;

                        $chargesAndMethods[$i] = $shippingChargesDetails;
                        $i++;
                    }
                }
            }
        }

        return $chargesAndMethods;
    }

    public static function getShippingMethods($shipmentData, $shipmentDetailsData, $paymentMethod = '', $shipmentType = '', $shippingId = '-1') {

        $shippingMethods = array();
        $total_weight = 0;
        $total_chargeable_weight = 0;
        $totalCost = '0.00';
        $deliveryIdData = array();
        foreach ($shipmentDetailsData as $eachData) {
            if (in_array($eachData['deliveryId'], $deliveryIdData))
                continue;
            $deliveryIdData[] = $eachData['deliveryId'];
            $total_weight += $eachData['deliveryChargeableWeight'];
            $total_chargeable_weight += $eachData['deliveryChargeableWeight'];
            $totalCost += $eachData['itemTotalCost'];
        }

        $weight_condition = " weight_min<='$total_chargeable_weight' AND (weight_limit='0' OR weight_limit>='$total_chargeable_weight')";
        if ($shippingId != '-1')
            $weight_condition .= " AND shippingid=" . $shippingId;


        $matchedShipmentMethod = Shippingmethods::select('shippingid', 'shipping', 'shipping_time')->where('code', '')
                        ->where('active', 'Y')
                        ->where('deleted', '0')
                        ->whereRaw($weight_condition)->orderBy('orderby', 'asc')->get();

        if (!empty($matchedShipmentMethod)) {
            $i = 0;
            foreach ($matchedShipmentMethod as $key => $eachShipmentMethod) {

                $shippingChargesExist = Shippingcharges::checkIfChargesExist($shipmentData, $shipmentDetailsData, $eachShipmentMethod->shippingid, $total_chargeable_weight,$totalCost);

                if (!empty($shippingChargesExist)) {
                    $shippingMethods[$eachShipmentMethod->shippingid] = $eachShipmentMethod->shipping;
                }
            }
        }

        return $shippingMethods;
    }

    public static function getDeliveryShippingMethods($shipmentData, $shipmentDetailsData) {
        $shippingMethods = array();
        $total_chargeable_weight = 0;
        $total_item_cost = '0.00';
        
        foreach ($shipmentDetailsData as $shipmentDetails) {

            $total_chargeable_weight +=!empty($shipmentDetails['deliveryChargeableWeight']) ? $shipmentDetails['deliveryChargeableWeight'] : 0;
            $total_item_cost += $shipmentDetails['itemTotalCost'];
        }


        $weight_condition = " weight_min <= '$total_chargeable_weight' AND (weight_limit = '0' OR weight_limit >= '$total_chargeable_weight')";

        $matchedShipmentMethod = Shippingmethods::select('shippingid', 'shipping', 'shipping_time')->where('code', '')
                        ->where('active', 'Y')
                        ->where('deleted', '0')
                        ->whereRaw($weight_condition)->orderBy('orderby', 'asc')->get();

        if (!empty($matchedShipmentMethod)) {
            $i = 0;
            foreach ($matchedShipmentMethod as $key => $eachShipmentMethod) {
                $shippingChargesExist = Shippingcharges::checkIfChargesExist($shipmentData, $shipmentDetailsData, $eachShipmentMethod->shippingid, $total_chargeable_weight, $total_item_cost);
                if (!empty($shippingChargesExist)) {
                    $shippingMethods[$key]['shippingid'] = $eachShipmentMethod->shippingid;
                    $shippingMethods[$key]['shipping'] = $eachShipmentMethod->shipping;
                }

            }
        }

        //print_r($shippingMethods); die;

        return $shippingMethods;
    }

}
