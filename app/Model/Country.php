<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Country extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.COUNTRY');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to fetch country list
     * @param array $param
     * @return object
     */
    public static function getCountryList($param) {
        $where = '1';

        if (!empty($param['searchByRegion']))
            $where .= "  AND region ='" . $param['searchByRegion'] . "'";

        $resultSet = Country::whereRaw($where)
                ->select(array('id', 'code', 'name', 'status', 'isdCode'))
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to change country status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Country::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to fetch country list for ajax
     * @param array $param
     * @return object
     */
    public static function getCountryData($param = 'all') {
        $where = 'status="1"';
        if (!empty($param != 'all'))
            $where .= "  AND region ='" . $param . "'";

        $resultSet = Country::whereRaw($where)
                        ->select(array('id', 'name'))->get();

        return $resultSet;
    }

    /**
     * Method used to fetch country list by region
     * @return array
     */
    public static function getRegionWiseCountry() {
        $data = array();

        $where = 'status="1"';

        $resultSet = Country::whereRaw($where)
                        ->select(array('id', 'region'))->get();

        foreach ($resultSet as $result) {
            $data[$result->region][] = $result->id;
        }

        return $data;
    }
    /**
     * This function return country state city mapped array
     * @param type $countryCode
     * @param type $stateId
     * @return type <Array>
     */
    public static function countryMappedStateCity($countryCode = '-1', $stateId = '-1') {

        $countryObj = New Country;
        $stateObj = New State;
        $cityObj = New City;
        $mappedData = array();
        $allData = array();

        $allData = DB::select("select co.code as countryCode, co.name as countryName, st.id as stateId, st.name as stateName, ct.id as cityId, ct.name as cityName from ".$countryObj->prefix.$countryObj->table." as co inner join ".$stateObj->prefix.$stateObj->table." as st on (co.code = st.countryCode and st.id=".$stateId." and co.code='".$countryCode."') inner join ".$cityObj->prefix.$cityObj->table." as ct on (co.code = ct.countryCode and ct.stateCode=st.code and ct.countryCode='".$countryCode."') order by co.id asc");              
        if(!empty($allData))
        {
            foreach($allData as $eachData)
            {
                $mappedData[$eachData->countryCode]['name'] = $eachData->countryName;
                $mappedData[$eachData->countryCode]['data'][$eachData->stateId]['name'] = $eachData->stateName;
                $mappedData[$eachData->countryCode]['data'][$eachData->stateId]['data'][$eachData->cityId] = $eachData->cityName;
            }
        }
        return $mappedData;
    }

}
