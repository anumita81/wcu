<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class FundWallet extends Model {

    public $table;
    public $timestamps = false;
    protected $fillable = ['user'];

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.FUNDWALLET');
    }

}
