<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
use Spatie\Activitylog\Traits\LogsActivity;

class Invoice extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    protected $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.INVOICE');
        $this->prefix = DB::getTablePrefix();
    }

    public static function checkIfExtraInvoiceExist($invoiceData) {
        $extraChargeExist = 0;
        if (!empty($invoiceData)) {
            foreach ($invoiceData as $eachInvoiceData) {
                if ($eachInvoiceData->invoiceType == 'invoice' && $eachInvoiceData->extraCostCharged == 'Y' && $eachInvoiceData->paymentStatus == 'unpaid') {
                    $extraChargeExist = 1;
                    break;
                }
            }
        }

        return $extraChargeExist;
    }

    public static function getUnpaidInvoiceList($param) {
        $invoice = new Invoice;
        $procurement = new Procurement;
        $shipment = new Shipment;
        $autoshipment = new Autoshipment;
        $paymentMethod = new Paymentmethod;

        $resultSet = Invoice::select(array("$invoice->table.*", "$procurement->table.paidCurrencyCode", "$procurement->table.exchangeRate", "$shipment->table.paidCurrencyCode AS shipmentPaidCurrencyCode", "$shipment->table.exchangeRate AS shipmentExchangeRate", "$autoshipment->table.paidCurrencyCode AS autoPaidCurrencyCode", "$autoshipment->table.exchangeRate AS autoExchangeRate", "$paymentMethod->table.paymentMethod",))
                        ->leftJoin($procurement->table, "$invoice->table.procurementId", '=', "$procurement->table.id")
                        ->leftJoin($shipment->table, "$invoice->table.shipmentId", '=', "$shipment->table.id")
                        ->leftJoin($autoshipment->table, "$invoice->table.shipmentId", '=', "$autoshipment->table.id")
                        ->leftJoin($paymentMethod->table, "$invoice->table.paymentMethodId", '=', "$paymentMethod->table.id")
                        ->where($invoice->table . '.userEmail', $param['userEmail'])
                        ->where($invoice->table . '.invoiceType', 'invoice')
                        ->where($invoice->table . '.paymentMethodId', NULL)
                        ->where($invoice->table . '.paymentStatus', 'unpaid')
                        ->where($invoice->table . '.extraCostCharged', 'Y')
                        ->whereNotNull($invoice->table . '.extraCostCharged')
                        ->groupby($invoice->table . '.id')
                        ->orderby('id', 'desc')
                        ->take($param['perPage'])
                        ->skip($param['offset'])->get();

        return $resultSet;
    }

}
