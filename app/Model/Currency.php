<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Currency extends Model {

    public $table;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.CURRENCY');
    }

    /**
     * Method used to fetch currency list
     * @param array $param
     * @return object
     */
    public static function getCurrencyList($param) {
        $defaultCurrency = new Defaultcurrency;
        $currency = new Currency;

        $where = "deleted = '0'";

        $resultSet = Currency::select(array("name", "code", "codeInt", "exchangeRate", "$currency->table.id", "$currency->table.symbol", "$currency->table.isDefault", "$currency->table.status",))
                ->leftJoin($defaultCurrency->table, "$defaultCurrency->table.id", '=', "$currency->table.currencyId")
                ->whereRaw($where)
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $resultSet;
    }

    /**
     * Method used to change currency status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Currency::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Currency::where('id', $id)
                ->update(array('deleted' => 1, 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to set a currency as default
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function setDefault($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        Currency::whereRaw(1)
                ->update(array('isDefault' => '0', 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        $row = Currency::where('id', $id)
                ->update(array('isDefault' => '1', 'exchangeRate' => '1', 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to fetch default currency details
     * @return object
     */
    public static function getDefaultCurrency() {
        $defaultCurrency = new Defaultcurrency;
        $currency = new Currency;
        $where = "deleted = '0'";

        $resultSet = Currency::select(array("name", "code", "codeInt", "exchangeRate", "$currency->table.id", "$currency->table.symbol", "$currency->table.isDefault", "$currency->table.status", "$currency->table.format",))
                        ->leftJoin($defaultCurrency->table, "$defaultCurrency->table.id", '=', "$currency->table.currencyId")
                        ->whereRaw($where)
                        ->where('isDefault', 1)->get();

        return $resultSet;
    }

    /**
     * Method used to fetch default currency details
     * @return object
     */
    public static function getCurrencyData($code) {
        $defaultCurrency = new Defaultcurrency;
        $currency = new Currency;
        $where = "deleted = '0'";

        $resultSet = Currency::select(array("name", "code", "codeInt", "exchangeRate", "$currency->table.id", "$currency->table.symbol", "$currency->table.isDefault", "$currency->table.status", "$currency->table.format",))
                        ->leftJoin($defaultCurrency->table, "$defaultCurrency->table.id", '=', "$currency->table.currencyId")
                        ->whereRaw($where)
                        ->where('code', $code)->get();

        return $resultSet;
    }

    /**
     * Method used to fetch currency list
     * @param array $param
     * @return object
     */
    public static function getAllCurrencyList($currencyCode = '') {
        $defaultCurrency = new Defaultcurrency;
        $currency = new Currency;

        $where = "deleted = '0' AND status = '1'";

        if (!empty($currencyCode))
            $where .= " AND code != '" . $currencyCode . "'";

        $resultSet = Currency::select(array("name", "code", "$currency->table.id", "$currency->table.symbol", "$currency->table.exchangeRate"))
                ->leftJoin($defaultCurrency->table, "$defaultCurrency->table.id", '=', "$currency->table.currencyId")
                ->whereRaw($where)
                ->orderBy("name", "asc")
                ->get();

        return $resultSet;
    }

    /**
     * Method used to fetch currency list except default set currency
     * @return object
     */
    public static function getAllCurrencyListExceptDefault() {
        $defaultCurrency = new Defaultcurrency;
        $currency = new Currency;

        $where = "deleted = '0' AND status = '1' AND isDefault = '0'";

        $resultSet = Currency::select(array("name", "code", "$currency->table.id", "$currency->table.symbol"))
                ->leftJoin($defaultCurrency->table, "$defaultCurrency->table.id", '=', "$currency->table.currencyId")
                ->whereRaw($where)
                ->orderBy("name", "asc")
                ->get();

        return $resultSet;
    }

    /**
     * This function used to get exchange rate between default currency and selected currency
     * @param type $from_currency
     * @param type $to_currency
     * @return type
     */
    public static function currencyExchangeRate($from_currency, $to_currency) {
        $combined = $from_currency . "_" . $to_currency;

        $exchangeRate = 1;
        //Edited in 07-06-2019

       // $getexchangerate = $getexchangerateother = array();

        $getexchangerate = \App\Model\Currencyconversion::where('fromCurrency', $from_currency)->where('toCurrency', $to_currency)->get()->toArray();

        if(!empty($getexchangerate))
        {
            $exchangeRate =  $getexchangerate[0]['exchangeRate'];
        }
        else{

          $getexchangerateother = \App\Model\Currencyconversion::where('toCurrency', $from_currency)->where('fromCurrency', $to_currency)->get()->toArray();           

          if(!empty($getexchangerateother)) 
          {
            //print_r($getexchangerateother);die;
            if($getexchangerateother[0]['exchangeRate']>0)
                $exchangeRate = (1/$getexchangerateother[0]['exchangeRate']);
          }else{


                $apikey = "4d5cf943-36c4-4a02-ae69-3627b5b65253";
                //$curlUrl = 'http://free.currencyconverterapi.com/api/v3/convert?q=' . $combined .'&compact=ultra&apiKey=' .$apikey;
                $curlUrl = 'https://api.currconv.com/api/v7/convert?q='.$combined.'&compact=ultra&apiKey='.$apikey;

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL =>  $curlUrl,
                ));

                $resp = curl_exec($curl);
                curl_close($curl);
                $decoded = json_decode($resp);

                /* IF EXCHANGE RATE NOT FOUND */
                if (isset($decoded->query->count) &&  $decoded->query->count== 0) {
                    $exchangeRate = 1;
                }else if(!empty($decoded)) {
                    $exchangeRate = $decoded->$combined;
                }else
                {
                    $exchangeRate = 1;
                }

          }

        }

        //API Key generated for free user - Changed in 19-02-2019

        //$apikey = "b96697ff702b0b239207";

        
        return $exchangeRate;
    }
    
    /**
     * This function used to get costs after conversion to selected currency
     * @param type $amount
     * @param type $exchangeRate
     * @return type
     */
    public static function currencyConverter($amount,$exchangeRate) {

        return round(($amount * $exchangeRate),2);
    }

}
