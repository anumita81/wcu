<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

//use App\libraries\helpers;
//use App\libraries\dbHelpers;

class Zonestate extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.ZONE_STATE');
        $this->prefix = DB::getTablePrefix();
    }

    public static function getData() {

        $return = array();

        $data = Zonestate::select('zoneId',\DB::raw('count(zoneId) as numCountry'))
                            ->groupBy('zoneId')
                            ->get();

        foreach($data as $eachData)
        {
            $return[$eachData->zoneId] = $eachData->numCountry;
        }

        return $return;
    }

    public static function zoneWiseData($zoneId) {

        $country = new Country;
        $state = new State;
        $zoneState = new Zonestate;
        
        $resultSet = DB::table("$country->table")
            ->join($state->table, "$state->table.countryCode",'=',"$country->table.code")
            ->join("$zoneState->table", "$zoneState->table.stateId", '=', "$state->table.id")
            ->select("$country->table.name as country_name", "$state->table.id", "$state->table.name")
            ->where('zoneId',$zoneId)
            ->where($state->table.'.deleted', '0')
            ->where($state->table.'.status', '1')
            ->where($country->table.'.status','1')    
            ->get();

        return $resultSet;
    }
}