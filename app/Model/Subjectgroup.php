<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Subjectgroup extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.SUBJECTGROUP');
        $this->prefix = DB::getTablePrefix();
    }

    

 
}
