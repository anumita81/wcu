<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
//use App\libraries\helpers;
//use App\libraries\dbHelpers;
use Illuminate\Support\Facades\DB;
use Config;

class Smstemplate extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    //protected $table = Config::get('constants.dbTable.EMAILTEMPLATE');

    public $table;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.SMSTEMPLATE');
    }

    public $timestamps = false;

    /**
     * Method used to fetch Email Template list
     * @param array $param
     * @return object
     */
    public static function getSmsTemplateList($param) {
        $where = "deleted='0' AND status='1'";

        if (!empty($param['searchByType']))
            $where .= " AND templateType = '" . $param['searchByType'] . "'";

        $resultSet = Smstemplate::whereRaw($where)
                ->select(array('id', 'templateKey', 'templateBody', 'createdOn', 'createdBy', 'updatedOn', 'updatedBy', 'deleted', 'status'))
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);


        //dd($resultSet);
        return $resultSet;
    }

}
