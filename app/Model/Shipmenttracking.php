<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

//use App\libraries\helpers;
//use App\libraries\dbHelpers;

class Shipmenttracking extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.SHIPMENTTRACKING');
        $this->prefix = DB::getTablePrefix();
    }

    /**
     * Method used to fetch shipment tracking list
     * @param array $param
     * @return object
     */
    public static function getTrackingList($param) {
        $shipmentTracking = new Shipmenttracking;
        $shipmentTrackingNo = new Shipmenttrackingnumber;
        $updateType = new Updatetype;
        $deliveryCompany = new Deliverycompany;
        $adminUser = new UserAdmin;

        $shipmentTrackingTable = $shipmentTracking->prefix . $shipmentTracking->table;
        $shipmentTrackingNoTable = $shipmentTracking->prefix . $shipmentTrackingNo->table;

        $where = "$shipmentTrackingTable.deleted='0'";

        if (!empty($param['searchShipmentTracking']['updateType']))
            $where .= " AND $shipmentTrackingTable.updateTypeId = " . $param['searchShipmentTracking']['updateType'];

        if (!empty($param['searchShipmentTracking']['deliveryCompanyId']))
            $where .= " AND $shipmentTrackingTable.deliveryCompanyId = " . $param['searchShipmentTracking']['deliveryCompanyId'];

        if (!empty($param['searchShipmentTracking']['name']))
            $where .= " AND $shipmentTrackingTable.name LIKE '%" . $param['searchShipmentTracking']['name'] . "%'";

        if (!empty($param['searchShipmentTracking']['trackingNumber']))
            $where .= " AND $shipmentTrackingNoTable.trackingNumber LIKE '%" . $param['searchShipmentTracking']['trackingNumber'] . "%'";

        if ($param['searchByCreatedOn'] != '') {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($shipmentTrackingTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $shipmentTrackingTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($shipmentTrackingTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($shipmentTrackingTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        $resultSet = Shipmenttracking::select(array("$shipmentTracking->table.id", "$shipmentTracking->table.name", "$shipmentTracking->table.message", "$shipmentTracking->table.createdOn",
                    "$adminUser->table.email as createdBy", "$updateType->table.name AS updateTypeName", "$deliveryCompany->table.name AS deliveryCompanyName",
                    DB::raw("(SELECT COUNT(id) FROM $shipmentTrackingNoTable WHERE $shipmentTrackingNoTable.trackingId = $shipmentTrackingTable.id) as trackingNumCount"),))
                ->leftJoin($updateType->table, "$shipmentTracking->table.updateTypeId", '=', "$updateType->table.id")
                ->leftJoin($deliveryCompany->table, "$shipmentTracking->table.deliveryCompanyId", '=', "$deliveryCompany->table.id")
                ->leftJoin($adminUser->table, "$shipmentTracking->table.createdBy", '=', "$adminUser->table.id")
                ->leftJoin($shipmentTrackingNo->table, "$shipmentTrackingNo->table.trackingId", '=', "$shipmentTracking->table.id")
                ->whereRaw($where)
                ->groupBy("$shipmentTracking->table.id")
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        //    dd(DB::getQueryLog()); 

        return $resultSet;
    }

    /**
     * Method used to fetch shipment tracking list
     * @param array $param
     * @return object
     */
    public static function exportData($param) {
        $shipmentTracking = new Shipmenttracking;
        $shipmentTrackingNo = new Shipmenttrackingnumber;
        $shipmentTrackingNotes = new Shipmenttrackingnotes;
        $updateType = new Updatetype;
        $deliveryCompany = new Deliverycompany;
        $adminUser = new UserAdmin;

        $shipmentTrackingTable = $shipmentTracking->prefix . $shipmentTracking->table;
        $shipmentTrackingNoTable = $shipmentTracking->prefix . $shipmentTrackingNo->table;
        $shipmentTrackingNotesTable = $shipmentTracking->prefix . $shipmentTrackingNotes->table;

        $where = "$shipmentTrackingTable.deleted='0'";

        if (!empty($param['searchShipmentTracking']['updateType']))
            $where .= " AND $shipmentTrackingTable.updateTypeId = " . $param['searchShipmentTracking']['updateType'];

        if (!empty($param['searchShipmentTracking']['deliveryCompanyId']))
            $where .= " AND $shipmentTrackingTable.deliveryCompanyId = " . $param['searchShipmentTracking']['deliveryCompanyId'];

        if (!empty($param['searchShipmentTracking']['name']))
            $where .= " AND $shipmentTrackingTable.name LIKE '%" . $param['searchShipmentTracking']['name'] . "%'";

        if (!empty($param['searchShipmentTracking']['trackingNumber']))
            $where .= " AND $shipmentTrackingNoTable.trackingNumber LIKE '%" . $param['searchShipmentTracking']['trackingNumber'] . "%'";

        if ($param['searchByCreatedOn'] != '') {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($shipmentTrackingTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $shipmentTrackingTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($shipmentTrackingTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($shipmentTrackingTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        $resultSet = Shipmenttracking::select(array("$shipmentTracking->table.name AS Name", "$updateType->table.name AS Update Type", "$deliveryCompany->table.name AS Delivery Company",
                    DB::raw("DATE_FORMAT($shipmentTrackingTable.deliveredOn, '%m-%d-%Y') AS `Delivered On`"), DB::raw("(GROUP_CONCAT($shipmentTrackingNoTable.trackingNumber)) as `Tracking Number`"),
                    DB::raw("(GROUP_CONCAT($shipmentTrackingNotesTable.notes)) as `Notes`"), "$shipmentTracking->table.message AS Customer Message",
                    "$adminUser->table.email as Created By", DB::raw("DATE_FORMAT($shipmentTrackingTable.createdOn, '%m-%d-%Y') AS `Created On`"),))
                ->leftJoin($updateType->table, "$shipmentTracking->table.updateTypeId", '=', "$updateType->table.id")
                ->leftJoin($deliveryCompany->table, "$shipmentTracking->table.deliveryCompanyId", '=', "$deliveryCompany->table.id")
                ->leftJoin($adminUser->table, "$shipmentTracking->table.createdBy", '=', "$adminUser->table.id")
                ->leftJoin($shipmentTrackingNo->table, "$shipmentTrackingNo->table.trackingId", '=', "$shipmentTracking->table.id")
                ->leftJoin($shipmentTrackingNotes->table, "$shipmentTrackingNotes->table.trackingId", '=', "$shipmentTracking->table.id")
                ->whereRaw($where)
                ->groupBy("$shipmentTracking->table.id")
                ->orderBy("$shipmentTracking->table.id", "desc")
                ->get();

        return $resultSet;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Shipmenttracking::where('id', $id)->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

}
