<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;
use Spatie\Activitylog\Traits\LogsActivity;

class Shipmentwarehouselocation extends Model {

    use LogsActivity;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    protected $prefix;
    public $timestamps = false;
    protected static $logName = 'warehouseLocation';

    protected static $logAttributes = ['id', 'shipmentId', 'warehouseRowId', 'warehouseZoneId'];

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.SHIPMENTWAREHOUSE');
        $this->prefix = DB::getTablePrefix();
    }

}
