<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Procurementitemstatus extends Model {

    public $table;
    protected $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.PROCUREMENTITEMSTATUS');
        $this->prefix = DB::getTablePrefix();
    }

    public static function getOrderStatusProgress($procurementId) {
    	$formattedData = array();
    	$data = Procurementitemstatus::where('procurementId',$procurementId)->get();
    	if(count($data)>0)
    	{
    		foreach ($data as $key => $eachData) {
    			$formattedData[$eachData->status] = $eachData->updatedOn;
    		}
    	}

    	return $formattedData;
    }

}
