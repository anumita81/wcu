<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Shipmentdelivery extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
        $this->table = Config::get('constants.dbTable.SHIPMENTDELIVERY');
        $this->prefix = DB::getTablePrefix();
    }
    
    public static function deliveryStatus() {
        return array('in_warehouse'=>'In Warehouse','in_transit'=>'In Transit','custom_clearing'=>'Customs Clearing','destination_warehouse'=>'In Destination Warehouse','out_for_delivery'=>'Out For Delivery','delivered'=>'Delivered');
    }

    public static function deliveryStatusQuickShipout()
    {
         return array('in_warehouse'=>'In Warehouse','in_transit'=>'In Transit','custom_clearing'=>'Customs Clearing','destination_warehouse'=>'In Destination Warehouse');
    }   

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Shipmentdelivery::where('id', $id)
                ->update(array('deleted' => 1, 'deletedBy' => $createrModifierId, 'deletedByType' => 'admin', 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }
    
    public static function fetchUsedFieldIds($field) {
        
        $resultset = Shipmentdelivery::distinct()->whereNotNull($field)->where("deleted", "0")->pluck($field)->toArray();

        return $resultset;
    }

}
