<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class UssdPaymentSettings extends Model
{
    protected $table;
    public $timestamps = false;
    public $prefix;

    public function __construct() {
        parent::__construct(); 
        $this->table = Config::get('constants.dbTable.USSDPAYMENTSETTINGS');
        $this->prefix = DB::getTablePrefix();
    }

    

    
}
