<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Consolidatedtracking extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.CONSOLIDATEDTRACKING');
        $this->prefix = DB::getTablePrefix();
    }
    
    public static function submitCloseOptions() {
        return array(
            'air' => array(
                'high_value_item' => array(
                    'label' => 'Are there High Value items - Phones, Jewelries and High Value Electronics?',
                    'options' => array(
                        'middle_of_box_highvalue' => 'Were they loaded in the middle of consolidated box?',
                    )
                ),
                'food_drug_item' => array(
                    'label' => 'Are there food, drug and health regulated items?',
                    'options' => array(
                        'middle_of_box_fooddrug' => 'Were they loaded in the middle of consolidated box?',
                    )
                ),
                'fragile_item' => array(
                    'label' => 'Are there fragile items?',
                    'options' => array(
                        'secure_box' => 'Were they crated, or packed in its own secured box? ',
                    )
                ),
                'liquid_item' => array(
                    'label' => 'Are there Liquid item?',
                    'options' => array(
                        'middle_of_box_liquiditem' => 'Were they sealed and loaded in the middle of the box?',
                    )
                ),
                'pictures_uploaded' => array(
                    'label' => 'Have you uploaded pictures of the final consolidated box, with details of its weight and dimensions in the notes section.',
                    'options' => array(
                    )
                )
            ),
            'sea' => array(
                'furniture_in_container' => array(
                    'label' => 'Is there furniture in the container?',
                    'options' => array(
                        'type' => 'textfield',
                        'name' => 'percent_furniture',
                        'label' => 'How many %?'
                    )
                ),
                'tv_in_container' => array(
                    'label' => 'Are there TV’s in the container?',
                    'options' => array(
                        'type' => 'textfield',
                        'name' => 'pieces_furniture',
                        'label' => 'How many pieces?'
                    )
                ),
                'machinery_in_container' => array(
                    'label' => 'Are there Generators/Machinery in the container?',
                    'options' => array(
                        'type' => 'textfield',
                        'name' => 'pieces_machinery',
                        'label' => 'How many pieces?'
                    )
                ),
                'cookingoil_in_container' => array(
                    'label' => 'Are there Groundnut or cooking oil in the container?',
                    'options' => array(
                        'type' => 'textfield',
                        'name' => 'cartons_cookingoil',
                        'label' => 'How many cartons?'
                    )
                ),
                'supermarketitem_in_container' => array(
                    'label' => 'Are there Supermarket items in the container?',
                    'options' => array(
                        'type' => 'textfield',
                        'name' => 'percent_supermarketitem',
                        'label' => 'How many %?'
                    )
                ),
                'cosmetics_in_container' => array(
                    'label' => 'Are there cosmetics in the container?',
                    'options' => array(
                        'type' => 'textfield',
                        'name' => 'boxes_cosmetics',
                        'label' => 'How many boxes?'
                    )
                ),
                'rice_in_container' => array(
                    'label' => 'Is there Rice in the container?',
                    'options' => array(
                        'type' => 'textfield',
                        'name' => 'bags_rice',
                        'label' => 'How many bags?'
                    )
                ),
                'cookingoil_in_container' => array(
                    'label' => 'Are there Groundnut or cooking oil in the container?',
                    'options' => array(
                        'type' => 'textfield',
                        'name' => 'cartons_cookingoil',
                        'label' => 'How many cartons?'
                    )
                ),
                'usedcloths_in_container' => array(
                    'label' => 'Is there used clothes and shoes in the container?',
                    'options' => array(
                        'type' => 'textfield',
                        'name' => 'percent_usedcloths',
                        'label' => 'How many %?'
                    )
                ),
                'bicycles_in_container' => array(
                    'label' => 'Is there Bicycles in the container?',
                    'options' => array(
                        'type' => 'textfield',
                        'name' => 'pieces_bicycles',
                        'label' => 'How many pieces?'
                    )
                ),
                'supplements_in_container' => array(
                    'label' => 'Is there Food supplements/Drugs in the container?',
                    'options' => array(
                        'type' => 'textfield',
                        'name' => 'percent_supplements',
                        'label' => 'How many %?'
                    )
                ),
                'books_in_container' => array(
                    'label' => 'Is there new/used books in the container?',
                    'options' => array(
                        'type' => 'textfield',
                        'name' => 'percent_books',
                        'label' => 'How many %?'
                    )
                ),
                'fragrance_in_container' => array(
                    'label' => 'Is there fragrance/air fresher in the container?',
                    'options' => array(
                        'type' => 'textfield',
                        'name' => 'percent_fragrance',
                        'label' => 'How many %?'
                    )
                ),
                'stationary_in_container' => array(
                    'label' => 'Is there stationery in the container?',
                    'options' => array(
                        'type' => 'textfield',
                        'name' => 'percent_stationary',
                        'label' => 'How many %?'
                    )
                ),
                'car_in_container' => array(
                    'label' => 'Are there cars in the container?',
                    'options' => array(
                        'type' => 'textfield',
                        'isNonNumeric' => '1',
                        'name' => 'vin_car',
                        'label' => 'Enter the full VIN number as seen from the car physically'
                    )
                ),
                'needexplain_in_container' => array(
                    'label' => 'Are there other things that need explanation? Eg bulky items',
                    'options' => array(
                        'type' => 'textfield',
                        'isNonNumeric' => '1',
                        'name' => 'notes_needexplain',
                        'label' => 'Please explain the notes section'
                    )
                ),
                'finalview_container' => array(
                    'label' => 'Have you uploaded pictures of the final view of the container?',
                    'showonno' => '1',
                    'options' => array(
                        'type' => 'image',
                        'name' => 'image_finalview',
                        'label' => 'Please upload before closing the consolidated tracking'
                    )
                ),
                'keychained_steering' => array(
                    'label' => 'Has the key to the car been chained to the steering wheel?',
                    'options' => array(
                        'type' => 'image',
                        'name' => 'image_stearing',
                        'label' => 'Please upload pictures showing this.'
                    )
                ),
                'car_picture' => array(
                    'label' => 'Have you uploaded pictures of cars in the container?',
                    'showonno' => '1',
                    'options' => array(
                        'type' => 'image',
                        'name' => 'image_car',
                        'label' => 'Please upload now'
                    )
                ),
            )
        );
    }

    /**
     * Method used to fetch tracking list
     * @param array $param
     * @return object
     */
    public static function getTrackingList($param) {
        $tracking = new Consolidatedtracking;
        $adminUser = new UserAdmin;
        $trackingNo = new Consolidatedtrackingnumber;

        $trackingTable = $tracking->prefix . $tracking->table;
        $trackingNoTable = $tracking->prefix . $trackingNo->table;

        $where = "$trackingTable.deleted = '0'";

        if (!empty($param['searchTracking']['name']))
            $where .= " AND $trackingTable.name LIKE '%" . $param['searchTracking']['name'] . "%'";

        if (!empty($param['searchTracking']['trackingNumber']))
            $where .= " AND $trackingNoTable.trackingNumber LIKE '%" . $param['searchTracking']['trackingNumber'] . "%'";

        if ($param['searchByCreatedOn'] != '') {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($trackingTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $trackingTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($trackingTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($trackingTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        $resultSet = Consolidatedtracking::select(array("$tracking->table.id", "$tracking->table.name", "$tracking->table.createdOn","$tracking->table.isClosed", "$adminUser->table.email", DB::raw("(SELECT COUNT(id) FROM $trackingNoTable  WHERE $trackingNoTable.trackingId = $trackingTable.id) as trackingNumCount")))
                ->leftJoin($adminUser->table, "$tracking->table.createdBy", '=', "$adminUser->table.id")
                ->leftJoin($trackingNo->table, "$trackingNo->table.trackingId", '=', "$tracking->table.id")
                ->whereRaw($where)
                ->groupBy("$tracking->table.id")
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);


        return $resultSet;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Consolidatedtracking::where('id', $id)->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to fetch data for export
     * @param array $param
     * @return object
     */
    public static function exportData($param) {
        $tracking = new Consolidatedtracking;
        $adminUser = new UserAdmin;
        $trackingNo = new Consolidatedtrackingnumber;

        $trackingTable = $tracking->prefix . $tracking->table;
        $trackingNoTable = $tracking->prefix . $trackingNo->table;

        $where = "$trackingTable.deleted='0'";


        if (!empty($param['searchTracking']['name']))
            $where .= " AND $trackingTable.name LIKE '%" . $param['searchTracking']['name'] . "%'";

        if (!empty($param['searchTracking']['trackingNumber']))
            $where .= " AND $trackingTable.trackingNumber LIKE '%" . $param['searchTracking']['trackingNumber'] . "%'";

        if ($param['searchByCreatedOn'] != '') {
            if ($param['searchByCreatedOn'] == 'thismonth')
                $where .= "  AND MONTH($trackingTable.createdOn) ='" . \Carbon\Carbon::now()->month . "'";
            else if ($param['searchByCreatedOn'] == 'thisweek')
                $where .= "  AND $trackingTable.createdOn BETWEEN '" . \Carbon\Carbon::now()->startOfWeek() . "' AND '" . \Carbon\Carbon::now()->endOfWeek() . "'";
            else if ($param['searchByCreatedOn'] == 'today')
                $where .= "  AND date($trackingTable.createdOn) ='" . \Carbon\Carbon::today()->toDateString() . "'";
            else if ($param['searchByCreatedOn'] == 'custom' && !empty($param['searchByDate'])) {
                $searchDate = explode('-', $param['searchByDate']);
                $searchByStartDate = trim($searchDate[0]);
                $searchByEndDate = trim($searchDate[1]);
                $where .= "  AND date($trackingTable.createdOn) BETWEEN '" . \Carbon\Carbon::parse($searchByStartDate)->format('Y-m-d') . "' AND '" . \Carbon\Carbon::parse($searchByEndDate)->format('Y-m-d') . "'";
            }
        }

        $resultSet = Consolidatedtracking::select(array("$tracking->table.name AS Name", DB::raw("DATE_FORMAT($trackingTable.deliveredOn, '%m-%d-%Y') AS `Delivered On`"), DB::raw("(GROUP_CONCAT($trackingNoTable.trackingNumber)) as `Tracking Number`"),
                    "$adminUser->table.email as Created By", DB::raw("DATE_FORMAT($trackingTable.createdOn, '%m-%d-%Y') AS `Created On`"),))
                ->leftJoin($adminUser->table, "$tracking->table.createdBy", '=', "$adminUser->table.id")
                ->leftJoin($trackingNo->table, "$trackingNo->table.trackingId", '=', "$tracking->table.id")
                ->whereRaw($where)
                ->groupBy("$tracking->table.id")
                ->orderBy("$tracking->table.id", "desc")
                ->get();

        return $resultSet;
    }

}
