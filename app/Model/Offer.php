<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Config;

class Offer extends Model {

    public $table;
    public $prefix;
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        $this->table = Config::get('constants.dbTable.OFFERS');
        $this->prefix = DB::getTablePrefix();
    }

    public static function getList($param) {
        $where = '1';
        $resultSet = Offer::whereRaw($where)->where('deleted', '0')->where('type', 'general')
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);
        return $resultSet;
    }

    public static function getListById($id) {
        $resultSet = Offer::where('id', $id)->where('deleted', '0')->get();
        return $resultSet;
    }

    /**
     * Method used to change tax status
     * @param integer $id
     * @param integer $createrModifierId
     * @param string $newStatus
     * @return boolean
     */
    public static function changeStatus($id, $createrModifierId, $newStatus = '') {
        if (empty($id))
            return false;

        $row = false;

        $row = Offer::where('id', $id)
                ->update(array('status' => $newStatus, 'modifiedBy' => $createrModifierId, 'modifiedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to delete record
     * @param integer $id
     * @param integer $createrModifierId
     * @return boolean
     */
    public static function deleteRecord($id, $createrModifierId) {
        if (empty($id))
            return false;

        $row = false;

        $row = Offer::where('id', $id)->update(array('deleted' => '1', 'deletedBy' => $createrModifierId, 'deletedOn' => Config::get('constants.CURRENTDATE')));

        return $row;
    }

    /**
     * Method used to count the offers code are being used for shipment
     * @param integer $code
     * @param string $type
     * @return integer
     */
    public static function countCouponUsedShipment($code, $type) {
        $count = 0;
        if ($type == 'count') {
            /* COUNT FROM SHIPMENT */
            $count = Shipment::where('couponcodeApplied', "$code")->where('deleted', '0')->where('paymentStatus', 'paid')->where('shippingMethod', 'Y')->count();
        } else if ($type == 'total') {
            /* TOTAL FROM SHIPMENT */
            $count = Shipment::where('couponcodeApplied', "$code")->where('deleted', '0')->where('paymentStatus', 'paid')->where('shippingMethod', 'Y')->sum('totalCost');
        } else {
            /* DISCOUNT FROM SHIPMENT */
            $count = Shipment::where('couponcodeApplied', "$code")->where('deleted', '0')->where('paymentStatus', 'paid')->where('shippingMethod', 'Y')->sum('totalDiscount');
        }
        return $count;
    }

    /**
     * Method used to count the offers code are being used for auto
     * @param integer $code
     * @param string $type
     * @return integer
     */
    public static function countCouponUsedAuto($code, $type) {
        $count = 0;
        if ($type == 'count') {
            /* COUNT FROM AUTO SHIPMENT */
            $count = Autoshipment::where('couponcodeApplied', "$code")->where('deleted', '0')->where('paymentStatus', 'paid')->count();
        } else if ($type == 'total') {
            /* TOTAL FROM SHIPMENT */
            $count = Autoshipment::where('couponcodeApplied', "$code")->where('deleted', '0')->where('paymentStatus', 'paid')->sum('totalCost');
        } else {
            /* DISCOUNT FROM SHIPMENT */
            $count = Autoshipment::where('couponcodeApplied', "$code")->where('deleted', '0')->where('paymentStatus', 'paid')->sum('discountAmount');
        }

        return $count;
    }

    /**
     * Method used to count the offers code are being used for auto
     * @param integer $code
     * @param string $type
     * @return integer
     */
    public static function countCouponUsedFillShip($code, $type) {
        $count = 0;
        if ($type == 'count') {
            /* COUNT FROM FillSHIP SHIPMENT */
            $count = Fillship::where('couponcodeApplied', "$code")->where('deleted', '0')->where('paymentStatus', 'paid')->where('shippingMethod', 'Y')->count();
        } else if ($type == 'total') {
            /* TOTAL FROM SHIPMENT */
            $count = Fillship::where('couponcodeApplied', "$code")->where('deleted', '0')->where('paymentStatus', 'paid')->where('shippingMethod', 'Y')->sum('totalCost');
        } else {
            /* DISCOUNT FROM SHIPMENT */
            $count = Fillship::where('couponcodeApplied', "$code")->where('deleted', '0')->where('paymentStatus', 'paid')->where('shippingMethod', 'Y')->sum('totalDiscount');
        }

        return $count;
    }

    /**
     * Method used to count the offers code are being used for auto
     * @param integer $code
     * @param string $type
     * @return integer
     */
    public static function countCouponUsedProc($code, $type) {
        $count = 0;
        if ($type == 'count') {
            /* COUNT FROM PROCUREMENT SHIPMENT */
            $count = Procurement::where('couponcodeApplied', "$code")->where('deleted', '0')->where('paymentStatus', 'paid')->where('shippingMethod', 'Y')->count();
        } else if ($type == 'total') {
            /* TOTAL FROM SHIPMENT */
            $count = Procurement::where('couponcodeApplied', "$code")->where('deleted', '0')->where('paymentStatus', 'paid')->where('shippingMethod', 'Y')->sum('totalCost');
        } else {
            /* DISCOUNT FROM SHIPMENT */
            $count = Procurement::where('couponcodeApplied', "$code")->where('deleted', '0')->where('paymentStatus', 'paid')->where('shippingMethod', 'Y')->sum('totalDiscount');
        }

        return $count;
    }

    /**
     * Method used to get the content based on coupon code applied
     * @param string $type
     * @return integer
     */
    public static function getShipmentByCoupon($type, $code, $param) {

        $row = false;

        $row = Shipment::where('couponcodeApplied', "$code")
                ->orderBy($param['field'], $param['type'])
                ->paginate($param['searchDisplay']);

        return $row;
    }

}
