<?php

namespace App\libraries;

use Illuminate\Support\Facades\DB;
use App\libraries\helpers;
use Illuminate\Database\Eloquent\Model;

Class dbHelpers {

    /**
     * Function used For get table field value
     * @param string $table
     * @param string $col
     * @param string $where
     * @param mix $value
     * @param string $type
     * @return array
     */
    public static function getNameTable($table, $col, $where, $type = '') {

        if (empty($table) || empty($col))
            return false;

        $clause = '';
        $resultSet = array();
        $data = array();
        if (empty($type))
            $type = 'TEXTAREA';

        if (!empty($where)) {
            $clause = " WHERE " . $where;
        }

        $sql = "SELECT " . $col . " FROM " . $table . $clause;
        $resultSet = DB :: select($sql);

        if (count($resultSet) > 0) {
            $count = 0;
            foreach ($resultSet as $key => $val) {
                foreach ($val as $key => $val) {
                    $data[$count][$key] = helpers::outputEscapeString($val, $type);
                }
                $count++;
            }
        }
        return $data;
    }

    /**
     * Function used to get single data from any table
     * @param string $table
     * @param string $col
     * @param string $where
     * @param string $type
     * @return string
     */
    public static function getSingle($table, $col, $where, $type = '') {

        if (empty($table) || empty($col))
            return false;


        $resultSet = false;
        if (empty($type))
            $type = 'TEXTAREA';

        $resultSet = DB::table($table)->selectRaw($col)->whereRaw($where)->first();
        $resultSet = collect($resultSet)->toArray();

        if (!empty($resultSet) && count($resultSet) > 0) {
            return helpers::outputEscapeFirstArray($resultSet, $type);
        } else
            return false;
    }

    /**
     * Function used to check whether member exists
     * @param string $table
     * @param string $col
     * @param mix $value
     * @param integer $id
     * @return boolean
     */
    public static function isExist($table, $col, $value, $id = '') {

        if (empty($table) || empty($col))
            return false;


        $where = $col . "='" . $value . "'";
        if (!empty($id))
            $where .= " AND id!='" . $id . "'";

        $resultSet = DB::table($table)->selectRaw('COUNT(id) AS CNT')->whereRaw($where)->first();
        if (!empty($resultSet->CNT))
            return true;
        else
            return false;
    }

    public static function generateURL($table, $fieldName, $fieldValue, $pkValue = '') {
        $fieldValue = helpers::url_slug($fieldValue);
        $existRecords = self::isExist($table, $fieldName, $fieldValue, $pkValue);
        if (!empty($existRecords)) {
            for ($i = 1; $i < 100; $i++) {
                $link = $fieldValue . "-" . $i;
                $existRecords = self::isExist($table, $fieldName, $link, $pkValue);
                if (empty($existRecords)) {
                    $fieldValue = $link;
                    break;
                }
            }
        }

        $url = strtolower($fieldValue);

        return $url;
    }

    /**
     * Function used to check whether member exists
     * @param string $table
     * @param string $col
     * @param mix $value
     * @param integer $id
     * @return boolean
     */
    public static function isExistRaw($table, $where, $id = '') {

        if (empty($table))
            return false;

        if (!empty($id))
            $where .= " AND id != '" . $id . "'";

        $resultSet = DB::table($table)->selectRaw('COUNT(id) AS CNT')->whereRaw($where)->first();
        $resultSet = array_values($resultSet);

        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }

    /**
     * Function used to check whether record exists
     * @param string $table
     * @param string $col
     * @param mix $value
     * @param integer $id
     * @return boolean
     */
    public static function isRecordExist($table, $col, $value, $id = '') {

        if (empty($table) || empty($col))
            return false;

        $where = $col . "='" . $value . "'";
        if (!empty($id))
            $where .= " AND id!='" . $id . "'";

        $resultSet = DB::table($table)->selectRaw('id')->whereRaw($where)->first();

        if (!empty($resultSet))
            return $resultSet['id'];
        else
            return "";
    }

    /**
     * Function used to fetch template data
     * @param integer $template_id
     * @return array
     */
    public static function templateData($templateCode) {
        if (empty($templateCode))
            return false;

        $data = array();

        $resultData = DB::table('emailTemplate')->where('templateCode', $templateCode)->first();

        if (!empty($resultData) && COUNT($resultData) > 0) {
            foreach ($resultData as $key => $val) {
                $data[$key] = helpers::outputEscapeString($val, 'TEXTAREA');
            }
        }
        return $data;
    }

    public static function setGlobalSettingsData() {
        $resultData = DB::table('generalSettings')->select(array('siteTitle', 'adminName', 'adminEmail', 'emailSignature', 'metaTitle', 'metaKeywords', 'metaDescription'))->take(1)->get();
        if (!empty($resultData) && count($resultData) > 0) {
            $count = 0;
            foreach ($resultData as $row) {
                foreach ($row as $key => $val) {
                    define(strtoupper($key), helpers::outputEscapeString($val, 'TEXTAREA'));
                }
            }
        }
    }

}
