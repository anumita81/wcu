<?php

namespace App\libraries;


use Image;
Class imageHelpers {

    

    /**
     * Method used to replace UTF-8 character to English Character or URL friendly Character
     * @param string $image 
     * @param string $path
     * @param array $imageSize make resize image 
     * @param string $name
     * @return string 
     */
    public static function imageupload($image,$path,$imageSize=array(),$name=''){		
		if(!empty($image)){			
			if(!empty($name)){
				$filename=$name.'-'.time();	
			}else{
				$filename=time();
			}
			
			$profileImage = $filename.'.'.$image->getClientOriginalExtension();
			$img = Image::make($image->getRealPath());
			$destinationPath = public_path($path);
			if(!empty($imageSize)){
				foreach($imageSize as $size){
						$width=!empty($size['width']) ? $size['width']:'';
						$height=!empty($size['height']) ? $size['height']:'';
						$folder=!empty($size['folder']) ? '/'.$size['folder']:'';

						$img->resize($width, $height, function ($constraint) {
							$constraint->aspectRatio();
						})->save($destinationPath.$folder.'/'.$profileImage);
				}
			}
			$image->move($destinationPath, $profileImage);
			return $profileImage;
		}else{
			return false;
		}
		
	}
}
