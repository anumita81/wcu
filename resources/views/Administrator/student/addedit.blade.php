<script src = "{{ asset('public/administrator/controller-css-js/student.js') }}" ></script>

<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$title}}</h4>
        </div>
        <div class="modal-body"> 
            {{ Form::open(array('url' => 'administrator/student/save/'.$id.'/'.$page, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Center<span class="text-red">*</span></label>
                        <select name="centerCode" id="centerCode" required="" class="customSelect">
                            <option value="">Select</option>
                            @foreach($centerList as $center)
                            <option value="{{isset($studentData->centerCode)?$studentData->centerCode:$center->centerCode}}" {{(!empty($studentData->centerCode) && ($studentData->centerCode==$center->centerCode))?"selected":""}}>{{$center->centerName}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Student Firstname <span class="text-red">*</span></label>
                        <input id="studentFirstName" name="studentFirstName" class="form-control" required value="{{!empty($studentData->studentFirstName)?$studentData->studentFirstName:''}}" placeholder="Enter ..." type="text">   
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="form-group">
                        <label>Student Lastname <span class="text-red">*</span></label>
                        <input id="studentLastName" name="studentLastName" class="form-control" required value="{{!empty($studentData->studentLastName)?$studentData->studentLastName:''}}" placeholder="Enter ..." type="text">   
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Class <span class="text-red">*</span></label>
                        <select name="studentClass" id="studentClass" class="customSelect" onchange="changeClass(this.value)">
                            <option value="">Select</option>
                            <option value="10"{{(!empty($studentData->studentClass) && ($studentData->studentClass=='10'))?"selected":""}} >10</option>
                            <option value="12" {{(!empty($studentData->subjectforClass) && ($studentData->studentClass=='12'))?"selected":""}}>12</option>
                            <option value="o" {{(!empty($studentData->subjectforClass) && ($studentData->studentClass=='o'))?"selected":""}}>others</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Age <span class="text-red">*</span></label>
                        <input id="studentAge" name="studentAge" class="form-control" required value="{{!empty($studentData->studentAge)?$studentData->studentAge:''}}" placeholder="Enter ..." type="text">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Contact Person <span class="text-red">*</span></label>
                        <input id="studentContactPerson" name="studentContactPerson" class="form-control" required value="{{!empty($studentData->studentContactPerson)?$studentData->studentContactPerson:''}}" placeholder="Enter ..." type="text">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Address <span class="text-red">*</span></label>
                        <textarea id="studentAddress" name="studentAddress" class="form-control" required>{{!empty($studentData->studentAddress)?$studentData->studentAddress:''}}</textarea>  
                    </div>
                </div>
               
            </div>
             <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>State</label>
                        <select name="studentState" id="studentState" class="customSelect">
                            <option value="">Select</option>                            
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>City</label>
                         <select name="studentCity" id="studentCity" class="customSelect">
                            <option value="">Select</option>                            
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                 <div class="col-md-6">
                    <div class="form-group">
                        <label>Contact Email <span class="text-red">*</span></label>
                        <input id="studentContactEmail" name="studentContactEmail" class="form-control" required value="{{!empty($studentData->studentContactEmail)?$studentData->studentContactEmail:''}}" placeholder="Enter ..." type="text">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Contact Phone<span class="text-red">*</span></label>
                        <input id="studentContactPhone" name="studentContactPhone" class="form-control" required value="{{!empty($studentData->studentContactPhone)?$studentData->studentContactPhone:''}}" placeholder="Enter ..." type="text">
                    </div>
                </div>
                
            </div>
            <div class="row">
                <div class="col-md-6" id="subjectGroup" style="display: none;">
                    <div class="form-group">
                        <label>Subject Group</label>
                        <select name="subjectGroupId" id="subjectGroupId" class="customSelect">
                            <option value="">Select</option> 
                            @foreach($subjectGroupList as $subjectGroup)
                            <option value="{{isset($studentData->subjectGroupId)?$studentData-> subjectGroupId:$subjectGroup->id}}" {{(!empty($studentData->subjectGroupId) && ($studentData->subjectGroupId==$subjectGroup->id))?"selected":""}}>{{$center->centerName}}</option>
                            @endforeach                           
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Subject</label>
                         <select name="subjectId[]" id="subjectId" class="customSelect" multiple>
                            <option value="">Select</option>  
                            @foreach($subjectList as $subject)
                            <option value="{{isset($studentData->subjectId)?$studentData-> subjectId:$subject->id}}" {{(!empty($studentData->subjectId) && (in_array($subject->id, explode(",", $studentData->subjectId))))?"selected":""}}>{{$subject->subjectName}}</option>
                            @endforeach                           
                        </select>
                    </div>
                </div>
            </div>
           
            <div class="modal-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->