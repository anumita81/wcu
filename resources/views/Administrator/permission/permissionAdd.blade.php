@extends('Administrator.layouts.master')
@section('content')


<section class="content">
                    <div class="row">
                        <section class="col-lg-8 connectedSortable"> 
                            <!-- Custom tabs (Charts with tabs)-->

                            <div class="box"> 
                                <!--<div class="box-header">
                                                      <h3 class="box-title">Hover Data Table</h3>
                                                    </div>--> 
                                <!-- /.box-header -->
                                <!-- <form id="addEditFrm" role="form" method="post" action="admin-roles.html"> -->
                                {{ Form::open(array('url' => 'administrator/permission/'.$userTypeId, 'name' => 'addeditFrm', 'id' => 'addEditFrm', 'method' => 'PUT')) }}
                                    <div class="box-body">
                                        <div class="row m-t-15">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>User Role <span class="text-red"></span></label>
                                                    <input id="company" readonly name="company" class="form-control" placeholder="Enter ..." type="text" value="{{$userTypeName[0]->userTypeName}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    <input type="hidden" name="permissionCount" value="{{$permissionCount}}">
                                    <div class="box-header">
                                        <h3 class="box-title">Permission</h3>
                                    </div>
                                    <div class="box-body">
                                        <table class="table permissionTable">
                                            <thead>
                                                <tr>
                                                    <th>Menus</th>
                                                    <th colspan="4" width="70%">Actions</th>
                                                </tr>
                                                <tr>
                                                    <th width="30%">&nbsp;</th>
                                                    <th>view</th>
                                                    <th>add</th>
                                                    <th>edit</th>
                                                    <th>delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                              {!! $arr !!}
                                                
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    @if($canEdit == 1)
                                    <div class="box-footer text-right">
                                        <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                                    </div>
                                    @endif
                                <!-- </form> -->
                                {{ Form::close() }}
                                <!-- /.box-body --> 
                            </div>

                            <!-- /.nav-tabs-custom --> 
                        </section>
                    </div>
                    <!--Block 01--> 
                </section>  

                <script src="{{ asset('public/administrator/controller-css-js/permission.js') }}"></script>    

@endsection
