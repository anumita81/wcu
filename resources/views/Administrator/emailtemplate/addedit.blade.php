@extends ('Administrator.layouts.master')

@section('content')
<script src="{{ asset('public/administrator/controller-css-js/emailtemplate.js')}}"></script>
<section class="content">

    <div class="row"> 
        <section class="col-lg-8 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <!--<div class="box-header">
                  <h3 class="box-title">Hover Data Table</h3>
                </div>-->
                <!-- /.box-header -->
                <input type="hidden" name="base_path_loadata" value="{{ url('/').'/administrator/emailtemplate/generatekeyoptions' }}">
                {{ Form::open(array('url' => 'administrator/emailtemplate/save/' . $id . '/' . $page, 'name' => 'frmsearch', 'id' => 'addeditFrm', 'method' => 'post', 'novalidate' => 'novalidate')) }}
                <div class="box-body">
                    <div style="{{!empty($id)?'display:none':''}}" class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Template Type</label>
                                <select name="templateType" id="templateType" class="customSelect form-control" required>
                                    <option value="">Select Template Type</option>
                                    <option value="G">General</option>
                                    <option value="O">Order Status</option>
                                    <option value="S">Order Status (single)</option>
                                    <option value="O">Shipment (notify)</option>
                                    <option value="T">Storage Notification (single)</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    @if(!empty($id))
                    <input type="hidden" name="templateType" id="templateType" value="{{$content['templateType']}}" />

                    @if($content['templateKey']!='G')
                    <input type="hidden" name="templateKey" id="templateKey" value="{{$content['templateKey']}}" />
                    @endif

                    @endif

                    @if(empty($id))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Template Key</label>
                                <select name="templateKey" id="templateKey" required class="customSelect form-control">
                                    <option value="">Select Template Key</option>
                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Template Subject</label>
                                <input id="templateSubject" name="templateSubject" class="form-control" placeholder="Enter template subject" type="text" required="" value="{{ Input::old('templateSubject', !empty($content['templateSubject']) ? $content['templateSubject'] : '') }}">
                            </div>
                        </div>
                    </div>

                    <div class="row m-t-15">  
                        <div class="col-md-12">
                            <div class="form-group">
                                <label></label>
                                <a class="pull-right btn btn-default custumButt btnBlue" href="javascript:void(0);" onclick="showAddEdit(0, 2, 'media/media');"><span class="Cicon"><i class="fa fa-image"></i></span> Add Media </a>
                            </div>
                        </div>
                    </div>

                    <div class="row m-t-15">  
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Template Description</label>
                                <textarea id="editor2" name="templateBody" rows="10" cols="80" required="">{{ Input::old('templateBody', !empty($content['templateBody']) ? $content['templateBody'] : '') }}</textarea>
                            </div>
                        </div>
                    </div>


                    <div class="row m-t-15"> 
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>From Name</label>
                                <input id="fromName" name="fromName" class="form-control" placeholder="Enter from name" type="text" required="" value="{{ Input::old('fromName', !empty($content['fromName']) ? $content['fromName'] : '') }}">
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>From Email</label>
                                <input id="fromemail" name="fromemail" class="form-control" placeholder="Enter from email" type="email" required="" value="{{ Input::old('fromemail', !empty($content['fromEmail']) ? $content['fromEmail'] : '') }}">
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Save</button>
                </div>
                {{ Form::close() }} 
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
        <section class="col-lg-4 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <!--<div class="box-header">
                  <h3 class="box-title">Hover Data Table</h3>
                </div>-->
                <!-- /.box-header -->

                <div class="box-body">




                    <div class="row m-t-15"> 
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                @foreach($templateVars as $value)
                                <div class="col-md-4 col-xs-6 templateVars"><a href="javascript:void(0);" onclick="insertAtCaret('editor2', '{{$value}} ');return false;">{{$value}}</a></div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->


                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
</section>

<div class="modal fade" id="modal-addEdit"></div>
<script src="{{ asset('public/global/bower_components/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('public/global/vendors/validator/validator.js')}}"></script>


@endsection

<script>
                                                    $(function () {
                                                    $("#addeditFrm").validate();
                                                    });
</script>