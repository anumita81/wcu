
{{ Form::open(array('url' => 'administrator/adminusers/notify/'.$id."/".$page, 'name' => 'notifyform', 'id' => 'notifyform', 'method' => 'POST')) }}
  
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Send Notification</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Email Address</label>
                        <input type="text" value="{{$adminuser->email}}" required name="notify_email" id="notify_email" class="form-control" readonly />
                        <!-- <input type="hidden" name="notify_userId" id="notify_userId" value="{{$adminuser->id}}"> -->
                    </div>
                </div>
            </div>

            <div class="row m-t-15">  
                <div class="col-md-12">
                    <div class="form-group">
                        <label></label>
                        <a class="pull-right btn btn-default custumButt btnBlue" href="javascript:void(0);" onclick="showAddEdit(0, 1, 'media/media');"><span class="Cicon"><i class="fa fa-image"></i></span> Add Media </a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Message</label>
                        <textarea required name="message" id="editor1" class="form-control "></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="{{url('administrator/notificationhistory/'.$adminuser->id)}}" class="btn btn-warning"><span class="Cicon"><i class="fa fa-history"></i></span>Notification History</a>
            <button class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Send</button>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
{{ Form::close() }}
<!-- /.modal-dialog -->

<!--modal close--> 
<script src="{{ asset('public/administrator/controller-css-js/adminusers.js') }}"></script> 