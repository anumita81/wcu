@extends('Administrator.layouts.master')
@section('content')
<section class="content">

    <div class="row"> 
        <section class="col-lg-8 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <!--<div class="box-header">
                  <h3 class="box-title">Hover Data Table</h3>
                </div>-->
                <!-- /.box-header -->
                <!-- <form id="addEditFrm" role="form" method="post" action="admin-roles.html"> -->
                @if(isset($userAdmin))
                {{ Form::open(array('url' => 'administrator/adminusers/save/'.$id.'/'.$page, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post', 'enctype' => 'multipart/form-data')) }}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Type</label>
                                    <select name="usertype" id="usertype" class="form-control customSelect">
                                         @if($typeAdminUser)
                                         @foreach ($typeAdminUser as $typeAdminUserData)
                                         

                                            <option value="{{$typeAdminUserData->id}}" @if($userAdmin->userType == $typeAdminUserData->id) selected @endif>{{$typeAdminUserData->userTypeName}}</option>
                                         
                                         @endforeach
                                         @endif
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Title</label>
                                    <select name="title" id="title" class="form-control customSelect">
                                        <option value="Mr." @if($userAdmin->title == 'Mr.') selected @endif>Mr.</option>
                                        <option value="Mrs." @if($userAdmin->title == 'Mrs.') selected @endif>Mrs.</option>
                                        <option value="Ms." @if($userAdmin->title == 'Ms.') selected @endif>Ms.</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>First Name <span class="text-red">*</span></label>
                                    <input id="firstName" name="firstName" class="form-control" value="{{$userAdmin->firstName}}" required placeholder="Enter ..." type="text">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Last Name <span class="text-red">*</span></label>
                                    <input id="lastName" name="lastName" class="form-control" value="{{$userAdmin->lastName}}" required placeholder="Enter ..." type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Company <span class="text-red">*</span></label>
                                    <input  id="company" name="company" class="form-control" value="{{$userAdmin->company}}" required placeholder="Enter ..." type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email Address <span class="text-red">*</span></label>
                                    <input  id="email" name="email" readonly style="background-color:#dddddd" class="form-control" value="{{$userAdmin->email}}" required placeholder="Enter ..." type="email">
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Contact No. <span class="text-red">*</span></label>
                                    <input  id="contactNo" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="14" name="contactno" class="form-control" value="{{$userAdmin->contactno}}" required placeholder="Enter ..." type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Warehouse <span class="text-red">*</span></label>
                                    <select multiple="multiple" name="warehouseId[]" id="warehouse" class="form-control" style="width:100%;" required>
                                         @if($warehouseData)
                                         @foreach ($warehouseData as $warehouse)
                                            <option value="{{$warehouse->id}}"   @foreach ($adminUserWarehouseSelectedData as $data) @if($data->id == $warehouse->id) Selected @endif @endforeach>{{$warehouse->name}} ({{ $warehouse->cityName }})</option>
                                        @endforeach
                                         @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                         <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group"> 
                                    <label>Image</label>
                                    <div class="main-img-preview">
                                           @if(!empty($userAdmin->fileName))
                                             <img id="loaded" src="{{asset('/public/uploads/admin/original/'.$userAdmin->fileName)}}" alt="">
                                            @else
                                            <img id="storeIcon" class="thumbnail img-preview" src="{{ asset('public/administrator/img/default-no-img.jpg')}}" title="Preview Logo">
                                             @endif
                                        <span class="closeImg"><i class="fa fa-times-circle"></i></span>
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <div class="fileUpload fixedWidth111 btn btn-custom-theme fake-shadow">
                                                <span>Upload Image</span>
                                                <input id="logo-id" name="fileName" type="file" class="attachment_upload">
                                            </div>
                                        </div>
                                    </div>
                                    <p>(Max. 4MB) </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <a href="{{ url('administrator/adminusers') }}" class="btn btn-primary"><span class="Cicon"><i class="fa fa-arrow-left"></i></span>Back</a>&nbsp;
                        <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                    </div>
                <!-- </form> -->
                {{ Form::close() }}
                @endif

                @if(!isset($userAdmin))
                <div class="box">
                <!--<div class="box-header">
                  <h3 class="box-title">Hover Data Table</h3>
                </div>-->
                <!-- /.box-header -->
                {{ Form::open(array('url' => 'administrator/adminusers/add/'.$page, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Type</label>
                                    <select name="usertype" id="usertype" class="form-control customSelect">
                                         @if($typeAdminUser)
                                         @foreach ($typeAdminUser as $typeAdminUserData)
                                         

                                            <option value="{{$typeAdminUserData->id}}" {{ (Input::old("usertype") == $typeAdminUserData->id ? "selected":"") }}>{{$typeAdminUserData->userTypeName}}</option>
                                         
                                         @endforeach
                                         @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Title</label>
                                    <select name="title" id="title" class="form-control customSelect">
                                        <option value="Mr." {{ (Input::old("title") == 'Mr.' ? "selected":"") }}>Mr.</option>
                                        <option value="Mrs." {{ (Input::old("title") == 'Mrs.' ? "selected":"") }}>Mrs.</option>
                                        <option value="Ms." {{ (Input::old("title") == 'Ms.' ? "selected":"") }}>Ms.</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>First Name <span class="text-red">*</span></label>
                                    <input id="firstName" name="firstName" class="form-control" value="{{ old('firstName') }}" required placeholder="Enter ..." type="text">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Last Name <span class="text-red">*</span></label>
                                    <input id="lastName" name="lastName" class="form-control" value="{{ old('lastName') }}" required placeholder="Enter ..." type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Company <span class="text-red">*</span></label>
                                    <input  id="company" name="company" class="form-control" value="{{ old('company') }}" required placeholder="Enter ..." type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email Address <span class="text-red">*</span></label>
                                    <input  id="email" name="email" class="form-control" value="{{ old('email') }}" required placeholder="Enter ..." type="email">
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Contact No. <span class="text-red">*</span></label>
                                    <input  id="contactNo" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="14" name="contactno" class="form-control" value="{{ old('contactno') }}" required placeholder="Enter ..." type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Warehouse <span class="text-red">*</span></label>
                                    <select multiple="multiple" name="warehouseId[]" id="warehouse" class="form-control" style="width:100%;" required>
                                         @if($warehouseData)
                                         @foreach ($warehouseData as $key => $warehouse)
                                         

                                            <option value="{{$warehouse->id}}" {{ (collect(old('warehouseId'))->contains($warehouse->id)) ? 'selected':'' }}>{{$warehouse->name}} ({{ $warehouse->cityName }})</option>
                                         
                                         @endforeach
                                         @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <a href="{{ url('administrator/adminusers') }}" class="btn btn-primary"><span class="Cicon"><i class="fa fa-arrow-left"></i></span>Back</a>&nbsp;
                        <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                    </div>
                {{ Form::close() }}
                <!-- /.box-body -->
            </div>
            @endif


                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
</section>

<script src="{{ asset('public/administrator/controller-css-js/adminusers.js') }}"></script>
@endsection