@extends('Administrator.layouts.master')
@section('content')
<script src="{{ asset('public/administrator/controller-css-js/generalsettings.js')}}"></script>
 <!-- Main content -->
                <section class="content">

                    <div class="row"> 
                        <section class="col-lg-8"> 
                            <input type="hidden" name="base_path_loadata" value="{{ url('/').'/administrator/generalsettings/getphone' }}">
                            <!-- Custom tabs (Charts with tabs)-->
                            {{ Form::open(array('url' => 'administrator/generalsettings/save', 'name' => 'addEditFrm', 'id' => 'addEditFrm', 'method' => 'post', 'novalidate' => 'novalidate')) }}                  
                                {{ csrf_field() }}                                        
                                   
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">General parameters</h3>
                                    </div>
                                    <!-- /.box-header -->
                                   
                                    <div class="box-body horizontal-Inline"> 
                                        <div class="row">
                                            <div class="col-md-12">
                                            @foreach ($settings as $data)
                                              @if($data['subGroupName'] == 'General Parameters')
                                              <input type="hidden" name="subgroupName1" value="{{$data['subGroupName']}}">
                                                <div class="form-group">
                                                    @if($data['settingsTitle'] != 'countryCode') <label class="control-label col-sm-6">{{str_replace('[CURRENCY]',$defaultSymbol,$data['settingsTitle'])}} @endif
                                                      @if($data['settingsKey'] == 'weight_symbol')
                                                       <span data-toggle="tooltip" title="Unit of weight" class="toolTip"><i class="fa fa-question-circle"></i></span>
                                                     @elseif($data['settingsKey'] == 'weight_value')
                                                        <span data-toggle="tooltip" title="Number of pounds/kilograms in the unit of weight defined by the weight symbol" class="toolTip"><i class="fa fa-question-circle"></i></span>
                                                     @elseif($data['settingsKey'] == 'length_symbol')
                                                        <span data-toggle="tooltip" title="Unit of length" class="toolTip"><i class="fa fa-question-circle"></i></span>
                                                     @elseif($data['settingsKey'] == 'countryCode')
                                                     <span data-toggle="tooltip" style="display:none;"></span>
                                                      @elseif($data['settingsKey'] == 'contact_number')
                                                        <span data-toggle="tooltip" title="Enter more than one contact number to contact Shoptomydoor" class="toolTip"><i class="fa fa-question-circle"></i></span>
                                                      @elseif($data['settingsKey'] == 'contact_email')
                                                        <span data-toggle="tooltip" title="Enter email id to contact Shoptomydoor" class="toolTip"><i class="fa fa-question-circle"></i></span>
                                                     @elseif($data['settingsKey'] == 'point_expiry_period')
                                                     <span data-toggle="tooltip" title="Expiry period (in days) for fund points" class="toolTip"><i class="fa fa-question-circle"></i></span>
                                                     @elseif($data['settingsKey'] == 'perpoint_cost')
                                                     <span data-toggle="tooltip" title="Per points to be credited with amount spendings" class="toolTip"><i class="fa fa-question-circle"></i></span>
                                                     @else
                                                        <span data-toggle="tooltip" title="Number of inches/meters in the unit of length defined by the unit of length symbol" class="toolTip"><i class="fa fa-question-circle"></i></span>
                                                     @endif
                                                    </label>
                                                    @if($data['settingsKey'] == 'weight_symbol')
                                                    <input type="hidden" name="{{$data['settingsKey']}}[]" value="{{$data['settingsValue']}}">
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <select id="volumeCharge" name="{{$data['settingsKey']}}[]" type="text" class="form-control customSelect" required>
                                                                <option value=""></option>
                                                                <option value="lbs" {{$data['settingsValue']=="lbs"? 'selected="selected"':''}}>lbs</option>
                                                                <option value="kgs" {{$data['settingsValue']=="kgs"? 'selected="selected"':''}}>Kgs</option>
                                                            </select>

                                                        </div>
                                                    </div>
                                                    @elseif($data['settingsKey'] == 'weight_value')
                                                    <input type="hidden" name="{{$data['settingsKey']}}[]" value="{{$data['settingsValue']}}">
                                                       <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <input id="volumeCharge" name="{{$data['settingsKey']}}[]" type="text" class="form-control"  placeholder="Enter weight value" value="{{$data['settingsValue']}}" onkeypress="return isNumberKey(event, this);" required>

                                                        </div>

                                                    </div>
                                                    @elseif($data['settingsKey'] == 'length_symbol')
                                                    <input type="hidden" name="{{$data['settingsKey']}}[]" value="{{$data['settingsValue']}}">
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <select id="volumeCharge" name="{{$data['settingsKey']}}[]" type="text" class="form-control customSelect" required>
                                                                <option value="inches" {{$data['settingsValue']=="inches"? 'selected="selected"':''}}>Inches</option>
                                                                <option value="meter" {{$data['settingsValue']=="meter"? 'selected="selected"':''}}>Meter</option>
                                                            </select>

                                                        </div>
                                                    </div>

                                                    @elseif($data['settingsKey'] == 'countryCode')
                                                    <input type="hidden" name="{{$data['settingsKey']}}[]" value="">

                                                    
                                                    @elseif($data['settingsKey'] == 'point_expiry_period')
                                                    <input type="hidden" name="{{$data['settingsKey']}}[]" value="{{$data['settingsValue']}}">
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <input id="volumeCharge" name="{{$data['settingsKey']}}[]" type="text" class="form-control"  placeholder="Enter integer value" value="{{$data['settingsValue']}}" onkeypress="return isNumberKey(event);" required>

                                                        </div>
                                                    </div>


                                                    @elseif($data['settingsKey'] == 'contact_number')
                                                    <input type="hidden" name="{{$data['settingsKey']}}[]" value="{{$data['settingsValue']}}">
                                                       <div class="col-sm-4">
                                                           <div class="input-group">
                                                            @php
                                                            
                                                                $valContacts[] = json_decode($data['settingsValue']);
                                                                
                                                                foreach($valContacts[0] as $keyContact => $valContact){
                                                                $explodeString = explode("^" , $valContact);
                                                                $deleteLink = url('administrator/generalsettings/deletecontact/' . $valContact);
                                                            @endphp
                                                            <div class="clearfix " style="clear:both; margin-bottom:15px;">
                                                            <div class="form-inline">
                                                                <div class="form-group">
                                                                    <select class="form-control customSelect2 fixedwidth120" id="countryCode" disabled="">
                                                                    @foreach ($getPhones as $phone)
                                                                        <option value="{{$phone->code}}" {{$explodeString[0] == $phone->code? 'selected="selected"':''}}>{{$phone->name}}</option>
                                                                    @endforeach
                                                                    </select>
                                                                    @foreach ($getPhones as $phone)
                                                                        @if($explodeString[0] == $phone->code)
                                                                        <input type="hidden" name="countryCode[]" value="{{$phone->code}}" />
                                                                        @endif
                                                                    @endforeach
                                                                    
                                                            </div>                                                      
                                                           <div class="form-group"><input d="volumeCharge" name="{{$data['settingsKey']}}[]" type="text" class="form-control fixedwidth110"  placeholder="Enter phone number"  value="{{$explodeString[1]}}" required></div>
                                                           <!--<div class="form-group"><a class="actionIcons color-theme-2 no-mrg" href="{{$deleteLink}}" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a></div>-->
                                                           </div>
                                                            </div>
                                                            @php
                                                            }
                                                            @endphp
<!--                                                            <p id="buildyourform">
                                                                    
                                                            </p>
                                                            <button type="button" id="add" class="btn btn-info addMoreButton"><span class="Cicon"><i class="fa fa-plus"></i></span>Add more contact</button>-->
                                                        </div>

                                                    </div>
                                                    @else 
                                                    <input type="hidden" name="{{$data['settingsKey']}}[]" value="{{$data['settingsValue']}}">
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <input id="volumeCharge" name="{{$data['settingsKey']}}[]" type="text" class="form-control"  placeholder="Enter length value" value="{{$data['settingsValue']}}" onkeypress="return isNumberKey(event);" required>
                                                        </div>
                                                    </div>
                                                   @endif 
                                                </div>
                                                 @endif
                                              @endforeach
                                            </div>
                                        </div>

                                        <!-- /.box-body -->

                                        <div class="box-header" style="display:none;">
                                            <h3 class="box-title">Payment options</h3>
                                        </div>
                                        <!-- /.box-header -->

                                        <div class="box-body" style="display:none;">                                        
                                            <div class="row">
                                                <div class="col-md-12">
                                                @foreach ($settings as $data)
                                                  @if($data['subGroupName'] == 'Payment options')
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-6">{{$data['settingsTitle']}}
                                                        @if($data['settingsKey'] == 'cc_info')
                                                          <span data-toggle="tooltip" title="Enable 'Issue Number' and 'Valid from' fields in the CC info form" class="toolTip"><i class="fa fa-question-circle"></i></span>
                                                         @elseif($data['settingsKey'] == 'dispaly_cvv2')
                                                          <span data-toggle="tooltip" title="Display CVV2 input box on the registration form and at the last stage of checkout if Manual CC processing is used (this also requires $store_cvv2=true, see manual for details)" class="toolTip"><i class="fa fa-question-circle"></i></span>
                                                         @elseif($data['settingsKey'] == 'auth_period')
                                                          <span data-toggle="tooltip" title="Number of days in advance that you want a reminder to be sent to the Orders department email address about an order in Pre-authorized status for which the authorization period is going to expire" class="toolTip"><i class="fa fa-question-circle"></i></span>
                                                         @endif
                                                         </label>

                                                         @if($data['settingsKey'] == 'cc_info')
                                                         <input type="hidden" name="{{$data['settingsKey']}}[]" value="{{$data['settingsValue']}}">
                                                        <div class="col-sm-4 withCheck">
                                                            <div class="input-group">
                                                                <label><input id="ccInfo" name="{{$data['settingsKey']}}[]" type="checkbox" class="flat-red" value="0" {{$data['settingsValue']=="1"? 'checked="checked"':''}}></label>
                                                            </div>
                                                        </div>
                                                        @elseif($data['settingsKey'] == 'dispaly_cvv2')
                                                        <input type="hidden" name="{{$data['settingsKey']}}[]" value="{{$data['settingsValue']}}">
                                                        <div class="col-sm-4 withCheck">
                                                            <div class="input-group">
                                                                <label><input id="dispalCVV" name="{{$data['settingsKey']}}[]" type="checkbox" class="flat-red" value="0" {{$data['settingsValue']=="1"? 'checked="checked"':''}}></label>

                                                            </div>
                                                        </div>
                                                        @elseif($data['settingsKey'] == 'auth_period')
                                                        <input type="hidden" name="{{$data['settingsKey']}}[]" value="{{$data['settingsValue']}}">
                                                         <div class="col-sm-4">

                                                            <div class="input-group">
                                                                <input id="authPeriod" type="text" name="{{$data['settingsKey']}}[]" class="form-control"  placeholder="Enter authorization period"  value="{{$data['settingsValue']}}" onkeypress="return isNumberKey(event, this);" required>

                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>                                            

                                                    @endif
                                              @endforeach                                            
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.box-body -->
                                        
                                         <div class="box-header">
                                            <h3 class="box-title">Referral Settings</h3>
                                        </div>
                                        <div class="box-body">  
                                           <div class="row">
                                               <div class="col-md-12">
                                                  @foreach ($settings as $data)
                                                   @if($data['subGroupName'] == 'Referral Settings')
                                                   
                                                    @if($data['settingsKey'] == 'refferal_pont_type')
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-6">{{$data['settingsTitle']}}<span data-toggle="tooltip" title="Absolute: Exact value of point to get per transaction Per Dollar: Point to get with each given dollar woth every transaction" class="toolTip"><i class="fa fa-question-circle"></i></span></label>
                                                         
                                                        <input type="hidden" name="subgroupName" value="{{$data['subGroupName']}}">
                                                         <input type="hidden" name="{{$data['settingsKey']}}[]" value="{{$data['settingsValue']}}">
                                                        <div class="col-sm-4 withCheck">
                                                            <div class="input-group">
                                                                <label>Absolute <input id="absolute" name="{{$data['settingsKey']}}[]" type="radio" class="flat-red" value="0" checked="{{$checked}}" ></label>
                                                                &nbsp; &nbsp;<label>Per Dollar <input id="perdollar" name="{{$data['settingsKey']}}[]" type="radio" class="flat-red" checked="{{$checked}}"></label>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group" style="display: block;" id="absolute_div">
                                                        <label class="control-label col-sm-6">Absolute Value<span data-toggle="tooltip" title="Absolute value of points will be added" class="toolTip"><i class="fa fa-question-circle"></i></span></label>                                                        
                                                         
                                                        <div class="col-sm-4 withCheck">
                                                            <div class="input-group">
                                                                 <input type="hidden" name="absolute_value[]" value="{{$absolute_value}}">
                                                                <input id="volumeCharge" name="absolute_value[]" type="text" class="form-control"  placeholder="Enter Points" value="{{$absolute_value}}" onkeypress="return isNumberKey(event);" required> 
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                     <div class="form-group" style="display: none;" id="perdollar_div">
                                                        <label class="control-label col-sm-6">Value Per Dollar<span data-toggle="tooltip" title="Value of points wiil be added with each amount entered" class="toolTip"><i class="fa fa-question-circle"></i></span></label>                                                        
                                                         
                                                        <div class="col-sm-4 withCheck">
                                                            <div class="form-inline">
                                                                <div class="form-group">
                                                                    <label>({{$currencySymbol}})</label>
                                                                    <input type="hidden" name="perdollarVal[]" value="{{$perdollarVal}}">
                                                                    <input id="volumeCharge" name="perdollarVal[]" type="text" class="form-control fixedwidth120"  placeholder="Enter Amount" value="{{$perdollarVal}}" onkeypress="return isNumberKey(event);" required>
                                                                    
                                                                </div>
                                                                 <div class="form-group">
                                                                      <input type="hidden" name="pointVal[]" value="{{$pointVal}}">
                                                                    <input id="volumeCharge" name="pointVal[]" type="text" class="form-control fixedwidth120"  placeholder="Enter Points" value="{{$pointVal}}" onkeypress="return isNumberKey(event);" required> 
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                     <div class="form-group">
                                                        <label class="control-label col-sm-6">Maximum Points<span data-toggle="tooltip" title="Maximum value of points can be added" class="toolTip"><i class="fa fa-question-circle"></i></span></label>                                                        
                                                         
                                                        <div class="col-sm-4 withCheck">
                                                            <div class="input-group">
                                                                <input type="hidden" name="maxpoints_value[]" value="{{$maxpoints_value}}">
                                                                <input id="volumeCharge" name="maxpoints_value[]" type="text" class="form-control"  placeholder="Enter Maximum Points" value="{{$maxpoints_value}}" onkeypress="return isNumberKey(event);" required> 
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @endif
                                                   @endforeach  
                                               </div> 
                                           </div>
                                        </div>
                                        
                                        <div class="box-footer text-right">
                                            <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                               </div>

                                
                           {{Form:: close()}}
                        </section>
                    </div>
                    <!--Block 01-->
                </section>
                <!-- /.content --> 
<script>
    $(function () {
        $("#addEditFrm").validate();
        
        $("#absolute").on("ifChecked" , function(){
            $('#absolute').val(0);
            $("#absolute_div").show();
            $("#perdollar_div").hide();
        });
        $("#perdollar").on("ifChecked" , function(){
             $('#perdollar').val(1);
            $("#absolute_div").hide();
            $("#perdollar_div").show();
        })
    });
</script>
 
@endsection