@extends('Administrator.layouts.master')
@section('content')

<script src = "{{ asset('public/administrator/controller-css-js/user.js') }}" ></script>

<!-- Main content -->
<section class="content">

    <div class="row"> 
        <section class="col-lg-8 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <!--<div class="box-header">
                  <h3 class="box-title">Hover Data Table</h3>
                </div>-->
                <!-- /.box-header -->
                {{ Form::open(array('url' => 'administrator/users/save/'.$id.'/'.$page, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Title</label>
                                <select name="title" id="title" class="form-control customSelect">
                                    <option value="Mr.">Mr.</option>
                                    <option value="Mrs.">Mrs.</option>
                                    <option value="Ms.">Ms.</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>First Name <span class="text-red">*</span></label>
                                <input id="firstName" name="firstName" class="form-control" required="" placeholder="Enter First Name" type="text">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Last Name <span class="text-red">*</span></label>
                                <input id="lastName" name="lastName" class="form-control" required="" placeholder="Enter Last Name" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-15">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email Address <span class="text-red">*</span></label>
                                <input  id="email" name="email" class="form-control" required="" placeholder="Enter Email" type="email">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contact No. <span class="text-red">*</span></label>
                                <input  id="contactNumber" name="contactNumber" class="form-control" required="" placeholder="Enter Contact No." type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-15">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Password <span class="text-red">*</span></label>
                                <input  id="password" name="password" class="form-control" required="" minlength="5" placeholder="Enter Password" type="password">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Confirm Password <span class="text-red">*</span></label>
                                <input  id="confirmpassword" name="conifrm_password" class="form-control" required="" minlength="5" equalTo="#password" placeholder="Confirm Password" type="password">
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-15">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Company <span class="text-red">*</span></label>
                                <input  id="company" name="company" class="form-control" required="" placeholder="Enter Company" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>

                <div class="box-footer text-right">
                    Password must contains minimum 8 characters, atleast one digit, one lowercase letter, one uppercase letter
                </div>


                {{ Form::close() }}
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
</section>
<!-- /.content --> 
@endsection