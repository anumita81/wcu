<script src = "{{ asset('public/administrator/controller-css-js/user.js') }}" ></script>
<script>
    $(function () {
        
        $('#addresscountryId').on('change', function () {
            var countryId = $(this).val();
            var url = baseUrl + "/country/getcountryisocode/" + countryId;
            $.getJSON(url, function (data) {
                if(data.countryCode!='' && data.isdCode!='')
                {
                    var countryIsoCode = data.countryCode;
                    var getClass = $('#phone').parent().find('.iti-flag').attr('class');
                    var classParts = getClass.split(" ");
                    $('#phone').parent().find('.iti-flag').removeClass(classParts[1]);
                    $('#phone').parent().find('.iti-flag').addClass(countryIsoCode.toLowerCase());
                    $('#phone').parent().find('.selected-dial-code').html('+'+data.isdCode);
                    $('#phone').parent().find('#country-listbox').hide();
                    $('#phone').parent().find('.iti-arrow').hide();
                    $('#phone').parent().find('.selected-dial-code').css('width', 'auto');
                    $('#phone').parent().find('.selected-dial-code').css('right', 'auto');
                }
                
                setPhoneIsdCode();
            });
        });
        
        $("#phone").intlTelInput({
            separateDialCode: true,
            hiddenInput: ''
        });
        $("#alternatePhone").intlTelInput({
            separateDialCode: true,
            hiddenInput: ''
        });
        setPhoneIsdCode();
        setAltphoneIsdCode();

        var input = document.querySelector("#phone");
        input.addEventListener("countrychange", function(e) {
            setPhoneIsdCode();
        });
        
        var altinput = document.querySelector("#alternatePhone");
        altinput.addEventListener("countrychange", function(e) {
            setAltphoneIsdCode();
        });
    });
    
    function setPhoneIsdCode() {
        var contryData = $('#phone').parent().find('.selected-dial-code').html();
        
        $("#isdCode").val(contryData);
        console.log(contryData);
    }
    
    function setAltphoneIsdCode() {
        var contryData = $('#alternatePhone').parent().find('.selected-dial-code').html();
        
        $("#altIsdCode").val(contryData);
        console.log(contryData);
    }
</script>

<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        <div class="modal-body"> 
            {{ Form::open(array('url' => 'administrator/users/saveaddress/'.$userId. '/'. $id.'/'.$page, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Title</label>
                        <select name="title" id="title" class="form-control customSelect">
                            <option {{!empty($addressBook->title) && $addressBook->title=='Mr.'?'selected':''}} value="Mr.">Mr.</option>
                            <option {{!empty($addressBook->title) &&$addressBook->title=='Mrs.'?'selected':''}} value="Mrs.">Mrs.</option>
                            <option {{!empty($addressBook->title) && $addressBook->title=='Ms.'?'selected':''}} value="Ms.">Ms.</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>First Name <span class="text-red">*</span></label>
                        <input id="firstName" name="firstName" required="" class="form-control" value="{{!empty($addressBook->firstName)?$addressBook->firstName:''}}" placeholder="Enter First Name" type="text">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Last Name <span class="text-red">*</span></label>
                        <input id="lastName" name="lastName" required="" class="form-control" value="{{!empty($addressBook->lastName)?$addressBook->lastName:''}}" placeholder="Enter Last Name" type="text">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Email <span class="text-red">*</span></label>
                        <input id="email" name="email" required="" class="form-control" value="{{!empty($addressBook->email)?$addressBook->email:''}}"  placeholder="Enter Email" type="text">
                    </div>
                </div>
            </div>           
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Address <span class="text-red">*</span></label>
                        <input id="address" name="address" required="" class="form-control" value="{{!empty($addressBook->address)?$addressBook->address:''}}" placeholder="Enter Address" type="text">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Address (line 2)</label>
                        <input id="alternateAddress" name="alternateAddress" class="form-control" value="{{!empty($addressBook->alternateAddress)?$addressBook->alternateAddress:''}}" placeholder="Enter Address" type="text">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Country <span class="text-red">*</span></label>
                        <select name="countryId" id="addresscountryId" required="" class="customSelect">
                            <option value="">Select</option>
                            @foreach($countryList as $country)
                            <option {{(!empty($addressBook->countryId) && $addressBook->countryId == $country['id'])?'selected':''}} value="{{!empty($country['id'])?$country['id']:''}}">{{$country['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>State</label>
                        <select  name="stateId" id="addressstateId" class="customSelect">
                            <option value="">Select</option>
                            @if(isset($stateList) && !empty($stateList))
                            @foreach($stateList as $state)
                            <option {{!empty($addressBook->stateId) && $addressBook->stateId == $state['id']?'selected':''}} value="{{$state['id']}}">{{$state['name']}}</option>
                            @endforeach
                            @endif
                        </select>                   
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>City</label>
                        <select  name="cityId" id="addresscityId" class="customSelect">
                            <option value="">Select</option>
                            @if(isset($cityList) && !empty($cityList))
                            @foreach($cityList as $city)
                            <option {{!empty($addressBook->cityId) &&  $addressBook->cityId == $city['id']?'selected':''}} value="{{$city['id']}}">{{$city['name']}}</option>
                            @endforeach
                            @endif
                        </select>                   
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Zip/Postal code</label>
                        <input id="zipcode" name="zipcode" class="form-control" value="{{!empty($addressBook->zipcode)?$addressBook->zipcode:''}}" placeholder="Enter Zipcode" type="text">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Phone <span class="text-red">*</span></label>
                        <input id="phone" name="phone" class="form-control" required value="{{!empty($addressBook->phone)?$addressBook->phone:''}}" placeholder="Enter Phone" type="text">
                        <input type="hidden" name="isdCode" id="isdCode">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Alternate Phone</label>
                        <input id="alternatePhone" name="alternatePhone" class="form-control" value="{{!empty($addressBook->alternatePhone)?$addressBook->alternatePhone:''}}" placeholder="Enter Alternate Phone" type="text">
                        <input type="hidden" name="altIsdCode" id="altIsdCode">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->

<script>
$(function () {
    $("#addeditFrm").validate();
});
</script>