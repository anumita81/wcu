@extends('Administrator.layouts.master')
@section('content')

<script src = "{{ asset('public/administrator/controller-css-js/user.js') }}" ></script>

<!-- Main content -->
<section class="content"> 
    <div class="row m-b-15">
        <div class="alert alert-success alert-dismissible showmsg" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Success!</h4>                            
            <p>Activation link sent successfully.</p>
        </div>
        <div class="alert alert-danger alert-dismissible errorsg" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Error!</h4>                            
            <p></p>
        </div>
        <div class="col-lg-9 col-md-9">
            {{ Form::open(array('url' => 'administrator/users/index', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchData['searchDisplay']=='10'?'selected':''}} value="10">10</option>
                    <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                    <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                    <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                    <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                    <option {{$searchData['searchDisplay']=='100'?'selected':''}} value="100">100</option>
                </select>
            </div>
            <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
            <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
            {{ Form::close() }}
        </div>
        <div class="col-lg-3 col-md-9 text-right">     
            <div class="btn-group actionGroup">
                <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Actions <span class="CiconR"><i class="fa fa-caret-down"></i></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="javascript:void(0);" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#modal-export-selected">Export Selected</a> </li>
                    <li><a href="javascript:void(0);" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#modal-export">Export All</a> </li>
                    <li role="separator" class="divider"></li>
                    @if($canAdd == 1)<li><a href="{{url('administrator/users/addedit/0/'.$page)}}">Add New User</a></li>@endif
                </ul>
            </div>
            <a class="accordion-toggle btn-sm btn btn-success" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="Cicon"> <i class="fa fa-plus"></i></span> Advanced Search </a> 
        </div>
    </div>     
    <div class="col-md-12 m-t-15">
        <div class="row">
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="box">
                    <div class="box-body">
                        {{ Form::open(array('url' => 'administrator/users/index/', 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
                        <div class="form-row">
                            <div class="col-md-12">
                                <label>Date Period</label>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6"> 
                                <div class="withRdaioButtons" style="padding:15px 0 0 0;"> 
                                    <label>
                                        <input  {{$searchData['searchByCreatedOn']=='thismonth'?'checked':''}} type="radio" name="searchByCreatedOn" value="thismonth" class="flat-red" onclick="show1();">
                                        <span class="radioSpan">This month</span> </label>
                                    <label>
                                        <input {{$searchData['searchByCreatedOn']=='thisweek'?'checked':''}} type="radio" name="searchByCreatedOn" value="thisweek" class="flat-red" onclick="show1();">
                                        <span class="radioSpan">This week</span> </label>
                                    <label>
                                        <input {{$searchData['searchByCreatedOn']=='today'?'checked':''}} type="radio" name="searchByCreatedOn" value="today" class="flat-red" onclick="show1();">
                                        <span class="radioSpan">Today</span> </label>
                                    <label>
                                        <input {{$searchData['searchByCreatedOn']=='custom'?'checked':''}} type="radio" name="searchByCreatedOn" value="custom" class="flat-red customRange" onclick="show2();">
                                        <span class="radioSpan">Custom</span> </label>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div id="customDaterange" {{$searchData['searchByCreatedOn']=='custom' && !empty($searchData['searchByDate'])?'style=visibility:visible':'style=visibility:hidden'}} class="form-group dateRange">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon input-daterange"> <i class="fa fa-calendar"></i> </div>
                                            <input type="text" class="form-control pull-right" name="searchByDate" value="{{$searchData['searchByDate']}}" id="searchByDate">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="sel1">First Name</label> 
                                <input class="form-control" placeholder="" value="{{$searchData['searchByfirstName']}}" type="text" name="searchByfirstName">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="sel1">Last Name</label> 
                                <input class="form-control" placeholder="" value="{{$searchData['searchBylastName']}}" type="text" name="searchBylastName">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="sel1">Unit</label> 
                                <input class="form-control" placeholder="" type="text" value="{{$searchData['searchByUnit']}}" name="searchByUnit">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="sel1">Email Address</label> 
                                <input class="form-control" placeholder="" type="text" value="{{$searchData['searchByEmail']}}" name="searchByEmail">
                            </div>

                        </div>
                        <div  class="form-row">
                            <div class="form-group col-md-3">
                                <label for="sel1">Company</label> 
                                <input class="form-control" placeholder="" type="text" value="{{$searchData['searchByCompany']}}" name="searchByCompany">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="sel1">Status</label>
                                <select class="form-control customSelect2" id="sel1" name="searchByStatus">
                                    <option  {{$searchData['searchByStatus'] ==''?'selected':''}} value="">All</option>
                                    <option  {{$searchData['searchByStatus'] =='0'?'selected':''}} value="0">Pending</option>
                                    <option  {{$searchData['searchByStatus'] =='2'?'selected':''}} value="2">Awaiting Approval</option>
                                    <option  {{$searchData['searchByStatus'] =='1'?'selected':''}} value="1">Approved</option>
                                    <option  {{$searchData['searchByStatus'] =='3'?'selected':''}} value="3">Declined</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="sel1">Subscription Status</label>
                                <select class="form-control customSelect2" id="sel1" name="searchBySubscriptionStatus">
                                    <option  {{$searchData['searchBySubscriptionStatus'] ==''?'selected':''}} value="">All</option>
                                    <option  {{$searchData['searchBySubscriptionStatus'] =='Y'?'selected':''}} value="Y">Subscribed Users</option>
                                    <option  {{$searchData['searchBySubscriptionStatus'] =='N'?'selected':''}} value="N">Non Subscribed Users</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="sel1">Subscription Duration</label>
                                <select class="form-control customSelect2" id="sel1" name="searchBySubscribedFor">
                                    <option {{$searchData['searchBySubscribedFor'] ==''?'selected':''}} value="">All</option>
                                    @foreach ($subscriptionSettings as $subscription)
                                    <option {{$searchData['searchBySubscribedFor'] =='Y'?'selected':''}} value="{{$subscription->duration}}">{{($subscription->duration>1)? $subscription->duration." Months" : $subscription->duration." Month"}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 text-right">
                                <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-search"></i></span>Search</button>
                                <button type="button" onclick="location.href ='{{url('administrator/users/showall')}}'" class="btn btn-danger"><span class="Cicon"><i class="fa fa-refresh"></i></span>Show All</button>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-body">
                    <table id="example2"  class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="withCheck"><label><input type="checkbox" class="flat-red chk_all"></label></th>
                                <th data-sort="unit" class="{{$sort['unit']['current']}} sortby">#</th>
                                <th data-sort="firstName" class="{{$sort['firstName']['current']}} sortby" >Name</th>
                                <th data-sort="email" class="{{$sort['email']['current']}} sortby" >Email</th>
                                <th data-sort="company" class="{{$sort['company']['current']}} sortby" >Company</th>
                                <th data-sort="createdOn" class="sorting_asc sortby">Registered On</th>
                                <th data-sort="orders" class="{{$sort['orderCount']['current']}} sortby" >Orders</th>
                                <th data-sort="isSubscribed" class="{{$sort['isSubscribed']['current']}} sortby"  width="12%">Subscription Status</th>
                                 <th data-sort="subscribedFor" class="{{$sort['subscribedFor']['current']}} sortby"  width="12%">Subscription Duration</th>
                                <th data-sort="subscribedOn" class="{{$sort['subscribedOn']['current']}} sortby"  width="12%">Subscribed On</th>
                                <th data-sort="expiryDate" class="{{$sort['expiryDate']['current']}} sortby"  width="12%">Expired On</th>
                                <th width="8%">Status</th>
                                <th width="17%">Actions</th>
                            </tr>
                        </thead>
                        <tbody id="checkboxes">
                            @if($userData)
                            @php $listIds = array(); @endphp

                            @foreach ($userData as $user)
                            @php                             
                            $listIds[] = $user->id;
                            $editLink = url('administrator/users/addedit/'.$user->id.'/'.$page); 
                            $changeStatusLink = url('administrator/users/changestatus/'.$user->id.'/'.$page); 
                            $deleteLink = url('administrator/users/delete/'.$user->id.'/'.$page); 

                            $verifyLink = url('administrator/users/verifyuser/'.$user->id.'/'.$page);
                            $subscriptionStatus ="Free User";

                            if($user->status == 1)
                            {
                                $resendMail = 0;
                            }
                            else{
                                $resendMail = 1;
                            }
                            if($user->isSubscribed == 'Y')
                            {
                                $subscribedDate = "Loading...";
                            }else{
                                $subscribedDate = "N/A";
                            }
                            if($user->isSubscribed == 'Y')
                            {
                                $expiryDate = "Loading...";
                            }else{
                                $expiryDate = "N/A";
                            }
                            if($user->isSubscribed == 'Y')
                            {
                                $subscribedFor = "Loading...";  
                                $subscriptionStatus ="Loading...";                             
                                
                            }else{
                                $subscribedFor = 0;
                                $subscriptionStatus ="Free User";
                            }
                            @endphp
                            <tr>
                                <td class="withCheck"><label><input type="checkbox" name="checkboxselected" value="{{$user->id}}" class="flat-red checkbox"></label></td>
                                <td>{{!empty($user->unit)?$user->unit:'N/A'}}</td>
                                <td>{{$user->firstName}} {{$user->lastName}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{!empty($user->company)?$user->company:'N/A'}}</td>
                                <td>{{ \Carbon\Carbon::parse($user->createdOn)->format('m-d-Y H:i')}}</td>
                                <td>N/A</td>
                                <td><span id="subscriptionStatus-{{$user->id}}">{{$subscriptionStatus}}</span></td>
                                <td><span id="subscribedFor-{{$user->id}}">{{$subscribedFor>1 ? $subscribedFor." Months" : $subscribedFor." Month"}}</span></td>
                                <td><span id="subscriptionDate-{{$user->id}}">{{$subscribedDate}}</span></td>
                                <td><span id="expiryDate-{{$user->id}}">{{$expiryDate}}</span></td>
                                <td>
                                    @if($canEdit == 1)
                                    <select class="form-control customSelect2" id="sel1" name="searchByStatus" onchange="changeStatus({{$user->id}}, {{$page}}, this.value)">
                                        <option  {{$user->status =='0'?'selected':''}} value="0">Pending</option>
                                        <option  {{$user->status =='2'?'selected':''}} value="2">Awaiting Approval</option>
                                        <option  {{$user->status =='1'?'selected':''}} value="1">Approved</option>
                                        <option  {{$user->status =='3'?'selected':''}} value="3">Declined</option>
                                    </select>
                                    @endif
                                </td>

                                <td class="posRel">
                                    @if($canEdit == 1)
                                        @if($resendMail == 1)
                                            <a href="javascript:void(0);" class="text-green edit actionIcons" title="Resend Activation Mail" onclick="resendActivationMail({{$user->id}}, {{$page}})"><i class="fa fa-fw fa-retweet"></i></a>
                                        @endif
                                        <a class="text-warning actionIcons" title="Click to Change Password" onclick="showAddEdit({{$user->id}}, {{$page}}, 'users/changepassword');"><i class="fa fa-fw fa-key"></i></a>
                                        <a class="text-info actionIcons" title="Click to Send Notification" onclick="showAddEdit({{$user->id}}, {{$page}}, 'users/sendnotification');"><i class="fa fa-fw fa-envelope-o"></i></a>
                                        <a href="{{$editLink}}" class="text-green edit actionIcons" title="Click to Edit"><i class="fa fa-fw fa-edit"></i></a>
                                        <a href="javascript:void(0)" class="text-info actionIcons" title="User Shipment Settings" onclick="showAddEdit({{$user->id}}, {{$page}}, 'users/getusershipmentsettings');"><i class="fa fa-cog"></i></a>

                                        <a class="actionIcons color-theme-2" href="{{$deleteLink}}"><i title="Click to Delete" class="fa fa-fw fa-trash"></i></a>
                                        @if($user->isSubscribed == 'Y')
                                            <a href="javascript:void(0)" class="text-info actionIcons" title="Assign Account Manager" onclick="showAddEdit({{$user->id}}, {{$page}}, 'users/assignrepresentative');"><i class="fa fa-2 fa-user-circle-o"></i></a>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @else 
                            <tr>
                                <td colspan="6">No Record Found</td>
                            </tr>
                            @endif
                            @php
                                $listIds = implode('^',$listIds);
                                echo '<script type="text/javascript">
                                    $(document).ready(function() {
                                        getSubscriptionDetails("'.$listIds.'");
                                    });
                                </script>';
                                
                            @endphp
                        </tbody>
                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $userData->firstItem() . ' - ' . $userData->lastItem() . ' of  ' . $userData->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $userData->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--modal open-->
    <div class="modal fade" id="modal-addEdit"></div>
    <!--modal close-->
</section>
<!-- /.content -->  

<!--modal open-->
<div class="modal fade" id="modal-export">
    {{ Form::open(array('url' => 'administrator/users/exportall/'.$page.'/', 'name' => 'exportAll', 'id' => 'exportAll', 'method' => 'post')) }}
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> Export User Data </h4>
            </div>
            <div class="modal-body">
                <div id="errorTxt" class="alert alert-danger" style="display:none;">

                </div>
                <h5 style="margin-bottom: 20px;">Choose fields to export</h5>

                <div class="row">                                        
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]"  value="unit" checked="checked" class="flat-red checkbox"><span style="margin-left: 10px;">Unit</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="title" checked="checked" class="flat-red checkbox"><span style="margin-left: 10px;">Title</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="firstName" checked="checked" class="flat-red checkbox"><span style="margin-left: 10px;">First Name</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="lastName" checked="checked" class="flat-red checkbox"><span style="margin-left: 10px;">Last Name</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="email" checked="checked" class="flat-red checkbox"><span style="margin-left: 10px;">Email Address</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="contactNumber" checked="checked" class="flat-red checkbox"><span style="margin-left: 10px;">Contact Number</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="status" checked="checked" class="flat-red checkbox"><span style="margin-left: 10px;">User Status</span></label>
                        </div>
                    </div>
                    <div class="col-md-6">                                        
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="dateOfBirth" checked="checked" class="flat-red checkbox"><span style="margin-left: 10px;">Date Of Birth</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="company" checked="checked" class="flat-red checkbox"><span style="margin-left: 10px;">Company</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="createdOn" checked="checked" class="flat-red checkbox"><span style="margin-left: 10px;">Registered On</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="cityName" checked="checked" class="flat-red checkbox"><span style="margin-left: 10px;">City</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="stateName" checked="checked" class="flat-red checkbox"><span style="margin-left: 10px;">State</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="countryName" accept="" class="flat-red checkbox"><span style="margin-left: 10px;">Country</span></label>
                        </div>

                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" onclick="exportall_submit();" class="btn btn-success" data-dismiss="modal">Export</button>
            </div>
        </div>

        <!-- /.modal-content -->
    </div>{
    { Form::close() }}
    <!-- /.modal-dialog -->
</div>
<!--modal close--> 
<!--modal open-->
<div class="modal fade" id="modal-export-selected">
    {{ Form::open(array('url' => 'administrator/users/exportselected/'.$page.'/', 'name' => 'exportAllse', 'id' => 'exportAllse', 'method' => 'post')) }}
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> Export Admin User Data </h4>
            </div>
            <div class="modal-body">
                <div id="errorTxtse" class="alert alert-danger" style="display:none;">

                </div>
                <h5 style="margin-bottom: 20px;">Choose fields to export</h5>

                <div class="row">                                        
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="unit" class="flat-red selecteField"><span style="margin-left: 10px;">Unit</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="title" class="flat-red selecteField"><span style="margin-left: 10px;">Title</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="firstName" class="flat-red selecteField"><span style="margin-left: 10px;">First Name</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="lastName" class="flat-red selecteField"><span style="margin-left: 10px;">Last Name</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="email" class="flat-red selecteField"><span style="margin-left: 10px;">Email Address</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="contactNumber" class="flat-red selecteField"><span style="margin-left: 10px;">Contact Number</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="status" class="flat-red selecteField"><span style="margin-left: 10px;">User Status</span></label>
                        </div>
                    </div>
                    <div class="col-md-6">                                        
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="dateOfBirth" class="flat-red selecteField"><span style="margin-left: 10px;">Date Of Birth</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="company" class="flat-red selecteField"><span style="margin-left: 10px;">Company</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="createdOn" class="flat-red selecteField"><span style="margin-left: 10px;">Registered On</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="cityName" class="flat-red selecteField"><span style="margin-left: 10px;">City</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="stateName" class="flat-red selecteField"><span style="margin-left: 10px;">State</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="countryName" class="flat-red selecteField"><span style="margin-left: 10px;">Country</span></label>
                        </div>

                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" onclick="exportallse_submit();" class="btn btn-success" data-dismiss="modal">Export</button>
            </div>
        </div>

        <!-- /.modal-content -->
    </div>{{ Form::close() }}
    <!-- /.modal-dialog -->
</div>
<!--modal close-->
@endsection