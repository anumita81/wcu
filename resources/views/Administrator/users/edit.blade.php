@extends('Administrator.layouts.master')
@section('content')

<script src = "{{ asset('public/administrator/controller-css-js/user.js') }}" ></script>

<!-- Main content -->
<section class="content">

    <div class="row"> 
        <section class="col-lg-8 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <!--<div class="box-header">
                  <h3 class="box-title">Hover Data Table</h3>
                </div>-->
                <!-- /.box-header -->
                {{ Form::open(array('url' => 'administrator/users/save/'.$id.'/'.$page, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Title</label>
                                <select name="title" id="title" class="form-control customSelect">
                                    <option {{$user->title=='Mr.'?'selected':''}} value="Mr.">Mr.</option>
                                    <option {{$user->title=='Mrs.'?'selected':''}} value="Mrs.">Mrs.</option>
                                    <option {{$user->title=='Ms.'?'selected':''}} value="Ms.">Ms.</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>First Name <span class="text-red">*</span></label>
                                <input id="firstName" name="firstName" class="form-control" required="" value="{{$user->firstName}}"  placeholder="Enter First Name" type="text">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Last Name <span class="text-red">*</span></label>
                                <input id="lastName" name="lastName" class="form-control" required="" value="{{$user->lastName}}"  placeholder="Enter Last Name" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-15">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email Address <span class="text-red">*</span></label>
                                <input  id="email" name="email" class="form-control" readonly="readonly" value="{{$user->email}}" placeholder="Enter Email" type="email">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contact No. <span class="text-red">*</span></label>
                                <input  id="contactNumber" name="contactNumber" class="form-control" required="" value="{{$user->contactNumber}}" placeholder="Enter Contact No." type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-15">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Password</label>
                                <input  id="password" name="password" class="form-control" minlength="8" placeholder="Enter Password" type="password">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Confirm Password </label>
                                <input  id="confirmpassword" name="conifrm_password" class="form-control" minlength="8" equalTo="#password" placeholder="Confirm Password" type="password">
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-15">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Company</label>
                                <input  id="company" name="company" class="form-control" value="{{$user->company}}" placeholder="Enter Company" type="text">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Date Of Birth</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input  id="dateOfBirth" name="dateOfBirth" readonly="" class="form-control datepicker" value="{{!empty($user->dateOfBirth)?$user->dateOfBirth->format('Y-m-d'):''}}" placeholder="Select Date Of Birth" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>

                <div class="box-footer text-right">
                    Password must contains minimum 8 characters
                </div>


                {{ Form::close() }}
                <!-- /.box-body -->


                <div class="box-header">
                    <h3 class="box-title">Address Book</h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <a  onclick="showAddEdit(0, {{$page}}, 'users/addressaddedit/{{$id}}');" class="accordion-toggle btn-sm btn btn-success"><span class="Cicon"> <i class="fa fa-plus"></i></span> Add New Address</a>
                            @if(!empty($addressBook))
                            <a href="javascript:void(0);" onclick="setDefault('shipping')" class="btn btn-sm btn-info"><span class="Cicon"><i class="fa fa-ship"></i></span>Set as Default Shipping Address</a>
                            <a href="javascript:void(0);" onclick="setDefault('billing')" class="btn btn-sm btn-warning"><span class="Cicon"><i class="fa fa-file-text"></i></span>Set as Default  Billing Address</a>
                            @endif
                        </div>
                    </div>
                    <div class="row m-t-15">

                        @if(!empty($addressBook))
                        @foreach($addressBook as $address)
                        @php
                        $deleteLink = url('administrator/users/addressdelete/'.$id.'/'.$address->id.'/'.$page); 
                        @endphp
                        <div class="col-md-4">
                            <div class="addressInfo active">
                                <div class="withCheck">
                                    <label>
                                        <input type="radio" name="addressBookId" id="addressBook{{$address->id}}" value="{{$address->id}}" class="flat-red addressBook">
                                    </label>
                                </div>

                                @if($address->isDefaultShipping == '1')
                                <span data-toggle="tooltip" title="Default Shipping Address" class="addIn">
                                    <i class="fa fa-ship"></i>
                                </span>
                                @endif
                                @if($address->isDefaultBilling == '1')
                                <span data-toggle="tooltip" title="Default Billing Address" class="addIn">
                                    <i class="fa fa-file-text"></i>
                                </span>
                                @endif
                                <h2> Address {{$loop->iteration}}</h2>
                                <p><b>First Name :</b> {{$address->firstName}}<br><b>Last Name :</b> {{$address->lastName}}<br>
                                    <b>Address :</b> {{$address->address}}<br>   
                                    <b>City :</b> {{$address->city->name}}<br>   
                                    <b>Email :</b> {{$address->email}}<br>   
                                    <b>Phone :</b> {{$address->phone}}<br>   
                                    @if($address->alternatePhone)
                                    <b>Alternate Phone :</b> {{$address->alternatePhone}}<br>   
                                    @endif
                                </p>
                                <p class="pull-right">
                                    <a onclick="showAddEdit({{$address->id}}, {{$page}}, 'users/addressaddedit/{{$id}}');" class="text-green"><span data-toggle="tooltip" title="Click to Edit" class="actionIcons"><i class="fa fa-edit"></i></span></a>
                                    @if($address->isDefaultShipping != '1' && $address->isDefaultBilling != '1')
                                    <a href="{{$deleteLink}}" data-toggle="confirmation" class="text-danger"><span class="actionIcons"><i class="fa fa-trash"></i></span></a>
                                    @endif
                                </p><br>   
                            </div>
                        </div>
                        @endforeach
                        {{ Form::open(array('url' => 'administrator/users/saveaddressbook/'.$id.'/'.$page, 'name' => 'saveaddressbook', 'id' => 'saveaddressbook', 'method' => 'post')) }}
                        <input type="hidden" name="id" id="addressBook" class="flat-red">
                        <input type="hidden" name="isDefaultType" id="isDefaultType" value="">
                        {{ Form::close() }}
                        @endif
                    </div>
                </div>

                @if(!$documents->isEmpty())
                <div class="box-header">
                    <h3 class="box-title">My Documents</h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Description</label>

                        </div>
                        <div class="col-sm-4">
                            <label>Document</label>

                        </div>
                        <div class="col-sm-2">
                            <label>Action</label>

                        </div>
                    </div>
                </div>
                @foreach($documents as $doc)
                @php
                $deleteDocLink = url('administrator/users/documentdelete/'.$doc->id); 
                @endphp
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <label>{{$doc->descr}}</label>

                        </div>
                        <div class="col-sm-4">
                            <label><a href={{url('/public/uploads/documents/'.$doc->userId.'/'.$doc->filename)}} download>{{$doc->filename}}</a></label>

                        </div>
                        <div class="col-sm-2">
                            <label><a href="{{$deleteDocLink}}" data-toggle="confirmation" class="text-danger"><span class="actionIcons"><i class="fa fa-trash"></i></span></a></label>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
    <!--modal open-->
    <div class="modal fade" id="modal-addEdit"></div>
    <!--modal close-->
</section>
<!-- /.content --> 
@endsection