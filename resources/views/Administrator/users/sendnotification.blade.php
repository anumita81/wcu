<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        <div class="modal-body">
            {{ Form::open(array('url' => 'administrator/users/sendnotification/'.$id.'/'.$page, 'name' => 'notificationFrm', 'id' => 'notificationFrm', 'method' => 'post')) }}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Email Address</label>
                        <input id="email" name="email" class="form-control" readonly="" required="" value="{{$user->email}}"  type="email">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Message</label>
                        <textarea name="message" id="message" class="form-control"></textarea>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <div class="text-right">
                    <a href="{{url("administrator/users/notificationhistory/".$id."/".$page."/0")}}" class="btn btn-warning"><span class="Cicon"><i class="fa fa-history"></i></span>Notification History</a>
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->



<script>
    $(function () {
        CKEDITOR.replace('message')
        $('.textarea').wysihtml5()

        $("#notificationFrm").validate();
    });
</script>