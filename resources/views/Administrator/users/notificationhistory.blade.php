@extends('Administrator.layouts.master')
@section('content')

<script src = "{{ asset('public/administrator/controller-css-js/user.js') }}" ></script>

<!-- Main content -->
<section class="content"> 
    <div class="row m-b-15">
        <div class="col-lg-4 col-md-4">
            {{ Form::open(array('url' => 'administrator/users/notificationhistory', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchData['searchDisplay']=='10'?'selected':''}} value="10">10</option>
                    <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                    <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                    <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                    <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                </select>
            </div>
            <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
            <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
            {{ Form::close() }}
        </div>
        <div class="col-md-8 text-right"> 
            <a href="{{url('administrator/users/?page='.$userpage)}}" class="btn btn-sm btn-primary"><span class="Cicon"><i class="fa fa-arrow-left"></i></span> Back to Users</a> 
        </div>
    </div>     
    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-body">
                    <table id="example2"  class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th data-sort="sentOn" class="sorting_asc sortby">Sent On</th>
                                <th width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody id="checkboxes">
                            @if($userNotificationData)
                            @foreach ($userNotificationData as $notification)
                            <tr>
                                <td>{{ \Carbon\Carbon::parse($notification->createdOn)->format('m-d-Y H:i')}}</td>
                                <td>
                                    <a class="text-info actionIcons" data-toggle="tooltip" title="Click to View Notification" onclick="showAddEdit({{$notification->id}}, {{$page}}, 'users/viewnotification');"><i class="fa fa-fw fa-eye"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else 
                            <tr>
                                <td colspan="6">No Record Found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $userNotificationData->firstItem() . ' - ' . $userNotificationData->lastItem() . ' of  ' . $userNotificationData->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $userNotificationData->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--modal open-->
    <div class="modal fade" id="modal-addEdit"></div>
    <!--modal close-->
</section>
<!-- /.content -->  

<!--modal open-->
<div class="modal fade" id="modal-export">
    {{ Form::open(array('url' => 'administrator/users/exportall/'.$page.'/', 'name' => 'exportAll', 'id' => 'exportAll', 'method' => 'post')) }}
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> Export User Data </h4>
            </div>
            <div class="modal-body">
                <div id="errorTxt" class="alert alert-danger" style="display:none;">

                </div>
                <h5 style="margin-bottom: 20px;">Choose fields to export</h5>

                <div class="row">                                        
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="uniqueId" class="flat-red checkbox"><span style="margin-left: 10px;">Unit</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="title" class="flat-red checkbox"><span style="margin-left: 10px;">Title</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="firstName" class="flat-red checkbox"><span style="margin-left: 10px;">First Name</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="lastName" class="flat-red checkbox"><span style="margin-left: 10px;">Last Name</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="email" class="flat-red checkbox"><span style="margin-left: 10px;">Email Address</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="contactNumber" class="flat-red checkbox"><span style="margin-left: 10px;">Contact Number</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="status" class="flat-red checkbox"><span style="margin-left: 10px;">User Status</span></label>
                        </div>
                    </div>
                    <div class="col-md-6">                                        
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="dateOfBirth" class="flat-red checkbox"><span style="margin-left: 10px;">Date Of Birth</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="company" class="flat-red checkbox"><span style="margin-left: 10px;">Company</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="createdOn" class="flat-red checkbox"><span style="margin-left: 10px;">Registered On</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="cityName" class="flat-red checkbox"><span style="margin-left: 10px;">City</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="stateName" class="flat-red checkbox"><span style="margin-left: 10px;">State</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="countryName" class="flat-red checkbox"><span style="margin-left: 10px;">Country</span></label>
                        </div>

                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" onclick="exportall_submit();" class="btn btn-success" data-dismiss="modal">Export</button>
            </div>
        </div>

        <!-- /.modal-content -->
    </div>{
    { Form::close() }}
    <!-- /.modal-dialog -->
</div>
<!--modal close--> 





<!--modal open-->
<div class="modal fade" id="modal-export-selected">
    {{ Form::open(array('url' => 'administrator/users/exportselected/'.$page.'/', 'name' => 'exportAllse', 'id' => 'exportAllse', 'method' => 'post')) }}
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> Export Admin User Data </h4>
            </div>
            <div class="modal-body">
                <div id="errorTxtse" class="alert alert-danger" style="display:none;">

                </div>
                <h5 style="margin-bottom: 20px;">Choose fields to export</h5>

                <div class="row">                                        
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="uniqueId" class="flat-red selecteField"><span style="margin-left: 10px;">Unit</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="title" class="flat-red selecteField"><span style="margin-left: 10px;">Title</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="firstName" class="flat-red selecteField"><span style="margin-left: 10px;">First Name</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="lastName" class="flat-red selecteField"><span style="margin-left: 10px;">Last Name</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="email" class="flat-red selecteField"><span style="margin-left: 10px;">Email Address</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="contactNumber" class="flat-red selecteField"><span style="margin-left: 10px;">Contact Number</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="status" class="flat-red selecteField"><span style="margin-left: 10px;">User Status</span></label>
                        </div>
                    </div>
                    <div class="col-md-6">                                        
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="dateOfBirth" class="flat-red selecteField"><span style="margin-left: 10px;">Date Of Birth</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="company" class="flat-red selecteField"><span style="margin-left: 10px;">Company</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="createdOn" class="flat-red selecteField"><span style="margin-left: 10px;">Registered On</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="cityName" class="flat-red selecteField"><span style="margin-left: 10px;">City</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="stateName" class="flat-red selecteField"><span style="margin-left: 10px;">State</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" value="countryName" class="flat-red selecteField"><span style="margin-left: 10px;">Country</span></label>
                        </div>

                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" onclick="exportallse_submit();" class="btn btn-success" data-dismiss="modal">Export</button>
            </div>
        </div>

        <!-- /.modal-content -->
    </div>{{ Form::close() }}
    <!-- /.modal-dialog -->
</div>
<!--modal close-->
@endsection