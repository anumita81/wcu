<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <body>
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
        <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
        <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
        <!--font-family: 'Roboto', sans-serif;-->
        <style type="text/css">
            body {
                width: 100% !important;
                -webkit-text-size-adjust: 100%;
                -ms-text-size-adjust: 100%;
                margin: 0;
                padding: 0;
                font-size: 12px;
                line-height: 1.4;
            }

            table td {
                border-collapse: collapse;
            }

            img {
                display: block;
                border: 0;
                outline: none;
            }

            a {
                outline: none;
                text-decoration: none;
            }
        </style>
        <table cellspacing="0" cellpadding="0" border="0" width="100%" bgcolor="#ffffff">
            <tr>
                <td align="center" valign="top" width="100%" style="padding:0; margin:0;">
                    <table cellspacing="0" cellpadding="0" border="0" class="tableWrap" bgcolor="#ffffff">
                        <tr>
                            <td style="padding:15px; margin:0; font-family: 'Roboto', sans-serif; border-bottom: 1px solid #e5e5e5">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td align="left" valign="top" width="32%" style="padding:0; margin:0;">
                                            @if(isset($invoiceFor) && $invoiceFor=='frontend')
                                            <img src="{{ asset('administrator/img/logoSTMD.jpg') }}" style="padding:0; margin:0;" />
                                            @else
                                            <img src="{{ asset('public/administrator/img/logoSTMD.jpg') }}" style="padding:0; margin:0;" />
                                            @endif
                                        </td>
                                        <td align="center" valign="top" width="36%" style="padding:0; margin:0;">
                                            <span style="display: block; font-size: 24px; text-transform: uppercase"><strong>{{$invoiceType}}</strong></span>
                                            <span style="display: block;">Date: {{ $invoice->createdOn }}</span>
                                            <span style="display: block;">Invoice ID: #{{$invoice->invoiceUniqueId}}</span>
                                        </td>
                                        <td align="right" valign="top" width="32%" style="padding:0; margin:0;">
                                            <span style="display: block"><strong>Subscription Shoptomydoor</strong></span>
                                            <span style="display: block">CALL US: 1-888 315 9027</span>
                                            <span style="display: block">Email: contact@test.com</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td style="padding:15px; margin:0; font-family: 'Roboto', sans-serif; border-bottom: 1px solid #e5e5e5">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td align="left" valign="top" width="20%" style="padding:0; margin:0;">Unit:</td>
                                        <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->userUnit}}</td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="20%" style="padding:0; margin:0;">Name:</td>
                                        <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->userFullName}}</td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="20%" style="padding:0; margin:0;">Email:</td>
                                        <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->userEmail}}</td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="20%" style="padding:0; margin:0;">Contact Number:</td>
                                        <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->userContactNumber}}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td style="padding:15px; margin:0; font-family: 'Roboto', sans-serif; border-bottom: 1px solid #e5e5e5">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td align="left" valign="top" style="padding:0 15px 0 0; margin:0; word-break: break-all">
                                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                <tr>
                                                    <td align="left" valign="top" colspan="2" style="padding:0 0 15px; margin:0; font-weight: bold; font-size: 16px">Billing
                                                        Address</td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Name:</td>
                                                    <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->billingName}}</td>
                                                </tr>

                                                <tr>
                                                    <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Email:</td>
                                                    <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->billingEmail}}</td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Address:</td>
                                                    <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->billingAddress}}</td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" width="40%" style="padding:0; margin:0;">City:</td>
                                                    <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->billingCity}}</td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" width="40%" style="padding:0; margin:0;">State:</td>
                                                    <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->billingState}}</td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Country:</td>
                                                    <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->billingCountry}}</td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Zip/Postal Code:</td>
                                                    <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->billingZipcode}}</td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Contact Number:</td>
                                                    <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->billingPhone}}</td>
                                                </tr>
                                            </table>
                                        </td>
                                        
                                    </tr>
                                </table>
                            </td>
                        </tr>

                      @php
                            $invoiceShipmentCosts = json_decode($invoice->invoiceParticulars)->payment;
                            
                        @endphp
                         <tr>
                            <td align="left" valign="top" style="padding:15px; margin:0; font-family: 'Roboto', sans-serif; font-size: 14px; font-weight: bold;">Details</td>

                        </tr>

                        <tr>
                            <td align="center" valign="top" style="padding:0; margin:0; border-bottom: 1px solid #e5e5e5;">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 12px; font-weight: 700;">Payment Method</td>
                                        <td align="left" valign="top" width="150" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 12px; font-weight: 700;">Subscription Duration</td>
                                        <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 12px; font-weight: 700;">Subscription Charges</td>
                                        <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 12px; font-weight: 700;">Subscribed On</td>
                                        <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 12px; font-weight: 700;">Subscription Expiry</td>
                                        
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" style="word-break: break-all; border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; font-size: 12px;">{{$invoiceShipmentCosts->paymentMethodName}}</td>
                                        <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; font-size: 12px;">{{($invoiceShipmentCosts->duration>1)?$invoiceShipmentCosts->duration.' Months':$invoiceShipmentCosts->duration.' Month'}}</td>
                                        
                                        <td align="left" valign="top" style="word-break: break-all; border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; font-size: 12px;">{{(float)$invoiceShipmentCosts->subscriptionAmount}}</td>
                                        
                                        <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; font-size: 12px;">{{date('M-d-Y', strtotime($invoiceShipmentCosts->subscribedOn))}}</td>
                                        <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; font-size: 12px;">{{date('M-d-Y', strtotime($invoiceShipmentCosts->expiryDate->date))}}</td>
                                        
                                    </tr>
                                </table>
                            </td>
                        </tr>
                     
                        <tr>
                            <td align="center" valign="top" style="padding:15px 0; margin:0; border-bottom: 1px solid #e5e5e5;">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td align="left" valign="top" colspan="3" style="padding:5px 15px; margin:0; font-family: 'Roboto', sans-serif; font-size: 14px; font-weight: bold;">Cost & Payment details</td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" colspan="2" style="padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; text-transform: uppercase; font-weight: 700;">Subscription Cost: </td>
                                        <td align="right" valign="top" style="padding:5px 15px 5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; font-weight: 700;">{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat($invoiceShipmentCosts->subscriptionAmount, $invoiceShipmentCosts->defaultCurrencyCode)}}</td>
                                       
                                        
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" colspan="2" style="padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; text-transform: uppercase; font-weight: 700;">Tax: </td>
                                        <td align="right" valign="top" style="padding:5px 15px 5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; font-weight: 700;">{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat($invoiceShipmentCosts->subscriptionTax, $invoiceShipmentCosts->defaultCurrencyCode)}}</td>
                                       
                                        
                                    </tr>
                                   
                                    <tr>
                                        <td align="left" valign="top" colspan="2" style="padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; text-transform: uppercase; font-weight: 700;">Total: </td>
                                        <td align="right" valign="top" style="padding:5px 15px 5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; font-weight: 700;">{{ (new \App\Helpers\customhelper)->getCurrencySymbolFormat($invoiceShipmentCosts->totalBillingAmount,$invoiceShipmentCosts->defaultCurrencyCode) }}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>

</html>