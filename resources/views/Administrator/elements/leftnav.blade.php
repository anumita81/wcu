<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <div id="menu_content">
        <section class="sidebar"> 
            <ul class="sidebar-menu" data-widget="tree">
                <!--<li class="header">MAIN NAVIGATION</li>-->
                @if(!empty($tree))
                @foreach($tree as $key=> $val)
                
                @if(count($val->children) == 0)
                @if (!empty($leftMenuSelection)) 
                @php if ($leftMenuSelection['menuMain'] == "leftNav".(preg_replace("/[^a-zA-Z]/", "", $tree[$key]->adminMenuName)).$tree[$key]->adminMenuParentId) { $activeClassMain = 'active'; } else { $activeClassMain = ''; } @endphp
                @endif
                @if($val->permissionView == 1 || $val->permissionAdd == 1 || $val->permissionEdit == 1 || $val->permissionDelete == 1)
                    <li class="@if(!empty($activeClassMain)) {{$activeClassMain}} @endif" id="leftNav{{$tree[$key]->adminMenuName}}{{$tree[$key]->adminMenuParentId}}"><a href="{{ url('administrator/'.$tree[$key]->adminMenuSlug) }}"> <i class="fa {{$tree[$key]->adminMenuIcon}}"></i> <span>{{$tree[$key]->adminMenuName}}</span> </a></li>
                @endif
                @endif
                @if(count($tree[$key]->children) > 0)
                @if (!empty($leftMenuSelection)) 
                @php if ($leftMenuSelection['menuMain'] == "leftNav".(preg_replace("/[^a-zA-Z]/", "", $tree[$key]->adminMenuName))) { $activeClassMain = 'active'; } else { $activeClassMain = ''; } @endphp
                @endif
                @if($tree[$key]->permissionView == 1 || $tree[$key]->permissionAdd == 1 || $tree[$key]->permissionEdit == 1 || $tree[$key]->permissionDelete == 1)
                    <li class="treeview @if(!empty($activeClassMain)) {{$activeClassMain}} @endif" id="leftNav{{preg_replace("/[^a-zA-Z]/", "", $tree[$key]->adminMenuName)}}">
                        <a href="#">
                            <i class="fa {{$tree[$key]->adminMenuIcon}}"></i> <span>{{$tree[$key]->adminMenuName}}</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                        @foreach ($tree[$key]->children as $keyLevel1 => $childLevel1)
                        @if (!empty($leftMenuSelection)) 
                        @php if ($leftMenuSelection['menuSub'] == "leftNav".(preg_replace("/[^a-zA-Z]/", "", $childLevel1->adminMenuName)).$childLevel1->adminMenuParentId) { $activeClassSub = 'active'; } else { $activeClassSub = ''; } @endphp
                        @endif
                            @if(count($childLevel1->children) > 0)
                            @if($childLevel1->permissionView == 1 || $childLevel1->permissionAdd == 1 || $childLevel1->permissionEdit == 1 || $childLevel1->permissionDelete == 1)
                            <li class="treeview @if(!empty($activeClassSub)) {{$activeClassSub}} @endif" id="leftNav{{preg_replace("/[^a-zA-Z]/", "", $childLevel1->adminMenuName)}}{{$childLevel1->adminMenuParentId}}">
                                <a href="#"><i class="fa fa-circle-o"></i> <span>{{$childLevel1->adminMenuName}}</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                
                                @if(count($childLevel1->children) > 0)
                                <ul class="treeview-menu">
                                    @foreach ($childLevel1->children as $keyLevel2 => $childLevel2)
                                    @if (!empty($leftMenuSelection)) 
                                    @php if ($leftMenuSelection['menuSubSub'] == "leftNav".(preg_replace("/[^a-zA-Z]/", "", $childLevel2->adminMenuName)).$childLevel2->adminMenuParentId) { $activeClassSubSub1 = 'active'; } else { $activeClassSubSub1 = ''; } @endphp
                                    @endif
                                    @if($childLevel2->permissionView == 1 || $childLevel2->permissionAdd == 1 || $childLevel2->permissionEdit == 1 || $childLevel2->permissionDelete == 1)
                                        <li class="@if(!empty($activeClassSubSub1)) {{$activeClassSubSub1}} @endif" id="leftNav{{preg_replace("/[^a-zA-Z]/", "", $childLevel2->adminMenuName)}}{{$childLevel2->adminMenuParentId}}"><a href="{{ ($childLevel2->adminMenuSlug != '#')?url('administrator/'.$childLevel2->adminMenuSlug):'javascript:void(0);' }}"><i class="fa {{$childLevel2->adminMenuIcon}}"></i> <span>{{$childLevel2->adminMenuName}}</span></a></li>
                                    @endif
                                    @endforeach
                                </ul>
                                @endif
                            </li>
                            @endif 
                            @endif

                                @if(count($childLevel1->children) == 0)
                                @if (!empty($leftMenuSelection)) 
                                @php if ($leftMenuSelection['menuSubSub'] == "leftNav".(preg_replace("/[^a-zA-Z]/", "", $childLevel1->adminMenuName)).$childLevel1->adminMenuParentId) { $activeClassSubSub2 = 'active'; } else { $activeClassSubSub2 = ''; } @endphp
                                @endif
                                @if($childLevel1->permissionView == 1 || $childLevel1->permissionAdd == 1 || $childLevel1->permissionEdit == 1 || $childLevel1->permissionDelete == 1)
                                    <li class="@if(!empty($activeClassSubSub2)) {{$activeClassSubSub2}} @endif" id="leftNav{{preg_replace("/[^a-zA-Z]/", "", $childLevel1->adminMenuName)}}{{$childLevel1->adminMenuParentId}}"><a href="{{ ($childLevel1->adminMenuSlug != '#')?url('administrator/'.$childLevel1->adminMenuSlug):'javascript:void(0);' }}"><i class="fa {{$childLevel1->adminMenuIcon}}"></i> {{$childLevel1->adminMenuName}}</a></li>
                                @endif
                                @endif

                       
                        @endforeach
                        </ul>
                    </li>
                    @endif

                @endif
                @endforeach
                @endif
            </ul>
        </section>
    </div>
    <!-- /.sidebar --> 
</aside>
