@php
$adminUserCreatedOn = (date_parse(session()->get('user_createdOn')));
$monthNum  = $adminUserCreatedOn['month'];
$dateObj   = DateTime::createFromFormat('!m', $monthNum);

@endphp

<script src="{{ asset('public/administrator/controller-css-js/topnav.js') }}"></script> 
<script>
$(function () {

    baseUrl = $('#baseUrl').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "post",
        url: baseUrl + '/dashboard/getshippingnotifications',
        data: {},
        success: function (msg) {
            $(".notifications-menu").html(msg);
        }
    });
    $.ajax({
        type: "post",
        url: baseUrl + '/dashboard/getcontactnotifications',
        data: {},
        success: function (msg) {
            $(".messages-menu").html(msg);
        }
    });
    
});
</script>
<header class="main-header">
    <!-- Logo --> 
    <a href="javascript:void(0);" class="logo"> 
    <!-- mini logo for sidebar mini 50x50 pixels --> 
    <span class="logo-mini"><img src="{{ asset('public/administrator/img/logoSm.jpg') }}" /></span> 
    <!-- logo for regular state and mobile devices --> 
    <span class="logo-lg"><b>Admin</b>LTE</span> </a> 
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top"> 
        <!-- Sidebar toggle button--> 
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button"> <span class="sr-only">Toggle navigation</span> </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu"></li>
                <!-- Notifications: style can be found in dropdown.less -->
                <li class="dropdown notifications-menu"></li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="{{ asset('public/administrator/img/user-icon.png') }}" class="user-image" alt="User Image"> <span class="hidden-xs">{{session()->get('user_firstname')}} {{session()->get('user_lastname')}}</span> </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header"> <img src="{{ asset('public/administrator/img/user-icon.png') }}" class="img-circle" alt="User Image">
                            <p> {{session()->get('admin_session_user_firstname', '')}} {{session()->get('admin_session_user_lastname', '')}} - {{session()->get('admin_session_user_userTypeName')}}  </p>
                        </li>
                        <!-- Menu Body -->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left"> <a href="{{ url('administrator/myProfile') }}" class="btn btn-default btn-flat">Profile</a> </div>
                            <div class="pull-right"> <a href="{{ url('administrator/logout') }}" class="btn btn-default btn-flat">Sign out</a> </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
            </ul>
        </div>
        <!-- search form -->
        
        <!-- /.search form -->
        
        
    </nav>
</header>