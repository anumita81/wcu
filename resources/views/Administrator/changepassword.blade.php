<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        <div class="modal-body">
            {{ Form::open(array('url' => 'administrator/users/changepassword/'.$id.'/'.$page, 'name' => 'changepasswordFrm', 'id' => 'changepasswordFrm', 'method' => 'post')) }}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>New Password <span class="text-red">*</span></label>
                        <input id="password" name="password" class="form-control" required minlength="5" placeholder="Enter New Password" type="password">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input id="confirmpassword" name="confirmpassword" class="form-control" required equalTo="#password" placeholder="Confirm Password" type="password">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->



<script>
    $(function () {
        $("#changepasswordFrm").validate();
    });
</script>