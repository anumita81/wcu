<script src = "{{ asset('public/administrator/controller-css-js/city.js') }}" ></script>

<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$title}}</h4>
        </div>
        <div class="modal-body"> 
            {{ Form::open(array('url' => 'administrator/subject/save/'.$id.'/'.$page, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Subject Group<span class="text-red">*</span></label>
                        <select name="subjectGroupId" id="subjectGroupId" required="" class="customSelect">
                            <option value="">Select</option>
                            @foreach($subjectgroup as $subject)
                            <option value="{{isset($subject['id'])?$subject['id']:''}}" {{(!empty($subjectData->subjectGroupId) && ($subjectData->subjectGroupId==$subject['id']))?"selected":""}}>{{$subject['groupName']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Subject SubGroup</label>
                        <select name="sujectSubgroupId" id="sujectSubgroupId" class="customSelect">
                            <option value="">Select</option>
                            @foreach($subjectsubgroup as $subject)
                            <option value="{{isset($subject['id'])?$subject['id']:''}}" {{(!empty($subjectData->sujectSubgroupId) && ($subjectData->sujectSubgroupId==$subject['id']))?"selected":""}}>{{$subject['groupName']."-".$subject['subgroupName']}}</option>
                            @endforeach
                        </select>                  
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Subject for Class <span class="text-red">*</span></label>
                        <select name="subjectforClass" id="subjectforClass" class="customSelect">
                            <option value="">Select</option>
                            <option value="10"{{(!empty($subjectData->subjectforClass) && ($subjectData->subjectforClass=='10'))?"selected":""}} >10</option>
                            <option value="12" {{(!empty($subjectData->subjectforClass) && ($subjectData->subjectforClass=='12'))?"selected":""}}>12</option>
                            <option value="o" {{(!empty($subjectData->subjectforClass) && ($subjectData->subjectforClass=='o'))?"selected":""}}>others</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Subject Code <span class="text-red">*</span></label>
                        <input id="subjectCode" name="subjectCode" class="form-control" required value="{{!empty($subjectData->subjectCode)?$subjectData->subjectCode:''}}" placeholder="Enter ..." type="text">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Subject Name <span class="text-red">*</span></label>
                        <input id="subjectName" name="subjectName" class="form-control" required value="{{!empty($subjectData->subjectName)?$subjectData->subjectName:''}}" placeholder="Enter ..." type="text">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>No of papers <span class="text-red">*</span></label>
                        <input id="noofPapers" name="noofPapers" class="form-control" required value="{{!empty($subjectData->noofPapers)?$subjectData->noofPapers:''}}" placeholder="Enter ..." type="text">
                    </div>
                </div>
               
            </div>
            <div class="row">
                 <div class="col-md-6">
                    <div class="form-group">
                        <label>Full Marks <span class="text-red">*</span></label>
                        <input id="fullMarks" name="fullMarks" class="form-control" required value="{{!empty($subjectData->fullMarks)?$subjectData->fullMarks:''}}" placeholder="Enter ..." type="text">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Written Total<span class="text-red">*</span></label>
                        <input id="writtenTotal" name="writtenTotal" class="form-control" required value="{{!empty($subjectData->writtenTotal)?$subjectData->writtenTotal:''}}" placeholder="Enter ..." type="text">
                    </div>
                </div>
                
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Others Total <span class="text-red">*</span></label>
                        <input id="othersTotal" name="othersTotal" class="form-control" required value="{{!empty($subjectData->othersTotal)?$subjectData->othersTotal:''}}" placeholder="Enter ..." type="text">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Pass Marks<span class="text-red">*</span></label>
                        <input id="passMarks" name="passMarks" class="form-control" required value="{{!empty($subjectData->passMarks)?$subjectData->passMarks:''}}" placeholder="Enter ..." type="text">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->