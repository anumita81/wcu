@extends('Administrator.layouts.master')
@section('content')

<section class="content">
    <input type="hidden" id="assetPath" value="{{ asset('public/') }}">
    <div class="row m-b-15">
        {{ Form::open(array('url' => 'administrator/specialofferbanner', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
        <div class="col-lg-6 col-md-6"> 
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchDisplay=='10'?'selected':''}}>10</option>
                    <option {{$searchDisplay=='15'?'selected':''}}>15</option>
                    <option {{$searchDisplay=='20'?'selected':''}}>20</option>
                    <option {{$searchDisplay=='30'?'selected':''}}>30</option>
                    <option {{$searchDisplay=='40'?'selected':''}}>40</option>
                    <option {{$searchDisplay=='50'?'selected':''}}>50</option>
                </select>
            </div>
        </div>
        <input type="hidden" name="sort_order" id="sort_order" value="{{$sort_order}}" />

        {{ Form::close() }}
        <div class="col-lg-6 col-md-6 text-right">
            @if($canEdit == 1)<a class="btn btn-sm btn btn-primary" href="{{url('administrator/blockcontentedit/'.$blockcontent->id.'/special_offers')}}"><span class="Cicon"><i class="fa fa-pencil"></i></span> Manage Content</a>@endif
            @if($canAdd == 1)<a class="btn btn-sm btn btn-info recordAdd"><span class="Cicon"><i class="fa fa-plus"></i></span> Add New Banner</a>@endif
            @if($canDelete == 1)<a href="javascript:void(0)" id="delete-selected" onclick="deleteSelected();" class="btn btn-sm btn-danger"><span class="Cicon"><i class="fa fa-trash"></i></span> Delete Selected</a>@endif
        </div>
    </div>
    <div class="row"> 
        {{ Form::open(array('url' => 'administrator/bannerdeletemultiple/8', 'name' => 'frmsearch', 'id' => 'deleteAll', 'method' => 'post')) }} 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="2%" class="withCheck">
                                    <label>
                                        <input type="checkbox" class="chk_all flat-red">
                                    </label>
                                </th>
                                <th>Image Preview</th>
                                <th class="sorting_asc"><span onclick="$('#frmsearch').submit();">Display Order</span></th>
                                <th>Page Link</th>
<!--                                <th>Status</th>-->
                                <th width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($bannerRecords)>0)
                            @foreach($bannerRecords as $eachBanner)
                            <tr>
                                <td class="withCheck">
                                    <label>
                                        <input type="checkbox" name="deleteData[]" value="{{ $eachBanner->id }}" class="checkbox flat-red">
                                    </label>
                                </td>
                                <td><img 
                                        @php if (file_exists(public_path() . '/uploads/banner/'. $eachBanner->imagePath)) { @endphp 
                                        src="{{ asset('public/uploads/banner/'.$eachBanner->imagePath) }}" @php } 
                                        else { @endphp src="{{ asset('public/administrator/img/default-no-img.jpg') }}" @php } @endphp
                                        height="100px" width="150px"></td>
                                <td>{{ $eachBanner->displayOrder }}</td>
                                <td>{{ $eachBanner->pageLink }}</td>
<!--                                <td>
                                    @if($eachBanner->status=='1')
                                    <a data-toggle="tooltip" title="" class="btn btn-success btnActive updateStatus" data-edit="{{ $eachBanner->id }}" data-status="1" id="AL" data-original-title="Click to Change Staus">Active</a>
                                    @else
                                    <a data-toggle="tooltip" title="" class="btn btn-danger btnActive updateStatus" data-edit="{{ $eachBanner->id }}" data-status="0"  id="AL" data-original-title="Click to Change Staus">Inactive</a>
                                    @endif
                                </td>-->
                                <td>
                                    <input type="hidden" id="baseUrl" value="{{ url('/') }}">
                                    @if($canEdit == 1)<a class="text-green edit actionIcons" data-edit="{{ $eachBanner->id }}"  data-page="{{ $page }}"><i data-toggle="tooltip" title="Click to Edit" class="fa fa-fw fa-edit recordEdit"></i>
                                    </a>@endif
                                    @if($canDelete == 1)<a href="{{ url('/administrator/bannerdelete/'.$eachBanner->id.'/8/'.$page) }}" class="color-theme-2 actionIcons" data-delete="{{ $eachBanner->id }}" data-toggle="confirmation"><i title="Click to Delete" class="fa fa-fw fa-trash"></i></a>@endif
                                </td>
                            </tr>
                            @endforeach
                            @else

                            <tr class="odd"><td valign="top" colspan="6" class="dataTables_empty" style="text-align: center;">No records found</td></tr>
                            @endif


                        </tbody>
                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $bannerRecords->firstItem() . ' - ' . $bannerRecords->lastItem() . ' of  ' . $bannerRecords->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $bannerRecords->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
        {{ Form::close() }}
    </div>
    <!--Block 01-->

    <!--modal open-->
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-red"><span class="text-red"><i class="fa fa-exclamation-triangle"></i></span> Are you sure?</h4>
                </div>
                <div class="modal-body">
                    <p>The items will be deleted permanently. The action cannot be reversed.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="$('#deleteAll').submit();">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--modal close-->
    <!--modal open-->
    <div class="modal fade" id="modal-record">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add New Banner</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(array('id'=>'data-form','url' => 'administrator/addbanner/3/'.$page,'files'=>true)) }}

                    <!--<div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Upload Banner Image <span class="text-red">*</span></label>
                                <input id="bannerImage" name="bannerImage" type="file">
                            </div>
                        </div>
                    </div>-->

                    <div class="form-group">
                        <label>Upload Banner Image </label>
                        <div class="main-img-preview">
                            <img id="storeIcon" class="thumbnail img-preview" src="{{ asset('public/administrator/img/default-no-img.jpg') }}" title="Preview Logo">
                            <img id="loaded" src="#" alt="" style="display:none;">
                            <span class="closeImg"><i class="fa fa-times-circle"></i></span>
                        </div>
                        <div class="input-group">

                            <div class="input-group-btn">
                                <div class="fileUpload btn btn-custom-theme fake-shadow fixedWidth111">
                                    <span>Change</span>
                                    <input id="logo-id" name="bannerImage" type="file" class="attachment_upload">
                                </div>
                            </div>
                            <!--<input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled">-->
                        </div>
                        <p>Supported file types are: jpg, jpeg, png, gif<p>
                        <p>Max upload limit: 4MB</p>
                        <p>Min width: 1600px, Min height: 626px </p>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Display Order <span class="text-red">*</span></label>
                                <input id="displayOrder" name="displayOrder" maxlength="6" class="form-control" required onkeypress="return isNumberKey(event, this);" placeholder="Enter display Order" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Page Link </label>
                                <input id="warehousePageLink" name="warehousePageLink" class="form-control" placeholder="Enter Page Link" type="text">
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="text-right" id="warehouseBannerSubmit">
                            <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--modal close-->
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                onConfirm: function () {
                    //alert('test');
                    var linkElem = $(this);
                    var deleteId = $(this).attr('data-delete');
                    var baseUrl = $('#baseUrl').val();
                    $.ajax({
                        type: "get",
                        dataType: 'json',
                        url: baseUrl + '/bannerdelete/' + deleteId,
                        data: {},
                        success: function (msg) {
                            if (msg.deleted)
                            {
                                linkElem.parent().parent().remove();
                            }
                        }
                    });

                },
                // other options
            });
            $('.recordEdit').on('click', function (e) {
                e.preventDefault();
                var assetPath = $('#assetPath').val();
                var recordEditId = $(this).parent().attr('data-edit');
                var page = $(this).parent().attr('data-page');
                var baseUrl = $('#baseUrl').val();
                formAction = baseUrl + '/editbanner/8' + '/' + recordEditId + '/' + page;
                $.ajax({
                    type: "get",
                    dataType: 'json',
                    url: baseUrl + '/bannerinfo/' + recordEditId,
                    data: {},
                    success: function (msg) {
                        console.log(msg);
                        $("#displayOrder").val(msg.displayOrder);
                        $("#warehousePageLink").val(msg.pageLink);
                        $('#data-form').attr('action', formAction);
                        $("#modal-record .modal-title").text('Edit Banner');
                        $("#modal-record").modal();
                        $("#bannerImage").parent().find('img').remove();
                        $("#bannerImage").parent().append('<img class="bannerPreview" src="' + assetPath + '/uploads/banner/' + msg.imagePath + '" height="100px" width="150px">');
                        $("#storeIcon").attr('src', assetPath + '/uploads/banner/' + msg.imagePath);
                        //$("#ajaxResponse").append("<div>"+msg+"</div>");
                    }
                });
            });

            $('.recordAdd').on('click', function (e) {
                var assetPath = $('#assetPath').val();
                var page = $(this).attr('data-page');
                var baseUrl = $('#baseUrl').val();
                var formAction = baseUrl + '/addbanner/8/' + page;
                $('#data-form').attr('action', formAction);
                $("#modal-record .modal-title").text('Add Banner');
                $(".img-preview").attr('src', assetPath + '/administrator/img/default-no-img.jpg');
                $("#modal-record").modal();
            });

            $('.updateStatus').on('click', function (e) {
                e.preventDefault();
                var linkElem = $(this);
                var recordEditId = $(this).attr('data-edit');
                var baseUrl = $('#baseUrl').val();
                var recordStatus = $(this).attr('data-status');
                var postStatus = 1;
                if (recordStatus == 1)
                    postStatus = 0;

                $.ajax({
                    type: "get",
                    dataType: 'json',
                    url: baseUrl + '/editbannerstatus/' + recordEditId + '/' + postStatus,
                    data: {},
                    success: function (msg) {
                        //console.log(msg.updated);
                        if (msg.updated)
                        {
                            //console.log('i am here');
                            if (recordStatus == 1)
                            {
                                linkElem.removeClass('btn-success');
                                linkElem.addClass('btn-danger');
                                linkElem.attr('data-status', 0);
                                linkElem.text('Inactive');
                            }
                            else
                            {
                                linkElem.removeClass('btn-danger');
                                linkElem.addClass('btn-success');
                                linkElem.attr('data-status', 1);
                                linkElem.text('Active');
                            }
                        }
                    }
                });


            });
        });

        function deleteSelected() {
            if ($('.checkbox:checked').length == 0) {
                alert('Please select atleast one item!!');
            } else {
                $('#modal-default').modal('show');
            }
        }


//Promo Image upload validation
        (function ($) {
            $.fn.checkFileType = function (options) {
                var defaults = {
                    allowedExtensions: [],
                    success: function () {
                    },
                    error: function () {
                    }
                };
                options = $.extend(defaults, options);

                return this.each(function () {

                    $(this).on('change', function () {
                        var value = $(this).val(),
                                file = value.toLowerCase(),
                                extension = file.substring(file.lastIndexOf('.') + 1);

                        if ($.inArray(extension, options.allowedExtensions) == -1) {
                            options.error();
                            $(this).focus();
                        } else {
                            options.success();

                        }

                    });

                });
            };

        })(jQuery);

        $(function () {
            $('#logo-id').checkFileType({
                allowedExtensions: ['jpg', 'jpeg', 'gif', 'png'],
                success: function () {
                    //alert('Success');
                    $("#warehouseBannerSubmit").css('display', 'block');


                },
                error: function () {
                    alert('Error! File type not supported');
                    readURL('error');
                    $('#loaded').css('display', 'none');
                    $('#storeIcon').css('display', 'block');
                    $("#warehouseBannerSubmit").css('display', 'none');
                }
            });



        });

        function readURL(input) {
            if (input != 'error') {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#loaded').css('display', 'block');
                        $('#loaded').attr('src', e.target.result);
                        $('#storeIcon').css('display', 'none');
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            } else {
                $('#loaded').css('display', 'none');
                $('#storeIcon').css('display', 'block');
            }
        }


        $("#logo-id").change(function () {
            //alert(this.files[0]);
            readURL(this);
        });

    </script>
</section>                  

@endsection

