@extends('Administrator.layouts.master')
@section('content')
               
<section class="content">
    <input type="hidden" id="assetPath" value="{{ asset('public/uploads/banner/') }}">
    <div class="row m-b-15">
        {{ Form::open(array('url' => 'administrator/fillnshipbanner', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
        <div class="col-lg-6 col-md-6"> 
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchDisplay=='10'?'selected':''}}>10</option>
                    <option {{$searchDisplay=='15'?'selected':''}}>15</option>
                    <option {{$searchDisplay=='20'?'selected':''}}>20</option>
                    <option {{$searchDisplay=='30'?'selected':''}}>30</option>
                    <option {{$searchDisplay=='40'?'selected':''}}>40</option>
                    <option {{$searchDisplay=='50'?'selected':''}}>50</option>
                </select>
            </div>
        </div>
        <input type="hidden" name="sort_order" id="sort_order" value="{{$sort_order}}" />
        
        {{ Form::close() }}
        <div class="col-lg-6 col-md-6 text-right">
            <a data-toggle="modal" data-target="#modal-record" class="btn btn-sm btn btn-info"><span class="Cicon"><i class="fa fa-plus"></i></span> Add New Banner</a>
            <a href="javascript:void(0)" id="delete-selected" onclick="deleteSelected();" class="btn btn-sm btn-danger"><span class="Cicon"><i class="fa fa-trash"></i></span> Delete Selected</a>
        </div>
    </div>
    <div class="row"> 
    {{ Form::open(array('url' => 'administrator/bannerdeletemultiple/4', 'name' => 'frmsearch', 'id' => 'deleteAll', 'method' => 'post')) }} 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="2%" class="withCheck">
                                    <label>
                                        <input type="checkbox" class="chk_all flat-red">
                                    </label>
                                </th>
                                <th>Image Preview</th>
                                <th class="sorting_asc"><span onclick="$('#frmsearch').submit();">Display Order</span></th>
                                <th>Page Link</th>
                                <th>Status</th>
                                <th width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($bannerRecords)>0)
                            @foreach($bannerRecords as $eachBanner)
                            <tr>
                                <td class="withCheck">
                                    <label>
                                        <input type="checkbox" name="deleteData[]" value="{{ $eachBanner->id }}" class="checkbox flat-red">
                                    </label>
                                </td>
                                <td><img src="{{ asset('public/uploads/banner/'.$eachBanner->imagePath) }}" height="100px" width="150px"></td>
                                <td>{{ $eachBanner->displayOrder }}</td>
                                <td>{{ $eachBanner->warehousePageLink }}</td>
                                <td>
                                    @if($eachBanner->status=='1')
                                        <a data-toggle="tooltip" title="" class="btn btn-success btnActive updateStatus" data-edit="{{ $eachBanner->id }}" data-status="1" id="AL" data-original-title="Click to Change Staus">Active</a>
                                    @else
                                        <a data-toggle="tooltip" title="" class="btn btn-danger btnActive updateStatus" data-edit="{{ $eachBanner->id }}" data-status="0"  id="AL" data-original-title="Click to Change Staus">Inactive</a>
                                    @endif
                                </td>
                                <td>
                                <input type="hidden" id="baseUrlContent" value="{{ url('/') }}">
                                    <a class="text-green edit actionIcons" data-edit="{{ $eachBanner->id }}"  data-page="{{ $page }}"><i data-toggle="tooltip" title="Click to Edit" class="fa fa-fw fa-edit recordEdit"></i>
                                    </a>
                                    <a href="{{ url('/administrator/bannerdelete/'.$eachBanner->id.'/4/'.$page) }}" class="color-theme-2 actionIcons" data-delete="{{ $eachBanner->id }}" data-toggle="confirmation"><i title="Click to Delete" class="fa fa-fw fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            
                                <tr class="odd"><td valign="top" colspan="6" class="dataTables_empty" style="text-align: center;">No records found</td></tr>
                            @endif
                            
                            
                        </tbody>
                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $bannerRecords->firstItem() . ' - ' . $bannerRecords->lastItem() . ' of  ' . $bannerRecords->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $bannerRecords->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
        {{ Form::close() }}
    </div>
    <!--Block 01-->

    <!--modal open-->
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-red"><span class="text-red"><i class="fa fa-exclamation-triangle"></i></span> Are you sure?</h4>
                </div>
                <div class="modal-body">
                    <p>The items will be deleted permanently. The action cannot be reversed.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="$('#deleteAll').submit();">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--modal close-->
    <!--modal open-->
    <div class="modal fade" id="modal-record">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add New Banner</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(array('id'=>'data-form','url' => 'administrator/addbanner/4/'.$page,'files'=>true)) }}

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Banner Image <span class="text-red">*</span></label>
                                    <input id="bannerImage" name="bannerImage" type="file">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Display Order <span class="text-red">*</span></label>
                                    <input id="displayOrder" name="displayOrder" class="form-control" required placeholder="Enter display Order" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Page Link </label>
                                    <input id="warehousePageLink" name="warehousePageLink" class="form-control" placeholder="Enter Page Link" type="text">
                                </div>
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                            <div class="text-right">
                                <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--modal close-->
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
              rootSelector: '[data-toggle=confirmation]',
              onConfirm:function(){
                //alert('test');
                var linkElem = $(this);
                var deleteId = $(this).attr('data-delete');
                var baseUrl = $('#baseUrlContent').val();
                $.ajax({
                    type: "get",
                    dataType:'json',
                    url: baseUrl + '/administrator/bannerdelete/'+deleteId,
                    data: {},
                    success: function( msg ) {
                        if(msg.deleted)
                        {
                            linkElem.parent().parent().remove();
                        }
                    }
                });

                },
              // other options
            });
            $('.recordEdit').on('click', function (e) {
                e.preventDefault();
                var assetPath = $('#assetPath').val();
                var recordEditId = $(this).parent().attr('data-edit');
                var page = $(this).parent().attr('data-page');
                var baseUrl = $('#baseUrlContent').val();
                //var formAction = $('#data-form').attr('action');
                formAction = baseUrl+'/administrator/editbanner/4'+'/'+recordEditId+'/'+page;
                //$('#data-form').attr('action','jakhusi');
                //console.log(baseUrl+' '+recordEditId);
                $.ajax({
                    type: "get",
                    dataType:'json',
                    url: baseUrl + '/administrator/bannerinfo/'+recordEditId,
                    data: {},
                    success: function( msg ) {
                        console.log(msg);
                        $("#displayOrder").val(msg.displayOrder);
                        $("#warehousePageLink").val(msg.warehousePageLink);
                        $('#data-form').attr('action',formAction);
                        $("#modal-record .modal-title").text('Edit Banner');
                        $("#modal-record").modal();
                        $("#bannerImage").parent().find('img').remove();
                        $("#bannerImage").parent().append('<img src="'+assetPath+'/'+msg.imagePath+'" height="100px" width="150px">');
                        //$("#ajaxResponse").append("<div>"+msg+"</div>");
                    }
                });
            });

            $('.updateStatus').on('click',function(e){
                e.preventDefault();
                var linkElem = $(this);
                var recordEditId = $(this).attr('data-edit');
                var baseUrl = $('#baseUrlContent').val();
                var recordStatus = $(this).attr('data-status');
                var postStatus = 1;
                if(recordStatus == 1)
                    postStatus = 0;

                $.ajax({
                    type: "get",
                    dataType:'json',
                    url: baseUrl+'/administrator/editbannerstatus/'+recordEditId+'/'+postStatus,
                    data: {},
                    success: function( msg ) {
                        //console.log(msg.updated);
                        if(msg.updated)
                        {
                            //console.log('i am here');
                            if(recordStatus == 1)
                            {
                                linkElem.removeClass('btn-success');
                                linkElem.addClass('btn-danger');
                                linkElem.attr('data-status',0);
                                linkElem.text('Inactive');
                            }
                            else
                            {
                                linkElem.removeClass('btn-danger');
                                linkElem.addClass('btn-success');
                                linkElem.attr('data-status',1);
                                linkElem.text('Active');
                            }
                        }
                    }
                });
                

            });
        });

        function deleteSelected() {
            if ($('.checkbox:checked').length == 0) {
                alert('Please select atleast one item!!');
            } else {
                $('#modal-default').modal('show');
            }
        }
    </script>
</section>                  
 
@endsection

