<script src="{{ asset('public/administrator/js/jQuery.print.js') }}"></script>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">

<div style="border: 1px solid #B0AFBE;padding 2px; align:center;width:100%;" id="printContent">
<div style="border: 2px solid #B0AFBE; width:98%; align:center; margin:2px;">
<table width="98%">

<tr>
<td align="left" width="40%"><img src="{{ asset('public/administrator/img/logoSTMD.jpg') }}" width="150" height="80">
</td>
<td width="60%">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
    		<td align="center"><font size="2">{{$shipmentData['fromAddress']}}</font></td>
		</tr>
		<tr>
  			<td colspan="2" nowrap="nowrap" align="center" ><font size="2">{{ $deliveryAddress['fromCityName'] }}, {{ $deliveryAddress['fromStateName'] }}, {{ $deliveryAddress['fromCountryName'] }}</font></td>
		</tr>
		 <tr>
            <td align="center"><font size="2">shoptomydoor.com</font></td>
        </tr>

		<tr>
		    <td align="center"><font size="2">+1 888 315 9027</font></td>
		</tr>

	</table>
</td>
</tr>
<tr>
    <td colspan="2" nowrap="nowrap" align="center" ><hr /></td>
</tr>

<tr>
<td align="left" width="40%" style="padding-left: 10px;">
	<table width="100%">
            <tr>
                <td align="left"><font size="3">{{$shipmentData['customerName']}}</font></td>
            </tr>
             <tr>
                <td align="left"><font size="3">{{$deliveryAddress['toAddress']}}</font></td>
            </tr>
             <tr>
                <td align="left"><font size="3"><b>{{$deliveryAddress['toCityName']}}&nbsp;{{$deliveryAddress['toStateName']}}&nbsp;&nbsp;{{$deliveryAddress['toCountryName']}}</b></font></td>
            </tr>
            @if(!empty($deliveryAddress['toEmail']))
             <tr>
                <td align="left"><font size="3">{{$deliveryAddress['toEmail']}}</font></td>
            </tr>
            @endif
             @if(!empty($deliveryAddress['toPhone']))
             <tr>
                <td align="left"><font size="3">{{$deliveryAddress['toPhone']}}</font></td>
            </tr>
            @endif
   </table>
</td>
<td width="60%" align="center">
<table width="100%" cellspacing="0" cellpadding="0">
	<tr>
	    <td nowrap="nowrap" align="center" ><font size="4"><b># of packages: {{$packageCount}}</b></font></td>
	</tr>
	<tr>
	    <td nowrap="nowrap" align="center" ><font size="3"><b>Shipment# :{{$shipmentData['id']}}/{{$packageCount}}/{{$packageCount}}</b></font></td>
	</tr>
	@if (!empty($orderId))
		<tr>
    		<td nowrap="nowrap" align="center" ><font size="3"><b>Order# :{{ $orderId }}</b></font></td>
    	</tr>
	@endif	
	<tr>
            <td nowrap="nowrap" align="center" ><font size="3"><b>Gross Weight: {{ $delivery['totalWeight'] }}Lbs</b></font></td>
        </tr>
	<tr>
	    <td nowrap="nowrap" align="center" ><font size="3"><b>{{ date('m-d-Y',strtotime($shipmentData['createdOn'])) }}</b></font></td>
	</tr>

</table>
</td>
</tr>
<tr>
    <td colspan="2" nowrap="nowrap" align="center" ><hr /></td>
</tr>

<tr>
    <td colspan="2">
		<table width="100%">
		<tr>
			<td align="center" width="35%"><b>Item(s)</b></td>
			<td align="center" width="15%"><b>Quantity</b></td>
			<td align="center" width="15%" nowrap="nowrap"><b>Delivery #</b></td>
			<td align="center" width="15%"><b>Packed</b></td>
		</tr>
		@php
			$totalQty = 0;
			$totalDelivery = 0;
                        $currentDelivery = 0;
		@endphp
		@foreach($delivery['deliveries'] as $eachDelivery)
		<tr>
			@php
				$items = array();
				$itemQty = 0;
				$currentDelivery++;
                                if($eachDelivery['deliveryAllItemReturned'] == 1)
                                    continue;
                                $totalDelivery++;
			@endphp
			@foreach($eachDelivery['packages'] as $eachPackages)
				@php
                                    if($eachPackages['itemReturn'] != '0')
                                        continue;
                                    $items[]=$eachPackages['itemName'];
                                    $itemQty += $eachPackages['itemQuantity'];
                                    $totalQty += $eachPackages['itemQuantity'];
				@endphp		
			@endforeach
			<td align="center">{{ (!empty($items)?implode(',',$items):"") }}</td>
			<td align="center">{{ $itemQty }}</td>
			<td align="center">{{ $currentDelivery }}</td>
			<td align="center"><div style="display:inline-block;width:16px;height:16px;border:1px solid #000;"></div></td>
		</tr>
		@endforeach
		<tr>
			<td>&nbsp;</td>
			<td colspan="3"><hr /></td>
            <td>&nbsp;</td>
		</tr>
		<tr>
                    <td>Total Number of Items</td>
                    <td align="center">{{ $totalQty }}</td>
                    <td align="center">{{ $totalDelivery }}</td>
                    <td>&nbsp;</td>
		</tr>
		<tr>
            <td>&nbsp;</td>
            <td colspan="3"><hr /></td>
            <td>&nbsp;</td>
        </tr>	
		</table>	
	</td>
</tr>

<tr>
    <td colspan="2" nowrap="nowrap" align="center" ><hr /></td>
</tr>
<tr>
    <td colspan="2" nowrap="nowrap" align="center">
        <table cellpadding="1" cellspacing="1" border="0">
        <tr>
            <td width="15%"></td>
            <td width="15%"></td>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td colspan="2" width="30%" nowrap="nowrap">All items packed and shipped?</td>
            <td width="10%" nowrap="nowrap">Yes <div style="display:inline-block;width:16px;height:16px;border:1px solid #000;margin-bottom:-3px;"></div></td>
            <td width="10%" nowrap="nowrap">No <div style="display:inline-block;width:16px;height:16px;border:1px solid #000;margin-bottom:-3px;"></div></td>
            <td width="30%" nowrap="nowrap">If No, When is Ship Out Date:</td>
            <td width="20%" nowrap="nowrap"><div style="display:inline-block;width:100%;height:16px;border-bottom:1px dashed #000;"></div></td>
        </tr>
        <tr>
            <td nowrap="nowrap">If No, provide reason:</td>
            <td colspan="5"><div style="display:inline-block;width:100%;height:16px;border-bottom:1px dashed #000;"></div></td>
        </tr>
        <tr>
            <td colspan="6"><div style="display:inline-block;width:100%;height:16px;border-bottom:1px dashed #000;"></div></td>
        </tr>	
		</table>	
	</td>
</tr>

<tr>
    <td colspan="2" nowrap="nowrap" align="center" ><hr /></td>
</tr>

<tr>
    <td colspan="2" nowrap="nowrap" align="center" >Thanks for choosing Shoptomydoor. We appreciate your business</td>
</tr>

<tr>
    <td colspan="2" nowrap="nowrap" align="center" ><hr /></td>
</tr>

<tr>
    <td colspan="2" nowrap="nowrap" align="center" >
		Proudly packed by {{ (!empty($packageDetailsData[0]) && $packageDetailsData[0]->user!='') ? $packageDetailsData[0]->user:"Nduka Udeh" }}<br />
		Issues or concerns with my packaging?? Email me at packaging@shoptomydoor.com<br /><br />
	</td>
</tr>
</table>
</div>
</div>
<div class="modal-footer">
    <div class="text-right">
        <button class="print-link no-print" id="printLabel">
        Print this
        </button>
    </div>
</div>
</div>
    </div>
    <!-- /.modal-content -->
<script>
$("#printLabel").on('click', function () {
    $("#printContent").print({
        deferred: $.Deferred().done(function () {
            $('#modal-addEdit').modal('hide');
        })
    });
});
</script>
</div>
<!-- /.modal-dialog -->
