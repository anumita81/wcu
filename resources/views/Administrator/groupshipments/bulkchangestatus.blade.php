<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        <div class="modal-body">
            {{ Form::open(array('url' => 'administrator/groupshipments/bulkchangestatus/'.$id.'/'.$page, 'name' => 'changestatusFrm', 'id' => 'changestatusFrm', 'method' => 'post')) }}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <select name="shipmentStatus" class="form-control customSelect2 input-lg" id="shipmentStatus">
                            @for($i=($shipmentStatus+1);$i <= count($shipmentStatusList); $i++)
                            <option  value="{{$i}}">{{$shipmentStatusList[$i]}}</option>
                            @endfor
                        </select>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->



<script>
    $(function () {
        $("#changepasswordFrm").validate();
    });
</script>