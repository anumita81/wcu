<script src="{{ asset('public/administrator/js/jQuery.print.js') }}"></script>

<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title"></h4>
    </div>
    <div class="modal-body" id="printContent"> @foreach($packageDetailsData as $key => $pck)
      <div style="border:1px solid #B0AFBE; padding:2px; width:100%; height:754px; margin-bottom:10px;">
        <div style="width:98%; margin:0 auto; height:746px;">
        <table width="95%" style="height: 650px;">
          <tr style="height:90px;">
            <td align="left" width="20%"><img src="{{ asset('public/administrator/img/logoSTMD.jpg') }}" width="150" height="80"></td>
            <td width="80%"><table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="center" nowrap><font size="{$font_size2}"><b>SENDER : American AirSea Cargo Ltd.</b></font></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td colspan="2"><hr /></td>
          </tr>
          <tr valign="top">
            <td align="center" width="40%"> @if($orderId != '') <img src="data:image/png;base64,{{DNS1D::getBarcodePNG("$orderId", 'C128')}}" width='100' height='50'>
          </div>
          
          <!--<img src="{$xcart_web_dir}/barcode.php?width=180&height=120&type=C128C&code={$orderid}" width="180" height="120">-->
          @endif<br />
          <b>{{ $orderId }}</b>
          </td>
          
          <td width="60%" align="center"><table width="100%" cellspacing="0" cellpadding="3">
                <tr valign="top">
                  <td nowrap="nowrap" align="center" colspan="2"><font size="{$h_size}"><b>Package: {{$key+1}} of {{$packageCount}}</b></font></td>
                </tr>
                <tr>
                  <td nowrap="nowrap" align="left" ><font size="{$font_size}"><b>Shipment# :{{$shipmentData['id']}}<b></font></td>
                  <td nowrap="nowrap" align="left" ><font size="{$font_size}"><b>Order# :{{ $orderId }}<b></font></td>
                </tr>
                <tr>
                  <td nowrap="nowrap" align="left" ><font size="{$font_size}"><b>Gross Weight: @if(isset($pck->chargeableWeight)) {{ round($pck->chargeableWeight/2.2, 2) }} kg @endif</b></font></td>
                  <td nowrap="nowrap" align="left" ><font size="{$font_size}"><b># of Items: {{ $noOfItem }}</b></font></td>
                </tr>
                <tr>
                  <td colspan="2" align="left"><b>CONTENTS:</b></td>
                </tr>
                <tr>
                  <td colspan="2" align="left">&nbsp;</td>
                </tr>
              </table></td>
          </tr>
          <tr valign="top">
            <td align="center" width="20%" nowrap><font size="{$font_size2}"><b>Waybill/Tracking Number</b></font></td>
            <td><table width="100%" cellspacing="0" cellpadding="1">
                <tr>
                  <td width="40%" align="left"><font size="{$font_size2}"><b>ORIGIN:</b></font></td>
                  <td width="60%" align="left"><font size="{$font_size2}"><b>DESTINATION:</b></font></td>
              </table></td>
          </tr>
          <tr>
            <td colspan="2" nowrap="nowrap" align="center" ><hr /></td>
          </tr>
          <tr>
            <td align="center" colspan="2" style="overflow:hidden; width:{$label_width}px;"><font size="{$font_size2}"><b>NOTE: Verify that there is no damage or cut on the package before accepting. Any issues, first call 0700 800 8000</b></font></td>
          </tr>
          <tr>
            <td colspan="2" nowrap="nowrap" align="center" ><hr /></td>
          </tr>
          <tr>
            <td align="center" colspan="2" style="padding-left:70px;"><table width="100%">
                <tr>
                  <td width="15%"><b>TO:</b></td>
                  <td align="left" width="85%"><font size="{$font_size}">{{$deliveryAddress['toName']}}</font></td>
                </tr>
                <tr>
                  <td width="15%">&nbsp;</td>
                  <td align="left" width="85%"><font size="{$font_size}">{{$deliveryAddress['toAddress']}}</font></td>
                </tr>
                <tr>
                  <td width="15%">&nbsp;</td>
                  <td align="left" width="85%"><font size="{$font_size}"><b>{{$deliveryAddress['toCityName']}}&nbsp;{{$deliveryAddress['toStateName']}}&nbsp;{{$deliveryAddress['toZipCode']}}&nbsp;{{$deliveryAddress['toCountryName']}}</b></font></td>
                </tr>
                @if(!empty($shipmentData['toEmail']))
                <tr>
                  <td width="15%">&nbsp;</td>
                  <td align="left" width="85%"><font size="{$font_size}">{{$deliveryAddress['toEmail']}}</font></td>
                </tr>
                @endif
                @if(!empty($shipmentData['toPhone']))
                <tr>
                  <td width="15%">&nbsp;</td>
                  <td align="left" width="85%"><font size="{$font_size}">{{$deliveryAddress['toPhone']}} @if(isset($deliveryAddress['toAlternatePhone'])) {{$deliveryAddress['toAlternatePhone']}} @endif</font></td>
                </tr>
                @endif
              </table></td>
          </tr>
          <tr valign="top">
            <td align="center" colspan="2"><table width="100%">
                <tr style="height:50px;" valign="top">
                  <td nowrap="nowrap" width="35%" align="center"><font size="{$font_size2}"><b><u>RECEIVER PHONE#</u></b></font></td>
                  <td nowrap="nowrap" width="35%" align="center"><font size="{$font_size2}"><b><u>RECEIVER NAME</u></b></font></td>
                  <td nowrap="nowrap" width="35%" align="center"><font size="{$font_size2}"><b><u>RECEIVER SIGN</u></b></font></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td colspan="2" nowrap="nowrap" align="center" ><hr /></td>
          </tr>
          <tr>
            <td colspan="2" nowrap="nowrap" align="center" ><table width="100%" cellspacing="0" cellpadding="1">
                <tr>
                  <td colspan="3" align="center"><font size="{$font_size2}"><b>OTHER DETAILS (Tick one box)</b></font></td>
                </tr>
                <tr>
                  <td align="center" nowrap="nowrap"><font size="{$font_size}"><b>Envelope&nbsp;&#9633;</b></font></td>
                  <td align="center" nowrap="nowrap"><font size="{$font_size}"><b>Nations Delivery Box&nbsp;&#9633;</b></font></td>
                  <td align="center" nowrap="nowrap"><font size="{$font_size}"><b>Others&nbsp;&#9633;</b></font></td>
                </tr>
                <tr>
                  <td colspan="3" align="left"><font size="{$font_size2}"><b>DELIVERY INSTRUCTIONS: </b></font></td>
                </tr>
                <tr>
                  <td align="center" nowrap="nowrap"><font size="{$font_size}"><b>Hold For Collection&nbsp;&#9633;</b></font></td>
                  <td align="center" nowrap="nowrap"><font size="{$font_size}"><b>Weekday&nbsp;&#9633;</b></font></td>
                  <td align="center" nowrap="nowrap"><font size="{$font_size}"><b>Weekend&nbsp;&#9633;</b></font></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td colspan="2" nowrap="nowrap" align="center" ><hr /></td>
          </tr>
          <tr valign="top">
            <td align="center" colspan="2"><table width="100%">
                <tr style="height:50px;" valign="top">
                  <td nowrap="nowrap" width="50%" align="center"><font size="{$font_size2}"><b><u>NATIONS DELIVERY SIGNATURE</u></b></font></td>
                  <td nowrap="nowrap" width="50%" align="center"><font size="{$font_size2}"><b><u>AASC SIGNATURE</u></b></font></td>
                </tr>
              </table></td>
          </tr>
          <tr valign="bottom">
            <td colspan="2" nowrap="nowrap" align="center" ><font size="{$font_size2}"><b>{{ date("d-m-Y") }}</b></font></td>
          </tr>
        </table>
      </div>
    </div>
    @endforeach
    <div class="modal-footer">
      <div class="text-right">
        <button class="print-link no-print" id="printLabel"> Print this </button>
      </div>
    </div>
  </div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
<script>
$("#printLabel").on('click', function () {
    $("#printContent").print({
        deferred: $.Deferred().done(function () {
            $('#modal-addEdit').modal('hide');
        })
    });
});
</script>