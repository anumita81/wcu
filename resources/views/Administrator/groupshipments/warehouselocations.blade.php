<label for="sel1">Location</label>
<div class="table-responsive m-t-20">
    <table class="table table-bordered table-hover no-margin">
        <thead>
            <tr class="headerTblBg">
                <th valign="top" align="left">Row</th>
                <th valign="top" align="left">Zone</th>
                <th width="25%" valign="top" align="left">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach($warehouselocation as $warehouselocation)
            <tr id="warehouse{{$warehouselocation->id}}">
                <td>{{$warehouselocation->rowName}}</td>
                <td>{{$warehouselocation->zoneName}}</td>
                <td>
                    <a href="javascript:void(0);" onclick="showAddEdit({{$warehouselocation->id}}, {{$id}}, 'shipments/addeditlocation')" class="text-green actionIcons" data-toggle="tooltip" title="Click to Edit"><i class="fa fa-fw fa-edit"></i></a>
                    <a class="actionIcons deletelocation color-theme-2" data-warehouseid="{{$warehouselocation->id}}" href="javascript:void(0);"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<br/>
<div class="form-group"> <a onclick="showAddEdit(0, {{$id}}, 'shipments/addeditlocation')" class="btn btn-default custumButt btnGreen m-r-15 add_warehouse_location">Add</a>
</div>
@if(!empty($warehouseLocationLog))
<div class="form-group">
    @foreach($warehouseLocationLog as $row)
    <p>{{$row}}</p>
    @endforeach
</div>
@endif