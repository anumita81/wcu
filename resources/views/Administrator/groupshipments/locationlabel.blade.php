<script src="{{ asset('public/administrator/js/jQuery.print.js') }}"></script>

<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        <div id="printContent" class="modal-body">
            <table width="500" height="750" border="0" align="center" cellspacing="0" cellpadding="0" style="border:3px solid #999;font-family:Arial,Helvetica,sans-serif;color:#000">
                <tr>
                    <td align="center">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="150">
                            <tr>
                                <td align="center" valign="top" style="font-size:28px"><strong>LOCATION LABEL</strong></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="middle">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="450">
                            <tr>
                                <td align="center" valign="top" style="font-size:28px">Shoptomydoor</td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" height="20"> </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="font-size:50px">Ship # -{{$shipment->id}}</td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" height="20"> </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="font-size:50px">Unit # - {{$unit}}</td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" height="20"> </td>
                            </tr>
                            @if(!empty($warehouselocation))
                            @foreach($warehouselocation as $row)
                            <tr>
                                <td align="center" valign="top" style="font-size:50px">{{$row->rowName}} - {{$row->zoneName}}</td>
                            </tr>
                            @endforeach
                            @endif
                            <tr>
                                <td align="center" valign="top" height="20"> </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top"><img src="data:image/png;base64,{{DNS1D::getBarcodePNG($shipment->id,"C39",5,50)}}" alt="barcode" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="150">
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <div class="text-right">
                <button id="printLabel" class="print-link no-print">
                    Print this
                </button>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->





<script>
$("#printLabel").on('click', function () {
    $("#printContent").print({
        globalStyles: false,
        mediaPrint: false,
        append: null,
        prepend: null,
        title: null,
        doctype: '<!doctype html>',
        deferred: $.Deferred().done(function () {
            $('#modal-addEdit').modal('hide');
        })
    });
});
</script>