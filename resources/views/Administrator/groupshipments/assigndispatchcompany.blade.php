<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        <div class="modal-body">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <select name="shippingMethod" class="form-control customSelect2 input-lg" id="shippingMethod">
                            <option value="">Select</option>
                            @foreach($dispatchCompany as $company)
                            <option value="{{$company['id']}}">{{$company['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <div class="text-right">
                    @if(isset($assignblock) && !empty($assignblock) && $assignblock == 'package') 
                        <button type="submit" class="btn btn-success" onclick="assignPackageDispatchCompany({{$id}});"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                    @else
                        <button type="submit" class="btn btn-success" onclick="assignDispatchCompany({{$id}});"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                    @endif
                </div>
            </div>
           
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->



<script>
    $(function () {
        $("#changepasswordFrm").validate();
    });
</script>