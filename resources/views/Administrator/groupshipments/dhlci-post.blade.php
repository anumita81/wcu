<script src="{{ asset('public/administrator/js/jQuery.print.js') }}"></script>

<div class="d-flex justify-content-center">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
<div style="border: 2px solid #B0AFBE; width:800px;" id="printContent">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
  <td width="60%" class="vtd">
    <table width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td class="vtop">
          <p class="vhead">Shipper name and contact name: </p>
          <p class="vline">{{$fromCompanyName}}</p>
          <p class="vline">{{$fromName}}</p>
          <p class="vhead">Address:</p>
          <p class="vline">{{$fromAddress}}</p>
          <p class="vline">{{$fromCity}}&nbsp;{{$fromState}}&nbsp;{{$fromZip}}</p>
          <br />
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td class="vtd pleft" width="30%"><p class="vhead">Post Code</p></td><td class="vtd pleft" width="30%"><p class="vhead">Country</p></td><td class="pleft" width="40%"><p class="vhead">Phone/Fax</p></td>
            </tr>
            <tr>
                            <td class="vtd pleft"><p class="vline">{{$fromZip}}</p></td><td class="vtd pleft"><p class="vline">{{$fromCountry}}</p></td><td class="pleft"><p class="vline">{{$fromPhone}}</p></td>
                        </tr>
          </table>
        </td>
      </tr>
    </table>  
  </td>
  <td width="40%" class="vtop">
    <table width="100%" cellspacing="0" cellpadding="0">
      <tr class="vtop">
        <td class="vtop pleft">
          <p class="vhead">Invoice Date:</p>
          {{$invoiceDate}}
        </td>
      </tr>
      <tr><td><hr /></td></tr>
      <tr class="vtop">
                <td class="vtop pleft">
                    <p class="vhead">Invoice Number:</p>
                  {{$invoiceNo}}
        </td>
            </tr>
      <tr><td><hr /></td></tr>
      <tr class="vtop">
                <td class="vtop pleft">
                    <p class="vhead">Terms of Trade (Incoterm):</p>
                    {{$incoterms}}
                </td>
            </tr>
    </table>  
  </td> 
</tr>
<tr>
    <td width="60%" class="vtd" style="border-top: 1px solid black; border-bottom: 1px solid black;">
    <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td class="vtop">
                    <p class="vhead">Consignee name and contact name:</p>
                    <p class="vline">{{$toCompanyName}}</p>
                    <p class="vline">{{$toName}}</p>
                    <p class="vhead">Delivery Address:</p>
                    <p class="vline">{{$toAddress}}</p>
                    <p class="vline">{{$toCity}}&nbsp;{{$toState}}&nbsp;{{$toZip}}</p>
                    <br />
                    <table width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="vtd pleft" width="30%"><p class="vhead">Post Code</p></td><td class="vtd pleft" width="30%"><p class="vhead">Country</p></td><td class="pleft" width="40%"><p class="vhead">Phone/Fax</p></td>
                        </tr>
                        <tr>
                            <td class="vtd pleft"><p class="vline">{{$toZip}}</p></td><td class="vtd pleft"><p class="vline">{{$toCountry}}</p></td><td class="pleft"><p class="vline">{{$toPhone}}</p></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
  </td>
  <td width="40%" class="vtop" style="border-top: 1px solid black; border-bottom: 1px solid black;">
    <table width="100%" cellspacing="0" cellpadding="0">
            <tr class="vtop">
                <td class="vtop pleft">
                    <p class="vhead">Total Number of Pieces:&nbsp;&nbsp;&nbsp;{{$Pieces}}</p>
                </td>
            </tr>
            <tr class="vtop">
                <td class="vtop pleft">
                    <p class="vhead">Total Gross Weight:&nbsp;&nbsp;&nbsp;{{$grossWeight}}lbs</p>
                </td>
            </tr>
            <tr class="vtop">
                <td class="vtop pleft">
                    <p class="vhead">Total Chargeable Weight:&nbsp;&nbsp;&nbsp;{{$netWeight}}lbs</p>
                </td>
            </tr>
      <tr><td><hr /></td></tr>
        </table>
    
    <table width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td class="pleft" style="font-style: italic;font-weight: bold;" width="30%">CARRIER:</td>
        <td class="vtop pleft"><img src="{{ asset('public/administrator/img/dhl_express.gif') }}" width="120" height="40"></td>
      </tr>
    </table>
    <br />
    <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td class="pleft" style="font-style: italic;font-weight: bold;" width="40%">AIR WAYBILL No:</td>
                <td class="vtop pleft" style="font-style italic;font-weight: bold;">{{$airWaybillNo}}</td>
            </tr>
        </table>

  </td>
</tr>

<table width="100%" cellspacing="0" cellpadding="5">

<tr>
    <td align="center" class="vtd" style="background: #DBDBDB;padding-top:15px;padding-bottom:15px;border-bottom: 1px solid black;"><b>Full Description of Goods</b></td>
    <td align="center" class="vtd" style="background: #DBDBDB;padding-top:15px;padding-bottom:15px;border-bottom: 1px solid black;"><b>Customs Commodity Code</b></td>
    <td align="center" class="vtd" style="background: #DBDBDB;padding-top:15px;padding-bottom:15px;border-bottom: 1px solid black;"><b>Country of Manufacturer</b></td>
    <td align="center" class="vtd" style="background: #DBDBDB;padding-top:15px;padding-bottom:15px;border-bottom: 1px solid black;"><b>Qty</b></td>
    <td align="center" class="vtd" style="background: #DBDBDB;padding-top:15px;padding-bottom:15px;border-bottom: 1px solid black;"><b>Unit Value</b></td>
    <td align="center" style="background: #DBDBDB;padding-top:15px;padding-bottom:15px;border-bottom: 1px solid black;"><b>Subtotal Value and Currency</b></td>
</tr>
@php $number=0; @endphp
@foreach($descr as $key => $dsc)
@php $number++; @endphp
<tr>
    <td class="vtd pleft">  
    <br />{{$number}}. {{$dsc}}
    </td>
    <td align="center" class="vtd">{{$ccc[$key]}}</td>
    <td align="center" class="vtd">{{$manufCountry[$key]}}</td>
    <td align="center" class="vtd">{{$qty[$key]}} Pcs.</td>
    <td align="center" class="vtd">{{$price[$key]}}</td>
    <td align="center" >@php $totalMultiQty = (new \App\Helpers\customhelper)->getExtractCurrency($price[$key]); $rawTotalProductWisePrice = ($totalMultiQty * $qty[$key]); 
      echo $TotalProductWisePrice = (new \App\Helpers\customhelper)->getCurrencySymbolFormat($rawTotalProductWisePrice);
      @endphp</td>
</tr>
@endforeach
@php $ship = (new \App\Helpers\customhelper)->getCurrencySymbolFormat('0.00'); @endphp 
<tr>
    <td width="60%" class="vtd pleft" colspan="3" style="text-decoration:underline;">  
    <br />
    Total Value and Currency Used
  <br />
  </td>
    <td align="center" class="vtd"></td>
    <td align="center" class="vtd"></td>
    <td align="center" style="text-decoration:underline;" aligh="right">@php echo (new \App\Helpers\customhelper)->getCurrencySymbolFormat($totalPrice); @endphp </td>
</tr>
<tr>
    <td width="60%" style="font-style: italic;font-weight: bold;" class="vtd pleft" colspan="3">  
    <br />
  Total Shipping Cost -
  </td>
    <td align="center" class="vtd" aligh="right"></td>
    <td align="center" class="vtd" aligh="right"></td>
    <td align="center" aligh="right" style="font-style: italic;font-weight: bold;">@php echo (new \App\Helpers\customhelper)->getCurrencySymbolFormat(round(($dhlAccountInfo->shippingCost*$grossWeight),2)); @endphp</td>
</tr>

<tr>
  <td colspan="6" style="border-top: 1px solid black;"></td>
</tr>

<tr>
    <td colspan="6" class="pleft">I/We hereby certify that the information on this invoice is true and correct and that the contents of this shipment are as stated above.</td>
</tr>

<tr>
    <td colspan="6" class="pleft">
  <br />
  Signature:
  <br />
  <br />
  __________________________________<br />SUPERVISOR
  </td>
</tr>

</table>
</div>
</div>
<div class="modal-footer">
    <div class="text-right">
        <button class="print-link no-print" id="printLabel">
        Print this
        </button>
    </div>
</div>
</div>
    </div>
    <!-- /.modal-content -->
<!--</div>-->
<!-- /.modal-dialog -->

<script src = "{{ asset('public/administrator/controller-css-js/shipment.js') }}" ></script>
<script>
$("#printLabel").on('click', function () {
    $("#printContent").print({
        deferred: $.Deferred().done(function () {
            $('#modal-addEdit').modal('hide');
        })
    });
});
</script>