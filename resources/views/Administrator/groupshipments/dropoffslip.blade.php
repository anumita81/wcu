<script src="{{ asset('public/administrator/js/jQuery.print.js') }}"></script>

<!--modal open-->
<div class="modal-dialog">
    <div style="width: 895px;" class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        <div id="printContent" class="modal-body">
            <div style="width:875px; background:#fff; margin:0 auto; font-family:Arial, Helvetica, sans-serif; color:#000; font-size:12px; line-height:16px;">
                <div style="width:702px; background:#fff; margin:0 auto;">
                    <div style="width:100%; border-bottom:2px solid #58595b; overflow:hidden; margin-top:60px;">
                        <div style="width:195px; padding-bottom:5px; float:left;"><img src="{{ asset('public/administrator/img/logo.png') }}" /></div>
                        <div style="width:312px; padding-bottom:5px; float:left; text-align:center; font-size:22px; line-height:28px; text-transform:uppercase; font-weight:bold;">ITEM DROP OFF SLIP</div>
                        <div style="width:195px; padding-bottom:5px; float:right; text-align:right;"><strong>Shoptomydoor</strong><br>
                            12011 Westbrae Parkway, Houston<br>
                            77031, Texas<br>
                            United States<br>
                            CALL US: 1-888 315 9027<br>
                            Email: contact@shoptomydoor.com</div>
                    </div>
                    <div style="width:100%; border-bottom:2px solid #58595b; overflow:hidden; padding-top:20px; padding-bottom:30px;">
                        <h2 style="font-size:15px; text-align:center; margin:0 0 20px 0; padding:0;">Shipment Details</h2>
                        <div style=" float:left; width:50%;">
                            <h3 style="font-size:14px; text-align:left; margin:0 0 20px 0; padding:0;">From</h3>
                            <table width="95%" border="0" cellspacing="0" cellpadding="0" style="font-size:12px; line-height:16px;">
                                <tr>
                                    <td>Unit: </td>
                                    <td>{{$shipment->userUnit}}</td>
                                </tr>
                                <tr>
                                    <td>Country: </td>
                                    <td>{{$shipment->toCountryName}}</td>
                                </tr>
                                <tr>
                                    <td>State:</td>
                                    <td>{{$shipment->toStateName}}</td>
                                </tr>
                                <tr>
                                    <td>City: </td>
                                    <td>{{$shipment->toCityName}}</td>
                                </tr>
                                <tr>
                                    <td>Contact Name:</td>
                                    <td>{{$shipment->toName}}</td>
                                </tr>
                                <tr>
                                    <td>Address:</td>
                                    <td>{{$shipment->toAddress}}</td>
                                </tr>
                                <tr>
                                    <td>Email: </td>
                                    <td>{{$shipment->toEmail}}</td>
                                </tr>
                            </table>
                        </div>
                        <div style=" float:left; width:50%;">
                            <h3 style="font-size:14px; text-align:left; margin:0 0 20px 0; padding:0;">To</h3>
                            <table width="95%" border="0" cellspacing="0" cellpadding="0" style="font-size:12px; line-height:16px; margin-top:15px;">
                                <tr>
                                    <td>Country:</td>
                                    <td>{{$shipment->fromCountryName}}</td>
                                </tr>
                                <tr>
                                    <td>State: </td>
                                    <td>{{$shipment->fromStateName}}</td>
                                </tr>
                                <tr>
                                    <td>City:</td>
                                    <td>{{$shipment->fromCityName}}</td>
                                </tr>
                                <tr>
                                    <td>Contact Name:</td>
                                    <td>{{$shipment->fromName}}</td>
                                </tr>
                                <tr>
                                    <td>Address:</td>
                                    <td>{{$shipment->fromAddress}}</td>
                                </tr>
                                <tr>
                                    <td>Email: </td>
                                    <td>{{$shipment->fromEmail}}</td>
                                </tr>
                                <tr>
                                    <td>Phone: </td>
                                    <td>{{$shipment->fromPhone}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="width:100%; border-bottom:2px solid #58595b; overflow:hidden; padding-top:20px; padding-bottom:20px;">
                        <h2 style="font-size:15px; text-align:center; margin:0 0 10px 0; padding:0;">Storage Details</h2>
                        <p>Shipment is stored in our warehouse and will be shipped on the next available carrier once payment is made. Please note the
                            storage charges below are automatically added to your invoice if payment is not made within 1 days. Storage is charged at
                            $0.1/pound/day and for this shipment it is $50/day.</p>
                        <div style=" float:left; width:50%; text-align:center; margin:0 0 10px 0; padding:0;"> Drop Off Date: 05-31-2018 </div>
                        <div style=" float:left; width:50%; text-align:center; margin:0 0 10px 0; padding:0;"> Storage Charge Start on: 06-02-2018 </div>
                        <div style="clear:both;"><strong>Storage Charge if Payment is Made on:</strong></div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:12px; line-height:16px;">
                            <tr>
                                <td>06-18-2018: <strong>5.00 $</strong></td>
                                <td>06-19-2018: <strong>55.00 $</strong></td>
                                <td>06-20-2018: <strong>105.00 $</strong></td>
                                <td>06-21-2018: <strong>155.00 $</strong></td>
                            </tr>
                            <tr>
                                <td>06-22-2018: <strong>205.00 $</strong></td>
                                <td>06-23-2018: <strong>255.00 $</strong></td>
                                <td>06-24-2018: <strong>305.00 $</strong></td>
                                <td>06-25-2018: <strong>355.00 $</strong></td>
                            </tr>
                        </table>
                        <p><strong>NOTE</strong>: These storage charges are automated and can't be waved, and have to be paid even if you decide not to ship the
                            items again. After 30 days we may move all items below to a permanent storage and we will dispose it after 60 days.</p>
                        <h3 style="font-size:15px; text-align:center; margin:0; padding:0;">Accumulated Storage Charge: {{(new \App\Helpers\customhelper)->getCurrencySymbolFormat($shipment->storageCharge)}}</h3>
                    </div>
                    <div style="width:100%; overflow:hidden; padding-top:20px; padding-bottom:20px;">
                        <div style="clear:both; margin-bottom:5px;"><strong style="border-bottom:2px solid #58595b; padding-bottom:2px;">Package &amp; shipment details</strong></div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:12px; line-height:16px; margin-bottom:5px;">
                            <tr>
                                <td>Total number of Items: {{!empty($shipment['delivery']['totalPackage'])?$shipment['delivery']['totalPackage']:'0'}}</td>
                                <td>Delivery/Shipping Company: </td>
                                <td>Tracking Number: </td>
                            </tr>
                            <tr>
                                <td>Total Gross Weight: {{!empty($shipment['delivery']['totalWeight'])?$shipment['delivery']['totalWeight']:'0.00'}}lbs</td>
                                <td>Total Chargeable Weight: {{!empty($shipment['delivery']['totalChargeableWeight'])?$shipment['delivery']['totalChargeableWeight']:'0.00'}}lbs</td>
                                <td>Shipment Value:  {{!empty($shipment['delivery']['totalValue'])?(new \App\Helpers\customhelper)->getCurrencySymbolFormat($shipment['delivery']['totalValue']):'0.00'}}</td>
                            </tr>
                        </table>
                        <div style="clear:both; margin-bottom:5px;"><strong style="border-bottom:2px solid #58595b; padding-bottom:2px;">Our 100% delivery guarantee</strong></div>
                        <p style="padding:0; margin:0 0 10px 0;">Your total declared value for this shipment is 300.00 $. You 100% covered for 300.00 $ only in case of total damage or total loss.</p>
                        @if(!empty($shipment['delivery']['deliveries']))
                        @foreach($shipment['delivery']['deliveries'] as $delivery)
                        <div style="clear:both;"><strong>Delivery {{$delivery['displayDeliveryId']}}:</strong></div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:12px; line-height:16px; margin-bottom:10px;">
                            <tr>
                                <td>Store: {{$delivery['storeName']}}</td>
                                <td>Delivery Company: {{$delivery['deliveryCompany']}}</td>
                                <td>Tracking #: {{$delivery['tracking']}}</td>
                            </tr>
                            <tr>
                                <td>Delivered: {{\Carbon\Carbon::parse($delivery['deliveredOn'])->format('m-d-Y | H:i')}}</td>
                                <td>Weight: {{$delivery['weight']}} lbs Dimensions: {{$delivery['length']}}x{{$delivery['width']}}x{{$delivery['height']}} in</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                        <table width="100%" style="border-collapse:collapse; border:2px solid #039; font-size:12px; line-height:16px; text-align:center;" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="border-collapse:collapse; border:2px solid #039; font-weight:bold;">Items / products ordered</td>
                                <td style="border-collapse:collapse; border:2px solid #039; font-weight:bold;">Value Per Item</td>
                                <td style="border-collapse:collapse; border:2px solid #039; font-weight:bold;">Quantity</td>
                                <td style="border-collapse:collapse; border:2px solid #039; font-weight:bold;">Note</td>
                            </tr>
                            @foreach($delivery['packages'] as $package)
                            <tr>
                                <td style="border-collapse:collapse; border:2px solid #039;">{{$package['actualContent']}}</td>
                                <td style="border-collapse:collapse; border:2px solid #039;">{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat($package['value'])}}</td>
                                <td style="border-collapse:collapse; border:2px solid #039;">{{$package['quantity']}}</td>
                                <td style="border-collapse:collapse; border:2px solid #039;">{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat($package['value']*$package['quantity'])}}</td>
                            </tr>
                            @endforeach
                        </table>
                        <br/>
                        @endforeach
                        @endif
                        <div style="text-align:right; margin-top:15px;">
                            <p style="margin:0 0 4px 0; padding:0;"><strong>Subtotal</strong>: 2010.00 $</p>
                            <p style="margin:0 0 4px 0; padding:0;"><strong>Shipping cost</strong>: 2010.00 $</p>
                            <p style="margin:0 0 4px 0; padding:0;"><strong>Clearing, Duty & Insurance</strong>: 30.00 $</p>
                            <p style="margin:0 0 4px 0; padding:0;"><strong>Other charges</strong>: 12.68 $</p>
                            <p style="margin:0 0 10px 0; padding:0;"><strong>Value Added Tax (VAT)</strong>: 81.90 $</p>
                            <p style="margin:0; padding:0;"><strong>Total: 2134.58 $</strong></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <div class="text-right">
                <button id="printLabel" class="print-link no-print">
                    Print this
                </button>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
<script>
$("#printLabel").on('click', function () {
    $("#printContent").print({
        globalStyles: false,
        mediaPrint: false,
        append: null,
        prepend: null,
        title: null,
        doctype: '<!doctype html>',
        deferred: $.Deferred().done(function () {
            $('#modal-addEdit').modal('hide');
        })
    });
});
</script>