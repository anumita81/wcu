<ul class="shipingCost">
    <li>Shipping Cost: <strong>${{ $shipmentCharges['shippingCost'] }}</strong>(Shipping cost will change depending upon selection of shipping method)</li>
    <li>
        @if($shipmentCharges['duty']=='0.00' || $shipmentCharges['duty']=='0')
            Clearing,Port Handling:
        @else
            Clearing,Port Handling and duty: 
        @endif
        <strong>${{ $shipmentCharges['clearing']+$shipmentCharges['duty'] }}</strong>
    </li>
    <li>Insurance: <strong>${{ $shipmentCharges['insurance'] }}</strong></li>
    <li>Packaging and Inventory: <strong>${{ $shipmentCharges['packagingInventory'] }}</strong></li>
    <li>Other Charges: <strong>${{ $shipmentCharges['otherCharges'] }}</strong></li>

    <li>Discount: <strong>${{ $shipmentCharges['discount'] }}</strong></li>
    <li>Coupon Saving: <strong>$0</strong> </li>
    <li>Value Added Tax: <strong>${{ $shipmentCharges['tax'] }}</strong>(It will reflect once payment is done)</li>
</ul>