@if(!empty($otherCharges))
@foreach($otherCharges as $row)
@php $otherChargeAmount =   (new \App\Helpers\customhelper)->getCurrencySymbolFormat($row->otherChargeAmount); @endphp

<li id="othercharge{{$row->id}}" class="list-group-item">
    <div class="bulletTxt"><span class="bullet"><img src="{{ asset('public/administrator/img/redsqrBullet.png') }}"></span>{{$row->otherChargeName}} {{!empty($row->notes)?'-'.$row->notes:''}}  - {{$otherChargeAmount}}<br>
        Added by: {{$row->createdBy}}</div>
    <span class="deleteBtn"><a data-id="{{$row->id}}" data-shipmentid="{{$row->shipmentId}}" data-deliveryid="{{ $row->deliveryId }}" class="deletecharge" href="javascript:void(0);"><img src="{{ asset('public/administrator/img/deleteIconRedSm.png') }}"></a></span>
</li>
@endforeach
@endif