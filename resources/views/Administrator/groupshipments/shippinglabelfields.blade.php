<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Confirm Shipping Contents</h4>
        </div>
        {{ Form::open(array('url' => route('shipmentshippinglabel',['id'=>$shipmentId,'page'=>$page]), 'name' => 'frmshippinglabel', 'id' => 'frmshippinglabel', 'method' => 'post')) }}
        <div class="modal-body">
        	<div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Contents</label>
                        <select class="form-control customSelect" id="sel1" name="contents">
                            <option value="Solid" selected="selected">Solid</option>
					      	<option value="Liquid">Liquid</option>
					      	<option value="Fragile">Fragile</option>
					      	<option value="High Value">High Value</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Type</label>
                        <select class="form-control customSelect" id="sel1" name="type">
                            <option value="Red">Red</option>
					      	<option value="Yellow">Yellow</option>
					      	<option value="Green">Green</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
			    <div class="text-right">
			        <button type="submit" class="btn btn-success printShippingLabel"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Print Shipping Label</button>
			    </div>
			</div>
        </div>
        {{ Form::close() }}
    </div>
</div>