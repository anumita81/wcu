@foreach($shipment['deliveries'][$id]['packages'] as $package)
@php
if($package['itemType']=='N')
$packageType = 'Normal';
else if($package['itemType']=='H')
$packageType = 'Hazmat';
else
$packageType = 'Secured';
@endphp
<tr id="package{{$package['id']}}">
    <td valign="top" align="left">
        <input type="hidden" name="packageId" value="{{$package['id']}}" />

        <div class="value_label">{{!empty($package['storeName'])?$package['storeName']:'Not Listed'}}</div>
        <div class="value_field" style="display: none">
            <select name="siteCategoryId" required="" data-name="Store" class="form-control packagestore input-lg customSelectBlack" id="storeId">
                <option value="0">Not Listed</option>
                @foreach($storeList as $store)
                <option value="{{$store->id}}" {{$store->id==$package['storeId']?'selected="selected"':''}}>{{$store->storeName}}</option>
                @endforeach
            </select>
        </div>
    </td>
    <td valign="top" align="left">
        <input type="hidden" name="packageId" value="{{$package['id']}}" />
        <div class="value_label">{{!empty($package['categoryName'])?$package['categoryName']:'Not Listed'}}</div>
        <div class="value_field" style="display: none">
            <select name="siteCategoryId" required="" data-name="Category" class="form-control packagecategory input-lg customSelectBlack" id="siteCategoryId">
                <option value="0">Not Listed</option>
                @foreach($categoryList as $categoryId => $categoryName)
                <option value="{{$categoryId}}" {{$categoryId==$package['siteCategoryId']?'selected="selected"':''}}>{{$categoryName}}</option>
                @endforeach
            </select>
        </div>
    </td>
    <td valign="top" align="left">
        <div class="value_label">{{!empty($package['subcategoryName'])?$package['subcategoryName']:'Not Listed'}}</div>
        <div class="value_field" style="display: none">
            <select name="siteSubCategoryId" required="" data-name="Sub Category" class="form-control packagesubcategory input-lg customSelectBlack" id="siteSubCategoryId">
                <option value="0">Not Listed</option>
            </select>
            <input type="hidden"  name="packagesubcategoryId" class="packagesubcategoryId" value="{{$package['siteSubCategoryId']}}" />
        </div>
    </td>
    <td valign="top" align="left">
        <div class="value_label">{{!empty($package['productName'])?$package['productName']:'Not Listed'}}</div>
        <div class="value_field" style="display: none">
            <select name="siteProductId" required="" data-name="Product" class="form-control packageproduct input-lg customSelectBlack" id="siteProductId">
                <option value="0">Not Listed</option>
            </select>
            <input type="hidden" name="packageproductId" class="packageproductId" value="{{$package['siteProductId']}}" />
        </div>
    </td> 
    <td valign="top" align="left">
        <div class="value_label">{{$package['itemName']}}</div>
        <div class="value_field" style="display: none"><input name="itemName" id="itemName" required="" data-name="Name of Item" class="form-control input-lg customInputBlack" value="{{$package['itemName']}}" type="text"></div>
    </td>
    <td valign="top" align="left">
        <div class="value_label">{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat($package['itemMinPrice'])}}</div>
        <div class="value_field" style="display: none"><input name="itemMinPrice" required="" data-name="Value Per Item" class="form-control productvalOtherMin format input-lg customInputBlack" value="{{$package['itemMinPrice']}}" type="text"></div>
    </td>
    <td valign="top" align="left">
        <div class="value_label">{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat($package['itemPrice'])}}</div>
        <div class="value_field" style="display: none"><input name="itemPrice" required="" data-name="Value Per Item" class="form-control productvalOther format input-lg customInputBlack" value="{{$package['itemPrice']}}" type="text"></div>
    </td>
    <td valign="top" align="left">
        <div class="value_label">{{$package['itemQuantity']}}</div>
        <div class="value_field" style="display: none"><input name="itemQuantity" required="" data-name="Quantity" class="form-control quantityOther input-lg customInputBlack" value="{{$package['itemQuantity']}}" type="number" min="1"></div>
    </td>
    <td valign="top" align="left">
        <div class="value_label">{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat($package['itemTotalCost'])}}</div>
        <div class="value_field" style="display: none"><input name="itemTotalCost" readonly="readonly" class="form-control format producttotal input-lg customInputBlack" value="{{$package['itemTotalCost']}}" type="text"></div>
    </td>
    <td valign="top" align="center">
        <div class="value_label">
            @if($package['itemReturn'] == '1')
                <a href="javascript:void(0)" class="color-theme actionIcons"><i class="fa fa-check-circle" aria-hidden="true"></i></a>
            @else
                <a href="javascript:void(0)" class="color-theme-2 actionIcons"><i class="fa fa-times" aria-hidden="true"></i></a>
            @endif    
        </div>
    </td>
    <td class="text-center buttMrg" valign="top" align="left">
        @if($package['snapshotOpt'] == 'Y')
        <a href="javascript:void(0);" onclick="uploadSnapshot({{ $shipment->id }},{{$package['id']}},{{$delivery['id']}})"  title="Upload or View Snapshot"><img src="{{ asset('public/administrator/img/uploadIconSm.png') }}"></a>
        @else
        N/A
        @endif
    </td>
    <td class="text-center buttMrg" valign="top" align="left">
        @if($package['itemDiscountedInvoiceFile']!="")
        <a data-target="tooltip" title="download" href="{{ url('administrator/shipments/downloaddiscountedinvoice/'.$package['id']) }}"><i class="fa fa-download" aria-hidden="true" style="font-size: 25px"></i></a>
        @else
        <span>N/A</span>
        @endif
    </td>

    <td class="text-center buttMrg" valign="top" align="left">
        <a href="javascript:void(0);" class="editPackage"><img data-target="tooltip" class="save-item" title="Click to Edit" src="{{ asset('public/administrator/img/editIconSm.png') }}"></a> 
        <a href="javascript:void(0);" style="display:none;" class="savePackage"><img data-target="tooltip" height="18" class="save-item" title="Click to Edit" src="{{ asset('public/administrator/img/download-button.png') }}"></a> 
        <a href="javascript:void(0);" data-toggle="confirmation" data-id="{{$package['id']}}" class="removePackage"><img data-target="tooltip" title="Remove Item / href="javascript:void(0);" dataProduct" src="{{ asset('public/administrator/img/deleteIconRedSm.png') }}"></a>
    </td>
</tr>
@endforeach