<div class="col-sm-6 col-xs-6">
    <h5 class="skyTxt">From</h5>
    <p>Unit: <strong>{{$shipment->unit}}</strong></p>
</div>
<div class="col-sm-6 col-xs-6">
    <h5 class="skyTxt">To</h5>
    <p>Name: <strong>{{$shipment->toName}}</strong></p>
</div>
<div class="col-sm-6 col-xs-6">
    <p>Country: <strong>{{$shipment->fromCountryName}}</strong></p>
</div>
<div class="col-sm-6 col-xs-6">
    <p>Country:<strong>{{$shipment->toCountryName}}</strong></p>
</div>
<div class="col-sm-6 col-xs-6">
    <p>State:<strong>{{!empty($shipment->fromStateName)?$shipment->fromStateName:'N/A'}}</strong></p>
</div>
<div class="col-sm-6 col-xs-6">
    <p>State: <strong>{{!empty($shipment->toStateName)?$shipment->toStateName:'N/A'}}</strong></p>
</div>
<div class="col-sm-6 col-xs-6">
    <p>City: <strong>{{!empty($shipment->fromCityName)?$shipment->fromCityName:'N/A'}}</strong></p>
</div>
<div class="col-sm-6 col-xs-6">
    <p>City: <strong>{{!empty($shipment->toCityName)?$shipment->toCityName:'N/A'}}</strong></p>
</div>
<div class="col-sm-6 col-xs-6">
    <p>Address: <strong>{{!empty($shipment->fromAddress)?$shipment->fromAddress:'N/A'}}</strong></p>
</div>
<div class="col-sm-6 col-xs-6">
    <p>Address: <strong>{{!empty($shipment->toAddress)?$shipment->toAddress:'N/A'}}</strong></p>
</div>
<div class="col-sm-6 col-xs-6">
    <p>Email: <strong>{{!empty($shipment->fromEmail)?$shipment->fromEmail:'N/A'}}</strong></p>
</div>
<div class="col-sm-6 col-xs-6">
    <p>Email: <strong>{{!empty($shipment->toEmail)?$shipment->toEmail:'N/A'}}</strong></p>
</div>
<div class="col-sm-6 col-xs-6">
    <p>Phone: <strong>{{!empty($shipment->fromPhone)?$shipment->fromPhone:'N/A'}}</strong></p>
</div>
<div class="col-sm-6 col-xs-6">
    <p>Phone: <strong>{{!empty($shipment->toPhone)?$shipment->toPhone:'N/A'}}</strong></p>
</div>