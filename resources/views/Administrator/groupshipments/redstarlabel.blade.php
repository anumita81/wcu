<script src="{{ asset('public/administrator/js/jQuery.print.js') }}"></script>

<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title"></h4>
    </div>
    <div class="modal-body"> @foreach($packageDetailsData as $key => $pck)
      <div style="border:1px solid #B0AFBE; padding:2px; width:100%; height:774px; margin-bottom:10px;" id="printContent_{{$pck->id}}">
        <div style="width:98%; margin:0 auto; height:746px;">
        <table width="95%" align="center" style="height: 650px;">
          <tr style="height:90px;">
            <td align="left" width="20%"><img src="{{ asset('public/administrator/img/logoSTMD.jpg') }}" width="150" height="80"></td>
            <td width="80%"><table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="center" nowrap><font size="{$font_size2}"><b>SENDER : American AirSea Cargo Ltd.</b></font></td>
                </tr>
                <tr>
                  <td align="center" nowrap><font size="{$font_size2}"><b>ACCOUNT #: 10100410</b></font></td>
                </tr>
                <tr>
                  <td align="center" nowrap><font size="{$font_size2}"><b>COMPANY #: 07011856459</b></font></td>
                </tr>
                <tr>
                  <td align="center" nowrap><font size="{$font_size2}"><b>{{ date('d-m-Y') }}</b></font></td>
                </tr>
              </table></td>
          </tr>
<!--          <tr>
            <td colspan="2"><hr /></td>
          </tr>-->
          <tr>
              <td align="center" colspan="2" style="padding-bottom: 8px;">
                <table width="100%" cellspacing="0" cellpadding="3">
                    <tr>
                        <td nowrap="nowrap" align="center" ><font size="{$font_size}"><b>Order# : {{ substr($orderId,(strpos($orderId,'-')+1)) }}<b></font></td>
                        <td nowrap="nowrap" align="center" ><font size="{$font_size}"><b>Shipment# : {{$shipmentData->id}}<b></font></td>
                    </tr>
                </table>
              </td>
          </tr>
          <tr valign="top">
              <td align="center" width="100%" colspan="2"> 
                @if($orderId != '') 
<!--                    <img src="data:image/png;base64,{{DNS1D::getBarcodePNG("$orderId", 'C128')}}">-->
                    <img src="data:image/png;base64,{{DNS1D::getBarcodePNG(substr($orderId,(strpos($orderId,'-')+1)), 'C39')}}" alt="barcode" style="display: block; width: 100%; height: 100px; margin-bottom: 8px; max-width: inherit; />
                @endif
               
            </td>
          </tr>
          <tr>
              <td align="center" colspan="2">
                <table width="100%" cellspacing="0" cellpadding="3">
                    <tr valign="top">
                        <td nowrap="nowrap" align="center" colspan="2"><font size="{$h_size}"><b>Package: {{$key+1}} of {{$packageCount}}</b></font></td>
                        <td nowrap="nowrap" align="center" ><font size="{$font_size}"><b># of Items: {{$noOfItem}}</b></font></td>
                        <td nowrap="nowrap" align="center" ><font size="{$font_size}"><b>Gross Weight: @if(isset($pck->chargeableWeight)) {{ round($pck->chargeableWeight/2.2, 2) }} kg @endif</b></font></td>         
                    </tr>
                    

                </table>
              </td>
          </tr>
          <tr valign="top">
            <td>&nbsp;</td>
          </tr>
          <tr valign="top">
            <td align="left" width="50%" nowrap><font size="{$font_size2}"><b>Waybill/Tracking #:</b></font></td>
            <td>
                <table width="100%" cellspacing="0" cellpadding="1">
                    <tr>
                        <td width="40%" align="left"><font size="{$font_size2}"><b>ORIGIN:</b></font></td>
                        <td width="60%" align="left"><font size="{$font_size2}"><b>DESTINATION:</b></font></td>
                    </tr>
                </table>
            </td>
          </tr>
          <tr valign="top">
            <td align="left" width="20%" nowrap><font size="{$font_size2}"><b>CONTENTS:</b></font></td>
          </tr>
          <tr>
            <td colspan="2" nowrap="nowrap" align="center" ><hr /></td>
          </tr>
          <tr>
              <td align="center" colspan="2" style="overflow:hidden; width:{$label_width}px; border: 1px solid #000; padding: 4px;"><font size="{$font_size2}"><b>NOTE: Verify that there is no damage or cut on the package before accepting. <br/>Any issues, first call 0700 800 8000</b></font></td>
          </tr>
          <tr>
            <td colspan="2" nowrap="nowrap" align="center" ><hr /></td>
          </tr>
          <tr>
            <td align="center" colspan="2" style="padding-left:70px;"><table width="100%">
                <tr>
                  <td width="15%"><b>TO:</b></td>
                  <td align="left" width="85%"><font size="{$font_size}">{{ $deliveryAddress['toName'] }}</font></td>
                </tr>
                <tr>
                  <td width="15%">&nbsp;</td>
                  <td align="left" width="85%"><font size="{$font_size}">{{$deliveryAddress['toAddress']}}</font></td>
                </tr>
                <tr>
                  <td width="15%">&nbsp;</td>
                  <td align="left" width="85%"><font size="{$font_size}"><b>{{$deliveryAddress['toCityName']}}&nbsp;{{$deliveryAddress['toStateName']}}&nbsp;{{$deliveryAddress['toZipCode']}}&nbsp;{{$deliveryAddress['toCountryName']}}</b></font></td>
                </tr>                
                @if(!empty($deliveryAddress['toPhone']))
                <tr>
                  <td width="15%">&nbsp;</td>
                  <td align="left" width="85%"><font size="{$font_size}">{{$deliveryAddress['toPhone']}} @if(isset($deliveryAddress['toAlternatePhone'])) {{$deliveryAddress['toAlternatePhone']}} @endif</font></td>
                </tr>
                @endif
              </table></td>
          </tr>
          <tr valign="top">
            <td align="center" colspan="2"><table width="100%">
                <tr style="height:50px;" valign="top">
                  <td nowrap="nowrap" width="35%" align="center"><font size="{$font_size2}"><b><u>RECEIVER PHONE#</u></b></font></td>
                  <td nowrap="nowrap" width="35%" align="center"><font size="{$font_size2}"><b><u>RECEIVER NAME</u></b></font></td>
                  <td nowrap="nowrap" width="35%" align="center"><font size="{$font_size2}"><b><u>RECEIVER SIGN</u></b></font></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td colspan="2" nowrap="nowrap" align="center" ><hr /></td>
          </tr>
          <tr>
            <td colspan="2" nowrap="nowrap" align="center" ><table width="100%" cellspacing="0" cellpadding="1">
                <tr>
                  <td colspan="3" align="center"><font size="{$font_size2}"><b>OTHER DETAILS (Tick one box)</b></font></td>
                </tr>
                <tr>
                    <td align="center" nowrap="nowrap"><font size="{$font_size}"><b>Envelope&nbsp;<span style="font-size: 20px">&#9633;</span></b></font></td>
                  <td align="center" nowrap="nowrap"><font size="{$font_size}"><b>RSE Box&nbsp;<span style="font-size: 20px">&#9633;</span></b></font></td>
                  <td align="center" nowrap="nowrap"><font size="{$font_size}"><b>Others&nbsp;<span style="font-size: 20px">&#9633;</span></b></font></td>
                </tr>
                <tr>
                  <td colspan="3" align="left"><font size="{$font_size2}"><b>DELIVERY INSTRUCTIONS: </b></font></td>
                </tr>
                <tr>
                  <td align="center" nowrap="nowrap"><font size="{$font_size}"><b>Hold For Collection&nbsp;<span style="font-size: 20px">&#9633;</span></b></font></td>
                  <td align="center" nowrap="nowrap"><font size="{$font_size}"><b>Weekday&nbsp;<span style="font-size: 20px">&#9633;</span></b></font></td>
                  <td align="center" nowrap="nowrap"><font size="{$font_size}"><b>Weekend&nbsp;<span style="font-size: 20px">&#9633;</span></b></font></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td colspan="2" nowrap="nowrap" align="center" ><hr /></td>
          </tr>
          <tr valign="top">
            <td align="center" colspan="2"><table width="100%">
                <tr style="height:50px;" valign="top">
                  <td nowrap="nowrap" width="50%" align="center"><font size="{$font_size2}"><b><u>RED STAR SIGNATURE</u></b></font></td>
                  <td nowrap="nowrap" width="50%" align="center"><font size="{$font_size2}"><b><u>AASC SIGNATURE</u></b></font></td>
                </tr>
              </table></td>
          </tr>
        </table>
      </div>
    </div>
    <div class="modal-footer">
        <div class="text-right">
            <button class="print-link no-print" id="printLabel" onclick="printLabel({{$pck->id}})">
                Print this
            </button>
        </div>
    </div>
    @endforeach
  </div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
<script>
function printLabel(packageId){
    $("#printContent_"+packageId).print({
        deferred: $.Deferred().done(function () {
            //$('#modal-addEdit').modal('hide');
        })
    });
}
</script>
