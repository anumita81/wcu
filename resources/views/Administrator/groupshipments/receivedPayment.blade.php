<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        {{Form::open(array('url' => "administrator/groupshipments/savePaymentDetails/$id/0", 'name' => 'paymentFrm', 'id' => 'paymentFrm', 'method' => 'post', 'autocomplete' => 'off','onsubmit' => 'return checkValid(this);'))}} 
        <div class="modal-body">            
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                       <label>Payment Method <span class="text-red">*</span></label>
                       <select name="paymentMethodId" id="paymentMethodId" required="" class="customSelect form-control">
                            <option value="">Select</option>
                            @foreach($paymentMethod as $row)
                            <option value="{{$row->id}}">{{$row->paymentMethod}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                       <label>Shipping Method <span class="text-red">*</span></label>
                       <select name="shippingMethodId" id="shippingMethodId" required="" class="customSelect form-control">
                            <option value="">Select</option>
                            @foreach($shippingMethod as $row)
                            <option value="{{$row->shippingid}}">{{$row->shipping}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                         <label>Paid Amount <span class="text-red">*</span></label> 
                         <input id="paidAmount" name="paidAmount" required="" class="form-control format" placeholder="Enter Paid Amount" type="number" value="" min="0.01">
                    </div>
                    <div class="form-group">
                         <label>Paid On <span class="text-red">*</span></label> 
                         <input id="paidOn" name="paidOn" required="" class="form-control datepicker" placeholder="Enter Payment Date" type="text" value="{{date('Y-m-d')}}">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Save</button>
                </div>
            </div>
        </div>
        {{Form:: close()}}
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->



<script>
    $(function () {
        $("#paymentFrm").validate();
    });
    $(function () {

            $("body").on('click','.datepicker',function(){
                $(this).datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
                });
            });
            $('.datepicker').datepicker( "setDate", "today" );
        });
</script>