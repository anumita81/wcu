<script src = "{{ asset('public/administrator/controller-css-js/groupshipment.js') }}" ></script>

<div class="modal-dialog lrgWidth">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4>Add New Delivery</h4>
        </div>
        <div class="modal-body">
            @if(!empty($userSettings))
            <div id="customerSettingsBlock">
                <div class="box-header">
                    <h3 class="box-title text-info">Shipment Settings</h3>
                </div>
                
                <div class="box-body">
                    <div class="form-group setting-item">
                        
                       
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                @if(isset($userSettings['remove_shoe_box']))
                                @if($userSettings['remove_shoe_box'] == 1)                               
                                <label id="remove_shoe_box" class='text-red'>Yes</label><label style="margin-left: 10px">Always remove shoe boxes. (Select Yes to save money)</label>
                                @else
                                <label id="remove_shoe_box">No</label><label style="margin-left: 10px">Always remove shoe boxes. (Select Yes to save money)</label>
                                @endif
                                @else
                                <label id="remove_shoe_box">No</label><label style="margin-left: 10px">Always remove shoe boxes. (Select Yes to save money)</label>
                                @endif
                            </div> 
                        </div>
                         
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                @if(isset($userSettings['original_box']))
                                @if($userSettings['original_box'] == 1)
                                <label id="original_box" class='text-red'>Yes</label><label style="margin-left: 10px">Ship all my items in their original boxes (Select No to save money)</label>
                                @else
                                <label id="original_box">No</label><label style="margin-left: 10px">Ship all my items in their original boxes (Select No to save money)</label>
                                @endif
                                @else
                                <label id="original_box">No</label><label style="margin-left: 10px">Ship all my items in their original boxes (Select No to save money)</label>                                
                                @endif
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                @if(isset($userSettings['quick_shipout']))
                                    @if($userSettings['quick_shipout'] == 1)
                                        <label id="quick_shipout" class='text-red'>Yes</label><label style="margin-left: 10px">Quick ship my items. (For subscribed customers shipping via DHL Priority)</label>
                                    @else
                                        <label id="quick_shipout">No</label><label style="margin-left: 10px">Quick ship my items. (For subscribed customers shipping via DHL Priority)</label>
                                    @endif
                                @else
                                    <label id="quick_shipout">No</label><label style="margin-left: 10px">Quick ship my items. (For subscribed customers shipping via DHL Priority)</label>                                
                                @endif
                           </div> 
                         </div>
                             
                         <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <label style="margin-right: 10px">Special Handling Instructions : <span id="special_instruction">{{(isset($userSettings['special_instruction']) && !empty($userSettings['special_instruction'])) ? $userSettings['special_instruction'] : 'N/A'}}</span></label>
                           </div> 
                         </div>
                        </div>
                    </div>
            </div>
            @endif
            {{ Form::open(array('url' => 'administrator/groupshipments/addeditdelivery/'.$shipmentId.'/0', 'name' => 'addEditDeliveryFrm', 'id' => 'addEditDeliveryFrm', 'method' => 'post', 'autocomplete' => 'off','onsubmit'=>'return validateDeliveryForm();')) }}               
            <div id="customerSettingsBlock">
                <div class="box-header">
                    <h3 class="box-title text-info">Delivery Details</h3>
                </div>
                <div class="row m-t-15">
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label>Unit Number<span class="text-red">*</span></label>
                            <input id="userUnitNumber" name="userUnitNumber" class="form-control" required="" placeholder="Enter Unit" type="text">
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <h4 class="form-group m-t-20 text-success" id="userName"></h4>
                    </div>
                </div>
                <div class="row m-t-15">
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label>Delivery Company <span class="text-red">*</span></label>
                            <select name="deliveryCompanyId" id="deliveryCompanyId" class="customSelect form-control" required="">
                                <option value="">Select Delivery Company</option>
                                @foreach($deliveryCompanyList as $deliveryCompany)
                                <option value="{{$deliveryCompany->id}}">{{$deliveryCompany->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label>Delivered <span class="text-red">*</span></label>
                            <input id="delivered" name="delivered" class="form-control datepicker" readonly="" required="" placeholder="Select Date" type="text" value="{{ date('Y-m-d') }}">
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label>Tracking 1<span class="text-red">*</span></label>
                            <input id="trackingNumber" name="trackingNumber" class="form-control" required="" placeholder="Enter Tracking ID" type="text">
                        </div>
                    </div>
                </div>
                <div class="row m-t-15">
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label>Tracking 2</label>
                            <input id="trackingNumber2" name="trackingNumber2" class="form-control" placeholder="Enter Tracking ID" type="text">
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label>Delivery Note</label>
                            <input id="trackingNumber2" name="note" class="form-control" placeholder="Enter Delivery Note" type="text">
                        </div>
                    </div>
                </div>
            </div>
            <div id="packageDiv">
                <div class="box-header">
                    <h3 class="box-title text-info">Package Details</h3>
                </div>
                <div id="modal_first_row" class="deliveryItem">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label>Store</label>
                            <select name="shipment[storeId][]" id="storeId" class="form-control input-lg customSelect2 storeId" >
                                <option value="0">Not Listed</option>
                                @foreach($storeList as $store)
                                <option value="{{$store->id}}">{{$store->storeName}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Category</label>
                            <select name="shipment[siteCategoryId][]" class="form-control input-lg customSelect2 siteCategoryId" >
                                <option value="0">Not Listed</option>
                                
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Subcategory</label>
                            <select class="form-control input-lg customSelect2 subCategory" name="shipment[siteSubCategoryId][]" >
                                <option value="0">Not Listed</option>
                            </select>
                            <input type="hidden" id="subcatPage" value="getsubcategory">
                        </div>
                        <div class="form-group col-md-3">
                            <label>Product</label>
                            <select class="form-control input-lg customSelect2 siteProduct" name="shipment[siteProductId][]" required="">
                                <option value="0">Not Listed</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="sel1">Item Name<span class="text-red">*</span></label>
                            <input name="shipment[item][]" required="" class="form-control input-lg" type="text">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="sel1">Min Value<span class="text-red">*</span></label>
                            <input  name="shipment[minValue][]" class="form-control input-lg product-minval-addDelivery format" placeholder="" type="number" min="0.01" required="">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="sel1">Max Value<span class="text-red">*</span></label>
                            <input  name="shipment[value][]" class="form-control input-lg product-val-addDelivery format" placeholder="" type="number" min="0.01" required="">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="sel1">Qty<span class="text-red">*</span></label>
                            <input name="shipment[quantity][]" class="form-control input-lg qtyAddDelivery" placeholder="" type="number" min="1" required="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="sel1">Total</label>
                            <input name="shipment[total][]" class="form-control input-lg product-total-addDelivery" placeholder="" type="text" readonly="">
                        </div>
                    </div>
                </div>
            </div>    
            <div class="form-row">
                <div class="form-group col-md-12 text-center">
                    <a class="btn btn-default custumButt btnGreen m-r-15 m-t-20 add-more-modal">Add More</a>
                </div>
            </div>
            <h4 class="box-title">Dimensions</h4>
            <div class="row m-t-15">
                <div class="col-md-12 col-xs-12">
                    <div id="foundProductDetails">

                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="form-group">
                        <label>Length <span class="text-red">*</span></label>
                        <input id="lenth" name="length" class="form-control input-lg format" placeholder="Enter Length" type="number" min="0.01" required="">
                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="form-group">
                        <label>Width <span class="text-red">*</span></label>
                        <input id="width" name="width" class="form-control input-lg format" placeholder="Enter Width" type="number" min="0.01" required="">
                    </div>
                </div>

                <div class="col-md-3 col-xs-12">
                    <div class="form-group">
                        <label>Height <span class="text-red">*</span></label>
                        <input id="height" name="height" class="form-control input-lg format" placeholder="Enter Height" type="number" min="0.01" required="">
                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="form-group">
                        <label>Weight <span class="text-red">*</span></label>
                        <input id="weight" name="weight" class="form-control input-lg format" placeholder="Enter Weight" type="number" min="0.01" required="">
                    </div>
                </div>
            </div>
            <div class="clearfix m-b-25">
                <br>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <span style="font-size: 15px"><strong class="text-red">NOTE:</strong> You must select extra charge and add for all (1) <strong>Lithium</strong> (2) <strong>Hazmat</strong> (3) <strong>Fragile</strong> (4) <strong>Liquid</strong> (5) <strong>TV</strong> Shipments. See below for a full list of extra charges</span>
                        </div>
                    </div>
                </div>
                <h5>Add extra charge</h5>
                @if(!empty($otherChargesList))
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="otherChargeId">Other charges:</label>
                            <select name="otherChargeId"  id="otherChargeIdNewdeliv" class="form-control oCharge input-lg customSelectBlack">
                                <option value="">Select</option>
                                @foreach($otherChargesList as $otherCharge)
                                <option value="{{$otherCharge->id}}">{{$otherCharge->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label for="notes">Notes:</label>
                            <input name="notes" id="chargeNotesNewdeliv" class="form-control oCharge input-lg customInputBlack" type="text" />
                        </div>
                        <div class="col-sm-2 text-left p-t-20">
                            <a id="otherChrgbtn" onclick="javascript:addNewChargeNewDelivery({{ $shipmentId }});" class="btn btn-default custumButt btnGreen">Add</a>
                        </div>
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-sm-8 col-md-6 customList">
                        <ul id="otherChargeList"  class="list-group">
                            @if(!empty($othercharges))
                            @foreach($othercharges as $row)
                            @php $otherChargeAmount =   (new \App\Helpers\customhelper)->getCurrencySymbolFormat($row->otherChargeAmount); @endphp
                            <li id="othercharge{{$row->id}}" class="list-group-item">
                                <div class="bulletTxt"><span class="bullet"><img src="{{ asset('public/administrator/img/redsqrBullet.png') }}"></span>{{$row->otherChargeName}} {{!empty($row->notes)?'-'.$row->notes:''}}  - {{$otherChargeAmount}}<br>
                                    Added by: {{$row->createdBy}}</div>
                                <span class="deleteBtn"><a data-id="{{$row->id}}" class="deletenewcharge color-theme-2 actionIcons" href="javascript:void(0);" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a></span>
                            </li>
                            @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <h5>Manual Charges Details</h5>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="otherChargeName">Manual charge:</label>
                            <input name="otherChargeName" id="otherChargeNameNewdeliv" class="form-control mCharge input-lg customInputBlack" type="text">
                        </div>
                        <div class="col-sm-3">
                            <label for="otherChargeAmount">Amount({{(new \App\Helpers\customhelper)->getCurrencySymbolCode() }}):</label>
                            <input name="otherChargeAmount" id="otherChargeAmountNewdeliv" class="form-control mCharge format input-lg customInputBlack" type="text">
                        </div>
                        <div class="col-sm-3">
                            <label for="otherNotes">Notes:</label>
                            <input name="notes" id="otherNotesNewdeliv" class="form-control mCharge input-lg customInputBlack" type="text">
                        </div>
                        <div class="col-sm-2 text-left p-t-20">
                            <a  id="manualChrgBtn"  onclick="javascript:addNewManualChargeNewDelivery({{ $shipmentId }});" class="btn btn-default custumButt btnGreen">Add</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-md-6 customList">
                        <ul id="manualChargeList"  class="list-group">
                            @if(!empty($manualcharges))
                            @foreach($manualcharges as $row)
                            @php $otherChargeAmount =   (new \App\Helpers\customhelper)->getCurrencySymbolFormat($row->otherChargeAmount); @endphp
                            <li id="othercharge{{$row->id}}" class="list-group-item">
                                <div class="bulletTxt"><span class="bullet"><img src="{{ asset('public/administrator/img/redsqrBullet.png') }}"></span>{{$row->otherChargeName}} {{!empty($row->notes)?'-'.$row->notes:''}}  - {{$otherChargeAmount}}<br>
                                    Added by: {{$row->createdBy}}</div>

                                <span class="deleteBtn"><a data-id="{{$row->id}}" class="deletenewcharge color-theme-2 actionIcons" href="javascript:void(0);" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a></span>

                            </li>
                            @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <span><strong>NOTE:</strong> Extra charges apply for TV's, Hazmat Items, Very Fragile Packages, Items that need to be crated, Liquids Items, Counting of more than 50 items, Customers Using Wrong Unit Numbers or Sharing Unit Numbers, Customers with No Unit Number on packages, Motor Cycles Crating, Customers that want copies of their invoices, Customers shipping car batteries, Any single item valued at more than $2,500, Shipping of used engines by air.</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12 text-center">
                    <button class="btn btn-default custumButt solidBtn m-r-15 m-t-20">Submit</button>
                </div>
            </div>
        </div>
        {{Form:: close()}}
        <div class="clearfix"></div>
    </div>
    <script>
        $(function () {
            $("body").on('click','.datepicker',function(){
                $(this).datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
                });
            });
            $('.datepicker').datepicker( "setDate", "today" );
        });


    </script>
</div>
