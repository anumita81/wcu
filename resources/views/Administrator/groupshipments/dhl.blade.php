<script src="{{ asset('public/administrator/js/jQuery.print.js') }}"></script>
<script src = "{{ asset('public/administrator/controller-css-js/dhl.js') }}" ></script>
<div id="wait-modal" style="display:none;" class="loaderMiddle"></div>
        <input type="hidden" name="base_path_offers" value="{{ url('/').'/administrator/offers/' }}">
        <input name="_token" type="hidden" value="{{ csrf_token() }}">
        <input name="fromCountryIDDefault" id="fromCountryIDDefault" type="hidden" value="{{$shipmentData['fromCountry']}}">
        <input name="toCountryIDDefault" id="toCountryIDDefault" type="hidden" value="{{$shipmentData['toCountry']}}">
        <input name="fromStateIDDefault" id="fromStateIDDefault" type="hidden" value="{{$shipmentData['fromState']}}">
        <input name="fromCityIDDefault" id="fromCityIDDefault" type="hidden" value="{{$shipmentData['fromCity']}}">
        <input name="toStateIDDefault" id="toStateIDDefault" type="hidden" value="{{$shipmentData['toState']}}">
        <input name="toCityIDDefault" id="toCityIDDefault" type="hidden" value="{{$shipmentData['toCity']}}">
        <input name="shipmentId" id="shipmentId" type="hidden" value="{{$shipmentData['id']}}">
        <div class="modal-dialog modal-lg">
          <div class="modal-content" style="height:550px; overflow:auto;">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Prepare DHL Shipment Label</h4>
            </div>
            <div class="modal-body longFrm">
              <!--<form id="addFrm" role="form" method="post" action="couponListing.html">-->
              {{ Form::open(array('url' => url('administrator/shipments/dhlpost'),  'name' => 'dhlpost', 'id' => 'dhlpost', 'method' => 'post')) }}
              <input type="hidden" name="base_path_dhlci" value="{{ url('/').'/administrator/shipments/dhlpost/' }}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                          <label>Dhl Account Number <span class="text-red">*</span></label>
                          <select id="dhlaccountnumber" required="" class="form-control customSelect">
                              <option value="">Select</option>
                              @foreach($dhlAccountNumber as $eachAccount)
                              <option value="{{ $eachAccount->id }}" data-description="{{ $eachAccount->dhlAccountDescription }}">{{ $eachAccount->dhlAccountNo }}</option>
                              @endforeach
                          </select>
                        </div>
                    </div>
                </div>
                <div id="dhlform" class="disableblock">
                    <div class="row">
                      <div class="col-md-6"> <strong> FROM (Must be US-located address)</strong> </div>
                      <div class="col-md-6"> <strong>To</strong> </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Address <span class="text-red">*</span></label>
                          <input name="fromAddress" class="form-control" placeholder="" type="text" value="{{$shipmentData['fromAddress']}}" required="">
                          <input name="fromAddress2" class="form-control" placeholder="" type="hidden" value="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Address <span class="text-red">*</span></label>
                          <input name="toAddress" class="form-control" placeholder="" type="text" value="{{$deliveryAddress['toAddress']}}" required="">
                          <input name="toAddress2" class="form-control" placeholder="" type="hidden" value="">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Country <span class="text-red">*</span></label>
                          <select name="fromCountry" id="fromCountry" class="form-control customSelect" required="">
                            <option value="">Select any one</option>
                            @foreach($countryList as $country)
                            <option @if($shipmentData['fromCountry'] == $country->id) selected @endif value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Country <span class="text-red">*</span></label>
                          <select name="toCountry" id="toCountry" class="form-control customSelect" required="">
                            <option value="">Select any one</option>
                            @foreach($countryList as $country)
                            <option @if($deliveryAddress['toCountry'] == $country->id) selected @endif value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>State <span class="text-red">*</span></label>
                          <select name="fromState" id="fromState" class="form-control customSelect" required="">
                            <option value=''>Select any one</option>
                        </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>State <span class="text-red">*</span></label>
                          <select name="toState" id="toState" class="form-control customSelect" required="">
                            <option value=''>Select any one</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>City <span class="text-red">*</span></label>
                          <select name="fromCity" id="fromCity" class="form-control customSelect" required=""> 
                            <option value=''>Select any one</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>City <span class="text-red">*</span></label>
                          <select name='toCity' id="toCity" class="form-control customSelect" required="">
                            <option value=''>Select any one</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Zip/Postal code <span class="text-red">*</span></label>
                          <input name="fromZip" class="form-control" placeholder="" type="text" value="{{!empty($shipmentData['fromZipCode']) ? $shipmentData['fromZipCode'] : 'N/A'}}" required="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Zip/Postal code <span class="text-red">*</span></label>
                          <input name="toZip" class="form-control" placeholder="" type="text" value="{{!empty($deliveryAddress['toZipCode']) ? $deliveryAddress['toZipCode'] : 'N/A'}}" required="">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Phone <span class="text-red">*</span></label>
                          <input name="fromPhone" class="form-control" placeholder="" type="text" value="+17135976225" required="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Phone <span class="text-red">*</span></label>
                          <input name="toPhone" class="form-control" placeholder="" type="text" value="{{$deliveryAddress['toPhone']}}" required="">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Email <span class="text-red">*</span></label>
                          <input name="fromEmail" class="form-control" placeholder="" type="text" value="N/A">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Email <span class="text-red">*</span></label>
                          <input name="toEmail" class="form-control" placeholder="" type="text" value="{{$deliveryAddress['toEmail']}}" required="">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Person Name </label>
                          <input name="fromName" class="form-control" placeholder="" type="text" value="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Person Name <span class="text-red">*</span></label>
                          <input name="toName" class="form-control" placeholder="" type="text" value="{{$deliveryAddress['toName']}}" required="">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Company Name <span class="text-red">*</span></label>
                          <input name="fromCompanyName" class="form-control" placeholder="" type="text" value="Shoptomydoor" required="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Company Name</label>
                          <input name="toCompanyName" class="form-control" placeholder="" type="text" value="{{!empty($shipmentData['toCompany']) ? $shipmentData['toCompany'] : ''}}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label>Terms Of Trade<span class="text-red">*</span></label>
                            <select id="termsOfTrade" name="termsOfTrade" required="" class="form-control customSelect">
                                <option value="">Select</option>
                                @foreach($termsOfTrades as $eachAccount)
                                    <option value="{{ $eachAccount->tradeTermsTitle }}" data-description="{{ $eachAccount->tardeTermsDescription }}">{{ $eachAccount->tradeTermsTitle }}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                              <label>Total Value <span class="text-red">*</span></label>
                              <input name="totalValue" class="form-control" placeholder="" type="number" min="0.01" step="0.01" value="" required="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                      @php 
                      $grossWeight = 0;
                      $grossChWeight = 0;
                      @endphp
                      @foreach($packageDetailsData as $key1 => $pck) 
                      @php
                      $grossWeight += $pck->weight;
                      $grossChWeight += $pck->chargeableWeight; 
                      @endphp
                      @endforeach
                      @if(count($packageDetailsData)>0)
                      <input type="hidden" name="invoiceId" value="{{$shipmentData['id']}}">
                      <input type="hidden" name="piecesCount" id="piecesCount" value="{{count($packageDetailsData)}}">
                      <input type="hidden" name="grossWeight" value="{{$grossWeight}}">
                      <input type="hidden" name="grossChWeight" value="{{$grossChWeight}}">

                      @php
                      $totalPrice = 0.00;
                      @endphp
                      @if(isset($delivery['deliveries']))
                      @foreach($delivery['deliveries'] as $eachDelivery)
                      @foreach($eachDelivery['packages'] as $eachPackages)
                      @php $totalPrice += $eachPackages['itemTotalCost']; @endphp

                      @endforeach
                      @endforeach
                      @endif
                      <input type="hidden" name="totalPrice" value="{{$totalPrice}}">
                      <input type="hidden" id="dhlaccountid" name="dhlaccountid" value="">
                      @endif
                      <div class="col-md-12 modal-title"> <strong class="modalSubTitle"> Package Details</strong> </div>
                    </div>

                    <table class="table table-bordered">
                      @if(count($packageDetailsData)>0)
                      @foreach($packageDetailsData as $key => $pck)
                      <thead id="head-{{$pck->id}}">
                        <tr>
                          <th colspan="4"> <strong class="modalSubTitle">Package #{{$key+1}}</strong> </th>
                          <th><a href="javascript:void(0)" onclick="removePackage({{$pck->id}})"><strong class="modalSubTitle text-red">Delete Package <i class="fa fa-times" aria-hidden="true"></i></strong></a></th>
                        </tr>
                        <tr>
                          <th>Length<span class="text-red">*</span></th>
                          <th>Width<span class="text-red">*</span></th>
                          <th>Height<span class="text-red">*</span></th>
                          <th>Weight<span class="text-red">*</span></th>
                          <th>Chargeable Weight<span class="text-red">*</span></th>
                        </tr>
                      </thead>
                      <tbody id="body-{{$pck->id}}">
                        <tr>
                          <td><input name="dhllenght[]" class="form-control" placeholder="length" type="text" value="{{$pck->length}}" required></td>
                          <td><input name="dhlwidth[]" class="form-control" placeholder="width" type="text" value="{{$pck->width}}" required></td>
                          <td><input name="dhlheight[]" class="form-control" placeholder="height" type="text" value="{{$pck->height}}" required></td>
                          <td><input name="dhlweight[]" class="form-control" placeholder="weight" type="text" value="{{$pck->weight}}" required></td>
                          <td><input name="dhlcweight[]" class="form-control" placeholder="Chargeable Weight" type="text" value="{{$pck->chargeableWeight}}" required></td>
                        </tr>
                        <tr>
                            <td colspan="5"><textarea name="dhlcontent[]" rows="5" class="form-control"></textarea></td>
                        </tr>
                      </tbody>
                      @endforeach
                      @else

                      <thead>
                        <tr>
                          <th colspan="5"> <strong class="modalSubTitle">Package #1</strong> </th>
                        </tr>
                        <tr>
                          <th>Length<span class="text-red">*</span></th>
                          <th>Width<span class="text-red">*</span></th>
                          <th>Height<span class="text-red">*</span></th>
                          <th>Weight<span class="text-red">*</span></th>
                          <th>Chargeable Weight<span class="text-red">*</span></th>
                        </tr>
                      </thead>
                      <tbody>


                        <tr>
                          <input type="hidden" name="invoiceId" value="{{$shipmentData['id']}}">
                          <input type="hidden" name="piecesCount" value="1">
                          <input type="hidden" name="grossWeight" value="0.00">
                          <input type="hidden" name="grossChWeight" value="0.00">
                          @php
                      $totalPrice = 0.00;
                      @endphp
                      @if(isset($delivery['deliveries']))
                      @foreach($delivery['deliveries'] as $eachDelivery)
                      @foreach($eachDelivery['packages'] as $eachPackages)
                      @php $totalPrice += $eachPackages['itemTotalCost']; @endphp

                      @endforeach
                      @endforeach
                      @endif
                      <input type="hidden" name="totalPrice" value="{{$totalPrice}}">
                          <td><input name="dhllenght[]" class="form-control" placeholder="" type="text" value="0.00" required=""></td>
                          <td><input name="dhlwidth[]" class="form-control" placeholder="" type="text" value="0.00" required=""></td>
                          <td><input name="dhlheight[]" class="form-control" placeholder="" type="text" value="0.00" required=""></td>
                          <td><input name="dhlweight[]" class="form-control" placeholder="" type="text" value="0.00" required=""></td>
                          <td><input name="dhlcweight[]" class="form-control" placeholder="" type="text" value="0.00" required=""></td>
                        </tr>
                        <tr>
                          <td colspan="5"><textarea name="dhlcontent[]" rows="4" cols="75"></textarea></td>
                        </tr>


                      </tbody>


                      @endif

                    </table>
                    <div class="modal-footer">
                        <div class="text-right disableblock" id="dhlSubmit">
                        <button onclick="dhl_submit();" type="button" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Create Label</button>
                      </div>
                    </div>
                </div>
              <!-- </form> -->
              {{ Form::close()}}
            </div>
          </div>
          <!-- /.modal-content --> 
        </div>
        <!-- /.modal-dialog --> 
        
        <div class="modal" id="confirmModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">DHL Account Description</h5>
                        <button type="button" class="close" onclick="hideDHLDescription()" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="confirmDHLDescription()">Confim</button>
                        <button type="button" class="btn btn-secondary" onclick="hideDHLDescription()">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal" id="dhl-trade-terms" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">DHL Terms of Trades Description</h5>
                        <button type="button" class="close" onclick="hideDHLDescription()" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="confirmDHLTrade()">Confim</button>
                        <button type="button" class="btn btn-secondary" onclick="hideDHLTrade()">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
<script src = "{{ asset('public/administrator/controller-css-js/shipment.js') }}" ></script>
<script>
    $(function () {
        $("#dhlpost").validate();
    });
</script>