@php
$isPaid = '';
if(!empty($orderData) && ($orderData->status==2 || $orderData->status==5)):
$isPaid = 'disableblock';
endif;
@endphp   
<div class="shadowBox m-t-20">
    <div class="row m-b-25 {{ $isPaid }}">
        <div class="col-sm-4">
            <label for="sel1">Hide shipment in Customer Page</label>
            <select name="hideFromCustomer" id="hideFromCustomer" class="form-control input-lg customSelectBlack">
                <option value="N" {{($shipment->hideFromCustomer == "N")? "selected":""}}>No</option>
                <option value="Y" {{($shipment->hideFromCustomer == "Y")? "selected":""}}>Yes</option>
            </select>
        </div>
    </div>
    <div  id="infoBoxDetail" class="row shipDetail m-b-25">
        <div class="col-sm-6 col-md-3">
            <div class="info-box shipDtlBoxBlue"> <span class="info-box-icon"><img src="{{ asset('public/administrator/img/detailIcon01.png') }}"></span>
                <div class="info-box-content"> <span class="info-box-number"><small>Gross Weight: </small>{{!empty($shipment['delivery']['totalWeight'])?$shipment['delivery']['totalWeight']:'0.00'}} lbs</span> </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="info-box shipDtlBoxSky"> <span class="info-box-icon "><img src="{{ asset('public/administrator/img/detailIcon02.png') }}"></span>
                <div class="info-box-content"> <span class="info-box-number"><small>Chargeable weight: </small>{{!empty($shipment['delivery']['totalChargeableWeight'])?$shipment['delivery']['totalChargeableWeight']:'0.00'}} lbs</span> </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="info-box shipDtlBoxGreen"> <span class="info-box-icon "><img src="{{ asset('public/administrator/img/detailIcon03.png') }}"></span>
                <div class="info-box-content"> <span class="info-box-number"><small>Total Shipment Value: </small> {{!empty($shipment['delivery']['totalValue'])?(new \App\Helpers\customhelper)->getCurrencySymbolFormat($shipment['delivery']['totalValue']):'0.00'}} </span> </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="info-box shipDtlBoxYellow"> <span class="info-box-icon "><img src="{{ asset('public/administrator/img/detailIcon04.png') }}"></span>
                <div class="info-box-content"> <span class="info-box-number"><small>Number of Deliveries:</small>{{!empty($shipment['delivery']['totalDelivery'])?$shipment['delivery']['totalDelivery']:0}}</span> </div>
            </div>
        </div>
    </div>

    @if(!empty($shipment['delivery']['deliveries']))
    @foreach($shipment['delivery']['deliveries'] as $delivery)
    @php
    $itemType = array();
    if(!empty($shipment['delivery']['itemType']))
    $itemType = explode(',',$shipment['delivery']['itemType']);
    if($delivery['shippingMethodId']!='')
    $deliveryShippingMethodAssign = 1;
    @endphp
    <div id="deliveryBox{{$delivery['id']}}" class="delevBox m-b-25 {{$delivery['deliveryAllItemReturned']==1?'disableblock':''}}">
        <div class="row m-b-25">
            <div class="col-sm-12 col-md-10">
                <div class="row">
                    <div class="col-sm-3">
                        <h3>Delivery {{$loop->iteration}}<small>&nbsp;</small> {{ "(".$delivery['memberName']."-".$delivery['unit'].") "}}</h3>
                        @php $deliveryNum = $loop->iteration @endphp
                        <input type="hidden" id="deliverySnapAllowed-{{$delivery['id']}}" value="{{ !empty($deliverySnapshotCount[$delivery['id']]) ? (5-$deliverySnapshotCount[$delivery['id']]) : 5 }}" />
                    </div>
                    <div class="col-sm-6">
                        @if($shipment->paymentStatus == 'paid' && !empty($delivery['shippingMethodId']))
                        @if(!empty($shippingMethodLogoList[$delivery['shippingMethodId']]))
                        <img src="{{ asset('public/uploads/shipping/'.$shippingMethodLogoList[$delivery['shippingMethodId']]) }}" alt="Logo not found" width="150px" height="100px">
                        @endif
                        <span style="font-size: 15px">{{$shippingMethodList[$delivery['shippingMethodId']]}}</span> 
                        @endif
                    </div>
                    <div class="col-sm-3">
                        @if(!empty($delivery['wrongInventory']) && $delivery['wrongInventory']== 'Y')
                        <h3 class="text-red text-left p-t-20">Wrong Inventory</h3>
                        @endif
                        @if($delivery['deliveryAllItemReturned']==1)
                        <div class="alert-disable">
                            <img src="{{ asset('public/administrator/img/returned.png') }}">
                        </div>
                        @endif
                    </div>
                </div>
                <div class="ltBlueBox iconItem">
                    <div class="row">
                        <div class="col-sm-3"><span class="icon"><img src="{{ asset('public/administrator/img/calenderIconBlue.png') }}"></span><span class="item"><strong>First Receive Date</strong>: {{ \Carbon\Carbon::parse($delivery['createdOn'])->format('m-d-Y | H:i')}}</span></div>
                            <div class="col-sm-3"><span class="icon"><img src="{{ asset('public/administrator/img/calenderIconBlue.png') }}"></span><span class="item"><strong>Maximum Storage Date</strong>: {{ \Carbon\Carbon::parse($delivery['maxStorageDate'])->format('m-d-Y')}}</span></div>
                            <div class="col-sm-3"><span class="icon"><img src="{{ asset('public/administrator/img/money.png') }}"></span><span class="item"><strong>Estimated Store Charge</strong>: {{ $delivery['deliveryStorageCharge'] }}</span></div>
                    </div>
                </div>
                <br/>
                <div class="ltBlueBox iconItem">
                    <div class="row">
                        <div class="col-sm-3">
                            <span class="icon"><img src="{{ asset('public/administrator/img/mailBoxIcon.png') }}"></span>
                            <span class="item"><strong>Delivery Added By</strong>:   {{$delivery['deliveryAddedBy']}}</span>
                        </div>
                        <div class="col-sm-3">
                            <span class="icon">
                                @if($delivery['snapshot'] == 'Y')
                                <img src="{{ asset('public/administrator/img/mailBoxIconWarning.png') }}">
                                @else
                                <img src="{{ asset('public/administrator/img/mailBoxIcon.png') }}">
                                @endif
                            </span>
                            <span class="item"><strong>Delivery Photo Shot</strong>:
                                @php
                                        if($delivery['snapshot'] == 'Y')
                                        echo "Yes ".\Carbon\Carbon::parse($delivery['snapshotRequestedOn'])->format('m-d-Y | H:i').' <a href="javascript:void(0);" onclick="uploadSnapshot(\''.$shipment->id.'\',\'0\',\''.$delivery['id'].'\')"  title="Upload or View Snapshot"><i class="fa fa-upload" aria-hidden="true"></i></a>';
                                        else
                                            echo "N/A";
                                        @endphp
                        </div>
                        <div class="col-sm-3">
                            <span class="icon">
                                @if($delivery['recount'] == 'Y')
                                <img src="{{ asset('public/administrator/img/mailBoxIconWarning.png') }}">
                                @else
                                <img src="{{ asset('public/administrator/img/mailBoxIcon.png') }}">
                                @endif
                            </span>
                            <span class="item"><strong>Re-Count</strong>:   {{($delivery['recount'] == 'Y')? "Yes ".\Carbon\Carbon::parse($delivery['recountRequestedOn'])->format('m-d-Y | H:i') : "N/A" }}</span>
                        </div>
                        <div class="col-sm-3">
                            <span class="icon">
                                @if($delivery['reweigh'] == 'Y')
                                <img src="{{ asset('public/administrator/img/mailBoxIconWarning.png') }}">
                                @else
                                <img src="{{ asset('public/administrator/img/mailBoxIcon.png') }}">
                                @endif
                            </span>
                            <span class="item"><strong>Reweigh</strong>:   {{($delivery['reweigh'] == 'Y')? "Yes ".\Carbon\Carbon::parse($delivery['reweighRequestedOn'])->format('m-d-Y | H:i') : "N/A" }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-2 text-center p-t-15">
                @if(in_array('N',$itemType))
                <img src="{{ asset('public/administrator/img/smBox.png') }}">
                @endif
                @if(in_array('H',$itemType))
                <img src="{{ asset('public/administrator/img/smFlame.png') }}">
                @endif
                @if(in_array('S',$itemType))
                <img src="{{ asset('public/administrator/img/smSecured.png') }}">
                @endif
            </div>
        </div>

        <div id="stages">
            <h3>Stages</h3>
            {{ Form::open(array('url' => route('updateshipmentstages',['shipmentId'=>$shipment->id,'deliveryId'=>$delivery['id']]), 'name' => 'updateStage', 'id' => 'updatestageform_'.$delivery['id'], 'method' => 'post')) }}
            <input type="hidden" name="assignCompany" id="assignCompany" value="">
            <div class="shadowBox m-b-25">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="sel1">In Warehouse <span onclick="showAddEdit('{{$delivery['id']."-".$shipment->id}}', 'in_warehouse', 'shipments/updatecomment')"><i class="fa fa-info-circle" aria-hidden="true"></i></span></label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" readonly="" class="form-control showdatepicker" disabled="" value="{{ !empty($statusLog[$delivery['id']]['in_warehouse']) ? $statusLog[$delivery['id']]['in_warehouse'] : '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="sel1">In Transit <span onclick="showAddEdit('{{$delivery['id']."-".$shipment->id}}', 'in_transit', 'shipments/updatecomment')"><i class="fa fa-info-circle" aria-hidden="true"></i></span></label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" {{ !empty($statusLog[$delivery['id']]['in_transit']) ? "readonly" : '' }} class="form-control datetimepicker" name="status[in_transit]" value="{{ !empty($statusLog[$delivery['id']]['in_transit']) ? $statusLog[$delivery['id']]['in_transit'] : '' }}" {{ !empty($statusLog[$delivery['id']]['in_transit']) ? "disabled" : '' }}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="sel1">Customs Clearing <span onclick="showAddEdit('{{$delivery['id']."-".$shipment->id}}', 'custom_clearing', 'shipments/updatecomment')"><i class="fa fa-info-circle" aria-hidden="true"></i></span></label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" {{ !empty($statusLog[$delivery['id']]['custom_clearing']) ? "readonly" : '' }} class="form-control datetimepicker" name="status[custom_clearing]" value="{{ !empty($statusLog[$delivery['id']]['custom_clearing']) ? $statusLog[$delivery['id']]['custom_clearing'] : '' }}" {{ !empty($statusLog[$delivery['id']]['custom_clearing']) ? "disabled" : '' }}>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="sel1">In Destination Warehouse <span onclick="showAddEdit('{{$delivery['id']."-".$shipment->id}}', 'destination_warehouse', 'shipments/updatecomment')"><i class="fa fa-info-circle" aria-hidden="true"></i></span></label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" {{ !empty($statusLog[$delivery['id']]['destination_warehouse']) ? "readonly" : '' }} class="form-control datetimepicker" name="status[destination_warehouse]" value="{{ !empty($statusLog[$delivery['id']]['destination_warehouse']) ? $statusLog[$delivery['id']]['destination_warehouse'] : '' }}" {{ !empty($statusLog[$delivery['id']]['destination_warehouse']) ? "disabled" : '' }}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="sel1">Out For Delivery <span onclick="showAddEdit('{{$delivery['id']."-".$shipment->id}}', 'out_for_delivery', 'shipments/updatecomment')"><i class="fa fa-info-circle" aria-hidden="true"></i></span></label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" {{ !empty($statusLog[$delivery['id']]['out_for_delivery']) ? "readonly" : '' }} class="form-control datetimepicker" name="status[out_for_delivery]" value="{{ !empty($statusLog[$delivery['id']]['out_for_delivery']) ? $statusLog[$delivery['id']]['out_for_delivery'] : '' }}" {{ !empty($statusLog[$delivery['id']]['out_for_delivery']) ? "disabled" : '' }}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="sel1">Delivered <span onclick="showAddEdit('{{$delivery['id']."-".$shipment->id}}', 'delivered', 'shipments/updatecomment')"><i class="fa fa-info-circle" aria-hidden="true"></i></span></label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" {{ !empty($statusLog[$delivery['id']]['delivered']) ? "readonly" : '' }} class="form-control datetimepicker" name="status[delivered]" value="{{ !empty($statusLog[$delivery['id']]['delivered']) ? $statusLog[$delivery['id']]['delivered'] : '' }}" {{ !empty($statusLog[$delivery['id']]['delivered']) ? "disabled" : '' }}>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                @if($shipment->paymentStatus == 'paid')
                                <a class="btn btn-default custumButt btnGreen" onclick="updateDeliveryStage({{$delivery['id']}})">Update Stage</a>
                                @else
                                <a class="btn btn-default custumButt btnGreen" disabled>Update Stage</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
        <div class="tableBlock {{ $isPaid }}">
            <p>Dimensions:</p>
            <div class="row">
                <div class="col-sm-2">
                    <label>Length</label>
                    <input class="form-control format input-lg customInputBlack deliveryLength" id="deliveryLength"  value="{{$delivery['length']}}" type="text"> 
                </div>
                <div class="col-sm-2">
                    <label>Width</label>
                    <input class="form-control format input-lg customInputBlack deliveryWidth" id="deliveryWidth" value="{{$delivery['width']}}" type="text"> 
                </div>
                <div class="col-sm-2">
                    <label>Height</label>
                    <input class="form-control format input-lg customInputBlack deliveryHeight" id="deliveryHeight" value="{{$delivery['height']}}" type="text"> 
                </div>
                <div class="col-sm-2">
                    <label>Weight (lbs)</label>
                    <input class="form-control format input-lg customInputBlack deliveryWeight" id="deliveryWeight" value="{{$delivery['weight']}}" type="text"> 
                </div>
                <div class="col-sm-2 text-left p-t-20">
                    <button class="btn btn-default custumButt btnGreen" onclick="updateDelivery({{$shipment-> id}}, {{$delivery['id']}})">Update Dimensions</button>
                </div>
                <div class="col-sm-2 text-left p-t-20">
                    <div style="position: relative; cursor: pointer; pointer-events: auto; opacity: 0.99">
                        <a href="javascript:void(0);" data-toggle="popover" trigger="hover" data-content="Some content inside the popover" onclick="showDeliveryDetails({{$delivery['packages'][0]['id']}})"  title="View Delivery Informations"><i class="fa fa-fw fa-info-circle" style="font-size: 30px"></i></a>
                    </div>
                    <div class="deliveryDetaiils-{{$delivery['packages'][0]['id']}}"  style="display: none">
                        @if(!empty($delivery['packages'][0]['deliveryCompany']))
                        <table class="userDetails text-data" style="width: 50%;border: 1px">
                            <tr>
                                <td><strong>Delivery Company</strong> : </td>
                                <td>{{$delivery['packages'][0]['deliveryCompany']}}</td>
                            </tr>
                            <tr>
                                <td><strong>Tracking 1</strong> : </td>
                                <td>{{$delivery['packages'][0]['tracking']}} &nbsp;&nbsp;
                                    @if($delivery['packages'][0]['trackingLock'] == 0)
                                    <a href="javascript:void(0)" class="text-green edit editTrackingData" data-toggle="tooltip" title="" data-original-title="Click to Edit"><i class="fa fa-fw fa-edit" style="font-size: 20px"></i></a>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Tracking 2</strong> : </td>
                                <td>{{($delivery['packages'][0]['tracking2']!="")?$delivery['packages'][0]['tracking2']:"N/A"}} &nbsp;&nbsp;</td>
                            </tr>
                            <tr>
                                <td><strong>Delivered On</strong> : </td>
                                <td>{{$delivery['packages'][0]['deliveredOn']}}</td>
                            </tr>
                            <tr>
                                <td><strong>Delivery Notes</strong> : </td>
                                <td>{{!empty($delivery['packages'][0]['deliveryNotes'])?$delivery['packages'][0]['deliveryNotes']:'N/A'}}</td>
                            </tr>
                        </table>
                        {{ Form::open(array('url' => 'administrator/shipments/updatetracking/'.$delivery['packages'][0]['id'], 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post', 'files'=>true)) }}
                        <div class="text-edit" style="display: none">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Tracking #: <span class="text-red">*</span></label>
                                        <input type="text" name="trackingNumber" id="trackingNumber" value="{{$delivery['packages'][0]['tracking']}}" class="form-control" rows="3" required="" placeholder="Enter Tracking Number">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Tracking 2#: <span class="text-red">*</span></label>
                                        <input type="text" name="tracking2" id="trackingNumber" value="{{$delivery['packages'][0]['tracking2']}}" class="form-control" rows="3" required="" placeholder="Enter Tracking Number 2">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" name="action" value="nothing" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Update</button>
                                    <button type="submit" name="action" value="lock" class="btn btn-success"><span class="Cicon"><i class="fa fa-lock"></i></span>Update and lock</button>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                        @else
                        <p>Delivery Details Not Found</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="table-responsive m-t-20">
                <input type="hidden" id="subcatPage" value="getsubcategory">
                <table class="table table-bordered table-hover no-margin">
                    <thead>
                        <tr class="headerTblBg">
                            <th width="8%" valign="top" align="left">Store</th>
                            <th width="8%" valign="top" align="left">Category</th>
                            <th width="8%" valign="top" align="left">Sub Category</th>
                            <th width="12%" valign="top" align="left">Product</th>
                            <th width="10%" valign="top" align="left">Name of Item</th>
                            <th width="8%" valign="top" align="left">Min Value/Item</th>
                            <th width="8%" valign="top" align="left">Max Value/Item</th>
                            <th width="5%" valign="top" align="left">Quantity</th>
                            <th width="8%" valign="top" align="left">Subtotal</th>
                            <th width="8%" valign="top" align="left">Return</th>
                            <th width="3%" valign="top" align="left">Snapshot</th>
                            <th>Discounted<br/> Invoice</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    @if($shipment->type='shopforme')
                    <tbody id="packageList">
                        @foreach($delivery['packages'] as $package)
                        @php
                        if($package['itemType']=='N')
                        $packageType = 'Normal';
                        else if($package['itemType']=='H')
                        $packageType = 'Hazmat';
                        else
                        $packageType = 'Secured';
                        @endphp
                        <tr id="package{{$package['id']}}" class="{{ ($package['itemReturn'] == '2'? 'disableblock':'') }}">
                            <td valign="top" align="left">
                                <input type="hidden" name="packageId" value="{{$package['id']}}" />

                                <div class="value_label">{{!empty($package['storeName'])?$package['storeName']:'Not Listed'}}</div>
                                <div class="value_field" style="display: none">
                                    <select name="storeId" required="" data-name="Store" class="form-control packagestore input-lg customSelectBlack" id="storeId" disabled="">
                                        <option value="0">Not Listed</option>
                                        @foreach($storeList as $store)
                                        <option value="{{$store->id}}" {{$store->id==$package['storeId']?'selected="selected"':''}}>{{$store->storeName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </td>
                            <td valign="top" align="left">

                                <div class="value_label">{{!empty($package['categoryName'])?$package['categoryName']:'Not Listed'}}</div>
                                <div class="value_field" style="display: none">
                                    <select name="siteCategoryId" required="" data-name="Category" class="form-control packagecategory input-lg customSelectBlack" id="siteCategoryId">
                                        <option value="0">Not Listed</option>
                                        @foreach($categoryList as $categoryId => $categoryName)
                                        <option value="{{$categoryId}}" {{$categoryId==$package['siteCategoryId']?'selected="selected"':''}}>{{$categoryName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </td>
                            <td valign="top" align="left">
                                <div class="value_label">{{!empty($package['subcategoryName'])?$package['subcategoryName']:'Not Listed'}}</div>
                                <div class="value_field" style="display: none">
                                    <select name="siteSubCategoryId" required="" data-name="Sub Category" class="form-control packagesubcategory input-lg customSelectBlack" id="siteSubCategoryId">
                                        <option value="0">Not Listed</option>
                                    </select>
                                    <input type="hidden"  name="packagesubcategoryId" class="packagesubcategoryId" value="{{$package['siteSubCategoryId']}}" />
                                </div>
                            </td>
                            <td valign="top" align="left">
                                <div class="value_label">{{!empty($package['productName'])?$package['productName']:'Not Listed'}}</div>
                                <div class="value_field" style="display: none">
                                    <select name="siteProductId" required="" data-name="Product" class="form-control packageproduct input-lg customSelectBlack" id="siteProductId">
                                        <option value="0">Not Listed</option>
                                    </select>
                                    <input type="hidden" name="packageproductId" class="packageproductId" value="{{$package['siteProductId']}}" />
                                </div>
                            </td> 
                            <td valign="top" align="left">
                                <div class="value_label">{{$package['itemName']}}</div>
                                <div class="value_field" style="display: none"><input name="itemName" id="itemName" required="" data-name="Name of Item" class="form-control input-lg customInputBlack" value="{{$package['itemName']}}" type="text"></div>
                            </td>
                            <td valign="top" align="left">
                                <div class="value_label">{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat($package['itemMinPrice'])}}</div>
                                <div class="value_field" style="display: none"><input name="itemMinPrice" required="" data-name="Value Per Item" class="form-control productvalOtherMin format input-lg customInputBlack" value="{{$package['itemMinPrice']}}" type="text"></div>
                            </td>
                            <td valign="top" align="left">
                                <div class="value_label">{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat($package['itemPrice'])}}</div>
                                <div class="value_field" style="display: none"><input name="itemPrice" required="" data-name="Value Per Item" class="form-control productvalOther format input-lg customInputBlack" value="{{$package['itemPrice']}}" type="text"></div>
                            </td>
                            <td valign="top" align="left">
                                <div class="value_label">{{$package['itemQuantity']}}</div>
                                <div class="value_field" style="display: none"><input name="itemQuantity" required="" data-name="Quantity" class="form-control quantityOther input-lg customInputBlack" value="{{$package['itemQuantity']}}" type="number" min="1"></div>
                            </td>
                            <td valign="top" align="left">
                                <div class="value_label">{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat($package['itemTotalCost'])}}</div>
                                <div class="value_field" style="display: none"><input name="itemTotalCost" readonly="readonly" class="form-control format producttotal input-lg customInputBlack" value="{{$package['itemTotalCost']}}" type="text"></div>
                            </td>
                            <td valign="top" align="center">
                                <div class="value_label">
                                    @if($package['itemReturn'] == '2')
                                    Return Complete
                                    @elseif($package['itemReturn'] == '1')
                                    Return Requested
                                    @else
                                    N/A
                                    @endif    
                                </div>
                            </td>
                            <td class="text-center buttMrg" valign="top" align="left">
                                @if($package['snapshotOpt'] == 'Y')
                                <a href="javascript:void(0);" onclick="uploadSnapshot({{ $shipment-> id }}, {{$package['id']}}, {{$delivery['id']}})"  title="Upload or View Snapshot"><img src="{{ asset('public/administrator/img/uploadIconSm.png') }}"></a>
                                @else
                                N/A
                                @endif
                            </td>
                            <td class="text-center buttMrg" valign="top" align="left">
                                @if($package['itemDiscountedInvoiceFile']!="")
                                <div style="position: relative; cursor: pointer; pointer-events: auto; opacity: 0.99">
                                    <a data-target="tooltip" title="download" href="{{ url('administrator/shipments/downloaddiscountedinvoice/'.$package['id']) }}"><i class="fa fa-download" aria-hidden="true" style="font-size: 25px"></i></a>
                                </div>
                                @else
                                <span>N/A</span>
                                @endif
                            </td>
                            <td class="text-center buttMrg" valign="top" align="left">
                                @if($package['itemReturn'] == '0')
                                <a href="javascript:void(0);" class="editPackage"><img data-target="tooltip" class="save-item" title="Click to Edit" src="{{ asset('public/administrator/img/editIconSm.png') }}"></a> 
                                <a href="javascript:void(0);" style="display:none;" class="savePackage"><img data-target="tooltip" height="18" class="save-item" title="Click to Edit" src="{{ asset('public/administrator/img/download-button.png') }}"></a> 
                                <a href="javascript:void(0);" data-toggle="confirmation" data-id="{{$package['id']}}" class="removePackage"><img data-target="tooltip" title="Remove Item / href="javascript:void(0);" dataProduct" src="{{ asset('public/administrator/img/deleteIconRedSm.png') }}"></a>
                                @elseif($package['itemReturn'] == '1')
                                <a href="javascript:void(0)" class="color-theme-2 actionIcons" title="Process Return" onclick="showAddEdit({{$package['id']}}, {{$page}}, 'shipments/processreturn/{{$shipment->id}}/{{$delivery['id']}}');"><i class="fa fa-exchange" aria-hidden="true"></i></a>
                                @if(!empty($package['returnLabel']))
                                <a href="{{ url('administrator/shipments/downloadreturnlabel/'.$shipment->id.'/'.$package['id']) }}" class="color-theme actionIcons" title="Download return label"><i class="fa fa-download" aria-hidden="true"></i></a>
                                @endif
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                    <tfoot>        
                        <tr id="blank_items" style="display: none">                                     
                            <td valign="top" align="left">
                                <select name="storeId" data-name="Store" id="storeId" required="" class="form-control storeId input-lg customSelectBlack" disabled="">
                                    <option value="0">Not Listed</option>
                                    @foreach($storeList as $store)
                                    <option value="{{$store->id}}">{{$store->storeName}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td >
                                <select id="addcategoryId" name="siteCategoryId" data-name="Category" required="" class="form-control siteCategoryId input-lg customSelectBlack">
                                    <option value="0">Not Listed</option>
                                    @foreach($categoryList as $categoryId => $categoryName)
                                    <option value="{{$categoryId}}">{{$categoryName}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td valign="top" align="left">
                                <select id="addsubcategoryId" name="siteSubCategoryId" data-name="Sub Category" required="" class="form-control subCategory input-lg customSelectBlack">
                                    <option value="0">Not Listed</option>
                                </select>
                            </td>
                            <td valign="top" align="left">
                                <select id="addproductId" name="siteProductId" data-name="Product" required="" class="form-control siteProduct input-lg customSelectBlack">
                                    <option value="0">Not Listed</option>
                                </select>
                            </td>
                            <td valign="top" align="left"><input name="itemName" required="" data-name="Name of Item" class="form-control input-lg customInputBlack" type="text"></td>
                            <td valign="top" align="left"><input name="itemMinPrice" required="" data-name="Item Min Price" class="form-control input-lg format product-val-min customInputBlack" type="text"></td>
                            <td valign="top" align="left"><input name="itemPrice" required="" data-name="Value Per Item" class="form-control input-lg format customInputBlack product-val" type="text"></td>
                            <td valign="top" align="left"><input name="itemQuantity" required="" data-name="Quantity"  class="form-control input-lg customInputBlack qty" type="text"></td>
                            <td valign="top" align="left"><input name="itemTotalCost" disabled="disabled" readonly="readonly" class="form-control format input-lg customInputBlack product-total" type="text"></td>
                            <td valign="top" align="left">&nbsp;</td>
                            <td valign="top" align="left">&nbsp;</td>
                            <td valign="top" align="left">&nbsp;</td>
                            <td class="text-center addDeliveryItem buttMrg" valign="top" align="left">
                                <input type="hidden" name="shipmentId" value="{{$shipment->id}}" />
                                <input type="hidden" name="deliveryId" value="{{$delivery['id']}}" />
                                <a href="javascript:void(0);" class="editPackage"><img data-target="tooltip" height="18" class="save-item" title="Click to Save" src="{{ asset('public/administrator/img/download-button.png') }}"></a>
                                <a href="javascript:void(0);" data-toggle="confirmation" class="removeItem"><img data-target="tooltip" title="Remove Item / Product" src="{{ asset('public/administrator/img/deleteIconRedSm.png') }}"></a>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                @if($shipment->paymentStatus == 'unpaid')
                <div class="clearfix text-right"><a data-target="tooltip" title="Add New Item / Product" href="javascript:void(0)" class="addButt">+</a></div>
                @endif
                <div class="form-group">
                    @if(!empty($priceUpdateLog[$delivery['id']]))
                    @foreach($priceUpdateLog[$delivery['id']] as $eachPackageUpdate)
                    <p class="text-red">
                        @if($eachPackageUpdate['actionType'] == 'update')
                        Value of package {{ $eachPackageUpdate['itemName'] }} changed from {{ $eachPackageUpdate['oldPrice'] }} to {{ $eachPackageUpdate['newPrice'] }} by {{ $eachPackageUpdate['updatedBy'] }} on {{ $eachPackageUpdate['updatedOn'] }}
                        @else
                        Request to change price of package {{ $eachPackageUpdate['itemName'] }} from {{ $eachPackageUpdate['oldPrice'] }} to {{ $eachPackageUpdate['newPrice'] }} is submitted on {{ $eachPackageUpdate['updatedOn'] }} by {{ $eachPackageUpdate['updatedBy'] }}
                        @endif
                    </p>
                    @endforeach
                    @endif
                </div>
                
                @if(!empty($shipment['deletedPackage']['deliveries'][$delivery['id']]))
                <div class="dltRedBox m-b-25">
                    <h3>Deleted Products / Items</h3>
                    <table class="table table-bordered table-hover no-margin">
                        <thead>
                            <tr class="headerTblBgRed">
                                <th width="10%" valign="top" align="left">Name of Item</th>
                                <th width="8%" valign="top" align="left">Item Price</th>
                                <th width="5%" valign="top" align="left">Quantity</th>
                                <th width="8%" valign="top" align="left">Subtotal</th>
                                <th width="8%" valign="top" align="left">Deleted By</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($shipment['deletedPackage']['deliveries'][$delivery['id']]['packages'] as $eachPackage)
                            <tr>
                                <td>{{ $eachPackage['itemName'] }}</td>
                                <td>{{ $eachPackage['itemPrice'] }}</td>
                                <td>{{ $eachPackage['itemQuantity'] }}</td>
                                <td>{{ $eachPackage['itemTotalCost'] }}</td>
                                <td>{{ $eachPackage['packageDeletedBy'] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
                <div class="shadowBox clearfix m-b-25">
                    <h5>Other Charges Details</h5>
                    @if($shipment->prepaid == 'N' && empty($shipment->paymentMethodId))

                    @if(!empty($otherChargesList))
                    <div class="form-group {{ $isPaid }}">
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="otherChargeId">Other charges:</label>
                                <select name="otherChargeId{{$delivery['id']}}"  id="otherChargeId{{$delivery['id']}}" class="form-control oCharge input-lg customSelectBlack">
                                    <option value="">Select</option>
                                    @foreach($otherChargesList as $otherCharge)
                                    <option value="{{$otherCharge->id}}">{{$otherCharge->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label for="notes">Notes:</label>
                                <input name="notes{{$delivery['id']}}" id="chargeNotes{{$delivery['id']}}" class="form-control oCharge input-lg customInputBlack" type="text" />
                            </div>
                            <div class="col-sm-2 text-left p-t-20">
                                <button id="otherChrgbtn" onclick="javascript:addNewCharge({{$shipment-> id}}, {{$delivery['id']}});" class="btn btn-default custumButt btnGreen">Add</button>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endif
                    <div class="row">
                        <div class="col-sm-8 col-md-6 customList">
                            <ul id="otherChargeList_{{ $delivery['id'] }}"  class="list-group">
                                @if(!empty($shipment['othercharges'][$delivery['id']]))
                                @foreach($shipment['othercharges'][$delivery['id']] as $row)
                                @php $otherChargeAmount =   (new \App\Helpers\customhelper)->getCurrencySymbolFormat($row->otherChargeAmount); @endphp
                                <li id="othercharge{{$row->id}}" class="list-group-item">
                                    <div class="bulletTxt"><span class="bullet"><img src="{{ asset('public/administrator/img/redsqrBullet.png') }}"></span>{{$row->otherChargeName}} {{!empty($row->notes)?'-'.$row->notes:''}}  - {{$otherChargeAmount}}<br>
                                        Added by: {{$row->createdBy}}</div>
                                    @if($shipment->prepaid == 'N' && empty($shipment->paymentMethodId))
                                    <span class="deleteBtn"><a data-id="{{$row->id}}" data-shipmentid="{{$shipment->id}}" data-deliveryid="{{ $delivery['id'] }}" class="deletecharge" href="javascript:void(0);"><img src="{{ asset('public/administrator/img/deleteIconRedSm.png') }}"></a></span>
                                    @endif    
                                </li>
                                @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                    <h5>Manual Charges Details</h5>
                    @if($shipment->prepaid == 'N' && empty($shipment->paymentMethodId))
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="otherChargeName">Manual charge:</label>
                                <input name="otherChargeName{{$delivery['id']}}" id="otherChargeName{{$delivery['id']}}" class="form-control mCharge input-lg customInputBlack" required="" type="text">
                            </div>
                            <div class="col-sm-3">
                                <label for="otherChargeAmount">Amount($):</label>
                                <input name="otherChargeAmount{{$delivery['id']}}" id="otherChargeAmount{{$delivery['id']}}" class="form-control mCharge format input-lg customInputBlack" required="" type="text">
                            </div>
                            <div class="col-sm-3">
                                <label for="otherNotes">Notes:</label>
                                <input name="notes{{$delivery['id']}}" id="otherNotes{{$delivery['id']}}" class="form-control mCharge input-lg customInputBlack" type="text">
                            </div>
                            <div class="col-sm-2 text-left p-t-20">
                                <button  id="manualChrgBtn" onclick="javascript:addNewManualCharge({{$shipment-> id}}, {{$delivery['id']}});" class="btn btn-default custumButt btnGreen">Add</button>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-sm-8 col-md-6 customList">
                            <ul  id="manualChargeList_{{ $delivery['id'] }}"  class="list-group">
                                @if(!empty($shipment['manualcharges'][$delivery['id']]))
                                @foreach($shipment['manualcharges'][$delivery['id']] as $row)
                                @php $otherChargeAmount =   (new \App\Helpers\customhelper)->getCurrencySymbolFormat($row->otherChargeAmount); @endphp
                                <li id="othercharge{{$row->id}}" class="list-group-item">
                                    <div class="bulletTxt"><span class="bullet"><img src="{{ asset('public/administrator/img/redsqrBullet.png') }}"></span>{{$row->otherChargeName}} {{!empty($row->notes)?'-'.$row->notes:''}}  - {{$otherChargeAmount}}<br>
                                        Added by: {{$row->createdBy}}</div>
                                    @if($shipment->prepaid == 'N' && empty($shipment->paymentMethodId))
                                    <span class="deleteBtn"><a data-id="{{$row->id}}" data-shipmentid="{{$shipment->id}}" class="deletecharge" href="javascript:void(0);"><img src="{{ asset('public/administrator/img/deleteIconRedSm.png') }}"></a></span>
                                    @endif    
                                </li>
                                @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>

                <h4>Shipping Charges Details</h4>
                @if(isset($delivery['id']) && !empty($delivery['id']))
                <div class="ltRedBox text-center m-b-25 deliveryShipingCostBox-{{ $delivery['id'] }}">
                    <div class="row">
                        <ul class="shipingCost deliveryShipingCost-{{ $delivery['id'] }}">
                            <li>Shipping Cost: <strong>{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat(($delivery['shippingCost']))}}</strong></li>
                            <li>
                                @if($delivery['isDutyCharged']=='1')
                                Clearing,Port Handling:
                                @else
                                Clearing,Port Handling and duty: 
                                @endif
                                <strong>{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat(($delivery['clearingDutyCost']))}}</strong>
                            </li>
                            <li>Warehousing / Inventory Charge: <strong>{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat(($delivery['inventoryCharge']))}}</strong></li>
                            <li>Other Charges: <strong>{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat(($delivery['otherChargeCost']))}}</strong></li>
                        </ul>
                        <div class="col-sm-12 m-t-20">
                            <div class="row">
                                <div class="col-sm-3 text-center">
                                    <label>Shipping Method:</label>
                                    @if(!empty($delivery['shippingMethodId'])) 
                                    <h3><small>{{$shippingMethodList[$delivery['shippingMethodId']]}}</small></h3>
                                    @else 
                                    <h3><small>Shipping Method Not Selected</small></h3>
                                    @endif

                                </div>
                                <div class="col-sm-3 text-center">
                                    <h3><small><strong>TOTAL:</strong></small><span class="txtRed">{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat(($delivery['totalCost']))}}</span> </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
        <div class="table-responsive m-t-20">
            <div class="m-t-20 col-sm-12 text-center">
                @if($shipment->paymentStatus == 'unpaid')
                <div class="customIconBox"><a href="javascript:void(0);" class="deletedelivery" onclick="deleteDelivery({{$delivery['id']}})"><img src="{{ asset('public/administrator/img/deleteIconRed.png') }}"></a></div>
                @endif
                <button onclick="showAddEdit({{$shipment-> id}}, {{'"'.$delivery['id'].'/'.$deliveryNum.'"'}}, 'shipments/printdeliverylabel')"  class="btn btn-default custumBtnWtIcn printBtn "><span class="btnIcon"><img src="{{ asset('public/administrator/img/faxIcon.png') }}"></span>Print Delivery Label</button>
            </div>
        </div>
    </div>
    @endforeach
    @endif
    @if($shipment->paymentStatus == 'unpaid')
    <div class="col-sm-12 text-center m-b-25 {{ $isPaid }}" id="add-new-delivery">
        <button class="btn btn-default custumButt solidBtn m-r-15" onclick="showAddEdit({{$shipment->id}}, 0, 'shipments/addeditdelivery')">+ Add new Delivery</button>
    </div>
    @endif
    <div class="clearfix"></div>
    @if(!$shipment['deleteddelivery']->isEmpty())
    <div class="dltRedBox m-b-25">
        <h3>Deleted Deliveries</h3>
        <div class="tableBlock">
            <div class="table-responsive m-t-20">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr class="headerTblBgRed">
                            <th>Delivery</th>
                            <th>Customer Name</th>
                            <th>Products Ordered</th>
                            <th>Total Value</th>
                            <th>Total Quantity</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th>Type</th>
                            <th>Deleted By</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($shipment['deleteddelivery'] as $delivery)
                        <tr class="whiteTblbodyBg">
                            <td><span class="redTxt">{{$delivery->deliveryId}}</span></td>
                            <td>{{$delivery->customerName}}</td>
                            <td>{{$delivery->itemName}}</td>
                            <td>{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat($delivery->value)}}</td>
                            <td>{{$delivery->quantity}}</td>
                            <td>{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat($delivery->quantity*$delivery->value)}}</td>
                            <td>{{$delivery->status}}</td>
                            <td>{{$delivery->itemType}}</td>
                            <td>{{$delivery->deletedByEmail}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif
</div>
<script>
    $(function () {
        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss'
        });
    });
</script>