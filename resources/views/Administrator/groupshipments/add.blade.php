@extends('Administrator.layouts.master')
@section('content')

<script src = "{{ asset('public/administrator/controller-css-js/groupshipment.js') }}" ></script>
<div id="wait" style="display:none;" class="loaderMiddle"></div>
<section class="content">
    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <!--<div class="box-header">
                  <h3 class="box-title">Hover Data Table</h3>
                </div>-->
                <!-- /.box-header -->
                {{ Form::open(array('url' => 'administrator/groupshipments/save/0/'.$page, 'name' => 'addEditFrm', 'id' => 'addEditFrm', 'method' => 'post', 'autocomplete' => 'off','onsubmit' => 'return checkValid(this);')) }} 

                <input type="hidden" name="groupId" id="groupId" value="">                 
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Group Coordinators Unit <span class="text-red">*</span></label>
                                <input id="userUnit" name="groupUnit" class="form-control" required="" placeholder="Enter Unit" type="text">
                                <div id="user-verification"></div><br/>
                                <a href="javascript:void(0);" onclick="userVerification();" type="text" class="btn btn-success pull-left"><span class="Cicon"><i class="fa fa-check-circle"></i></span>Verify Customer</a>
                            </div>
                        </div>
                    </div>
                     <div id="customerSettingsBlock" style="display: none">
                        <div class="box-header">
                            <h3 class="box-title text-info">Shipment Settings</h3>
                        </div>
                        <div class="box-body">
                         <div class="form-group setting-item">
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <label id="remove_shoe_box">No</label><label style="margin-left: 10px">Always remove shoe boxes. (Select Yes to save money)</label>
                               </div> 
                             </div>
                             <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <label id="original_box">No</label><label style="margin-left: 10px">Ship all my items in their original boxes (Select No to save money)</label>
                               </div> 
                             </div>
                             <div class="row">
                                 <div class="col-md-6 col-xs-12">
                                    <label id="quick_shipout">No</label><label style="margin-left: 10px">Quick ship my items. (For subscribed customers shipping via DHL Priority)</label>
                               </div>
                             </div>

                             <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <label style="margin-right: 10px">Special Handling Instructions : <span id="special_instruction">N/A</span></label>
                               </div> 
                             </div>
                             </div>
                     </div>
                    </div>
                    <div id="shipmentDeliveryDetails" class="disableblock">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Warehouse Location</label>
                                    <select name="warehouseId" id="warehouseId" class="customSelect form-control" required="">
                                        <option value="">Select</option>
                                    </select>
                                    <div id="warehouse-location"></div><br/>
                                </div>
                            </div>
                        </div>
                        <div id="shipmentDeliveryRest" class="disableblock">
                            <div class="row m-t-15">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Shipment Type <span class="text-red">*</span></label>
                                        <select name="partucilarShipmentType" id="partucilarShipmentType" required="" class="customSelect form-control">
                                            <option value="">Select</option>
                                            @foreach($particularShipmentTypeList as $typeId=>$typeName)
                                            <option value="{{$typeId}}">{{$typeName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Location Row <span class="text-red">*</span></label>
                                        <select name="warehouseRowId" id="warehouseRowId" onchange="getWarehouseZoneList(this.value)" required="" class="customSelect form-control">
                                            <option value="">Select</option>
                                            @foreach($warehouseRowList as $row)
                                            <option value="{{$row->id}}">{{$row->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Location Zone <span class="text-red">*</span></label>
                                        <select name="warehouseZoneId" id="warehouseZoneId" required="" class="customSelect form-control">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-15">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Delivery Company <span class="text-red">*</span></label>
                                        <select name="deliveryCompanyId" id="deliveryCompanyId" class="customSelect form-control" required="">
                                            <option value="">Select Delivery Company</option>
                                            @foreach($deliveryCompanyList as $deliveryCompany)
                                            <option value="{{$deliveryCompany->id}}">{{$deliveryCompany->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Delivered <span class="text-red">*</span></label>
                                        <input id="delivered" name="delivered" class="form-control datepicker" readonly="" required="" placeholder="Select Date" type="text" value="{{ date('Y-m-d') }}">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Tracking 1<span class="text-red">*</span></label>
                                        <input id="trackingNumber" name="trackingNumber" class="form-control" required="" placeholder="Enter Tracking ID" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-15">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Tracking 2</label>
                                        <input id="trackingNumber2" name="trackingNumber2" class="form-control" placeholder="Enter Tracking ID" type="text">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Delivery Note</label>
                                        <input id="trackingNumber2" name="note" class="form-control" placeholder="Enter Delivery Note" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-15">  
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Delivery 1:</label>
                                        <div class="table-responsive dash_custom_table dash_custom_shipment_tble">
                                            <div class="form-group col-md-2">
                                                <label>Unit Number<span class="text-red">*</span></label>
                                                <input id="userUnitNumber" name="userUnitNumber" class="form-control" required="" placeholder="Enter Unit" type="text"><div id="userName"></div>
                                            </div>

                                            <table class="table table-field table-striped table-hover small-text" id="tb">
                                                <thead>
                                                    <tr class="tr-header">
                                                        <th>Store</th>
                                                        <th>Category</th>
                                                        <th>Subcategoty</th>
                                                        <th>Product</th>
                                                        <th>Item Name</th>
                                                        <th>Min Value/Item</th>
                                                        <th>Max Value/Item</th>
                                                        <th>Quantity</th>
                                                        <th>Total</th>
                                                        <th>
                                                            <a href="javascript:void(0);" id="addMore" title="Add More Item" class="addRow skyTxt">
                                                                <span class="glyphicon glyphicon-plus skyTxt"></span>
                                                            </a>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <select name="shipment[store][]" id="storeId" class="customSelect form-control storeId">
                                                                 <option value="0">Not Listed</option>
                                                               
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select name="shipment[siteCategoryId][]" class="customSelect form-control siteCategoryId">
                                                                <option value="0">Not Listed</option>
                                                                @foreach($categoryList as $categoryId => $categoryName)
                                                                <option value="{{ $categoryId }}">{{ $categoryName }}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select class="form-control customSelect subCategory" name="shipment[siteSubCategoryId][]" required="">
                                                                <option value="0">Not Listed</option>
                                                            </select>
                                                            <input type="hidden" id="subcatPage" value="getsubcategory">
                                                        </td>
                                                        <td>
                                                            <select class="form-control customSelect siteProduct" name="shipment[siteProductId][]" required="">
                                                                <option value="0">Not Listed</option>
                                                            </select>
                                                        </td>
                                                        <td><input name="shipment[item][]" required="" class="form-control" type="text"></td>
                                                        <td><input name="shipment[minValue][]" required="" class="form-control product-min-val format" type="number" min="1"></td>
                                                        <td><input name="shipment[value][]" required="" class="form-control format product-val" type="number" min="1"></td>
                                                        <td><input name="shipment[quantity][]" required="" class="form-control qty" type="number" min="1"></td>
                                                        <td><input name="shipment[total][]" required=""  readonly=""  class="form-control format product-total" type="text"></td>
                                                        <td>
                                                            <a href="javascript:void(0);" class="remove delRow text-red">
                                                                <span class="glyphicon glyphicon-remove text-red"></span>
                                                            </a>
                                                        </td>

                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="text-danger" id="trackingError" style="display: none">Please Enter Tracking details of every package</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4 class="box-title">Dimensions</h4>
                            <div class="row m-t-15">
                                <div class="col-md-12 col-xs-12">
                                    <div id="foundProductDetails">

                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label>Length</label>
                                        <input id="lenth" name="length" class="form-control format" min="1" placeholder="Enter Length" type="number" required="">
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label>Width</label>
                                        <input id="width" name="width" class="form-control format" min="1" placeholder="Enter Width" type="number" required="">
                                    </div>
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label>Height</label>
                                        <input id="height" name="height" class="form-control format" min="1" placeholder="Enter Height" type="number" required="">
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label>Weight</label>
                                        <input id="weight" name="weight" class="form-control format" min="0.01" step="0.01" placeholder="Enter Weight" type="number" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-15">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <span style="font-size: 15px"><strong class="text-red">NOTE:</strong> <span class="text-red">If this shipment is going by a fixed and only method, select to lock it to that method. Otherwise, do not select.</span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-15">
                                <div class="col-md-4 col-xs-12 disableblock">
                                    <div class="form-group">
                                        <label>Select Applicable Shipping Method For This Shipment</label>
                                        <select name="selectedShippingMethod"  id="selectedShippingMethod" class="form-control oCharge input-lg customSelectBlack" disabled>
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <a class="form-control btn btn-info" onclick="getShippingMethods();">Select to lock to a particular shipping method</a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="clearfix m-b-25">
                            <br>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <span style="font-size: 15px"><strong class="text-red">NOTE:</strong> You must select extra charge and add for all (1) <strong>Lithium</strong> (2) <strong>Hazmat</strong> (3) <strong>Fragile</strong> (4) <strong>Liquid</strong> (5) <strong>TV</strong> Shipments. See below for a full list of extra charges</span>
                                    </div>
                                </div>
                            </div>
                            <h5>Add extra charge</h5>
                            @if(!empty($otherChargesList))
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="otherChargeId">Other charges:</label>
                                        <select name="otherChargeId"  id="otherChargeIdNewdeliv" class="form-control oCharge input-lg customSelectBlack">
                                            <option value="">Select</option>
                                            @foreach($otherChargesList as $otherCharge)
                                            <option value="{{$otherCharge->id}}">{{$otherCharge->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="notes">Notes:</label>
                                        <input name="notes" id="chargeNotesNewdeliv" class="form-control oCharge input-lg customInputBlack" type="text" />
                                    </div>
                                    <div class="col-sm-2 text-left p-t-20">
                                        <a id="otherChrgbtnNewdeliv" onclick="javascript:addNewChargeNewDelivery('0');" class="btn btn-default custumButt btnGreen">Add</a>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-sm-8 col-md-6 customList">
                                    <ul id="otherChargeList"  class="list-group">
                                        @if(!empty($othercharges))
                                        @foreach($othercharges as $row)
                                        @php $otherChargeAmount =   (new \App\Helpers\customhelper)->getCurrencySymbolFormat($row->otherChargeAmount); @endphp
                                        <li id="othercharge{{$row->id}}" class="list-group-item">
                                            <div class="bulletTxt"><span class="bullet"><img src="{{ asset('public/administrator/img/redsqrBullet.png') }}"></span>{{$row->otherChargeName}} {{!empty($row->notes)?'-'.$row->notes:''}}  - {{$otherChargeAmount}}<br>
                                                Added by: {{$row->createdBy}}</div>
                                            <span class="deleteBtn"><a data-id="{{$row->id}}" class="deletenewcharge color-theme-2 actionIcons" href="javascript:void(0);" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a></span>
                                        </li>
                                        @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <h5>Manual Charges Details</h5>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="otherChargeName">Manual charge:</label>
                                        <input name="otherChargeName" id="otherChargeNameNewdeliv" class="form-control mCharge input-lg customInputBlack" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="otherChargeAmount">Amount({{(new \App\Helpers\customhelper)->getCurrencySymbolCode() }}):</label>
                                        <input name="otherChargeAmount" id="otherChargeAmountNewdeliv" class="form-control mCharge format input-lg customInputBlack" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="otherNotes">Notes:</label>
                                        <input name="notes" id="otherNotesNewdeliv" class="form-control mCharge input-lg customInputBlack" type="text">
                                    </div>
                                    <div class="col-sm-2 text-left p-t-20">
                                        <a  id="manualChrgBtn"  onclick="javascript:addNewManualChargeNewDelivery('0');" class="btn btn-default custumButt btnGreen">Add</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8 col-md-6 customList">
                                    <ul id="manualChargeList"  class="list-group">
                                        @if(!empty($manualcharges))
                                        @foreach($manualcharges as $row)
                                        @php $otherChargeAmount =   (new \App\Helpers\customhelper)->getCurrencySymbolFormat($row->otherChargeAmount); @endphp
                                        <li id="othercharge{{$row->id}}" class="list-group-item">
                                            <div class="bulletTxt"><span class="bullet"><img src="{{ asset('public/administrator/img/redsqrBullet.png') }}"></span>{{$row->otherChargeName}} {{!empty($row->notes)?'-'.$row->notes:''}}  - {{$otherChargeAmount}}<br>
                                                Added by: {{$row->createdBy}}</div>
                                            
                                            <span class="deleteBtn"><a data-id="{{$row->id}}" class="deletenewcharge color-theme-2 actionIcons" href="javascript:void(0);" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a></span>
                                                
                                        </li>
                                        @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <span><strong>NOTE:</strong> Extra charges apply for TV's, Hazmat Items, Very Fragile Packages, Items that need to be crated, Liquids Items, Counting of more than 50 items, Customers Using Wrong Unit Numbers or Sharing Unit Numbers, Customers with No Unit Number on packages, Motor Cycles Crating, Customers that want copies of their invoices, Customers shipping car batteries, Any single item valued at more than $2,500, Shipping of used engines by air.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-right">
                    <button id="submitBtn" disabled="" type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Create New Shipment</button>
                </div>
                {{Form:: close()}}
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
    <!--modal open-->
    <div class="modal fade" id="modal-verify" style="display: none;"></div>
    <!--modal close-->
    <script>
      $(function () {
          $("#addeditFrm").validate();
      });
      </script>
</section>
@endsection