<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
<!--font-family: 'Roboto', sans-serif;-->
<style type="text/css">
    body {
        width: 100% !important;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
        margin: 0;
        padding: 0;
        font-size: 12px;
        line-height: 1.4;
    }

    table td {
        border-collapse: collapse;
    }

    img {
        display: block;
        border: 0;
        outline: none;
    }

    a {
        outline: none;
        text-decoration: none;
    }
</style>
@php 
$asseturl = asset('/administrator/img/logo.png');
if (strpos($asseturl,'public') !== false) {
$asseturl = asset('/administrator/img/logo.png');
} else {
$asseturl = asset('/public/administrator/img/logo.png');
}
@endphp  
<table cellspacing="0" cellpadding="0" border="0" width="100%" bgcolor="#ffffff">
    <tr>
        <td align="center" valign="top" width="100%" style="padding:0; margin:0;">
            <table cellspacing="0" cellpadding="0" border="0" class="tableWrap" bgcolor="#ffffff">
                <tr>
                    <td style="padding:15px; margin:0; font-family: 'Roboto', sans-serif; border-bottom: 1px solid #e5e5e5">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td align="left" valign="top" width="32%" style="padding:0; margin:0;">
                                    <img src="{{ $asseturl }}" style="padding:0; margin:0;" />
                                </td>
                                <td align="center" valign="top" width="36%" style="padding:0; margin:0;">
                                    <span style="display: block; font-size: 24px; text-transform: uppercase"><strong>{{$invoice->paymentStatus == 'paid' ? 'RECEIPT' : 'INVOICE'}}</strong></span>
                                    <span style="display: block;">Date: {{ $invoice->createdOn }}</span>
                                    <span style="display: block;">Invoice ID: #{{$invoice->invoiceUniqueId}}</span>
                                    @if(isset($orderNumber) && !empty($orderNumber))
                                    <span style="display: block;">Order No: #{{$orderNumber}}</span>
                                    @endif
                                </td>
                                <td align="right" valign="top" width="32%" style="padding:0; margin:0;">
                                    <span style="display: block"><strong>Shoptomydoor</strong></span>
                                    <span style="display: block">{{json_decode($invoice->invoiceParticulars)->warehouse->fromAddress}}, {{(new \App\Helpers\customhelper)->getCountryStateCityName(json_decode($invoice->invoiceParticulars)->warehouse->fromCity,'city')}}</span>
                                    <span style="display: block">{{json_decode($invoice->invoiceParticulars)->warehouse->fromZipCode}}, {{(new \App\Helpers\customhelper)->getCountryStateCityName(json_decode($invoice->invoiceParticulars)->warehouse->fromState,'state')}}</span>
                                    <span style="display: block">{{(new \App\Helpers\customhelper)->getCountryStateCityName(json_decode($invoice->invoiceParticulars)->warehouse->fromCountry,'country')}}</span>
                                    <span style="display: block">CALL US: 1-888 315 9027</span>
                                    <span style="display: block">Email: contact@shoptomydoor.com</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="padding:15px; margin:0; font-family: 'Roboto', sans-serif; border-bottom: 1px solid #e5e5e5">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td align="left" valign="top" width="20%" style="padding:0; margin:0;">Unit:</td>
                                <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->userUnit}}</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" width="20%" style="padding:0; margin:0;">Name:</td>
                                <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->userFullName}}</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" width="20%" style="padding:0; margin:0;">Email:</td>
                                <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->userEmail}}</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" width="20%" style="padding:0; margin:0;">Contact Number:</td>
                                <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->userContactNumber}}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                @if($invoice->paymentStatus == 'paid')
                <tr>
                    <td align="center" valign="top" width="100%" style="padding:15px 15px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; font-size: 20px; font-weight: 700; color: #1b1b1b; text-transform: uppercase">Paid</td>
                </tr>
                <tr>
                    <td align="center" valign="top" width="100%" style="padding:0 15px 15px 15px; margin:0; font-family: 'Roboto', sans-serif; color: #515151; font-size: 14px;  border-bottom: 1px solid #e5e5e5; color: #515151; font-size: 14px;">Payment
                        By: {{json_decode($invoice->invoiceParticulars)->payment->paymentMethodName}}</td>
                </tr>
                @endif
                <tr>
                    <td style="padding:15px; margin:0; font-family: 'Roboto', sans-serif; border-bottom: 1px solid #e5e5e5">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td align="left" valign="top" width="50%" style="padding:0 15px 0 0; margin:0; word-break: break-all">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td align="left" valign="top" colspan="2" style="padding:0 0 15px; margin:0; font-weight: bold; font-size: 16px">Billing
                                                Address</td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Name:</td>
                                            <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->billingName}}</td>
                                        </tr>

                                        <tr>
                                            <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Email:</td>
                                            <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->billingEmail}}</td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Address:</td>
                                            <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->billingAddress}}</td>
                                        </tr>
                                        @if(!empty($invoice->billingAlternateAddress))
                                        <tr>
                                            <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Alternate Address:</td>
                                            <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->billingAlternateAddress}}</td>
                                        </tr>
                                        @endif
                                        <tr>
                                            <td align="left" valign="top" width="40%" style="padding:0; margin:0;">City:</td>
                                            <td align="left" valign="top" style="padding:0; margin:0;">{{(new \App\Helpers\customhelper)->getCountryStateCityName($invoice->billingCity,'city')}}</td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" width="40%" style="padding:0; margin:0;">State:</td>
                                            <td align="left" valign="top" style="padding:0; margin:0;">{{(new \App\Helpers\customhelper)->getCountryStateCityName($invoice->billingState,'state')}}</td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Country:</td>
                                            <td align="left" valign="top" style="padding:0; margin:0;">{{(new \App\Helpers\customhelper)->getCountryStateCityName($invoice->billingCountry,'country')}}</td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Zip/Postal Code:</td>
                                            <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->billingZipcode}}</td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Contact Number:</td>
                                            <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->billingPhone}}</td>
                                        </tr>
                                        @if(!empty($invoice->billingAlternatePhone))
                                        <tr>
                                            <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Contact Number:</td>
                                            <td align="left" valign="top" style="padding:0; margin:0;">{{$invoice->billingAlternatePhone}}</td>
                                        </tr>
                                        @endif
                                    </table>
                                </td>
                                <td align="left" valign="top" width="50%" style="padding:0; margin:0;">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td align="left" valign="top" colspan="2" style="padding:0 0 15px; margin:0; font-weight: bold; font-size: 16px">Shipping
                                                Address</td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Name:</td>
                                            <td align="left" valign="top" style="padding:0; margin:0;">{{json_decode($invoice->invoiceParticulars)->shippingaddress->toName}}</td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Address:</td>
                                            <td align="left" valign="top" style="padding:0; margin:0;">{{json_decode($invoice->invoiceParticulars)->shippingaddress->toAddress}}</td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" width="40%" style="padding:0; margin:0;">City:</td>
                                            <td align="left" valign="top" style="padding:0; margin:0;">{{(new \App\Helpers\customhelper)->getCountryStateCityName(json_decode($invoice->invoiceParticulars)->shippingaddress->toCity,'city')}}</td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" width="40%" style="padding:0; margin:0;">State:</td>
                                            <td align="left" valign="top" style="padding:0; margin:0;">{{(new \App\Helpers\customhelper)->getCountryStateCityName(json_decode($invoice->invoiceParticulars)->shippingaddress->toState,'state')}}</td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Country:</td>
                                            <td align="left" valign="top" style="padding:0; margin:0;">{{(new \App\Helpers\customhelper)->getCountryStateCityName(json_decode($invoice->invoiceParticulars)->shippingaddress->toCountry,'country')}}</td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Zip/Postal Code:</td>
                                            <td align="left" valign="top" style="padding:0; margin:0;">{{json_decode($invoice->invoiceParticulars)->shippingaddress->toZipCode}}</td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" width="40%" style="padding:0; margin:0;">Contact Number:</td>
                                            <td align="left" valign="top" style="padding:0; margin:0;">{{json_decode($invoice->invoiceParticulars)->shippingaddress->toPhone}}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top" style="padding:15px 0; margin:0; border-bottom: 1px solid #e5e5e5;">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td align="left" valign="top" colspan="3" style="padding:5px 15px; margin:0; font-family: 'Roboto', sans-serif; font-size: 14px; font-weight: bold;">Package
                                    & shipment details</td>

                            </tr>
                            <tr>
                                <td align="left" valign="top" style="padding:5px 15px; margin:0; font-family: 'Roboto', sans-serif;">Total
                                    number of Items: <strong>{{json_decode($invoice->invoiceParticulars)->shipment->totalQuantity}}</strong>
                                </td>
                                <td align="left" valign="top" style="padding:5px 15px; margin:0; font-family: 'Roboto', sans-serif;">Total
                                    Gross Weight: <strong>{{json_decode($invoice->invoiceParticulars)->shipment->totalWeight}} lbs</strong>
                                </td>
                                <td align="left" valign="top" style="padding:5px 15px; margin:0; font-family: 'Roboto', sans-serif;">Total
                                    Billing Amount: <strong>{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat(json_decode($invoice->invoiceParticulars)->shipment->totalCost)}}</strong>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="left" valign="top" style="padding:15px; margin:0; font-family: 'Roboto', sans-serif; font-size: 14px; font-weight: bold;">Package
                        Details</td>

                </tr>
                <tr>
                    <td align="center" valign="top" style="padding:0; margin:0; border-bottom: 1px solid #e5e5e5;">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            @php $deliveryNum = 1; @endphp
                            @foreach(json_decode($invoice->invoiceParticulars)->deliveries as $delivery) 
                            <tr><td colspan="8" align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 12px; font-weight: 700;"><b>Delivery #{{$deliveryNum}} - {{$delivery->shippingMethod}}</b></td></tr> 
                            @php $deliveryNum++; @endphp

                            <tr>
                                <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 12px; font-weight: 700;">#</td>
                                <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 12px; font-weight: 700;">Name
                                    of Item</td>
                                <td align="left" valign="top" width="150" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 12px; font-weight: 700;">URL/Website
                                    Link</td>
                                <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 12px; font-weight: 700;">Color/Size</td>
                                <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 12px; font-weight: 700;">Unit
                                    Price</td>
                                <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 12px; font-weight: 700;">Quantity</td>
                                <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 12px; font-weight: 700;">Shipping</td>
                                <td align="right" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 15px 5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 12px; font-weight: 700;">Item Price</td>
                            </tr>
                            @foreach($delivery->packages as $package)
                            <tr>
                                <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; font-size: 12px;">{{ $package->id }}</td>
                                <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; font-size: 12px;">{{ $package->itemName }}</td>
                                <td align="left" valign="top" style="word-break: break-all; border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; font-size: 12px;">{{ (strlen($package->websiteUrl)>20)? substr($package->websiteUrl,0,20)." ...":$package->websiteUrl }}</td>
                                <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; font-size: 12px;">{{ $package->options }}</td>
                                <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; font-size: 12px;">{{ (new \App\Helpers\customhelper)->getCurrencySymbolFormat($package->itemPrice) }}</td>
                                <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; font-size: 12px;">{{ $package->itemQuantity }}</td>
                                <td align="left" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 5px; margin:0; font-family: 'Roboto', sans-serif; font-size: 12px;">{{ (new \App\Helpers\customhelper)->getCurrencySymbolFormat($package->itemShippingCost) }}</td>
                                <td align="right" valign="top" style="border-top: 1px solid #e5e5e5; padding:5px 15px 5px 5px; margin:0; font-family: 'Roboto', sans-serif; font-size: 12px;">{{ (new \App\Helpers\customhelper)->getCurrencySymbolFormat($package->itemTotalCost) }}</td>
                            </tr>
                            @endforeach
                            @endforeach
                            
                            @if(isset(json_decode($invoice->invoiceParticulars)->shipment->shippingCost))
                            <tr>
                                <td align="left" valign="top" colspan="7" style="padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; text-transform: uppercase; font-weight: 700;">Shipping cost</td>
                                <td align="right" valign="top" style="padding:5px 15px 5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; font-weight: 500;">{{  (new \App\Helpers\customhelper)->getCurrencySymbolFormat(json_decode($invoice->invoiceParticulars)->shipment->shippingCost) }}</td>
                            </tr>
                            @endif
                            @if(isset(json_decode($invoice->invoiceParticulars)->shipment->clearingDutyCost))
                            @if(json_decode($invoice->invoiceParticulars)->shipment->isDutyCharged=='0')
                            <tr>
                                <td align="left" valign="top" colspan="7" style="padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; text-transform: uppercase; font-weight: 700;">Clearing, Port Handling</td>
                                <td align="right" valign="top" style="padding:5px 15px 5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; font-weight: 500;">{{  (new \App\Helpers\customhelper)->getCurrencySymbolFormat(json_decode($invoice->invoiceParticulars)->shipment->clearingDutyCost) }}</td>
                            </tr>
                            @else
                            <tr>
                                <td align="left" valign="top" colspan="7" style="padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; text-transform: uppercase; font-weight: 700;">Clearing, Port Handling and Duty</td>
                                <td align="right" valign="top" style="padding:5px 15px 5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; font-weight: 500;">{{  (new \App\Helpers\customhelper)->getCurrencySymbolFormat(json_decode($invoice->invoiceParticulars)->shipment->clearingDutyCost) }}</td>
                            </tr>
                            @endif
                            @endif
                            @if(isset(json_decode($invoice->invoiceParticulars)->shipment->inventoryCharge))
                            <tr>
                                <td align="left" valign="top" colspan="7" style="padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; text-transform: uppercase; font-weight: 700;">Inventory Charge</td>
                                <td align="right" valign="top" style="padding:5px 15px 5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; font-weight: 700;">{{  (new \App\Helpers\customhelper)->getCurrencySymbolFormat(json_decode($invoice->invoiceParticulars)->shipment->inventoryCharge) }}</td>
                            </tr>
                            @endif
                            @if(isset(json_decode($invoice->invoiceParticulars)->shipment->otherChargeCost))
                            <tr>
                                <td align="left" valign="top" colspan="7" style="padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; text-transform: uppercase; font-weight: 700;">Other Charges</td>
                                <td align="right" valign="top" style="padding:5px 15px 5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; font-weight: 700;">{{  (new \App\Helpers\customhelper)->getCurrencySymbolFormat(json_decode($invoice->invoiceParticulars)->shipment->otherChargeCost) }}</td>
                            </tr>
                            @endif
                            @if(isset(json_decode($invoice->invoiceParticulars)->shipment->storageCharge))
                            <tr>
                                <td align="left" valign="top" colspan="7" style="padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; text-transform: uppercase; font-weight: 700;">Storage Charge</td>
                                <td align="right" valign="top" style="padding:5px 15px 5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; font-weight: 700;">{{  (new \App\Helpers\customhelper)->getCurrencySymbolFormat(json_decode($invoice->invoiceParticulars)->shipment->storageCharge) }}</td>
                            </tr>
                            @endif
                            @if(isset(json_decode($invoice->invoiceParticulars)->shipment->isInsuranceCharged))
                            @if(json_decode($invoice->invoiceParticulars)->shipment->isInsuranceCharged == 'Y')
                            <tr>
                                <td align="left" valign="top" colspan="7" style="padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; text-transform: uppercase; font-weight: 700;">Insurance</td>
                                <td align="right" valign="top" style="padding:5px 15px 5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; font-weight: 500;">{{  (new \App\Helpers\customhelper)->getCurrencySymbolFormat(json_decode($invoice->invoiceParticulars)->shipment->totalInsurance) }}</td>
                            </tr>
                            @endif
                            @endif
                            @if(isset(json_decode($invoice->invoiceParticulars)->shipment->totalTax) && !empty(json_decode($invoice->invoiceParticulars)->shipment->totalTax))
                            <tr>
                                <td align="left" valign="top" colspan="7" style="padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; text-transform: uppercase; font-weight: 700;">Vat</td>
                                <td align="right" valign="top" style="padding:5px 15px 5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; font-weight: 500;">{{  (new \App\Helpers\customhelper)->getCurrencySymbolFormat(json_decode($invoice->invoiceParticulars)->shipment->totalTax) }}</td>
                            </tr>
                            @endif

                            @if(isset(json_decode($invoice->invoiceParticulars)->shipment->discount) && !empty(json_decode($invoice->invoiceParticulars)->shipment->discount))
                            <tr>
                                <td align="left" valign="top" colspan="7" style="padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; text-transform: uppercase; font-weight: 700;">Discount</td>
                                <td align="right" valign="top" style="padding:5px 15px 5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; font-weight: 700;"> - {{  (new \App\Helpers\customhelper)->getCurrencySymbolFormat(json_decode($invoice->invoiceParticulars)->shipment->discount) }}</td>
                            </tr>
                            @endif
                            @if($invoice->extraCostCharged == 'Y')
                            <tr>
                                <td align="left" valign="top" colspan="7" style="padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; text-transform: uppercase; font-weight: 700;">Extra Cost Charged</td>
                                <td align="right" valign="top" style="padding:5px 15px 5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; font-weight: 700;">{{  (new \App\Helpers\customhelper)->getCurrencySymbolFormat(json_decode($invoice->invoiceParticulars)->extracharge->extraCost) }}</td>
                            </tr>
                            @endif
                            @if(isset(json_decode($invoice->invoiceParticulars)->shipment->totalCost))
                            <tr>
                                <td align="left" valign="top" colspan="7" style="padding:5px 5px 5px 15px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; text-transform: uppercase; font-weight: 700;">Total</td>
                                <td align="right" valign="top" style="padding:5px 15px 5px 5px; margin:0; font-family: 'Roboto', sans-serif; color: #1b1b1b; font-size: 14px; border-top: 1px solid #e5e5e5; font-weight: 700;">{{  (new \App\Helpers\customhelper)->getCurrencySymbolFormat(json_decode($invoice->invoiceParticulars)->shipment->totalCost) }}</td>
                            </tr>
                            @endif
                            
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
