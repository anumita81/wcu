<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Verify Shipment</h4>
        </div>
        <div class="modal-body">
            <form id="addFrm" role="form" method="post" action="">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Group Name<!--<span class="text-red">*</span>--></label>
                            <p>{{$group->groupName}}</p>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Coordinator Name</label>
                            <p>{{$user->firstName}} {{$user->lastName}}</p>
                        </div>
                    </div>
                </div>
                @if(!empty($groupmember))                
                   <div class="col-md-6">
                        <div class="form-group">
                            <label>Members Name</label>
                            @foreach($groupmember as $member)
                            <p>{{$member->firstName}} {{$member->lastName}}</p>
                            @endforeach
                        </div>
                    </div> 
                @endif
                @foreach($unpaidShipment as $eachWarehouse=>$unpaidWarehouse)
                 <div class="row">
                    @if(!empty($userWarehouse[$eachWarehouse]))
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Unpaid Shipments  - {{$userWarehouse[$eachWarehouse]}}</label>
                            <div class="idShow">
                                @foreach($unpaidWarehouse as $eachShipmentType=>$unpaidShipments)
                                    <div>
                                        <span>{{ ($eachShipmentType=='fillnship') ? 'Fill and Ship' : $shipmentTypes[$eachShipmentType] }} :</span>
                                        @foreach($unpaidShipments as $eachUnpaidShipment)
                                            <a href="{{url('administrator/groupshipments/addedit/'.$eachUnpaidShipment.'/1')}}">#{{$eachUnpaidShipment}}</a>
                                           
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                @endforeach
                <div class="modal-footer">
                    <div class="text-right">
                        <!--<button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>-->
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
