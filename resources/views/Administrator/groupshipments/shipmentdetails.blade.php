<div class="col-sm-6 col-md-3">
    <div class="info-box shipDtlBoxBlue"> <span class="info-box-icon"><img src="{{ asset('public/administrator/img/detailIcon01.png') }}"></span>
        <div class="info-box-content"> <span class="info-box-number"><small>Gross Weight: </small>{{!empty($shipment['totalWeight'])?$shipment['totalWeight']:'0.00'}} lbs</span> </div>
    </div>
</div>
<div class="col-sm-6 col-md-3">
    <div class="info-box shipDtlBoxSky"> <span class="info-box-icon "><img src="{{ asset('public/administrator/img/detailIcon02.png') }}"></span>
        <div class="info-box-content"> <span class="info-box-number"><small>Chargeable weight: </small>{{!empty($shipment['totalChargeableWeight'])?$shipment['totalChargeableWeight']:'0.00'}} lbs</span> </div>
    </div>
</div>
<div class="col-sm-6 col-md-3">
    <div class="info-box shipDtlBoxGreen"> <span class="info-box-icon "><img src="{{ asset('public/administrator/img/detailIcon03.png') }}"></span>
        <div class="info-box-content"> <span class="info-box-number"><small>Total Shipment Value: </small> {{!empty($shipment['totalValue'])?(new \App\Helpers\customhelper)->getCurrencySymbolFormat($shipment['totalValue']):'0.00'}} </span> </div>
    </div>
</div>
<div class="col-sm-6 col-md-3">
    <div class="info-box shipDtlBoxYellow"> <span class="info-box-icon "><img src="{{ asset('public/administrator/img/detailIcon04.png') }}"></span>
        <div class="info-box-content"> <span class="info-box-number"><small>Number of Deliveries:</small>{{!empty($shipment['totalDelivery'])?$shipment['totalDelivery']:0}}</span> </div>
    </div>
</div>