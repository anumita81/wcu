<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        <div class="modal-body">
        @if(count($delivery)>0)
        
           @foreach($delivery as $item)
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <h5>Delivery #{{$loop->iteration}} - <span>{{ucfirst(str_replace("_", "",$item['status']))}}</span></h5>

                    </div>
                </div>

            </div>
            @endforeach
        @endif
            <div class="modal-footer">
                <div class="text-right">
                    <a href="{{url('administrator/groupshipments/addedit/'.$id.'/1')}}" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Change Status</a>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->



<script>
    $(function () {
        $("#changepasswordFrm").validate();
    });
</script>