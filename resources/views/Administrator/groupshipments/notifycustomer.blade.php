<script src = "{{ asset('public/administrator/controller-css-js/groupshipment.js') }}" ></script>

<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Notify Customer</h4>
        </div>
        <div class="modal-body">
            {{ Form::open(array('url' => 'administrator/groupshipments/savenotification/'.$messageId.'/'.$shipmentId, 'name' => 'notifyFrm', 'id' => 'notifyFrm', 'method' => 'post')) }}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Notify Customer</label>
                        <textarea name="message" id="message" class="form-control" style="height: 300px;" row="10">{{$warehousemsg[0]['message']}}</textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Notify</button>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->



<script>
$(function () {
    $("#notifyFrm").validate();
});
</script>