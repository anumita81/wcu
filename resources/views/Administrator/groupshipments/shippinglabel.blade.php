<script src="{{ asset('public/administrator/js/jQuery.print.js') }}"></script>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            @php $currentPackage = 1; @endphp
            @foreach($packageDetailsData as $eachPackage)
            <div style="border: 1px solid #B0AFBE;padding: 4px; align-content: center; align:center; width: 384px; height: 576px; margin: 0 auto;" id="printContent_{{$eachPackage->id}}">
                <div style="align:center;">
                    <table width="98%" style="align-content: center">

                        <tr>
                            <td align="left" width="40%"><img src="{{ asset('public/administrator/img/logoSTMD.jpg') }}" width="150" height="80">
                            </td>
                            <td width="60%">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center"><font size="2">{{$shipmentData->fromAddress}}</font></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" nowrap="nowrap" align="center" ><font size="2">{{ $deliveryAddress['fromCityName'] }}, {{ $deliveryAddress['fromStateName'] }}, {{ $deliveryAddress['fromCountryName'] }}</font></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><font size="2">shoptomydoor.com</font></td>
                                    </tr>

                                    <tr>
                                        <td align="center"><font size="2">+1 888 315 9027</font></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" nowrap="nowrap" align="center" ><hr style="margin: 10px 0;" /></td>
                        </tr>

                        <tr>
                            <td align="left" width="20%" style="padding-left: 10px;">
                                @if(!empty($deliveryShippingMethod[$currentPackage]['name']) && strstr(strtolower($deliveryShippingMethod[$currentPackage]['name']),'ocean'))
                                <img src="{{ asset('public/administrator/img/ship.png') }}" width="30" height="30">
                                @else
                                <img src="{{ asset('public/administrator/img/plane.png') }}" width="30" height="30">
                                @endif
                                <img src="{{ asset('public/administrator/img/package.png') }}" width="30" height="30">
                                <br /><span style="display:inline-block;font-weight:bold;font-size:20px;font-family: Arial;">{{ $contents }}</span><br/><span style="display:inline-block;font-weight:bold;font-size:20px;font-family: Arial;">{{$type}}</span>
                            </td>
                            <td width="80%" align="center">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td nowrap="nowrap" align="center" ><font size="4"><b>Package : {{ $currentPackage }} of {{$packageCount}}</b></font></td>
                                    </tr>
                                    <tr>
                                        <td nowrap="nowrap" align="center" ><font size="2"><b>Shipment# : {{$shipmentData->id}}/{{$currentPackage}}/{{$packageCount}}<b></font></td>
                                    </tr>
                                    <tr>
                                        <td nowrap="nowrap" align="center" ><font size="2"><b>Order# : {{ $orderId }}/{{$currentPackage}}/{{$packageCount}}<b></font></td>
                                    </tr>	
                                    <tr>
                                        <td nowrap="nowrap" align="center" ><font size="2"><b>Gross Weight: {{ $eachPackage->chargeableWeight }}Lbs<b></font></td>
                                    </tr>
                                    <tr>
                                        <td nowrap="nowrap" align="center" ><font size="2"><b>{{ date('m-d-Y',strtotime($shipmentData->createdOn)) }}<b></font></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" nowrap="nowrap" align="center" ><hr style="margin: 10px 0;" /></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center; font-size: 16px"><b>{{ !empty($deliveryShippingMethod[$currentPackage]['name']) ? $deliveryShippingMethod[$currentPackage]['name'] : ""  }}</b></td>
                        </tr>
                        <tr>
                            <td colspan="2" nowrap="nowrap" align="center" ><hr style="margin: 10px 0;" /></td>
                        </tr>

                        <tr style="padding-left: 10px">
                            <td colspan="2">
                                <table width="100%">
                                    <tr><td width="30" align="left"><font size="2">TO :</font></td>

                                    <td align="left"><font size="2">{{ $deliveryAddress['toName'] }}</font></td>
                                    </tr>
                                    <tr>
                                        
                                        <td colspan="2" align="left"><font size="2">{{$deliveryAddress['toAddress']}}</font></td>
                                    </tr>
                                    <tr>
                                        
                                        <td colspan="2" align="left"><font size="2"><b>{{$deliveryAddress['toCityName']}}&nbsp;{{$deliveryAddress['toStateName']}}&nbsp;&nbsp;{{$deliveryAddress['toCountryName']}}</b></font></td>
                                    </tr>
                                   
                                    @if(!empty($deliveryAddress['toPhone']))
                                    <tr>
                                        
                                        <td colspan="2" align="left"><font size="2">{{$deliveryAddress['toPhone']}} @if(isset($deliveryAddress['toAlternatePhone'])) {{ ",".$deliveryAddress['toAlternatePhone'] }} @endif</font></td>
                                    </tr>
                                    @endif
                                </table>	
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2" nowrap="nowrap" align="center" ><hr style="margin: 10px 0;" /></td>
                        </tr>
                        <tr>
                            @if(!empty($shipmentData->paymentStatus) && $shipmentData->paymentStatus=="paid")
                            @php
                                $orderbarcodetext = $orderId.'/'.$currentPackage.'/'.$packageCount;
                            @endphp
                            <td colspan="2" align="center">
                                <img src="data:image/png;base64,{{DNS1D::getBarcodePNG($orderbarcodetext, 'C39')}}" alt="barcode" width="300px" height="60px" />
                            </td>
                            @else
                            <td colspan="2" style="font-size: 70px;font-weight: bold;line-height: 90px;font-family: Arial;" align="center">NOT PAID</td>
                            @endif
                        </tr>
                        <tr>
                            <td colspan="2" nowrap="nowrap" align="center" ><hr style="margin: 10px 0;" /></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="overflow:hidden;" align="center"><font size="2">Verify contents before signing for the package. Once signed for, contents on invoice is assumed to be complete.</font></td>
                        </tr> 
                        <tr>

                            @if(!empty($shipmentData->id))
                                @php
                                $barcodetext = $shipmentData->id.'/'.$currentPackage.'/'.$packageCount;
                                @endphp
                            <td colspan="2" align="center">
                                <img src="data:image/png;base64,{{DNS1D::getBarcodePNG($barcodetext, 'C39')}}" width="200px" alt="barcode" />
                            </td>
                            @endif
                        </tr>
                        <tr>
                        <td>&nbsp;&nbsp;</td>
                        </tr>   
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-right">
                    <button class="print-link no-print" id="printLabel" onclick="printLabel({{$eachPackage->id}})">
                        Print this
                    </button>
                </div>
            </div>
            @php $currentPackage++ @endphp
            @endforeach
        </div>
    </div>
<!-- /.modal-content -->
<script>
function printLabel(packageId){
    $("#printContent_"+packageId).print({
        deferred: $.Deferred().done(function () {
            //$('#modal-addEdit').modal('hide');
        })
    });
}
</script>
</div>
<!-- /.modal-dialog -->
