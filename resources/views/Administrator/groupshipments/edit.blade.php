@extends('Administrator.layouts.master')
@section('content')

<script src = "{{ asset('public/administrator/controller-css-js/groupshipment.js') }}" ></script>

@php
if($shipment->shipmentStatus == '0') :
$statusTxt = 'Shipment Submitted';
$statusClass = 'greenTxt';
elseif($shipment->shipmentStatus == '1') :
$statusTxt = 'In Warehouse';
$statusClass = 'greenTxt';
elseif($shipment->shipmentStatus == '2') :
$statusTxt = 'In Transit';
$statusClass = 'text-primary';
elseif($shipment->shipmentStatus == '3') :
$statusTxt = 'Customs clearing';
$statusClass = 'text-info';
elseif($shipment->shipmentStatus == '4') :
$statusTxt = 'In destination warehouse';
$statusClass = 'text-warning';
elseif($shipment->shipmentStatus == '5') :
$statusTxt = 'Out for delivery';
$statusClass = 'text-olive';
else :
$statusTxt = 'Delivered';
$statusClass = 'text-success';
endif;
$isPaid = '';
if(!empty($orderData) && ($orderData->status==2 || $orderData->status==5)):
$isPaid = 'disableblock';
endif;
$deliveryShippingMethodAssign = 0;
$totalShipmentCost = 0;
@endphp
<div id="wait" style="display:none;" class="loaderMiddle"></div>
<section class="content-header stepBg" data-spy="affix" data-offset-top="131">
    <ul class="progressbar nav">
        <li class="active"><a href="#shipment-overview">Shipment Overview</a></li>
        <li><a href="#package-shipment-details">Package &amp; Shipment Details</a></li>
        @if($shipment->prepaid == 'N' && empty($shipment->paymentMethodId))
        <li><a href="#other-charges">Storage Charges Details</a></li>
        @endif
        <li><a href="#order-details">Order Details</a></li>
        <li><a href="#packaging">Packaging</a></li>
    </ul>
    @if($shipment->paymentStatus == 'unpaid')
    <button class="btn btn-default custumButt solidBtn" onclick="showAddEdit({{$shipment->id}}, 0, 'groupshipments/addeditdelivery')">Add new Delivery</button>
    @endif
</section>
<!--  Main content -->
<section class="content gapdashboard detailsPage"> 
    <!--Block 01-->
    <div class="row">
        <section class="col-lg-12">
            <div id="shipment-overview">
                <h3>Shipment Details of {{$groupname}}<small>This section allows you to view shipment details.</small></h3>
                <input type="hidden" id="groupId" value="{{$shipment->groupId}}">
                <div class="shadowBox m-b-25">
                    <h4 class="redTxt m-b-15">Shipment #{{$shipment->id}}</h4>
                    <ul class="list-inline shipmentDetail m-b-15">
                        <li class="list-inline-item">Other Shipment</li>
                        <li class="list-inline-item">Status: <span class="{{$statusClass}}">{{$statusTxt}}</span></li>
                    </ul>
                    <div class="ltBlueBox iconItem">
                        <div class="row">
                            <div class="col-sm-3"><span class="icon"><img src="{{ asset('public/administrator/img/calenderIconBlue.png') }}"></span><span class="item"><strong>Shipment Created on</strong>: {{ \Carbon\Carbon::parse($shipment->createdOn)->format('m-d-Y | H:i')}}</span></div>
                            <div class="col-sm-3"><span class="icon"><img src="{{ asset('public/administrator/img/money.png') }}"></span><span class="item"><strong>Total Store Charge</strong>: {{ (new \App\Helpers\customhelper)->getCurrencySymbolFormat(round($totalShipmentStorageCharge,2)) }}</span></div>
                            @if(!empty($wrongInvoice->filename))
                            <!-- <div class="col-sm-3"><span class="icon">Wrong Invoice</span><span class="item"><strong><a href="{{ url('administrator/groupshipments/wronginvoice/download/'.$shipment->id) }}">Download</a></strong></span></div> -->
                            @endif
                        </div>
                    </div>
                </div>
                <div class="shadowBox m-b-25">
                    <div class="row">
                        <div class="col-sm-6  brd-rt p-r-25">
                            <div class="form-group">
                                <label for="sel1">Warehouse message</label>
                                <div class="row">
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control input-lg customSelectBlack" id="shipmentMessage">
                                            <option value="">Select Template</option>
                                            @foreach($warehousemessages as $warehousemessage)
                                            <option value="{{$warehousemessage->id}}">{{$warehousemessage->name}}</option>
                                            @endforeach
                                        </select>
                                        <input type="hidden" id="notify-customer" value="">
                                    </div>
                                    <div class="col-sm-12 col-md-5 text-right"> <a data-toggle="modal" class="btn btn-default custumButt btnBlue"  onclick="showAddEdit($('#notify-customer').val(), {{$shipment->id}}, 'groupshipments/notifycustomer')">Notify Customer</a> </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div id="locationTrack">
                                    @if(!$shipment['warehouselocation']->isEmpty())
                                    <label for="sel1">Location</label>
                                    <div class="table-responsive m-t-20">
                                        <table class="table table-bordered table-hover no-margin">
                                            <thead>
                                                <tr class="headerTblBg">
                                                    <th valign="top" align="left">Row</th>
                                                    <th valign="top" align="left">Zone</th>
                                                    <th valign="top" align="left">Changed By</th>
                                                    <th width="25%" valign="top" align="left">&nbsp;</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($shipment['warehouselocation'] as $warehouselocation)
                                                <tr id="warehouse{{$warehouselocation->id}}">
                                                    <td>{{$warehouselocation->rowName}}</td>
                                                    <td>{{$warehouselocation->zoneName}}</td>
                                                    <td>{{$warehouselocation->firstName." ".$warehouselocation->lastName}}</td>
                                                    <td>
                                                        <a href="javascript:void(0);" onclick="showAddEdit({{$warehouselocation->id}}, {{$shipment->id}}, 'groupshipments/addeditlocation')" class="text-green actionIcons" data-toggle="tooltip" title="Click to Edit"><i class="fa fa-fw fa-edit"></i></a>
                                                        <a class="actionIcons deletelocation color-theme-2" href="javascript:void(0);" data-warehouseid="{{$warehouselocation->id}}"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <br/>
                                    @endif
                                    <div class="form-group {{ $isPaid }}"> <a onclick="showAddEdit(0, {{$shipment->id}}, 'groupshipments/addeditlocation')" class="btn btn-default custumButt btnGreen m-r-15 add_warehouse_location">Add</a>
                                    </div>
                                    @if(!empty($shipment['warehouseLocationLog']))
                                    <div class="form-group">
                                        @foreach($shipment['warehouseLocationLog'] as $row)
                                        <p>{{$row}}</p>
                                        @endforeach
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <button onclick="showAddEdit({{$shipment->id}},{{$page}}, 'groupshipments/printlocationlabel');" class="btn btn-default custumBtnWtIcn printBtn"><span class="btnIcon"><img src="{{ asset('public/administrator/img/faxIcon.png') }}"></span>Print Location Label</button>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control customTxtareaBlack" rows="3" id="addcomment"></textarea>
                                    <span id="showmsg"></span>
                                </div>
                                <div class="form-group" id="showNote">
                                    @foreach($warehouseNotes as $notes)
                                    <div class="row">
                                        <div class="col-sm-12">By: {{\App\Model\Useradmin::where('id', $notes['sentBy'])->pluck('firstname')->first()}} {{\App\Model\Useradmin::where('id', $notes['sentBy'])->pluck('lastname')->first()}}, {{\App\Model\Useradmin::where('id', $notes['sentBy'])->pluck('email')->first()}}, {{\Carbon\Carbon::parse($notes['sentOn'])->format('Y-m-d')}}, <a onclick="showAddEdit({{$notes['id']}}, {{$notes['shipmentId']}}, 'groupshipments/shownotes')"><strong>Comment Details</strong></a>. </div>
                                    </div>
                                    @endforeach
                                    <div class="row">
                                        <div class="col-sm-6 col-md-2">
                                            <button class="btn btn-default custumButt solidBtn m-r-15" onclick="saveComment({{$shipment->id}})">Save</button>
                                        </div>
                                        <div class="col-sm-6 col-md-3"> <a class="btn btn-default custumButt solidBtn m-r-15" onclick="showAddEdit(0, {{$shipment->id}}, 'groupshipments/sendcomment')">Save &amp; Notify</a> </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 p-l-25">
                            <h4>Shipment</h4>
                            <div class="row shipList">
                                <div class="col-sm-6 col-xs-6">
                                    <h5 class="skyTxt">From</h5>
                                    <p>Unit: <strong>{{$userData->unit}}</strong></p>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <h5 class="skyTxt">To</h5>
                                    <p>Coordinator Name: <strong>{{$deliveryAddress['toName']}}</strong></p>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <p>Country: <strong>{{!empty($countryList[$shipment->fromCountry]) ? $countryList[$shipment->fromCountry] : "N/A" }}</strong></p>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <p>Country:<strong>{{!empty($countryList[$deliveryAddress['toCountry']]) ? $countryList[$deliveryAddress['toCountry']] : "N/A" }}</strong></p>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <p>State:<strong>{{!empty($stateList[$shipment->fromState])?$stateList[$shipment->fromState]:'N/A'}}</strong></p>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <p>State: <strong>{{!empty($stateList[$deliveryAddress['toState']])?$stateList[$deliveryAddress['toState']]:'N/A'}}</strong></p>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <p>City: <strong>{{!empty($cityList[$shipment->fromCity])?$cityList[$shipment->fromCity]:'N/A'}}</strong></p>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <p>City: <strong>{{!empty($cityList[$deliveryAddress['toCity']])?$cityList[$deliveryAddress['toCity']]:'N/A'}}</strong></p>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <p>Address: <strong>{{!empty($shipment->fromAddress)?$shipment->fromAddress:'N/A'}}</strong></p>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <p>Address: <strong>{{!empty($deliveryAddress['toAddress'])?$deliveryAddress['toAddress']:'N/A'}}</strong></p>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <p>Email: <strong>{{!empty($shipment->fromEmail)?$shipment->fromEmail:'N/A'}}</strong></p>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <p>Email: <strong>{{!empty($deliveryAddress['toEmail'])?$deliveryAddress['toEmail']:'N/A'}}</strong></p>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <p>Phone: <strong>{{!empty($shipment->fromPhone)?$shipment->fromPhone:'N/A'}}</strong></p>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <p>Phone: <strong>{{!empty($deliveryAddress['toPhone'])?$deliveryAddress['toPhone']:'N/A'}}</strong></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 m-t-20 text-center {{ $isPaid }}"> <a onclick="showAddEdit({{$shipment->id}}, {{$shipment->userId}}, 'groupshipments/changeaddress')" class="btn btn-default custumButt btnBlue">Change Delivery Address</a> </div>
                                <div class="col-sm-12 m-t-20 text-center">
                                    <button onclick="showAddEdit({{$shipment->id}},{{$page}}, 'groupshipments/printdropslip')"  class="btn btn-default custumBtnWtIcn printBtn "><span class="btnIcon"><img src="{{ asset('public/administrator/img/faxIcon.png') }}"></span>Print Drop Off Slip</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($shipment->paymentStatus == 'paid' && $shipment->prepaid=='Y')
                <h3 class="text-success text-center p-t-20">This is a prepaid shipment. Shipment has been fully Paid.</h3>
            @elseif($shipment->paymentStatus == 'paid' && $shipment->prepaid=='N')
                <h3 class="text-success text-center p-t-20">Shipment has been fully Paid.</h3>
            @else
                <h3 class="text-red text-center p-t-20">This Shipment has not been Paid.</h3>
            @endif
            <div id="deliveryDetails" class="shadowBox clearfix m-b-25">
                <div id="package-shipment-details">    
                    <div class="flex-colapse">
                        <h3>Package &amp; Shipment Details</h3>          
                        <a class="btn btn-default custumButt solidBtn" id="loadDeliveryDetails" onclick="loadDeliveryDetails({{$shipment->id}},'othershipment')">load delivery details</a>
                        <a class="btn btn-default custumButt solidBtn" style="visibility: hidden">load delivery details</a>
                    </div>
                    <div class="deliveryDtlsColps">
                            
                    </div>
                </div>
            </div>
            <div id="other-charges">
                <h3>Storage Charges Details</h3>
                <div class="shadowBox clearfix m-b-25">
                    <div class="ltGrayBox m-b-25">
                        @if($totalShipmentStorageCharge!='0.00')
                        @php $storageCharge =   (new \App\Helpers\customhelper)->getCurrencySymbolFormat(round($totalShipmentStorageCharge,2)); @endphp
                        <h4 id="storageChargeTxt" class="text-black m-b-25"> Storage Charge - Maximum storage date exceeded - {{$storageCharge}}<br>
                            @if(!empty($shipment->storageChargeModifiedBy))
                            <small class="text-black">Changed By: {{$shipment->storageChargeModifiedBy}} {{\Carbon\Carbon::parse($shipment->storageChargeModifiedOn)->format('m-d-Y | H:i')}}</small>
                            @endif
                        </h4>
                        @endif
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="storageCharge">Set Storage Charge Manually (enter zero value for automatic calculation) : </label>
                                </div>
                                <div class="col-sm-2">
                                    <input name="storageCharge" id="storageCharge" class="form-control format input-lg customInputBlack" required="" type="text">
                                </div>
                                <div class="col-sm-2 text-left">
                                    <button id="storageChrgBtn" onclick="updateStorageCharge({{$shipment->id}})" class="btn btn-default custumButt btnBlue">Save</button>
                                </div>
                            </div>
                        </div>
                        <!--<div class="h4 text-black">Total Other Charges: <span id="totalShipmentChrg" >{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat($shipment['totalOtherCharges'])}}</span></div>-->
                    </div>
                </div>
            </div>

            <div id="order-details">
                <h3>Order Details</h3>
                <div class="shadowBox clearfix m-b-25">                   
                    <div class="row">
                        <div class="col-sm-10"><h4>Invoices</h4></div>
                        @if($shipment->paymentStatus == "unpaid")
                            @if($adminRole == "1" ||  $adminRole == "10")
                                <div class="col-sm-2"><button type="button" onclick="showAddEdit({{$shipment->id}}, 0,  'groupshipments/receivedPayment');" class="btn btn-info">Confirm Payment</button></div>
                            @endif
                        @endif
                        @if($shipment['invoices']->isEmpty() && $shipment->paymentStatus == "paid")
                            <div class="col-sm-2"><button type="button" onclick="generateInvoice({{$shipment->id}}, 'othershipment');" class="btn btn-info">Generate Invoice</button></div>

                        @endif
                    </div>
                    <div class="row">
                     <div class="col-md-12">
                        <div class="table-responsive m-t-20">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr class="headerTblBg">
                                        <th>#</th>
                                        <th>Invoice No.</th>
                                        <th>Invoice Date</th>
                                        <th>Total Amount Paid</th>
                                        <th>Payment Status</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!$shipment['invoices']->isEmpty())
                                    @foreach($shipment['invoices'] as $invoice) 
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$invoice->invoiceUniqueId}}</td>
                                        <td>{{\Carbon\Carbon::parse($invoice->createdOn)->format('m-d-Y')}}</td>
                                        <td>{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat($invoice->totalBillingAmount)}}</td>
                                        <td>
                                            @if(!empty($invoice->paymentMethodId) && $invoice->paymentStatus=='unpaid' && $invoice->extraCostCharged=='Y')
                                            <select name="paymentStatus" onchange="updatePaymentStatus({{$invoice->id}}, this.value)" class="form-control customSelectBlack" id="paymentStatus">
                                                <option {{$invoice->paymentStatus=='unpaid'?'selected="selected"':''}} value="unpaid">Unpaid</option>
                                                <option {{$invoice->paymentStatus=='paid'?'selected="selected"':''}} value="paid">Paid</option>
                                            </select>
                                            @else 
                                            <label class="btn btn-sm {{$invoice->paymentStatus=='paid'? 'btn-success' : 'btn-danger'}}">{{$invoice->paymentStatus=='paid'? 'Paid' : 'Unpaid'}}</label>
                                            @endif
                                        </td>
                                        <td><button type="button" onclick="showAddEdit({{$shipment->id}}, {{$invoice->id}},  'groupshipments/printinvoice');" class="btn btn-default custumBtnWtIcn printBtns"><i class="fa fa-print"></i> Print</button></td>
                                    </tr>
                                    @endforeach                         
                                    @endif
                                </tbody>
                            </table>
                            <div class="ltRedBox text-center m-b-25">
                                <div class="row">
                                    <div class="col-sm-12 m-t-20">
                                        <div class="row">
                                            @if(isset($delivery['id']) && !empty($delivery['id']))
                                            <ul class="shipingCost deliveryShipingCost-{{ $delivery['id'] }}">
                                                <li>Total Insurance: <strong>{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat(($shipment->totalInsurance))}}</strong></li>
                                                <li>Total Discount: <strong>{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat(($shipment->totalDiscount))}}</strong></li>
                                                <li>Total Tax: <strong>{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat(($shipment->totalTax))}}</strong></li>
                                                
                                            </ul>
                                            @endif
                                            <div class="col-sm-4 text-left">
                                                <label>Payment Status: </label>
                                                @if(!empty($shipment->paymentMethodId) && !empty($shipment->paymentStatus == 'paid'))
                                                <h3><small>Payment Made</small></h3>
                                                @elseif(!empty($shipment->paymentMethodId) && $shipment->paymentStatus == 'unpaid')
                                                <select name="paymentStatus" class="form-control input-lg customSelectBlack" id="paymentInvoiceStatus">
                                                    <option value="unpaid">Unpaid</option>
                                                    <option value="paid">Paid</option>
                                                </select>
                                                @else 
                                                <h3><small>Payment Not Made</small></h3>
                                                @endif
                                            </div>
                                            <div class="col-sm-4 text-left">
                                                <label>Payment Method: </label>
                                                @if(!empty($shipment->paymentMethodId))
                                                    {{ $paymentMethodList[$shipment->paymentMethodId] }}
                                                @else 
                                                <h3><small>Payment Not Made</small></h3>
                                                @endif

                                            </div>
                                            <div class="col-sm-4 text-left">
                                                <h3><small>TOTAL:</small><span class="txtRed">{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat(($shipment->totalCost))}}</span> </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-t-20 col-sm-12 text-center">
                                @if($shipment['paymentStatus']=='unpaid' && $shipment['paymentMethodId']!='')
                                    @if($adminRole == "1" ||  $adminRole == "10")
                                        <button onclick="updatePayment({{$shipment->id}},'othershipment')"  class="btn btn-default custumButt btnBlue updatePayment">Update</button>
                                    @endif
                                @else
                                @if(empty($orderData) && $shipment['paymentStatus']=='paid' && $deliveryShippingMethodAssign==1)
                                <a href="{{ route('shipmentcreateorder',['shipmentId'=>$shipment->id,'page'=>$page]) }}" class="btn btn-default solidBtn btnBlue">Create Order</a>
                                @elseif(!empty($orderData))
                                <a class="btn btn-default custumButt btnGreen disabledIcon">Order Created</a>
                                @endif
                                @endif

                            </div>
                            @if($shipment['paymentStatus']=='paid' && $shipment['paymentMethodId']!='')
                                @if($shipment['shipmentStatus']=='1' || $shipment['shipmentStatus']=='0')
                                <h4>Create Invoice</h4>
                                {{ Form::open(array('url' => 'administrator/groupshipments/createextrainvoice/'.$shipment->id, 'name' => 'invoiceFrm', 'id' => 'invoiceFrm', 'method' => 'post')) }}
                                <div class="form-group m-t-20">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label for="sel1">Cost: $ </label>
                                            <input class="form-control input-lg numFormat customInputBlack" value="" id="extraCost" name="extraCost" type="text">
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="sel1">Notes:</label>
                                            <input class="form-control input-lg customInputBlack" id="notes" name="notes" type="text">
                                        </div>

                                        <div class="col-sm-2 text-left p-t-20">
                                            <button type="submit" id="createInvoiceBtn" class="btn btn-default custumButt btnBlue">Create</button>
                                        </div>

                                    </div>
                                </div>
                                {{ Form::close() }}           
                                @endif
                            @endif
                        </div>
                     </div>
                   </div>
                </div>
            </div>
            <div id="packaging">
                <h3>Add New Package</h3>               
                <div class="shadowBox m-b-25">
                    @php 
                        $packedBtnDisabled = "disableblock";
                        if(!empty($packageAcceptableLimit) && $packageAcceptableLimit['code']==1)
                        $packedBtnDisabled = "";
                        $printBtnDisabled = "Disabled"; 
                        if((!empty($packageAcceptableLimit) && $packageAcceptableLimit['code']==1) && $shipment->packed=='packing_complete')
                        $printBtnDisabled = "";
                        if(empty($packageAcceptableLimit))
                        $printBtnDisabled = "Disabled";
                        if($extraChargeExist == 1)
                        $printBtnDisabled = "Disabled";
                        @endphp
                    @if($shipment->packed=='packing_complete')
                    <h3 class="text-success text-center p-t-20">Packaging is complete for this shipment.</h3>
                    @endif
                    <div id="packingDetails" class="{{($shipment->packed=='packing_complete')?'disableblock':''}}">
                         <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-12 text-left">
                                <span><strong>Step 1 : </strong></span>

                                <a class="btn btn-default custumButt btnGreen" onclick="showAddEdit({{$shipment->userId}}, 0, 'users/getusershipmentsettings');">View Shipment Settings</a>

                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-12 text-left">
                                <span><strong>Step 2 : </strong></span>

                                @if($shipment->paymentStatus == 'paid' && isset($orderData->status) &&  ($orderData->status== '2' || $orderData->status== '5') && $shipment->allowedPackaging=='N')
                                {{ Form::open(array('url' => route('shipmentstartpackaging',['id'=>$shipment->id,'page'=>$page]), 'name' => 'frmstartpkg', 'id' => 'frmstartpkg', 'method' => 'post')) }}

                                <a class="btn btn-default custumButt btnGreen" onclick="showAddEdit({{$shipment->id}}, {{$page}}, 'groupshipments/validatepackage');">Start Packaging</a>
                                <input type="hidden" name="packageShipment" id="packageShipment">
                                {{ Form::close() }}
                                @else
                                <a class="btn btn-default custumButt btnGreen disabledIcon">Start Packaging</a>
                                @endif

                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-12 text-left">
                                <span><strong>Step 3 : </strong></span>
                                <button class="btn btn-default custumBtnWtIcn printBtn" {{ ($shipment->allowedPackaging=='N')?'disabled="disabled"' : ''}} onclick="showAddEdit({{$shipment->id}}, {{ $page }}, 'groupshipments/printmanifest')"><span class="btnIcon"><img src="{{ asset('public/administrator/img/faxIcon.png') }}"></span>Print Verification Manifest</button>
                            </div>
                        </div>
                        <div class="packaging_step_3">
                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-sm-12 text-left">
                                    <span><strong>Step 4 : </strong></span>
                                    <span><strong>Add Package Details</strong></span>
                                    {{ Form::open(array('url' => route('shipmentnewpackaging',['id'=>$shipment->id,'page'=>$page]), 'name' => 'frmaddpkg', 'id' => 'frmaddpkg', 'method' => 'post')) }}    
                                    <div class="row"> 
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Length</label>
                                                    <input class="form-control input-lg customInputBlack numFormat" name="length" id="packagingLength" type="text">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Width</label>
                                                    <input class="form-control input-lg customInputBlack numFormat" name="width" id="packagingWidth" type="text">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Height</label>
                                                    <input class="form-control input-lg customInputBlack numFormat" name="height" id="packagingHeight" type="text">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Weight</label>
                                                    <input class="form-control input-lg customInputBlack numFormat" name="weight" id="packagingWeight" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            &nbsp;
                                        </div>
                                        <div class="col-sm-3 text-left p-t-20">
                                            <button type="submit" id="addPackaging" data-shipment="{{ $shipment->id }}" data-page="{{ $page }}" class="btn btn-default custumButt btnGreen" {{ ($shipment->allowedPackaging=='N')?'disabled="disabled"' : ''}}>Add New Package</button>
                                        </div>
                                    </div>
                                    {{ Form::Close() }}
                                    <div class="clearfix"></div>

                                    @if(count($packageDetailsData)>0)
                                    <div class="table-responsive m-t-20" id="packaginTab">
                                        {{ Form::open(array('url' => route('shipmenteditpackaging',['id'=>$shipment->id,'page'=>$page]), 'name' => 'frmpkgdetails', 'id' => 'frmpkgdetails', 'method' => 'post')) }}
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="headerTblBg">
                                                    <th>&nbsp;</th>
                                                    <th>Length </th>
                                                    <th>Width </th>
                                                    <th>Height </th>
                                                    <th>Weight </th>
                                                    <th>Chargeable weight </th>
                                                    <th>Package added by:</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php 
                                                $grossWeight = 0;
                                                $grossChWeight = 0;
                                                $numPackages = 0;
                                                @endphp
                                                @foreach($packageDetailsData as $eachPackageData)
                                                @php 
                                                $numPackages++; 
                                                $grossWeight += $eachPackageData->weight;
                                                $grossChWeight += $eachPackageData->chargeableWeight;
                                                @endphp
                                                <tr id="packagingrow_{{$eachPackageData->id}}">
                                                    <td class="withCheck">
                                                        <label>
                                                            <input type="checkbox" name="update[{{$eachPackageData->id}}][cb]" value="{{ $eachPackageData->id }}" class="checkbox flat-red cb_{{$eachPackageData->id}}">
                                                        </label>
                                                    </td>
                                                    <td valign="top" align="left">
                                                        <div class="value_label"> 
                                                            {{ $eachPackageData->length }}
                                                        </div>
                                                        <div class="value_field" style="display: none"><input class="form-control input-lg customInputBlack" value="{{$eachPackageData->length}}" name="update[{{$eachPackageData->id}}][length]" type="text"></div>
                                                    </td>
                                                    <td valign="top" align="left">
                                                        <div class="value_label"> 
                                                            {{ $eachPackageData->width }}
                                                        </div>
                                                        <div class="value_field" style="display: none"><input class="form-control input-lg customInputBlack" value="{{$eachPackageData->width}}" name="update[{{$eachPackageData->id}}][width]" type="text"></div>
                                                    </td>
                                                    <td valign="top" align="left">
                                                        <div class="value_label"> 
                                                            {{ $eachPackageData->height }}
                                                        </div>
                                                        <div class="value_field" style="display: none"><input class="form-control input-lg customInputBlack" value="{{$eachPackageData->height}}" name="update[{{$eachPackageData->id}}][height]" type="text"></div>
                                                    </td>
                                                    <td valign="top" align="left">
                                                        <div class="value_label"> 
                                                            {{ $eachPackageData->weight }}
                                                        </div>
                                                        <div class="value_field" style="display: none"><input class="form-control input-lg customInputBlack" value="{{$eachPackageData->weight}}" name="update[{{$eachPackageData->id}}][weight]" type="text"></div>
                                                    </td>

                                                    <td>{{ $eachPackageData->chargeableWeight }}</td>
                                                    <td>{{ isset($eachPackageData->packageaddedby)? $eachPackageData->packageaddedby->firstName." ".$eachPackageData->packageaddedby->lastName:"N/A"}}</td>
                                                    <td class="text-center buttMrg">
                                                        <a href="javascript:void(0)" data-toggle="confirmation" data-id="{{ $eachPackageData->id }}" class="removePackaging"><img src="{{ asset('public/administrator/img/deleteIconRedSm.png') }}"></a> 
                                                        <a href="javascript:void(0)" class="editPackage" data-id="{{ $eachPackageData->id }}"><img src="{{ asset('public/administrator/img/editIconSm.png') }}"></a>
                                                        <a href="javascript:void(0);" style="display:none;" class="savePackaging" data-id="{{ $eachPackageData->id }}"><img data-target="tooltip" height="18" class="save-item" title="Click to Update" src="{{ asset('public/administrator/img/download-button.png') }}"></a>
                                                    </td>

                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                        <div class="col-sm-12 text-center m-b-25">
                                            <button class="btn btn-default custumButt btnBlue" name="action" value="update">Update Selected</button>&nbsp;&nbsp;
                                            <button class="btn btn-default custumButt btnDelete" data-toggle="confirmation" name="action" value="delete">Delete Selected items</button>
                                        </div>
                                        {{ Form::close() }}
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="row shipDetail m-b-25">
                                        <div class="col-sm-4">
                                            <div class="info-box shipDtlBoxBlue"> <span class="info-box-icon"><img src="{{ asset('public/administrator/img/detailIcon01.png') }}"></span>
                                                <div class="info-box-content"> <span class="info-box-number"><small>Gross Weight: </small>{{ $grossWeight }}lbs</span> </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="info-box shipDtlBoxSky"> <span class="info-box-icon "><img src="{{ asset('public/administrator/img/detailIcon02.png') }}"></span>
                                                <div class="info-box-content"> <span class="info-box-number"><small>Chargeable weight: </small>
                                                        {{ $grossChWeight }}lbs</span> </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="info-box shipDtlBoxYellow"> <span class="info-box-icon "><img src="{{ asset('public/administrator/img/detailIcon04.png') }}"></span>
                                                <div class="info-box-content"> <span class="info-box-number"><small>Number of Packages:</small>{{ $numPackages }}</span> </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="clearfix"></div>

                                    <div class="form-group">
                                        <label>Packaging Details: </label>
                                        @if(!empty($packageAcceptableLimit))
                                        {!! $packageAcceptableLimit['message'] !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- step 3 end --> 
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-12 text-left">
                                <span><strong>Step 5 : </strong></span>
                                <a class="btn btn-default custumButt btnBlue {{ $packedBtnDisabled }}" onclick="savepacked({{ $shipment->id }})">Save Package Details</a>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-sm-12 text-left">
                            <span><strong>Step 6 : </strong></span>
                            <span><strong>Add any packaging note if applicable</strong></span>
                        </div>
                        {{ Form::open(array('url' => route('shipmentpackagedetails',['id'=>$shipment->id,'page'=>$page]), 'name' => 'packagenotes', 'id' => 'packagenotes', 'method' => 'post')) }}
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea class="form-control customTxtareaBlack" rows="3" id="comment" name="packageDetails"></textarea>
                            </div>
                            <div class="col-sm-12 text-left m-b-25">
                                <div class="row">
                                    <button {{ count($packageDetailsData)>0 ? '' : 'disabled' }} class="btn btn-default custumButt btnBlue">Save</button>

                                </div>
                            </div>
                            <div class="col-sm-12 text-left m-b-25">
                                @if(!empty($packageDetailsCommentData))
                                @foreach($packageDetailsCommentData as $eachPackageDetails)
                                <div class="row">
                                    <div class="col-sm-12 col-md-7">By: {{$eachPackageDetails->user}}, {{$eachPackageDetails->date}}, <strong>Package Details:</strong>{{ $eachPackageDetails->message }} </div>
                                </div>

                                @endforeach
                                @endif
                            </div>
                        </div>
                        {{ Form::close() }}        
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-sm-12 text-left">
                            <span><strong>Step 7 : </strong></span>
                            <button class="btn btn-default custumBtnWtIcn printBtn" {{ $printBtnDisabled }} onclick="showAddEdit({{$shipment->id}}, {{ $page }}, 'groupshipments/printmanifest')"><span class="btnIcon"><img src="{{ asset('public/administrator/img/faxIcon.png') }}"></span>Print Customer Manifest</button>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-sm-12 text-left">
                            <span><strong>Step 8 : </strong></span>
                            <button class="btn btn-default custumBtnWtIcn printBtn" {{ $printBtnDisabled }} onclick="showAddEdit({{$shipment->id}}, {{ $page }}, 'groupshipments/getshippinglabelfields')"><span class="btnIcon"><img src="{{ asset('public/administrator/img/faxIcon.png') }}"></span>Print Shipping Label</button>
                        </div>
                        <div class="col-sm-12 text-left">
                            <span><strong>Step 9 : </strong></span>
                        </div>
                        <div id="stages" class="{{ ($printBtnDisabled!='') ? 'disableblock' : '' }}">
                            <span>&nbsp;</span>
                            {{ Form::open(array('url' => route('updatepackagestages',['shipmentId'=>$shipment->id]), 'name' => 'updatepackagestages', 'id' => 'updatepackagestages', 'method' => 'post')) }}
                            <input type="hidden" name="assignPackageCompany" id="assignPackageCompany" value="">
                            <div class="shadowBox m-b-25">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="sel1">In Transit <span onclick="showAddEdit('{{$shipment->id}}', 'in_transit', 'groupshipments/updatestatuscomment')"><i class="fa fa-info-circle" aria-hidden="true"></i></span></label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" {{ !empty($packageStatusLog->in_transit) ? "readonly" : '' }} class="form-control datetimepicker" name="status[in_transit]" value="{{ !empty($packageStatusLog->in_transit) ? $packageStatusLog->in_transit : '' }}" {{ !empty($packageStatusLog->in_transit) ? "disabled" : '' }}>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="sel1">Customs Clearing <span onclick="showAddEdit('{{$shipment->id}}', 'custom_clearing', 'groupshipments/updatestatuscomment')"><i class="fa fa-info-circle" aria-hidden="true"></i></span></label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" {{ !empty($packageStatusLog->custom_clearing) ? "readonly" : '' }} class="form-control datetimepicker" name="status[custom_clearing]" value="{{ !empty($packageStatusLog->custom_clearing) ? $packageStatusLog->custom_clearing : '' }}" {{ !empty($packageStatusLog->custom_clearing) ? "disabled" : '' }}>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="sel1">In Destination Warehouse <span onclick="showAddEdit('{{$shipment->id}}', 'destination_warehouse', 'groupshipments/updatestatuscomment')"><i class="fa fa-info-circle" aria-hidden="true"></i></span></label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" {{ !empty($packageStatusLog->destination_warehouse) ? "readonly" : '' }} class="form-control datetimepicker" name="status[destination_warehouse]" value="{{ !empty($packageStatusLog->destination_warehouse) ? $packageStatusLog->destination_warehouse : '' }}" {{ !empty($packageStatusLog->destination_warehouse) ? "disabled" : '' }}>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="sel1">Out For Delivery <span onclick="showAddEdit('{{$shipment->id}}', 'out_for_delivery', 'groupshipments/updatestatuscomment')"><i class="fa fa-info-circle" aria-hidden="true"></i></span></label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" {{ !empty($packageStatusLog->out_for_delivery) ? "readonly" : '' }} class="form-control datetimepicker" name="status[out_for_delivery]" value="{{ !empty($packageStatusLog->out_for_delivery) ? $packageStatusLog->out_for_delivery : '' }}" {{ !empty($packageStatusLog->out_for_delivery) ? "disabled" : '' }}>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="sel1">Delivered <span onclick="showAddEdit('{{$shipment->id}}', 'delivered', 'groupshipments/updatestatuscomment')"><i class="fa fa-info-circle" aria-hidden="true"></i></span></label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" {{ !empty($packageStatusLog->delivered) ? "readonly" : '' }} class="form-control datetimepicker" name="status[delivered]" value="{{ !empty($packageStatusLog->delivered) ? $packageStatusLog->delivered : '' }}" {{ !empty($packageStatusLog->delivered) ? "disabled" : '' }}>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <div class="input-group">
                                                @if($shipment->paymentStatus == 'paid')
                                                <a class="btn btn-default custumButt btnGreen" onclick="updatePackageDeliveryStage({{$shipment->id}})">Update Stage</a>
                                                @else
                                                <a class="btn btn-default custumButt btnGreen" disabled>Update Stage</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                        <div class="col-sm-12 text-left">
                            <h3>Other Shipping Labels @if($extraChargeExist==1)<span class="font-size-base txtRed">Unpaid Extra Invoice</span>@endif</h3>
                            <button {{ $printBtnDisabled }} class="btn btn-default custumButt btnBlue m-r-15" onclick="showAddEdit({{$shipment->id}}, {{ $page }}, 'groupshipments/redstarlabel')">Red Star Label</button>
                            <button {{ $printBtnDisabled }} class="btn btn-default custumButt btnBlue m-r-15" onclick="showAddEdit({{$shipment->id}}, {{ $page }}, 'groupshipments/dhl')">DHL Label</button>
                            <button {{ $printBtnDisabled }} class="btn btn-default custumButt btnBlue m-r-15" onclick="showAddEdit({{$shipment->id}}, {{ $page }}, 'groupshipments/dhlci')"> DHL Commercial Invoice</button>
                            <button {{ $printBtnDisabled }} class="btn btn-default custumButt btnBlue m-r-15" onclick="showAddEdit({{$shipment->id}}, {{ $page }}, 'groupshipments/nationsdelivery')">Nations Delivery</button>
                            <br/>
                            <div class="form-group" id="showNote">
                                @if(!empty($shippingLabelLog))
                                @foreach($shippingLabelLog as $eachLog)
                                <div class="row">
                                    <div class="col-sm-12">
                                        {{$eachLog->labelType}} printed by: {{$eachLog->createdByEmail}} ,on {{\Carbon\Carbon::parse($eachLog->createdOn)->format('Y-m-d h:i:s')}}
                                        @if(!empty($eachLog->labelFile))
                                        , <a href="{{ url('administrator/groupshipments/downloadlabel/'.$eachLog->id) }}" title="Download Label">{{ $eachLog->labelFile }}</a>
                                        @endif
                                    </div>
                                </div>
                                @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
                </div>
                @php
                    if($shipmentDetailsViewed ==1)
                    {
                    echo '<script type="text/javascript">
                        $(document).ready(function() {
                            $("#loadDeliveryDetails").trigger("click");
                        });
                    </script>';
                    }
                @endphp
                <input type="hidden" name="shipmentId" id="shipmentId" value="{{$shipment->id}}" />
                <!--modal open-->
                <div class="modal fade" id="modal-addEdit"></div>
                <!--modal close-->

        </section>
        <!--  \Main content -->

        <div class="modal fade" id="modal-upload" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Upload Snapshot Image</h4>
                    </div>
                    {{ Form::open(array('url' => url('administrator/groupshipments/uploadimage'),  'files'=>true, 'name' => 'uploadFrm', 'id' => 'uploadFrm', 'method' => 'post')) }}
                    <div class="modal-body">
                        <div class="row m-t-15">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Picture</label>
                                    <label for="file-upload" class="custom-file-upload btn-primary">
                                        Browse image 
                                    </label>
                                    <small style="color: red">Please select all images at a time (maximum 5 snapshots per delivery)</small>
                                    <input data-uploadallowed="5" id="file-upload" name='upload_cont_img[]' multiple type="file" style="display:none;">
                                </div>

                                <div class="default-Noimg">
                                    <img id='defaultLoaded' src="{{ asset('public/administrator/img/default-no-img.jpg') }}"/>
                                    <img id="loaded" src="#" alt="" style='display:none;' />                                   
                                    <span class="closeImg"><i class="fa fa-times"></i></span>
                                </div>
                                <input type="hidden" name="shipmentId" id="shipmentId" value="{{$shipment->id}}" />
                                <input type="hidden" name="deliveryId" id="deliveryId" />
                                <input type="hidden" name="packageId" id="packageId" />

                            </div>
                            <div class="images col-md-12"></div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Upload</button>
                    </div>
                    {{ Form::close()}}

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="modal-delivery-details" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Delivery Details</h4>
                    </div>
                    <div class="modal-body"></div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!--modal open-->
    <div class="modal fade" id="modal-deliveryDetails" style="display: none;">
        <!--modal-dialog-->
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>-->
                    <h4 class="modal-title">Delivery Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Delivery Company:</label>
                                <select name="deliveryCompanyId" id="deliveryCompanyId" class="customSelect form-control">
                                    <option value="">Select Delivery Company</option>
                                    @foreach($deliveryCompanyList as $deliveryCompany)
                                    <option value="{{$deliveryCompany->id}}">{{$deliveryCompany->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Delivered: <span class="text-red">*</span></label>
                                <input id="delivered" name="delivered" class="form-control datepicker" readonly="" required="" placeholder="Select Date" type="text">
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tracking:</label>
                                <input id="trackingNumber" name="trackingNumber" class="form-control" required="" placeholder="Enter Tracking ID" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Note:</label>
                                <input id="note" name="note" class="form-control" required="" placeholder="Enter Delivery Note" type="text">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="selectedReceivedDroop" value="" />
                    <div class="modal-footer">
                        <div class="text-right">
                            <button type="submit" class="btn btn-success" id="saveDeliveryDetailsEdit"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--modal close-->
        <script type="text/javascript">
            $(document).ready(function() {
                $(".progressbar.nav li:first-child").addClass("active");
                $(".progressbar.nav li:last-child").removeClass("active");
            });
        </script>
        @endsection