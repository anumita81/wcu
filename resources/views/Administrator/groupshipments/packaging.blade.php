@php 
$printBtnDisabled = ""; 
if(!empty($packageAcceptableLimit) && $packageAcceptableLimit['code']!=1)
    $printBtnDisabled = "Disabled";
if(empty($packageAcceptableLimit))
    $printBtnDisabled = "Disabled";
@endphp
<div class="table-responsive m-t-20">
    {{ Form::open(array('url' => route('shipmenteditpackaging',['id'=>$shipment->id,'page'=>$page]), 'name' => 'frmpkgdetails', 'id' => 'frmpkgdetails', 'method' => 'post')) }}
    <table class="table table-bordered table-hover">
        <thead>
            <tr class="headerTblBg">
                <th>&nbsp;</th>
                <th>Length </th>
                <th>Width </th>
                <th>Height </th>
                <th>Weight </th>

                <th>Chargeable weight </th>
                <th>Package added by:</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @php 
            $grossWeight = 0;
            $grossChWeight = 0;
            $numPackages = 0;
            @endphp
            @foreach($packageDetailsData as $eachPackageData)
            @php 
            $numPackages++; 
            $grossWeight += $eachPackageData->weight;
            $grossChWeight += $eachPackageData->chargeableWeight;
            @endphp
            <tr id="packagingrow_{{$eachPackageData->id}}">
                <td class="withCheck">
                    <label>
                        <input type="checkbox" name="update[{{$eachPackageData->id}}][cb]" value="{{ $eachPackageData->id }}" class="checkbox flat-red cb_{{$eachPackageData->id}}">
                    </label>
                </td>
                <td valign="top" align="left">
                    <div class="value_label"> 
                        {{ $eachPackageData->length }}
                    </div>
                    <div class="value_field" style="display: none"><input class="form-control input-lg customInputBlack" value="{{$eachPackageData->length}}" name="update[{{$eachPackageData->id}}][length]" type="text"></div>
                </td>
                <td valign="top" align="left">
                    <div class="value_label"> 
                        {{ $eachPackageData->width }}
                    </div>
                    <div class="value_field" style="display: none"><input class="form-control input-lg customInputBlack" value="{{$eachPackageData->width}}" name="update[{{$eachPackageData->id}}][width]" type="text"></div>
                </td>
                <td valign="top" align="left">
                    <div class="value_label"> 
                        {{ $eachPackageData->height }}
                    </div>
                    <div class="value_field" style="display: none"><input class="form-control input-lg customInputBlack" value="{{$eachPackageData->height}}" name="update[{{$eachPackageData->id}}][height]" type="text"></div>
                </td>
                <td valign="top" align="left">
                    <div class="value_label"> 
                        {{ $eachPackageData->weight }}
                    </div>
                    <div class="value_field" style="display: none"><input class="form-control input-lg customInputBlack" value="{{$eachPackageData->weight}}" name="update[{{$eachPackageData->id}}][weight]" type="text"></div>
                </td>

                <td>{{ $eachPackageData->chargeableWeight }}</td>
                <td>{{ $eachPackageData->user }}</td>
                <td class="text-center buttMrg">
                    <a href="javascript:void(0)" data-toggle="confirmation" data-id="{{ $eachPackageData->id }}" class="removePackaging"><img src="{{ asset('public/administrator/img/deleteIconRedSm.png') }}"></a> 
                    <a href="javascript:void(0)" class="editPackage" data-id="{{ $eachPackageData->id }}"><img src="{{ asset('public/administrator/img/editIconSm.png') }}"></a>
                    <a href="javascript:void(0);" style="display:none;" class="savePackaging" data-id="{{ $eachPackageData->id }}"><img data-target="tooltip" height="18" class="save-item" title="Click to Update" src="{{ asset('public/administrator/img/download-button.png') }}"></a>
                </td>

            </tr>
            @endforeach
        </tbody>
    </table>

    <div class="col-sm-12 text-center m-b-25">
        <button class="btn btn-default custumButt btnBlue" name="action" value="update">Update Selected</button>&nbsp;&nbsp;
        <button class="btn btn-default custumButt btnDelete" data-toggle="confirmation" name="action" value="delete">Delete Selected items</button>
    </div>
    {{ Form::close() }}
</div>
<div class="clearfix"></div>
<div class="row shipDetail m-b-25">
    <div class="col-sm-4">
        <div class="info-box shipDtlBoxBlue"> <span class="info-box-icon"><img src="{{ asset('public/administrator/img/detailIcon01.png') }}"></span>
            <div class="info-box-content"> <span class="info-box-number"><small>Gross Weight: </small>{{ $grossWeight }}lbs</span> </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="info-box shipDtlBoxSky"> <span class="info-box-icon "><img src="{{ asset('public/administrator/img/detailIcon02.png') }}"></span>
            <div class="info-box-content"> <span class="info-box-number"><small>Chargeable weight: </small>
                    {{ $grossChWeight }}lbs</span> </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="info-box shipDtlBoxYellow"> <span class="info-box-icon "><img src="{{ asset('public/administrator/img/detailIcon04.png') }}"></span>
            <div class="info-box-content"> <span class="info-box-number"><small>Number of Packages:</small>{{ $numPackages }}</span> </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
{{ Form::open(array('url' => route('shipmentpackagedetails',['id'=>$shipment->id,'page'=>$page]), 'name' => 'frmstartpkg', 'id' => 'frmstartpkg', 'method' => 'post')) }}
<div class="form-group">
    <label>Packaging Details: </label>
    @if(!empty($packageAcceptableLimit))
    {!! $packageAcceptableLimit['message'] !!}
    @endif

    <textarea class="form-control customTxtareaBlack" rows="3" id="comment" name="packageDetails"></textarea>
</div>
<div class="col-sm-12 text-left m-b-25">
    <div class="row">
        <button {{ $printBtnDisabled }} class="btn btn-default custumButt btnBlue">Save</button>
    </div>
</div>
<div class="col-sm-12 text-left m-b-25">
    @if(!empty($packageDetailsCommentData))
    @foreach($packageDetailsCommentData as $eachPackageDetails)
    <div class="row">
        <div class="col-sm-12 col-md-7">By: {{$eachPackageDetails->user}}, {{$eachPackageDetails->date}}, <strong>Package Details:</strong>{{ $eachPackageDetails->message }} </div>
    </div>

    @endforeach
    @endif
</div>
{{ Form::close() }}