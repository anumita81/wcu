@foreach($deliveryData['deliveries'] as $delivery)
<div class="row">
    <ul class="shipingCost deliveryShipingCost-{{ $delivery['id'] }}">
        <li>Shipping Cost: <strong>{{ (new \App\Helpers\customhelper)->getCurrencySymbolFormat(($delivery['shippingCost'])) }}</strong></li>
        <li>
            @if($delivery['isDutyCharged']=='1')
            Clearing,Port Handling:
            @else
            Clearing,Port Handling and duty: 
            @endif
            <strong>{{ (new \App\Helpers\customhelper)->getCurrencySymbolFormat(($delivery['clearingDutyCost'])) }}</strong>
        </li>
        <li>Warehousing / Inventory Charge: <strong>{{ (new \App\Helpers\customhelper)->getCurrencySymbolFormat(($delivery['inventoryCharge'])) }}</strong></li>
        <li>Other Charges: <strong>{{ (new \App\Helpers\customhelper)->getCurrencySymbolFormat(($delivery['otherChargeCost'])) }}</strong></li>
    </ul>
    <div class="col-sm-12 m-t-20">
        <div class="row">
            <div class="col-sm-3 text-left">
                <label>Shipping Method:</label>
                @if(!empty($delivery['shippingMethodId']))
                @foreach($shippingMethodList as $shippingMethod)
                @if($delivery['shippingMethodId']==$shippingMethod->shippingid)
                    <h3><small>{{$shippingMethod->shipping}}</small></h3>
                @endif
                @endforeach 
                @else 
                    <h3><small>Shipping Method Not Selected</small></h3>
                @endif

            </div>
            <div class="col-sm-3 text-left">
                <label>Payment Status: </label>
                @if(!empty($shipment->paymentMethodId))
                    <h3><small>Payment Made</small></h3>
                @else 
                    <h3><small>Payment Not Made</small></h3>
                @endif
            </div>
            <div class="col-sm-3 text-left">
                <label>Payment Method: </label>
                @if(!empty($shipment->paymentMethodId))
                <select disabled="disabled" class="form-control input-lg customSelectBlack" id="sel1">
                    @foreach($paymentMethodList as $paymentMethod) 
                    <option {{$shipment->paymentMethodId==$paymentMethod->id?'selected="selected"':''}} value="{{$paymentMethod->id}}">{{$paymentMethod->paymentMethod}}</option>
                    @endforeach                
                </select>
                @else 
                <h3><small>Payment Not Made</small></h3>
                @endif

            </div>
            <div class="col-sm-3 text-left">
                <h3><small>TOTAL:</small><span class="txtRed">{{ (new \App\Helpers\customhelper)->getCurrencySymbolFormat(($delivery['totalCost'])) }}</span> </h3>
            </div>
        </div>
    </div>
</div>
@endforeach