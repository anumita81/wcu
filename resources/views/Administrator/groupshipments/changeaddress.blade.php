<script src = "{{ asset('public/administrator/controller-css-js/groupshipment.js') }}" ></script>

<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <select name="addressBookId" id="addressBookId" class="form-control customSelect">
                            <option value="new">Add New Address</option>
                            @foreach($addressBookData as $addressBook)
                            <option value="{{$addressBook->id}}">{{$addressBook->firstName." ".$addressBook->lastName}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            {{ Form::open(array('url' => 'administrator/groupshipments/changeaddress/'.$id.'/'.$userId, 'name' => 'changeaddressFrm', 'id' => 'changeaddressFrm', 'method' => 'post')) }}
            <div id="userAddressBook">
                <input type="hidden" id="userId" name="userId" value="<?php echo $userId; ?>" />
                    <input type="hidden" id="userAddressBookId" name="userAddressBookId" />

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Title</label>
                            <select name="title" id="title" class="form-control customSelect">
                                <option value="Mr.">Mr.</option>
                                <option value="Mrs.">Mrs.</option>
                                <option value="Ms.">Ms.</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>First Name <span class="text-red">*</span></label>
                            <input id="firstName" name="firstName" required="" class="form-control"  placeholder="Enter First Name" type="text">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Last Name <span class="text-red">*</span></label>
                            <input id="lastName" name="lastName" required="" class="form-control" placeholder="Enter Last Name" type="text">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Email <span class="text-red">*</span></label>
                            <input id="email" name="email" required="" class="form-control" placeholder="Enter Email" type="text">
                        </div>
                    </div>
                </div>           
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Address <span class="text-red">*</span></label>
                            <input id="address" name="address" required="" class="form-control" placeholder="Enter Address" type="text">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Address (line 2)</label>
                            <input id="alternateAddress" name="alternateAddress" class="form-control" placeholder="Enter Address" type="text">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Country <span class="text-red">*</span></label>
                            <select name="countryId" id="addresscountryId" required="" class="customSelect" autocomplete="false">
                                <option value="">Select</option>
                                @foreach($countryList as $country)
                                <option value="{{!empty($country['id'])?$country['id']:''}}">{{$country['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>State</label>
                            <select  name="stateId" id="addressstateId" class="customSelect" autocomplete="false">
                                <option value="">Select</option>
                            </select>                   
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>City</label>
                            <select  name="cityId" id="addresscityId" class="customSelect" autocomplete="false">
                                <option value="">Select</option>
                            </select>                   
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Zip/Postal code</label>
                            <input id="zipcode" name="zipcode" class="form-control" placeholder="Enter ..." type="text">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Phone <span class="text-red">*</span></label>
                            <input id="phone" name="phone" class="form-control" required placeholder="Enter ..." type="text">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Alternate Phone</label>
                            <input id="alternatePhone" name="alternatePhone" class="form-control" placeholder="Enter ..." type="text">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <div class="text-right">
                    <a id="changeAddressBtn" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</a>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
