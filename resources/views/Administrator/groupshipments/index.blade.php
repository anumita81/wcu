@extends('Administrator.layouts.master')
@section('content')

<script src = "{{ asset('public/administrator/controller-css-js/groupshipment.js') }}" ></script>

<!-- Main content -->
<section class="content"> 
    <div class="row m-b-15">
        <div class="col-lg-7 col-md-9">
            {{ Form::open(array('url' => 'administrator/groupshipments/index', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchData['searchDisplay']=='10'?'selected':''}} value="10">10</option>
                    <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                    <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                    <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                    <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                </select>
            </div>
            <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
            <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
            {{ Form::close() }}

        </div>
        <div class="col-lg-2 col-md-9 text-right"><a class="btn btn-sm btn-success" href="{{url('administrator/groupshipments/addedit/0/'.$page)}}">Add New Group Shipment</a></div>
        <div class="col-lg-3 col-md-9 text-right">
            <div class="btn-group actionGroup open">

                <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    Actions <span class="CiconR"><i class="fa fa-caret-down"></i></span>
                </button>
                <ul class="dropdown-menu">
                    @if($canEdit == 1)<li><a onclick="scheduleNotification();" href="javascript:void(0)">Schedule Notification</a> </li>@endif
                    <li><a href="javascript:void(0)" onclick="checkSelectRecord();">Export Selected</a> </li>
                    <li><a href="#" data-toggle="modal" data-target="#modal-export">Export All</a> </li>
                    @if($canEdit == 1)<li><a onclick="changeStatus();" href="#">Bulk Status Update</a> </li>@endif
                    @if($canAdd == 1)<li><a href="{{url('administrator/groupshipments/addedit/0/'.$page)}}">Add New Shipment</a></li>@endif
                    <li role="separator" class="divider"></li>
                    @if($canDelete == 1)<li><a href="#" onclick="deleteSelected();">Delete Selected</a></li>@endif
                </ul>
            </div>
            <a class="accordion-toggle btn-sm btn btn-success" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="Cicon"> <i class="fa fa-plus"></i></span> Advanced Search </a> 
        </div>
    </div>     
    <div class="col-md-12 m-t-15">
        <div class="row">
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="box">
                    <div class="box-body">
                        {{ Form::open(array('url' => 'administrator/groupshipments/index/', 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
                        <div class="form-row">
                            <div class="col-md-12">
                                <label>Date Period</label>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6"> 
                                <!-- <label for="inputEmail4">Email</label>-->
                                <div class="withRdaioButtons" style="padding:15px 0 0 0;"> 
                                    <label>
                                        <input {{$searchData['searchByCreatedOn']=='thismonth'?'checked':''}} type="radio" name="searchByCreatedOn" value="thismonth" class="flat-red" onclick="show1();">
                                        <span class="radioSpan">This month</span> </label>
                                    <label>
                                        <input {{$searchData['searchByCreatedOn']=='thisweek'?'checked':''}} type="radio" name="searchByCreatedOn" value="thisweek" class="flat-red" onclick="show1();">
                                        <span class="radioSpan">This week</span> </label>
                                    <label>
                                        <input {{$searchData['searchByCreatedOn']=='today'?'checked':''}} type="radio" name="searchByCreatedOn" value="today" class="flat-red" onclick="show1();">
                                        <span class="radioSpan">Today</span> </label>
                                    <label>
                                        <input {{$searchData['searchByCreatedOn']=='custom'?'checked':''}} type="radio" name="searchByCreatedOn" value="custom" class="flat-red customRange" onclick="show2();">
                                        <span class="radioSpan">Custom</span> </label>
                                </div>
                            </div>
                            <div class="form-group col-md-6"> 
                                <div id="customDaterange" class="form-group dateRange">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon input-daterange"> <i class="fa fa-calendar"></i> </div>
                                            <input type="text" class="form-control pull-right" name="searchByDate" value="{{$searchData['searchByDate']}}" id="searchByDate">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="sel1">Shipment ID</label>
                                <div class="input-group">
                                    <input name="searchShipment[idFrom]" id="idFrom" value="{{$searchData['searchShipment']['idFrom']}}" class="form-control input-lg" placeholder="" type="text">
                                    <span class="input-group-addon no-border">to</span>
                                    <input name="searchShipment[idTo]" class="form-control input-lg" value="{{$searchData['searchShipment']['idTo']}}" placeholder="" type="text">
                                </div>
                            </div>    
                            <div class="form-group col-md-3">
                                <label for="sel1">Unit #</label>
                                <input name="searchShipment[userUnit]" value="{{$searchData['searchShipment']['userUnit']}}" class="form-control input-lg" placeholder="" type="text">
                            </div>
                            @if(!empty($deliveryCompanyList))
                            <div class="form-group col-md-3">
                                <label for="deliveryCompanyId">Delivery Company</label>
                                <select name="searchShipment[deliveryCompanyId]" class="form-control customSelect2 input-lg" id="deliveryCompanyId">
                                    <option value="">All</option>
                                    @foreach($deliveryCompanyList as $deliveryCompany)
                                    <option {{$searchData['searchShipment']['deliveryCompanyId']==$deliveryCompany->id?"selected":''}} value="{{$deliveryCompany->id}}">{{$deliveryCompany->name}}</option>
                                    @endforeach
                                </select>
                            </div> 
                            @endif
                            <div class="form-group col-md-3">
                                <label for="sel1">Shipping method</label>
                                <select name="searchShipment[shippingMethod]" id="shippingMethod" class="form-control customSelect2 input-lg">
                                    <option value="">All</option>
                                    @foreach($shippingMethodList as $eachId=>$eachMethod)
                                    <option value="{{ $eachId }}" {{$searchData['searchShipment']['shippingMethod']==$eachId?"selected":''}}>{{ $eachMethod }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="shipmentStatus">Shipment Status</label>
                                <select name="searchShipment[shipmentStatus]" class="form-control customSelect2 input-lg" id="shipmentStatus">
                                    <option value="">All</option>
                                    <option {{$searchData['searchShipment']['shipmentStatus']=='1'?'selected':''}} value="1">In warehouse</option>
                                    <option {{$searchData['searchShipment']['shipmentStatus']=='2'?'selected':''}} value="2">In transit</option>
                                    <option {{$searchData['searchShipment']['shipmentStatus']=='3'?'selected':''}} value="3">Customs clearing</option>
                                    <option {{$searchData['searchShipment']['shipmentStatus']=='4'?'selected':''}} value="4">In destination warehouse</option>
                                    <option {{$searchData['searchShipment']['shipmentStatus']=='5'?'selected':''}} value="5">Out for delivery</option>
                                    <option {{$searchData['searchShipment']['shipmentStatus']=='6'?'selected':''}} value="6">Delivered</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="prepaid">Prepaid</label>
                                <select name="searchShipment[prepaid]" class="form-control customSelect2 input-lg" id="prepaid">
                                    <option value="">All</option>  
                                    <option {{$searchData['searchShipment']['prepaid']=='Y'?'selected':''}} value="Y">Yes</option>
                                    <option {{$searchData['searchShipment']['prepaid']=='N'?'selected':''}} value="N">No</option>
                                </select>                            
                            </div>
                            @if(!empty($dispatchCompanyList))
                            <div class="form-group col-md-3">
                                <label for="dispatchCompanyId">Dispatch Company</label>
                                <select name="searchShipment[dispatchCompanyId]" class="form-control customSelect2 input-lg" id="dispatchCompanyId">
                                    <option value="">All</option>
                                    @foreach($dispatchCompanyList as $dispatchCompany)
                                    <option {{$searchData['searchShipment']['dispatchCompanyId']==$dispatchCompany->id?"selected":''}} value="{{$dispatchCompany->id}}">{{$dispatchCompany->name}}</option>
                                    @endforeach
                                </select>
                            </div> 
                            @endif
                            <div class="form-group col-md-3">
                                <label for="shipmentType">Shipment Type</label>
                                <select name="searchShipment[shipmentType]" class="form-control customSelect2 input-lg" id="shipmentType">
                                    <option value="">All</option>
                                    @foreach($shipmentTypeList as $eachShipmentypeIndex => $eachShipmentypeName)
                                    <option {{$searchData['searchShipment']['shipmentType']==$eachShipmentypeIndex?'selected':''}} value="{{ $eachShipmentypeIndex }}">{{ $eachShipmentypeName }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="shipmentType">Packaging Status</label>
                                <select name="searchShipment[packed]" class="form-control customSelect2 input-lg" id="shipmentType">
                                    <option value="">All</option>  
                                    <option value="not_started" {{$searchData['searchShipment']['packed']=='not_started'?'selected':''}}>Not started</option>
                                    <option value="started" {{$searchData['searchShipment']['packed']=='started'?'selected':''}}>Started</option>
                                    <option value="packing_complete" {{$searchData['searchShipment']['packed']=='packing_complete'?'selected':''}}>Packing Complete</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="paymentStatus">Payment Status</label>
                                <select name="searchShipment[paymentStatus]" class="form-control customSelect2 input-lg" id="paymentStatus">
                                    <option value="">All</option>  
                                    <option {{$searchData['searchShipment']['paymentStatus']=='paid'?'selected':''}} value="paid">Paid</option>
                                    <option {{$searchData['searchShipment']['paymentStatus']=='unpaid'?'selected':''}} value="unpaid">Not Paid</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="total">Cost ($)</label>
                                <div class="input-group">
                                    <input name="searchShipment[totalFrom]" value="{{$searchData['searchShipment']['totalFrom']}}" class="form-control input-lg" placeholder="" type="text">
                                    <span class="input-group-addon no-border">to</span>
                                    <input name="searchShipment[totalTo]" value="{{$searchData['searchShipment']['totalTo']}}" class="form-control input-lg" placeholder="" type="text">
                                </div>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="sel1">Declared Value ($)</label>
                                <div class="input-group">
                                    <input name="searchShipment[itemCostFrom]" value="{{$searchData['searchShipment']['itemCostFrom']}}" class="form-control input-lg" placeholder="" type="text">
                                    <span class="input-group-addon no-border">to</span>
                                    <input name="searchShipment[itemCostTo]" value="{{$searchData['searchShipment']['itemCostTo']}}" class="form-control input-lg" placeholder="" type="text">
                                </div>
                            </div>

                        </div>
                        <div class="form-row">                                                
                            <div class="form-group col-md-4">
                                <label for="sel1">Customer </label>
                                <input name="searchShipment[user]" value="{{$searchData['searchShipment']['user']}}" class="form-control input-lg" placeholder="First Name / Email" type="text">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="sel1">Receiver</label>
                                <input name="searchShipment[receiver]" value="{{$searchData['searchShipment']['receiver']}}" class="form-control input-lg" placeholder="Name / Email" type="text">
                            </div>

                            @if(!empty($countryRec))
                            <div class="form-group col-md-4">
                                <label for="toCountry">Destination Country</label>
                                <select name="searchShipment[toCountry]" class="form-control customSelect2 input-lg" id="toCountry">
                                    <option value="">All</option>
                                    @foreach($countryRec as $country)
                                    <option {{$searchData['searchShipment']['toCountry']==$country->id?'selected':''}} value="{{$country->id}}">{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div> 
                            @endif

                            <!-- <div class="form-group col-md-3">
                                <label for="sel1">Company</label>
                                <input class="form-control input-lg" placeholder="" type="text">
                            </div> -->
                        </div>
                        <div class="form-row">          
                            <div class="form-group col-md-6">
                                <label for="labelType">Shipment Label</label>
                                <select  name="searchShipment[labelType]" class="form-control input-lg customSelect2" id="labelType">
                                    <option value="" selected="selected">All</option>
                                    <option {{$searchData['searchShipment']['labelType']=='standard'?'selected':''}} value="standard">Standard</option>
                                    <option {{$searchData['searchShipment']['labelType']=='red_star'?'selected':''}} value="red_star">Red Star</option>
                                    <option {{$searchData['searchShipment']['labelType']=='dhl'?'selected':''}} value="dhl">DHL</option>
                                    <option {{$searchData['searchShipment']['labelType']=='nations_delivery'?'selected':''}} value="nations_delivery">Nations Delivery</option>
                                </select>
                            </div>
                            @if(!empty($warehouseList))
                            <div class="form-group col-md-6">
                                <label for="warehouseId">Warehouse</label>
                                <select name="searchShipment[warehouseId]" class="form-control input-lg customSelect2" id="warehouseIdSearch">
                                    <option value="" selected="selected">All</option>
                                    @foreach($warehouseList as $warehouse)
                                    <option {{$searchData['searchShipment']['warehouseId']==$warehouse->id?'selected':''}} value="{{$warehouse->id}}">{{$warehouse->countryName}} ({{ $warehouse->cityName }})</option>
                                    @endforeach
                                </select>
                            </div> 
                            @endif
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="shipmentType">Store</label>
                                <select name="searchShipment[store]" class="form-control customSelect2 input-lg" id="shipmentType">
                                    <option value="">All</option>  
                                    @foreach($storeList as $store)
                                    <option value="{{$store->id}}" {{$searchData['searchShipment']['store']==$store->id?'selected':''}}>{{$store->storeName}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="shipmentType">Category</label>
                                <select name="searchShipment[category]" class="form-control customSelect2 input-lg" id="rootCategory">
                                    <option value="">All</option>  
                                    @foreach($categoryList as $categoryId => $categoryName)
                                    <option value="{{ $categoryId }}" {{$searchData['searchShipment']['category']==$categoryId?'selected':''}}>{{ $categoryName }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="shipmentType">Sub Category</label>
                                <select name="searchShipment[subcategory]" class="form-control customSelect2 input-lg" id="subCategory">
                                    <option value="">Select</option>
                                    @foreach($subcategoryList as $eachSubCat)
                                    <option value="{{ $eachSubCat['id'] }}" {{$searchData['searchShipment']['subcategory']==$eachSubCat['id']?'selected':''}}>{{ $eachSubCat['category'] }}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" id="subcatPage" value="getsubcategory">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="shipmentType">Product</label>
                                <select name="searchShipment[product]" class="form-control customSelect2 input-lg" id="siteProduct">
                                    <option value="">Select</option>
                                    @foreach($productList as $eachProduct)
                                    <option value="{{ $eachProduct->id }}" {{$searchData['searchShipment']['product']==$eachProduct->id?'selected':''}}>{{ $eachProduct->productName }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">          
                            <div class="form-group col-md-6">
                                <label for="rows">Rows</label>
                                <select  name="searchShipment[rows]" onchange="getWarehouseZoneList(this.value)" class="form-control input-lg customSelect2" id="labelType">
                                    <option value="">Select</option>
                                    @foreach($warehouseRowList as $row)
                                    <option {{$searchData['searchShipment']['rows']==$row->id?'selected':''}} value="{{$row->id}}">{{$row->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                           
                            <div class="form-group col-md-6">
                                <label for="zones">Zones</label>
                                <select name="searchShipment[zones]" class="form-control input-lg customSelect2" id="warehouseZoneId">
                                    <option value="" selected="selected">All</option>
                                    @if(!empty($searchData['searchShipment']['rows']))
                                        @foreach($warehouseZoneList as $zone)
                                        <option {{$searchData['searchShipment']['zones']==$zone->id?'selected':''}} value="{{$zone->id}}">{{$zone->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div> 
                         
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-3"></div>
                            <div class="form-group col-md-3"></div>
                            <div class="form-group col-md-3"></div>
                            <div class="form-group col-md-3">
                                <button class="btn btn-default custumButt solidBtn m-r-15 m-t-20">Search</button>
                                <button type="button" onclick="location.href = '{{url('administrator/groupshipments/showall')}}'" class="btn btn-danger custumButt solidBtn  m-r-15 m-t-20">Show All</button>
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">

                <div class="box-body table-responsive">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="withCheck"><label><input type="checkbox" class="flat-red chk_all"></label></th>
                                <th data-sort="id" class="sorting_asc sortby">#</th>
                                <th data-sort="fromName" class="{{$sort['fromName']['current']}} sortby" >Group</th>
                                <th>Type</th>
                                <th>Shipment Type</th>
                                <th>Shipment Location</th>
                                <th data-sort="deliveryCompany" class="{{$sort['deliveryCompany']['current']}} sortby" >Delivery Company</th>
                                <th data-sort="dispatchCompany" class="{{$sort['dispatchCompany']['current']}} sortby" >Dispatch Company</th>
                                <th width="10%">Destination</th>
                                <th data-sort="createdOn" class="{{$sort['createdOn']['current']}} sortby" >Date</th>
                                <th width="10%">Status</th>
                                <th>Packaging</th>
                                <th width="10%">Payment Status</th>
                                <th width="10%">Actions</th>
                            </tr>
                        </thead>
                        <tbody id="checkboxes">
                            @php
                            $listIds = array();
                            @endphp
                            @if(!empty($shipmentData)) 
                            @foreach ($shipmentData as $shipment)

                            @php
                            $listIds[] = $shipment->id;
                            $editLink = url('administrator/groupshipments/addedit/'.$shipment->id.'/'.$page); 
                            $deleteLink = url('administrator/groupshipments/delete/'.$shipment->id.'/'.$page); 
                            $totalCost = (new \App\Helpers\customhelper)->getCurrencySymbolFormat($shipment->total);

                            if($shipment->shipmentStatus == '0') :
                            $statusTxt = 'Shipment Submitted';
                            $statusClass = 'btn-default';
                            elseif($shipment->shipmentStatus == '1') :
                            $statusTxt = 'In Warehouse';
                            $statusClass = 'btn-danger';
                            elseif($shipment->shipmentStatus == '2') :
                            $statusTxt = 'In Transit';
                            $statusClass = 'btn-primary';
                            elseif($shipment->shipmentStatus == '3') :
                            $statusTxt = 'Customs clearing';
                            $statusClass = 'btn-info';
                            elseif($shipment->shipmentStatus == '4') :
                            $statusTxt = 'In destination warehouse';
                            $statusClass = 'btn-warning';
                            elseif($shipment->shipmentStatus == '5') :
                            $statusTxt = 'Out for delivery';
                            $statusClass = 'bg-olive';
                            else :
                            $statusTxt = 'Delivered';
                            $statusClass = 'btn-success';
                            endif;

                            if($shipment->paymentStatus == 'unpaid') :
                            $paymentStatusTxt = 'Not Paid';
                            $paymentStatusClass = 'btn-danger';
                            else :
                            $paymentStatusTxt = 'Complete';
                            $paymentStatusClass = 'btn-success';
                            endif;
                           
                            $itemType = array();
                            if(!empty($shipment->itemType))
                            $itemType = explode(',',$shipment->itemType);

                            $packedStatusClass = 'btn-success';
                            if($shipment->packed == 'not_started')
                                $packedStatusClass = 'btn-default';
                            else if($shipment->packed == 'started')
                                $packedStatusClass = 'btn-info';

                            @endphp
                            <tr>
                                <td class="withCheck"><label><input type="checkbox" name="checkboxselected" data-attr="{{$shipment->shipmentStatus}}" value="{{$shipment->id}}" class="flat-red checkbox"></label></td>
                                <td><a href="{{url('administrator/groupshipments/addedit/'.$shipment->id.'/1')}}">{{$shipment->id}}</a></td>
                                <td width="20%"><strong>{{$shipment->groupName}}</strong><p>Coordinator:<a href="{{url('administrator/users/addedit/'.$shipment->userId.'/1')}}">{{!empty($userList[$shipment->userId])?$userList[$shipment->userId]:""}}</a></p></td>
                                <td>{{ucfirst($shipment->shipmentType)}}</td>
                                <td>{{ $shipmentTypeList[$shipment->partucilarShipmentType] }}</td>
                                <td>
                                    <span id="location-{{$shipment->id}}">
                                    @php
                                        $locations = explode(",",$shipment->shipmentLocation);
                                        $printedLocations = array();
                                        $displayLocation = '';
                                        foreach($locations as $eachLocations)
                                        {
                                            if(!in_array($eachLocations,$printedLocations))
                                            {
                                                $displayLocation .= $eachLocations.',';
                                                $printedLocations[] = $eachLocations;
                                            }
                                        }
                                        $displayLocation = substr($displayLocation,0,'-1');
                                        if(empty($searchData['searchShipment']['rows']) && empty($searchData['searchShipment']['zones']))
                                        {
                                            echo 'Loading ...';
                                        }
                                    @endphp
                                    {{ $displayLocation }}
                                    </span>
                                </td>
                               <!--  <td>{{!empty($shipment->deliveryCompany)?$shipment->deliveryCompany:'N/A'}}</td> -->
                               <td><span id="delivery-{{$shipment->id}}">{{!empty($shipment->deliveryCompany)?$shipment->deliveryCompany:'Loading..'}}</span></td>
                                <!-- <td>{{!empty($shipment->dispatchCompany)?$shipment->dispatchCompany:'N/A'}}@if(!empty($shipment->expectedDispatchDate))<br><strong>Estimated Date:</strong> <br>{{ \Carbon\Carbon::parse($shipment->expectedDispatchDate)->format('m-d-Y H:i')}}@endif</td> -->
                                <td><span id="delivery-{{$shipment->id}}">{{!empty($shipment->dispatchCompany)?$shipment->dispatchCompany:'N/A'}}@if(!empty($shipment->expectedDispatchDate))<br><strong>Estimated Date:</strong> <br>{{ \Carbon\Carbon::parse($shipment->expectedDispatchDate)->format('m-d-Y H:i')}}@endif</span></td>
                                <td><b>City :</b> {{!empty($cityList[$shipment->toCity])?$cityList[$shipment->toCity]:'N/A'}}<br> <b>State</b> : {{!empty($stateList[$shipment->toState])?$stateList[$shipment->toState]:'N/A'}} <br> <b>Country :</b> {{!empty($countryList[$shipment->toCountry])?$countryList[$shipment->toCountry]:'N/A'}}</td>                                
                                <td>{{ \Carbon\Carbon::parse($shipment->createdOn)->format('m-d-Y H:i')}}</td>
                                <td>@if($canEdit == 1)
                                    @if($shipment->shipmentStatus !=6)
                                        <a onclick="showAddEdit({{$shipment->id}}, {{$page}}, 'groupshipments/changestatus');" class="btn btn-xs {{$statusClass}} shipmentStatusTxt"><span data-toggle="tooltip" title="" data-original-title="Click to Change Status">{{$statusTxt}}</span></a>
                                    @else    
                                        <a class="btn btn-xs {{$statusClass}} shipmentStatusTxt"><span>{{$statusTxt}}</span></a>
                                    @endif
                                    @endif
                                </td>
                                <td><span class="btn btn-xs {{$packedStatusClass}} shipmentStatusTxt">{{ucfirst(str_replace('_',' ',$shipment->packed))}}</span></td>
                                <td class="text-center">
                                    <span class="labelImg" data-toggle="tooltip" title="" data-original-title="{{ ($shipment->paymentStatus == 'paid')?'Paid':'Not Paid' }}">
                                        @if($canEdit == 1)
                                            @if($shipment->paymentStatus == 'paid')
                                                <img src="{{asset('public/administrator/img/paid-money.png')}}">
                                            @else
                                                <img src="{{asset('public/administrator/img/not-paid-money.png')}}">
                                            @endif
                                        @endif
                                    </span>
                                </td>
                                <td>
                                    @if($canView == 1)<a href="{{$editLink}}" class="color-theme actionIcons" data-toggle="tooltip" title="Click to View"><i class="fa fa-fw fa-eye"></i></a>@endif
                                    @if($canDelete == 1)<a class="actionIcons color-theme-2" href="{{$deleteLink}}" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a>@endif
                                    @if($shipment->packed == 'packing_complete' && $shipment->dispatchCompany == "" && $shipment->shipmentStatus <= '2')
                                    <a href="javascript:void(0);" class="color-theme actionIcons" data-toggle="tooltip" title="Click to Assign" onclick="showAddEdit({{$shipment->id}}, {{$page}}, 'shipments/assignDispatchCompany');"><i class="fa fa-truck" aria-hidden="true"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @else 
                            <tr>
                                <td colspan="6">No Record Found</td>
                            </tr>
                            @endif
                            @php
                                //print_r($listIds);
                                
                                $listIds = implode('^',$listIds);
                                echo '<script type="text/javascript">
                                    $(document).ready(function() {
                                        getLocationDetails("'.$listIds.'");
                                    });
                                </script>';
                                
                            @endphp
                        </tbody>
                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $shipmentData->firstItem() . ' - ' . $shipmentData->lastItem() . ' of  ' . $shipmentData->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $shipmentData->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--modal open-->
    <div class="modal fade" id="modal-addEdit"></div>
    <!--modal close-->
</section>
<!-- /.content -->  

<!--modal open-->
<div class="modal fade" id="modal-export">
    {{ Form::open(array('url' => route('shipmentexportall',['page'=>$page]), 'name' => 'exportAll', 'id' => 'exportAll', 'method' => 'post')) }}
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> Export Shipment Data </h4>
            </div>
            <div class="modal-body">
                <div id="errorTxt" class="alert alert-danger" style="display:none;">

                </div>
                <h5 style="margin-bottom: 20px;">Choose fields to export</h5>

                <div class="row">                                        
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="unit" class="flat-red selecteField"><span style="margin-left: 10px;">Unit</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="name" class="flat-red selecteField"><span style="margin-left: 10px;">Customer Name</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="email" class="flat-red selecteField"><span style="margin-left: 10px;">Email Address</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="number" class="flat-red selecteField"><span style="margin-left: 10px;">Contact Number</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="status" class="flat-red selecteField"><span style="margin-left: 10px;">Shipment Status</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="type" class="flat-red selecteField"><span style="margin-left: 10px;">Shipment Type</span></label>
                        </div> 
                    </div>
                    <div class="col-md-6">                                        
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="paymentstatus" class="flat-red selecteField"><span style="margin-left: 10px;">Payment Status</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="cityName" class="flat-red selecteField"><span style="margin-left: 10px;">Destination City</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="stateName" class="flat-red selecteField"><span style="margin-left: 10px;">Destination State</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="countryName" class="flat-red selecteField"><span style="margin-left: 10px;">Destination Country</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="location" class="flat-red selecteField"><span style="margin-left: 10px;">Shipment Location</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="packed" class="flat-red selecteField"><span style="margin-left: 10px;">Packaging Status</span></label>
                        </div>

                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" onclick="exportall_submit();" class="btn btn-success" data-dismiss="modal">Export</button>
            </div>
        </div>

        <!-- /.modal-content -->
    </div>{
    {{ Form::close() }}
    <!-- /.modal-dialog -->
</div>
<!--modal close--> 
<!--modal open-->
<div class="modal fade" id="modal-export-selected">

    <div class="modal-dialog" id="exportAllse">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> Export Shipment Data </h4>
            </div>
            <div class="modal-body">
                <div id="errorTxtse" class="alert alert-danger" style="display:none;">

                </div>
                <input type="hidden" name="base_path_selected_export" value="{{ route('shipmentexportselected',['page'=>$page]) }}">

                <h5 style="margin-bottom: 20px;">Choose fields to export</h5>

                <div class="row">                                        
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="unit" class="flat-red selecteField"><span style="margin-left: 10px;">Unit</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="name" class="flat-red selecteField"><span style="margin-left: 10px;">Customer Name</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="email" class="flat-red selecteField"><span style="margin-left: 10px;">Email Address</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="number" class="flat-red selecteField"><span style="margin-left: 10px;">Contact Number</span></label>
                        </div> 
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="status" class="flat-red selecteField"><span style="margin-left: 10px;">Shipment Status</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="type" class="flat-red selecteField"><span style="margin-left: 10px;">Shipment Type</span></label>
                        </div> 
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="paymentstatus" class="flat-red selecteField"><span style="margin-left: 10px;">Payment Status</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="cityName" class="flat-red selecteField"><span style="margin-left: 10px;">Destination City</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="stateName" class="flat-red selecteField"><span style="margin-left: 10px;">Destination State</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="countryName" class="flat-red selecteField"><span style="margin-left: 10px;">Destination Country</span></label>
                        </div>
                         <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="location" class="flat-red selecteField"><span style="margin-left: 10px;">Shipment Location</span></label>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="selectall[]" checked="checked" value="packed" class="flat-red selecteField"><span style="margin-left: 10px;">Packaging Status</span></label>
                        </div>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" onclick="exportallse_submit();" class="btn btn-success" data-dismiss="modal">Export</button>
            </div>
        </div>

        <!-- /.modal-content -->
    </div>

    <!-- /.modal-dialog -->
</div>
<!--modal close-->
{{ Form::open(array('url' => '', 'name' => 'frmCheckedItem', 'id' => 'frmCheckedItem', 'method' => 'post')) }}
<input type="hidden"  id="checkedval" name="checkedval" />
{{ Form::close() }}
@endsection