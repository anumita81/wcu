<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Start Packaging</h4>
        </div>
        <div class="modal-body">    
                    {{ Form::open(array('id'=>'data-form','url' => '','files'=>true)) }}
                        <input type="hidden" id="shipmentId" value="{{ $shipmentId }}">
                        <input type="hidden" id="deliveryCount" value="{{ $deliveryCount }}">
                        <input type="hidden" id="deliveryTobeScaned" value="{{ $deliveryTobeScaned }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        1.Scan the location label on the outside of the box<br/>
                                        2.Scan all the deliveries in the shipment one at a time<br/>
                                        3.Click on Package Now to continue with packaging<br/>
 
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea style="width: 100%" rows="7" id="labeldata"></textarea>
                                </div>
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                            <div class="text-right">
                                <button type="submit" class="btn btn-success" id="packagenow"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Package Now</button>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
