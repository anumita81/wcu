<script src = "{{ asset('public/administrator/controller-css-js/shipment.js') }}" ></script>

<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        <div class="modal-body">
            {{ Form::open(array('url' => 'administrator/shipments/schedulenotification/'.$id.'/'.$page, 'name' => 'schedulenotificationFrm', 'id' => 'schedulenotificationFrm', 'method' => 'post')) }}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Date <span class="text-red">*</span></label>
                        <div class="input-group date">
                            <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                            <input class="form-control datepicker pull-right" id="datepicker" type="text" name="scheduleDate" value="{{ date('Y-m-d') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email Message <span class="text-red">*</span></label>
                        <select name="emailtemplateKey" class="form-control customSelect" id="emailtemplateKey" required="">
                            <option value="">Select template</option>
                            @foreach($emailNotificationList as $emailNotification)
                                <option value="{{$emailNotification['id']}}">{{ !empty($templateKeysList[$emailNotification['templateKey']]) ? $templateKeysList[$emailNotification['templateKey']] : $emailNotification['templateKey'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>SMS Message <span class="text-red">*</span></label>
                        <select name="smstemplateKey" class="form-control customSelect" id="smstemplateKey" required="">
                            <option value="">Select template</option>
                            @foreach($smsNotificationList as $smsNotification)
                            <option  value="{{$smsNotification['id']}}">{{ !empty($templateKeysList[$emailNotification['templateKey']]) ? $templateKeysList[$smsNotification['templateKey']] : $smsNotification['templateKey'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->



<script>
    $(function () {
        $("#changepasswordFrm").validate();
    });
</script>