<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Account Details</h4>
        </div>
        <div class="modal-body">
            @if(!empty($accountInfo))
            <table class="userDetails text-data" style="width: 50%;border: 1px">
                <tr>
                    <td><strong>Account Number</strong> : </td>
                    <td>{{$accountInfo->accountnumber}}</td>
                </tr>
                <tr>
                    <td><strong>Bank Name</strong> : </td>
                    <td>{{$accountInfo->bankName}}</td>
                </tr>
                <tr>
                    <td><strong>Account Holder Name</strong> : </td>
                    <td>{{$accountInfo->accountHolderName}}</td>
                </tr>
            </table>
            @else
            <p>Account Details Not Found</p>
            @endif
        </div>
    </div>
    <!-- /.modal-content -->
</div>