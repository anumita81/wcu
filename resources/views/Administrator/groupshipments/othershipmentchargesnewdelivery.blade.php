@if(!empty($otherCharges))
@foreach($otherCharges as $row)
@php $otherChargeAmount =   (new \App\Helpers\customhelper)->getCurrencySymbolFormat($row->otherChargeAmount); @endphp

<li id="othercharge{{$row->id}}" class="list-group-item">
    <div class="bulletTxt"><span class="bullet"><img src="{{ asset('public/administrator/img/redsqrBullet.png') }}"></span>{{$row->otherChargeName}} {{!empty($row->notes)?'-'.$row->notes:''}}  - {{$otherChargeAmount}}<br>
        Added by: {{$row->createdBy}}</div>
    <span class="deleteBtn"><a data-id="{{$row->id}}" class="deletenewcharge color-theme-2 actionIcons" href="javascript:void(0);" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a></span>
</li>
@endforeach
@endif