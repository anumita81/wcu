<script src="{{ asset('public/administrator/js/jQuery.print.js') }}"></script>
<script src = "{{ asset('public/administrator/controller-css-js/dhl.js') }}" ></script>
@if(empty($shipmentData))
<div class="modal-dialog modal-lg">
    <div class="modal-content" style="height:550px; overflow:auto;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Prepare DHL Commercial Invoice</h4>
        </div>
        <div class="modal-body"><span class="text-red text-center">Please generate DHL label first</span></div>
    </div>
</div>
@else
<div id="wait-modal" style="display:none;" class="loaderMiddle"></div>
        <input type="hidden" name="base_path_offers" value="{{ url('/').'/administrator/offers/' }}">
        <input name="_token" type="hidden" value="{{ csrf_token() }}">
        <input name="fromCountryIDDefault" id="fromCountryIDDefault" type="hidden" value="{{$shipmentData['fromCountry']}}">
        <input name="toCountryIDDefault" id="toCountryIDDefault" type="hidden" value="{{$shipmentData['toCountry']}}">
        <input name="fromStateIDDefault" id="fromStateIDDefault" type="hidden" value="{{$shipmentData['fromState']}}">
        <input name="fromCityIDDefault" id="fromCityIDDefault" type="hidden" value="{{$shipmentData['fromCity']}}">
        <input name="toStateIDDefault" id="toStateIDDefault" type="hidden" value="{{$shipmentData['toState']}}">
        <input name="toCityIDDefault" id="toCityIDDefault" type="hidden" value="{{$shipmentData['toCity']}}">
        <div class="modal-dialog modal-lg">
          <div class="modal-content" style="height:550px; overflow:auto;">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Prepare DHL Commercial Invoice</h4>
            </div>
            <div class="modal-body longFrm">
              <!--<form id="addFrm" role="form" method="post" action="couponListing.html">-->
              {{ Form::open(array('url' => url('administrator/shipments/dhlcipost'),  'name' => 'dhlcipost', 'id' => 'dhlcipost', 'method' => 'post')) }}
              <input type="hidden" name="base_path_dhlci" value="{{ url('/').'/administrator/shipments/dhlcipost/' }}">
                <div class="row">
                  <div class="col-md-6"> <strong> FROM (Must be US-located address)</strong> </div>
                  <div class="col-md-6"> <strong>To</strong> </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Address <span class="text-red">*</span></label>
                      <input name="fromAddress" class="form-control" placeholder="" type="text" value="{{$shipmentData['fromAddress']}}" required="">
                      <input name="fromAddress2" class="form-control" placeholder="" type="hidden" value="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Address <span class="text-red">*</span></label>
                      <input name="toAddress" class="form-control" placeholder="" type="text" value="{{$deliveryAddress['toAddress']}}" required="">
                      <input name="toAddress2" class="form-control" placeholder="" type="hidden" value="">
                    </div>
                  </div>
                </div>
                <!--<div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Address (line 2)<span class="text-red">*</span></label>
                      <input name="fromAddress2" class="form-control" placeholder="" type="text" value="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Address (line 2)<span class="text-red">*</span></label>
                      <input name="toAddress2" class="form-control" placeholder="" type="text" value="">
                    </div>
                  </div>
                </div>-->
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Country <span class="text-red">*</span></label>
                      <select name="fromCountry" id="fromCountry" class="form-control customSelect" required="">
                        <option value="">Select any one</option>
                        @foreach($countryList as $country)
                        <option @if($shipmentData['fromCountry'] == $country->id) selected @endif value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Country <span class="text-red">*</span></label>
                      <select name="toCountry" id="toCountry" class="form-control customSelect" required="">
                        <option value="">Select any one</option>
                        @foreach($countryList as $country)
                        <option @if($deliveryAddress['toCountry'] == $country->id) selected @endif value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>State <span class="text-red">*</span></label>
                      <select name="fromState" id="fromState" class="form-control customSelect" required="">
                        <option value=''>Select any one</option>
                    </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>State <span class="text-red">*</span></label>
                      <select name="toState" id="toState" class="form-control customSelect" required="">
                        <option value=''>Select any one</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>City <span class="text-red">*</span></label>
                      <select name="fromCity" id="fromCity" class="form-control customSelect" required="">
                        <option value=''>Select any one</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>City <span class="text-red">*</span></label>
                      <select name='toCity' id="toCity" class="form-control customSelect" required="">
                        <option value=''>Select any one</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Zip/Postal code <span class="text-red">*</span></label>
                      <input name="fromZip" class="form-control" placeholder="" type="text" value="{{!empty($shipmentData['fromZipCode']) ? $shipmentData['fromZipCode'] : 'N/A' }}" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Zip/Postal code <span class="text-red">*</span></label>
                      <input name="toZip" class="form-control" placeholder="" type="text" value="{{!empty($deliveryAddress['toZipCode']) ? $deliveryAddress['toZipCode'] : 'N/A' }}" required="">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Phone <span class="text-red">*</span></label>
                      <input name="fromPhone" class="form-control" placeholder="" type="text" value="+17135976225" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Phone <span class="text-red">*</span></label>
                      <input name="toPhone" class="form-control" placeholder="" type="text" value="{{!empty($deliveryAddress['toPhone']) ? $deliveryAddress['toPhone'] : 'N/A' }}" required="">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Email <span class="text-red">*</span></label>
                      <input name="fromEmail" class="form-control" placeholder="" type="text" value="N/A" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Email <span class="text-red">*</span></label>
                      <input name="toEmail" class="form-control" placeholder="" type="text" value="{{$deliveryAddress['toEmail']}}" required="">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Person Name</label>
                      <input name="fromName" class="form-control" placeholder="" type="text" value="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Person Name <span class="text-red">*</span></label>
                      <input name="toName" class="form-control" placeholder="" type="text" value="Shoptomydoor" required="">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Company Name <span class="text-red">*</span></label>
                      <input name="fromCompanyName" class="form-control" placeholder="" type="text" value="Shoptomydoor" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Company Name</label>
                      <input name="toCompanyName" class="form-control" placeholder="" type="text" value="{{!empty($shipmentData['toCompany']) ? $shipmentData['toCompany'] : ''}}">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 modal-title"> <strong class="modalSubTitle"> Common Info</strong> </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group"> <strong>Total Number of Pieces</strong> : <span id="piecesPlaceholder"></span> </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group"> <strong>Total Gross Weight</strong> : {{ $totalPackageWeight }}lbs </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group"> <strong>Total Chargeable Weight</strong> : {{ $totalChargeableWeight }}lbs </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group"> <strong>Invoice Date</strong> : @php $createdOn=date("m-d-Y H:i", strtotime($shipmentData['createdOn']));  @endphp {{$createdOn}}</div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group"> <strong>Invoice Number</strong> : {{$shipmentData['id']}} </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group"> <strong>Air Waybill No</strong> : {{ $dhlLabelInfo->dhlWaybillNumber }}
                        <input name="airWaybillNo" class="form-control" placeholder="" type="hidden" value="{{ $dhlLabelInfo->dhlWaybillNumber }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group"> <strong>Terms of Trade</strong> : {{ $dhlLabelInfo->termsOfTrade }}
                        <input type="hidden" value="{{ $dhlLabelInfo->termsOfTrade }}" name="incoterms" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 modal-title"> <strong class="modalSubTitle">Items</strong> </div>
                </div>
                
                <input type="hidden" name="grossWeight" id="" value="{{round($totalPackageWeight, 2)}}">
                <input type="hidden" name="netWeight" value="{{round($totalChargeableWeight, 2)}}">
                <input type="hidden" name="invoiceDate" value="@php echo $createdOn=date("m-d-Y H:i", strtotime($shipmentData['createdOn']));  @endphp">
                <input type="hidden" name="invoiceNo" value="{{$shipmentData['id']}}">
                <input type="hidden" name="Pieces" id="numPieces" value="">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Description</th>
                      <th>Commodity Code</th>
                      <th>Manufacturer Country</th>
                      <th>Quantity</th>
                      <th width='10%'>Unit Value ($)</th>
                      <th>Sub Total</th>
                      <th>&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
            @php
            $totalQty = 0;
            $totalDelivery = 0;
            $totalPrice = 0.00;
            @endphp
            @if(isset($delivery['deliveries']))
            @foreach($delivery['deliveries'] as $eachDelivery)
            @foreach($eachDelivery['packages'] as $eachPackages)
            @php $totalQty += $eachPackages['itemQuantity']; @endphp
            @php $totalPrice += $eachPackages['itemTotalCost']; @endphp
            @php $replacedAmount = (new \App\Helpers\customhelper)->getCurrencySymbolFormat($eachPackages['itemPrice']); @endphp      
            
                    <tr>
                        <td><input name="descr[]" class="form-control" placeholder="" type="text" value="{{$eachPackages['itemName']}}"></td>
                        <td><input name="ccc" class="form-control" placeholder="" type="text" value="{{ !empty($productScheduleList[$eachPackages['siteProductId']]) ? $productScheduleList[$eachPackages['siteProductId']] : (!empty($subCategoryScheduleList[$eachPackages['siteSubCategoryId']]) ? $subCategoryScheduleList[$eachPackages['siteSubCategoryId']] : 'N/A') }}"></td>
                        <td> 
                            <select name="manufacturerCountry[]" id="" class="form-control customSelect">
                                <option value="">Select any one</option>
                                @foreach($countryList as $country)
                                <option value="{{$country->name}}">{{$country->name}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>{{$eachPackages['itemQuantity']}} <input name="qty[]" class="qty" type="hidden" value="{{$eachPackages['itemQuantity']}}"></td>
                        <td><input name="price[]" type="text" class="form-control product-val" value=""></td>
                        <td><input required="" readonly="" class="form-control format product-total" type="text"></td>
                        <td>
                            <a href="javascript:void(0);" class="delDHLRow text-red">
                                <span class="glyphicon glyphicon-remove text-red"></span>
                            </a>
                        </td>
                    </tr>@endforeach
            @endforeach
            <tr>
                <td colspan="5" align="right">Total Shipment Value</td>
                <td><input readonly="" class="form-control format" id="totalShipmentValue" type="text" value="0.00"></td>
            </tr>
            @endif

            @php $totalPrice = (new \App\Helpers\customhelper)->getCurrencySymbolFormat(round($totalPrice, 2)); @endphp  

                    <input type="hidden" id="Pieces" value="{{ $totalQty }}">
                  </tbody>
                </table>
                <div class="modal-footer">
                  <div class="text-right">
                    <button onclick="dhl_ci_submit();" type="button" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Create Commercial Invoice</button>
                  </div>
                </div>
              <!-- </form> -->
              {{ Form::close()}}
            </div>
          </div>
          <!-- /.modal-content --> 
        </div>
        <!-- /.modal-dialog --> 
<script src = "{{ asset('public/administrator/controller-css-js/shipment.js') }}" ></script>
@endif