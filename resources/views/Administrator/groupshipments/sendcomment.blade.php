<script src = "{{ asset('public/administrator/controller-css-js/groupshipment.js') }}" ></script>

<!--modal open-->
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Notify Employee</h4>
      </div>
      <div class="modal-body">
        {{ Form::open(array('url' => 'administrator/groupshipments/saveandnotify/'.$shipmentId, 'name' => 'notifyFrm', 'id' => 'notifyFrm', 'method' => 'post')) }}
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Select one or more emails</label>
                <select class="form-control select2" id="admin-emails" name="admin[]" multiple="multiple"  style="width: 100%">
                  @foreach($customerEmailList as $customerEmail)
                  <option value="{{$customerEmail['email']}}">{{$customerEmail['email']}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Enter Message</label>
                <textarea class="form-control" rows="5" cols="20" placeholder="Enter ..." name="warehousemsg" required=""></textarea>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <div class="text-right">
              <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Notify</button>
            </div>
          </div>
        {{Form::close()}}
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->



<script>
$(function () {
    $("#notifyFrm").validate();
    //$('#admin-emails').select2();
    $('#admin-emails').select2({
        dropdownParent: $('#modal-addEdit')
    });
});
</script>