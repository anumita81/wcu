<script src = "{{ asset('public/administrator/controller-css-js/shipment.js') }}" ></script>

<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Comments</h4>
        </div>
        <div class="modal-body">
            {{ Form::open(array('url' => 'administrator/shipments/savestatuscomment/'.$deliveryId.'/'.$shipmentId.'/'.$statusName, 'name' => 'notifyFrm', 'id' => 'notifyFrm', 'method' => 'post')) }}
            @if(!empty($msg))
            <div class="alert alert-danger">{{ $msg }}</div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Comments</label>
                        <textarea name="message" id="message" class="form-control" row="10">{{!empty($comments)? $comments:""}}</textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success" {{!empty($msg)?"disabled":""}}><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->



<script>
$(function () {
    $("#notifyFrm").validate();
});
</script>