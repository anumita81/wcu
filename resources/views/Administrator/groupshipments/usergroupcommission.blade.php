@extends('Administrator.layouts.master')
@section('content')

<section class="content">
    <div class="row m-b-15">
        {{ Form::open(array('url' => 'administrator/usergroupcommission', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
        <div class="col-lg-4 col-md-3"> 
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchData['searchDisplay']=='10'?'selected':''}} value="10">10</option>
                    <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                    <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                    <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                    <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                </select>
            </div>
            <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
            <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
        </div>
        <div class="col-md-8 text-right">
            <a class="accordion-toggle btn btn-success btn-sm toggleAccordian" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="Cicon"> <i class="fa fa-plus"></i></span> Advanced Search </a>
        </div>
        <div class="col-md-12 m-t-15">
            <div class="row">
                <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="box">
                        <div class="box-body">
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="sel1">Group Name</label> 
                                    <input class="form-control input-lg" placeholder="" type="text" value="{{ !empty($searchData['searchData']['groupName']) ? $param['searchData']['groupName'] : ''}}" name="searchData[groupName]">
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="sel1">Coordinator Name</label> 
                                    <input class="form-control input-lg" placeholder="" type="text" name="searchData[coordinator]" value="{{ !empty($searchData['searchData']['coordinator']) ? $param['searchData']['coordinator'] : ''}}">
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="sel1">Payment Status</label>
                                    <select class="form-control input-lg customSelect2" id="rootCategory" name="searchData[paymentStatus]">
                                        <option value="">All</option>
                                        <option value="0" {{ (!empty($searchData['searchData']['paymentStatus']) && $searchData['searchData']['paymentStatus'] == 0) ? 'selected':'' }}>Unpaid</option>
                                        <option value="1" {{ (!empty($searchData['searchData']['paymentStatus']) && $searchData['searchData']['paymentStatus'] == 1) ? 'selected':'' }}>Paid</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="form-group col-md-12 text-right">
                                    <button class="btn btn-default custumButt solidBtn m-r-15 m-t-20">Search</button>
                                    <button type="button" onclick="location.href = '{{url('administrator/usergroupcommission/showall')}}'" class="btn btn-danger custumButt solidBtn  m-r-15 m-t-20">Show All</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div> 
        {{ Form::close() }}
    </div>
    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="20%">Group</th>
                                <th width="10%">Co-ordinator</th>
                                <th width="10%">Shipment #</th>
                                <th width="10%">Total Commission</th>
                                <th width="10%">Payment Request</th>
                                <th width="10%">Payment Status</th>       
                                <th class="text-center" width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($chCatData)
                            @foreach ($chCatData as $cat)
                            @php
                            $deleteLink = url('administrator/managedhlsettings/delete/' . $cat->id . '/' . $page);
                            if(empty($cat->RedemedCommission))
                                $cat->RedemedCommission = '0.00';
                            @endphp
                            <tr>
                                <td>{{$cat->groupName}}</td>
                                <td>{{$cat->firstName." ".$cat->lastName}} <a class="actionIcons edit" href="javascript:void(0)" onclick="showAddEdit({{$cat->coordinatorId}},0,'usergroupcommission/coordinatoraccountinfo')"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                                <td>{{$cat->shipmentId}}</td>
                                <td>{{(new \App\Helpers\customhelper)->getCurrencySymbolFormat($cat->commissionAmount)}}</td>  
                                <td>
                                    {{ ($cat->isRequested == 1 ? 'Request Submitted' : 'Not Requested') }}
                                </td>
                                <td>
                                    <span class="labelImg" data-toggle="tooltip" title="" data-original-title="{{ ($cat->isRedem == '1')?'Paid':'Not Paid' }}">
                                        @if($cat->isRedem == '1')
                                            <img src="{{asset('public/administrator/img/paid-money.png')}}">
                                        @else
                                            <img src="{{asset('public/administrator/img/not-paid-money.png')}}">
                                        @endif
                                    </span>
                                </td>
                                <td class="text-center">
                                    @if($canEdit == 1 && $cat->isRedem =='0')
                                    <a class="actionIcons text-green edit" href="{{url('administrator/usergroupcommission/settlement/'.$cat->id.'/'.$page)}}" data-toggle="confirmation"><i title="Mark As Paid" class="fa fa-check-square-o" aria-hidden="true"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @else 
                            <tr>
                                <td colspan="3">No Record Found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $chCatData->firstItem() . ' - ' . $chCatData->lastItem() . ' of  ' . $chCatData->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $chCatData->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->


    <!--modal close-->
    <!--modal open-->
    <div class="modal fade" id="modal-addEdit">
    </div>
    <!--modal close-->
    <!--modal open-->
    <div class="modal fade" id="modal-edit">
    </div>
    <!--modal close-->
</section>       
<!-- /.content -->             
@endsection