<script src = "{{ asset('public/administrator/controller-css-js/groupshipment.js') }}" ></script>

<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        <div class="modal-body">
            {{ Form::open(array('url' => 'administrator/groupshipments/addeditlocation/'.$id.'/'.$shipmentId, 'name' => 'addeditLocationFrm', 'id' => 'addeditLocationFrm', 'method' => 'post')) }}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Location Row <span class="text-red">*</span></label>
                        <select name="warehouseRowId" id="warehouseRowId" onchange="getWarehouseZoneList(this.value)" required="" class="customSelect form-control">
                            <option value="">Select</option>
                            @foreach($warehouseRowList as $row)
                            <option {{!empty($warehouseData->warehouseRowId) && $warehouseData->warehouseRowId==$row->id?'selected="selected"':''}} value="{{$row->id}}">{{$row->name}}</option>
                            @endforeach
                        </select>           
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Location Zone <span class="text-red">*</span></label>
                        <select name="warehouseZoneId" id="warehouseZoneId" required="" class="customSelect form-control">
                            <option value="">Select</option>
                            @if(!empty($id))
                            @foreach($warehouseZoneList as $row)
                            <option {{!empty($warehouseData->warehouseZoneId) && $warehouseData->warehouseZoneId==$row->id?'selected="selected"':''}} value="{{$row->id}}">{{$row->name}}</option>
                            @endforeach
                            @endif
                        </select>                   
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <div class="text-right">
                    <button type="button"  class="btn btn-success" onclick="addeditlocation()"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
<script>
    $(function () {
        $("#addeditFrm").validate();
    });
</script>