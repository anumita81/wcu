<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Create Order</h4>
        </div>
        <div class="modal-body">
            @if(empty($shippingMethods))
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                        <label class="warning">No shipping charges set for source and destination zone</label>
            @endif
            {{ Form::open(array('id'=>'data-form','url' => route('shipmentcreateorder',['id'=>$shipmentId,'page'=>$page]),'files'=>true)) }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Choose Shipping Method <span class="text-red">*</span></label>
                            <select class="customSelect form-control" name="shippingMethod" required="">
                            @foreach($shippingMethods as $eachShippingId => $eachShippingMethod)
                                <option value="{{ $eachShippingId }}">{{ $eachShippingMethod }}</option>
                            @endforeach    
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="text-right">
                        <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->