@extends('Administrator.layouts.master')
@section('content')

<section class="content">
    <div class="row m-b-15">
        {{ Form::open(array('url' => 'administrator/media/index', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
        <div class="col-lg-3 col-md-3"> 
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchData['searchDisplay']=='10'?'selected':''}} value="10">10</option>
                    <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                    <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                    <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                    <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                </select>
            </div>
            <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
            <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
        </div>
        {{ Form::close() }}
        @if($canAdd == 1)
        <div class="col-lg-9 col-md-9 text-right">
            <a href="javascript:void(0);" onclick="showAddEdit(0, {{$page}}, 'media/addedit');" class="btn btn-info btn-sm"><span class="Cicon"><i class="fa fa-plus"></i></span> Add New File</a>
        </div>
        @endif
    </div>
    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th >Name</th>
                                <th >Copy to Clipboard</th>
                                <th width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($mediaData)
                            @foreach ($mediaData as $media)
                            @php
                            $deleteLink = url('administrator/media/delete/' . $media->id . '/' . $page);
                            @endphp
                            <tr>
                                <td><img id="defaultLoaded" class="thumbnail img-preview" @php if(isset($media->fileName)) { @endphp src="{{ asset('public/uploads/media/thumb/') }}/{{$media->fileName}}" @php } else { @endphp src="{{ asset('public/administrator/img/default-no-img.jpg') }}" @php } @endphp title="Preview Logo"</td>
                                <td><input type="text" size="105" class="form-control" id="imageId{{$media->id}}" readonly value="{{ asset('public/uploads/media/original/') }}/{{$media->fileName}}">
                                <a class="actionIcons text-black edit" ><i data-toggle="tooltip" title="Click to Copy" id="{{$media->id}}" class="fa fa-fw far fa-copy copytoclip"></i></a>
                                </td>
                                <td class="text-center">
                                    
                                    @if($canDelete == 1)
                                    <a class="actionIcons color-theme-2" href="{{$deleteLink}}" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @else 
                            <tr>
                                <td colspan="3">No Record Found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $mediaData->firstItem() . ' - ' . $mediaData->lastItem() . ' of  ' . $mediaData->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $mediaData->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->


    <!--modal close-->
    <!--modal open-->
    <div class="modal fade" id="modal-addEdit">
    </div>
    <!--modal close-->
    <!--modal open-->
    <div class="modal fade" id="modal-edit">
    </div>
    <!--modal close-->
</section>       
<script src="{{ asset('public/administrator/controller-css-js/media.js') }}"></script>
<!-- /.content -->             
@endsection