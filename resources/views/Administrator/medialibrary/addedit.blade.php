<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        <div class="modal-body">
            @if(!isset($mediaData))
            {{ Form::open(array('url' => 'administrator/media/savedata/0/'.$page, 'files' => true, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
            @else
            {{ Form::open(array('url' => 'administrator/media/savedata/'.$mediaId. '/' .$page, 'files' => true, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
            @endif
            <div class="row">
                <div class="col-md-12" style="display:none;">
                    <div class="form-group">
                        <label>Select Type <span class="text-red">*</span></label>
                        <select required="" class="form-control customSelect" name='fileType'>
                                <option value="I">Image</option>
                                
                        </select>
                    </div>
                </div>
                
            </div>

            <!-- <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Upload Image File (Max. 4MB) <span class="text-red">*</span></label>
                        <input type="file" name="fileName">
                    </div>
                </div>
                
            </div> -->

            <div class="form-group">
                <!--<label>Image</label>-->
                <div class="main-img-preview">
                    <img id="storeIcon" class="thumbnail img-preview" src="{{ asset('public/administrator/img/default-no-img.jpg') }}" title="Preview Logo">
                         <img id="loaded" src="#" alt="" style="display:none;">
                    <span class="closeImg"><i class="fa fa-times-circle"></i></span>
                </div>
                <div class="input-group">

                    <div class="input-group-btn">
                        <div class="fileUpload fixedWidth111 btn btn-custom-theme fake-shadow">
                            <span>Upload Image</span>
                            <input id="logo-id" name="fileName" type="file" class="attachment_upload">
                        </div>
                    </div>

                    <!--<input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled">-->
                </div>
                <p>(Max. 4MB) </p>
            </div>



            <div class="modal-footer">
                <div class="text-right" id="submitMediaImage">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
<script src="{{ asset('public/administrator/controller-css-js/media.js') }}"></script>
<script>
    $(function () {
        $("#addeditFrm").validate();
    });
</script>