<script src="{{ asset('public/administrator/controller-css-js/media.js') }}"></script>

<div class="modal-dialog">
    <style>
        .selectable .ui-selecting {
            border: 2px dashed #ff0000;
        }

        .selectable .ui-selected {
            border: 2px solid #ff0000;
            -webkit-box-shadow: 0px 0px 8px 0px rgba(255, 0, 0, 1);
            -moz-box-shadow: 0px 0px 8px 0px rgba(255, 0, 0, 1);
            box-shadow: 0px 0px 8px 0px rgba(255, 0, 0, 1);
        }

        .selectable img {
            margin: 5px;
            padding: 5px;
            border: 2px solid #ffffff;
        }

        .selected-items {
            border: 2px solid #ff0000;
            line-height: 32px;
        }
    </style>
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        <div class="modal-body">
            <input type="hidden" name="_csrfToken" id="csrfToken" value="{{ csrf_token() }}">
            <input type="hidden" name="base_path_loadata" value="{{ url('/').'/administrator/media/loaddata/' }}">
            <input type="hidden" name="base_path_loaddataSelected" value="{{ url('/').'/administrator/media/loaddataselected/' }}">

            @if(count($mediaData) > 0)
            <div class="selectable" id="load-data">
                @foreach ($mediaData as $media)

                <tr>
                    <td><img id="{{$media->id}}" class="ui-widget-content" @php if(isset($media->fileName)) { @endphp src="{{ asset('public/uploads/media/thumb/') }}/{{$media->fileName}}" @php } else { @endphp src="{{ asset('public/administrator/img/default-no-img.jpg') }}" @php } @endphp title="Preview Logo" /></td>
                </tr>

                @endforeach
            </div>

            <p class="selected-items" style="display:none;">You have selected the following images: <span id="selected-item-list"></span></p>


            <div id="remove-row">
                <button id="btn-more" data-id="{{$lastId}}" class="nounderline btn-block mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" ><span class="Cicon" ><i class="fa fa-paper-plane"></i></span> Load More </button>
            </div>


            <div class="modal-footer">
                <div class="text-right">
                    <a href="javascript:void(0);" class="btn btn-success" onclick="insertAtCaret('{{$editorName}}')"><span class="Cicon" ><i class="fa fa-paper-plane"></i></span>Add Images</a>
                </div>
            </div>
            @else
            <div>
                No data found !
            </div>
            @endif
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
