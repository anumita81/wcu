
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Backup Database</h4>
        </div>
        <div class="modal-body">
            <div class="container">
                <div class="loading-progress"></div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('public/administrator/js/jquery.progresstimer.js') }}"></script>
<script>
    var progress = $(".loading-progress").progressTimer({
        timeLimit: 10,
        onFinish: function () {
            alert('completed!');
        }
    });
    $.ajax({
       url:baseUrl+'/dbbackup-restore/createbackup';
    }).error(function(){
        progress.progressTimer('error', {
            errorText:'ERROR!',
            onFinish:function(){
                alert('There was an error processing your information!');
            }
        });
    }).done(function(){
        progress.progressTimer('complete');
    });
</script>