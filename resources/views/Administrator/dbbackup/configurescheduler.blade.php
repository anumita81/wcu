<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Configure Scheduler</h4>
        </div>
        {{ Form::open(array('url' => 'administrator/dbbackup-restore/saveconfiguration/0/0', 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
        <div class="modal-body">
            
            <div class="row">                   
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Schedule Backup Event By <span class="text-red">*</span></label>
                        <select class="form-control customSelect" id="eventBy" name="event_by" required="">
                            <option value="">Select</option>
                            <option value="daily" {{ ($eventBy=='daily')?'selected':'' }}>Daily</option>
                            <option value="weekly" {{ ($eventBy=='weekly')?'selected':'' }}>Weekly</option>
                            <option value="monthly" {{ ($eventBy=='monthly')?'selected':'' }}>Monthly</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6" id="scheduleOption">
                    <div class="form-group">
                        <label>Schedule Backup Event On <span class="text-red">*</span></label>
                        <div class="optionValues daily_option" style="{{ ($eventBy=='daily')?'':'display:none' }}">
                            <label>Hour</label>
                            <select class="form-control customSelect" name="dailyval[]">
                                @for($h = 0; $h <= 23; $h++)
                                <option value="{{$h}}" {{ ($eventOn[0]==$h)?'selected':'' }}>{{ $h }}</option>
                                @endfor
                            </select>
                            <label>Minute</label>
                            <select class="form-control customSelect" name="dailyval[]">
                                @for($m = 0; $m <= 59; $m++)
                                <option value="{{$m}}" {{ (!empty($eventOn[1]) && $eventOn[1]==$m)?'selected':'' }}>{{ $m }}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="optionValues weekly_option" style="{{ ($eventBy=='weekly')?'':'display:none' }}">
                            <label>Day of week</label>
                            <select class="form-control customSelect" name="weeklyval">
                            @for($w = 1; $w <= 7; $w++)
                            <option value="{{$w}}" {{ ($eventOn[0]==$w)?'selected':'' }}>{{ $w }}</option>
                            @endfor
                            </select>
                        </div>
                        <div class="optionValues monthly_option" style="{{ ($eventBy=='monthly')?'':'display:none' }}">
                            <label>Day of Month</label>
                            <select class="form-control customSelect" name="monthlyval">
                            @for($d = 1; $d <= 28; $d++)
                            <option value="{{$d}}" {{ ($eventOn[0]==$d)?'selected':'' }}>{{ $d }}</option>
                            @endfor
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
        <div class="modal-footer">
            <div class="text-right">
                <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
            </div>
        </div>
        {{Form::close()}}
    </div>
</div>
<script>
$(function () {
   $("#eventBy").on("change",function(){
      if($(this).val() !='')
      {
          $("#scheduleOption").show();
          $(".optionValues").hide();
          $("."+$(this).val()+"_option").show();
      }
      else
      {
          $("#scheduleOption").hide();
          $(".optionValues").hide();
      }
   }); 
});
</script>