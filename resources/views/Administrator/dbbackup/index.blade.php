@extends('Administrator.layouts.master')
@section('content')

<!-- Main content -->
<section class="content">   
    <div class="row m-b-15">
        <div class="col-lg-7 col-md-9 text-right">&nbsp;</div>
        <div class="col-lg-5 col-md-9 text-right">
            @if($canAdd == 1)<a href="{{ url('administrator/dbbackup-restore/createbackup') }}" class="btn btn-info btn-sm pull-right">Create Backup</a>@endif &nbsp;
            @if($canAdd == 1)<a href="javascript:void(0)" onclick="showAddEdit(0, {{$page}}, 'dbbackup-restore/configurescheduler');" class="btn btn-info btn-sm pull-right">Configure Scheduler</a>@endif
        </div>
    </div>                 
    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->                          

            <div class="box">
                <div class="box-body"> 
                    <table id="example2" class="table table-bordered table-hover countryName">
                        <thead>
                            <tr>                                                
                                <th>File name</th>
                                <th>Created From</th>
                                <th>Created By</th>
                                <th>Created On</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($dbBackupData))
                            @foreach ($dbBackupData as $eachBackupData)
                            @php 
                            $deleteLink = url('administrator/dbbackup-restore/delete/'.$eachBackupData->id); 
                            @endphp
                            <tr>
                                <td>{{$eachBackupData->backupFile}}</td>
                                <td>{{$eachBackupData->createdByType}}</td>
                                <td>{{ $eachBackupData->createdBy }}</td>
                                <td>{{$eachBackupData->createdOn}}</td>
                                <td>
                                    @if($canEdit == 1)<a class="actionIcons" href="{{url('administrator/dbbackup-restore/download/'.$eachBackupData->id)}}"><i class="fa fa-download" aria-hidden="true"></i></a>@endif
                                    @if($canDelete == 1)<a class="color-theme-2 actionIcons" href="{{$deleteLink}}" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a>@endif
                                    
                                </td>
                            </tr>
                            @endforeach
                            @else 
                            <tr>
                                <td colspan="9">No Record Found</td>
                            </tr>
                            @endif                                      
                        </tbody>

                    </table>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->


    <!--modal open-->
    <div class="modal fade" id="modal-addEdit">

    </div>
    <!--modal close-->

</section>
<!-- /.content --
@endsection