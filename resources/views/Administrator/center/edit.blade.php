@extends('Administrator.layouts.master')
@section('content')

<script src = "{{ asset('public/administrator/controller-css-js/center.js') }}" ></script>

<!-- Main content -->
<section class="content">

    <div class="row"> 
        <section class="col-lg-8 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <!--<div class="box-header">
                  <h3 class="box-title">Hover Data Table</h3>
                </div>-->
                <!-- /.box-header -->
                {{ Form::open(array('url' => 'administrator/center/save/'.$id.'/'.$page, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Center Name</label>
                                <input id="centerName" name="centerName" class="form-control" required="" value="{{$center->centerName}}"  placeholder="Enter Center Name" type="text">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Center Code <span class="text-red">*</span></label>
                                <input id="centerCode" name="centerCode" class="form-control" required="" value="{{$center->centerCode}}"  placeholder="Enter  Center Code" type="text">
                            </div>
                        </div>
                    </div>
                     <div class="row m-t-15">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Password</label>
                                <input  id="password" name="password" class="form-control" minlength="8" placeholder="Enter Password" type="password">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Confirm Password </label>
                                <input  id="confirmpassword" name="conifrm_password" class="form-control" minlength="8" equalTo="#password" placeholder="Confirm Password" type="password">
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-15">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contact Person Name <span class="text-red">*</span></label>
                                <input  id="contactPersonName" name="contactPersonName" class="form-control" required="" value="{{$center->contactPersonName}}"  placeholder="Enter Contact Person Name" type="text">
                            </div>
                        </div>
                        <div class="col-md-6">
                                <div class="form-group">
                                <label>Contact Person Phone<span class="text-red">*</span></label>
                                <input  id="contactPersonPhone" name="contactPersonPhone" class="form-control" value="{{$center->contactPersonPhone}}" required="" placeholder="Enter Contact No." type="text">
                            </div>
                           
                        </div>
                    </div>
                   
                    <div class="row m-t-15">
                        <div class="col-md-6">                            
                             <div class="form-group">
                                <label>Contact Person Alteanate Phone</label>
                                <input  id="contactPersonAlterPhone" name="contactPersonAlterPhone" class="form-control" value="{{$center->contactPersonAlterPhone}}" placeholder="Enter Alternate No" type="text">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contact Person Email<span class="text-red">*</span></label>
                                <input  id="contactPersomEmail" name="contactPersomEmail" class="form-control" value="{{$center->contactPersomEmail}}" required="" placeholder="Enter Alternate No" type="text">
                            </div>
                        </div>
                    </div>
                     <div class="row m-t-15">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Center Address</label>
                                <input  id="address" name="address" value="{{$center->address}}" class="form-control" placeholder="Enter Address" type="text">
                            </div>
                        </div>
                         
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>

                <div class="box-footer text-right">
                    Password must contains minimum 8 characters
                </div>


                {{ Form::close() }}
                <!-- /.box-body -->


                <div class="box-header">
                    <h3 class="box-title">Address Book</h3>
                </div>

            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
    <!--modal open-->
    <div class="modal fade" id="modal-addEdit"></div>
    <!--modal close-->
</section>
<!-- /.content --> 
@endsection