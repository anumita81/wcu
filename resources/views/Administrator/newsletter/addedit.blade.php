@extends ('Administrator.layouts.master')

@section('content')

<section class="content">

    <div class="row"> 
        <section class="col-lg-8 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                             
                {{ Form::open(array('url' => 'administrator/newsletter/save/' . $id . '/' . $page, 'name' => 'frmsearch', 'id' => 'addeditFrm', 'method' => 'post', 'novalidate' => 'novalidate')) }}
                <div class="box-body">
                    <div style="{{!empty($id)?'display:none':''}}" class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Email Template</label>                               
                                <select name="emailtemplate" id="emailtemplate" class="customSelect form-control" required>
                                    <option value="0">Select Template</option>
                                    @foreach($emailTemplate as $template) 
                                    <option value="{{$template['id']}}">{{$template['templateSubject']}}</option>
                                  @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                
                    <div class="row m-t-15">  
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Newsletter Name</label>
                                <input id="newsletterName" name="newsletterName" class="form-control" placeholder="Enter newsletter name" type="text" required="" value="{{ !empty($content['newsletterName']) ? $content['newsletterName'] : ''}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Schedule Date</label>
                                <input id="scheduleDate" name="scheduleDate" class="form-control datepicker" placeholder="Enter schedule date" type="text" required="" value="{{ !empty($content['scheduleDate']) ? $content['scheduleDate'] : ''}}">
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-15">  
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Newsletter Description</label>
                                <textarea id="editor2" name="newsletterDescription" rows="10" cols="80" required="">{{ Input::old('templateBody', !empty($content['templateBody']) ? $content['templateBody'] : '') }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-15"> 
                        <div class="col-md-6 col-xs-12">                            
                            <div class="form-group">
                                <label>Schedule Repeat : </label>
                                <label> Yes <input id="scheduleRepeat" name="scheduleRepeat" class="flat-red" style="position: absolute; opacity: 0;" type="radio" required="" value="1"></label>
                                <label> No <input id="scheduleRepeat" name="scheduleRepeat" class="flat-red" style="position: absolute; opacity: 0;" type="radio" required="" value="0"></label>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Allow Html Tags : </label>
                                <label> Yes <input id="allowedHtmlTags" name="allowedHtmlTags" class="flat-red" style="position: absolute; opacity: 0;" type="radio" required="" value="Y"></label>
                                <label> No <input id="allowedHtmlTags" name="allowedHtmlTags" class="flat-red" style="position: absolute; opacity: 0;" type="radio" required="" value="N"></label>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Save</button>
                </div>
                {{ Form::close() }} 
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
        
    </div>
    <!--Block 01-->
</section>


<script src="{{ asset('public/global/bower_components/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('public/global/vendors/validator/validator.js')}}"></script>
<script>
        $(function () {
            $("#addeditFrm").validate();
            $('.datepicker').datepicker({
                autoclose: true
             });
        });
        CKEDITOR.replace('editor2', {
        allowedContent: true,
//        toolbar: [
//            {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', ]},
//            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},
//            {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates']},
//        ]
    });
</script>

@endsection

