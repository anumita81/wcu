<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        {{ Form::open(array('url' => 'administrator/newsletter/savesubscriber/' . $id . '/' . $page, 'name' => 'frmsearch', 'id' => 'addeditFrm', 'method' => 'post', 'novalidate' => 'novalidate')) }}
        <div class="modal-body">            
                 <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Select Subscriber</label>                               
                                <select name="subscriberlist[]" id="subscriberlist" class="customSelect form-control select2" multiple="" style="width:100%">
                                    <option value="0">Select Subscriber</option>
                                    @foreach($subscriberList as $subscriber) 
                                    <option value="{{$subscriber['userId']}}" {{ (in_array($subscriber['userId'], $selectedSubscriberList)) ? 'selected':'' }}>{{$subscriber['userEmail']}}</option>
                                  @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Select Customer</label>                               
                                <select name="customerlist[]" id="customerlist" class="customSelect form-control select2" multiple="" style="width:100%">
                                    <option value="0">Select Customer</option>
                                    @foreach($customerList as $customer) 
                                    <option value="{{$customer['id']}}" {{ (in_array($customer['id'], $selectedSubscriberList)) ? 'selected':'' }}>{{$customer['email']}}</option>
                                  @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Save</button>
        </div>                
        {{ Form::close() }} 
        <!-- /.modal-content -->
    </div>
</div>
<!-- /.modal-dialog -->
<script>
    $(function () {
        $("#addeditFrm").validate();
         $('#subscriberlist').select2({
             dropdownParent: $('#modal-addEdit')
         });
         $('#customerlist').select2({
             dropdownParent: $('#modal-addEdit')
         });
     });
</script>


