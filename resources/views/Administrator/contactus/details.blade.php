@extends('Administrator.layouts.master')
@section('content')
<section class="content">
    <div class="row"> 
        <section class="col-lg-8"> 
            <!-- Custom tabs (Charts with tabs)-->
            {{ Form::open(array('id'=>'data-form','class'=>'form-horizontal','url' => route('contactusreply',["id"=>$id,"page"=>$page]),'files'=>true)) }}
                <!-- /.nav-tabs-custom -->

                <div class="box">
                    @if($record->reason_id != '11')
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <div class="form-row">
                <div class="form-group col-sm-2">
                    <label>Name
                        
                    </label>
                </div>
                <div class="col-sm-4">    
                    <div>{{ $record->name }}</div>
                </div>
                <div class="form-group col-sm-4">
                    <label>Company</label>
                    <div>{{ $record->company }}</div>
                </div>
                <div class="form-group col-sm-12">
                    <label>Address
                        
                    </label>
                    <div>{{ $record->address }}</div>
                </div>
                <div class="form-group col-sm-12">
                    <label>Address(Line 2)</label>
                    <div>{{ $record->alternateAddress }}</div>
                </div>
                <div class="form-group col-sm-3 col-6">
                    <label>Country
                        
                    </label>
                    <div>{{ $record->name }}</div>
                </div>
                <div class="form-group col-sm-3 col-6">
                    <label>State
                        
                    </label>
                    <div>{{ $record->name }}</div>
                </div>
                <div class="form-group col-sm-3 col-6">
                    <label>City
                        
                    </label>
                    <div>{{ $record->name }}</div>
                </div>
                <div class="form-group col-sm-3 col-6">
                    <label>Zip/Postal code</label>
                    <div>{{ $record->zipcode }}</div>
                </div>
                <div class="form-group col-sm-4 col-6">
                    <label>Phone
                        
                    </label>
                    <div>{{ $record->phone }}</div>
                </div>
                <div class="form-group col-sm-4 col-6">
                    <label>Email
                        
                    </label>
                    <div>{{ $record->email }}</div>
                </div>
                <div class="form-group col-sm-4 col-6">
                    <label>Fax</label>
                    <div>{{ $record->fax }}</div>
                </div>
                <div class="form-group col-6">
                    <label>Website</label>
                    <div>{{ $record->website }}</div>
                </div>
                <div class="form-group col-sm-6">
                    <label>Subject
                        
                    </label>
                    <div>{{ $record->subject }}</div>
                </div>
                
                <div class="form-group col-sm-12 mb-0">
                    <label>Message
                        
                    </label>
                    <div>{!! $record->message !!}</div>

                </div>
                @endif

                @if($record->reason_id == '11')
                @php
                $msg = json_decode($record->message);
                @endphp


                <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <div class="form-row">
                
                <div class="form-group col-sm-3 col-6">
                    <label>Date
                        
                    </label>
                    <div>{{\Carbon\Carbon::parse($msg->claimDate)->format('m-d-Y')}}</div>
                </div>

                 <div class="form-group col-sm-3 col-6">
                    <label>Name of Claimant
                        
                    </label>
                    <div>{{ $msg->cliamentName }}</div>
                </div>

                <div class="form-group col-sm-3 col-6">
                    <label>Address
                        
                    </label>
                    <div>{{ $msg->cliamentAddress }}</div>
                </div>


                <div class="form-group col-sm-3 col-6">
                    <label>Phone
                        
                    </label>
                    <div>{{ $msg->cliamentPhone }}</div>
                </div>


                <div class="form-group col-sm-3 col-6">
                    <label>Description of item
                        
                    </label>
                    <div>{{ $msg->Descriptionofitem }}</div>
                </div>


                <div class="form-group col-sm-3 col-6">
                    <label>Order Number
                        
                    </label>
                    <div>{{ $msg->OrderNumber }}</div>
                </div>


                <div class="form-group col-sm-3 col-6">
                    <label>Item number
                        
                    </label>
                    <div>{{ $msg->ItemNumber }}</div>
                </div>


                <div class="form-group col-sm-3 col-6">
                    <label>Value if item at date of purchase
                        
                    </label>
                    <div>{{ $msg->Valueifitematdateofpurchase }}</div>
                </div>


                <div class="form-group col-sm-3 col-6">
                    <label>Purchase date for item
                        
                    </label>
                    <div>{{\Carbon\Carbon::parse($msg->PurchaseDateforItem)->format('m-d-Y')}}</div>
                </div>

                <div class="form-group col-sm-3 col-6">
                    <label>Place of purchase
                        
                    </label>
                    <div>{{ $msg->PlaceOfPurchase }}</div>
                </div>

                <div class="form-group col-sm-3 col-6">
                    <label>Total amount been claimed
                        
                    </label>
                    <div>{{ $msg->Totalamountbeenclaimed }}</div>
                </div>

                <div class="form-group col-sm-3 col-6">
                    <label>Explain nature of damage
                        
                    </label>
                    <div>{{ $msg->Explainnatureofdamage }}</div>
                </div>

                <div class="form-group col-sm-3 col-6">
                    <label>Shipment via
                        
                    </label>
                    <div>{{ $msg->shipmentVia }}</div>
                </div>

                <div class="form-group col-sm-3 col-6">
                    <label>The Claim is made against the carrier for
                        
                    </label>
                    <div>@php if($msg->loss == NULL) { echo ''; } else { echo $msg->loss."<br>"; }  
                        if($msg->damage == NULL) { echo ''; } else { echo $msg->damage."<br>"; } 
                        if($msg->incomplete == NULL) { echo ''; } else { echo $msg->incomplete; } @endphp</div>
                </div>


                <div class="form-group col-sm-12 col-12">
                    <label>Date items were received
                        
                    </label>
                    <div>{{ $msg->Dateitemswerereceived }}</div>
                </div>
                @if(isset($msg->image))
                <div class="form-group col-sm-12 col-12">
                    <label>Scanned copy of invoice
                        
                    </label>
                    <div><a download href="{{$msg->image}}">{{$msg->image}}</a></div>
                </div>
                @endif

                @if(isset($msg->image2))
                <div class="form-group col-sm-12 col-12">
                    <label>Photo of Damaged Item
                        
                    </label>
                    <div><a download href="{{$msg->image2}}">{{$msg->image2}}</a></div>
                </div>
                @endif
                @endif

                @if($record->status != 4)
                <div class="form-group col-sm-12 mb-0">
                    <textarea class="form-control" name="reply"></textarea>

                </div>
                <div class="form-group col-sm-12 mb-0">
                    <button class="btn btn-warning" type="submit"><i class="fa fa-fw fa-mail-forward fa-lg"></i>Reply</button>
                </div>
                @endif
            </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <a class="btn btn-info" href="{{route('contactuslist')}}"><span class="Cicon"><i class="fa fa-arrow-left"></i></span>Back</a>
                        
                    </div>
                    <!-- /.box-body -->
                </div>
            {{Form::close()}}

        </section>
    </div>
    <!--Block 01-->

    <section class="col-lg-12 connectedSortable"> 
        <!-- Custom tabs (Charts with tabs)-->                          

        <div class="">

            <div class="box-body">
                                                 
                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                    <thead>
                        <tr role="row">
                            <th>Reply</th>
                            <th>By</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($replied)>0)
                        @foreach($replied as $reply)
                        <tr role="row" class="odd">
                            <td width="50%">{{ $reply->replyText }}</td>
                            <td>{{ $reply->firstName }} {{ $reply->lastName }}</td>
                            <td>{{ \Carbon\Carbon::parse($reply->repliedOn)->format('m-d-Y')}}</td>
                        </tr>
                        @endforeach
                        @endif    
                    </tbody>
                </table>


            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.nav-tabs-custom --> 
    </section>

</section>
@endsection