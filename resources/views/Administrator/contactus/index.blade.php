@extends('Administrator.layouts.master')
@section('content')   
<section class="content">   
    <div class="row m-b-15">
        {{ Form::open(array('url' => route('contactus'), 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
        <div class="col-md-12 text-right">
            <div class="col-md-3">
                <div class="pull-left"> <label>Entries per page</label>
                    <div class="adddrop m-l-15 w-100">
                        <select class="form-control highLight" name="searchDisplay" onchange="$('#frmsearch').submit();">

                            @for($step = 10; $step<=50; $step+=10)
                            <option value="{{ $step }}" {{($param['searchDisplay'] == $step) ? "selected" : ""}}>{{ $step }}</option>
                            @endfor
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="pull-left"> <label>Status</label>
                    <div class="adddrop m-l-15 w-100">
                        <select class="form-control highLight" name="searchData" onchange="$('#frmsearch').submit();">
                        <option value="">All</option>
                            @foreach($allStatus as $statusId => $eachStatus)
                            <option value="{{ $statusId }}" {{($param['searchData'] == $statusId) ? "selected" : ""}}>{{ $eachStatus }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="pull-left"> <label>Type</label>
                    <div class="adddrop m-l-15 w-100">
                        <select class="form-control highLight" name="searchByType" onchange="$('#frmsearch').submit();">
                            <option value="">All</option>
                            <option value="12" {{($param['searchByType'] == 12) ? "selected" : ""}}>Contact Us</option>
                            <option value="11" {{($param['searchByType'] == 11) ? "selected" : ""}}>File a Claim</option>
                        </select>
                    </div>
                </div>
            </div>
            @if($param['searchByType'] == 12)
            <div class="col-md-2">
                <div class="pull-left"> <label>Reason</label>
                    <div class="adddrop m-l-15 w-100">
                        <select class="form-control highLight" name="searchByReason" onchange="$('#frmsearch').submit();">
                            <option value="">All</option>
                            @foreach($reasons as $reasonVal)
                            <option value="{{ $reasonVal->id }}" {{($param['searchByReason'] == $reasonVal->id) ? "selected" : ""}}>{{ $reasonVal->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            @endif

            <div class="col-md-3 text-right">                 
                @if($canDelete == 1)<a href="javascript:void(0)" class="accordion-toggle btn btn-success btn-sm" onclick="deleteSelected()"><span class="Cicon"> <i data-toggle="tooltip" title="Click To Delete Data" class="fa fa-fw fa-trash"></i></span> Delete Selected </a>@endif
            </div>
            <input type="hidden" name="field" id="field" value="{{$param['sortField']}}" />
            <input type="hidden" name="type" id="type" value="{{$param['sortOrder']}}" />
        </div> 
        {{ Form::close() }} 
    </div>

    <section class="col-lg-12 connectedSortable"> 
        <!-- Custom tabs (Charts with tabs)-->                          

        <div class="box">

            <div class="box-body">
                {{ Form::open(array('url' => route('contactusdeleteSelected'), 'name' => 'frmsearch', 'id' => 'deleteAll', 'method' => 'post')) }}                                    
                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                    <thead>
                        <tr role="row">
                            <th><label>
                                    <input type="checkbox" class="flat-red chk_all"></label></th>
                            <th>Contacted By</th>
                            <th>Contact Subject</th>
                            <th class="{{ ($param['sortField'] == 'postedOn')? 'sorting_'.$param['sortOrder'] :'' }} sorting sortby" data-sort="postedOn">Request sent on</th>
                            <th width="15%">Status</th>
                            <th width="15%">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($records)>0)
                        @foreach($records as $record)
                        <tr role="row" class="odd">
                            <td class="sorting_1"><label>
                                    <input type="checkbox" class="flat-red checkbox" name="deleteData[]" value="{{ $record->id }}"></label></td>
                            <td>{{ $record->name }}</td>
                            <td>{{ $record->subject }} @php if($record->reason_id == 11) { $msg = json_decode($record->message); echo "(".$msg->OrderNumber .')';} @endphp</td>
                            <td>{{ $record->postedOn }}</td>
                            <td>
                                <button data-toggle="tooltip" title="Status : {{ $allStatus[$record->status] }}" type="button" class="btn btn-sm {{($record->status=='1'?'btn-danger':($record->status=='2'?'btn-info': ($record->status=='4' ? 'btn-primary' : 'btn-success')))}}">{{ $allStatus[$record->status] }}</button>
                            </td>
                            <td>
                                @if($canEdit == 1)<a class="actionIcons text-green editStatus" href="{{ route('contactusdetails',['id'=>$record->id,'page'=>$page]) }}">
                                    <i data-toggle="tooltip" title="Click to View Details" class="fa fa-fw fa-eye"></i>
                                </a>@endif
                                @if($canDelete == 1)<a class="color-theme-2 actionIcons" href="{{ route('contactusdelete',['id'=>$record->id,'page'=>$page]) }}" data-toggle="confirmation"><i data-toggle="tooltip" title="Click To Delete Data" class="fa fa-fw fa-trash"></i>
                                </a>@endif

                                @if($canEdit == 1)
                                @if($record->status != 4)
                                <a class="actionIcons editStatus" href="{{ route('resolved',['id'=>$record->id,'page'=>$page]) }}">
                                    <i data-toggle="confirmation" title="Click to Resolve. Once Resolved, you will be unable to reply further" class="fa fa-stop-circle"></i>
                                </a>
                                @endif
                                @endif
                            </td>

                        </tr>
                        @endforeach
                        @endif    
                    </tbody>
                </table>
                {{ Form::close() }}
                <div class="row mt-20">
                    <div class="col-md-6 col-sm-6 col-xs-6 for12">
                        <p class="results">Showing {{ $records->firstItem() . ' - ' . $records->lastItem() . ' of  ' . $records->total() }}</p>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                            {{ $records->links() }}
                        </div>
                    </div>
                </div>


            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.nav-tabs-custom --> 
    </section>
</div>
<input type="hidden" id="ajaxPage" value="editsiteproductstatus">
<input type="hidden" id="subcatPage" value="getsubcategory">
<div class="modal fade" id="modal-addEdit"></div>
<!--modal open-->
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-red"><span class="text-red"><i class="fa fa-exclamation-triangle"></i></span> Are you sure?</h4>
            </div>
            <div class="modal-body">
                <p>The items will be deleted permanently. The action cannot be reversed.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="$('#deleteAll').submit();">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!--modal close-->
</section>

@endsection