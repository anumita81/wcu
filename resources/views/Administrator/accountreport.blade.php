@extends('Administrator.layouts.master')
@section('content')
<script src = "{{ asset('public/administrator/controller-css-js/report.js') }}" ></script>
<section class="content">
    {{ Form::open(array('url' => 'administrator/report/account', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
    <div class="row">
        <div class="col-md-4">
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchData['searchDisplay']=='1000'?'selected':''}} value="1000">1000</option>
                    <option {{$searchData['searchDisplay']=='2000'?'selected':''}} value="2000">2000</option>
                    <option {{$searchData['searchDisplay']=='3000'?'selected':''}} value="3000">3000</option>
                    <option {{$searchData['searchDisplay']=='4000'?'selected':''}} value="4000">4000</option>
                    <option {{$searchData['searchDisplay']=='5000'?'selected':''}} value="5000">5000</option>
                </select>

            </div>
            <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
            <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
        </div>
        <div class="col-md-8 text-right"> <a class="accordion-toggle btn btn-sm btn-success toggleAccordian"
                                             data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="Cicon"> <i
                        class="fa fa-minus"></i></span> Advanced Search </a> <a href="#" class="btn btn-sm btn-warning" onclick="exportallCustomer();"><span class="Cicon"><i class="fa fa-share"></i></span>
                Export All</a>  </div>
    </div>
    <div class="col-md-12 m-t-15">
        <div class="row">
            <div id="collapseTwo" class="panel-collapse collapse in" aria-expanded="true">
                <div class="box">
                    <div class="box-header">
                       <h3 class="box-title">Generate Report For :</h3>     
                    </div> 
                    <div class="box-body">
                        <div class="form-row">
                            <div class="col-md-12 form-group">
                                <div class="withRdaioButtons" style="padding:15px 0 0 0;">  
                                    <label>
                                        <input type="radio" name="searchByCondition" value="allshipment" class="flat-red searchBy" {{$searchData['searchByCondition']=='allshipment'?'checked':''}}>
                                        <span class="radioSpan">All Shipment</span> 
                                    </label>
                                    <label>
                                        <input  type="radio" name="searchByCondition" value="withstoragecharge" class="flat-red searchBy" {{$searchData['searchByCondition']=='withstoragecharge'?'checked':''}}>
                                        <span class="radioSpan">Shipments with storage charges</span> 
                                    </label>
                                    <label>
                                        <input  type="radio" name="searchByCondition" value="withoutstoragecharge" class="flat-red searchBy" {{$searchData['searchByCondition']=='shipmentweight'?'checked':''}}>
                                        <span class="radioSpan">Shipment with pending storage charge</span>
                                    </label>
                                    <label>
                                        <input type="radio" name="searchByCondition" value="revenuewithshippingmethod" class="flat-red searchBy"  {{$searchData['searchByCondition']=='revenuewithshippingmethod'?'checked':''}}>
                                        <span class="radioSpan">Revenue from various shipping methods</span> 
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 form-group">
                                 <div class="withRdaioButtons" style="padding:15px 0 0 0;">
                                    <label>
                                        <input type="radio" name="searchByCondition" value="registrationdate" class="flat-red searchBy" >
                                        <span class="radioSpan">Value of all items in a given shipment</span> 
                                    </label>
                                    <label>
                                        <input type="radio" name="searchByCondition" value="numberoforder" class="flat-red searchBy" >
                                        <span class="radioSpan">Consolidated tracking report</span> 
                                    </label>
                                    <label>
                                        <input type="radio" name="searchByCondition" value="orderdate" class="flat-red searchBy" >
                                        <span class="radioSpan">Shop For Me report</span> 
                                    </label>
                                    <label>
                                        <input type="radio" name="searchByCondition" value="procurementshipmenttype" class="flat-red  searchBy" >
                                        <span class="radioSpan">Auto Parts report</span> 
                                    </label>
                                </div>
                            </div>
                      </div>
                      <div class="form-row" id="searchByDiv"></div>
                      <div class="form-row">
                            <div class="col-md-12 text-right">
                                <div class="form-group"> 
                                    <button {{$searchData['searchByCondition']!=''?'':'disabled=""'}} class="btn btn-default custumButt searchAccountBtn solidBtn m-r-15 m-t-20">Search</button>
                                    <button type="button" onclick="location.href = '{{url("administrator/report/account/showall/account")}}'" class="btn btn-danger custumButt solidBtn  m-r-15 m-t-20">Show All</button>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
    <!--Block 01-->
    
    <div id="">
        <div class="row">
            <section class="col-lg-12 shipmentNav">
                <!-- Custom tabs (Charts with tabs)-->
                <h3>{{$pageTitle}}</h3>
                <div>
                    <div class="tab-content no-padding">
                        <!-- Morris chart - Sales -->                       
                            <div class="box mt-3">
                                <div class="box-body p-0">
                                   <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                         <div class="row">
                                          <div class="col-12">
                                            <!-- Custom Tabs -->
                                            <div class="card p-0">
                                              <div class="card-header d-flex p-0">          
                                                <ul class="nav nav-pills ml-auto p-2">
                                                  <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab">Paid</a></li>
                                                  <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab">Unpaid</a></li>                                                  
                                                </ul>
                                              </div><!-- /.card-header -->
                                              <div class="card-body">
                                                <div class="tab-content">
                                                  <div class="tab-pane active" id="tab_1">
                                                   <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="table-responsive">
                                                                <table id="example2" class="table table-bordered table-hover dataTable m-0" role="grid" aria-describedby="example2_info">
                                                                    <thead>
                                                                        <tr role="row">
                                                                            @foreach($dataList['headers'] as $key => $header)
                                                                                <th data-sort="{{$key}}" class="{{$sort[$key]['current']}} sortby" colspan="1" rowspan="1">{{$header}}</th>
                                                                             @endforeach       
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="checkboxes">
                                                                        @foreach($dataList['records'] as $record)
                                                                         @if($record['paymentStatus'] == "paid") 
                                                                            <tr>  
                                                                             @foreach($dataList['headers'] as $key => $header)
                                                                                @if($key == "paymentStatus")
                                                                                <td>{{ucfirst($record[$key])}}</td>
                                                                                @else
                                                                                <td>{{ucfirst($record[$key])}}</td>
                                                                                @endif
                                                                             @endforeach       
                                                                            </tr>     
                                                                          @endif
                                                                        @endforeach 
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                    </div>
                                                  </div>
                                                  <!-- /.tab-pane -->
                                             <div class="tab-pane" id="tab_2">
                                                <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="table-responsive">
                                                                <table id="example2" class="table table-bordered table-hover dataTable m-0" role="grid" aria-describedby="example2_info">
                                                                    <thead>
                                                                        <tr role="row">
                                                                            @foreach($dataList['headers'] as $key => $header)
                                                                                <th data-sort="{{$key}}" class="{{$sort[$key]['current']}} sortby" colspan="1" rowspan="1">{{$header}}</th>
                                                                             @endforeach       
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="checkboxes">
                                                                        @foreach($dataList['records'] as $record)
                                                                         @if($record['paymentStatus'] == "unpaid") 
                                                                            <tr>  
                                                                             @foreach($dataList['headers'] as $key => $header)
                                                                                @if($key == "paymentStatus")
                                                                                <td>{{ucfirst($record[$key])}}</td>
                                                                                @elseif($key == "totalCost")
                                                                                <td>0.00</td>
                                                                                @else
                                                                                <td>{{ucfirst($record[$key])}}</td>
                                                                                @endif
                                                                             @endforeach       
                                                                            </tr>     
                                                                          @endif
                                                                        @endforeach 
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                    </div>
                                                  </div>
                                                  <!-- /.tab-pane -->                                                
                                                </div>
                                                <!-- /.tab-content -->
                                              </div><!-- /.card-body -->
                                            </div>
                                            <!-- ./card -->
                                          </div>
                                          <!-- /.col -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                      
                    </div>
                </div>
                    <!-- /.nav-tabs-custom -->
            </section>
            @if(!empty($dataList['records']))
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{$dataList['records']->firstItem() . ' - ' .$dataList['records']->lastItem() . ' of  ' .$dataList['records']->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{$dataList['records']->links() }}
                            </div>
                        </div>
                    </div>
                    @endif
        </div>
       
     </div>
    
    <!--Block 01-->


</section>      
<!-- /.content -->  

@endsection