<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$pageTitle}}</h4>
        </div>
        <div class="modal-body">
            {{ Form::open(array('url' => 'administrator/state/save/'.$id.'/'.$page, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Country <span class="text-red">*</span></label>
                        <select {{$action=='Edit'?'disabled':''}} name="countryCode" id="countryCode" required="" class="customSelect">
                            <option value="">Select</option>
                            @foreach($countryList as $country)
                            <option {{(isset($state->countryCode) && $state->countryCode==$country['code'])?'selected':''}} value="{{$country['code']}}">{{$country['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Code <span class="text-red">*</span></label>
                        <input id="code" name="code" class="form-control" required value="{{isset($state->code)?$state->code:''}}" placeholder="Enter ..." type="text">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Name <span class="text-red">*</span></label>
                        <input id="name" name="name" class="form-control" required value="{{isset($state->name)?$state->name:''}}" placeholder="Enter ..." type="text">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->



<script>
    $(function () {
        $("#addeditFrm").validate();
    });
</script>