<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$action}} Country</h4>
        </div>
        <div class="modal-body">
            {{ Form::open(array('url' => 'administrator/country/save/'.$id.'/'.$page, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Code <span class="text-red">*</span></label>
                        <input id="code" name="code" class="form-control" required maxlength="2" value="{{$country->code}}" placeholder="Enter ..." type="text">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Name</label>
                        <input id="name" name="name" class="form-control" required value="{{$country->name}}" placeholder="Enter ..." type="text">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>ISD Code</label>
                        <input id="isdCode" name="isdCode" class="form-control" required value="{{$country->isdCode}}" placeholder="Enter ..." type="text">
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->



<script>
    $(function () {
        $("#addeditFrm").validate();
    });
</script>