@extends('Administrator.layouts.master')
@section('content')

<!-- Main content -->
<section class="content"> 
    <div class="row m-b-15">
        {{ Form::open(array('url' => 'administrator/country/index', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
        <div class="col-lg-8 col-md-8">
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                    <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                    <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                    <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                </select>
            </div>
            <label class="m-l-15">Region</label>
            <div class="adddrop m-l-15 w-200">
                <select name="searchByRegion" id="searchByRegion" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option value="">All regions</option>
                    <option {{$searchData['searchByRegion']=='NA'?'selected':''}} value="NA">North America</option>
                    <option {{$searchData['searchByRegion']=='EU'?'selected':''}} value="EU">Europe</option>
                    <option {{$searchData['searchByRegion']=='AU'?'selected':''}} value="AU">Australia and Oceania</option>
                    <option {{$searchData['searchByRegion']=='LA'?'selected':''}} value="LA">Latin America</option>
                    <option {{$searchData['searchByRegion']=='AS'?'selected':''}} value="AS">Asia</option>
                    <option {{$searchData['searchByRegion']=='AF'?'selected':''}} value="AF">Africa</option>
                    <option {{$searchData['searchByRegion']=='AN'?'selected':''}} value="AN">Antarctica</option>All
                </select>
            </div>
            <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
            <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
        </div>
        {{ Form::close() }}
        
    </div>                     
    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-body">
                    <table id="example2"  class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th data-sort="code" class="{{$sort['code']['current']}} sortby">Code</th>
                                <th data-sort="name" class="{{$sort['name']['current']}} sortby">Country Name</th>
                                <th width="15%" data-sort="isdCode" class="{{$sort['isdCode']['current']}} sortby">ISD Code</th>
                                <th width="15%">Status</th>
                                <th width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($countryData)
                            @foreach ($countryData as $country)
                            @php $url = url('administrator/country/changestatus/'.$country->id.'/'.$page); @endphp
                            <tr>
                                <td>{{$country->code}}</td>
                                <td>{{$country->name}}</td>
                                <td>{{$country->isdCode}}</td>
                                <td>
                                @if($canEdit == 1)
                                @if($country->status == 1)
                                <a data-toggle="confirmation" href="{{$url. '/0'}}" class="btn btn-success btnActive">Active</a>
                                @else 
                                <a data-toggle="confirmation" href="{{$url. '/1'}}" class="btn btn-danger btnActive">Inactive</a>
                                @endif
                                @endif
                                </td>
                                <td>
                                    @if($canEdit == 1)<a class="text-green edit actionIcons" data-toggle="tooltip" title="Click to Edit" onclick="showAddEdit({{$country->id}}, {{$page}}, 'country/addedit');"><i class="fa fa-fw fa-edit"></i></a>@endif
                                </td>
                            </tr>
                            @endforeach
                            @else 
                            <tr>
                                <td colspan="6">No Record Found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $countryData->firstItem() . ' - ' . $countryData->lastItem() . ' of  ' . $countryData->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $countryData->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--modal open-->
    <div class="modal fade" id="modal-addEdit"></div>
    <!--modal close-->
    <!--modal open-->
     <div class="modal fade" id="modal-export"></div>
    <!--modal close-->
</section>
<!-- /.content -->             
@endsection