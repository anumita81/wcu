@extends('Administrator.layouts.master')
@section('content')

<!-- Main content -->
<section class="content"> 
    <div class="row m-b-15">
        <div class="col-lg-8 col-md-8 col-sm-8">
            {{ Form::open(array('url' => 'administrator/city/index', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchData['searchDisplay']=='10'?'selected':''}} value="10">10</option>
                    <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                    <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                    <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                    <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                </select>
            </div>
            <label class="m-l-15">Country</label>
            <div class="adddrop m-l-15 w-200">
                <select name="searchByCountry" id="searchByCountry" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option value="">All countries</option>
                    @foreach($countryList as $country)
                    <option {{$searchData['searchByCountry']==$country['code']?'selected':''}} value="{{$country['code']}}">{{$country['name']}}</option>
                    @endforeach
                </select>
            </div>
            <label class="m-l-15">State</label>
            <div class="adddrop m-l-15 w-200">
                <select name="searchByState" id="searchByState" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option value="">All states</option>
                    @foreach($stateList as $state)
                    <option {{$searchData['searchByState']==$state['code']?'selected':''}} value="{{$state['code']}}">{{$state['name']}}</option>
                    @endforeach
                </select>
            </div>
            <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
            <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
            {{ Form::close() }}
        </div>
         <div class="col-lg-4 col-md-4 col-sm-4 text-right">
            <div class=" col-lg-8 col-md-7 col-sm-6 col-xs-7 text-right">
            @if($canAdd == 1)<a href="#" class="btn btn-sm btn-warning" onclick="showImport('city/importdata');"><span class="Cicon"><i class="glyphicon glyphicon-import"></i></span> Import</a>@endif
            </div>
            <div class="col-lg-4 col-md-5 col-sm-6 col-xs-5 text-right">    
                @if($canAdd == 1)<a href="javascript:void(0)" class="btn btn-sm btn-info" onclick="showAddEdit(0, {{$page}}, 'city/addedit');"><span class="Cicon"><i class="fa fa-plus"></i></span> Add New City</a>@endif
            </div>
       
          </div>  
    </div>                   
    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-body">
                    <table id="example2"  class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th data-sort="countryName" class="{{$sort['countryName']['current']}} sortby" >Country Name</th>
                                <th>State Name</th>
                                <th data-sort="cityName" class="sorting_asc sortby">City Name</th>
                                <th width="15%">Status</th>
                                <th width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($cityData)
                            @foreach ($cityData as $city)
                            @php 
                            $changeStatusLink = url('administrator/city/changestatus/'.$city->id.'/'.$page); 
                            $deleteLink = url('administrator/city/delete/'.$city->id.'/'.$page); 
                            @endphp
                            <tr>
                                <td>{{$city->countryName}}</td>
                                <td>{{!empty($city->stateName)?$city->stateName:'N/A'}}</td>
                                <td>{{$city->cityName}}</td>
                                <td>
                                @if($canEdit == 1)
                                @if($city->status == 1)
                                <a data-to$uapprlggle="confirmation" href="{{$changeStatusLink. '/0'}}" class="btn btn-success btnActive">Active</a>
                                @else 
                                <a data-toggle="confirmation" href="{{$changeStatusLink. '/1'}}" class="btn btn-danger btnActive">Inactive</a>
                                @endif
                                @endif
                                </td>
                                
                                <td>
                                    @if($canEdit == 1)<a class="text-green edit actionIcons" data-toggle="tooltip" title="Click to Edit" onclick="showAddEdit({{$city->id}}, {{$page}}, 'city/addedit');"><i class="fa fa-fw fa-edit"></i></a>@endif
                                    @if($canDelete == 1)<a class="actionIcons color-theme-2" href="{{$deleteLink}}" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a>@endif
                                </td>
                            </tr>
                            @endforeach
                            @else 
                            <tr>
                                <td colspan="6">No Record Found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $cityData->firstItem() . ' - ' . $cityData->lastItem() . ' of  ' . $cityData->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $cityData->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--modal open-->
    <div class="modal fade" id="modal-addEdit"></div>
    <!--modal close-->
    <div class="modal fade" id="modal-export"></div>
</section>
<!-- /.content -->                    
@endsection