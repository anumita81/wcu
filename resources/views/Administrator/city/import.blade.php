<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Import Data</h4>
        </div>
        {{ Form::open(array('url' => 'administrator/city/saveimport/', 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post', 'files'=> true)) }}
        <div class="modal-body">
        
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="exampleInputFile">Upload File </label>
                    <!--<input id="exampleInputFile" name="logo" required type="file" >-->
                        <div class="input-group">
                        <div class="input-group-btn">
                        <div class="fileUpload btn btn-custom-theme fake-shadow">
                            <span>Browse...</span>
                            <input id="exampleInputFile" name="uploadFile" type="file" class="attachment_upload">
                        </div>
                        </div>
                       
                        </div>
                        
                    </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                 <label>Download Sample</label>
                 <div id="inputImage"><a class="text-green" href="{{asset('public/sampleimports/country-city-masterData.csv')}}"><i class="fa fa-2x fa-download" aria-hidden="true"></i></a></div>
                 </div>
                </div>
                <div id="browselabel" class="col-md-12" style="display:none;"></div>
            </div>
        
        </div>
        <div class="modal-footer">
            <div class="text-right">

                <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
            </div>
        </div>
        {{Form::close()}}
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
<script>
    $(function () {
        $("#addeditFrm").validate();
    });
</script>
<script src="{{ asset('public/administrator/controller-css-js/warehouselocation.js') }}"></script>