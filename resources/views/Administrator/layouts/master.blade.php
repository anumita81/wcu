<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>ADMIN - WCU</title>   
        <!-- Tell the browser to be responsive to screen width -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{ asset('public/global/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('public/global/bower_components/font-awesome/css/font-awesome.min.css') }}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{ asset('public/global/bower_components/Ionicons/css/ionicons.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('public/administrator/css/theme.min.css') }}">
        <!--Skins. Choose a skin from the css/skins
               folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{ asset('public/administrator/css/skins/_all-skins.min.css') }}">
        <!-- Date Picker -->
        <link rel="stylesheet" href="{{ asset('public/global/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{{ asset('public/global/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
        <!-- DataTables -->
        <link rel="stylesheet" href="{{ asset('public/global/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="{{ asset('public/global/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
        <link rel="stylesheet" href="{{ asset('public/administrator/css/intlTelInput.css') }}">
        <link rel="stylesheet" href="{{ asset('public/administrator/css/skins/style.css') }}">
        <link rel="stylesheet" href="{{ asset('public/administrator/css/skins/responsive.css') }}">
        <link rel="stylesheet" href="{{ asset('public/administrator/css/bootstrap-datetimepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('public/global/plugins/iCheck/flat/green.css') }}">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
        <!-- jQuery 3 --> 
        <script src="{{ asset('public/global/bower_components/jquery/dist/jquery.min.js') }}"></script> 

        <script src="{{ asset('public/global/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

        <!-- jQuery UI 1.11.4 --> 
        <script src="{{ asset('public/global/bower_components/jquery-ui/jquery-ui.min.js') }}"></script> 
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip --> 
        <script>
$.widget.bridge('uibutton', $.ui.button);
        </script> 
        <!-- Bootstrap 3.3.7 --> 
        <script src="{{ asset('public/global/bower_components/bootstrap/js/popover.js') }}"></script>
        <script src="{{ asset('public/global/bootstrap-confirmation.js') }}"></script>
        <!-- DataTables --> 
        <script src="{{ asset('public/global/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script> 
        <script src="{{ asset('public/global/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script> 
        <!-- daterangepicker --> 
        <script src="{{ asset('public/global/bower_components/moment/min/moment.min.js') }}"></script> 
        <script src="{{ asset('public/global/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script> 
        <!-- datepicker --> 
        <script src="{{ asset('public/global/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script> 
        <!-- Bootstrap WYSIHTML5 --> 
        <script src="{{ asset('public/global/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
        <!-- Slimscroll --> 
        <script src="{{ asset('public/global/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script> 
        <!-- FastClick --> 
        <script src="{{ asset('public/global/bower_components/fastclick/lib/fastclick.js') }}"></script> 
        <!-- jQuery Validate --> 
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script> 
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.js"></script> 
        <!--App --> 
        <script src="{{ asset('public/administrator/js/theme.min.js') }}"></script> 
        <!--for demo purposes --> 
        <script src="{{ asset('public/global/plugins/iCheck/icheck.min.js') }}"></script>
        <script src = "{{ asset('public/administrator/js/demo.js') }}" ></script>
        <script src = "{{ asset('public/administrator/js/custom.js') }}" ></script>
        <script src = "{{ asset('public/administrator/js/intlTelInput-jquery.js') }}" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <script src="{{ asset('public/global/bower_components/ckeditor/ckeditor.js') }}"></script> 
        <script src="{{ asset('public/administrator/js/bootstrap-datetimepicker.min.js') }}"></script>
    </head>

    <body data-spy="scroll" class="sidebar-mini sidebar-collapse skin-black-light">
        <div class="wrapper">

            <input type="hidden" id="baseUrl" name="baseUrl" value="{{url('administrator/')}}" />
            @include('Administrator.elements.topnav')    


            <!-- Left side column. contains the logo and sidebar -->

            @include('Administrator.elements.leftnav')

            <!-- Left navigation End -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper"> 
                <!-- Content Header (Page header) -->
                @include('Administrator.elements.breadcrumb',array('pageTtile' => isset($pageTitle)?$pageTitle:'', 'contentTop'=>$contentTop))

                @if ($errors->any())
                <div class="m-t-20">
                    <div class="col-md-12">
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Error!</h4>
                            <p>{{ implode('', $errors->all(':message')) }}</p>
                        </div>
                    </div>
                </div>
                @endif

                @if (Session::get('successMessage'))
                <div class="m-t-20">
                    <div class="col-md-12">
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>                            
                            <p>{!! Session::get('successMessage') !!}</p>
                        </div>
                    </div>
                </div>
                @endif

                @if (Session::get('errorMessage')) 
                <div class="m-t-20">
                    <div class="col-md-12">
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Error!</h4>                            
                            <p>{{ Session::get('errorMessage') }}</p>
                        </div>
                    </div>
                </div>
                @endif 

                <!-- /page content -->
                @yield('content')
                <!-- /.content --> 
            </div>

            <!-- footer content -->
            @include('Administrator.elements.footer')
            <!-- /footer content -->


        </div>

    </body>

</html>