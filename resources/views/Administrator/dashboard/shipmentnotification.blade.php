@if($totalNotificationData>0)
<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <span class="label label-warning">{{ $totalNotificationData }}</span> </a>
<ul class="dropdown-menu">
    <li class="header">You have {{ $totalNotificationData }} notification(s)</li>
    <li> 
        <!-- inner menu: contains the actual data -->
        <ul class="menu">
            @if($newOrderData>0)
            <li> <a href="javascript:void(0)" title="{{ $newOrderData }} new order(s) created"> <i class="fa fa-shopping-cart text-green"></i> {{ $newOrderData }} new order(s) created </a> </li>
            @endif
            @if($newShipmentData>0)
            <li> <a href="javascript:void(0)" title="{{ $newShipmentData }} new shipment(s) created"> <i class="fa fa-shopping-cart text-green"></i> {{ $newShipmentData }} new shipment(s) created </a> </li>
            @endif
            @if($newAutoshipmentData>0)
            <li> <a href="javascript:void(0)" title="{{ $newAutoshipmentData }} new auto shipment(s) created"> <i class="fa fa-shopping-cart text-green"></i> {{ $newAutoshipmentData }} new auto shipment(s) created </a> </li>
            @endif
            @if($newProcurementData>0)
            <li> <a href="javascript:void(0)" title="{{ $newProcurementData }} new procurement request(s) submitted"> <i class="fa fa-shopping-cart text-green"></i> {{ $newProcurementData }} new procurement request(s) submitted </a> </li>
            @endif
            @if($newAutopartData>0)
            <li> <a href="javascript:void(0)" title="{{ $newAutopartData }} new autoparts procurement request(s) submitted"> <i class="fa fa-shopping-cart text-green"></i> {{ $newAutopartData }} new autoparts procurement request(s) submitted </a> </li>
            @endif
            @if($newBuycarData>0)
            <li> <a href="javascript:void(0)" title="{{ $newBuycarData }} new buy a car request(s) submitted"> <i class="fa fa-shopping-cart text-green"></i> {{ $newBuycarData }} new buy a car request(s) submitted </a> </li>
            @endif
        </ul>
    </li>

</ul>
@else
<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <span class="label label-warning">0</span> </a>
<ul class="dropdown-menu">
    <li class="header">You have no recent notification</li>
</ul>
@endif
