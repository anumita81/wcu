@if($totalContactData>0)
<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-envelope-o"></i> <span class="label label-success">{{ $totalContactData }}</span> </a>
<ul class="dropdown-menu">
    <li class="header">You have {{ $totalContactData }} contact request(s)</li>
    <li> 
        <!-- inner menu: contains the actual data -->
        <ul class="menu">
            @if($newFileclaimData>0)
            <li> <a href="javascript:void(0)" title="{{ $newFileclaimData }} new claim request(s)"> {{ $newFileclaimData }} new claim request(s) submitted</a> </li>
            @endif
            @if($newShippingquoteData>0)
            <li> <a href="javascript:void(0)" title="{{ $newShippingquoteData }} new shipping quote request(s) submitted"> {{ $newShippingquoteData }} new shipping quote request(s) submitted </a> </li>
            @endif
        </ul>
    </li>
</ul>
@else
<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-envelope-o"></i> <span class="label label-success">0</span> </a>
<ul class="dropdown-menu">
    <li class="header">You have no recent notification</li>
</ul>
@endif