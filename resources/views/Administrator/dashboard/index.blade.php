@extends ('Administrator.layouts.master')

@section('content')

<!-- Main content -->
<section class="content gapdashboard">

    <div class="row"> 
        <!-- Left col -->
        <section class="col-lg-7 col-md-12"> 
            <!-- Custom tabs (Charts with tabs)-->
            <h3>Overview</h3>
            <div class="nav-tabs-custom"> 
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs Overview">
                    <li class="active overview-tab"><a href="#revenue-chart" data-toggle="tab" onclick="changeOverview('revenue')">Revenue <!--<span class="dlr">$2,987</span> <span class="upGrf">23%</span>--></a> </li>                   
                    <li style="margin-top: -20px;" class="pull-right p-r-10">
                        <div id="overview_range" class="btn btn-default withdateRangePicker reportrange">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <b class="caret"></b>
                        </div>
                    </li>
                    <li style="margin-top: -20px;" class="pull-right">
                        <div class="dropdown myDropDn">
                            <button class="btn btn-default dropdown-toggle" id="filterBtn" type="button" data-toggle="dropdown">Select<span class="caret skybg m-l-10"></span></button>
                            <input type="hidden" id="selectedfilterval" value="1">
                            <ul class="dropdown-menu country-filter-overview" id="changeFilter">
                                
                            </ul>
                        </div>
                    </li>

                </ul>
                <div class="tab-content no-padding"> 
                    <!-- Morris chart - Sales -->
                    <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 390px;"><canvas id="revenue-chart-data"></canvas></div>
                    <div class="chart tab-pane" id="shipment-chart" style="position: relative; height: 390px;"><canvas id="shipment-chart-data"></canvas></div>
                    <div class="chart tab-pane" id="customer-chart" style="position: relative; height: 390px;"><canvas id="customer-chart-data"></canvas></div>
                </div>
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
        <!-- /.Left col --> 
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 col-md-12"> 
            <!-- Map box -->
            <h3></h3>
            <div class="box box-solid no-border shadowBoxStyl">
                <div class="titleSec clearfix">
                    <h4>Countries</h4>
                    <div class="droupRt1 pull-right">
                        <div id="customers_range" class="btn btn-default withdateRangePicker reportrange">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <b class="caret"></b>
                        </div>
                    </div>

                </div>
                <div><canvas id="countrywise-customer" width="800px" height="450px"></canvas></div>
            </div>
            <h3>Current Active Users</h3>
            <div class="row currentActive">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <a href="{{url('/administrator/users/')}}" target="_blank"> <div class="info-box drkGray"> <span class="info-box-icon desktop"><img src="{{ asset('public/administrator/img/deskTopIcon.png') }}" /></span>
                       <div class="info-box-content"> <span class="info-box-number f-S-27"><!--<small>%</small>--></span> <span class="info-box-text">Desktop Users</span> </div>
                        </div></a>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <a href="{{url('/administrator/users/')}}" target="_blank"><div class="info-box skyBlue"> <span class="info-box-icon mobile"><img src="{{ asset('public/administrator/img/mobileIcon.png') }}" /></span>
                      <div class="info-box-content"> <span class="info-box-number f-S-27"><!--<small>%</small>--></span> <span class="info-box-text">Mobile Users</span> </div>
                            <!-- /.info-box-content --> 
                        </div></a>
                    <!-- /.info-box --> 
                </div>
            </div>
        </section>
        <!-- right col --> 
    </div>
    <!--Block 01-->
    <div class="row">
        <section class="col-lg-12 shipmentNav"> 
            <!-- Custom tabs (Charts with tabs)-->
            <h3>Shipments</h3>
            <div class="nav-tabs-custom"> 
                <input type="hidden" name="showShipmentType" id="showShipmentType" />
                <input type="hidden" name="tblchoice" id="tblchoice" />
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-left procurementShipmentNav noBg">
                    <li class="active"><a href="#shopformeShip" data-toggle="tab" onclick="showShipments('shopforme')">Shop For Me Shipment</a> </li>
                    <li><a href="#autopartsShip" data-toggle="tab" onclick="showShipments('autopart')">Auto Parts Shipment</a></li>
                    <li><a href="#autoShipment" data-toggle="tab" onclick="showProcurement('buycarforme')">Auto Shipment(Buy a car)</a></li>
                    <li><a href="#procurementShipment" data-toggle="tab" onclick="showProcurement('shopforme')">Procurement(Shop For Me)</a></li>
                    <li><a href="#processedShipment" data-toggle="tab" onclick="showProcurement('autopart')">Procurement(Auto Part) </a></li>
                    <!--                    <li><a href="#processedShipmentPackingReport" data-toggle="tab">Processed Shipment Packing</a></li>-->

                    <li class="pull-right">
                        <div id="shipment_status_range" class="pull-right btn btn-default withdateRangePicker reportrange">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <b class="caret"></b>
                        </div>
                    </li>
                </ul>
                <div class="tab-content no-padding"> 
                    <!-- Morris chart - Sales -->
                    <div class="chart tab-pane active" id="shopformeShip" style="position: relative;">
                        <ul class="airShip">

                        </ul>
                    </div>
                    <div class="chart tab-pane" id="autopartsShip" style="position: relative;">
                        <ul class="airShip">

                        </ul>
                    </div>
                    <div class="chart tab-pane" id="autoShipment" style="position: relative;">
                        <ul class="airShip">

                        </ul>
                    </div>
                    <div class="chart tab-pane" id="procurementShipment" style="position: relative;">
                        <ul class="airShip">

                        </ul>
                    </div>

                    <div class="chart tab-pane" id="processedShipment" style="position: relative;">
                        <ul class="airShip">

                        </ul>
                    </div>

                </div>
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>

    <!--    <div class="row">
            <section class="col-lg-12 shipmentNav"> 
                 Custom tabs (Charts with tabs)
                <h3>Visitors</h3>
                <div class="nav-tabs-custom"> 
                    <div class="tab-content no-padding"> 
                         Morris chart - Sales 
                        <div class="chart tab-pane active" id="AirShip" style="position: relative;">
                            <ul class="airShip">
                                <li>
                                    <div class="airShipBox"> <span class="borairCir"><span class="socialCir"> <img src="{{ asset('public/administrator/img/if_Facebook_194929.png') }}" width="58" height="58"> </span></span>
                                        <p class="num">1000</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="airShipBox"> <span class="borairCir"> <span class="socialCir"><img src="{{ asset('public/administrator/img/if_twiter-social-network-brand-logo_1820440.png') }}" width="58" height="58"></span></span>
                                        <p class="num">569</p>
    
                                    </div></li>
                                <li><div class="airShipBox"> <span class="borairCir"><span class="socialCir"> <img src="{{ asset('public/administrator/img/if_40-google-plus_843777.png') }}" width="58" height="58"> </span></span>
                                        <p class="num">256</p>
                                    </div></li>
                                <li><div class="airShipBox"> <span class="borairCir"><span class="socialCir"><img src="{{ asset('public/administrator/img/if_LinkedIn_194920.png') }}" width="58" height="58"></span></span>
                                        <p class="num">352</p>
    
                                    </div></li>
                                <li><div class="airShipBox "> <span class="borairCir"><span class="socialCir"> <img src="{{ asset('public/administrator/img/if_38-instagram_1161953.png') }}" width="58" height="58"></span></span>
                                        <p class="num">689</p>
                                    </div></li>
                            </ul>
                        </div>
                    </div>
                </div>
                 /.nav-tabs-custom  
            </section>
        </div>-->

    <!--Block 02-->
    <div class="row">
        <section class="col-lg-12"> 
            <!-- Custom tabs (Charts with tabs)-->
            <h3>Order by Destination Country</h3>
            <div class="box box-solid no-border shadowBoxStyl">
                <div class="droupRt1 pull-right">
                    <div id="order_destinationcountry_range" class="btn btn-default withdateRangePicker reportrange">
                        <i class="fa fa-calendar"></i>&nbsp;
                        <span></span> <b class="caret"></b>
                    </div>
                </div>
                <div class="chart" id="order-destination-country">
                    <canvas id="order-by-country" style="height:200px"></canvas>
                </div>
            </div>
        </section>
    </div>
    <!--Block 03-->
    <div class="row">
        <section class="col-md-4 col-sm-12"> 
            <!-- Custom tabs (Charts with tabs)-->
            <h3>Feedback Statistics</h3>
             <a href="{{url('/administrator/contactus?type=11')}}">
                <div class="info-box blkTxt statistics"> <span class="info-box-icon bgDrkRed"><img src="{{ asset('public/administrator/img/issuesIcon.png') }}" /></span>
                    <div class="info-box-content"> <span class="info-box-number"><small>File a Claim Requests</small></span> </div>
                </div>
            </a>
            <a href="{{url('/administrator/contactus?type=12')}}">
                <div class="info-box blkTxt statistics"> <span class="info-box-icon bgDrkYellow"><img src="{{ asset('public/administrator/img/enquiry.png') }}" /></span>
                    <div class="info-box-content"> <span class="info-box-number"><small>Contact Us Requests</small></span></div>
                </div>
            </a>
        </section>
        <section class="col-md-8  col-sm-12"> 
            <!-- Custom tabs (Charts with tabs)-->
            <h3>Service Overview</h3>
            <div class="nav-tabs-custom"> 
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-left service-overview noBg">
                    <li class="service-overview-tab active"><a class="active" href="#Order" data-toggle="tab">Order</a> </li>
                    <li class="service-overview-tab"><a href="#Shipment" data-toggle="tab">Shipment</a></li>
                    <li class="service-overview-tab"><a href="#Revenue" data-toggle="tab">Revenue</a></li>
                    <li class="pull-right">
                        <div id="service_overview_range" class="pull-right btn btn-default withdateRangePicker reportrange">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <b class="caret"></b>
                        </div>
                    </li>
                </ul>
                <div class="tab-content no-padding service-overview-div"> 
                    <!-- Morris chart - Sales -->
                    <div class="chart active" id="Order">
                        <canvas id="Order-overview" height="100px"></canvas>
                    </div>
                    <div class="chart" id="Shipment">
                        <canvas id="Shipment-overview" height="100px"></canvas>
                    </div>
                    <div class="chart" id="Revenue">
                        <canvas id="Revenue-overview" height="100px"></canvas>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
<!-- /.content-wrapper -->
<!-- Sparkline --> 
<script src="{{ asset('public/global/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script> 

<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('public/global/bower_components/jvectormap/jquery-jvectormap.css') }}">
<!-- jvectormap --> 
<script src="{{ asset('public/global/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script> 
<script src="{{ asset('public/global/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script> 
<!-- jQuery Knob Chart --> 
<script src="{{ asset('public/global/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script> 
<!-- chart js -->
<script src="{{ asset('public/administrator/js/chartjs/Chart.bundle.js') }}"></script>
<script src="{{ asset('public/administrator/js/chartjs/utils.js') }}"></script>

<script src="{{ asset('public/administrator/controller-css-js/dashboardGraph.js') }}"></script>
@endsection



