<script src = "{{ asset('public/administrator/controller-css-js/city.js') }}" ></script>

<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$title}}</h4>
        </div>
        <div class="modal-body"> 
            {{ Form::open(array('url' => 'administrator/studentclass/save/'.$id.'/'.$page, 'name' => 'addeditFrm',  'id' => 'addeditFrm', 'method' => 'post')) }}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Class <span class="text-red">*</span></label>
                        <select name="className" id="className" class="customSelect" required>
                            <option value="">Select</option>
                            <option value="10"{{(!empty($classData->className) && ($classData->className=='10'))?"selected":""}} >10</option>
                            <option value="12" {{(!empty($classData->subjectforClass) && ($classData->className=='12'))?"selected":""}}>12</option>
                            <option value="o" {{(!empty($classData->subjectforClass) && ($classData->className=='o'))?"selected":""}}>others</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Class Fees <span class="text-red">*</span></label>
                        <input id="classFees" name="classFees" class="form-control" required value="{{!empty($classData->classFees)?$classData->classFees:''}}" placeholder="Enter ..." type="text">
                    </div>
                </div>
            </div>
        
            <div class="modal-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->