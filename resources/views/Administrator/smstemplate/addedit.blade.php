@extends ('Administrator.layouts.master')

@section('content')

<section class="content">

    <div class="row"> 
        <section class="col-lg-8 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <!--<div class="box-header">
                  <h3 class="box-title">Hover Data Table</h3>
                </div>-->
                <!-- /.box-header -->

                {{ Form::open(array('url' => 'administrator/smstemplate/save/' . $id . '/' . $page, 'name' => 'frmsearch', 'id' => 'addEditFrm', 'method' => 'post', 'novalidate' => 'novalidate')) }}
                <div class="box-body">
                    <div style="{{!empty($id)?'display:none':''}}" class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Template Type</label>
                                <select name="templateType" id="templateType" class="customSelect form-control">
                                    <option value="G">General</option>
                                    <option value="O">Order Status</option>
                                    <option value="S">Order Status (single)</option>
                                    <option value="W">Shipment (notify)</option>
                                    <option value="T">Storage Notification (single)</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    @if(!empty($id))
                    <input type="hidden" name="templateType" id="templateType" value="{{$content['templateType']}}" />
                    @endif
                    <div style="{{!empty($content['templateKey']) && $content['templateKey']!='G'?'display:none':''}}" class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Template Key</label>
                                <select name="templateKey" id="templateKey" class="customSelect form-control">
                                    <option value="">Select Template Key</option>
                                    @foreach ($templateKey as $template)
                                    <option value="{{ $template['id'] }}" {{!empty($content['templateKey'])? ($content['templateKey'] ==$template['id'] ? 'selected' :' '): ''}}>{{ $template['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="row m-t-15">  
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Template Description</label>
                                <textarea class="form-control" name="templateBody" rows="10" cols="80" required="" style="resize:none;">{{ Input::old('templateBody', !empty($content['templateBody']) ? $content['templateBody'] : '') }}</textarea>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- /.box-body -->


                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Save</button>
                </div>
                {{ Form::close() }} 
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
</section>


<script src="{{ asset('public/global/bower_components/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('public/global/vendors/validator/validator.js')}}"></script>


@endsection

<script>
$(function () {
    $("#addeditFrm").validate();
});
</script>