@extends('Administrator.layouts.master')
@section('content')

<section class="content">
    <div class="row m-b-15">
        {{ Form::open(array('url' => 'administrator/smstemplate/index', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
        <div class="col-md-12">
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchData['searchDisplay']=='10'?'selected':''}} value="10">10</option>
                    <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                    <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                    <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                    <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                </select>
                <input type="hidden" name="field" id="field" />
                <input type="hidden" name="type" id="type" />
            </div>
            @if($canAdd == 1)<a href="{{url('administrator/smstemplate/addedit')}}" class="btn btn-info pull-right btn-sm"><span class="Cicon"><i class="fa fa-plus"></i></span> Add SMS Template</a>@endif
            <label class="m-l-15">Type</label>
            <div class="adddrop m-l-15 w-200">
                <select name="searchByType" id="searchByType" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchData['searchByType']=='G'?'selected':''}} value="G">General</option>
                    <option {{$searchData['searchByType']=='O'?'selected':''}} value="O">Order Status</option>
                    <option {{$searchData['searchByType']=='S'?'selected':''}} value="S">Order Status (single)</option>
                    <option {{$searchData['searchByType']=='W'?'selected':''}} value="W">Shipment (notify)</option>
                    <option {{$searchData['searchByType']=='T'?'selected':''}} value="T">Storage Notification (single)</option>
                </select>
            </div>
        </div>
        {{ Form::close() }} 
    </div>

    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <div class="box-body">                                   
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th data-sort="createdOn" class="{{$sort['createdOn']['current']}} sortby">Created On</th>
                                <th data-sort="templateKey" class="{{$sort['templateKey']['current']}} sortby">Template name</th>
                                <th width="15%">Message</th>
                                <th width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($smsData)
                            @foreach ($smsData as $smstemplate) 
                            <tr>
                                <td>{{ $smstemplate->createdOn }}</td>
                                <td>{{ !empty($templateKeysList[$smstemplate->templateKey]) ? $templateKeysList[$smstemplate->templateKey] : $smstemplate->templateKey }}</td>
                                <td>{{ strip_tags($smstemplate->templateBody) }}</td>                                                
                                <td>
                                    @if($canEdit == 1)<a class="actionIcons text-green edit" href="{{url('administrator/smstemplate/addedit/'.$smstemplate->id.'/'.$page)}}"><i data-toggle="tooltip" title="Click to Edit" class="fa fa-fw fa-edit"></i></a>@endif
                                    <!-- <a class=" actionIcons text-danger" data-toggle="modal" data-target="#modal-default"><i class="fa fa-fw fa-trash"></i></a> -->
                                </td>
                            </tr> 
                            @endforeach 
                            @else 
                            <tr><td colspan="4">No record present</td></tr>
                            @endif
                        </tbody>

                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $smsData->firstItem() . ' - ' . $smsData->lastItem() . ' of  ' . $smsData->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $smsData->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->

</section>

@endsection