@extends('Administrator.layouts.master')
@section('content')
<script src="{{ asset('public/administrator/controller-css-js/generalsettings.js')}}"></script>

   <!-- Main content -->
                <section class="content">

                    <div class="row"> 
                        <section class="col-lg-8 connectedSortable"> 
                            <!-- Custom tabs (Charts with tabs)-->                           
                            {{ Form::open(array('url' => 'administrator/socialmedia/save', 'name' => 'addEditFrm', 'id' => 'addEditFrm', 'method' => 'post', 'class' => 'form-horizontal')) }} 
                            <div class="box">
                                <div class="box-header">
                                  <h3 class="box-title">Connect With Us</h3>
                                </div>
                                <!-- /.box-header -->                                
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-12 socialIcons">
                                            @foreach ($settings as $data)
                                                <div class="form-group">
                                                    <input type="hidden" name="{{$data['media']}}[]" value="{{$data['media']}}">
                                                    <label class="control-label col-sm-4" title="Facebook"><i class="fa {{$data['faclass']}}"></i></label>
                                                    <div class="col-sm-4 withCheck">
                                                        <input id="volumeCharge" name="{{$data['media']}}[]" type="text" class="form-control"  placeholder="Enter ..." value="{{$data['mediaLink']}}">
                                                    </div>
                                                </div>
                                                
                                            @endforeach    
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer text-right">
                                        <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                                    </div>
                                    <!-- /.box-body -->
                            </div>
                            <!-- /.nav-tabs-custom -->
                            
                          
                            {{Form:: close()}}
                        </section>
                    </div>
                    <!--Block 01-->
                </section>
                <!-- /.content --> 
                <script>
    $(function () {
        $("#addeditFrm").validate();
    });
</script>
 
@endsection