@extends('Administrator.layouts.master')
@section('content')

<script src = "{{ asset('public/administrator/controller-css-js/user.js') }}" ></script>

<!-- Main content -->
<section class="content">

    <div class="row"> 
        <section class="col-lg-8 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <!--<div class="box-header">
                  <h3 class="box-title">Hover Data Table</h3>
                </div>-->
                <!-- /.box-header -->
                {{ Form::open(array('url' => 'administrator/usergroups/save/'.$id.'/'.$page, 'name' => 'addeditFrm', 'id' => 'addeditFrm', 'method' => 'post')) }}
                @php
                if($groupDetails->status == 0)
                {
                  $status = "Requested";
                }
                else if($groupDetails->status == 1)
                {
                  $status = "Active"; 
                }
                else{
                  $status = "Inactive";
                }
                @endphp
                <div class="box-body">
                    <div class="row m-t-15">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Group Name</label>
                                <input id="groupName" name="groupName" class="form-control" readonly="readonly" value="{{$groupDetails->groupName}}"  placeholder="Group Name" type="text">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Expected Members <span class="text-red">*</span></label>
                                <input id="expectedMembers" name="expectedMembers" class="form-control" required="" value="{{$groupDetails->expectedMembers}}"  placeholder="Expected Members" type="text">
                            </div>
                        </div>
                       
                    </div>
                    <div class="row m-t-15">
                         <div class="col-md-6">
                            <div class="form-group">
                                <label>Ready To Join (within 1st Week) </label>
                                <input id="readyTojoinMembers" name="readyTojoinMembers" class="form-control" readonly="readonly" value="{{$groupDetails->readyTojoinMembers}}"  placeholder="Ready To Join" type="text">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Total Group Members </label>
                                <input  id="totalGroupMembers" name="totalGroupMembers" class="form-control" readonly="readonly" value="{{$groupDetails->totalGroupMembers}}" placeholder="Total Group Members" type="text">
                            </div>
                        </div>
                      
                    </div>
                    
                    <div class="row m-t-15">
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>Reason for approval </label>
                                <textarea readonly="readonly" class="form-control" rows="5" cols="50">{{$groupDetails->approveReason}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Status</label>
                                <input  id="status" name="status" class="form-control" value="{{$status}}" placeholder="Group Status" readonly="readonly" type="text">
                            </div>
                        </div>
                       
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>

               


                {{ Form::close() }}
              

            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
    <!--modal open-->
    <div class="modal fade" id="modal-addEdit"></div>
    <!--modal close-->
</section>
<!-- /.content --> 
@endsection