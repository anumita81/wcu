<!--modal open-->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Status</h4>
        </div>
        <div class="modal-body">
            {{ Form::open(array('url' => "administrator/usergroups/changestatus/$id/$page", 'name' => 'changestatusFrm', 'id' => 'changestatusFrm', 'method' => 'post')) }}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <select name="groupStatus" class="form-control customSelect2 input-lg" id="orderStatus" required="">
                            <option value="1">Approve</option>
                            <option value="2">Decline</option>
                        </select>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Submit</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->



<script>
    $(function () {
        $("#changestatusFrm").validate();
    });
</script>