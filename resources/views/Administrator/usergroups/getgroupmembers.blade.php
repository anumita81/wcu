<!--modal open-->
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>            
            <h4 class="modal-title">Group Member details</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="box">
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Group Member</th>
                                    <th>Unit Number</th>
                                    <th>Member Role</th>
                                    <th>Status</th>
                                    <th>Invitation Sent on</th>
                                    <th>Joined On</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($groupMembers))
                                    @foreach($groupMembers as $eachMember)
                                    <tr>
                                        <td>{{ $eachMember->firstName.' '.$eachMember->lastName }}</td>
                                        <td>{{ $eachMember->unit }}</td>
                                        <td>
                                            @if($eachMember->memberType == '1')
                                                Group Member
                                            @else
                                                Co-ordinator
                                            @endif
                                        </td>
                                        <td>
                                            @if($eachMember->status == '1')
                                                Accepted
                                            @elseif($eachMember->status == '0')
                                                Pending
                                            @else
                                                Declined
                                            @endif
                                        </td>
                                        <td>{{ $eachMember->invitationSentOn }}</td>
                                        <td>{{ $eachMember->joinedOn }}</td>
                                    </tr>
                                    @endforeach
                                @else
                                <tr><th>No record found</th></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
   
        <div class="modal-footer">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <button type="button" class="btn btn-info" name="submit" data-dismiss="modal" aria-label="Close">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- thumbnail img-preview m-auto mb-3  -->