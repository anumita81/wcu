@extends('Administrator.layouts.master')
@section('content')

<section class="content">
    <div class="row m-b-15">
        {{ Form::open(array('url' => 'administrator/managedhltradeterms', 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'post')) }}
        <div class="col-lg-3 col-md-3"> 
            <label>Entries per page</label>
            <div class="adddrop m-l-15 w-100">
                <select name="searchDisplay" id="searchDisplay" class="form-control highLight" onchange="$('#frmsearch').submit();">
                    <option {{$searchData['searchDisplay']=='10'?'selected':''}} value="10">10</option>
                    <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                    <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                    <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                    <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                </select>
            </div>
            <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
            <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
        </div>
        {{ Form::close() }}
    </div>
    <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->

            <div class="box">
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Group Name</th>
                                <th>Expected Members</th>
                                <th>Ready To Join <br>within 1st Week</th>
                                <th>Group Members</th>
                                <th>Requested On</th>
                                <th>Requested By</th>
                                <th>Status</th>
                                <th class="text-center" width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($userGroupData)
                            @foreach ($userGroupData as $eachGroupdata)
                            @php
                            $deleteLink = url('administrator/managedhltradeterms/delete/' . $eachGroupdata->id . '/' . $page);
                            
                            if($eachGroupdata->status == '0') :
                            $statusTxt = 'Pending';
                            $statusClass = 'btn-info';
                            elseif($eachGroupdata->status == '1') :
                            $statusTxt = 'Approved';
                            $statusClass = 'btn-success';
                            elseif($eachGroupdata->status == '2') :
                            $statusTxt = 'Declined';
                            $statusClass = 'btn-danger';
                            endif;
                            
                            @endphp
                            <tr>
                                <td>{{$eachGroupdata->groupName}}</td>
                                <td>{{$eachGroupdata->expectedMembers}}</td>
                                <td>{{$eachGroupdata->readyTojoinMembers}}</td>
                                <td>
                                    @if($eachGroupdata->totalGroupMembers > 0)
                                        <a onclick="showAddEdit({{$eachGroupdata->id}}, {{$page}}, 'usergroups/getgroupmembers');" class="btn btn-xs">{{$eachGroupdata->totalGroupMembers}}</a>
                                    @else
                                        {{$eachGroupdata->totalGroupMembers}}
                                    @endif
                                </td>
                                <td>{{$eachGroupdata->requestedOn}}</td>
                                <td>{{$eachGroupdata->firstName." ".$eachGroupdata->lastName}}</td>
                                <td>
                                    <a onclick="showAddEdit({{$eachGroupdata->id}}, {{$page}}, 'usergroups/changestatus');" class="btn btn-xs {{$statusClass}} shipmentStatusTxt"><span data-toggle="tooltip" title="" data-original-title="Click to Change Status">{{$statusTxt}}</span></a>
                                </td>
                                <td class="text-center">
                                    
                                    @if($canDelete == 1)
                                    <a class="actionIcons color-theme-2" href="{{$deleteLink}}" data-toggle="confirmation"><i data-toggle="tooltip" title="Click to Delete" class="fa fa-fw fa-trash"></i></a>
                                    @endif                                    
                                    @if($canEdit == 1)
                                        @if($eachGroupdata->status == '1')
                                            <a href="javascript:void(0)" class="text-info actionIcons" title="Assign Account Manager" onclick="showAddEdit({{$eachGroupdata->id}}, {{$page}}, 'usergroups/assignrepresentative');"><i class="fa fa-2 fa-user-circle-o"></i></a>
                                        @endif
                                        <a href="javascript:void(0)" class="text-green edit actionIcons" title="Edit Group Details" onclick="showAddEdit({{$eachGroupdata->id}}, {{$page}}, 'usergroups/editgroupdetails');"><i class="fa fa-edit"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @else 
                            <tr>
                                <td colspan="3">No Record Found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="row mt-20">
                        <div class="col-md-6 col-sm-6 col-xs-6 for12">
                            <p class="results">Showing {{ $userGroupData->firstItem() . ' - ' . $userGroupData->lastItem() . ' of  ' . $userGroupData->total() }}</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                {{ $userGroupData->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->


    <!--modal close-->
    <!--modal open-->
    <div class="modal fade" id="modal-addEdit">
    </div>
    <!--modal close-->
    <!--modal open-->
    <div class="modal fade" id="modal-edit">
    </div>
    <!--modal close-->
</section>       
<!-- /.content -->             
@endsection