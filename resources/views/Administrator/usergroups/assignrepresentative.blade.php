<!--modal open-->
<div class="modal-dialog modal-lg">
    <script src = "{{ asset('public/administrator/controller-css-js/user.js') }}" ></script>
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>            
            <h4 class="modal-title">{{$title}}</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                
                    @php $i=1;
                    foreach($repuser as $user){
                    @endphp
                    <div class="col-md-6 mb-3">
                        <div class="d-flex align-items-center bg-light">
                            @if(!empty($user->fileName))                                
                                <img id="loaded" src="{{asset('/public/uploads/admin/thumb/'.$user->fileName)}}" alt="" class="avatar mr-3">  
                            @else
                                <img id="storeIcon" class="avatar mr-3" src="{{ asset('public/administrator/img/default-no-img.jpg')}}" title="Preview Logo">
                            @endif

                            <div class="m-3">
                                <span class="d-block h6 mb-2 mt-0">{{$user->firstName}} {{$user->lastName}}</span>
                                <span class="d-block">{{$user->email}}</span>
                                <span class="d-block mb-2">{{$user->contactno}}</span>
                                @if($representativeId == $user->id)                                
                                <a id="assignBtn{{$groupId}}" href="javascript:void(0)" class="btn btn-success btn-sm" onclick="assignGroupRepresentative({{$groupId}}, {{$user->id}})">Reassign</a> 
                                @else
                                <a id="reassignBtn{{$groupId}}" href="javascript:void(0)" class="btn btn-success btn-sm" onclick="assignGroupRepresentative({{$groupId}}, {{$user->id}})">Assign</a>
                                @endif
                            </div>
                        </div>
                        @if($representativeId == $user->id)
                        <span class="item-check"><i class="fa fa-check" aria-hidden="true"></i></span>
                        @endif
                    </div>
                    @php
                    $i++; 
                    }
                    @endphp
                
            </div>
        </div>
   
        <div class="modal-footer">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <button type="button" class="btn btn-info" name="submit" data-dismiss="modal" aria-label="Close">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- thumbnail img-preview m-auto mb-3  -->