<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<title>Untitled Document</title>
<link href="{{ asset('/public/global/vendors/css/skins/style.css') }}" rel="stylesheet">
<style type="text/css">
body.fourzerofour{ background:#f2f6f9;}
.numb{ transform:translateY(-50%); -webkit-transform: translateY(-50%); -ms-transform: translateY(-50%); top:50%; position:absolute; left:0; right:0; margin:0 auto; width:auto; text-align:center;}
.numb span{font-size:210px; line-height:210px; color:#5bc5d2; display:block; text-align:center; text-shadow: 1px 0 1px #171717;}
.numb p{ padding:0; margin:0; font-size:32px; line-height:38px;}
</style>
</head>
<body class="fourzerofour">
<div class="numb">
<span>404</span>
<p>Opps! Could not Find this!</p>
</div>
</body>
</html>
