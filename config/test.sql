---- Migrate customer data ------------
INSERT INTO `stmd_users`(`id`, `uniqueId`, `unit`, `unitNumber`, `title`, `firstName`, `lastName`, `email`, `password`, `isdCode`, `contactNumber`, 
`cityId`, `stateId`, `countryId`, `company`, `dateOfBirth`, `referrer`, `status`, `virtualTourVisited`, `setDeliveryAddress`, `setShipmentSettings`, 
`aggreedToTerms`, `registeredBy`, `deleted`,createdBy) 
select id,'','','0',title,firstname,lastname,email,encpassword,'','','0','0','0',company,'1970-01-01','','1','1','1','0','0','website','0','1' from stmd_customers where usertype='C';

----- Migrate addressbook id ------------------
INSERT INTO `stmd_address_book` ( `userId`, `isDefaultShipping`, `isDefaultBilling`, `title`, `firstName`, `lastName`, `email`,`address`,`cityId`,`stateId`,`countryId`,`zipcode`,`phone`,`isdCode`)
SELECT u.id, IF(xab.default_s='Y','1','0'), IF(xab.default_b='Y','1','0'), xab.title, xab.firstname, xab.lastname, u.email, xab.address, ku.id, s.id, 
co.id, xab.zipcode, xab.phone, co.isdCode FROM xcart_address_book AS xab INNER JOIN stmd_users u ON u.id = xab.userid 
INNER JOIN stmd_countries co ON co.code = xab.country 
INNER JOIN stmd_states s ON s.code = xab.state AND s.countryCode = co.code 
INNER JOIN stmd_cities ku ON ku.name=xab.city GROUP BY xab.id ORDER BY xab.id

------ In case of user id changed ---------------

update `stmd_users` set id=id+10 WHERE `id` > 132180 ORDER BY `id` desc;
update `stmd_address_book` set `userId`=`userId`+10 WHERE `userId` > 132180 ORDER BY `id` desc;

