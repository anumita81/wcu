$(function () {

	    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });
        $('.datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });


    $('.addressBook').on('ifChecked', function (event) {
        $('#addressBook').val($(this).val());
    });

    $('#searchByDate').daterangepicker();

    //Custom radio button show daterange
    $('.customRange').on('ifChecked', function () {
        document.getElementById('customDaterange').style.visibility = 'visible';
    });
    $('.customRange').on('ifUnchecked', function () {
        document.getElementById('customDaterange').style.visibility = 'hidden';
    });


    var baseUrl = $('#baseUrl').val();

    $('#toCountry').on('change', function(){


    /* Get State List For Search */
    if ($('#toCountry').val() != '') {
        var countryId = $('#toCountry').val();
        var stateId = $('#toState').attr('data-value');

        $('#toState').find('option:gt(0)').remove();
        if (countryId != '') {
            var url = baseUrl + "/fillnship/getstatelist/" + countryId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    if (stateId == entry.id)
                        $('#toState').append($('<option selected="selected">Destination State</option>').attr('value', entry.id).text(entry.name));
                    else
                        $('#toState').append($('<option>Destination State</option>').attr('value', entry.id).text(entry.name));
                })
            });

        }
    }

     });

    /* Get City List For Search */
    $('#toState').on('change', function(){
    if ($('#toState').val() != '') {
        var stateId = $('#toState').val();
        var cityId = $('#toCity').attr('data-value');

        $('#toCity').find('option:gt(0)').remove();

        if (stateId != '') {
            var url = baseUrl + "/fillnship/getcitylist/" + stateId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    if (cityId == entry.id)
                        $('#toCity').append($('<option selected="selected">Destination City</option>').attr('value', entry.id).text(entry.name));
                    else
                        $('#toCity').append($('<option>Destination City</option>').attr('value', entry.id).text(entry.name));
                })


            });

        }
    }

    });


    $('#shipmentMessage').on('change', function(){

    	$('#notify-customer').val($('#shipmentMessage').val());

    });

    $("body").on("change", ".packagecategory", function (event) {

    var categoryId = $(this).val();
    var category = $(this);

    // Populate dropdown with list of subcategories
    $(this).parent().next().find('.packagesubcategory').find('option:gt(0)').remove();
    $.ajax({
        url: baseUrl + "/fillnship/getsubcategorylist/" + categoryId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $.each(response, function (key, entry) {
                $(category).parent().next().find('.packagesubcategory').append($('<option></option>').attr('value', entry.id).text(entry.category));
            });
        }
    });

    $(this).parent().next().find('.packageproduct').find('option:gt(0)').remove();
});

$("body").on("change", ".packagesubcategory", function (event) {

    var categoryId = $(this).val();
    var category = $(this);
     
    // Populate dropdown with list of subcategories
    $('.packageproduct').find('option:gt(0)').remove();
    $.ajax({
        url: baseUrl + "/fillnship/getproductlist/" + categoryId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $.each(response, function (key, entry) {
                $('.packageproduct').append($('<option></option>').attr('value', entry.id).text(entry.productName));
            });
        }
    });
});

$('.addButt').click(function () {
        var table = $(this).parent().prev();
        table.find('tbody').append('<tr>' + $('#blank_items').html() + '</tr>');
    });

$("body").on('blur', '.productval', function () {

    var value = $(this).val();
    var qty = $(this).parent().parent().find('.quantity').val();
    if ($.isNumeric(qty)) {
        var producttotal = parseFloat(qty * value);
        $('.product-total').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});

$("body").on("click", ".addPackage", function (event) {
    $(this).attr('disabled', true);
    $(this).closest("tr:has(input)").each(function () {
        var inputArray = [];
        var err = false;

        $('select', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
            } else
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
        });

        $('input', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
            } else
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
        });

        var fillshipId = $('#fillshipId').val();

        if (err != true) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: baseUrl + "/fillnship/adddeliveryitem",
                type: 'POST',
                data: {data: inputArray},
                success: function (response) {
                    /* Check Procurement Status */
                    $("#packageList").load(baseUrl + "/fillnship/getpackagedetails/" + fillshipId);
                    $("#infoBoxDetail").load(baseUrl + "/fillnship/getshipmentdetails/" + fillshipId);
                }
            });
        }
    });


});

$("body").on("click", ".removePackage", function (event) {
    var id = $(this).attr('data-id');
    var fillshipId = $('#fillshipId').val();

    $.ajax({
        url: baseUrl + "/fillnship/deletedeliveryitem/" + id,
        type: 'GET',
        success: function (response) {
            if (response == 1) {
//                $("#packageList").load(baseUrl + "/fillnship/getpackagedetails/" + shipmentId);
//                $("#infoBoxDetail").load(baseUrl + "/fillnship/getshipmentdetails/" + shipmentId);
                  window.location.reload();
            }
        }
    });
});

$("body").on("click", ".removeItem", function (event) {
    $(this).parent().parent().remove();
});



});


$("body").on("click", "#addEditLocation", function (event) {
    $(this).attr('disabled', true);
    var shipmentId = $('#shipmentId').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: $('#addeditLocationFrm').attr('action'),
        type: 'POST',
        data: $('#addeditLocationFrm').serialize(),
        success: function (response) {
            if (response == 1) {
                $("#locationTrack").load(baseUrl + "/fillnship/getwarehouselocations/" + shipmentId);
                $("#modal-addEdit").modal('hide');
                alert("Warehouse location details saved successfully.");
            }
        }
    });
});

$("body").on("click", ".deletelocation", function (event) {
    var warehouseLocationId = $(this).attr('data-warehouseid');
    var shipmentId = $('#shipmentId').val();
    $(this).confirmation({
        onConfirm: function () {
            var url = baseUrl + "/fillnship/deletelocation/" + warehouseLocationId + "/" + shipmentId;
            $.ajax({
                url: url,
                type: 'GET',
                success: function (response) {
                    if (response == 1) {
                        $("#locationTrack").load(baseUrl + "/fillnship/getwarehouselocations/" + shipmentId);
                        alert('Warehouse location deleted successfully.');
                    }
                }
            });
        },
    });
});

/* Start packaging validation */
$(document).on('click', '#packagenow', function () {
    var shipmentId = $("#shipmentId").val();
    var deliveryCount = $("#deliveryCount").val();
    var addeddata = $("#labeldata").val();
    addeddata = addeddata.trim();
    var allData = new Array();
    addeddata.split("\n").forEach(function (item) {
        allData.push(item);
    });

    var match = 1;
    var deliveryScaned = 0;
    if (allData.length > 0)
    {
        for (var i = 0; i < allData.length; i++)
        {
            if(i==0)
            {
                if(allData[i] != shipmentId)
                {
                    match = 0;
                    break;
                }
            }
            else
            {
            var ifDelivery = allData[i].indexOf('-')
            if (ifDelivery != '-1')
            {
                    deliveryScaned++;
                    var delivId = allData[i].substr(parseInt(ifDelivery)+1);
                allData[i] = allData[i].substr(0, ifDelivery);
                    
                    if (allData[i] != shipmentId || (delivId!='D'+deliveryScaned))
                    {
                        match = 0;
                        break;
            }
                }
                else
            {
                    match = 0;
                break;
            }

                
        }

        }
        if(match!=0 && deliveryScaned!=deliveryCount)
        {
            match = 0;
        }
        //console.log(match);
        if (match == 0)
            alert("Wrong data! Please recheck");
        else
        {
            $("#packageShipment").val(shipmentId);
            var action = $("#frmstartpkg").attr('action'); 
            var data = $("#frmstartpkg").serialize();
            $.ajax({
                url: action,
                data: data,
                dataType: 'JSON',
                type: 'post',
                success: function (response) {
                    if (response.message == 'success')
                    {
                        $('#modal-addEdit').modal('hide');
                        $("#addPackaging").removeAttr("disabled");
                        window.location.reload();
                    }
                    else
                    {
                        alert("Something went wrong");
                    }
                }
            });
        }
    } else
        alert("Wrong data! Please recheck");

    return false;
});
/* End */

$(document).on('click', '#addPackaging', function () {
    var error = 0;
    $("#frmaddpkg input[type='text']").each(function(){
      if($(this).val()=='' || $(this).val()=='0.00')
      {
          error = 1;
      }
    });
    if(error == 0)
    {
        var action = $("#frmaddpkg").attr("action");
        var data = $("#frmaddpkg").serialize();
        var shipmentId = $(this).attr('data-shipment');
        var page = $(this).attr('data-page');
        var baseUrl = $('#baseUrl').val();
        $.ajax({
            url: action,
            data: data,
            dataType: 'JSON',
            type: 'post',
            success: function (response) {
                if (response.message == 'success')
                {
                    //$("#packaginTab").load(baseUrl + "/shipments/getpackagingdata/" + shipmentId + "/" + page);
                    window.location.reload();
                }
            }
        });
    }
    else
    {
        alert('Enter packaging details');
    }

    return false;


});

$("body").on("click", ".removePackaging", function (event) {
    var link = $(this);
    var deleteId = $(this).attr('data-id');
    //console.log(updateId);
    $(".cb_" + deleteId).attr("checked", "checked");
    var data = $("#frmpkgdetails").serializeArray();
    data.push({name: "action", value: 'delete'});
    data.push({name: "fromSource", value: "ajax"});
    var url = $("#frmpkgdetails").attr("action");

    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        data: data,
        success: function (response) {
            if (response.message == 'success')
            {
                $("#packagingrow_" + deleteId).remove();
            }
        }
    });

});

/* Show Edit Delivery Item Form */
$("body").on("click", ".editPackage", function (event) {
    $(this).closest("tr").each(function () {

        $('.packagecategory', this).each(function () {
            var categoryId = $(this).val();
            var category = $(this);
            var subcategoryId = $(this).parent().parent().next().find('.packagesubcategoryId').val();
            var productId = $(this).parent().parent().next().next().find('.packageproductId').val();

            // Populate dropdown with list of subcategories
            $(this).parent().parent().next().find('.packagesubcategory').find('option:gt(0)').remove();
            $.ajax({
                url: baseUrl + "/shipments/getsubcategorylist/" + categoryId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    $.each(response, function (key, entry) {
                        if (subcategoryId == entry.id)
                            $(category).parent().parent().next().find('.packagesubcategory').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.category));
                        else
                            $(category).parent().parent().next().find('.packagesubcategory').append($('<option></option>').attr('value', entry.id).text(entry.category));
                    });
                }
            });

            // Populate dropdown with list of products
            $(this).parent().parent().next().next().find('.packageproduct').find('option:gt(0)').remove();
            $.ajax({
                url: baseUrl + "/shipments/getproductlist/" + subcategoryId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    $.each(response, function (key, entry) {
                        if (productId == entry.id)
                            $(category).parent().parent().next().next().find('.packageproduct').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.productName));
                        else
                            $(category).parent().parent().next().next().find('.packageproduct').append($('<option></option>').attr('value', entry.id).text(entry.productName));
                    });
                }
            });
        });

        $('.value_label', this).each(function () {
            $(this).hide();
        });
        $('.value_field', this).each(function () {
            $(this).show();
        });
    });
    $(this).hide();
    $(this).next().show();
});

$("body").on("click", ".savePackaging", function (event) {
    var link = $(this);
    var updateId = $(this).attr('data-id');
    $(".cb_" + updateId).attr("checked", "checked");
    var data = $("#frmpkgdetails").serializeArray();
    data.push({name: "action", value: 'update'});
    data.push({name: "fromSource", value: "ajax"});
    var url = $("#frmpkgdetails").attr("action");

    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        data: data,
        success: function (response) {
            if (response.message == 'success')
            {
                window.location.reload();
//                $(link).closest("tr:has(input)").each(function () {
//                    $('.value_field input', this).each(function () {
////                        $(this).parent().hide();
////                        $(this).parent().prev().text($(this).val()).show();
//                       window.location.reload();
//                    });
//
//                });
            }
        }
    });

    $(this).hide();
    $(this).prev().show();

});

$(document).on('click', '.printShippingLabel', function () {
    var action = $("#frmshippinglabel").attr("action");
    var data = $("#frmshippinglabel").serialize();
    $.ajax({
        url: action,
        data: data,
        type: 'POST',
        success: function (response) {
            $("#modal-addEdit").html(response);
        }

    });
    return false;
});

$(document).on('click','.updateInvoicePayment', function() {
   
    var r = confirm("Are you sure you want to update payment status? This action cannot be reversed back!!");
    if(r) {
        var invoiceId = $(this).attr("data-id");
        $.ajax({
             url: baseUrl+'/fillnship/updateextracostinvoice',
             data: {invoiceId:invoiceId},
             type: 'POST',
             success: function (response) {
                window.location.reload();
             }

        });
    }
});

function savepacked(shipmentid) {
    
    var r = confirm("You will not be allowed to do any changes in packaging after this. This action cannot be reversed back!!");
    if(r) {
        $.ajax({
        url: baseUrl + "/fillnship/updatepackedstatus/" + shipmentid,
        type: 'GET',
        success: function (response) {
                if(response == '1')
                {
                    window.location.reload();
                }
            }
        });
    }
}

function dhl_submit() {
    var base_path_dhlci = $("input[name='base_path_dhlci']").val();
    var data = $('#dhlpost').serialize();
    var _token = $("input[name='_token']").val();

    var length = $('input[name^=dhllenght]').map(function (idx, elem) {
        return $(elem).val();
    }).get();

    var width = $('input[name^=dhlwidth]').map(function (idx, elem) {
        return $(elem).val();
    }).get();

    var height = $('input[name^=dhlheight]').map(function (idx, elem) {
        return $(elem).val();
    }).get();

    var weight = $('input[name^=dhlweight]').map(function (idx, elem) {
        return $(elem).val();
    }).get();

    var cweight = $('input[name^=dhlcweight]').map(function (idx, elem) {
        return $(elem).val();
    }).get();

    var contents = $('textarea[name^=dhlcontent]').map(function (idx, elem) {
        return $(elem).val();
    }).get();

    var shipmentId = $("#shipmentId").val();


    $.ajax({
        url: base_path_dhlci,
        type: 'POST',
        data: {data: data, _token: _token, shipmentId: shipmentId, length: length, width: width, height: height, weight: weight, cweight: cweight, contents: contents, toCountry: $("#toCountry option:selected").text(), fromCountry: $("#fromCountry option:selected").text(), fromState: $("#fromState option:selected").text(), toState: $("#toState option:selected").text(), fromCity: $("#fromCity option:selected").text(), toCity: $("#toCity option:selected").text()},
        success: function (data) {
            if ($.isEmptyObject(data.error)) {
                // success
                $("#modal-addEdit .modal-content").html(data);

            } else {
                printErrorMsg(data.error);
            }
        }
    });


}

function getWarehouseZoneList(rowId) {
    $('#warehouseZoneId').find('option:gt(0)').remove();
    if (rowId != '') {
        var url = baseUrl + "/fillnship/getwarehousezonelist/" + rowId;
        // Populate dropdown with list of provinces
        $.getJSON(url, function (data) {
            $.each(data, function (key, entry) {
                $('#warehouseZoneId').append($('<option></option>').attr('value', entry.id).text(entry.name));
            })
        });

    }
}

function saveComment(shipmentId)
{
    var message = $('#addcomment').val();
    $.ajax({
        url: baseUrl + "/fillnship/addcomment/" + shipmentId,
        type: 'POST',
        data: {comment: message},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
             var response = JSON.parse(response);
            if (response != '')
            {
                $('#addcomment').val('');
                var url = response["noteId"] + ", " + response["fillshipId"] + ", 'fillnship/shownotes'";
                $('#showNote').prepend('<div class="row"><div class="col-sm-12">By: ' + response["firstname"] + ' ' + response["lastname"] + ', ' + response["email"] + ',' + response["sentOn"] + ', <a onclick="showAddEdit(' + url + ')"><strong>Comment Details</strong></a>. </div> </div>');
            } else {
                $('#showmsg').html('<div class="danger">Notes not saved</div>')
            }
        }
    });
}


function updateItemStatus(status, itemId) {
    var selected = [];
    var shipmentstatus = [];
    var fillshipId = $('#fillshipId').val();

    

    $('#packageList input:checked').each(function () {
        shipmentstatus.push($(this).attr('data-attr'));
        selected.push($(this).attr('value'));
    });
    if (checkForDuplicates(shipmentstatus) == 1) {
	 if (shipmentstatus[0] == 'received') {
            alert("Status cannot be further updated");
            $("#itemStatus").val('');
            return false;
        }

            var r = confirm("All the selected items will be modifed. This action cannot be reversed back!!");

            if (r == true) {
                var fillshipItemId = itemId;
                if (status != 'received') {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: baseUrl + "/fillnship/updateitemstatus",
                        type: 'POST',
                        data: {fillshipItemId: fillshipItemId, status: status},
                        success: function (response) {
                            if (response == 1) {
                                $("#packageList").load(baseUrl + "/fillnship/getpackagedetails/" + fillshipId);
                                $("#itemStatus").val('');
                            }
                        }
                    });
                } else {
                    showAddEdit(fillshipItemId, status, 'fillnship/receivepackage');
                }


            }
        
    } else {
        alert('Please select procurements having same status!!');
    }
}


function checkForDuplicates(arr) {
    var x = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (x != arr[i]) {
            return 0;
        }
    }
 
    return 1;
}


function exportall_submit() {
    if ($('#exportAll input[type=checkbox]:checked').length) {
        document.getElementById("exportAll").submit();
        return false;
    }
    else {
        $("#errorTxt").css("display", "block");
        $("#errorTxt").text("Select atleast one field");
    }

}
function exportallse_submit() {    
    if ($('.selecteField:checked').length) {
        var selectedField = [];
        $('.selecteField:checked').each(function () {
            selectedField.push($(this).attr('value'));
        });
    
        var selected = [];
        $('#checkboxes input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
        
        if (selected == '') {
            setTimeout(function () {
                $('#modal-export-selected').modal('show');
            }, 1000);
            $("#errorTxtse").css("display", "block");
            $("#errorTxtse").text("Select atleast one shipment");
        }        
        else {
            $(document).ready(function () {
                var _token = $("input[name='_token']").val();
                $.ajax({
                    url:  baseUrl + "/fillnship/exportselected/1",
                    type: 'POST',
                    data: {selected: selected, selectall: selectedField, _token: _token},
                    success: function (data) {
                        window.location = data.path;
                    }
                });
            });
        }
        return false;
    }
    else {
        setTimeout(function () {
            $('#modal-export-selected').modal('show');
        }, 1000);
        $("#errorTxtse").css("display", "block");
        $("#errorTxtse").text("Select atleast one field");
        return false;
    }

}

function deleteSelected() {
    var selected = [];
    $('#checkboxes input:checked').each(function () {

        selected.push($(this).attr('value'));
    });
    var checkedIds = selected.join('^');
    if (checkedIds != '') {
        var r = confirm("All the selected items will be deleted. This action cannot be reversed back!!");

        if (r == true) {
            $('#checkedval').val(checkedIds);
            $('#frmCheckedItem').attr('action', baseUrl + "/fillnship/deleteall");
            $('#frmCheckedItem').submit();
        }
    } else {
        alert("Select atleast one field");
        return false;
    }

}

function updatePayment(shipmentId) {
    
    var paymentStatus = $('#paymentInvoiceStatus').val();
    r = confirm("Are you sure? This action cannot be reversed back!!");
    if(r)
    {
        $.ajax({
            url: baseUrl + "/fillnship/updatepaymentstatus/" + shipmentId,
            type: 'POST',
            data: {paymentStatus: paymentStatus},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if(response == 1){
                    $('#paymenyInvoiceStatus').parent().html('<button type="button" class="btn btn-default custumBtnWtIcn printBtns">'+paymentStatus+'</button>');
                    //$('.updatePayment').hide();
                    window.location.reload();
                }
            }
        });
    }
    
}

