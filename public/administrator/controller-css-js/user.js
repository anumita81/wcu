$(function () {
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('.addressBook').on('ifChecked', function (event) {
        $('#addressBook').val($(this).val());
    });

    $('#searchByDate').daterangepicker();

    //Custom radio button show daterange
    $('.customRange').on('ifChecked', function () {
        document.getElementById('customDaterange').style.visibility = 'visible';
    });
    $('.customRange').on('ifUnchecked', function () {
        document.getElementById('customDaterange').style.visibility = 'hidden';
    });


    var baseUrl = $('#baseUrl').val();
    $('#countryId').on('change', function () {

        var countryId = $('#countryId').val();

        $('#stateId').find('option:gt(0)').remove();
        $('#cityId').find('option:gt(0)').remove();

        if (countryId != '') {
            var url = baseUrl + "/users/getstatelist/" + countryId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#stateId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });

            var url = baseUrl + "/users/getcitylistbycountry/" + countryId;
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#cityId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });

        }
    });

    $('#stateId').on('change', function () {

        var stateId = $('#stateId').val();

        $('#cityId').find('option:gt(0)').remove();

        if (stateId != '') {
            var url = baseUrl + "/users/getcitylist/" + stateId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#cityId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });

        }
    });

    $('#addresscountryId').on('change', function () {
        var countryId = $('#addresscountryId').val();
        $('#addressstateId').find('option:gt(0)').remove();
        $('#addresscityId').find('option:gt(0)').remove();
        if (countryId != '') {
            var url = baseUrl + "/users/getstatelist/" + countryId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#addressstateId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });

            var url = baseUrl + "/users/getcitylistbycountry/" + countryId;
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#addresscityId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });
        }
    });

    $('#addressstateId').on('change', function () {
        var stateId = $('#addressstateId').val();
        $('#addresscityId').find('option:gt(0)').remove();

        if (stateId != '') {
            var url = baseUrl + "/users/getcitylist/" + stateId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#addresscityId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });
        }
    });
});

function setDefault(defaultType) {
    if ($('#addressBook').val() != '') {
        $('#isDefaultType').val(defaultType);
        $('#saveaddressbook').submit();
    } else {
        alert('Select an address');
        return false;
    }
}

function exportall_submit() {
    if ($('#exportAll input[type=checkbox]:checked').length) {
        document.getElementById("exportAll").submit();
        return false;
    }
    else {
        $("#errorTxt").css("display", "block");
        $("#errorTxt").text("Select atleast one field");
    }

}
function exportallse_submit() {

    if ($('.selecteField:checked').length) {
        var selectedField = [];
        $('.selecteField:checked').each(function () {
            selectedField.push($(this).attr('value'));
        });

        var selected = [];
        $('#checkboxes input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
        if (selected == '') {
            setTimeout(function () {
                $('#modal-export-selected').modal('show');
            }, 1000);
            $("#errorTxtse").css("display", "block");
            $("#errorTxtse").text("Select atleast one user");
        }
        else {
            $(document).ready(function () {
                var _token = $("input[name='_token']").val();

                $.ajax({
                    url: "users/exportselected/1",
                    type: 'POST',
                    data: {selected: selected, selectall: selectedField, _token: _token},
                    success: function (data) {
                        window.location = data.path;
                    }
                });
            });
        }
        return false;
    }
    else {
        setTimeout(function () {
            $('#modal-export-selected').modal('show');
        }, 1000);
        $("#errorTxtse").css("display", "block");
        $("#errorTxtse").text("Select atleast one field");
        return false;
    }

}
//Update on 27-11-2018
function resendActivationMail(userId, page)
{
    var _token = $("input[name='_token']").val();
    var baseUrl = $('#baseUrl').val();
    
    $.ajax({
            url: baseUrl+"/users/resendActivationMail/",
            type: 'POST',
            data: {userId: userId, page: page,  _token: _token},
            success: function (data) {


               $('.showmsg').show();                            
                       
                window.location.reload();
            }
        });
}

//Update on 03-04-2019

function changeStatus(userId, page, status)
{
    var baseUrl = $('#baseUrl').val();
    
    $.ajax({
            url: baseUrl+"/users/changestatus/"+ userId + "/" + page + "/" + status,
            type: 'POST',
            success: function (data) {
                console.log(data);
                if(data.status == 1)
                {
                    $('.showmsg p').text(data.message);
                    $('.showmsg').show();
                    window.location.reload();
                }else{
                    $('.errorsg p').text(data.message);
                    $('.errorsg').show();
                    //window.location.reload();
                }
                // $('.showmsg').text();                            
                       
                // window.location.reload();
            }
        });
}


function assignRepresentative(customerId, adminUserId)
{
    var baseUrl = $('#baseUrl').val();

        $.ajax({
            url: baseUrl+"/users/saverepresentative/"+ customerId + "/" + adminUserId,
            type: 'POST',
            success: function (data) {
                if(data.status == 1)
                {
                   
                    $('#reassignBtn'+customerId).show();
                    $('#assignBtn'+customerId).hide();
                    window.location.reload();
                    $('.showmsg p').text(data.message);
                    $('.showmsg').show();
                }else{
                    $('#reassignBtn'+customerId).hide();
                    $('#assignBtn'+customerId).show();
                }
                
            }
        });


}

function assignGroupRepresentative(groupId, adminUserId)
{
    var baseUrl = $('#baseUrl').val();

        $.ajax({
            url: baseUrl+"/usergroups/saverepresentative/"+ groupId + "/" + adminUserId,
            type: 'POST',
            success: function (data) {
                if(data.status == 1)
                {
                   
                    $('#reassignBtn'+groupId).show();
                    $('#assignBtn'+groupId).hide();
                    window.location.reload();
                    $('.showmsg p').text(data.message);
                    $('.showmsg').show();
                }else{
                    $('#reassignBtn'+groupId).hide();
                    $('#assignBtn'+groupId).show();
                }
                
            }
        });




}
function getSubscriptionDetails(listIds)
{
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url: baseUrl+"/users/getsubscriptions",
        type: 'POST',
        data: {listIds : listIds},
        success: function (response) {
            if(response)
            {
                var response = JSON.parse(response);
                console.log(response);
                for(var key in response)
                {
                    value = response[key];
                    if(value !=null)
                    {
                        $("#subscriptionStatus-"+value.userId).html("Subscribed User (<strong>"+value.status+"</strong>)");
                        $("#subscribedFor-"+value.userId).html(value.subscribedFor>1 ? value.subscribedFor+" Months":value.subscribedFor+" Month");
                        $("#subscriptionDate-"+value.userId).html(value.subscribedOn);
                        $("#expiryDate-"+value.userId).html(value.expiryDate);

                    }else{
                        $("#subscriptionStatus-"+value.userId).html("Invalid Subscription");
                    }
                    
                }

                
                
            }
        }
    });
}
