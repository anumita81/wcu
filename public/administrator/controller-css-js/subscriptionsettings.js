
$(document).on('click', '.removeBtn', function (events) {
    $(this).parents('div').eq(1).remove();
});

$(document).ready(function () {
    $("#add").click(function () {
        var token_csrf = $("input[name=_token]").val();
        var count = $("input[name=count]").val();
        $.ajax({
            url: baseUrl + "/subscriptionsettings/getsubscribtion",
            method: "POST",
            data: {_token: token_csrf, count: count},
            dataType: "text",
            success: function (data)
            {
                if (data != '')
                {
                    $('#buildyourform').append(data);
                    $("input[name=count]").val(parseInt(count) + 1);
                }
                else
                {
                    console.log('no data');
                }
            }
        });
    });



});
