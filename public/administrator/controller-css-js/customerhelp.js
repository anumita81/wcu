$(function () {

    CKEDITOR.replace('editor2', {
        allowedContent: true,
//        toolbar: [
//            {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', ]},
//            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},
//            {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates']},
//        ]
    });
})



function showAddEditMedia(id, page, urlSegment) {
    var url = urlSegment + '/' + id + '/' + page;
    $.ajax({
        method: "GET",
        url: baseUrl + '/' + url,
    }).done(function (response) {
        $('#modal-addEditMedia').html(response);
        $('#modal-addEditMedia').modal({backdrop: 'static', keyboard: false});
        $('#modal-addEditMedia').modal('show');
    });
}