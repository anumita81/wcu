$(function () {


    var baseUrl = $('#baseUrl').val();

      $('.addButt').click(function () {
        var table = $(this).parent().prev();
        table.find('tbody').append('<tr>' + $('#blank_items').html() + '</tr>');
    });

});


$("body").on("click", ".removeItem", function (event) {
    $(this).parent().parent().remove();
});

function removeItem(deleteId) {
    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: baseUrl + "/paymentmethod/deleteussdconfiguration",
                type: 'POST',
                data: {id: deleteId},
                success: function (response) {                   
                    window.location.reload();
                }
            });
    
}


$("body").on("click", ".addPackage", function (event) {
    $(this).attr('disabled', true);
    $(this).closest("tr:has(input)").each(function () {
        var inputArray = [];
        var err = false;

        $('input', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
            } else
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
        });

       
        var paymentMethodId = $('#paymentMethodId').val();

        if (err != true) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: baseUrl + "/paymentmethod/addussdconfiguration",
                type: 'POST',
                data: {data: inputArray},
                success: function (response) {
                    window.location.reload();
                }
            });
        }
    });


});




$("body").on("click", ".editPackage", function (event) {
    $(this).closest("tr").each(function () {


        $('.value_label', this).each(function () {
            $(this).hide();
        });
        $('.value_field', this).each(function () {
            $(this).show();
        });
    });
    $(this).hide();
    $(this).next().show();
});

function saveItem(itemId, index) {
    var inputArray = [];
    var err = false;
   

    $('#package'+index).each(function () {
        $('input', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
            } else
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
        });
    });
    if (err != true) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/paymentmethod/saveussdconfiguration/"+itemId,
            type: 'POST',
            data: {data: inputArray},
            success: function (response) {
                window.location.reload();
            }
        });

        
    }

}






function exportall_submit() {
    if ($('#exportAll input[type=checkbox]:checked').length) {
        document.getElementById("exportAll").submit();
        return false;
    }
    else {
        $("#errorTxt").css("display", "block");
        $("#errorTxt").text("Select atleast one field");
    }

}
function exportallse_submit() {

    if ($('.selecteField:checked').length) {
        var selectedField = [];
        $('.selecteField:checked').each(function () {
            selectedField.push($(this).attr('value'));
        });

        var selected = [];
        $('#checkboxes input:checked').each(function () {

            selected.push($(this).attr('value'));
        });
        if (selected == '') {
            setTimeout(function () {
                $('#modal-export-selected').modal('show');
            }, 1000);
            $("#errorTxtse").css("display", "block");
            $("#errorTxtse").text("Select atleast one shipment");
        }
        else {
            $(document).ready(function () {
                var _token = $("input[name='_token']").val();
                $.ajax({
                    url: baseUrl + "/procurement/exportselected/1",
                    type: 'POST',
                    data: {selected: selected, selectall: selectedField, _token: _token},
                    success: function (data) {
                        //console.log(data);
                        window.location = data.path;
                    }
                });
            });
        }
        return false;
    }
    else {
        setTimeout(function () {
            $('#modal-export-selected').modal('show');
        }, 1000);
        $("#errorTxtse").css("display", "block");
        $("#errorTxtse").text("Select atleast one field");
        return false;
    }

}


function checkForDuplicates(arr) {
    var x = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (x != arr[i]) {
            return 0;
        }
    }
    return 1;
}








