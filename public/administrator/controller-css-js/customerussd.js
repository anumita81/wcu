/*$("#userUnit").keypress(function(e){
    if(e.keyCode == 13)
    {
        userVerification();
    }
});*/

$(document).on('blur', '.roundFormat', function () {
    if ($.isNumeric(this.value)) {
        var newVal = parseFloat(this.value);
        newVal = Math.round(newVal);
        $(this).val(newVal);
    } else {
        //var newVal = 0;
        $(this).val('');
    }
});

function userVerification() {
    var userUnit = $('#userUnit').val();

    if (userUnit != '') {
        $.ajax({
            url: baseUrl + "/shipments/verifyuser/" + userUnit,
            type: 'GET',
            success: function (response) {
                if (response == 0) {
                    $('#user-verification').removeClass('text-success');
                    $('#user-verification').addClass('text-danger');
                    $('#user-verification').html('<b>This is an Invalid Customer.</b>');
                    $("#submitBtn").prop("disabled", true);
                    if($(".disableblock").length==0)
                        $("#shipmentDeliveryDetails").addClass("disableblock");
                    $("#customerSettingsBlock").hide();
                } else {
                    
                    $("#shipmentDeliveryDetails").removeClass("disableblock");
                    $('#user-verification').removeClass('text-danger');
                    $('#user-verification').addClass('text-success');
                    $('#user-verification').html('<b>This is a valid Customer.</b>');
                    $("#submitBtn").prop("disabled", false);
                    $('#modal-verify').html(response);
                    $('#modal-verify').modal('show');
                }
            }
        });
    } else {
        $("#addEditFrm input:not('#userUnit')").prop("disabled", true);
        $("#addEditFrm select").prop("disabled", true);
        $('#user-verification').removeClass('text-success');
        $('#user-verification').removeClass('text-danger');
        $('#user-verification').html('');
    }
}