$(function () {
    $("#addeditFrm").validate();

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('#searchByDate').daterangepicker();

    var baseUrl = $('#baseUrl').val();


    $('#trackingNumber').keydown(function (e) {

        newLines = $(this).val().split("\n").length;
        $('#numberTxt').text(newLines);


    });
    
    /*$('body').on('click','.editTrackingData',function(e){
        $("#text-data").hide();
        $("#text-edit").show();
    });*/

});

function exportall_submit() {
    if ($('#exportAll input[type=checkbox]:checked').length) {
        document.getElementById("exportAll").submit();
        return false;
    }
    else {
        $("#errorTxt").css("display", "block");
        $("#errorTxt").text("Select atleast one field");
    }

}

function checkSelectRecord() {
    //var selected = [];
    if ($("input[name=checkboxselected]:checked").length > 0) {
        //selected.push($(this).attr('name'));
        $('#modal-export-selected').modal('show');
    } else {
        alert('Please select record');
    }
}

function exportallse_submit() {
    if ($('.selecteField:checked').length) {
        var selectedField = [];
        $('.selecteField:checked').each(function () {
            selectedField.push($(this).attr('value'));
        });

        var selected = [];
        $('#checkboxes input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
        if (selected == '') {
            setTimeout(function () {
                $('#modal-export-selected').modal('show');
            }, 1000);
            $("#errorTxtse").css("display", "block");
            $("#errorTxtse").text("Select atleast one user");
        }
        else {
            $(document).ready(function () {
                var _token = $("input[name='_token']").val();

                $.ajax({
                    url: "shipmenttracking/exportselected/1",
                    type: 'POST',
                    data: {selected: selected, selectall: selectedField, _token: _token},
                    success: function (data) {
                        window.location = data.path;
                    }
                });
            });
        }
        return false;
    }
    else {
        setTimeout(function () {
            $('#modal-export-selected').modal('show');
        }, 1000);
        $("#errorTxtse").css("display", "block");
        $("#errorTxtse").text("Select atleast one field");
        return false;
    }

}

function deleteSelected() {
    var selected = [];
    $('#checkboxes input:checked').each(function () {
        selected.push($(this).attr('value'));
    });
    var checkedIds = selected.join('^');

    if (checkedIds != '') {
        var r = confirm("All the selected items will be deleted. This action cannot be reversed back!!");
        if (r == true) {
            $('#checkedval').val(checkedIds);
            $('#frmCheckedItem').attr('action', baseUrl + '/shipmenttracking/deleteall');
            $('#frmCheckedItem').submit();
        }
    } else {
        alert("Select atleast one field");
        return false;
    }

}