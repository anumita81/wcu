$(function () {
    /*CKEDITOR.replace('editor1')
     $('.textarea').wysihtml5()
     
     CKEDITOR.replace('editor2')
     $('.textarea').wysihtml5()*/

    CKEDITOR.replace('editor1', {
        allowedContent: true,
        toolbar: [
            {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', ]},
            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},
            {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates']},
        ]
    });

    CKEDITOR.replace('editor2', {
        allowedContent: true,
        toolbar: [
            {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', ]},
            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},
            {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates']},
        ]
    });

    //$("#addEditFrm").validate();

})
$('#file-upload').change(function () {
    var i = $(this).prev('label').clone();
    var file = $('#file-upload')[0].files[0].name;
    $(this).prev('label').text(file);
});


(function ($) {
    $.fn.checkFileType = function (options) {
        var defaults = {
            allowedExtensions: [],
            success: function () {},
            error: function () {}
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            $(this).on('change', function () {
                var value = $(this).val(),
                        file = value.toLowerCase(),
                        extension = file.substring(file.lastIndexOf('.') + 1);

                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();

                }

            });

        });
    };

})(jQuery);

$(function () {
    $('#file-upload').checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'gif', 'png'],
        success: function () {
            //alert('Success');


        },
        error: function () {
            alert('Error! File type not supported');
            readURL('error');
            //$('#loaded').css('display', 'none');
            //$('#defaultLoaded').css('display', 'block');
        }
    });

});

function readURL(input) {
    if (input != 'error') {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#loaded').css('display', 'block');
                $('#loaded').attr('src', e.target.result);
                $('#defaultLoaded').css('display', 'none');
            }

            reader.readAsDataURL(input.files[0]);
        }
    } else {
        $('#loaded').css('display', 'none');
        $('#defaultLoaded').css('display', 'block');
    }
}


$("#file-upload").change(function () {
    //alert(this.files[0]);
    readURL(this);
});
