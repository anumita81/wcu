
$(function () {

    $('#warehouse').select2();

    $("#addEditFrm").validate();


    $('#reservation').daterangepicker();
    //Date picker
    $('#datepicker').datepicker({
        autoclose: true
    });

    //Custom radio button show daterange
    $('.customRange').on('ifChecked', function () {
        document.getElementById('customDaterange').style.visibility = 'visible';
    });
    $('.customRange').on('ifUnchecked', function () {
        document.getElementById('customDaterange').style.visibility = 'hidden';
    });

    $("#addeditFrm").validate();

    $(".sidebar-menu>li.active").removeClass("active");
    $("#leftNavUsers").addClass("active");
    $("#leftNavAdmin").addClass("active");
    $("#leftNavadminUser").addClass("active");

    $('#modal-changepwd').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        $('#password_yes').val('');
        $('#password_yes').val(rowid);
    });

    $('#modal-notify').on('show.bs.modal', function (e) {
        var rowid1 = $(e.relatedTarget).data('id');
        var rowid2 = $(e.relatedTarget).data('userid');
        $('#notify_email').val('');
        $('#notify_email').val(rowid1);
        $('#notify_userId').val(rowid2);
    });

    $('input[name=search_rangedate]').on('ifChanged', function () {
        if ($("input[name=search_rangedate]:checked").val() == 'custom') {
            document.getElementById('customDaterange').style.visibility = 'visible';
        } else {
            document.getElementById('customDaterange').style.visibility = 'hidden';
        }
    });

    if ($("#isCustomSelected").val() == 'custom') {
        document.getElementById('customDaterange').style.visibility = 'visible';
    }


    CKEDITOR.replaceAll('msg1')
    $('.textarea').wysihtml5();

    CKEDITOR.replace('editor1', {
        allowedContent: true,
        toolbar: [
            {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', ]},
            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},
            {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates']},
        ]
    });

    

});

$(document).ready(function(){
$("#password_submit").click(function (e) {
        e.preventDefault();
        var _token = $("input[name='_token']").val();
        var new_password = $("input[name='new_password']").val();
        var confirm_password = $("input[name='confirm_password']").val();
        var userId = $("input[name='userId']").val();
        var base_path = $("input[name='base_path']").val();
        $.ajax({
            url: base_path,
            type: 'POST',
            data: {_token: _token, new_password: new_password, confirm_password: confirm_password, userId: userId},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    //alert(data.success);
                    $(".print-error-msg").css('display', 'none');
                    $(".print-success-msg").css('display', 'block');
                    $(".print-success-msg").find("ul").append('<li>' + data.success + '</li>');
                    setTimeout(function () {
                        $('#modal-addEdit').modal('hide');
                    }, 3000);
                    setTimeout(function () {
                        $(".print-error-msg").css('display', 'none');
                        $(".print-success-msg").css('display', 'none');
                    }, 3000);
                } else {
                    printErrorMsg(data.error);
                }
            }
        });


    }); 
});

function form_submit() {
    document.getElementById("passwordfrom").submit();
}

function exportall_submit() {
    if ($('#exportAlls input[type=checkbox]:checked').length) {
        document.getElementById("exportAlls").submit();
        return false;
    }
    else {
        setTimeout(function () {
            $('#modal-export').modal('show');
        }, 1000);
        $("#errorTxt").css("display", "block");
        $("#errorTxt").text("Select atleast one field");
        //return false;
    }
}

function exportallse_submit() {
    if ($('#exportAllse input[type=checkbox]:checked').length) {
        var selectedField = [];
        $('#exportAllse input:checked').each(function () {
            selectedField.push($(this).attr('value'));
        });
        var selected = [];
        $('#checkboxes input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
        if (selected == '') {
            setTimeout(function () {
                $('#modal-export-selected').modal('show');
            }, 1000);
            $("#errorTxtse").css("display", "block");
            $("#errorTxtse").text("Select atleast one user");
        }
        else {
            var _token = $("input[name='_token']").val();
            var base_path_selected_export = $("input[name='base_path_selected_export']").val();


            $.ajax({
                /*headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
                 },*/
                url: base_path_selected_export,
                type: 'POST',
                data: {selected: selected, selectedField: selectedField, _token: _token},
                success: function (data) {
                    //alert(data.path);
                    window.location = data.path;
                    if ($.isEmptyObject(data.error)) {
                        //alert(data);
                    } else {
                        //alert(data.error);
                    }
                }
            });
        }
        return false;
    }
    else {
        setTimeout(function () {
            $('#modal-export-selected').modal('show');
        }, 1000);
        $("#errorTxtse").css("display", "block");
        $("#errorTxtse").text("Select atleast one field");
        return false;
    }

}

function printErrorMsg(msg) { //alert(msg);
    setTimeout(function () {
        $('#modal-add').modal('show');
    }, 3000);
    $(".print-error-msg").find("ul").html('');
    $(".print-success-msg").css('display', 'none');
    $(".print-error-msg").css('display', 'block');
    $.each(msg, function (key, value) {
        $(".print-error-msg").find("ul").append('<li>' + value + '</li>');
    });
}

function checkSelectRecord() {
    //var selected = [];
    if ($("input[name=checkboxselected]:checked").length > 0) {
        //selected.push($(this).attr('name'));
        $('#modal-export-selected').modal('show');
    } else {
        alert('Please select record');
    }
}

function showAddEditCustom(id, page, urlSegment) {
    var url = urlSegment + '/' + id + '/' + page;
    $.ajax({
        method: "GET",
        url: baseUrl + '/' + url,
    }).done(function (response) {
        $('#modal-addEditCustom').html(response);
        $('#modal-addEditCustom').modal({backdrop: 'static', keyboard: false});
        $('#modal-addEditCustom').modal('show');
    });
}


function exportallperm_submit() {
        var selected = [];
        $('#checkboxes input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
        console.log(selected);
        if (selected == '') {
            setTimeout(function () {
                $('#modal-export-perm').modal('show');
            }, 1000);
            $("#errorTxtperm").css("display", "block");
            $("#errorTxtperm").text("Select atleast one user");
        }
        else {
            $("#wait").css("display", "block");
            //$("#mainContentNotLoad").css("display", "none");
            var _token = $("input[name='_token']").val();
            var base_path_selected_perm = $("input[name='base_path_selected_perm']").val();
            $.ajax({
                url: base_path_selected_perm,
                type: 'POST',
                data: {selected: selected, _token: _token},
                success: function (data) {
                    //alert(data.path);
                    //window.location = data.path;
                    if ($.isEmptyObject(data.error)) {
                        $("#wait").css("display", "none");
                        $("#userroleApply").show();
                        //$("#mainContentNotLoad").css("display", "block");
                        //alert(data.returnValue);
                    } else {
                        alert(data.error);
                    }
                }
            });
        }
        return false;
    }
