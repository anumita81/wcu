function showAddCustom(urlSegment, id, taxId, page) {
    var url = urlSegment;
    $.ajax({
        method: "GET",
        url: baseUrl + '/' + url + id + '/' + taxId + '/' + page,
    }).done(function (response) {
        $('#modal-add').html(response);
        $('#modal-add').modal('show');
    });
}

function showEditCustom(urlSegment) {
    var url = urlSegment;
    $.ajax({
        method: "GET",
        url: baseUrl + '/' + url,
    }).done(function (response) {
        $('#modal-edit').html(response);
        $('#modal-edit').modal('show');
    });
}

