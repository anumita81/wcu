$(function () {

    $("#addeditFrm").validate();

    var baseUrl = $('#baseUrl').val();
    $('#countryCode').on('change', function () {

        var countryCode = $('#countryCode').val();

        $('#stateCode').find('option:gt(0)').remove();

        if (countryCode != '') {
            var url = baseUrl + "/city/getstatelist/" + countryCode;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#stateCode').append($('<option></option>').attr('value', entry.code).text(entry.name));
                })
            });

        }
    });
});