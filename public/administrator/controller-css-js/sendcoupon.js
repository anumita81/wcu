// Send Coupon Code to Users
function CouponToSelectedUsers(selectedType) {
            
    if(selectedType == 'S') {
        var selected = [];
        $('#checkboxes input:checked').each(function() {
            selected.push($(this).attr('value'));
        });
        if(selected == ''){
            setTimeout(function() {$('#modal-export-selected').modal('show');}, 1000);
            $("#errorTxtse").css("display", "block");
            $("#errorTxtse").text("Select atleast one user");
        }
        else {
                $(document).ready(function() {

                var _token = $("input[name='_token']").val();
                var base_path_selected = $("input[name='base_path_selected']").val(); 


                $.ajax({
                    url: base_path_selected,
                    type:'POST',
                    data: {selected:selected, _token:_token, selectedType:selectedType},
                    success: function(data) { 
                        //console.log(data.success);
                        setTimeout(function() {$('#modal-export-selected').modal('show');}, 1000);
                        $("#errorTxtse").css("display", "block");
                        $("#errorTxtse").text(data.success);
                        if(data.success == 'Data saved successfully. Sending coupon codes to users will started shortly. This will take time based on number of users.'){
                            $("#errorTxtse").removeClass('alert-danger');
                            $("#errorTxtse").addClass('alert-success');
                        } else {
                            $("#errorTxtse").addClass('alert-danger');
                            $("#errorTxtse").removeClass('alert-success');
                        }
                        if($.isEmptyObject(data.error)){
                            //console.log(data);
                        }else{
                            //console.log(data);
                        }
                    }
                });

            
            });
        }
    } else {
        $(document).ready(function() {

            var _token = $("input[name='_token']").val();
            var base_path_selected = $("input[name='base_path_selected']").val(); 


            $.ajax({
                url: base_path_selected,
                type:'POST',
                data: {selected:selected, _token:_token, selectedType:selectedType},
                success: function(data) { 
                    //console.log(data.success);
                    setTimeout(function() {$('#modal-export-selected').modal('show');}, 1000);
                    if(data.success == 'X'){
                        $("#errorTxtse").css("display", "block");
                        $("#errorTxtse").text('No data to save to send coupon code');
                        $("#errorTxtse").addClass('alert-danger');
                        $("#errorTxtse").removeClass('alert-success');
                    } else {
                        $("#errorTxtse").css("display", "block");
                        $("#errorTxtse").text(data.success);
                        $("#errorTxtse").removeClass('alert-danger');
                        $("#errorTxtse").addClass('alert-success');
                    }
                    
                    if($.isEmptyObject(data.error)){
                        //console.log(data.error);
                    }else{
                        //console.log(data);
                    }
                }
            });

        
        });
    }
    
} 