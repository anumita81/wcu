$(function () {

    $("#addeditFrm").validate();

    var baseUrl = $('#baseUrl').val();
    $('#countryId').on('change', function () {

        var countryId = $('#countryId').val();

        $('#stateId').find('option:gt(0)').remove();
        $('#cityId').find('option:gt(0)').remove();

        if (countryId != '') {
            var url = baseUrl + "/location/getstatelist/" + countryId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $('#phonecode').val(data.phCode);
                $.each(data.stateList, function (key, entry) {
                    $('#stateId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });

            var url = baseUrl + "/location/getcitylistbycountry/" + countryId;
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#cityId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });

        }
    });

    $('#stateId').on('change', function () {

        var stateId = $('#stateId').val();

        $('#cityId').find('option:gt(0)').remove();

        if (stateId != '') {
            var url = baseUrl + "/location/getcitylist/" + stateId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#cityId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });

        }
    });

    // jQuery


});