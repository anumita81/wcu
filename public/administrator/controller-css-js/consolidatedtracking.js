$(function () {

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });


    $("#addeditFrm").validate();

    var baseUrl = $('#baseUrl').val();


    $('#trackingNum').keydown(function (e) {
        newLines = $(this).val().split("\n").length;
        $('#numberTxt').text(newLines);


    });

});
(function ($) {
    $.fn.checkFileType = function (options) {
        var defaults = {
            allowedExtensions: [],
            success: function () {
            },
            error: function () {
            }
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            $(this).on('change', function () {
                var value = $(this).val(),
                        file = value.toLowerCase(),
                        extension = file.substring(file.lastIndexOf('.') + 1);

                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();

                }

            });

        });
    };

})(jQuery);
$(function () {
    $('#file-upload').checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'gif', 'png'],
        success: function () {
            //alert('Success');


        },
        error: function () {
            alert('Error! File type not supported');
            
        }
    });

});

$(document).on('click', '.withSubOption', function () {
    if($(this).val() == 'yes')
    {
        $(this).parent().parent().parent().parent().parent().find('.subOption').show();
        $(this).parent().parent().parent().parent().parent().find('.subOption input').prop('required',true);
    }
    else
    {
        $(this).parent().parent().parent().parent().parent().find('.subOption').hide();
        $(this).parent().parent().parent().parent().parent().find('.suboptionRadio').prop('checked', false);
        $(this).parent().parent().parent().parent().parent().find('.subOption input').prop('required',false);
        
    }
});

$(document).on('click', '.withSubOptionNo', function () {
    
    if($(this).val() == 'no')
    {
        $(this).parent().parent().parent().parent().parent().find('.subOption').show();
        $(this).parent().parent().parent().parent().parent().find('.subOption input').prop('required',true);
    }
    else
    {
        $(this).parent().parent().parent().parent().parent().find('.subOption').hide();
        $(this).parent().parent().parent().parent().parent().find('.suboptionRadio').prop('checked', false);
        $(this).parent().parent().parent().parent().parent().find('.subOption input').prop('required',false);
        
    }
});

$(document).on('change', '#consolidatedShipmentTypeOptions', function () {
    if($(this).val() == 'air')
    {
        $("#airOptions").show();
        $("#seaOptions").hide();
        $("#seaOptions").find('.subOption input').prop('required',false);
    }
    else
    {
        $("#airOptions").hide();
        $("#airOptions").find('.subOption input').prop('required',false);
        $("#seaOptions").show();
    }
});


function exportall_submit() {
    if ($('#exportAll input[type=checkbox]:checked').length) {
        document.getElementById("exportAll").submit();
        return false;
    }
    else {
        $("#errorTxt").css("display", "block");
        $("#errorTxt").text("Select atleast one field");
        return false;
    }

}

function deleteSelected() {
    var selected = [];
    $('#checkboxes input:checked').each(function () {
        selected.push($(this).attr('value'));
    });
    var checkedIds = selected.join('^');

    if (checkedIds != '') {
        var r = confirm("All the selected items will be deleted. This action cannot be reversed back!!");
        if (r == true) {
            $('#checkedval').val(checkedIds);
            $('#frmCheckedItem').attr('action', baseUrl + '/consolidatedtracking/deleteall');
            $('#frmCheckedItem').submit();
        }
    } else {
        alert("Select atleast one field");
        return false;
    }

}

function submitClose(id,page) {
    var error = 0;
    if($("#trackingName").val() == '')
    {
        error = 1;
        $("#trackingName").focus();
        alert("Please enter tracking name");
    }
    else if($("#deliveredOn").val() == '')
    {
        error = 1;
        alert("Please enter date when packaging is done");
    }
    else if($("#trackingNum").val() == '')
    {
        error = 1;
        $("#trackingNum").focus();
        alert("Please enter all tracking numbers");
    }
    if(id == 0 && error == 0)
    {
        if($("#file-upload").val() == '')
        {
            error = 1;
            alert("Please attach images");
        }
    }
    if(error == 0)
    {
        var fileField = $("#file-upload")[0]

        for (var i = 0; i < fileField.files.length; ++i) {
            
            var fileName = fileField.files[i].name;
            fileName = fileName.toLowerCase();
            if (!(/\.(gif|jpg|jpeg|svg|png)$/i).test(fileName)) {              
                error = 1;  
                break;
            }
        }
        
        if(error == 1)
        {
            alert("Please upload files of type gif,jpg,jpeg,svg,png");
        }
    }
    
    if(error == 0)
    {
        $("#consolidatedAction").val('submit_close');

        var form = $('#addFrm')[0];
        var formData = new FormData(form);

        $.ajax({
            method: "POST",
            url: baseUrl + '/consolidatedtracking/save/' + id + '/' + page,
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json"
        }).done(function (response) {

            $("#modal-addEdit").modal('hide');
            var url = 'consolidatedtracking/getsubmitcloseoption/'+ response.id + '/' + page;
            $.ajax({
                method: "GET",
                url: baseUrl + '/' + url,
            }).done(function (response) {
                $('#modal-submit-close-option').html(response);
                $('#modal-submit-close-option').modal({backdrop: 'static', keyboard: false});
                $('#modal-submit-close-option').modal('show');
            });
        });
    }

}

function updateStage(trackingId) {
    //alert($("#updatestageFrm").attr('action'));
    $('#updatebtn').attr('disabled');
    $('#updatebtn').text('Updating...');
       $.ajax({
            url: $("#updatestageFrm").attr('action'),
            data : $("#updatestageFrm").serialize(),
            type: 'POST',
            success: function (response) {
                if (response == 1) {
                    
                    alert('Stages data updated successfully.');
                     window.location.reload();
                }
            }
        });
     
    
        
    return false;
}

