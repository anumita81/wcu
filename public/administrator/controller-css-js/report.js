$(function () {

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('.addressBook').on('ifChecked', function (event) {
        $('#addressBook').val($(this).val());
    });

    $('#searchByDate').daterangepicker();

    //Custom radio button show daterange
    $('.customRange').on('ifChecked', function () {
        document.getElementById('customDaterange').style.visibility = 'visible';
    });
    $('.customRange').on('ifUnchecked', function () {
        document.getElementById('customDaterange').style.visibility = 'hidden';
    });


    var baseUrl = $('#baseUrl').val();
    var searchByCondition = "";
    
//    $('.searchBy').each(function(i,j) {
//       if($(this).prop("checked"))
//       {
//           searchByCondition = $(this).val();
//       }
//    });
//    
//    if(searchByCondition!='')
//    {
//        $.ajaxSetup({
//            headers: {
//                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//            }
//        });
//        $.ajax({
//            url: baseUrl + "/report/getaccountsearchcondition/"+searchByCondition,
//            type: 'GET',
//            success: function (response) {
//                $('#searchByDiv').html(response);
//                $('.searchAccountBtn').removeAttr('disabled');
//            }
//        });
//    }

    /* Get State List For Search */
    if ($('#destinationCountry').val() != '') {
        var countryId = $('#destinationCountry').val();
        var stateId = $('#destinationState').attr('data-value');

        $('#destinationState').find('option:gt(0)').remove();
        if (countryId != '') {
            var url = baseUrl + "/procurement/getstatelist/" + countryId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    if (stateId == entry.id)
                        $('#destinationState').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.name));
                    else
                        $('#destinationState').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });

        }
    }

    /* Get City List For Search */
    if ($('#destinationState').attr('data-value') != '') {
        var stateId = $('#destinationState').attr('data-value');
        var cityId = $('#destinationCity').attr('data-value');

        $('#destinationCity').find('option:gt(0)').remove();

        if (stateId != '') {
            var url = baseUrl + "/procurement/getcitylist/" + stateId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    if (cityId == entry.id)
                        $('#destinationCity').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.name));
                    else
                        $('#destinationCity').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })


            });

        }
    }

    /* Get State List On Chnage */
    $('#destinationCountry').on('change', function () {

        var countryId = $('#destinationCountry').val();

        $('#destinationState').find('option:gt(0)').remove();

        if (countryId != '') {
            var url = baseUrl + "/procurement/getstatelist/" + countryId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#destinationState').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });

        }
    });

    $('#destinationState').on('change', function () {

        var stateId = $('#destinationState').val();

        $('#destinationCity').find('option:gt(0)').remove();

        if (stateId != '') {
            var url = baseUrl + "/procurement/getcitylist/" + stateId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#destinationCity').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })


            });

        }
    });

    $('.addButt').click(function () {
        var table = $(this).parent().prev();
        table.find('tbody').append('<tr>' + $('#blank_items').html() + '</tr>');
    });

    $('#addressBookId').on('change', function () {
        $('#changeaddressFrm')[0].reset();
        if ($(this).val() != 'new') {
            var id = $(this).val();
            $.getJSON(baseUrl + "/procurement/getaddressbookdetails/" + id, function (data) {
                $('#userAddressBookId').val(id);
                $('#title').val(data.title);
                $('#firstName').val(data.firstName);
                $('#lastName').val(data.lastName);
                $('#email').val(data.email);
                $('#address').val(data.address);
                $('#alternateAddress').val(data.alternateAddress);
                $('#zipcode').val(data.zipcode);
                $('#zipcode').val(data.zipcode);
                $('#phone').val(data.phone);
                $('#alternatePhone').val(data.alternatePhone);
                $('#addresscountryId').val(data.countryId);

                var url = baseUrl + "/procurement/getstatelist/" + data.countryId;
                // Populate dropdown with list of provinces
                $.getJSON(url, function (response) {
                    $.each(response, function (key, entry) {
                        if (entry.id == data.stateId)
                            $('#addressstateId').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.name));
                        else
                            $('#addressstateId').append($('<option></option>').attr('value', entry.id).text(entry.name));

                    })
                });

                var url = baseUrl + "/procurement/getcitylistbycountry/" + data.countryId;
                $.getJSON(url, function (response) {
                    $.each(response, function (key, entry) {
                        if (data.cityId != null && entry.id == data.cityId)
                            $('#addresscityId').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.name));
                        else
                            $('#addresscityId').append($('<option></option>').attr('value', entry.id).text(entry.name));

                    })
                });
            });

            $('#changeaddressFrm input').attr('readonly', 'readonly');
            $('#changeaddressFrm select').not('#addressBookId').attr('readonly', 'readonly').attr("style", "pointer-events: none;");

        } else {
            $('#changeaddressFrm input').removeAttr('readonly');
            $('#changeaddressFrm select').removeAttr('readonly').removeAttr("style");
        }
    });
    $('#addresscountryId').on('change', function () {
        var countryId = $('#addresscountryId').val();
        $('#addressstateId').find('option:gt(0)').remove();
        $('#addresscityId').find('option:gt(0)').remove();

        if (countryId != '') {
            var url = baseUrl + "/procurement/getstatelist/" + countryId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#addressstateId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });

            var url = baseUrl + "/procurement/getcitylistbycountry/" + countryId;
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#addresscityId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });

        }
    });

    $('#addressstateId').on('change', function () {
        var stateId = $('#addressstateId').val();
        $('#addresscityId').find('option:gt(0)').remove();
        if (stateId != '') {
            var url = baseUrl + "/procurement/getcitylist/" + stateId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#addresscityId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });
        }
    });


    $('.searchByCondition').on('ifChecked', function () {
        $('#serachDataRecords').hide();
        var searchByCondition = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: baseUrl + "/report/getcustomersearchcondition/"+searchByCondition,
            type: 'GET',
            success: function (response) {
                $('#searchByDiv').html(response);
                $('.searchCustomerBtn').removeAttr('disabled');
            }
        });
    });

    $('.searchBy').on('ifChecked', function () {
        var searchByCondition = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: baseUrl + "/report/getaccountsearchcondition/"+searchByCondition,
            type: 'GET',
            success: function (response) {
                $('#searchByDiv').html(response);
                $('.searchAccountBtn').removeAttr('disabled');
            }
        });
    });

});


$("body").on("blur", ".format", function (event) {
    if ($.isNumeric(this.value)) {
        var newVal = parseFloat(this.value);
        $(this).val(newVal.toFixed(2));
    } else {
        var newVal = 0;
        $(this).val(newVal.toFixed(2));
    }
});



$("body").on("click", "#changeAddressBtn", function (event) {
    $(this).attr('disabled', true);
    var shipmentId = $('#shipmentId').val();
    var userId = $('#userId').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: $('#changeaddressFrm').attr('action'),
        type: 'POST',
        data: $('#changeaddressFrm').serialize(),
        success: function (response) {
            if (response == 1) {
                $(".shipList").load(baseUrl + "/procurement/getaddressdetails/" + shipmentId);
                $("#modal-addEdit").modal('hide');
                alert("Address details saved successfully.");

            }
        }
    });
});


$(document).on('blur', '.qty', function () {
    var qty = $(this).val();
    var value = $(this).parent().parent().prev().find('.product-val').val();
    var shipping = $(this).parent().parent().next().find('.product-shipping').val();
    if (shipping == '')
        shipping = 0;

    if ($.isNumeric(qty)) {
        var producttotal = parseFloat(qty * value);
        producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().next().next().find('.product-total').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});

$(document).on('blur', '.product-val', function () {
    var value = $(this).val();
    var qty = $(this).parent().parent().next().find('.qty').val();
    var shipping = $(this).parent().parent().next().next().find('.product-shipping').val();
    if (shipping == '')
        shipping = 0;

    if ($.isNumeric(qty)) {
        var producttotal = parseFloat(qty * value);
        producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().next().next().next().find('.product-total').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});

$(document).on('blur', '.product-shipping', function () {
    var shipping = $(this).val();
    var value = $(this).parent().parent().prev().prev().find('.product-val').val();
    var qty = $(this).parent().parent().prev().find('.qty').val();
    if (shipping == '')
        shipping = 0;

    if ($.isNumeric(qty)) {
        var producttotal = parseFloat(qty * value);
        producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().next().find('.product-total').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});


$(document).on('blur', '.quantity', function () {
    var qty = $(this).val();
    var value = $(this).parent().parent().find('.productval').val();
    var shipping = $(this).parent().parent().find('.productshipping').val();
    if (shipping == '')
        shipping = 0;

    if ($.isNumeric(qty)) {
        var producttotal = parseFloat(qty * value);
        producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().find('.producttotal').val((producttotal.toFixed(2)));
    } else {
        $(this).val(0);
    }
});

$(document).on('blur', '.productval', function () {
    var value = $(this).val();
    var qty = $(this).parent().parent().find('.quantity').val();
    var shipping = $(this).parent().parent().find('.productshipping').val();
    if (shipping == '')
        shipping = 0;
    if ($.isNumeric(value)) {
        var producttotal = parseFloat(qty * value);
        producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().find('.producttotal').val((producttotal.toFixed(2)));
    } else {
        ;
        $(this).val(0);
    }
});

$(document).on('blur', '.productshipping', function () {
    var shipping = $(this).val();
    var qty = $(this).parent().parent().find('.quantity').val();
    var value = $(this).parent().parent().find('.productval').val();
    if (shipping == '')
        shipping = 0;
    if ($.isNumeric(shipping)) {
        var producttotal = parseFloat(qty * value);
        producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().find('.producttotal').val((producttotal.toFixed(2)));
    } else {
        $(this).val(0);
    }
});

$("body").on("click", ".addPackage", function (event) {
    $(this).attr('disabled', true);
    $(this).closest("tr:has(input)").each(function () {
        var inputArray = [];
        var err = false;

        $('select', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
            } else
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
        });

        $('input', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
            } else
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
        });

        var shipmentId = $('#shipmentId').val();

        if (err != true) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: baseUrl + "/procurement/adddeliveryitem",
                type: 'POST',
                data: {data: inputArray},
                success: function (response) {
                    /* Check Procurement Status */
                    $("#packageList").load(baseUrl + "/procurement/getpackagedetails/" + shipmentId);
                    $("#infoBoxDetail").load(baseUrl + "/procurement/getprocurementdetails/" + shipmentId);
                }
            });
        }
    });


});


$("body").on("click", ".removeItem", function (event) {
    $(this).parent().parent().remove();
});

$("body").on("click", ".editPackage", function (event) {
    $(this).closest("tr").each(function () {

        $('.packagecategory', this).each(function () {
            var categoryId = $(this).val();
            var category = $(this);
            var subcategoryId = $(this).parent().parent().next().find('.packagesubcategoryId').val();
            var productId = $(this).parent().parent().next().next().find('.packageproductId').val();

            // Populate dropdown with list of subcategories
            $(this).parent().parent().next().find('.packagesubcategory').find('option:gt(0)').remove();
            $.ajax({
                url: baseUrl + "/procurement/getsubcategorylist/" + categoryId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    $.each(response, function (key, entry) {
                        if (subcategoryId == entry.id)
                            $(category).parent().parent().next().find('.packagesubcategory').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.category));
                        else
                            $(category).parent().parent().next().find('.packagesubcategory').append($('<option></option>').attr('value', entry.id).text(entry.category));
                    });
                }
            });

            // Populate dropdown with list of products
            $(this).parent().parent().next().next().find('.packageproduct').find('option:gt(0)').remove();
            $.ajax({
                url: baseUrl + "/procurement/getproductlist/" + subcategoryId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    $.each(response, function (key, entry) {
                        if (productId == entry.id)
                            $(category).parent().parent().next().next().find('.packageproduct').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.productName));
                        else
                            $(category).parent().parent().next().next().find('.packageproduct').append($('<option></option>').attr('value', entry.id).text(entry.productName));
                    });
                }
            });
        });

        $('.value_label', this).each(function () {
            $(this).hide();
        });
        $('.value_field', this).each(function () {
            $(this).show();
        });
    });
    $(this).hide();
    $(this).next().show();
});

$("body").on("change", ".packagecategory", function (event) {

    var categoryId = $(this).val();
    var category = $(this);

    // Populate dropdown with list of subcategories
    $(this).parent().parent().next().find('.packagesubcategory').find('option:gt(0)').remove();
    $.ajax({
        url: baseUrl + "/procurement/getsubcategorylist/" + categoryId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $.each(response, function (key, entry) {
                $(category).parent().parent().next().find('.packagesubcategory').append($('<option></option>').attr('value', entry.id).text(entry.category));
            });
        }
    });

    $(this).parent().parent().next().next().find('.packageproduct').find('option:gt(0)').remove();
});

$("body").on("change", ".packagesubcategory", function (event) {

    var categoryId = $(this).val();
    var category = $(this);

    // Populate dropdown with list of subcategories
    $(this).parent().parent().next().find('.packageproduct').find('option:gt(0)').remove();
    $.ajax({
        url: baseUrl + "/procurement/getproductlist/" + categoryId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $.each(response, function (key, entry) {
                $(category).parent().parent().next().find('.packageproduct').append($('<option></option>').attr('value', entry.id).text(entry.productName));
            });
        }
    });
});

$("body").on("click", ".savePackage", function (event) {
    $(this).attr('disabled', true);
    var inputArray = [];
    var link = $(this);
    var err = false;
    var shipmentId = $('#shipmentId').val();

    $(this).closest("tr:has(input)").each(function () {

        $('select', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
            } else
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
        });

        $('input', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
            } else
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
        });
    });

    if (err != true) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/procurement/editdeliveryitem",
            type: 'POST',
            data: {data: inputArray},
            success: function (response) {
                $("#packageList").load(baseUrl + "/procurement/getpackagedetails/" + shipmentId);
                $("#infoBoxDetail").load(baseUrl + "/procurement/getprocurementdetails/" + shipmentId);
            }
        });

        $(this).hide();
        $(this).prev().show();
    }

});

$("body").on("click", ".removePackage", function (event) {
    var id = $(this).attr('data-id');
    var shipmentId = $('#shipmentId').val();

    $.ajax({
        url: baseUrl + "/procurement/deletedeliveryitem/" + id,
        type: 'GET',
        success: function (response) {
            if (response == 1) {
                $("#packageList").load(baseUrl + "/procurement/getpackagedetails/" + shipmentId);
                $("#infoBoxDetail").load(baseUrl + "/procurement/getprocurementdetails/" + shipmentId);
            }
        }
    });
});

$("body").on("change", ".itemcategory", function (event) {

    var categoryId = $(this).val();
    var category = $(this);

    // Populate dropdown with list of subcategories
    $(this).parent().next().find('.itemsubcategory').find('option:gt(0)').remove();
    $.ajax({
        url: baseUrl + "/procurement/getsubcategorylist/" + categoryId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $.each(response, function (key, entry) {
                $(category).parent().next().find('.itemsubcategory').append($('<option></option>').attr('value', entry.id).text(entry.category));
            });
        }
    });

    $(this).parent().next().next().find('.itemproduct').find('option:gt(0)').remove();
});

$("body").on("change", ".itemsubcategory", function (event) {

    var categoryId = $(this).val();
    var category = $(this);

    // Populate dropdown with list of subcategories
    $(this).parent().next().find('.itemproduct').find('option:gt(0)').remove();
    $.ajax({
        url: baseUrl + "/procurement/getproductlist/" + categoryId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $.each(response, function (key, entry) {
                $(category).parent().next().find('.itemproduct').append($('<option></option>').attr('value', entry.id).text(entry.productName));
            });
        }
    });
});

$("body").on("click", ".receivePackage", function (event) {
    $(this).attr('disabled', true);
    var shipmentId = $('#shipmentId').val();

    $.ajax({
        url: $('#receivePkgFrm').attr('action'),
        type: 'POST',
        data: $('#receivePkgFrm').serialize(),
        headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response == 1) {
                    window.location.href = window.location.href;
//                $("#packageList").load(baseUrl + "/procurement/getpackagedetails/" + shipmentId);
//              
//                
//                $("#modal-addEdit").modal('hide');
//                $("#procurementStatus").val('');
            }
        }
    });
});

$(document).ready(function(){
$(".saveProcurementWeight").click(function(){
//$("body").on("click", ".saveProcurementWeight", function (event) {
    var shipmentId = $('#shipmentId').val();

    if ($('#totalWeight').val() != '') {
        $(this).attr('disabled', true);

        $.ajax({
            url: $('#weightdetailsFrm').attr('action'),
            type: 'POST',
            data: $('#weightdetailsFrm').serialize(),
            headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $("#modal-addEdit").modal('hide');
                alert("Kindly check the Paymant Details section for shipping option(s)");
                $("#paymentDetails").load(baseUrl + "/procurement/paymentdetails/" + shipmentId);
            }
        });
    } else {
        alert("Please enter total weight to continue");
        return false;
    }
});
});

$("body").on("click", "#createInvoiceBtn", function (event) {
    $(this).attr('disabled', true);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: $('#invoiceFrm').attr('action'),
        type: 'POST',
        data: $('#invoiceFrm').serialize(),
        success: function (response) {
            window.location.href = window.location.href;
        }
    });
});

$(document).ready(function(){
  var tabName = $("#tabType").val();

  if(tabName != '')
  {
    $('.nav-tabs a[href="#' + tabName + '"]').tab('show');
}else{
    $('.nav-tabs a[href="#shipment"]').tab('show');
}

var accName = $("#accountType").val();

  if(accName != '')
  {
    $('.nav-tabs a[href="#' + accName + '"]').tab('show');
}else{
    $('.nav-tabs a[href="#paid"]').tab('show');
}
  
});

function getShipmentData(tabtype)
{
    $('#tabType').val(tabtype);

    $('.nav-tabs a[href="#' + tabtype + '"]').tab('show');

    $("form#frmsearch").submit();

}

function getAccountData(accountType)
{
    $('#accountType').val(accountType);

    $('.nav-tabs a[href="#' + accountType + '"]').tab('show');

    $("form#frmsearch").submit();

}


function setDefault(defaultType) {
    if ($('#addressBook').val() != '') {
        $('#isDefaultType').val(defaultType);
        $('#saveaddressbook').submit();
    } else {
        alert('Select an address');
        return false;
    }
}

function storeChanged(storeId)
{
    $('#siteCategoryId').find('option:gt(0)').remove();
    if (storeId != '') {
        var url = baseUrl + "/shipments/getcategorylist/" + storeId;
        // Populate dropdown with list of provinces
        $.getJSON(url, function (data) {
            $.each(data, function (key, entry) {
                $('#siteCategoryId').append($('<option></option>').attr('value', entry.id).text(entry.categoryName));
            })
        });

    }

}

function saveComment(shipmentId)
{
    var message = $('#addcomment').val();
    $.ajax({
        url: baseUrl + "/procurement/addcomment/" + shipmentId,
        type: 'POST',
        data: {comment: message},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response == '1')
            {
                $('#showmsg').html('<div class="success">Notes saved successfully</div>');
            } else {
                $('#showmsg').html('<div class="danger">Notes not saved</div>')
            }
        }
    });
}


function exportall_submit() {
    if ($('#exportAll input[type=checkbox]:checked').length) {
        document.getElementById("exportAll").submit();
        return false;
    }
    else {
        $("#errorTxt").css("display", "block");
        $("#errorTxt").text("Select atleast one field");
    }

}

function exportallshipment_submit() {
    if ($('#exportAll input[type=checkbox]:checked').length) {
        document.getElementById("exportAll").submit();
        return false;
    }
    else {
        $("#errorTxt").css("display", "block");
        $("#errorTxt").text("Select atleast one field");
    }

}

function exportallStores()
{
    var _token = $("input[name='_token']").val();
    var baseUrl = $("#baseUrl").val();
    $("#wait").show();
    $.ajax({
             url: baseUrl+"/report/storehistory/exportAllStores/1",
             type: 'POST',
             data: {_token: _token},
             success: function (data) {
                 window.location = data.path;
                 $("#wait").hide();
             }
        });
}

function exportallReferral()
{
    $("#wait").show();
    var baseUrl = $("#baseUrl").val();

    $.ajax({
             url: baseUrl+"/report/referral/export",
             type: 'get',
             success: function (data) {
                 window.location = data.path;
                 $("#wait").hide();
             }
        });
}

function exportallCustomer()
{
    $("#wait").show();
    var baseUrl = $("#baseUrl").val();

    $.ajax({
             url: baseUrl+"/report/customer/export",
             type: 'get',
             success: function (data) {
                 window.location = data.path;
                 $("#wait").hide();
             }
        });
}

function exportallAccount()
{
    $("#wait").show();
    var baseUrl = $("#baseUrl").val();

    $.ajax({
        url: baseUrl+"/report/account/export",
        type: 'get',
        success: function (data) {
            $("#wait").hide();
            window.location = data.path;
        }
   });
}

function exportallRecord()
{
    var baseUrl = $("#baseUrl").val();

    $("#wait").show();

    $.ajax({
             url: baseUrl+"/report/warehouse-tracking/export",
             type: 'get',
             success: function (data) {
                 window.location = data.path;
                 $("#wait").hide();
             }
        });

}

function checkSelectRecordshipment() {
    //var selected = [];
    if ($("input[name=checkboxselected]:checked").length > 0) {
        //selected.push($(this).attr('name'));
        $('#modal-export-selected').modal('show');
    } else {
        alert('Please select record');
    }
}

function exportselectedshipment_submit() {

    if ($('.selecteField:checked').length) {
        var selectedField = [];
        $('.selecteField:checked').each(function () {
            selectedField.push($(this).attr('value'));
        });

        var selected = [];
        $('#checkboxes input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
        if (selected == '') {
            setTimeout(function () {
                $('#modal-export-selected').modal('show');
            }, 1000);
            $("#errorTxtse").css("display", "block");
            $("#errorTxtse").text("Select atleast one user");
        } else {
            
            $(document).ready(function () {
                var _token = $("input[name='_token']").val();
                var baseUrl = $("#baseUrl").val();
                $.ajax({
                    url: baseUrl+"/report/shipment-tracking/exportselected/1",
                    type: 'POST',
                    data: {selected: selected, selectedField: selectedField, _token: _token},
                    success: function (data) {
                        window.location = data.path;
                    }
                });
            });
        }
        return false;
    } else {
        setTimeout(function () {
            $('#modal-export-selected').modal('show');
        }, 1000);
        $("#errorTxtse").css("display", "block");
        $("#errorTxtse").text("Select atleast one field");
        return false;
    }

}

function exportallse_submit() {

    if ($('.selecteField:checked').length) {
        var selectedField = [];
        $('.selecteField:checked').each(function () {
            selectedField.push($(this).attr('value'));
        });

        var selected = [];
        $('#checkboxes input:checked').each(function () {

            selected.push($(this).attr('value'));
        });
        if (selected == '') {
            setTimeout(function () {
                $('#modal-export-selected').modal('show');
            }, 1000);
            $("#errorTxtse").css("display", "block");
            $("#errorTxtse").text("Select atleast one shipment");
        }
        else {
            $(document).ready(function () {
                var _token = $("input[name='_token']").val();
                $.ajax({
                    url: baseUrl + "/report/procurement-tracking/exportselected/1",
                    type: 'POST',
                    data: {selected: selected, selectall: selectedField, _token: _token},
                    success: function (data) {
                        //console.log(data);
                        window.location = data.path;
                    }
                });
            });
        }
        return false;
    }
    else {
        setTimeout(function () {
            $('#modal-export-selected').modal('show');
        }, 1000);
        $("#errorTxtse").css("display", "block");
        $("#errorTxtse").text("Select atleast one field");
        return false;
    }

}


function updateProcurementStatus(status) {
    var selected = [];
    var shipmentstatus = [];
    var shipmentId = $('#shipmentId').val();

    if (status == '') {
        alert("Please select status to continue.");
        return false;
    }

    $('#packageList input:checked').each(function () {
        shipmentstatus.push($(this).attr('data-attr'));
        selected.push($(this).attr('value'));
    });
    if (checkForDuplicates(shipmentstatus) == 1) {

        if (shipmentstatus[0] == 'submitted') {
            var statusArr = ["orderplaced", "unavailable"];
            if (jQuery.inArray(status, statusArr) < 0) {
                alert("Please update status to Order Placed or Unavailable");
                $("#procurementStatus").val('');
                return false;
            }
        } else if (shipmentstatus[0] == 'orderplaced') {
            var statusArr = ["received"];

            if (jQuery.inArray(status, statusArr) < 0) {
                alert("Please specify if Package has been received");
                $("#procurementStatus").val('');
                return false;
            }
        } else if (shipmentstatus[0] == 'unavailable' || shipmentstatus[0] == 'received') {
            alert("Status cannot be further updated");
            $("#procurementStatus").val('');
            return false;
        }

        var checkedIds = selected.join('^');
        if (checkedIds != '') {
            var r = confirm("All the selected items will be modifed. This action cannot be reversed back!!");

            if (r == true) {
                $('#checkedval').val(checkedIds);
                if (status != 'received') {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: baseUrl + "/procurement/updateprocurementitemstatus",
                        type: 'POST',
                        data: {procurementItemIds: checkedIds, status: status},
                        success: function (response) {
                            if (response == 1) {
                                $("#packageList").load(baseUrl + "/procurement/getpackagedetails/" + shipmentId);
                                $("#procurementStatus").val('');
                            }
                        }
                    });
                } else {
                    showAddEdit(checkedIds, status, 'procurement/receivepackage');
                }


            }
        } else {
            alert("Select atleast one field");
            return false;
        }
    } else {
        alert('Please select procurements having same status!!');
    }
}

function checkForDuplicates(arr) {
    var x = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (x != arr[i]) {
            return 0;
        }
    }
    return 1;
}


function updateProcurementStatu1s(status) {
    var selected = [];
    var shipmentId = $('#shipmentId').val();

    if (status == '') {
        alert("Please select status to continue.");
        return false;
    }

    $('#packageList input:checked').each(function () {

        selected.push($(this).attr('value'));
    });
    var checkedIds = selected.join('^');
    if (checkedIds != '') {
        var r = confirm("All the selected items will be modifed. This action cannot be reversed back!!");

        if (r == true) {
            $('#checkedval').val(checkedIds);
            if (status != 'received') {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: baseUrl + "/procurement/updateprocurementitemstatus",
                    type: 'POST',
                    data: {procurementItemIds: checkedIds, status: status},
                    success: function (response) {
                        if (response == 1) {
                            $("#packageList").load(baseUrl + "/procurement/getpackagedetails/" + shipmentId);
                            $("#procurementStatus").val('');
                        }
                    }
                });
            } else {
                showAddEdit(checkedIds, status, 'procurement/receivepackage');
            }


        }
    } else {
        alert("Select atleast one field");
        return false;
    }

}


function deleteSelected() {
    var selected = [];
    $('#checkboxes input:checked').each(function () {

        selected.push($(this).attr('value'));
    });
    var checkedIds = selected.join('^');
    if (checkedIds != '') {
        var r = confirm("All the selected items will be deleted. This action cannot be reversed back!!");

        if (r == true) {
            $('#checkedval').val(checkedIds);
            $('#frmCheckedItem').attr('action', baseUrl + "/procurementfees/deleteall");
            $('#frmCheckedItem').submit();
        }
    } else {
        alert("Select atleast one field");
        return false;
    }

}

function calculateCost() {
    var shipmentId = $('#shipmentId').val();

    $.ajax({
        url: baseUrl + "/procurement/getprocurementcostdetails/" + shipmentId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: "POST",
                data: {weightCalculated: response.weightCalculated, totalWeight: response.totalWeight, totalQuantity: response.totalQuantity, totalItems: response.totalItems},
                url: baseUrl + "/procurement/procurementweightdetails/" + shipmentId,
            }).done(function (response) {
                $('#modal-addEdit').html(response);
                $('#modal-addEdit').modal({backdrop: 'static', keyboard: false});
                $('#modal-addEdit').modal('show');
            });
        }
    });


}

function sendCustomerInvoice(procurementId) {
    $.ajax({
        url: baseUrl + "/procurement/sendcustomerinvoice/" + procurementId,
        type: 'GET',
        success: function (response) {
            alert('Shipping cost has been sent successfully to the customer');
            window.location.href = window.location.href;
        }
    });
}

function notifyCustomer() {
    var shipmentId = $('#shipmentId').val();
    var messageId = $('#shipmentMessage').val();

    if (messageId != '')
        showAddEdit(messageId, shipmentId, 'procurement/notifycustomer');
    else
        alert("Please select a message to continue.");
}

function showShipmentModal() {
    var selected = [];
    var shipmentstatus = [];
    var shipmentId = $('#shipmentId').val();

    $('#packageList input:checked').each(function () {
        shipmentstatus.push($(this).attr('data-attr'));
        selected.push($(this).attr('value'));
    });
    if (checkForDuplicates(shipmentstatus) == 1) {
        if (shipmentstatus[0] != 'received') {
            alert('Please select procurements that have been received!!');
            return false;
        } else {
            var checkedIds = selected.join('^');
            if (checkedIds != '') {
                showAddEdit(checkedIds, shipmentId, 'procurement/getcreateshipmentoptions');
            }
        }
    } else {
        alert('Please select procurements having same status!!');
        return false;
    }

}

function createShipmentOption(optionId) {
    $('#userShipmentId').find('option:gt(0)').remove();
    if (optionId == 'existing') {
        var userId = $('#userId').val();
        $.ajax({
            url: baseUrl + "/procurement/getunpaidshipmentlist/" + userId,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                $.each(response, function (key, entry) {
                    $('#userShipmentId').append($('<option></option>').attr('value', entry.id).text('#Shipment-' + entry.id));
                });
                $('#shipmentDiv').removeClass('hide');
            }
        });

    } else {
        $('#shipmentDiv').addClass('hide');
    }
}

function createnewshipment(){
    var shipmentId = $('#shipmentId').val();
      $.ajax({
            url: baseUrl + "/procurement/createprepaidshipment/" + shipmentId,
            type: 'GET',
            success: function (response) {
                window.location.href = window.location.href;
            }
        });
}

function updatePaymentStatus(invoiceId, paymentStatus) {
    $.ajax({
        url: baseUrl + "/procurement/updatepaymentstatus/" + invoiceId + '/' + paymentStatus,
        type: 'GET',
        success: function (response) {
            window.location.href = window.location.href;
        }
    });
}

function updateProcurementPayment(paymentStatus) {
    var shipmentId = $('#shipmentId').val();

    $.ajax({
        url: baseUrl + "/procurement/updateprocurementpayment/" + shipmentId + '/' + paymentStatus,
        type: 'GET',
        success: function (response) {
            window.location.href = window.location.href;
        }
    });
}


function renewEmail(userId) {
    $.ajax({
        url: baseUrl + "/report/renewemail",
        data: {userId: userId},
        type: 'POST',
        success: function (response) {
        }
    });
}

