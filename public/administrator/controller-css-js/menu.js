function displayPanel(value) {
    if (value == 1)
        $('#menuPannelDiv').show();
    else
        $('#menuPannelDiv').hide();

}

function pageType(value) {
    if (value == 1) {
        $('#menuLinkDev').show();
        $('#pageListDiv').hide();
        $('#blockListDiv').hide();
    } else if (value == 2) {
        $('#menuLinkDev').show();
        $('#blockListDiv').hide();
        $('#pageListDiv').hide();
    } else
    {
        $('#menuLinkDev').hide();
        $('#pageListDiv').hide();
        $('#blockListDiv').show();
    }

}