
$(function(){
    //tooltip
            $('[data-toggle="tooltip"]').tooltip();
  
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
            $('input[type="checkbox"].flat-red').on('ifChecked', function (event) {
                $(this).val(1); // alert value
                $(this).attr('checked', 'true');
            });
            $('input[type="checkbox"].flat-red').on('ifUnchecked', function (event) {
                $(this).val(0);
            });

});
            
            
$(document).ready(function() {
    $("#add").click(function() {
        var lastField = $("#buildyourform div:last");
        var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
        var fieldWrapper = $("<div class=\"fieldwrapper\" id=\"field" + intId + "\"/>");
        fieldWrapper.data("idx", intId);

        

        //var fName = $("<input type=\"text\" class=\"fieldname\" />");

        var base_path_loadata = $("input[name='base_path_loadata']").val(); 
        var token_csrf = $("input[name=_token]").val();

        $.ajax({
           url : base_path_loadata,
           method : "POST",
           data : {_token:token_csrf},
           dataType : "text",
           success : function (data)
           {
              if(data != '') 
              {
                  //$('#remove-row').remove();
                  //$('#load-data').append(data);
                  var fType = data;
                  fieldWrapper.append(fType);
                  var inputPhone = $("");
                  var removeButton = $("");
                  removeButton.click(function() {
                      $(this).parent().remove();
                      intId = intId - 1;
                  });
                  //fieldWrapper.append(inputPhone);
                  //fieldWrapper.append(removeButton);
              }
              else
              {
                  console.log('no data');
              }
           }
       });


        //var fType = $("<select class=\"form-control customSelect2\" name=\"countryCode\" id=\"countryCode\"><option data-countryCode=\"US\" value=\"1\">USA (+1)</option></select>");
        
        //fieldWrapper.append(fName);
        //fieldWrapper.append(fType);
        
        $("#buildyourform").append(fieldWrapper);
    });



});


function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 46))
        return false;
    return true;
}