$(function () {
    $('ul.Overview li.overview-tab').click(function () {
        $('ul.Overview li.overview-tab').removeClass('active');
        $(this).addClass('active');
        var selectedLi = $(this).find('a').attr('href').substr(1);

        if (selectedLi == 'customer-chart')
            $('.myDropDn').hide();
        else
            $('.myDropDn').show();

        var dateRange = $('#overview_range span').html();
        var dateParts = dateRange.split(" - ");
        var startParts = dateParts[0].split('/');
        var endParts = dateParts[1].split('/');
        var startDate = new Date(startParts[1] + "," + startParts[0] + "," + startParts[2]);
        var endDate = new Date(endParts[1] + "," + endParts[0] + "," + endParts[2]);
        populateGraphs('overview', selectedLi, moment(startDate), moment(endDate));
    });

    $('ul.service-overview li.service-overview-tab').click(function () {
        $('ul.service-overview li.service-overview-tab').removeClass('active');
        $(this).addClass('active');
        var liId = $(this).find('a').attr('href').substr(1);
        var dateRange = $('#service_overview_range span').html();
        var dateParts = dateRange.split(" - ");
        var startParts = dateParts[0].split('/');
        var endParts = dateParts[1].split('/');
        var startDate = new Date(startParts[1] + "," + startParts[0] + "," + startParts[2]);
        var endDate = new Date(endParts[1] + "," + endParts[0] + "," + endParts[2]);
        populateGraphs('service_overview', liId, moment(startDate), moment(endDate));
    });

    $('ul.country-filter-overview li a').click(function () {
        var selectedLiCountry = $(this).text();
        $(this).parents('.myDropDn').find('button').html(selectedLiCountry + '<span class="caret skybg m-l-10"></span>');
        var selectedLi = $('ul.Overview li.active a').attr('href').substr(1);
        var dateRange = $('#overview_range span').html();
        var dateParts = dateRange.split(" - ");
        var startParts = dateParts[0].split('/');
        var endParts = dateParts[1].split('/');
        var startDate = new Date(startParts[1] + "," + startParts[0] + "," + startParts[2]);
        var endDate = new Date(endParts[1] + "," + endParts[0] + "," + endParts[2]);
        populateGraphs('overview', selectedLi, moment(startDate), moment(endDate));
    });

    $('ul.procurementShipmentNav li').click(function () {
        $('ul.procurementShipmentNav li').removeClass('active');
        $(this).addClass('active');
    });



    $('ul.service-date-filter li a').click(function () {
        var selectedLiOrderwiseDate = $(this).text();
        $(this).parents('.myDropDn').find('button').html(selectedLiOrderwiseDate + '<span class="caret skybg m-l-10"></span>');
        var selectedSection = $(this).parents('.service-overview').find('li.active a').attr('href');
        overviewBarchart(selectedSection.substr(1), selectedLiOrderwiseDate);
    });

    /*Shipments count function call on document ready*/

    showShipments('shopforme');

    /*Shipments count function call on document ready*/
});
var baseUrl = $('#baseUrl').val();

var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
var d = new Date();

function changeOverview(searchKey)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: baseUrl + "/dashboard/getSearchOption/" + searchKey,
        type: 'POST',
        dataType: "json",
        success: function (response) {
            $('#changeFilter li').remove();
            $.each(response, function (i, entry) {
                $('#changeFilter').append($('<li></li>').html('<a href="#" onclick="selectWarehouse(' + entry.id + ')">' + entry.countryName + '</a>'));
            });
        }
    });
}

function selectCountry(searchId)
{
    $('#selectedfilterval').val(searchId);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: baseUrl + "/dashboard/getCountryName/" + searchId,
        type: 'POST',
        dataType: "json",
        success: function (response) {
            $('#filterBtn').html(response[0].name + '<span class="caret skybg m-l-10"></span>');
        }
    });
    cb(start, end);
}

function selectWarehouse(searchId)
{
    $('#selectedfilterval').val(searchId);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: baseUrl + "/dashboard/getWarehouseName/" + searchId,
        type: 'POST',
        dataType: "json",
        success: function (response) {
            $('#filterBtn').html(response[0].name +'('+response[0].cityName+ ')<span class="caret skybg m-l-10"></span>');
        }
    });
    cb(start, end)
}
/* Revenue line graph config */

var config = {
    type: 'line',
    data: {
        labels: getMonths(d.getFullYear()),
        datasets: [{
                label: 'Total sales',
                backgroundColor: window.chartColors.red,
                borderColor: window.chartColors.blue,
                data: [10, 45, 21, 35, 15, 45, 50],
                fill: false,
            }]
    },
    options: {
        responsive: true,
        title: {
            display: true,
            text: 'This year revenue'
        },
        legend: {
            onHover: function (e) {
                e.target.style.cursor = 'pointer';
            }
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true,
            onHover: function (e) {
                var point = this.getElementAtEvent(e);
                if (point.length)
                    e.target.style.cursor = 'pointer';
                else
                    e.target.style.cursor = 'default';
            }
        },
        scales: {
            xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: false,
                        labelString: 'Month'
                    }
                }],
            yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Revenue Generate'
                    }
                }]
        }
    }
};

/* config end */

/* dynamic date config */

var xAxisLabel = '';
function getMonths(year)
{
    var monthsThisYear = [];
    for (var i = 0; i < 12; i++)
    {
        monthsThisYear.push(MONTHS[i]);
        if (i == d.getMonth())
        {
            break;
        }
    }

    return monthsThisYear;
}

function getChartLabels(dateFilter)
{
    dateFilter = dateFilter.trim();
    chartLabels = [];
    if (dateFilter == 'This Year') {
        chartLabels = getMonths(d.getFullYear());
        xAxisLabel = 'Month';
    }
    else if (dateFilter == 'Previous Year') {
        chartLabels = MONTHS;
        xAxisLabel = 'Month';
    }
    else if (dateFilter == 'This Month' || dateFilter == 'Previous Month') {
        chartLabels = ['Week 1', 'Week 2', 'Week 3', 'Week 4'];
        xAxisLabel = 'Week';
    }
    else {
        var firstday = new Date(d.setDate(d.getDate() - d.getDay()));
        var lastday = new Date(d.setDate(d.getDate() - d.getDay() + 6));
        while (firstday <= lastday) {
            chartLabels.push((firstday.getMonth() + 1) + ' / ' + firstday.getDate())
            var newDate = firstday.setDate(firstday.getDate() + 1);
            firstday = new Date(newDate);
        }
        xAxisLabel = 'Day';
    }

    return true;

}
/* End */



/* Date Range picker */

var start = moment().subtract(29, 'days');
var end = moment();
var curDate = new Date();
var curyear = curDate.getFullYear();
var curyearMonth = curDate.getMonth() + 1;
var curyearDay = curDate.getDate();
var lastYear = curyear - 1;


//alert(lastDate);

/* overview daterange */
function cb(start, end) {
    $('#overview_range span').html(start.format('D/M/YYYY') + ' - ' + end.format('D/M/YYYY'));
    var selectedLi = $('ul.Overview li.active a').attr('href');

    if (selectedLi != undefined)
    {
        populateGraphs('overview', selectedLi.substr(1), start, end);
    }
    else
    {
        $('ul.Overview li:first').addClass('active');
        populateGraphs('overview', 'revenue-chart', start, end);
    }


}

$('#overview_range').daterangepicker({
    startDate: start,
    endDate: end,
    ranges: {
        'This Year': [moment().startOf('year'), moment().endOf('year')],
        'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
}, cb);

cb(start, end);
/* End */

/* dynamic overview line charts */
function overviewCharts(selectedLi, startDate, endDate) {
    var selectedLiCountry = $('.country-filter-overview').parents('.myDropDn').find('button').text();
    var ctx = document.getElementById(selectedLi + '-data').getContext('2d');
    var searchId = $('#selectedfilterval').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: baseUrl + "/dashboard/getChartsRecords/" + selectedLi,
        type: 'POST',
        dataType: "json",
        data: {"startDate": startDate.format(), "endDate": endDate.format(), "searchId": searchId},
        success: function (response) {
            config.data.labels = chartLabels;
            if (selectedLi == 'revenue-chart')
            {
                config.options.title.text = "Revenue";
                config.data.datasets = [];
                config.data.datasets.push({
                    label: 'Total Revenue',
                    behaveLikeLine: true,
                    backgroundColor: '#009954',
                    borderColor: window.chartColors.blue,
                    data: response.data,
                    fill: false,
                });
                config.options.scales.yAxes[0] = {
                    ticks: {
                        // Include a dollar sign in the ticks
                        callback: function (value, index, values) {
                            return response.currency + " " + value.toFixed(2);
                        }
                    }
                };

                config.options.tooltips = {
                    callbacks: {
                        label: function (tooltipItems, data) {
                            return response.currency + " " + tooltipItems.yLabel.toString();
                        }
                    }
                };
            }
            else if (selectedLi == 'shipment-chart')
            {
                config.options.title.text = "Shipments";
                config.data.datasets = [];
                config.data.datasets.push({
                    label: 'Total paid shipments',
                    behaveLikeLine: true,
                    backgroundColor: '#009954',
                    borderColor: window.chartColors.green,
                    data: response.data,
                    fill: false,
                });

                config.options.scales.yAxes[0] = {
                    ticks: {
                        beginAtZero: true,
                        stepSize: 20,
                        // Include a dollar sign in the ticks
                        callback: function (value, index, values) {
                            return value;
                        }
                    }
                };

                config.options.tooltips = {
                    callbacks: {
                        label: function (tooltipItems, data) {
                            return tooltipItems.yLabel;
                        }
                    }
                };

                //window.myLine.update();
            }
            else if (selectedLi == 'customer-chart')
            {
                config.options.title.text = "Customers";
                config.data.datasets = [];
                config.data.datasets.push({
                    label: 'Total new customers',
                    behaveLikeLine: true,
                    backgroundColor: '#a41b08',
                    borderColor: window.chartColors.orange,
                    data: response.data,
                    fill: false,
                });
                config.options.scales.yAxes[0] = {
                    ticks: {
                        beginAtZero: true,
                        stepSize: 20,
                        // Include a dollar sign in the ticks
                        callback: function (value, index, values) {
                            return value;
                        }
                    }
                };

                config.options.tooltips = {
                    callbacks: {
                        label: function (tooltipItems, data) {
                            return tooltipItems.yLabel;
                        }
                    }
                };



                //  window.myLine.update();
            }

            if (typeof myLine === 'undefined')
                window.myLine = new Chart(ctx, config);
            else
                myLine.update();

            Chart.plugins.register({
                afterDraw: function (chart) {
                    if (myLine.data.datasets.length === 0) {
                        // No data is present
                        var ctx = chart.chart.ctx;
                        var width = chart.chart.width;
                        var height = chart.chart.height
                        chart.destroy();

                        ctx.save();
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';
                        ctx.font = "16px normal 'Helvetica Nueue'";
                        ctx.fillText('No data to display', width / 2, height / 2);
                        ctx.restore();
                    }
                }
            });



            document.getElementById(selectedLi + '-data').onclick = function (evt) {
                var activePoint = myLine.getElementAtEvent(evt)[0];
                var data = activePoint._chart.data;
                var datasetIndex = activePoint._datasetIndex;
                var index = activePoint._index;
                var serviceType = data.datasets[datasetIndex].label;
                var month = data.labels[index];
                if (selectedLi == 'revenue-chart')
                    location.href = 'javascript:void(0)';
                else if (selectedLi == 'shipment-chart')
                    location.href = 'javascript:void(0)';
                else
                    location.href = 'javascript:void(0)';
            };
        }
    });
}

/* line chart end */

var chartLabels = [];
function populateGraphs(section, subSection, startDate, endDate)
{
    var labels = getLabels(startDate, endDate);
    if (section == 'overview')
        overviewCharts(subSection, startDate, endDate);

    else if (section == 'service_overview')
        overviewBarchart(subSection, startDate, endDate);

}

function getLabels(startDate, endDate)
{
    //console.log(startDate.format('MMMM D, YYYY'));
    chartLabels = [];
    dayDifference = endDate.diff(startDate, 'days');
    //console.log(dayDifference);
    var currDate = moment(startDate).startOf('day');
    var lastDate = moment(endDate).startOf('day');
    if (dayDifference <= 7)
    {
        while (currDate.diff(lastDate) <= 0) {
            chartLabels.push(parseInt(currDate.month() + 1) + ' / ' + currDate.date());
            currDate.add(1, 'days');
            //dates.push(currDate.clone().toDate());
        }
    }
    else if (dayDifference > 7 && dayDifference <= 30)
    {
        while (lastDate.diff(currDate, 'days') >= 7) {
            //console.log(lastDate.diff(currDate,'days'));
            var endOfWeek = moment(currDate);
            endOfWeek.add(6, 'days');
            var label = parseInt(currDate.month() + 1) + ' / ' + currDate.date() + ' - ' + parseInt(endOfWeek.month() + 1) + ' / ' + endOfWeek.date()
            chartLabels.push(label);
            currDate.add(7, 'days');
        }
        chartLabels.push(parseInt(currDate.month() + 1) + ' / ' + currDate.date() + ' - ' + parseInt(lastDate.month() + 1) + ' / ' + lastDate.date());

    }
    else if (dayDifference > 30 && dayDifference < 365)
    {
        while (lastDate.diff(currDate, 'months') > 0) {
            chartLabels.push(currDate.format('MMMM'));
            currDate.add(1, 'months');
        }
        chartLabels.push(lastDate.format('MMMM'));
    }
    return true;

}


//////////////////////////////////////////////////////////// LINE CHART END/////////////////////////////////////////



function customerDounoughtChart(startDate, endDate) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: baseUrl + "/dashboard/gettopCustomers/",
        type: 'POST',
        dataType: "json",
        data: {"startDate": startDate.format(), "endDate": endDate.format()},
        success: function (response) {

            if (!jQuery.isEmptyObject(response)) {
                var ctx = document.getElementById('countrywise-customer').getContext('2d');

                var countries = [];
                var users = [];

                $.each(response, function (k, v)
                {
                    countries.push(v.name);
                    users.push(v.maxuser);
                });

                window.myDoughnut = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        datasets: [{
                                data: users,
                                backgroundColor: [
                                    "#a41b08",
                                    "#f66955",
                                    "#537b25",
                                    "#00a65a",
                                    "#6475e1",
                                    "#f39c11",
                                    "#303e3f",
                                    "#00c0ef",
                                    "#9761ad",
                                    "#3c8dbc",
                                ],
                                label: 'Dataset 1'
                            }],
                        labels: countries
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'right',
                            onHover: function (e) {
                                e.target.style.cursor = 'pointer';
                            }
                        },
                        title: {
                            display: false,
                            text: 'Chart.js Doughnut Chart'
                        },
                        animation: {
                            animateScale: true,
                            animateRotate: true
                        },
                        hover: {
                            onHover: function (e) {
                                var point = this.getElementAtEvent(e);
                                if (point.length)
                                    e.target.style.cursor = 'pointer';
                                else
                                    e.target.style.cursor = 'default';
                            }
                        }

                    }
                });
            } else {
                Chart.plugins.register({
                    afterDraw: function (chart) {
                        if (myDoughnut.data.datasets.length === 0) {
                            // No data is present
                            var ctx = chart.chart.ctx;
                            var width = chart.chart.width;
                            var height = chart.chart.height
                            chart.destroy();

                            ctx.save();
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';
                            ctx.font = "16px normal 'Helvetica Nueue'";
                            ctx.fillText('No data to display', width / 2, height / 2);
                            ctx.restore();
                        }
                    }
                });

                var ctx = document.getElementById('countrywise-customer').getContext('2d');
                window.myDoughnut = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: ['Jan', 'Feb', 'Mar', 'Apr'],
                        datasets: []
                    }
                });
                myDoughnut.update();
            }
        }
    });
}



/* top 10 customer */
function cbCustomer(start, end) {
    $('#customers_range span').html(start.format('D/M/YYYY') + ' - ' + end.format('D/M/YYYY'));
    customerDounoughtChart(start, end);

}

$('#customers_range').daterangepicker({
    startDate: start,
    endDate: end,
    ranges: {
        'This Year': [moment().startOf('year'), moment().endOf('year')],
        'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        //'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
}, cbCustomer);
cbCustomer(start, end);

/* End */

/* Doughnut chart start */

var randomScalingFactor = function () {
    return Math.round(Math.random() * 100);
}

/////////////////////////////////////////////////////* Doughnut chart end *///////////////////////////////////////////////

/* Shipment status date range */

function showProcurement(keyword)
{
    $('#showShipmentType').val(keyword);
    $('#tblchoice').val('procurement');

    if (keyword == 'shopforme')
    {

        $('#procurementShipment ul li').remove();
        $('#procurementShipment').show();
        $('#processedShipment').hide();
        $('#autoShipment').hide();
        $('#autopartsShip').hide();
        $('#shopformeShip').hide();
    }
    else if (keyword == 'autopart')
    {

        $('#processedShipment ul li').remove();
        $('#processedShipment').show();
        $('#procurementShipment').hide();
        $('#autoShipment').hide();
        $('#autopartsShip').hide();
        $('#shopformeShip').hide();
    }
    else {
        $('#autoShipment ul li').remove();
        $('#autoShipment').show();
        $('#processedShipment').hide();
        $('#procurementShipment').hide();
        $('#autopartsShip').hide();
        $('#shopformeShip').hide();
    }

    $('#shipment_status_range').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'This Year': [moment().startOf('year'), moment().endOf('year')],
            'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cbShipmentStatus);
    cbShipmentStatus(start, end);

}

function showShipments(keyword)
{
    $('#showShipmentType').val(keyword);
    $('#tblchoice').val('');

    if (keyword == 'shopforme')
    {
        $('#shopformeShip ul li').remove();
        $('#shopformeShip').show();
        $('#autopartsShip').hide();
        $('#autoShipment').hide();
        $('#processedShipment').hide();
        $('#procurementShipment').hide();

    }
    else if (keyword == 'autopart')
    {
        $('#autopartsShip ul li').remove();
        $('#autopartsShip').show();
        $('#shopformeShip').hide();
        $('#autoShipment').hide();
        $('#processedShipment').hide();
        $('#procurementShipment').hide();
    }

    $('#shipment_status_range').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'This Year': [moment().startOf('year'), moment().endOf('year')],
            'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cbShipmentStatus);
    cbShipmentStatus(start, end);
}

function cbShipmentStatus(startDate, endDate) {
    var keyword = $('#showShipmentType').val();
    var tblchoice = $('#tblchoice').val();

    $('#shipment_status_range span').html(startDate.format('D/M/YYYY') + ' - ' + endDate.format('D/M/YYYY'));

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: baseUrl + "/dashboard/getShipmentStatus/",
        type: 'POST',
        dataType: "json",
        data: {"startDate": startDate.format(), "endDate": endDate.format(), "keyword": keyword, "tblchoice": tblchoice},
        success: function (response) {
            $("#shopformeShip .airShip").html("");
            $("#autopartsShip .airShip").html("");
            $("#procurementShipment .airShip").html("");
            $("#processedShipment .airShip").html("");
            $('#autoShipment .airShip').html("");

            $.each(response.statusname, function (key, value) {
                if (keyword == 'shopforme' && tblchoice == '') {
                    $("#shopformeShip .airShip").append("<li><div class='airShipBox " + response.shipmentClassname[key] + "'><span class='borairCir'><a href='../administrator/shipments' class='airCir'><img src=" + $(location).attr('protocol') + "//" + $(location).attr("hostname") + "/stmd/public/administrator/img/icon01.png  width='58' height='58'></a> </span><p class='num'><a href='javascript:void(0);'>" + value + "</a></p><p class='tag'>" + key + "</p></div></li>");
                }
                if (keyword == 'autopart' && tblchoice == '') {
                    $("#autopartsShip .airShip").append("<li><div class='airShipBox " + response.shipmentClassname[key] + "'><span class='borairCir'><a href='../administrator/shipments' class='airCir'><img src=" + $(location).attr('protocol') + "//" + $(location).attr("hostname") + "/stmd/public/administrator/img/icon01.png width='58' height='58'></a> </span><p class='num'><a href='javascript:void(0);'>" + value + "</a></p><p class='tag'>" + key + "</p></div></li>");
                }
                if (keyword == 'autopart' && tblchoice != '') {
                    keyVal = capitalizeFirstLetter(key);
                    $("#processedShipment .airShip").append("<li><div class='airShipBox " + response.shipmentClassname[key] + "'><span class='borairCir'><a href='../administrator/autoparts' class='airCir'><img src=" + $(location).attr('protocol') + "//" + $(location).attr("hostname") + "/stmd/public/administrator/img/icon01.png width='58' height='58'></a> </span><p class='num'><a href='javascript:void(0);'>" + value + "</a></p><p class='tag'>" + keyVal + "</p></div></li>");
                }
                if (keyword == 'shopforme' && tblchoice != '') {
                    keyVal = capitalizeFirstLetter(key);
                    $("#procurementShipment .airShip").append("<li><div class='airShipBox " + response.shipmentClassname[key] + "'><span class='borairCir'><a href='../administrator/procurement' class='airCir'><img src=" + $(location).attr('protocol') + "//" + $(location).attr("hostname") + "/stmd/public/administrator/img/icon01.png width='58' height='58'></a> </span><p class='num'><a href='javascript:void(0);'>" + value + "</a></p><p class='tag'>" + keyVal + "</p></div></li>");
                }
                if (keyword == 'buycarforme' && tblchoice != '') {
                    keyVal = capitalizeFirstLetter(key);
                    $("#autoShipment .airShip").append("<li><div class='airShipBox " + response.shipmentClassname[key] + "'><span class='borairCir'><a href='../administrator/autopickup' class='airCir'><img src=" + $(location).attr('protocol') + "//" + $(location).attr("hostname") + "/stmd/public/administrator/img/icon01.png width='58' height='58'></a> </span><p class='num'><a href='javascript:void(0);'>" + value + "</a></p><p class='tag'>" + keyVal + "</p></div></li>");
                }
            });

        }

    });
}



/* End */

/* Order by destination country date range */

function cbOrderCountry(start, end) {
    $('#order_destinationcountry_range span').html(start.format('D/M/YYYY') + ' - ' + end.format('D/M/YYYY'));
    countryDataBarChart(start, end);
}

$('#order_destinationcountry_range').daterangepicker({
    startDate: start,
    endDate: end,
    ranges: {
        'This Year': [moment().startOf('year'), moment().endOf('year')],
        'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
}, cbOrderCountry);
cbOrderCountry(start, end);

/* End */

/* barchart area for destination country start */

var color = Chart.helpers.color;
function countryDataBarChart(startDate, endDate) {
    $('#order-by-country').remove();
    $('#order-destination-country').html('<canvas id="order-by-country" style="height:200px"></canvas>');
    var ctx = document.getElementById('order-by-country').getContext('2d');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: baseUrl + "/dashboard/getOrderRecords/",
        type: 'POST',
        dataType: "json",
        data: {"startDate": startDate.format(), "endDate": endDate.format()},
        success: function (response) {
            var countries = [];
            var orders = [];

            $.each(response, function (k, v)
            {
                countries.push(v.name);
                orders.push(v.maxorder);
            });

            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: {
                    datasets: [{
                            data: orders,
                            backgroundColor: [
                                "#4b9ece",
                                "#76d1da",
                                "#a41b08",
                                "#f66955",
                                "#537b25",
                                "#00a65a",
                                "#6475e1",
                                "#f39c11",
                                "#303e3f",
                                "#00c0ef",
                                "#9761ad",
                                "#3c8dbc",
                            ],
                            label: 'Orders Completed'
                        }],
                    labels: countries
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: true,
                    legend: {
                        position: 'bottom',
                        onHover: function (e) {
                            e.target.style.cursor = 'pointer';
                        }
                    },
                    title: {
                        display: true,
                        text: 'Orders Completed'
                    },
                    hover: {
                        onHover: function (e) {
                            var point = this.getElementAtEvent(e);
                            if (point.length)
                                e.target.style.cursor = 'pointer';
                            else
                                e.target.style.cursor = 'default';
                        }
                    },
                    scales: {
                        xAxes: [{
                                display: true,
                            }],
                        yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Revenue Generate'
                                },
                                ticks: {
                                    beginAtZero: true,
                                    //max:plugin.settings.maxDataValue,
                                    maxTicksLimit: 10
                                }
                            }]
                    }

                }
            });
        }

    });



//    document.getElementById("order-by-country").onclick = function (evt) {
//        var activePoint = myBar.getElementAtEvent(evt)[0];
//        var data = activePoint._chart.data;
//        var datasetIndex = activePoint._datasetIndex;
//        var shipmentType = data.datasets[datasetIndex].label;
//        var country = data.labels[datasetIndex];
//
//        if (shipmentType == 'Air Shipment')
//            location.href = 'order.html?mode=search&shipmentType=air&country=' + country;
//        else
//            location.href = 'order.html?mode=search&shipmentType=ocean&country=' + country;
//    };

}


/////////////////////////////////////////////////////////////* Country  BarChart area end */////////////////////////////////////



////////////////////////////////////////////////////* barchart area for service overview start *//////////////////////////

/* Service overview date range */

function cbServiceOverview(start, end) {
    $('#service_overview_range span').html(start.format('D/M/YYYY') + ' - ' + end.format('D/M/YYYY'));
    var selectedLi = $('ul.service-overview li.active a').attr('href');
    //console.log(selectedLi);
    populateGraphs('service_overview', selectedLi.substr(1), start, end);

}

$('#service_overview_range').daterangepicker({
    startDate: start,
    endDate: end,
    ranges: {
        'This Year': [moment().startOf('year'), moment().endOf('year')],
        'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
}, cbServiceOverview);
cbServiceOverview(start, end);

/* End */

var color = Chart.helpers.color;

function overviewBarchart(tabId, startDate, endDate) {
    $('#Order-overview').remove();
    $('#Shipment-overview').remove();
    $('#Revenue-overview').remove();
    $('.service-overview-div').find('#' + tabId).html('<canvas id="' + tabId + '-overview" height="200px"></canvas>');
    var ctx = document.getElementById(tabId + '-overview').getContext('2d');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    if (tabId == 'Order')
    {
        $.ajax({
            url: baseUrl + "/dashboard/getCompletedOrder/",
            type: 'POST',
            dataType: "json",
            data: {"startDate": startDate.format(), "endDate": endDate.format()},
            success: function (response) {
                var shopformeArray = [];
                var autopartsArray = [];
                var autoshipmentArray = [];

                $.each(response.shopformeOrderList, function (k, v)
                {
                    shopformeArray.push(v.maxorder);
                });
                $.each(response.autopartsOrderList, function (k, v)
                {
                    autopartsArray.push(v.maxorder);
                });
                $.each(response.autoshipmentOrderList, function (k, v)
                {
                    autoshipmentArray.push(v.maxorder);
                });

                window.myBar = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: chartLabels,
                        datasets: [{
                                label: 'Shop For Me',
                                backgroundColor: '#60b7b1',
                                borderColor: '#60b7b1',
                                borderWidth: 1,
                                data: shopformeArray
                            }, {
                                label: 'Auto Parts',
                                backgroundColor: '#ffdb88',
                                borderColor: '#ffdb88',
                                borderWidth: 1,
                                data: autopartsArray
                            }, {
                                label: 'Auto Shipments',
                                backgroundColor: '#00c0ef',
                                borderColor: '#00c0ef',
                                borderWidth: 1,
                                data: autoshipmentArray
                            }]
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                            position: 'bottom',
                            onHover: function (e) {
                                e.target.style.cursor = 'pointer';
                            }
                        },
                        title: {
                            display: true,
                            text: 'Orders Completed'
                        },
                        hover: {
                            onHover: function (e) {
                                var point = this.getElementAtEvent(e);
                                if (point.length)
                                    e.target.style.cursor = 'pointer';
                                else
                                    e.target.style.cursor = 'default';
                            }
                        },
                        scales: {
                            yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                        }

                    }
                });

            }
        });
        document.getElementById(tabId + '-overview').onclick = function (evt) {
            var activePoint = orderBar.getElementAtEvent(evt)[0];
            var data = activePoint._chart.data;
            var datasetIndex = activePoint._datasetIndex;
            var index = activePoint._index;
            var serviceType = data.datasets[datasetIndex].label;
            var month = data.labels[index];

            if (serviceType == 'Auto')
                location.href = 'auto-shipment.html?month=' + month;
            else if (serviceType == 'Procurement')
                location.href = 'procrument-service.html?month=' + month;

        };

    }
    else if (tabId == 'Shipment')
    {
        $.ajax({
            url: baseUrl + "/dashboard/getTotalShipment/",
            type: 'POST',
            dataType: "json",
            data: {"startDate": startDate.format(), "endDate": endDate.format()},
            success: function (response) {
                var shopformeArray = [];
                var autopartsArray = [];
                var autoshipmentArray = [];

                $.each(response.shopformeShipmentList, function (k, v)
                {
                    shopformeArray.push(v.maxshipment);
                });
                $.each(response.autopartsShipmentList, function (k, v)
                {
                    autopartsArray.push(v.maxshipment);
                });
                $.each(response.autoshipmentList, function (k, v)
                {
                    autoshipmentArray.push(v.maxshipment);
                });

                window.myBar = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: chartLabels,
                        datasets: [{
                                label: 'Shop For Me',
                                backgroundColor: '#60b7b1',
                                borderColor: '#60b7b1',
                                borderWidth: 1,
                                data: shopformeArray
                            }, {
                                label: 'Auto Parts',
                                backgroundColor: '#ffdb88',
                                borderColor: '#ffdb88',
                                borderWidth: 1,
                                data: autopartsArray
                            },
                            {
                                label: 'Auto Shipments',
                                backgroundColor: '#00c0ef',
                                borderColor: '#00c0ef',
                                borderWidth: 1,
                                data: autoshipmentArray
                            }]

                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                            position: 'bottom',
                            onHover: function (e) {
                                e.target.style.cursor = 'pointer';
                            }
                        },
                        title: {
                            display: true,
                            text: 'Shipments Completed'
                        },
                        hover: {
                            onHover: function (e) {
                                var point = this.getElementAtEvent(e);
                                if (point.length)
                                    e.target.style.cursor = 'pointer';
                                else
                                    e.target.style.cursor = 'default';
                            }
                        },
                        scales: {
                            yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                        }
                    }
                });

            }
        });


    }
    else if (tabId == 'Revenue')
    {
        $.ajax({
            url: baseUrl + "/dashboard/getTotalRevenue/",
            type: 'POST',
            dataType: "json",
            data: {"startDate": startDate.format(), "endDate": endDate.format()},
            success: function (response) {
                //console.log(response);
                var shopformeArray = [];
                var autopartsArray = [];
                var autoshipmentArray = [];

                $.each(response.shopformeRevenue, function (k, v)
                {
                    shopformeArray.push(v.revenue);
                });
                $.each(response.autopartsRevenue, function (k, v)
                {
                    autopartsArray.push(v.revenue);
                });
                $.each(response.autoshipmentRevenue, function (k, v)
                {
                    autoshipmentArray.push(v.revenue);
                });
                window.myBar = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: chartLabels,
                        datasets: [{
                                label: 'Shop For Me',
                                backgroundColor: '#60b7b1',
                                borderColor: '#60b7b1',
                                borderWidth: 1,
                                data: shopformeArray
                            }, {
                                label: 'Auto Parts',
                                backgroundColor: '#ffdb88',
                                borderColor: '#ffdb88',
                                borderWidth: 1,
                                data: autopartsArray
                            },
                            {
                                label: 'Auto Shipments',
                                backgroundColor: '#00c0ef',
                                borderColor: '#00c0ef',
                                borderWidth: 1,
                                data: autoshipmentArray
                            }]

                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                            position: 'bottom',
                            onHover: function (e) {
                                e.target.style.cursor = 'pointer';
                            }
                        },
                        title: {
                            display: true,
                            text: 'Revenue Genertated'
                        },
                        hover: {
                            onHover: function (e) {
                                var point = this.getElementAtEvent(e);
                                if (point.length)
                                    e.target.style.cursor = 'pointer';
                                else
                                    e.target.style.cursor = 'default';
                            }
                        },
                        scales: {
                            yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                        }
                    }
                });

            }
        });
    }

    document.getElementById(tabId + '-overview').onclick = function (evt) {
        var activePoint = myBar.getElementAtEvent(evt)[0];
        var data = activePoint._chart.data;
        var datasetIndex = activePoint._datasetIndex;
        var index = activePoint._index;
        var serviceType = data.datasets[datasetIndex].label;
        var month = data.labels[index];

        if (serviceType == 'Auto')
            location.href = 'auto-shipment.html?mode=search&date=' + month;
        else if (serviceType == 'Procurement')
            location.href = 'procrument-service.html?mode=search&date=' + month;

    };

}





//////////////////////////////////////////////* barchart area for service overview start end *////////////////////////////

function capitalizeFirstLetter(string) {
    return string[0].toUpperCase() + string.slice(1);
}