$("body").on("change", "#dhlaccountnumber", function (event) {
    if($(this).val()!="")
    {
        $('#dhlaccountid').val($(this).val());
        var accountDesciption = '<p>'+$(this).find('option:selected').attr("data-description")+'</p>';
        accountDesciption += '<p>Please confirm this the DHL account you want to continue with.</p>';

        $('#confirmModal .modal-body').html(accountDesciption);
        $('#confirmModal').modal('show');
    }
});

$("body").on("change", "#termsOfTrade", function (event) {
    console.log("here");
    if($(this).val()!="")
    {
        
        var accountDesciption = '<p>'+$(this).find('option:selected').attr("data-description")+'</p>';
        accountDesciption += '<p>Please confirm this the DHL Terms of Trade you want to continue with.</p>';

        $('#dhl-trade-terms .modal-body').html(accountDesciption);
        $('#dhl-trade-terms').modal('show');
    }
});

/* Item Add -  Calculate Sub Total per Item / Package  on Value Change */
$(document).on('blur', '.product-val', function () {

    var value = $(this).val();
    var qty = $(this).parent().parent().find('.qty').val();
    //var totalValue = $("#totalShipmentValue").val();
    var totalValue = '0.00';
    if ($.isNumeric(value)) {
        var producttotal = parseFloat(qty * value);
        $(".product-total").each(function(){
            if($(this).val()!='')
                totalValue = parseFloat(totalValue) + parseFloat($(this).val());
        });

        $("#totalShipmentValue").val(totalValue.toFixed(2));
        //producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().find('.product-total').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});

$(document).on('click', '.delDHLRow', function () {
    
    $(this).parent().parent().remove();
    var totalValue = '0.00';
    $(".product-total").each(function(){
        if($(this).val()!='')
            totalValue = parseFloat(totalValue) + parseFloat($(this).val());
    });
    $("#totalShipmentValue").val(totalValue.toFixed(2));
});

function confirmDHLDescription() {
    
    $("#dhlform").removeClass("disableblock");
    $('#confirmModal').modal('hide');
}

function hideDHLDescription() {

    $('#confirmModal').modal('hide');
}

function confirmDHLTrade() {
    
    $("#dhlSubmit").removeClass("disableblock");
    $('#dhl-trade-terms').modal('hide');
}

function hideDHLTrade() {

    $('#dhl-trade-terms').modal('hide');
}

function removePackage(packageId) {
    $("#head-"+packageId).remove();
    $("#body-"+packageId).remove();
    var noOfPkg = $("#piecesCount").val()-1;
    $("#piecesCount").val(noOfPkg);
}


