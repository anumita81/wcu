$(function () {

    $("#addeditFrm").validate();
    
     $('.format').blur(function () {
        if ($.isNumeric(this.value)) {
            var newVal = parseFloat(this.value);
            $(this).val(newVal.toFixed(2));
        } else {
            var newVal = 0;
            $(this).val(newVal.toFixed(2));
        }
    });

    var baseUrl = $('#baseUrl').val();
    $('#makeId').on('change', function () {

        var makeId = $('#makeId').val();

        $('#modelId').find('option:gt(0)').remove();

        if (makeId != '') {
            var url = baseUrl + "/shippingcost/getmodellist/" + makeId;
            // Populate dropdown with list of provinces
            $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    $('#modelId').append($('<option></option>').attr('value', entry.id).text(entry.name));
                })
            });

        }
    });
});