$(function () {

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    
    $('.datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });

    $('.addressBook').on('ifChecked', function (event) {
        $('#addressBook').val($(this).val());
    });

    $('#searchByDate').daterangepicker();

    $("#file-upload").change(function () {
        readURL(this);
    });

    /*Form Validation */
    $("#changeaddressFrm").validate();
    $("#addEditDeliveryFrm").validate();
    /*Form Validation */

    $("#userUnit").keypress(function(e){
        if(e.keyCode == 13)
        {
            userVerification();
        }
    });

    $("#userUnitNumber").on("blur", function (event) {
       var userUnit = $(this).val();
       var groupId = $("#groupId").val();
       $.ajax({
            url: baseUrl + "/groupshipments/getuser/" + userUnit + "/" + groupId,
            type: 'GET',
            success: function (response) {
                if(response == 0)
                {
                    alert("The member does not belongs to coordinators group");
                }else{
                  var data = JSON.parse(response);
                  $("#userName").text(data.memberName); 
                }
                
            }

        });


       
   });


     $('#addMore').on('click', function () {
        var storeId = $("#tb tr:eq(1) #storeId").val();
        var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
        data.find("input").val('');
        data.find("#storeId").val(storeId);
    });


    $("body").on("click", ".remove", function (event) {
        var trIndex = $(this).closest("tr").index();
        if (trIndex > 1) {
            $(this).closest("tr").remove();
        } else {
            alert("Sorry!! Can't remove first row!");
        }
    });



    $("body").on("click", ".removeItem", function (event) {
        $(this).parent().parent().remove();
    });

    $("body").on("change","#warehouseId",function() {
       var selectedWarehouse = $("#warehouseId option:selected").text(); 
       r = confirm("You have selected the warehouse "+selectedWarehouse+" for this shipment. You want to continue with that?");
       if(!r)
       {
           $("#warehouseId option:selected").prop("selected", false)
       }
       else{
            warehouseChanged($("#warehouseId option:selected").val());
       }

});

    $("body").on("change",".siteCategoryId",function () {
        var rootCat = $(this);
        var rootCatVal = rootCat.val();
        if(rootCatVal != '0')
        {
            var baseUrl = $('#baseUrl').val();
            var ajaxMethod = $('#subcatPage').val();
            //rootCat.parent().parent().find('.siteProduct').html('<option value="">Select</option>');
            $.ajax({
                type: "get",
                dataType: 'json',
                url: baseUrl + '/' + ajaxMethod + '/' + rootCatVal,
                data: {},
                success: function (msg) {
                    //console.log(msg.updated);
                    if (msg.data)
                    {
                        //console.log(msg.data);
                        rootCat.parent().parent().find('.subCategory').html('<option value="0">Not Listed</option>');
                        //rootCat.parent().parent().find('.subCategory').append('<option value="0">Not Listed</option>');
                        
                        $.each(msg.data, function (i, j) {
                            //console.log(j);
                            rootCat.parent().parent().find('.subCategory').append('<option value="' + j.id + '">' + j.category + '</option>')
                        });

                    }

                }
            });
        }
        else
        {
            rootCat.parent().parent().find('.subCategory').html('<option value="0">Not Listed</option>');
            rootCat.parent().parent().find('.siteProduct').html('<option value="0">Not Listed</option>');
        }
    });
    
    $("body").on("change",".subCategory",function () {
        var rootCat = $(this);
        var rootCatVal = rootCat.val();
        var baseUrl = $('#baseUrl').val();
        if(rootCatVal !='0')
        {
            $.ajax({
                type: "get",
                dataType: 'json',
                url: baseUrl + '/getsiteproduct/' + rootCatVal,
                data: {},
                success: function (msg) {
                    //console.log(msg.updated);
                    if (msg.data)
                    {
                        //console.log(msg.data);
                        rootCat.parent().parent().find('.siteProduct').html('<option value="0">Not Listed</option>');
                        //rootCat.parent().parent().find('.siteProduct').append('<option value="0">Not Listed</option>');
                        $.each(msg.data, function (i, j) {
                            //console.log(j);
                            rootCat.parent().parent().find('.siteProduct').append('<option value="' + j.id + '">' + j.product + '</option>');
                        });

                    }
                }
            });
        }
        else
        {
            rootCat.parent().parent().find('.siteProduct').html('<option value="0">Not Listed</option>');
        }
    });
    
    $("body").on("change",".siteProduct",function() {
        var productVal = $(this).val();
        if(productVal !='')
        {
            $.ajax({
                type: "get",
                dataType: 'json',
                url: baseUrl + '/getsiteproductweight/' + productVal,
                data: {},
                success: function (msg) {
                    if(msg!='0')
                    {
                        var weightOfItem = 0;
                        var totalItem = 0;
                        if($('#totalWeight').html()!=undefined)
                        {
                            weightOfItem = parseInt($('#totalWeight').html())+msg;
                        }
                        else
                            weightOfItem = msg;
                        $('.siteProduct').each(function(){
                            if($(this).val()>0)
                            {
                                totalItem++;
                            }
                        });
                        
                        $("#foundProductDetails").html('<span style="text-color:#e01d44">We found weigth of '+totalItem+' item(s) is: <span id="totalWeight">'+weightOfItem+'</span>lbs <span data-toggle="tooltip" title="Enter dimension of all items together" class="toolTip"><i class="fa fa-question-circle"></i></span></span>');
                    }
                }
            });
        }
    });

    $("body").on("change",".storeId",function () {
        var rootCat = $(this);
        var rootCatVal = rootCat.val();
        if(rootCatVal != '0')
        {
            
            var baseUrl = $('#baseUrl').val();
            //rootCat.parent().parent().find('.siteProduct').html('<option value="">Select</option>');
            $.ajax({
                type: "get",
                dataType: 'json',
                url: baseUrl + "/shipments/getcategorylist/" + rootCatVal,
                data: {},
                success: function (msg) {
                    //console.log(msg.updated);
                    if (msg)
                    {
                      
                        rootCat.parent().parent().find('.siteCategoryId').html('<option value="0">Not Listed</option>');
                        rootCat.parent().parent().find('.subCategory').html('<option value="0">Not Listed</option>');
                        rootCat.parent().parent().find('.siteProduct').html('<option value="0">Not Listed</option>');                        
                        $.each(msg, function (i, j) {
                            rootCat.parent().parent().find('.siteCategoryId').append('<option value="' + j.id + '">' + j.categoryName + '</option>')
                        });

                    }

                }
            });
        }
        else
        {
            rootCat.parent().parent().find('.siteCategoryId').html('<option value="0">Not Listed</option>');
            rootCat.parent().parent().find('.subCategory').html('<option value="0">Not Listed</option>');
            rootCat.parent().parent().find('.siteProduct').html('<option value="0">Not Listed</option>');
        }
    });

     $("body").on("click",".addButt", function () {
        var table = $(this).parent().prev();
        var firstCategory = table.children('tbody').children("tr").first().find('.packagestore').val();
        table.find('tbody').append('<tr id="test">' + table.children('tfoot').find("#blank_items").html() + '</tr>');
        $(this).parent().prev().children('tbody').children("tr").last().find(".storeId").val(firstCategory);
    });

     /* Show Edit Delivery Item Form */
$("body").on("click", ".editPackage", function (event) {
    $(this).closest("tr").each(function () {

        $('.packagecategory', this).each(function () {
            var categoryId = $(this).val();
            var category = $(this);
            var subcategoryId = $(this).parent().parent().next().find('.packagesubcategoryId').val();
            var productId = $(this).parent().parent().next().next().find('.packageproductId').val();

            // Populate dropdown with list of subcategories
            $(this).parent().parent().next().find('.packagesubcategory').find('option:gt(0)').remove();
            $.ajax({
                url: baseUrl + "/groupshipments/getsubcategorylist/" + categoryId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    $.each(response, function (key, entry) {
                        if (subcategoryId == entry.id)
                            $(category).parent().parent().next().find('.packagesubcategory').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.category));
                        else
                            $(category).parent().parent().next().find('.packagesubcategory').append($('<option></option>').attr('value', entry.id).text(entry.category));
                    });
                }
            });

            // Populate dropdown with list of products
            $(this).parent().parent().next().next().find('.packageproduct').find('option:gt(0)').remove();
            $.ajax({
                url: baseUrl + "/groupshipments/getproductlist/" + subcategoryId,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    $.each(response, function (key, entry) {
                        if (productId == entry.id)
                            $(category).parent().parent().next().next().find('.packageproduct').append($('<option selected="selected"></option>').attr('value', entry.id).text(entry.productName));
                        else
                            $(category).parent().parent().next().next().find('.packageproduct').append($('<option></option>').attr('value', entry.id).text(entry.productName));
                    });
                }
            });
        });

        $('.value_label', this).each(function () {
            $(this).hide();
        });
        $('.value_field', this).each(function () {
            $(this).show();
        });
    });
    //$(this).hide();
    $(this).next().show();
});

$("body").on("change", ".packagecategory", function (event) {

    var categoryId = $(this).val();
    var category = $(this);

    // Populate dropdown with list of subcategories
    $(this).parent().parent().next().find('.packagesubcategory').find('option:gt(0)').remove();
    $.ajax({
        url: baseUrl + "/groupshipments/getsubcategorylist/" + categoryId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $.each(response, function (key, entry) {
                $(category).parent().parent().next().find('.packagesubcategory').append($('<option></option>').attr('value', entry.id).text(entry.category));
            });
        }
    });

    $(this).parent().parent().next().next().find('.packageproduct').find('option:gt(0)').remove();
});

$("body").on("change", ".packagesubcategory", function (event) {

    var categoryId = $(this).val();
    var category = $(this);

    // Populate dropdown with list of subcategories
    $(this).parent().parent().next().find('.packageproduct').find('option:gt(0)').remove();
    $.ajax({
        url: baseUrl + "/groupshipments/getproductlist/" + categoryId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $.each(response, function (key, entry) {
                $(category).parent().parent().next().find('.packageproduct').append($('<option></option>').attr('value', entry.id).text(entry.productName));
            });
        }
    });
});


/* Add New Delivery Item */
$("body").on("click", ".addDeliveryItem", function (event) {
    $(this).attr('disabled', true);
    var err = false;
    var inputArray = [];
    var shipmentId = $('#shipmentId').val();

    $(this).closest("tr:has(input)").each(function () {

        $('input', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else {
                    if ($(this).hasClass('qty') && $(this).val() < 1)
                    {
                        err = true;
                        alert("Enter value greater than 0 for " + $(this).attr('data-name'));
                    } else if ($(this).hasClass('product-val-min'))
                    {
                        var maxVal = $(this).closest("tr:has(input)").find('.product-val').val();
                        if (parseFloat($(this).val()) > parseFloat(maxVal))
                        {
                            err = true;
                            alert("Product Minimum value can not be greater than product maximum value");
                        }
                    } else if ($(this).hasClass('product-val'))
                    {
                        var minVal = $(this).closest("tr:has(input)").find('.product-val-min').val();
                        if (parseFloat($(this).val()) < parseFloat(minVal))
                        {
                            err = true;
                            alert("Product Maximum value can not be lower than product minimum value");
                        }
                    }
                    if (!err)
                    {
                        inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                    }
                }
            } else
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
        });

        $('select', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
            } else
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
        });
    });

    if (err != true) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/groupshipments/adddeliveryitem",
            type: 'POST',
            data: {data: inputArray},
            success: function (response) {
                alert('Data saved successfully');
                $(this).find('.editPackage').attr('disabled','disabled');
                $("#packageList").load(baseUrl + "/groupshipments/getpackagedetails/" + response);
                $("#infoBoxDetail").load(baseUrl + "/groupshipments/getshipmentdetails/" + shipmentId);
            }
        });
    }

});

/* Save Edit Delivery Item Items */
$("body").on("click", ".savePackage", function (event) {
    $(this).attr('disabled', true);
    var inputArray = [];
    var link = $(this);
    var err = false;
    var shipmentId = $('#shipmentId').val();

    $(this).closest("tr:has(input)").each(function () {
        $('input', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else
                    {
                        if($(this).hasClass('quantityOther') && $(this).val()<1)
                        {
                            err = true;
                            alert("Enter value greater than 0 for "+$(this).attr('data-name'));
                        }
                        else if($(this).hasClass('productvalOtherMin'))
                        {
                            var maxVal = $(this).closest("tr:has(input)").find('.productvalOther').val();
                            if(parseFloat($(this).val())>parseFloat(maxVal))
                            {
                                err = true;
                                alert("Product Minimum value can not be greater than product maximum value");
                            }
                        }
                        else if($(this).hasClass('productvalOther'))
                        {
                            var minVal = $(this).closest("tr:has(input)").find('.productvalOtherMin').val();
                            if(parseFloat($(this).val())<parseFloat(minVal))
                            {
                                err = true;
                                alert("Product Maximum value can not be lower than product minimum value");
                            }
                        }
                        if(!err)
                        {
                            inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                        }
                    }
                    
            } else
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
        });

        $('select', this).each(function () {
            if ($(this)[0].hasAttribute("required")) {
                if ($(this).val() == '') {
                    err = true;
                    alert($(this).attr('data-name') + " field is required.");
                } else{
                    inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
                }
            } else{
                inputArray.push({'name': $(this).attr('name'), 'value': $(this).val()});
            }
        });
    });

    if (err != true) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/groupshipments/editdeliveryitem",
            type: 'POST',
            data: {data: inputArray},
            success: function (response) {
                alert('Data saved successfully');
                $("#packageList").load(baseUrl + "/groupshipments/getpackagedetails/" + response);
                $("#infoBoxDetail").load(baseUrl + "/groupshipments/getshipmentdetails/" + shipmentId);
            }
        });
        $(this).hide();
        $(this).prev().show();
    }
});

/* Delete Delivery Item */
$("body").on("click", ".removePackage", function (event) {
    var id = $(this).attr('data-id');
    var shipmentId = $('#shipmentId').val();

    $.ajax({
        url: baseUrl + "/groupshipments/deletedeliveryitem/" + id,
        type: 'GET',
        success: function (response) {
            if (response == 1) {
                $('#package' + id).remove();
                $("#infoBoxDetail").load(baseUrl + "/groupshipments/getshipmentdetails/" + shipmentId);
            }

        }
    });
});

    $('#shipmentMessage').on('change', function () {
        var messageId = $(this).val();
        if(messageId == "")
        {
            alert("Please select one warehouse message.");
        }else{
            $('#notify-customer').val(messageId);
        }
        
    });

    $('#hideFromCustomer').on('change', function () {
        var hideFromCustomer = $(this).val();
        var shipmentId = $('#shipmentId').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/groupshipments/updateshipment/" + shipmentId,
            type: 'POST',
            data: {hideFromCustomer: hideFromCustomer, field: 'hideFromCustomer'},
            success: function (response) {
                if (response == '1')
                    alert("Information saved successfully!!");
            }
        });
    });




});

/*  Item Add - Calculate Sub Total per Item / Package on Quantity Change  */
$(document).on('blur', '.qty', function () {
    var qty = $(this).val();
    var value = $(this).parent().parent().find('.product-val').val();
    
    
    if ($.isNumeric(qty)) {
        var producttotal = parseFloat(qty * value);
        $(this).parent().parent().find('.product-total').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});

/*  Item Add - Calculate Sub Total per Item / Package on Quantity Change  */
$(document).on('blur', '.qtyAddDelivery', function () {
    var qty = $(this).val();
    var value = $(this).parent().parent().parent().find('.product-val-addDelivery').val();
    /*var shipping = $(this).parent().parent().find('.product-shipping').val();
    if (shipping == '')
        shipping = 0;*/
    
    if ($.isNumeric(qty)) {
        var producttotal = parseFloat(qty * value);
        //producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().parent().find('.product-total-addDelivery').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});

/* Item Add -  Calculate Sub Total per Item / Package  on Value Change */
$(document).on('blur', '.product-val-addDelivery', function () {
    var value = $(this).val();
    var qty = $(this).parent().parent().parent().find('.qtyAddDelivery').val();
    /*var shipping = $(this).parent().parent().find('.product-shipping').val();
    if (shipping == '')
        shipping = 0;*/

    if ($.isNumeric(value)) {
        var producttotal = parseFloat(qty * value);
        //producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().parent().find('.product-total-addDelivery').val(producttotal.toFixed(2));
    } else {
        $(this).val(0);
    }
});

$(document).on('blur', '.product-shipping', function () {
    var shipping = $(this).val();
    var qty = $(this).parent().parent().find('.qty').val();
    var value = $(this).parent().parent().find('.product-val').val();
    if (shipping == '')
        shipping = 0;

    if ($.isNumeric(shipping)) {
        var producttotal = parseFloat(qty * value);
        producttotal = producttotal + parseFloat(shipping);
        $(this).parent().parent().find('.product-total').val((producttotal.toFixed(2)));
    } else {
        $(this).val(0);
    }
});

/* Start packaging validation */
$(document).on('click', '#packagenow', function () {
    var shipmentId = $("#shipmentId").val();
    var deliveryCount = $("#deliveryCount").val();
    var addeddata = $("#labeldata").val();
    var deliveryTobeScaned = $("#deliveryTobeScaned").val();
    var deliveryTobeScanedArr = deliveryTobeScaned.split("^");
    addeddata = addeddata.trim();
    var allData = new Array();
    addeddata.split("\n").forEach(function (item) {
        allData.push(item);
    });

    var match = 1;
    var deliveryScaned = 0;
    if (allData.length > 0)
    {
        for (var i = 0; i < allData.length; i++)
        {
            if (i == 0)
            {
                if (allData[i] != shipmentId)
                {
                    match = 0;
                    break;
                }
            } 
            else
            {
                deliveryScaned++;
                var ifDelivery = allData[i].indexOf('-')
                if (ifDelivery != '-1')
                {
                    var delivId = allData[i].substr(parseInt(ifDelivery) + 1);
                    allData[i] = allData[i].substr(0, ifDelivery);
                    if (allData[i] != shipmentId || (deliveryTobeScanedArr.indexOf(delivId)=="-1"))
                    {
                        match = 0;
                        break;
                    }
                } 
                else
                {
                    match = 0;
                    break;
                }


            }

        }
        if (match != 0 && deliveryScaned != deliveryCount)
        {
            match = 0;
        }
        //console.log(match);
        if (match == 0)
            alert("Wrong data! Please recheck");
        else
        {
            $("#packageShipment").val(shipmentId);
            var action = $("#frmstartpkg").attr('action');
            var data = $("#frmstartpkg").serialize();
            $.ajax({
                url: action,
                data: data,
                dataType: 'JSON',
                type: 'post',
                success: function (response) {
                    if (response.message == 'success')
                    {
                        $('#modal-addEdit').modal('hide');
                        $("#addPackaging").removeAttr("disabled");
                        window.location.reload();
                    } else
                    {
                        alert("Something went wrong");
                    }
                }
            });
        }
    } else
        alert("Wrong data! Please recheck");

    return false;
});
/* End */

$("body").on("click", ".savePackaging", function (event) {
        var link = $(this);
        var updateId = $(this).attr('data-id');
        $(".cb_" + updateId).attr("checked", "checked");
        var data = $("#frmpkgdetails").serializeArray();
        data.push({name: "action", value: 'update'});
        data.push({name: "fromSource", value: "ajax"});
        var url = $("#frmpkgdetails").attr("action");

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: data,
            success: function (response) {
                if (response.message == 'success')
                {
                    window.location.reload();
                }
            }
        });

        $(this).hide();
        $(this).prev().show();

    });

$(document).on('click', '#addPackaging', function () {
    var error = 0;
    $("#frmaddpkg input[type='text']").each(function(){
      if($(this).val()=='' || $(this).val()=='0.00')
      {
          error = 1;
      }
    });
    if(error == 0)
    {
        var action = $("#frmaddpkg").attr("action");
        var data = $("#frmaddpkg").serialize();
        var shipmentId = $(this).attr('data-shipment');
        var page = $(this).attr('data-page');
        var baseUrl = $('#baseUrl').val();
        $.ajax({
            url: action,
            data: data,
            dataType: 'JSON',
            type: 'post',
            success: function (response) {
                if (response.message == 'success')
                {
                    window.location.reload();
                }
            }
        });
    }
    else
    {
        alert('Enter packaging details');
    }

    return false;


});

function getWarehouseZoneList(rowId) {
    $('#warehouseZoneId').find('option:gt(0)').remove();
    if (rowId != '') {
        var url = baseUrl + "/groupshipments/getwarehousezonelist/" + rowId;
        // Populate dropdown with list of provinces
        $.getJSON(url, function (data) {
            $.each(data, function (key, entry) {
                $('#warehouseZoneId').append($('<option></option>').attr('value', entry.id).text(entry.name));
            })
        });

    }
}

function userVerification() {
    var userUnit = $('#userUnit').val();

    if (userUnit != '') {
        $.ajax({
            url: baseUrl + "/groupshipments/verifyuser/" + userUnit,
            type: 'GET',
            success: function (response) {
                if (response == 0) {
                    $('#user-verification').removeClass('text-success');
                    $('#user-verification').addClass('text-danger');
                    $('#user-verification').html('<b>This is an Invalid Customer.</b>');
                    $("#submitBtn").prop("disabled", true);
                    if($(".disableblock").length==0)
                        $("#shipmentDeliveryDetails").addClass("disableblock");
                    $("#customerSettingsBlock").hide();
                }else if(response == -1)
                {
                    alert("User has no group please try to create individual shipment");
                } else {                    
                    $("#warehouseId").html("<option value=''>Select</option>");
                    $.ajax({
                    url: baseUrl + "/groupshipments/getuserwarehouse/" + userUnit,
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                            if(response.data.length>0)
                            {
                                $('#warehouse-location').removeClass('text-danger');
                                $('#warehouse-location').html('');
                                $.each(response.data, function (i, j) {
                                    $("#warehouseId").append('<option value="' + j.id + '">' + j.name +' ('+ j.cityName +')</option>');
                                });
                                $("#shipmentDeliveryRest").removeClass("disableblock");
                                $("#submitBtn").prop("disabled", false);
                                $("#customerSettingsBlock").show();
                                $.each(response.userSettings, function (i, j) {
                                    if(j.fieldType == 'special_instruction'){
                                        if(j.fieldValue != '' || j.fieldValue != 'null')
                                            $('#'+j.fieldType).text(j.fieldValue);
                                        else
                                            $('#'+j.fieldType).text('N/A');
                                    } else {
                                        if(j.fieldValue == 1)
                                            $('#'+j.fieldType).html("<strong class='text-red'>Yes</strong>");
                                         else
                                            $('#'+j.fieldType).html("<strong>No</strong>");
                                    }
                                });

                                $("#groupId").val(response.groupId);
                            }
                            else
                            {
                                $("#shipmentDeliveryRest").addClass("disableblock");
                                $("#submitBtn").prop("disabled", true);
                                $('#warehouse-location').addClass('text-danger');
                                $('#warehouse-location').html('<b>Shipping address is not set for this user.</b>');
                                $("#customerSettingsBlock").hide();
                            }
                        }
                    });
                    $("#shipmentDeliveryDetails").removeClass("disableblock");
                    $('#user-verification').removeClass('text-danger');
                    $('#user-verification').addClass('text-success');
                    $('#user-verification').html('<b>This is a valid Customer.</b>');
                    $('#modal-verify').html(response);
                    $('#modal-verify').modal('show');
                }
            }
        });
    } else {
        $("#addEditFrm input:not('#userUnit')").prop("disabled", true);
        $("#addEditFrm select").prop("disabled", true);
        $('#user-verification').removeClass('text-success');
        $('#user-verification').removeClass('text-danger');
        $('#user-verification').html('');
    }
}

function warehouseChanged(warehouseId)
{

    $('#storeId').find('option:gt(0)').remove();
    if (warehouseId != '') {
        var url = baseUrl + "/groupshipments/getstorelist/" + warehouseId;
        // Populate dropdown with list of provinces
        $.getJSON(url, function (data) {
            $.each(data, function (key, entry) {
                $('#storeId').append($('<option></option>').attr('value', entry.id).text(entry.storeName));
            })
        });

    }

}

function storeChanged(storeId)
{
    $('.siteCategoryId').find('option:gt(0)').remove();
    if (storeId != '') {
        var url = baseUrl + "/groupshipments/getcategorylist/" + storeId;
        // Populate dropdown with list of provinces
        $.getJSON(url, function (data) {
            $.each(data, function (key, entry) {
                $('.siteCategoryId').append($('<option></option>').attr('value', entry.id).text(entry.categoryName));
            })
        });

    }

}

function getShippingMethods() {
    var warehouseId = $("#warehouseId").val();
    var userunit = $("#userUnit").val();
    var totalCost = 0;
    $(".product-total").each(function() {
        totalCost = totalCost + parseInt($(this).val());
    });
    var length = $("#lenth").val();
    var height = $("#width").val();
    var width = $("#height").val();
    var weight = $("#weight").val();
    if(warehouseId == '' || userunit == '' || totalCost == 0 || length == '' || height =='' || width =='' || weight =='')
    {
        alert("Please add all required information first");
    }
    else
    {
        $("#wait").show();
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: baseUrl+"/groupshipments/getavailableshipping",
            type: 'POST',
            dataType : 'json',
            data: {warehouseId:warehouseId,userunit:userunit,totalCost:totalCost,length:length,height:height,width:width,weight:weight},
            success: function (response) {
                $("#wait").hide();
                console.log(response.status);
                if(response.status == 1) {
                    
                    $("#selectedShippingMethod").parent().parent().removeClass("disableblock");
                    $("#selectedShippingMethod").prop("disabled",false);
                    $.each(response.data, function (key, entry) {
                        $('#selectedShippingMethod').append($('<option></option>').attr('value', entry.shippingid).text(entry.shipping));
                    });
                }
            }
        });
    }
}

function getLocationDetails(listIds) {    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: baseUrl+"/groupshipments/getwerehouselocations",
        type: 'POST',
        data: {listIds : listIds},
        success: function (response) {
            if(response)
            {
                var totalArray = JSON.parse(response);

                var array = totalArray.returnLocationArr;
                
                for (var key in array) {
                    var value = array[key];
                    //console.log(key, value);
                    $("#location-"+key).html(value);
                }

                var delievryArray = totalArray.returnDeliveryArr;

                for (var key in delievryArray) {
                    var value = delievryArray[key];
                    $("#delivery-"+key).html(value);
                }

                var dispatchArray = totalArray.returndispatchCompany;

                for (var key in dispatchArray) {
                    var value = dispatchArray[key];
                    $("#dispatch-"+key).html(value);
                }
                
            }
        }
    });
}

function loadDeliveryDetails(shipmentId,shipmentType) {
    $("#wait").show();
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: baseUrl+"/groupshipments/getdeliverydetails",
        type: 'POST',
        data: {shipmentId : shipmentId,shipmentType:shipmentType},
        success: function (response) {
            $("#wait").hide();
            if(response)
            {
                $(".deliveryDtlsColps").html(response);
                $("#loadDeliveryDetails").text('refresh delivery details');
            }
        }
    });
}

function showDeliveryDetails(packageId) {
    var modalContent = $(".deliveryDetaiils-"+packageId).html();
    $("#modal-delivery-details .modal-body").html(modalContent);
    $("#modal-delivery-details").modal('show');
}

function getShippingCharge(shipmentId,deliveryId,shippingId) {
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/groupshipments/methodwiseshippingcharge",
            type: 'POST',
            data: {shipmentId: shipmentId, deliveryId: deliveryId, shippingMethodId: shippingId},
            success: function (response) {
                $(".deliveryShipingCost-"+deliveryId).html(response);
            }
        });
    
}
function addNewCharge(shipmentId,deliveryId) {
    var otherChargeId = $('#otherChargeId'+deliveryId).val();
    var notes = $('#chargeNotes'+deliveryId).val();

    if (otherChargeId != '') {
        $('#otherChrgBtn').attr('disabled', true);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/groupshipments/addshipmentcharge/" + shipmentId+"/"+deliveryId,
            type: 'POST',
            data: {otherChargeId: otherChargeId, notes: notes, type: 'other'},
            success: function (response) {
                if (response == '1') {
                    $('#otherChrgBtn').attr('disabled', false);
                    $('.oCharge').val('');
                    $("#otherChargeList_"+deliveryId).load(baseUrl + "/groupshipments/getothercharges/other/" + shipmentId+"/"+deliveryId, function (responseTxt, statusTxt, xhr) {
                    });
                    $(".deliveryShipingCostBox-"+deliveryId).load(baseUrl + "/groupshipments/deliveryshippingcost/" + shipmentId+"/"+deliveryId, function (responseTxt, statusTxt, xhr) {
                    });
                    
                }
            }
        });
    } else {
        alert("Select a charge to continue!!");
        return false;
    }
}

function addNewChargeNewDelivery(shipmentId) {
    var otherChargeId = $('#otherChargeIdNewdeliv').val();
    var notes = $('#chargeNotesNewdeliv').val();
    if (otherChargeId != '') {
        $('#otherChrgBtn').attr('disabled', true);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: baseUrl + "/groupshipments/addshipmentnewdeliverycharge/" + shipmentId,
            type: 'POST',
            data: {otherChargeId: otherChargeId, notes: notes, type: 'other'},
            success: function (response) {
                if (response == '1') {
                    $('#otherChrgBtn').attr('disabled', false);
                    $('.oCharge').val('');
                    $("#otherChargeList").load(baseUrl + "/groupshipments/getotherchargesnewdelivery/other/" + shipmentId, function (responseTxt, statusTxt, xhr) {
                    });

                }
            }
        });
    } else {
        alert("Select a charge to continue!!");
        return false;
    }
}

function updateDelivery(shipmentId, deliveryId)
{

    var deliveryLength = $('#deliveryBox'+deliveryId+' .deliveryLength').val();
    var deliveryWidth = $('#deliveryBox'+deliveryId+' .deliveryWidth').val();
    var deliveryHeight = $('#deliveryBox'+deliveryId+' .deliveryHeight').val();
    var deliveryWeight = $('#deliveryBox'+deliveryId+' .deliveryWeight').val();

    $.ajax({
        url: baseUrl + "/groupshipments/updatedelivery/" + shipmentId + "/" + deliveryId,
        type: 'POST',
        data: { deliveryLength: deliveryLength, deliveryWidth: deliveryWidth, deliveryHeight: deliveryHeight, deliveryWeight: deliveryWeight},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            alert("Delivery information saved successfully.")
            //$("#infoBoxDetail").load(baseUrl + "/shipments/getshipmentdetails/" + shipmentId);
        }
    });


}

function addNewManualCharge(shipmentId,deliveryId) {
    var otherChargeName = $('#otherChargeName'+deliveryId).val();
    var otherChargeAmount = $('#otherChargeAmount'+deliveryId).val();
    var notes = $('#chargeNotes'+deliveryId).val();

    if (otherChargeName != '' && otherChargeAmount != '') {
        $('#manualChrgBtn').attr('disabled', true);
        $.ajax({
            url: baseUrl + "/groupshipments/addshipmentcharge/" + shipmentId + "/" + deliveryId,
            type: 'POST',
            data: {otherChargeName: otherChargeName, otherChargeAmount: otherChargeAmount, notes: notes, type: 'manual'},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            success: function (response) {
                if (response == '1') {
                    $('#manualChrgBtn').attr('disabled', false);
                    $('.mCharge').val('');
                    $("#manualChargeList_"+deliveryId).load(baseUrl + "/groupshipments/getothercharges/manual/" + shipmentId+"/"+deliveryId, function (responseTxt, statusTxt, xhr) {
                    });
                    $(".deliveryShipingCostBox-"+deliveryId).load(baseUrl + "/groupshipments/deliveryshippingcost/" + shipmentId+"/"+deliveryId, function (responseTxt, statusTxt, xhr) {
                    });
                    $("#totalShipmentChrg").load(baseUrl + "/groupshipments/gettotalcharge/" + shipmentId);
                }
            }
        });
    } else {
        alert("Enter charge name & amount to continue!!");
        return false;
    }
}

function checkValid(form) {
    var totalTrackingField = 0;
    var trackingEntered = 0;
    var error = false

    $('.dash_custom_shipment_tble table tbody tr').each(function() {
        
        var minVal = $(this).find('.product-min-val').val();
        var maxVal = $(this).find('.product-val').val();

        if(parseFloat(minVal) > parseFloat(maxVal))
        {
            error = true;
        }
    });
    
    if(error)
        alert("Minimum price of item can not be higher than maximum value");
    
    if(error)
        return false;
    else
        return true;
    
}

function updateStorageCharge(shipmentId) {
    if (shipmentId != '') {
        $('#storageChrgBtn').attr('disabled', true);
        var storageCharge = $('#storageCharge').val();

        $.ajax({
            url: baseUrl + "/groupshipments/updateshipment/" + shipmentId,
            type: 'POST',
            data: {storageCharge: storageCharge, field: 'storageCharge'},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            success: function (response) {
                if (response == '1') {
                    $('#storageChrgBtn').attr('disabled', false);
                    $("#storageChargeTxt").load(baseUrl + "/groupshipments/updatestoragecharge/" + shipmentId, function (responseTxt, statusTxt, xhr) {
                    });
                    $("#totalShipmentChrg").load(baseUrl + "/groupshipments/gettotalcharge/" + shipmentId);

                }
            }
        });
    }
}

function updateDelivery(shipmentId, deliveryId)
{
    var deliveryLength = $('#deliveryBox'+deliveryId+' .deliveryLength').val();
    var deliveryWidth = $('#deliveryBox'+deliveryId+' .deliveryWidth').val();
    var deliveryHeight = $('#deliveryBox'+deliveryId+' .deliveryHeight').val();
    var deliveryWeight = $('#deliveryBox'+deliveryId+' .deliveryWeight').val();

    $.ajax({
        url: baseUrl + "/groupshipments/updatedelivery/" + shipmentId + "/" + deliveryId,
        type: 'POST',
        data: { deliveryLength: deliveryLength, deliveryWidth: deliveryWidth, deliveryHeight: deliveryHeight, deliveryWeight: deliveryWeight},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            alert("Delivery information saved successfully.")
        }
    });


}

function updatePaymentStatus(shipmentId) {
    var paymentStatus = $('#paymentStatus').val();
    $.ajax({
        url: baseUrl + "/shipments/updatepaymentstatus/" + shipmentId,
        type: 'POST',
        data: {paymentStatus: paymentStatus},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if(response == 1){
                if(paymentStatus == 3 || paymentStatus == 4 || paymentStatus == 5){
                     $('#paymentStatus').attr('disabled','disabled');
                }
                alert("Payment status updated successfully.")
                $("#infoBoxDetail").load(baseUrl + "/groupshipments/getshipmentdetails/" + shipmentId);
            }
        }
    });
}

function updatePayment(shipmentId,type) {
    
    var paymentStatus = $('#paymentInvoiceStatus').val();
    r = confirm("Are you sure? This action cannot be reversed back!!");
    if(r)
    {
        $.ajax({
            url: baseUrl + "/groupshipments/updatepaymentstatus/" + shipmentId+'/'+type,
            type: 'POST',
            data: {paymentStatus: paymentStatus},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if(response == 1){
                    $('#paymenyInvoiceStatus').parent().html('<button type="button" class="btn btn-default custumBtnWtIcn printBtns">'+paymentStatus+'</button>');
                    //$('.updatePayment').hide();
                    window.location.reload();
                }
            }
        });
    }
    
}

function generateInvoice(shipmentId, type)
{
    
    $.ajax({
            url: baseUrl + "/groupshipments/generateInvoice/" + shipmentId+'/'+type,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if(response == 1){
                    
                    window.location.reload();
                }
            }
        });
}

function deleteDelivery(deliveryId) {
    if (deliveryId != '') {
        var r = confirm("All the selected items will be modifed. This action cannot be reversed back!!");
        if (r)
        {
            $.ajax({
                url: baseUrl + "/groupshipments/deletedelivery/" + deliveryId,
                type: 'GET',
                success: function (response) {
                    if (response == 1) {
                        $('#deliveryBox' + deliveryId).remove();
                        alert('Shipment delivery deleted successfully.');
                    }
                }
            });

        }
    }
}

function updateDeliveryStage(deliveryId) {
    var url = $("#updatestageform_"+deliveryId).attr('action');
    var showDiv = false;

        $("#updatestageform_"+deliveryId+" input[type='text']:not([disabled]").each(function(){
            if($(this).attr("name") == "status[in_transit]")
              {
                showDiv = true;
                return false;
              }
              else{
                showDiv = false;
              }
        });
    if(showDiv == true)
    {
        
       showAddEdit(deliveryId, 1, 'groupshipments/assignDispatchCompany');
        
    }
    if(showDiv == false){
       $.ajax({
            url: url,
            data : $("#updatestageform_"+deliveryId).serialize(),
            type: 'POST',
            success: function (response) {
                if (response == 1) {
                    $("#updatestageform_"+deliveryId+" input[type='text']").each(function(){

                       if($(this).val()!="")
                       {
                           $(this).attr("disabled",true);
                       }
                    });
                    alert('Stages data updated successfully.');
                }
            }
        });
     
    }
        
    return false;
}
function assignDispatchCompany(deliveryId)
{
    $('#assignCompany').val($('#shippingMethod').val());
    $('#modal-addEdit').modal('hide');
    var url = $("#updatestageform_"+deliveryId).attr('action');

     $.ajax({
            url: url,
            data : $("#updatestageform_"+deliveryId).serialize(),
            type: 'POST',
            success: function (response) {
                    if (response == 1) {
                         
                        $("#updatestageform_"+deliveryId+" input[type='text']").each(function(){

                           if($(this).val()!="")
                           {
                               $(this).attr("disabled",true);
                           }
                        });
                        alert('Stages data updated successfully.');
                    }
                }
            }); 



}

function assignPackageDispatchCompany(shipmentId)
{
    $('#assignPackageCompany').val($('#shippingMethod').val());
    $('#modal-addEdit').modal('hide');
    var url = $("#updatepackagestages").attr('action');

     $.ajax({
            url: url,
            data : $("#updatepackagestages").serialize(),
            type: 'POST',
            success: function (response) {
                    if (response == 1) {
                        alert('Stages data updated successfully.');
                        window.location.reload();
                    }
                }
            }); 



}

function getWarehouseZoneList(rowId) {
    $('#warehouseZoneId').find('option:gt(0)').remove();
    if (rowId != '') {
        var url = baseUrl + "/groupshipments/getwarehousezonelist/" + rowId;
        // Populate dropdown with list of provinces
        $.getJSON(url, function (data) {
            $.each(data, function (key, entry) {
                $('#warehouseZoneId').append($('<option></option>').attr('value', entry.id).text(entry.name));
            })
        });

    }
}

function addeditlocation()
{
    var shipmentId = $('#shipmentId').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });    

    $.ajax({
        url: $('#addeditLocationFrm').attr('action'),
        type: 'POST',
        data: $('#addeditLocationFrm').serialize(),
        success: function (response) {
            if (response == 1) {
                $("#locationTrack").load(baseUrl + "/groupshipments/getwarehouselocations/" + shipmentId);
                $("#modal-addEdit").modal('hide');
                alert("Warehouse location details saved successfully.");
            }
        }
    }); 
}


function saveComment(shipmentId)
{
    var message = $('#addcomment').val();
    if (message != '') {
        $.ajax({
            url: baseUrl + "/groupshipments/addcomment/" + shipmentId,
            type: 'POST',
            data: {comment: message},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response != '')
                {
                    var url = response["noteId"] + ", " + response["shipmentId"] + ", 'shipments/shownotes'";
                    $('#showNote').prepend('<div class="row"><div class="col-sm-12">By: ' + response["firstname"] + ' ' + response["lastname"] + ', ' + response["email"] + ',' + response["sentOn"] + ', <a onclick="showAddEdit(' + url + ')"><strong>Comment Details</strong></a>. </div> </div>');
                } else {
                    $('#showmsg').html('<div class="danger">Notes not saved</div>')
                }
            }
        });
    } else {
        $('#showmsg').html('<div class="success">Please enter a message above</div>');
        $('#addcomment').focus();
    }

}

function savepacked(shipmentid) {
    
    var r = confirm("You will not be allowed to do any changes in packaging after this. This action cannot be reversed back!!");
    if(r) {
        $.ajax({
        url: baseUrl + "/groupshipments/updatepackedstatus/" + shipmentid,
        type: 'GET',
        success: function (response) {
                if(response == '1')
                {
                    window.location.reload();
                }
            }
        });
    }
}

function updatePackageDeliveryStage(shipmentId) {
    var url = $("#updatepackagestages").attr('action');
    var showDiv = false;

        $("#updatepackagestages input[type='text']:not([disabled]").each(function(){
            if($(this).attr("name") == "status[in_transit]")
              {
                showDiv = true;
                return false;
              }
              else{
                showDiv = false;
              }
        });
    if(showDiv == true)
    {
        
       showAddEdit(shipmentId, 1, 'groupshipments/assignpackagedispatchcompany');
        
    }
    if(showDiv == false){
        $("#wait").show();
       $.ajax({
            url: url,
            data : $("#updatepackagestages").serialize(),
            type: 'POST',
            success: function (response) {
                $("#wait").show();
                if (response == 1) {
                    alert('Stages data updated successfully.');
                    window.location.reload();
                }
            }
        });
     
    }
        
    return false;
}

function scheduleNotification() {
    var selected = [];
    $('#checkboxes input:checked').each(function () {

        selected.push($(this).attr('value'));
    });
    var checkedIds = selected.join('^');
    if (checkedIds != '') {
        showAddEdit(checkedIds, 0, 'groupshipments/schedulenotification');
    } else {
        alert("Select atleast one field");
        return false;
    }
}

function checkForDuplicates(arr) {
    var x = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (x != arr[i]) {
            return 0;
        }
    }
    return 1;
}

function exportall_submit() {
    if ($('#exportAll input[type=checkbox]:checked').length) {
        document.getElementById("exportAll").submit();
        return false;
    } else {
        $("#errorTxt").css("display", "block");
        $("#errorTxt").text("Select atleast one field");
    }

}
function exportallse_submit() {

    if ($('.selecteField:checked').length) {
        var selectedField = [];
        $('.selecteField:checked').each(function () {
            selectedField.push($(this).attr('value'));
        });

        var selected = [];
        $('#checkboxes input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
        if (selected == '') {
            setTimeout(function () {
                $('#modal-export-selected').modal('show');
            }, 1000);
            $("#errorTxtse").css("display", "block");
            $("#errorTxtse").text("Select atleast one user");
        } else {
            $(document).ready(function () {
                var _token = $("input[name='_token']").val();

                $.ajax({
                    url: "groupshipments/exportselected/1",
                    type: 'POST',
                    data: {selected: selected, selectall: selectedField, _token: _token},
                    success: function (data) {
                        window.location = data.path;
                    }
                });
            });
        }
        return false;
    } else {
        setTimeout(function () {
            $('#modal-export-selected').modal('show');
        }, 1000);
        $("#errorTxtse").css("display", "block");
        $("#errorTxtse").text("Select atleast one field");
        return false;
    }

}

function changeStatus() {
    var selected = [];
    var shipmentstatus = [];
    $('#checkboxes input:checked').each(function () {
        shipmentstatus.push($(this).attr('data-attr'));
        selected.push($(this).attr('value'));
    });
    if (checkForDuplicates(shipmentstatus) == 1) {
        var checkedIds = selected.join('^');
        if (checkedIds != '') {
            var r = confirm("Status of all the selected items will be modified. This action cannot be reversed back!!");
            if (r == true) {
                showAddEdit(checkedIds, 0, 'groupshipments/bulkchangestatus');
            }
        }
    } else {
        alert('Please select shipments having same status!!');
    }
}

